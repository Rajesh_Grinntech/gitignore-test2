/**
========================================================================================================================
@page rtc_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	rtc.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	14-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_RTC_H_
#define CORE_LAYER_RTC_H_

/* Includes for definitions */
#include "main.h"

/* Macro definitions */
#define RTC_MODULE_ENABLE 		COM_HDR_ENABLED


/** @{
 * Type definition */
#define RTC_INT_STARTING_YEAR 	2020
#define RTC_SLEEP_TIME_SEC			10
#define RTC_SLEEP_CLK				RTC_CR_WUCKSEL_2 /* 1MHz Clock -> Counter Resolution 1s to 36hr ;  Need To Confirm*/

typedef struct rtc_timedate_tst_tag
{
	/* Should not change the order */ /* based on debug communication protocol*/
	U8 hours_u8;
	U8 minutes_u8;
	U8 seconds_u8;
	U8 date_u8;
	U8 months_u8;
	U16 year_u16;
	U8 weekdays_u8;
}rtc_timedate_tst;

#if 0
typedef struct rtc_alrm_timedate_tst_tag
{
	U8 date_u8;
	U8 hours_u8;
	U8 minutes_u8;
	U8 seconds_u8;
}rtc_alrm_timedate_tst;
#endif

/** @} */
/** @{
 *  Public Variable Definitions */

extern BOOL rtc_wakeup_flag_b;
/** @} */

/* Public Function Prototypes **/
extern U8 gt_rtc_init_u8(void);
extern U8 gt_rtc_start_base_u8(void);
extern U8 gt_rtc_stop_base_u8(void);
extern U8 gt_rtc_set_time_date_u8(rtc_timedate_tst * rtc_time_arpst);
extern U8 gt_rtc_get_time_date_u8(rtc_timedate_tst * rtc_time_arpst);
extern U8 gt_rtc_set_alarm_u8(rtc_timedate_tst* rtc_alrm_arpst, BOOL repeat_enable_arb);
extern U8 gt_rtc_set_wakeup_timer_u8();
extern U8 gt_rtc_wakeup_timer_deinit_u8();
extern U8 gt_rtc_deinit_u8(void);

#endif /* CORE_LAYER_RTC_H_ */
