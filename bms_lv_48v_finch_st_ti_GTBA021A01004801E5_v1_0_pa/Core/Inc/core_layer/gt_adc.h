/**
========================================================================================================================
@page adc_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	adc.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_ADC_H_
#define CORE_LAYER_ADC_H_

/* Includes for definitions */
#include "main.h"

/* Macro Definitions */
#define ADC_INSTANCE_0			COM_HDR_ENABLED
#define ADC_INSTANCE_1			COM_HDR_DISABLED



/** @{
 * Type definition */
typedef enum adc_instances_te_tag
{
	ADC_0_INSTANCE = 0,
	ADC_1_INSTANCE,

}adc_instances_te;

typedef enum adc_channels_te_tag
{
	ADC_0_CHANNEL0 = 0,
	ADC_0_CHANNEL1,
	ADC_0_NUM_CHANNELS

}adc_channels_te;

#if 0
typedef enum adc_group_instance_te_tag
{
	ADC_0_GROUP_0,
	ADC_0_GROUP_1,
	ADC_1_GROUP_0,

}adc_group_instance_te;
#endif
/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/
extern U8 gt_adc_init_u8(adc_instances_te adc_instances_are);
extern U8 gt_adc_start_conversion_u8(void);
extern U8 gt_adc_stop_conversion_u8(void);
extern U8 gt_adc_read_data_u8(U16* raw_adc_val_arg_pu16);
extern U8 gt_adc_deinit_u8(adc_instances_te adc_instances_are);

extern void MX_ADC1_Init(void);
#endif /* CORE_LAYER_ADC_H_ */
