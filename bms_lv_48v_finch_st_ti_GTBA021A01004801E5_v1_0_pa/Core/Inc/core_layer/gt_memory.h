/**
========================================================================================================================
@page memory_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	memory.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_MEMORY_H_
#define CORE_LAYER_MEMORY_H_

/* Includes for definitions */

/** @{
 * Type definition */

/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/
extern void memory_copy_u8_array_v(U8* destination_arg_pu8, U8* source_arg_pu8, U32 bytes_to_copy_arg_u32);
extern void memory_set_u8_array_v(U8* destination_arg_pu8, U8 value_arg_u8, U32 bytes_to_copy_arg_u32) ;
extern U8 memory_comp_u8(U8 *val1_arpu8, U8 *val2_arpu8, U32 bytes_to_compare_aru32);


#endif /* CORE_LAYER_MEMORY_H_ */
