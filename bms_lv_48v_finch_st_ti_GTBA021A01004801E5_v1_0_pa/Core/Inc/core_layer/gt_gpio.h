/**
========================================================================================================================
@page gpio_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	gpio.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	24-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_GPIO_H_
#define CORE_LAYER_GPIO_H_

/* Includes for definitions */
#include "common_header.h"



/** @{
 * Type definition */
/* IO registers */
typedef enum gpio_port_te_tag
{
    GPIO_PORT_PTA = 0,
	GPIO_PORT_PTB,
	GPIO_PORT_PTC,
	GPIO_PORT_PTD,
	GPIO_PORT_PTE,

    GPIO_MAX_NUM_PORTS     /* always leave this at the end */
}gpio_port_te;

typedef enum gpio_pins_te_tag
{
    GPIO_PINS_0 = 0,
	GPIO_PINS_1,
	GPIO_PINS_2,
	GPIO_PINS_3,
	GPIO_PINS_4,
	GPIO_PINS_5,
	GPIO_PINS_6,
	GPIO_PINS_7,
	GPIO_PINS_8,
	GPIO_PINS_9,
	GPIO_PINS_10,
	GPIO_PINS_11,
	GPIO_PINS_12,
	GPIO_PINS_13,
	GPIO_PINS_14,
	GPIO_PINS_15,
	GPIO_PINS_16,
	GPIO_PINS_17,

    GPIO_MAX_NUM_PINS     /* always leave this at the end */
}gpio_pins_te;

typedef enum  gpio_do_state_te_tag
{
   GPIO_DO_INIT = 0,
   GPIO_DO_ON,
   GPIO_DO_OFF,
   GPIO_DO_FAULT,
   GPIO_DO_MAX_STATE
}gpio_do_di_state_te;

typedef enum  gpio_di_state_te_tag
{
   GPIO_DI_INIT = 0,
   GPIO_DI_ON,
   GPIO_DI_OFF,

   GPIO_DI_MAX_STATE
}gpio_di_state_te;

typedef struct gpio_do_map_dir_tst_tag
{
	gpio_port_te gpio_port_reg_e;
	gpio_pins_te gpio_pins_e;
	gpio_do_di_state_te gpio_default_state_e;

}gpio_do_map_dir_tst;

typedef struct gpio_di_int_tst_tag
{
	gpio_port_te gpio_port_reg_e;
	gpio_pins_te gpio_pins_e;
	//port_interrupt_config_t gpio_int_state_e;
}gpio_di_int_tst_tag;
/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/
extern U8 gt_gpio_pin_mux_init_u8(void);
extern void gt_gpio_digital_output_init_v(gpio_do_map_dir_tst gpio_io_map_dir_arst);
extern void gt_gpio_digital_interrupt_init_v(gpio_port_te port_state);
extern void gt_gpio_set_direct_output_v(const U16 pin_number_aru16,gpio_do_di_state_te state_are,const gpio_do_map_dir_tst *gpio_io_map_dir_arpst);
extern void gt_gpio_toggle_direct_output_v(const U16 pin_number_aru16,const gpio_do_map_dir_tst *gpio_io_map_dir_arpst);
extern void gt_gpio_get_direct_input_v(const U16 pin_number_aru16, gpio_do_di_state_te *state_are,const gpio_do_map_dir_tst *gpio_io_map_dir_arpst);

extern void MX_GPIO_Init(void);
#endif /* CORE_LAYER_GPIO_H_ */
