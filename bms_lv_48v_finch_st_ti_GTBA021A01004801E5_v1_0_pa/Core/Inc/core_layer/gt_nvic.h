/**
========================================================================================================================
@page nvic_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	nvic.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	09-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_NVIC_H_
#define CORE_LAYER_NVIC_H_

/* Includes for definitions */
#include "common_header.h"
#include "main.h"

/* @{
 * Macro Definitions */
 
 /** @{
 * Type definition */
typedef enum nvic_irq_state_te_tag
{
    NVIC_IRQ_DISABLE = 0,
    NVIC_IRQ_ENABLE = 1

}nvic_irq_state_te;

typedef enum nvic_irq_lock_te_tag
{
    NVIC_IRQ_LOCK_DISABLE = 0,
    NVIC_IRQ_LOCK_ENABLE = 1

}nvic_irq_lock_te;

typedef struct nvic_init_struct_st_tag
{
    IRQn_Type irq_num_e;
    U8 priority_u8;
    nvic_irq_lock_te preos_irqlock_e;
    nvic_irq_state_te state_e;
} nvic_init_struct_st;
/* @} */

/* @{
 * Type definition */

/* @} */

/* @{
 *  Public Variable Definitions */

/* @} */

/* Public Function Prototypes **/
extern void nvic_init_v(void);
extern void nvic_enable_all_interrupts_v(void);
extern void nvic_disable_all_interrupts_v(void);
extern void nvic_enable_interrupt_v(IRQn_Type interrupt_are);
extern void nvic_disable_interrupt_v(IRQn_Type interrupt_are);


#endif /* CORE_LAYER_NVIC_H_ */
