/**
========================================================================================================================
@page wdg_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	wdg.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	08-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_WDG_H_
#define CONFIGURATION_LAYER_WDG_H_

/* Includes for definitions */
#include "common_header.h"
/* @{
 * Macro definition */
#define WDG_MODULE_ENABLE 	(COM_HDR_ENABLED)


#define WDG_WINDOW_ENABLE 	COM_HDR_DISABLED	//Window
#define WDG_IDPT_ENABLE		COM_HDR_ENABLED	//Independent
/* @} */

/* @{
 * Type definition */

/* @} */

/* @{
 *  Public Variable Definitions */
#if WDG_MODULE_ENABLE

#if WDG_IDPT_ENABLE /* Independent Watchdog*/
extern IWDG_HandleTypeDef hiwdg;
#endif

#if WDG_WINDOW_ENABLE /* Window Watchdog*/
extern WWDG_HandleTypeDef hwwdg;
#endif

#endif
/* @} */

/* Public Function Prototypes **/

extern U8 wdg_init_u8(void);
extern U8 wdg_deinit_u8(void);
extern void wdg_refresh_counter_v(void);

#if WDG_MODULE_ENABLE

#if WDG_IDPT_ENABLE /* Independent Watchdog*/
extern void MX_IWDG_Init(void);
#endif

#if WDG_WINDOW_ENABLE /* Window Watchdog*/
extern void MX_WWDG_Init(void);
#endif

#endif
#endif /* CONFIGURATION_LAYER_WDG_H_ */
