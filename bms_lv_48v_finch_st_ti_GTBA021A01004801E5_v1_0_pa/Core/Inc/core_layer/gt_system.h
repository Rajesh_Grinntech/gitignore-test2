/**
========================================================================================================================

@page system_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	system.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	08-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_SYSTEM_H_
#define CORE_LAYER_SYSTEM_H_

/* Includes for definitions */
#include "common_header.h"
/* @{
 * Macro definition */


/** @{
 * Private Definitions **/

/** @{
 * Type definition */
typedef enum system_fault_sts_te_tag
{
	SYSTEM_NO_FAULT = 0	,
	SYSTEM_FAULT
}system_fault_sts_te;

typedef enum system_diag_init_fault_bits_te_tag
{
	/* Bootloader Faults STARTS*/
	FAULT_BIT_BOOT_CLOCK_INIT			,
	FAULT_BIT_BOOT_CAN_INIT				,
	FAULT_BIT_BOOT_SPI_INIT				,

	FAULT_BIT_BOOT_CANTX				,
	FAULT_BIT_BOOT_SPI_FLASH			,

	/* Bootloader Faults ENDS*/

	FAULT_BIT_CLOCK_INIT			,

	FAULT_BIT_ADC_INIT				,
	FAULT_BIT_CAN_INIT				,

	FAULT_BIT_I2C_INIT			,
	FAULT_BIT_SPI_INIT			,

	FAULT_BIT_TIMER6			,
	FAULT_BIT_TIMER7			,
	FAULT_BIT_TIMER15			,

	FAULT_BIT_AFE_START			,
	FAULT_BIT_CAN_TX			,
	FAULT_BIT_SPI_FLASH_INIT	,

	FAULT_BIT_TIM6_IT			,
	FAULT_BIT_TIM7_IT			,
	FAULT_BIT_TIM15_IT			,

	FAULT_BIT_TIMER16			,
	FAULT_BIT_TIM16_IT			,

	FAULT_BIT_RTC_INIT				,
	FAULT_BIT_RTC_FAILED_SET_TIME	,
	FAULT_BIT_RTC_FAILED_SET_DATE	,
	FAULT_BIT_RTC_FAILED_RUN		,

	FAULT_BIT_OS_CREATE_QUEUE	,
	FAULT_BIT_OS_CREATE_TASK	,

	FAULT_BIT_AFE_READ			,
	FAULT_BIT_AFE_FET_MALFN		,
	FAULT_BIT_CB_FET_MALFN		,

	FAULT_BIT_ADC_READ			,




}system_diag_init_fault_bits_te;

typedef enum system_diag_app_runtime_flt_bits_te_tag
{
	SYSTEM_RT_FLT_BIT_AFE_CRITICAL 		= 0,					/* RT: Run Time */
	SYSTEM_RT_FLT_BIT_AFE_NON_CRITICAL	,
	SYSTEM_RT_FLT_BIT_HVADC_SNS			,
	SYSTEM_RT_FLT_BIT_ISNS				,
	SYSTEM_RT_FLT_BIT_HVADC_FUS_SNS		,
	SYSTEM_RT_FLT_BIT_C_LV				,
	SYSTEM_RT_FLT_BIT_C_HV				,
	SYSTEM_RT_FLT_BIT_P_LV				,
	SYSTEM_RT_FLT_BIT_P_HV				,
	SYSTEM_RT_FLT_BIT_DELTA_PV			,
	SYSTEM_RT_FLT_BIT_C_HCT				,
	SYSTEM_RT_FLT_BIT_C_HDT				,
	SYSTEM_RT_FLT_BIT_P_LT				,
	SYSTEM_RT_FLT_BIT_P_HT				,
	SYSTEM_RT_FLT_BIT_M_LT				,
	SYSTEM_RT_FLT_BIT_M_HT				,
	SYSTEM_RT_FLT_BIT_CI				,
	SYSTEM_RT_FLT_BIT_DCI				,
	SYSTEM_RT_FLT_BIT_DDCI				,
	SYSTEM_RT_FLT_BIT_SCD				,
	SYSTEM_RT_FLT_BIT_BAL				,

}system_diag_app_runtime_flt_bits_te;

#if 0
typedef enum system_diag_os_init_fault_bits_te_tag
{
	FAULT_BIT_OS_CREATE_SEMPR = 32,
	FAULT_BIT_OS_CREATE_QUEUE	,
	FAULT_BIT_OS_CREATE_MUTEX	,
	FAULT_BIT_OS_CREATE_TASK
}system_diag_os_init_fault_bits_te;

typedef enum system_diag_app_fault_bits_te_tag
{
	FAULT_BIT_AFE_START = 40	,
	FAULT_BIT_FLASH_START		,
	FAULT_BIT_CAN_RX_START		,
	FAULT_BIT_AFE_START_MEAS	,
	FAULT_BIT_AFE_HW_FAILED		,
	FAULT_BIT_HVADC_SNS_START	,
	FAULT_BIT_ISNS_START		,
}system_diag_app_init_fault_bits_te;

typedef enum system_diag_app_runtime_flt_bits_te_tag
{
	SYSTEM_RT_FLT_BIT_AFE_CRITICAL = 48	,					/* RT: Run Time */
	SYSTEM_RT_FLT_BIT_AFE_NON_CRITICAL	,
	SYSTEM_RT_FLT_BIT_HVADC_SNS			,
	SYSTEM_RT_FLT_BIT_ISNS				,
}system_diag_app_runtime_flt_bits_te;
#endif
/** @} */
/** @{
 *  Public Variable Definitions */
#if 0
extern U32 system_diag_init_fault_code_gu32;
extern U8  system_diag_os_init_fault_codes_gu8;
extern U16 system_diag_app_init_fault_code_gu16;
#endif
extern U64 app_main_system_flt_status_u64;
extern U64 system_diag_app_runtime_flt_code_gu64;
extern U64 system_diag_app_runtime_prev_flt_gu64;
/** Public Function Prototypes **/
extern void system_mcu_wait_us_v(U32 delay);
extern void system_mcu_wait_ms_v(U32 delay);
extern U16 system_generate_crc_u16(U8 *debug_data_array_au8, U16 length_u16);

#endif /* CORE_LAYER_SYSTEM_H_ */
