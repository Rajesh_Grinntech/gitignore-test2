/**
========================================================================================================================
@page timer_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	timer.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	23-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef TEMPLATE_H_
#define TEMPLATE_H_

/* Includes for definitions */
#include "main.h"


/** @{
 * Type definition */
typedef enum timer_resolution_te_tag
{
 //   TIMER_RESOLUTION_MICROSECOND,  /*!< Tick resolution is microsecond */
    TIMER_RESOLUTION_MILISECOND    /*!< Tick resolution is millisecond  */
}timer_resolution_te;

/** @} */
/** @{
 *  Public Variable Definitions */
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern TIM_HandleTypeDef htim15;
extern TIM_HandleTypeDef htim16;


/** @} */

/* Public Function Prototypes **/
void MX_TIM6_Init(void);
void MX_TIM7_Init(void);
void MX_TIM15_Init(void);
void MX_TIM16_Init(U32 timer_1ms_u32);

extern void gt_timer_set_one_shot_delay_v(U32 comp_timer_u32);
extern void gt_timer_start_base_v();
extern void gt_timer_init_v(void);
extern void gt_timer_delay_v(timer_resolution_te res_e, U32 delay_us_arg_u32);

extern U32 gt_timer_get_tick_u32(void);

#endif /* TEMPLATE_H_ */
