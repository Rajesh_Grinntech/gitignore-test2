/**
========================================================================================================================
@page spi_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	spi.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_SPI_H_
#define CORE_LAYER_SPI_H_

/* Includes for definitions */
#include "main.h"


#define SPI_MASTER_INSTANCE_0	COM_HDR_ENABLED
#define SPI_MASTER_INSTANCE_1	COM_HDR_DISABLED
#define SPI_MASTER_INSTANCE_2	COM_HDR_DISABLED

#define SPI_SLAVE_INSTANCE_0	COM_HDR_DISABLED
#define SPI_SLAVE_INSTANCE_1	COM_HDR_DISABLED
#define SPI_SLAVE_INSTANCE_2	COM_HDR_DISABLED


typedef enum spi_instances_te_tag
{
	SPI_0_INSTANCE = 0,
	SPI_1_INSTANCE,
	SPI_2_INSTANCE,
	MAX_SPI_INSTANCES
}spi_instances_te;

/** @{
 * Type definition */
typedef enum spi_config_te_tag
{
	SPI_INST_0_MASTER_CFG_0 = 0,
	SPI_INST_0_MASTER_CFG_1 = 1,
	SPI_INST_1_MASTER_CFG_0 = 0,
	SPI_INST_1_MASTER_CFG_1 = 1,
	SPI_INST_2_MASTER_CFG_0 = 0,
	SPI_INST_2_MASTER_CFG_1 = 1
}spi_inst_cfg_te;


/* Public Function Prototypes **/
extern U8 gt_spi_master_init_u8(spi_instances_te spi_instance_are);

extern U8 gt_spi_master_transmit_receive_u8(const U8 instance_aru8,const void* tx_buff_arpv,uint16_t tx_num_of_frames_aru16,
		  void* rx_buff_arpv, uint16_t rx_num_of_frames_aru16);
#if 0
extern U8 spi_master_config_u8(spi_instances_te spi_instance_are,
		spi_inst_cfg_te master_cfg_are);
extern U8 gt_spi_slave_init_u8(spi_instances_te spi_instance_are);
extern U8 spi_slave_transfer_u8(spi_instances_te spi_instance_arg,const U8 *tx_buffer_arpu8,
		U8 *rx_buffer_arpu8, U16 tx_byte_count_aru16);
extern U8 spi_slave_abort_transfer_u8(spi_instances_te spi_instance_are);
extern U8 spi_slave_get_transfer_sts_u8(spi_instances_te spi_instance_arg_te,U32 *bytes_remained_pu32);
extern U8 spi_slave_deinit_u8(spi_instances_te spi_instance_are);
#endif
extern U8 gt_spi_master_deinit_u8(spi_instances_te spi_instance_are);

extern void MX_SPI1_Init(void);
#endif /* CORE_LAYER_SPI_H_ */
