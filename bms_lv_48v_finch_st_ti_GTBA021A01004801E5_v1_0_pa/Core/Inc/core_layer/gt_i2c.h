/**
========================================================================================================================
@page i2c_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	i2c.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_I2C_H_
#define CORE_LAYER_I2C_H_

/* Includes for definitions */
#include "main.h"

/* Macro Definitions */
#define I2C_PAL_INSTANCE_0					COM_HDR_ENABLED
#define I2C_PAL_FLEXIO_INSTANCE				COM_HDR_DISABLED

#define I2C_INST_0				&hi2c1

/** @{
 * Type definition */
typedef enum i2c_inst_type_te_tag
{
	I2C_INSTANCE = 0,
	I2C_INSTANCE_FLEXIO = 1,
}i2c_inst_type_te;

/** @} */
/** @{
 *  Public Variable Definitions */
extern I2C_HandleTypeDef hi2c1;

extern U8 i2c_inst0_slv_addr_u8;
/** @} */

/* Public Function Prototypes **/
extern U16 gt_i2c_init_u16(i2c_inst_type_te inst_type_are);
extern U16 gt_i2c_write_data_blocking_u16(i2c_inst_type_te inst_type_are, U8 *tx_buff_arpu8, U32 tx_size_aru32,U8 is_send_stop_aru8);
extern U16 gt_i2c_read_data_blocking_u16(i2c_inst_type_te inst_type_are,U8 *rx_buff_arpu8, U32 rx_size_aru32,U8 is_send_stop_aru8);
extern U16 gt_i2c_set_slave_address_u16(i2c_inst_type_te inst_type_are,U8 slave_addr_aru8,BOOL is_10bit_addr_arb);
extern U16 gt_i2c_deinit_u16(i2c_inst_type_te inst_type_are);

extern void MX_I2C1_Init(void);
#endif /* CORE_LAYER_I2C_H_ */
