/**
========================================================================================================================
@page can_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	can.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CORE_LAYER_CAN_H_
#define CORE_LAYER_CAN_H_

/* Includes for definitions */
#include "main.h"

#define CAN_TX_BLOCKING_MAX_TIMEOUT_MS (10U)

//#define CAN_TX_MAILBOX0 				(0U)
//#define CAN_TX_MAILBOX1 				(1U)
//
//#define CAN_RX_MAILBOX0 				(2U)
//#define CAN_RX_MAILBOX1 				(3U)
//#define CAN_RX_MAILBOX2 				(4U)
//#define CAN_RX_MAILBOX3 				(5U)

#define CAN_INSTANCE_0			 		COM_HDR_ENABLED
#define CAN_INSTANCE_1			 		COM_HDR_DISABLED
#define CAN_INSTANCE_2			 		COM_HDR_DISABLED

#define CAN_0_IDE						CAN_ID_EXT

/** @{
 * Type definition */

typedef enum can_tx_type_te_tag
{
	CAN_TX_BLOCKING = 0x01,
	CAN_TX_NB_DMA,
	CAN_TX_NB_IT

}can_tx_type_te;

typedef enum can_instances_te_tag
{
	CAN_0_INSTANCE = 0,
	CAN_1_INSTANCE,
	CAN_2_INSTANCE
}can_instances_te;

typedef struct can_message_tst_tag
{
	U32 can_id_u32;
  U8  data_au8[8];
  U8  length_u8;
}can_message_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
//extern can_message_t can_0_rx_message_buf_tst;
//extern can_message_t can_2_rx_message_buf_tst;

/** @} */

/* Public Function Prototypes **/
extern U8 gt_can_init_u8(U8 can_instance_aru8);
extern U8 gt_can_transmit_data_u8(can_instances_te can_instance_are,U8 bank_num_aru8, can_tx_type_te type_are, can_message_tst *msg_arpst);
//extern U8 can_receive_data_u8(can_instances_te can_instance_are, U8 bank_num_aru8);
extern U8 can_deinit_u8(U8 can_instance_aru8);
extern void MX_CAN1_Init(void);

#endif /* CORE_LAYER_CAN_H_ */
