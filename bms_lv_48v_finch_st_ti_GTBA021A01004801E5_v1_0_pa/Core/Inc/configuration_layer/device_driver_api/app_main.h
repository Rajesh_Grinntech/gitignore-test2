/**
========================================================================================================================
@page app_main_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	app_main.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	24-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_DEVICE_DRIVER_API_APP_MAIN_H_
#define CONFIGURATION_LAYER_DEVICE_DRIVER_API_APP_MAIN_H_

/* Includes for definitions */
#include "common_header.h"


/** @{
 * Type definition */

/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/

extern U8 app_main_start_u8(void);
extern U8 app_main_post_os_periph_config_u8(void);
extern void app_main_sensor_on_v(void);
extern void app_main_sensor_off_v(void);
extern U8 system_init_flt_update_u8();
#endif /* CONFIGURATION_LAYER_DEVICE_DRIVER_API_APP_MAIN_H_ */
