/**
========================================================================================================================
@page ai_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	ai_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	21-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_DEVICE_DRIVER_API_AI_APP_H_
#define CONFIGURATION_LAYER_DEVICE_DRIVER_API_AI_APP_H_

/* Includes for definitions */
/* @{
 * Macro definition */

/* @} */

/* @{
 * Type definition */
typedef enum ai_app_te_tag
{
	/* MCU ADC 0 instances */
	//AI_APP_NTC1_TEMP_AI_C,
//	AI_APP_NTC2_TEMP_AI_C,

	AI_APP_FUSE_SENS_AI_MV,

	/* MCU ADC 1 instances */

	/* Internal Temp Sensor */
	AI_APP_INT_TEMP_AI_C,

	AI_APP_LAST_MCU_DIRECT_AI,

	//AI_APP_MFET_VOLTAGE_AI_V,

	//AI_APP_LAST_HVADC_AI,

}ai_app_te;

#define AI_APP_NUM_MCU_DIRECT  (AI_APP_LAST_MCU_DIRECT_AI)
//#define AI_APP_NUM_EXT1        (AI_APP_LAST_HVADC_AI )
/* #define AI_APP_NUM_EXT2        (AI_APP_LAST_TMSADC_AI - AI_APP_LAST_MCU_DIRECT_AI - 2 ) */
#define AI_APP_NUM_AI          (AI_APP_NUM_MCU_DIRECT)

typedef enum ai_app_states_te_tag
{
    AI_INIT = 0,
    AI_READING,
    AI_FAULT,
    AI_NUM_STATES       /* leave this at the end of the enum */
} ai_app_states_te;

typedef struct ai_app_status_tst_tag
{
	ai_app_states_te  state_e;
    U16 raw_u16;
    U32 scaled_u32;
    S32 scaled_s32;
    U32 override_u32;
    S32 override_s32;
} ai_app_status_tst;



/* @} */

/* @{
 *  Public Variable Definitions */
extern ai_app_status_tst    ai_app_status_gast[AI_APP_NUM_AI];
/* @} */

/* Public Function Prototypes **/
extern U8 ai_app_init_ai_u8(void);
extern U8 ai_app_read_ai_u8(void);
#endif /* CONFIGURATION_LAYER_DEVICE_DRIVER_API_AI_APP_H_ */
