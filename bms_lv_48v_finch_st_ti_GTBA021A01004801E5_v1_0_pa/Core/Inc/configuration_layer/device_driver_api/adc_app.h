/**
========================================================================================================================
@page adc_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	adc_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	20-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_DEVICE_DRIVER_API_ADC_APP_H_
#define CONFIGURATION_LAYER_DEVICE_DRIVER_API_ADC_APP_H_

/* Includes for definitions */
#include "bms_config.h"

/** @{
 * Type definition */
#define ADC_APP_SUCCESS    			(0)
#define ADC_APP_FAIL       			(1)
#define ADC_CHANNEL_START  			(0)

#define ADC_INIT_U16  				(COM_HDR_MAX_U16 - 2)
#define ADC_FAULT_VALUE_U16 		(COM_HDR_MAX_U16)
#define ADC_INIT_S16  				(COM_HDR_MAX_S16 -2 )
#define ADC_FAULT_VALUE_S16 		(COM_HDR_MAX_S16)

#define ADC_INIT_S32   				(COM_HDR_MAX_S32 - 2)
#define ADC_FAULT_VALUE_S32 		(COM_HDR_MAX_S32)
#define ADC_INIT_U32 				(COM_HDR_MAX_U32 - 2)
#define ADC_FAULT_VALUE_U32 		(COM_HDR_MAX_U32)

#define ADC_APP_NUM_ADC1_DIRECT  	(ADC_APP_LAST_ADC1_DIRECT_AI)

#if 0
/* Number of ADC2 direct channels */
#define ADC_APP_NUM_ADC2_DIRECT  	(ADC_APP_LAST_ADC2_DIRECT_AI - ADC_APP_LAST_ADC1_DIRECT_AI - 1)
#endif

/* Total number of direct channels */
#define ADC_APP_NUM_DIRECT       	(ADC_APP_LAST_ADC1_DIRECT_AI)
/* the +1 in the define below is because ADC_APP_LAST_AD2_DIRECT_AI is just a marker */
//#define ADC_APP_NUM_SERIAL       (ADC_APP_LAST_SERIAL_AI - (ADC_APP_NUM_ADC2_DIRECT + 1))

/* Total number of analogue inputs (ADC_APP_LAST_DIRECT_AI is a dummy that isn't used but required to align mapping) */
#define ADC_APP_NUM_AI           	(ADC_APP_NUM_DIRECT)

/** @} */
/** @{
 *  Public Variable Definitions */
typedef enum ADC_APP_AI_te_tag
{
	/*******************Direct Micro ADC0 iNPUTS************/
//	ADC_APP_NTC1_TEMP_AI_C,
//	ADC_APP_NTC2_TEMP_AI_C,

	ADC_APP_FUSE_SENS_AI_V,

	ADC_APP_INT_TEMP_AI_C,

	ADC_APP_LAST_ADC1_DIRECT_AI,


}ADC_APP_AI_te;

typedef enum adc_app_instances_te_tag
{
	ADC_APP_0_INSTANCE = 0,

}adc_app_instances_te;

typedef struct adc_app_tst_tag
{
	U32 voltage_div_resistor_1ohm_u32;
	U16 adc_ref_voltage_1mV_u16;
	U16 adc_resolution_expanded_u16;
	U16 adc_filter_series_resistor_1ohm_u16;
}adc_app_tst;
typedef struct adc_app_data_tst_tag
{
	U16 dest_ai_u16;
    U16 raw_u16;
    U32 scaled_u32;
    S32 scaled_s32;
} adc_app_data_tst;

/** @} */
extern adc_app_data_tst   adc_app_data_gast[ADC_APP_NUM_AI];

/* Public Function Prototypes **/
extern U8 adc_app_init_adc_u8(adc_app_instances_te instances_are);
extern U8 adc_app_read_ai_u8(void);
extern U8 adc_app_start_conversion_u8(void);
#endif /*CONFIGURATION_LAYER_DEVICE_DRIVER_API_ADC_APP_H_ */
