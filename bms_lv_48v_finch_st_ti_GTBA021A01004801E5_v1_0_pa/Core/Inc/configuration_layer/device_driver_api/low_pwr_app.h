/*
 * LowPowerApp.h
 *
 *  Created on: 15-Oct-2019
 *      Author: Dell
 */

#ifndef LOWPOWERAPP_H_
#define LOWPOWERAPP_H_

/** Includes for definitions **/
#include "common_header.h"

/** Definitions **/
#define SLEEP_MODE_RUNNING   0x02
#define LPAPP_SLEEP_CAN_ID			(0x530)
#define LPAPP_GOTO_SLEEP_AFTER_1MS  (2000U) /* 2 second */
//#define LPAPP_SLEEP_TIME_1S  		(1000000U)   /* 10 second */

typedef enum lpapp_different_requester_te_tag
{
	SLEEP_NOT_INIT = 0,
	SLEEP_ENABLE_CAN = 1,
	SLEEP_DISABLE_CAN,
	SLEEP_ENABLE_CUV,
	SLEEP_DISABLE_NORMAL_AFTER_CUV

}lpapp_different_requester_te;

typedef enum lpapp_task_list_te_tag
{
	TASK_BMS_AFE = 0,
	TASK_BMS_PROT,
	TASK_FET_CTRL,
	TASK_COMM_CAN,
	TASK_STORAGE,
	TASK_DIAG_TASK,
	TASK_CURR_SOC_EST,

	TASK_MAX_NUM
}lpapp_task_list_te;

typedef struct lpapp_mode_status_tst_tag
{
	volatile BOOL enable_low_power_b;
	volatile BOOL enable_resleep_rtc_b;
	U32 goto_sleep_timer_u32;
	volatile BOOL woke_up_b;
	volatile lpapp_different_requester_te LPAPP_current_requesting_resource_e;
	volatile lpapp_different_requester_te LPAPP_previous_mode_e;
	volatile lpapp_different_requester_te LPAPP_current_mode_e;

}lpapp_mode_status_tst;

/** Public Variable Declarations **/
extern volatile U8 lpapp_task_states_agu8[TASK_MAX_NUM];
extern lpapp_mode_status_tst lpapp_mode_status_gst;



/** Public Function Prototypes **/
extern U8 low_pwr_app_enter_low_pwr_mode_u8(void);
extern void low_pwr_app_sm_v(void);
extern void low_pwr_app_wakeup_cantx_v(void);
extern void low_pwr_mode_check_v(void);

#endif /* LOWPOWERAPP_H_ */
