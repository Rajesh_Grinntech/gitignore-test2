/**
========================================================================================================================
@page spi_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	spi_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	30-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_DEVICE_DRIVER_API_SPI_APP_H_
#define CONFIGURATION_LAYER_DEVICE_DRIVER_API_SPI_APP_H_

/* Includes for definitions */
#include "common_header.h"

/*Macro Definitions */
#define SPI_APP_INST_0_MASTER_CFG	COM_HDR_DISABLED
#define SPI_APP_INST_1_MASTER_CFG	COM_HDR_DISABLED
#define SPI_APP_INST_2_MASTER_CFG	COM_HDR_DISABLED

/** @{
 * Type definition */
typedef enum spi_app_device_te_tag
{
	SPI_APP_FLASH,
}spi_app_device_te;

typedef enum spi_app_inst_cfg_te_tag
{
	SPI_APP_0_MASTER_CFG_0 = 0,
	SPI_APP_0_MASTER_CFG_1 = 1,
	SPI_APP_1_MASTER_CFG_0 = 0,
	SPI_APP_1_MASTER_CFG_1 = 1,
	SPI_APP_2_MASTER_CFG_0 = 0,
	SPI_APP_2_MASTER_CFG_1 = 1

}spi_app_inst_cfg_te;

typedef enum spi_app_instances_te_tag
{
	SPI_APP_0_INSTANCE = 0,
	SPI_APP_1_INSTANCE,
	SPI_APP_2_INSTANCE,
	SPI_APP_MAX_INSTANCES
}spi_app_instances_te;


/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/
extern U8 spi_app_master_init_u8(spi_app_device_te spi_app_device_are);
extern U8 spi_app_master_tx_rx_u8(const spi_app_device_te spi_app_device_are,const void* tx_buffer_arpv,
																		 U16 tx_num_of_frames_aru16,
																		 void* rx_buffer_argpv,
																		 U16 rx_num_of_frames_aru16);
#if 0
extern U8 spi_app_slave_init_u8(spi_app_device_te spi_app_device_are);
extern U8 spi_app_slave_transfer_u8(spi_app_device_te spi_app_device_are,const U8 *tx_buffer_arpu8,U8 *rx_buffer_arpu8,
																		   U16 transfer_byte_count_aru16);
extern U8 spi_app_slave_abort_transfer_u8(spi_app_device_te spi_app_device_are);
extern U8 spi_app_slave_get_transfer_sts_u8(spi_app_device_te spi_app_device_are,U32 *bytes_remained_arpu32);
extern U8 spi_app_slave_deinit_u8(spi_app_device_te spi_app_device_are);
#endif
extern U8 spi_app_master_deinit_u8(spi_app_device_te spi_app_device_are);

#endif /* CONFIGURATION_LAYER_DEVICE_DRIVER_API_SPI_APP_H_ */
