/**
========================================================================================================================
@page i2c_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	i2c_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	18-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_DEVICE_DRIVER_API_I2C_APP_H_
#define CONFIGURATION_LAYER_DEVICE_DRIVER_API_I2C_APP_H_

/* Includes for definitions */

/** @{
 * Type definition */
typedef enum i2c_app_inst_type_te_tag
{
	I2C_APP_INSTANCE = 0,
	I2C_APP_INSTANCE_FLEXIO = 1,
}i2c_app_inst_type_te;

/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/
extern U8 i2c_app_init_u8(i2c_app_inst_type_te inst_type_are);
extern U8 i2c_app_tx_u8(i2c_app_inst_type_te inst_type_are, U8 slave_addr_aru8, U8 *data_arpu8, U32 data_len_aru32, U8 is_send_stop_aru8);
extern U8 i2c_app_rx_u8(i2c_app_inst_type_te inst_type_are, U8 slave_addr_aru8, U8 *rx_data_arpu8, U32 data_len_aru32, U8 is_send_stop_aru8);
extern U8 i2c_app_tx_rx_u8(i2c_app_inst_type_te inst_type_are, U8 slave_addr_aru8, U8 *data_arpu8,U32 data_len_tx_aru32,
		U8 is_send_stop_tx_aru8,U8 *rx_data_arpu8, U32 data_len_rx_aru32, U8 is_send_stop_rx_aru8);

#endif /* CONFIGURATION_LAYER_DEVICE_DRIVER_API_I2C_APP_H_ */
