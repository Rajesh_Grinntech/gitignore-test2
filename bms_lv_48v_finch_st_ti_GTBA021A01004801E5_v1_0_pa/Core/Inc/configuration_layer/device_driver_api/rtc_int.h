/**
========================================================================================================================
@page rtc_int_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	rtc_int.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	14-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_DEVICE_DRIVER_API_RTC_INT_H_
#define CONFIGURATION_LAYER_DEVICE_DRIVER_API_RTC_INT_H_

/* Includes for definitions */
#include "common_header.h"
#include "gt_rtc.h"

/** @{
 * Type definition */
typedef struct rtc_int_time_date_tst_tag
{
	/* Should not change the order */ /* based on debug communication protocol*/
	U8 hours_u8;
	U8 minutes_u8;
	U8 seconds_u8;
	U8 date_u8;
	U8 months_u8;
	U16 year_u16;
	U8 weekdays_u8;
} rtc_int_time_date_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern U8 rtc_date_time_flag_u8;

extern rtc_int_time_date_tst rtc_time_date_gst;
/** @} */

/* Public Function Prototypes **/
extern U8 rtc_int_init_u8();
extern U8 rtc_int_start_u8();
extern U8 rtc_int_stop_u8();
extern U8 rtc_int_set_time_date_u8(void *time_date_info_arpv);
extern U8 rtc_int_get_time_date_u8(void *time_date_info_arpv);
extern U8 rtc_int_set_alarm_u8(void *time_date_info_arpv, BOOL repeat_enable_arb);

extern U8 rtc_int_set_wakeup_timer_u8();
extern U8 rtc_int_wakeup_timer_deinit_u8();
#endif /* CONFIGURATION_LAYER_DEVICE_DRIVER_API_RTC_INT_H_ */
