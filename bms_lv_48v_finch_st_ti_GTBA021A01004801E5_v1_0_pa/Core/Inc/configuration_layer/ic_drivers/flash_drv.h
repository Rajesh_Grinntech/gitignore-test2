/**
========================================================================================================================
@page flash_drv_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	flash_drv.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	16-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_IC_DRIVERS_FLASH_DRV_H_
#define CONFIGURATION_LAYER_IC_DRIVERS_FLASH_DRV_H_

/* Includes for definitions */
#include "common_header.h"
#include "common_var.h"
#include "w25q.h"

/* Macro Definitions */
#define FLASH_DRV_CYPRESS_FLASH				(0U)
#define FLASH_DRV_WINBOND_FLASH				(1U)

#define FLASH_DRV_FLASH_IC_USED				(FLASH_DRV_WINBOND_FLASH)

#define SPI_FLASH_DEFAULT_VALUE          	0xFFFFFFFF
#define FLASH_DRV_FLASH_PAGE_SIZE         	256
#define SPI_FLASH_MAX_ADDR           		0xFFFFFFFF
#define SPI_FLASH_SECTOR_SIZE        		4096

//#define FLASH_DRV_PSW_ENABLE           		gpio_app_set_digital_output_v(UC_EN_FLASH_PSW_PTD4, GPIO_DO_ON)
//#define FLASH_DRV_PSW_DISABLE           	gpio_app_set_digital_output_v(UC_EN_FLASH_PSW_PTD4, GPIO_DO_OFF)
#define MAX_SECTOR_ERASE_TIME_MS     		100
#define MAX_BLOCK_ERASE_TIME_MS      		500
#define MAX_CHIP_ERASE_TIME_MS       		2000

/** @{
 * Type definition */
typedef enum flash_drv_flash_type_te_tag
{
	FLASH_DRV_WINBOND = 1,
	FLASH_DRV_CYPRESS
}flash_drv_flash_type_te;

typedef enum flash_drv_erase_mode_te_tag
{
	FLASH_DRV_ERASE_MODE_FULL_CHIP = 1,
	FLASH_DRV_ERASE_MODE_BLOCK,
	FLASH_DRV_ERASE_MODE_SECTOR

}flash_drv_erase_mode_te;

typedef enum spi_flash_status_e_tag
{
  FLASH_DRV_SPI_SUCCESS,
  FLASH_DRV_SPI_FAIL,
  FLASH_DRV_FLASH_DATA_SIZE_EXCEED,
  FLASH_DRV_SPI_UNKNOWN
}spi_flash_Status_e;

typedef enum flash_drv_flash_wr_sts_te_tag
{
	FLASH_DRV_WR_SUCCESS = 0	,
	FLASH_DRV_WR_INPROGRESS		,
	FLASH_DRV_WR_FAILURE
}flash_drv_flash_wr_sts_te;

typedef enum flash_drv_erase_sts_te_tag
{
	FLASH_DRV_SECTOR_ERASE_CYCLE_COMPLETED = 0	,
	FLASH_DRV_SECTOR_ERASE_CYCLE_INPROGRESS		,
	FLASH_DRV_BLOCK_ERASE_CYCLE_COMPLETED		,
	FLASH_DRV_BLOCK_ERASE_CYCLE_INPROGRESS
}flash_drv_erase_sts_te;

typedef enum flash_drv_user_erase_type_te_tag
{
	FLASH_DRV_USR_ERASE_TYPE_CP_DP_EN = 0	,
	FLASH_DRV_USR_ERASE_TYPE_SECTOR_EN		,
	FLASH_DRV_USR_ERASE_TYPE_BLOCK_EN		,
	FLASH_DRV_USR_ERASE_ALL_TYPE_DIS		,
}flash_drv_user_erase_type_te;

typedef enum flash_drv_user_store_type_te_tag
{
	FLASH_DRV_ST_BATT_1_TIME_DATA_TYPE_EN = 0,
	FLASH_DRV_ST_ALL_TYPE_DIS
}flash_drv_user_store_type_te;

typedef enum flash_drv_flash_operation_status_te_tag
{
	FLASH_DRV_OPERATION_COMPLETED = 0	,
	FLASH_DRV_OPERATION_INPROGRESS		,
	FLASH_DRV_OPERATION_FAILED
}flash_drv_flash_operation_status_te;

typedef struct flash_drv_gen_tst_te_tag
{
	#if(FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
	w25qxx_id_te id_e;
	#elif(FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
		cs25flxx_id_te id_e;
	#endif

	U8 uniq_id_au8[8];
	U16 pagesize_u16;
	U32 pagecount_u32;
	U32 sectorsize_u32;
	U32 sectorcount_u32;
	U32 blocksize_u32;
	U32 blockcount_u32;
	U32 capacity_in_kb_u32;
	U8 status_reg1_u8;
	U8 status_reg2_u8;
	U8 status_reg3_u8;
	BOOL addr_4byte_enable_b;
	BOOL lock_b;
} flash_drv_gen_tst;

typedef struct flash_drv_flash_info_st_tag
{
	U8 erase_mode_u8;
	U8 erase_active_sts_u8;
	U8 store_active_sts_u8;
	U8 erase_sts_u8;
	U8 sector_erase_position_u8;
	U8 status_check_wrt_tmr_b;
}flash_drv_flash_info_tst;

typedef struct flash_drv_sector_erase_st_tag
{
	U16 start_sect_cnt_u16;
	U16 end_sect_cnt_u16;
}flash_drv_sector_erase_tst;

typedef struct flash_drv_block_erase_st_tag
{
	U16 start_block_cnt_u16;
	U16 end_block_cnt_u16;
	U16 total_blocks_to_erase_u16;						/* How many blocks to erase */
	U16 current_block_cnt_erased_u16;					/* How many blocks totally erased from total blocks to erase */
	U8 total_blocks_erased_in_percentage_u8;
}flash_drv_blocks_erase_tst;

typedef struct flash_drv_addr_data_tst_tag
{
	U32 start_addr_u32;
	bms_drive_onetime_log_tst bms_batt_upd_log_st;
}flash_drv_addr_data_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern U8 flash_drv_write_sts_u8;
extern U8 flash_drv_sts_check_tmr_cnt_u8;
extern U8 flash_drv_flash_operation_sts_u8;
extern U16 sector_count_u16 ;
extern U16 block_count_u16 ;
extern U8 flash_drv_status_reg_1_u8;
extern flash_drv_flash_info_tst flash_drv_flash_info_st;
extern flash_drv_sector_erase_tst flash_drv_sector_erase_st;
extern flash_drv_blocks_erase_tst flash_drv_blocks_erase_st;
extern flash_drv_addr_data_tst flash_drv_addr_data_st;
/** @} */

/* Public Function Prototypes **/
extern U8 flash_drv_init_u8(void);
extern U8 flash_drv_erase_flash_u8(flash_drv_erase_mode_te mode_are, U32 addr_aru32,U16 erase_wait_time_in_ms_aru16);
extern U8 flash_drv_writebyte_u8(U8 *flash_data_arpu8 ,U32 flash_wr_start_addr_aru32,U32 data_length_aru32);
extern U8 flash_drv_readbyte_u8(U8 *flash_rd_data_arpu8 ,U32 flash_rd_start_addr_aru32,U32 flash_rd_data_length_aru32);
extern U8 flash_drv_app_wr_data_u8(U8 *data_arpu8 ,U32 *write_start_addr_arpu32,U32 data_length_aru32);
extern U8 flash_drv_app_rd_data_u8(U8 *read_data_arpu8 ,U32 read_start_addr_aru32,U32 read_data_length_aru32);
extern void flash_drv_erase_cp_dp_data_v(void);
extern U8 flash_drv_wait_for_wr_end_no_blocking_u8();
extern U8 flash_drv_enable_sleep_u8(void);

#endif /* CONFIGURATION_LAYER_IC_DRIVERS_FLASH_DRV_H_ */
