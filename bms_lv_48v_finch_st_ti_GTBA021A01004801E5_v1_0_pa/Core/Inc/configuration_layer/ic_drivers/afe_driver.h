/**
========================================================================================================================
@page afe_driver_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	afe_driver.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	30-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_IC_DRIVERS_AFE_DRIVER_H_
#define CONFIGURATION_LAYER_IC_DRIVERS_AFE_DRIVER_H_

/* Includes for definitions */
//#include "cell_bal_app.h"
#include "bq76952.h"

/** @{
 * Type definition */
#define AFE_NO_OF_CELLLS_USED 	(BQ76952_NO_OF_CELLLS_USED - 2)
#define AFE_NO_OF_THERMS_USED 	BQ76952_NO_OF_THERMS_USED
#define AFE_DRIVER_USE 			COM_HDR_ENABLED
/* ADC1-A and ADC1-B functional verification. */
//#define DIAG_ADC1

/* Over-voltage and under-voltage functional verification. */
//#define DIAG_CELL_OV_UV

/* CTx open detect. */
#define DIAG_CTX_OPEN

/* Cell voltage channel functional verification. */
//#define DIAG_CELL_VOLT

/* Cell terminal leakage diagnostics. */
#define DIAG_CELL_LEAK

/* Current measurement diagnostic. */
//#define DIAG_CURRENT_MEAS

/* Verification of the current shunt connection to the current channel low-pass
 * filter. */
//#define DIAG_SHUNT_CONN

/* GPIOx OT/UT functional verification. */
//#define DIAG_GPIOX_OT_UT

/* GPIOx open terminal diagnostics. */
//#define DIAG_GPIOX_OPEN

/* Cell balance open load diagnostics. */
//#define DIAG_CBX_OPEN


typedef enum afe_driver_afe_state_te_tag{
	AFE_NO_FAULT,
	AFE_FAULT
}afe_driver_afe_state_te;

typedef enum afe_driver_adc_mode_te_tag
{
	ADC_CELL_VOLTAGE,
	ADC_GPIO_VOLTAGE,
	ADC_ADDITIONAL_PARAMETERS,
	ADC_ALL_PARAMETERS

}afe_driver_adc_mode_te;

typedef struct afe_driver_data_tst_tag
{
#if 0
	U8  ic_rev_number_u8;
	U16 cell_voltage_au16[6];
	U16 gpio_voltage_au16[7];
	S32 coulomb_cnt_raw_s32;
	U16 coulomb_sample_count_u16;
	S32 current_raw_uV_s32;
	S32 measured_current_1mA_s32;
	U16 ic_2nd_ref_voltage_u16;
	U16 sum_of_voltage_1mV_u16;
	S16 ic_internal_die_temp_1dC_s16;
	U16 ic_analog_ref_voltage_u16;
	U16 ic_digital_ref_voltage_adc_a_u16;
	U16 ic_digital_ref_voltage_adc_b_u16;
	BOOL ic_thermal_shutdown_en_b;
	BOOL ic_mux_selftest_fail_b;
	U16 raw_meas_au16[30];
	U16 fault_wakeup_au16[9];
	U16 raw_sys_cfg_au16[2];
	U16 raw_adc_cfg_au16[2];
	U16 cell_bal_register_status_u16;
#endif

	U16 cell_voltage_au16[AFE_NO_OF_CELLLS_USED];
	U16 tos_vtg_u16;
	U16 pack_pin_vtg_u16;
	//U16 fuse_pin_vtg_u16;

	U16 ld_pin_vtg_u16;

	S16 int_temp_1cc_s16;

	S16 mfet_temp_1cc_s16;
	S16 pack_temp_1cc_s16;


	S16 cell_temp_1cc_as16[AFE_NO_OF_THERMS_USED];

}afe_driver_data_tst;

typedef struct  afe_driver_cc_soc_tst_tag
{
	S32 coulomb_cnt_scaled_s32;
	S32 avg_current_1mA_s32;
	S32 delta_1mAmSec_s32;
	S32 cum_capacity_1mAmSec_s32;
	U32 new_sample_time_u32;
	U32 old_sample_time_u32;
}afe_driver_cc_soc_tst;

typedef union afe_driver_diag_status_tun_tag
{
	struct __attribute__((packed))
		{
		   BIT adc_a_b_fn_b :1;
		   BIT cell_ov_uv_b :1;
		   BIT cell_open_detect_b :1;
		   BIT cell_volt_meas_b :1;
		   BIT cell_leak_b :1;
		   BIT curr_meas_b :1;
		   BIT shunt_conn_b :1;
		   BIT gpio_ot_ut_b :1;
		   BIT gpio_open_detect_b :1;
		   BIT cb_open_detect_b :1;
		}diag_status_st;
		U16 diag_status_byte_u16;
}afe_driver_diag_status_tun;

typedef enum afe_driver_fet_ctrl_te_tag
{
	AFE_CHG_FET,
	AFE_DSG_FET,
	AFE_PCHG_FET,
	AFE_PDSG_FET,
	AFE_ALL_FET,
}afe_driver_fet_te;

typedef enum afe_driver_fet_state_te_tag
{
	AFE_FET_ON,
	AFE_FET_OFF,
}afe_driver_fet_state_te;

/** @} */
/** @{
 *  Public Variable Definitions */
extern afe_driver_data_tst afe_driver_data_gst;
extern afe_driver_cc_soc_tst afe_driver_cc_soc_gst;
extern afe_driver_diag_status_tun afe_driver_diag_status_un;

extern bq76952_config_tst bq76952_config_st;

/** @} */

/* Public Function Prototypes **/
extern U8 afe_driver_init_u8(void);
extern U8 afe_driver_configuration_u8(void);
extern U8 afe_driver_read_all_parameter_u8(void);
extern U8 afe_driver_read_current_u8(S16* curr_val_arps16);

//extern U8 afe_driver_start_adc_measurement_u8(afe_driver_adc_mode_te mode_arg_te);

extern U8 afe_driver_enter_sleep_mode_u8(void);
extern U8 afe_driver_fet_control_u8(afe_driver_fet_te afe_driver_fet_are,afe_driver_fet_state_te afe_driver_fet_state_are);

extern U8 afe_driver_balancing_fet_control_u8(U16 cb_bal_reg_aru16);
extern void afe_driver_wakeup_v(void);
extern U8 afe_driver_read_flt_wakeup_reg_u8(void);
extern U8 afe_driver_clear_flt_flags_u8(void);
extern U8 afe_driver_read_reg_u8(U8 mode_arg_au8);
extern U8 afe_driver_fault_check_u8(void);
extern U8 afe_driver_diagnostics_u8(void);

#endif /* CONFIGURATION_LAYER_IC_DRIVERS_AFE_DRIVER_H_ */
