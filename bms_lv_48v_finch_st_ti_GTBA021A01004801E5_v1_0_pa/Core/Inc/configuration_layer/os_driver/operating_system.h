/**
========================================================================================================================
@page operating_system_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	operating_system.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	09-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_OS_DRIVER_OPERATING_SYSTEM_H_
#define CONFIGURATION_LAYER_OS_DRIVER_OPERATING_SYSTEM_H_

/* Includes for definitions */
#include "common_header.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
/* @{
 * Macro definition */
typedef enum operating_system_priority_te_tag
{
  osPriorityIdle          = 0,          // priority: idle (lowest)
  osPriorityLow           = 1,          // priority: low
  osPriorityBelowNormal   = 2,          // priority: below normal
  osPriorityNormal        = 3,          // priority: normal (default)
  osPriorityAboveNormal   = 4,          // priority: above normal
  osPriorityHigh          = 5,          // priority: high
  osPriorityRealtime      = 6,          // priority: realtime (highest)
  osPriorityError         = 0x84        // system cannot determine priority or thread has illegal priority
} operating_system_priority_te;

typedef enum operating_system_queue_event_list_te_tag
{
	CAN_RX = 1,
	CAN_TIMER_100MS,
	CAN_TIMER_500MS,
	CAN_BMS_EVENT

}operating_system_queue_event_list_te;


typedef enum operating_system_bms_queue_event_list_te_tag
{
	BMS_START,
	BMS_MEASURE_VTG,
	BMS_MEASURE_CURRENT,
	BMS_FAULT_HIT,
	NUMBER_OF_BMS_AFE_TASK_EVENTS
}operating_system_bms_queue_event_list_te;

typedef enum operating_system_uart_comm_queue_list_te_tag
{
	OS_BMS_AFE_DATA,
}operating_system_uart_comm_queue_list_te;


typedef enum os_curr_soc_comm_qlist_te_tag
{
	OS_CURR_TIMER = 1,
}os_curr_soc_comm_qlist_te;

typedef enum operating_system_can_source_te_tag
{
	VCU_CAN,
	DEBUG_CAN,
	STORAGE_TASK,
}operating_system_can_source_te;

typedef enum operating_system_can_comm_queue_list_te_tag
{
	OS_CAN_RX = 1	,
	OS_CAN_TX		,
	OS_CAN_TX_50MS	,
	OS_CAN_TX_100MS	,
	OS_CAN_TX_1000MS,
	OS_CAN_DEBUG_TX	,
	OS_CAN_DEBUG_RX,
	OS_CAN_CLIENT_RX,
	OS_CAN_CLIENT_TX,
	OS_CAN_0_TX_FLT,
	OS_CAN_0_TX_AFTER_STORAGE_COMPLETE
}operating_system_can_comm_queue_list_te;

/* Storage queue event list */
typedef enum os_storage_qevent_list_te_tag
{
	OS_STORAGE_START_ADDR_INIT = 0		,
	OS_STORAGE_ONE_SEC_DATA				,
	OS_STORAGE_UPDATE_BATT_DATA			,
	OS_STORAGE_DATA_RETRIVAL			,
	OS_STORAGE_BOOTLOADER_ERASE			,
	OS_STORAGE_BOOTLOADER_UPDATE		,
	OS_STORAGE_CHIP_ERASE				,
	OS_STORAGE_DATA_BLOCK_ERASE			,
	OS_STORAGE_DATA_SECTOR_ERASE

}os_storage_qevent_list_te;



typedef enum os_storage_flash_mode_te_tag
{
	OS_FLASH_MODE_WR = 0		,
	OS_FLASH_MODE_RD			,
	OS_FLASH_RD_AND_SECTOR_ERASE,
	OS_FLASH_WRITE_READ_BACK	,
	OS_FLASH_MODE_STATUS_CHECK	,
	OS_FLASH_ERASE_SECTOR		,
	OS_FLASH_ERASE_SECTOR_CHECK ,
	OS_FLASH_ERASE_CHIP			,
	OS_FLASH_ERASE_BLOCK		,

}os_storage_flash_mode_te;

typedef struct os_storage_queue_tst_tag
{
	os_storage_qevent_list_te event_e;
	os_storage_flash_mode_te  mode_e;
	void *payload_pv;
}os_storage_queue_tst;

typedef struct operating_system_bms_afe_queue_tst_tag
{
	operating_system_bms_queue_event_list_te event_e;
	U8 source_u8 ;
}operating_system_bms_afe_queue_tst;

typedef struct operating_system_uart_comm_queue_tst_tag
{
	operating_system_uart_comm_queue_list_te comm_event_e;

}operating_system_uart_comm_queue_tst;


typedef struct os_curr_soc_queue_tst_tag
{
	os_curr_soc_comm_qlist_te source_e;
}os_curr_soc_queue_tst;

typedef struct operating_system_can_comm_queue_tst_tag
{
	operating_system_can_comm_queue_list_te comm_event_e;
	void* can_rx_msg_pv;
}operating_system_can_comm_queue_tst;

typedef enum bms_op_cmd_timeout_state_te_tag
{
  TIMEOUT_SET = 1,
  TIMEOUT_CLEAR
}bms_op_cmd_timeout_state_te;

/* CAN queue structure */


/* @} */

/* @{
 * Type definition */

/* @} */

/* @{
 *  Public Variable Definitions */
extern QueueHandle_t os_bms_afe_queue_handler_ge;
extern QueueHandle_t os_uart_comm_queue_handler_ge ;
extern QueueHandle_t os_curr_soc_queue_handler_ge ;
extern QueueHandle_t os_can_comm_queue_handler_ge ;
extern QueueHandle_t os_storage_t7_qhandler_ge;


extern SemaphoreHandle_t os_battparm_bmutex_ge ;
extern SemaphoreHandle_t OS_afe_spi_bsmpr_ge ;

extern operating_system_can_comm_queue_tst os_can_comm_queue_st;
/* @} */

/* Public Function Prototypes **/
extern U8 operating_system_init_u8(void);
extern void operating_system_start_scheduler_v(void);

extern os_storage_queue_tst  os_storage_queue_st;
#endif /* CONFIGURATION_LAYER_OS_DRIVER_OPERATING_SYSTEM_H_ */
