/**
========================================================================================================================
@page w25q_header_file

@subsection  Details

@n@b Title:
@n@b Filename:   	w25q.h
@n@b MCU:
@n@b Compiler:
@n@b Author:     	KRISH

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#
@n@b Date:       	22-Jul-2020
@n@b Description:  	Created

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef CONFIGURATION_LAYER_IC_MODULES_W25Q_H_
#define CONFIGURATION_LAYER_IC_MODULES_W25Q_H_

/* Includes for definitions */
#include "common_header.h"
#include "gpio_app.h"
#include "gt_gpio.h"

/* Macro Definitions */
#define W25Q_CHIP_SELECT_ENABLE		gpio_app_set_digital_output_v(MCU_CS_FM_PTB0, GPIO_DO_OFF) 		/* Active Low CS enable   */
#define W25Q_CHIP_SELECT_DISABLE	gpio_app_set_digital_output_v(MCU_CS_FM_PTB0, GPIO_DO_ON)  		/* Active High CS disable */
#define W25Q_WRITE_PROTECT_ENABLE	gpio_app_set_digital_output_v(MCU_WP_FM_PTA3, GPIO_DO_OFF) 		/* Active Low WP enable   */
//#define W25Q_WRITE_PROTECT_DISABLE	gpio_app_set_digital_output_v(MCU_WP_FM_PTA3, GPIO_DO_ON) 			/* Active High WP enable  */
#define W25Q_HW_RESET_ENABLE		gpio_app_set_digital_output_v(MCU_RESET_FM_PTA4, GPIO_DO_OFF)   	/* Active Low RST disable */
//#define W25Q_HW_RESET_DISABLE		gpio_app_set_digital_output_v(MCU_RESET_FM_PTA4, GPIO_DO_ON)  		/* Active High RST disable*/


#define W25Q_W25Q512_				0x401A
#define W25Q_W25Q256_				0x4019
#define W25Q_W25Q128_				0x4018
#define W25Q_W25Q64_				0x4017
#define W25Q_W25Q32_ 				0x4016
#define W25Q_W25Q16_  				0x4015
#define W25Q_W25Q80_  				0x4014
#define W25Q_W25Q40_  				0x4013
#define W25Q_W25Q20_				0x4012
#define W25Q_W25Q10_				0x4011


#define W25Q_MAX_SECTOR_ERASE_TIME_MS   250   // TODO: need to test this module

#define W25Q_MAX_BLOCK_ERASE_TIME_MS    500

#define W25Q_MAX_CHIP_ERASE_TIME_MS     1000

//############################################################################
// in Page,Sector and block read/write functions, can put 0 to read maximum bytes
//############################################################################

#define W25Q_SPI_FLASH_DEFAULT_VALUE          0xFFFFFFFF
#define W25Q_SPI_FLASH_PAGE_SIZE              256
#define W25Q_SPI_FLASH_MAX_ADDR               0xFFFFFF
#define W25Q_SPI_FLASH_SECTOR_SIZE            4096

/** @{
 * Type definition */
/** Flash initialization structure and enums **/


typedef enum w25qxx_id_te_tag
{
	W25Q_W25Q10 = 1,
	W25Q_W25Q20,
	W25Q_W25Q40,
	W25Q_W25Q80,
	W25Q_W25Q16,
	W25Q_W25Q32,
	W25Q_W25Q64,
	W25Q_W25Q128,
	W25Q_W25Q256,
	W25Q_W25Q512,
} w25qxx_id_te;

typedef enum w25q_erase_mode_te_tag
{
	W25Q_ERASE_MODE_FULL_CHIP = 1,
	W25Q_ERASE_MODE_BLOCK,
	W25Q_ERASE_MODE_SECTOR
}w25q_erase_mode_te;

typedef enum w25q_read_mode_te_tag
{
	W25Q_READ_MODE_NORMAL,
	W25Q_READ_MODE_FAST

}w25q_read_mode_te;

/** @} */
/** @{
 *  Public Variable Definitions */
extern U32 ext_flash_id_u32; /* global flash device id variable*/

/*Flash Commands for W25Q and Cypress */
static const U8 W25Q_FLASH_DUMMY_BYTE[1]  = {0xA5};
static const U8 W25Q_READ_ID[1]  		  = {0x9F};
static const U8 W25Q_READ_UNIQ_ID[1]  	  = {0x4B};
static const U8 W25Q_WRITE_ENABLE_CMD[1]  = {0x06};
static const U8 W25Q_WRITE_DISABLE_CMD[1] = {0x04};
static const U8 W25Q_CHIP_ERASE_CMD[1]    = {0xC7};
static const U8 W25Q_RDSR1_CMD[1]  		  = {0x05};
static const U8 W25Q_WRSR1_CMD[1]         = {0x01};
static const U8 W25Q_POWER_DOWN[1]  	  = {0xB9};

static const U8 W25Q_RDSR2_CMD[1] 		  = {0x35};
static const U8 W25Q_RDSR3_CMD[1]         = {0x15};
static const U8 W25Q_WRSR2_CMD[1]         = {0x31};
static const U8 W25Q_WRSR3_CMD[1]         = {0x11};
static const U8 W25Q_SECTOR_ERASE_CMD[1]  = {0x20};
static const U8 W25Q_BLOCK_ERASE_CMD[1]   = {0xD8};
static const U8 W25Q_READ_CMD[1] 		  = {0x03};
static const U8 W25Q_FAST_READ_CMD[1] 	  = {0x0B};
static const U8 W25Q_PAGE_PROGRAM_CMD[1]  = {0x02};


/** @} */

/* Public Function Prototypes **/
extern U8 w25q_read_id_and_blocks_u8(w25qxx_id_te *id_arpe, U32* block_size_arpu32);
extern U8 w25q_read_uniq_id_u8(U8* uniq_id_arpu8);
extern U8 w25q_read_status_registers_u8(U8 reg_number_aru8, U8* status_register_arpu8);
extern U8 w25q_write_enable_u8(void);
extern U8 w25q_write_disable_u8(void);
extern U8 w25q_erase_u8(w25q_erase_mode_te mode_are, U32 address_aru32, BOOL addr_4byte_en_arb);
extern U8 w25q_wait_for_write_end_u8(U16 wait_counter_ms_aru16);
extern U8 w25q_wait_for_wr_end_no_blocking_u8(U8 *status_reg_1_u8);
extern U8 w25q_write_data_u8(U8 *data_arpu8,U32 data_length_aru32);
extern U8 w25q_read_data_u8(U8 *write_data_arpu8,U32 write_data_length_aru32,U8 *read_data_arpu8,U32 read_data_length_aru32);
extern U8 w25q_app_write_data_u8(U8 *flash_write_data_arpu8,U32 flash_write_data_length_aru16);
extern U8 w25q_app_read_data_u8(U8 *write_data_aru8,U32 write_data_length_aru32,U8 *read_data_arpu8,U32 read_data_length_aru32);
extern U8 w25q_app_erase_u8(w25q_erase_mode_te mode_are, U32 address_aru32, BOOL addr_4byte_en_arb,U16 erase_wait_time_in_ms_aru16);
extern U8 w25q_power_down_u8(void);

#endif /* CONFIGURATION_LAYER_IC_MODULES_W25Q_H_ */
