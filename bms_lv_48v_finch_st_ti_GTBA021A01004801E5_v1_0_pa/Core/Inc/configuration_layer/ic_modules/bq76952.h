/*
 * bq76952.h
 *
 *  Created on: Nov 27, 2020
 *      Author: glads
 */

#ifndef INC_BQ76952_H_
#define INC_BQ76952_H_

#include "common_header.h"

/* Configurables start */
#define BQ76952_NO_OF_CELLLS_USED 16   // TODO: was 16
#define BQ76952_NO_OF_THERMS_USED 4

#define BQ76952_CURR_SNS_RESISTOR 0.2 /*TODO: Get correct resistor when hardware ready*/

/* Multifunction Thermistors*/
#define CFET_THERM			COM_HDR_ENABLED
#define DFET_THERM			COM_HDR_DISABLED
#define ALERT_THERM			COM_HDR_DISABLED
#define HDQ_THERM			COM_HDR_DISABLED
#define DCHG_THERM			COM_HDR_ENABLED
#define DDSG_THERM			COM_HDR_ENABLED

/* Configurables end */
#define DBG_BAUD            115200

#define BQ_I2C_ADDR   0x10 /* 7-bit Address 0x08 left-shifted to 1*/

#define BQ_I2C_ADDR_WRITE   0x10
#define BQ_I2C_ADDR_READ    0x11

#define BQ76952_TIMEOUT_MS 150
#define BQ76952_SUB_CMD_DLY_MS 15

// BQ76952 - Address Map
#define CMD_DIR_SUBCMD_LOW            0x3E
#define CMD_DIR_SUBCMD_HI             0x3F
#define CMD_DIR_RESP_LEN              0x61
#define CMD_DIR_RESP_START            0x40
#define CMD_DIR_RESP_CHKSUM           0x60

// BQ76952 - Voltage measurement commands
#define CMD_READ_VOLTAGE_CELL_1   0x14
#define CMD_READ_VOLTAGE_CELL_2   0x16
#define CMD_READ_VOLTAGE_CELL_3   0x18
#define CMD_READ_VOLTAGE_CELL_4   0x1A
#define CMD_READ_VOLTAGE_CELL_5   0x1C
#define CMD_READ_VOLTAGE_CELL_6   0x1E
#define CMD_READ_VOLTAGE_CELL_7   0x20
#define CMD_READ_VOLTAGE_CELL_8   0x22
#define CMD_READ_VOLTAGE_CELL_9   0x24
#define CMD_READ_VOLTAGE_CELL_10  0x26
#define CMD_READ_VOLTAGE_CELL_11  0x28
#define CMD_READ_VOLTAGE_CELL_12  0x2A
#define CMD_READ_VOLTAGE_CELL_13  0x2C
#define CMD_READ_VOLTAGE_CELL_14  0x2E
#define CMD_READ_VOLTAGE_CELL_15  0x30
#define CMD_READ_VOLTAGE_CELL_16  0x32
#define CMD_READ_VOLTAGE_STACK    0x34
#define CMD_READ_VOLTAGE_PACK     0x36

// BQ76952 - Direct Commands
#define CMD_DIR_CTRL_STAT          0x00

#define CMD_DIR_SA_PRT_VTG         0x02 /* SA -> Safety Alert*/
#define CMD_DIR_FS_PRT_VTG         0x03 /* FS -> Fault Status*/
#define CMD_DIR_SA_PRT_TEMP        0x04
#define CMD_DIR_FS_PRT_TEMP        0x05
#define CMD_DIR_SA_PRT_LAT         0x06
#define CMD_DIR_FS_PRT_LAT         0x07

#define CMD_DIR_FA_PFA        0x0A  /* PF -> Permanent Fail*/
#define CMD_DIR_FS_PFA        0x0B  /* FA -> Permnt. Failure Alert*/
#define CMD_DIR_FA_PFB        0x0C	 /* FS -> Permnt. Failure Status*/
#define CMD_DIR_FS_PFB        0x0D
#define CMD_DIR_FA_PFC        0x0E
#define CMD_DIR_FS_PFC        0x0F
#define CMD_DIR_FA_PFD        0x10
#define CMD_DIR_FS_PFD        0x11

#define CMD_DIR_BATT_STAT         0x12
#define CMD_DIR_VCELL_1           0x14
#define CMD_DIR_ALRM_STAT		  0x62
#define CMD_DIR_ALRM_RAW_STAT	  0x64
#define CMD_DIR_ALRM_EN			  0x66
#define CMD_DIR_INT_TEMP          0x68
#define CMD_DIR_CC2_CUR           0x3A
#define CMD_DIR_FET_STAT          0x7F


// BQ76952 - Sub/In-Direct Commands
#define SCMD_INDIR_DEV_ID	       	0x0001
#define SCMD_INDIR_FWVER_NO			0x0002
#define SCMD_INDIR_ENTER_DEEP		0x000F
#define SCMD_INDIR_EXIT_DEEP     	0x000E
#define SCMD_INDIR_ALLOW_SLP     	0x0099
#define SCMD_INDIR_EXIT_SLP     	0x009A
#define SCMD_INDIR_ENTER_SHUT     	0x0010

#define SCMD_INDIR_ALL_FETS_OFF    	0x0095
#define SCMD_INDIR_ALL_FETS_ON     	0x0096

#define SCMD_INDIR_RESET           0x0012
#define SCMD_INDIR_FET_EN		   0x0022
#define SCMD_INDIR_CHGFET_TEST	   0x001F
#define SCMD_INDIR_PCHGFET_TEST	   0x001E
#define SCMD_INDIR_DSGFET_TEST	   0x0020
#define SCMD_INDIR_PDSGFET_TEST	   0x001C

#define SCMD_INDIR_MANF_STS        0x0057
#define SCMD_INDIR_CB_ACT_STS      0x0083
#define SCMD_INDIR_ENT_CFG         0x0090
#define SCMD_INDIR_EXT_CFG         0x0092
#define SCMD_INDIR_FET_CTRL        0x0097

// Alert Bits in BQ76952 registers
#define BIT_SA_SC_DCHG            7
#define BIT_SA_OC2_DCHG           6
#define BIT_SA_OC1_DCHG           5
#define BIT_SA_OC_CHG             4
#define BIT_SA_CELL_OV            3
#define BIT_SA_CELL_UV            2

#define BIT_SB_OTF                7
#define BIT_SB_OTINT              6
#define BIT_SB_OTD                5
#define BIT_SB_OTC                4
#define BIT_SB_UTINT              2
#define BIT_SB_UTD                1
#define BIT_SB_UTC                0

// Inline functions
#define CELL_NO_TO_ADDR(cellNo) (0x14 + ((cellNo-1)*2))
#define LOW_BYTE(data) (U8)(data & 0x00FF)
#define HIGH_BYTE(data) (U8)((data >> 8) & 0x00FF)


typedef enum bq76952_thermistor_te_tag {
	TS1,
	TS2,
	TS3,
	HDQ,
	DCHG,
	DDSG,
	CFET_OFF,
	DFET_OFF,
	ALERT
}bq76952_thermistor_te;

typedef enum bq76952_cc_no_te_tag {
	CC1,
	CC2,
	CC3,

}bq76952_cc_no_te;

typedef enum bq76952_non_cvtg_types_te_tag {
	TOS_VTG,
	PK_VTG,
	LD_VTG

}bq76952_non_cvtg_types_te;

typedef enum bq76952_fet_te_tag {
	CHG,
	DSG,
	PCHG,
	PDSG,
	ALL
}bq76952_fet_te;

typedef enum bq76952_fet_state_te_tag {
	ON,
	OFF
}bq76952_fet_state_te;

typedef enum bq76952_scd_thresh_te_tag {
	SCD_10,
	SCD_20,
	SCD_40,
	SCD_60,
	SCD_80,
	SCD_100,
	SCD_125,
	SCD_150,
	SCD_175,
	SCD_200,
	SCD_250,
	SCD_300,
	SCD_350,
	SCD_400,
	SCD_450,
	SCD_500
}bq76952_scd_thresh_te;

typedef enum bq76952_configmode_te_tag {
	ENTER_CONFIG_MODE = 0x90,
	EXIT_CONFIG_MODE = 0x92
}bq76952_configmode_te;

typedef enum bq76952_protect_alert_te_tag {
	PROT_VTG_SA1, /*SA -> Safety Alert*/
	PROT_TEMP_SA2,
	PROT_LAT_SA3
}bq76952_protect_alert_te;

typedef enum bq76952_protect_status_te_tag {
	PROT_VTG_FS1, /*FS -> Fault Status*/
	PROT_TEMP_FS2,
	PROT_LAT_FS3
}bq76952_protect_status_te;

typedef enum bq76952_permanentfail_alert_te_tag {
	PF_ALERT_A,
	PF_ALERT_B,
	PF_ALERT_C,
	PF_ALERT_D
}bq76952_permanentfail_alert_te;

typedef enum bq76952_permanentfail_status_te_tag {
	PF_STATUS_A,
	PF_STATUS_B,
	PF_STATUS_C,
	PF_STATUS_D
}bq76952_permanentfail_status_te;

typedef enum bq76952_ldo_regno_te_tag {
	LDO_REG1,
	LDO_REG2
}bq76952_ldo_regno_te;

typedef enum bq76952_ldo_reg_state_te_tag {
	LDO_OFF,
	LDO_ON,
}bq76952_ldo_reg_state_te;

typedef enum bq76952_ldo_regout_te_tag {
	REGOUT_1V8 = 3,
	REGOUT_2V5 = 4,
	REGOUT_3V = 5,
	REGOUT_3V3 = 6,
	REGOUT_5V = 7
}bq76952_ldo_regout_te;

typedef enum bq76952_ldo_regin_src_te_tag {
	REGIN_EXT_SRC,
	REGIN_PREREG,
}bq76952_ldo_regin_src_te;

typedef enum bq76952_sleep_mode_te_tag {
	SLEEP_NORMAL,
	SLEEP_DEEP,
	SLEEP_SHUT
}bq76952_sleep_mode_te;

typedef enum bq76952_ic_sts_te_tag {
	BATT_STS,
	CTRL_STS,
	FET_STS,
	ALRM_STS
}bq76952_ic_sts_te;

/*Registers - Direct Commands*/
/*Control Status Register #1 0x00*/
typedef union bq76952_control_stat_tu_tag {
	struct __attribute__((packed)){

		U8 ld_stat_b1		:1; /* Load detect on prev vtg measurement status*/
		U8 ld_timeout_b1	:1; /* Load detect timeout */

		U8 deep_sleep_b1	:1;

		U8 resrvd_b5		:5; /*Reserved*/

	} bits;
	U8 byte;
} bq76952_control_stat_tu;

/*Protection1 Voltage Alert Register #2 0x02*/
typedef union bq76952_prot1_vtg_alert_tu_tag {
	struct __attribute__((packed)){
		U8 resrvd_b2			:2; /*Reserved*/
		U8 cell_uv_b1           :1;
		U8 cell_ov_b1           :1;
		U8 oc_chg_b1            :1;
		U8 oc_dsg1_b1           :1;
		U8 oc_dsg2_b1           :1;
		U8 scd_b1	            :1;
	} bits;
	U8 byte;
} bq76952_prot1_vtg_alert_tu;

/*Protection1 Voltage Status Register #3 0x03*/
typedef union bq76952_prot1_vtg_stat_tu_tag {
	struct __attribute__((packed)){
		U8 resrvd_b2			:2; /*Reserved*/
		U8 cell_uv_b1           :1;
		U8 cell_ov_b1           :1;
		U8 oc_chg_b1            :1;
		U8 oc_dsg1_b1           :1;
		U8 oc_dsg2_b1           :1;
		U8 scd_b1	            :1;
	} bits;
	U8 byte;
} bq76952_prot1_vtg_stat_tu;

/*Protection2 Temperature Alert Register #4 0x04*/
typedef union bq76952_prot2_temp_alert_tu_tag {
	struct __attribute__((packed)){

		U8 chg_ut_b1		:1;
		U8 dsg_ut_b1		:1;

		U8 int_ut_b1		:1;

		U8 resrvd_b1		:1; /*Reserved*/
		U8 chg_ot_b1		:1;
		U8 dsg_ot_b1		:1;

		U8 int_ot_b1		:1;
		U8 fet_ot_b1		:1;
	} bits;
	U8 byte;
} bq76952_prot2_temp_alert_tu;

/*Protection2 Temperature Status Register #5 0x05*/
typedef union bq76952_prot2_temp_stat_tu_tag {
	struct __attribute__((packed)){

		U8 chg_ut_b1		:1;
		U8 dsg_ut_b1		:1;

		U8 int_ut_b1		:1;

		U8 resrvd_b1		:1; /*Reserved*/
		U8 chg_ot_b1		:1;
		U8 dsg_ot_b1		:1;

		U8 int_ot_b1		:1;
		U8 fet_ot_b1		:1;
	} bits;
	U8 byte;
} bq76952_prot2_temp_stat_tu;

/*Protection3 Latch Alert Register #6 0x06*/
typedef union bq76952_prot3_latch_alert_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd_b3			:3;/*Reserved*/
		U8 pchg_tout_suspd_b1	:1;

		U8 cell_ov_lat_b1		:1;

		U8 oc_dsg_lat_b1        :1;
		U8 sc_dsg_lat_b1		:1;
		U8 oc_dsg3_b1           :1;
	} bits;
	U8 byte;
} bq76952_prot3_latch_alert_tu;

/*Protection3 Latch Status Register #7 0x07*/
typedef union bq76952_prot3_latch_stat_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b1			:1; /* Reserved 1*/
		U8 host_wdg_b1			:1; /* Comm not happening fault -only if feature enabled*/
		U8 pchg_tout_b1			:1;

		U8 resrvd2_b1			:1; /* Reserved 2*/
		U8 cell_ov_lat_b1		:1;

		U8 oc_dsg_lat_b1        :1;
		U8 sc_dsg_lat_b1		:1;
		U8 oc_dsg3_b1           :1;
	} bits;
	U8 byte;
} bq76952_prot3_latch_stat_tu;

/*Permanent Fail1 Alert Register #8 0x0A*/
typedef union bq76952_pf1_alert_tu_tag {
	struct __attribute__((packed)){

		U8 cell_uv_b1			:1;
		U8 cell_ov_b1			:1;

		U8 oc_chg_b1			:1;
		U8 oc_dsg_b1			:1;

		U8 cell_ot_b1			:1;
		U8 resrvd_b1       		:1;		/*Reserved*/

		U8 fet_ot_b1			:1;
		U8 cu_dep_b1           	:1;
	} bits;
	U8 byte;
} bq76952_pf1_alert_tu;

/*Permanent Fail1 Status Register #9 0x0B*/
typedef union bq76952_pf1_stat_tu_tag {
	struct __attribute__((packed)){

		U8 cell_uv_b1		:1;
		U8 cell_ov_b1		:1;

		U8 oc_chg_b1		:1;
		U8 oc_dsg_b1		:1;

		U8 cell_ot_b1		:1;
		U8 resrvd_b1       	:1;		/*Reserved*/

		U8 fet_ot_b1		:1;
		U8 cu_dep_b1        :1;
	} bits;
	U8 byte;
} bq76952_pf1_stat_tu;

/*Permanent Fail2 Alert Register #10 0x0C*/
typedef union bq76952_pf2_alert_tu_tag {
	struct __attribute__((packed)){

		U8 chg_fet_b1			:1;
		U8 dsg_fet_b1			:1;

		U8 slvl_prot_b1			:1;

		U8 vtg_imb_rest_b1		:1;
		U8 vtg_imb_act_b1		:1;

		U8 resrvd_b2       		:2;		/*Reserved*/

		U8 sc_dsg_lat_b1		:1;
	} bits;
	U8 byte;
} bq76952_pf2_alert_tu;

/*Permanent Fail2 Status Register #11 0x0D*/
typedef union bq76952_pf2_stat_tu_tag {
	struct __attribute__((packed)){

		U8 chg_fet_b1			:1;
		U8 dsg_fet_b1			:1;

		U8 slvl_prot_b1			:1;

		U8 vtg_imb_rest_b1		:1;
		U8 vtg_imb_act_b1		:1;

		U8 resrvd_b2       		:2;		/*Reserved*/

		U8 sc_dsg_lat_b1		:1;
	} bits;
	U8 byte;
} bq76952_pf2_stat_tu;

/*Permanent Fail3 Alert Register #12 0x0E*/
typedef union bq76952_pf3_alert_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b3       		:3;		/*Reserved*/

		U8 int_lfo_b1			:1;
		U8 int_vtg_ref_b1		:1;
		U8 int_vss_meas_b1		:1;

		U8 hw_mux_b1			:1;

		U8 resrvd2_b1			:1;
	} bits;
	U8 byte;
} bq76952_pf3_alert_tu;

/*Permanent Fail3 Status Register #13 0x0F*/
typedef union bq76952_pf3_stat_tu_tag {
	struct __attribute__((packed)){

		U8 otp_b1				:1;
		U8 data_rom_b1			:1;
		U8 instrt_rom_b1		:1;

		U8 int_lfo_b1			:1;
		U8 int_vtg_ref_b1		:1;
		U8 int_vss_meas_b1		:1;

		U8 hw_mux_b1			:1;

		U8 cmded_b1				:1;
	} bits;
	U8 byte;
} bq76952_pf3_stat_tu;

/*Permanent Fail4 Alert Register #14 0x10*/
typedef union bq76952_pf4_alert_tu_tag {
	struct __attribute__((packed)){

		U8 tp_stk_cell_sum_b1 	:1;	/*Top of stack vs cell sum fail alert - only if feature enabled*/
		U8 resrvd_b7       		:7;	/*Reserved*/

	} bits;
	U8 byte;
} bq76952_pf4_alert_tu;

/*Permanent Fail4 Status Register #15 0x11*/
typedef union bq76952_pf4_stat_tu_tag {
	struct __attribute__((packed)){

		U8 tp_stk_cell_sum_b1 	:1;	/*Top of stack vs cell sum fail alert - only if feature enabled*/
		U8 resrvd_b7       		:7;	/*Reserved*/

	} bits;
	U8 byte;
} bq76952_pf4_stat_tu;

/*Battery Status Register #16 0x12*/
typedef union bq76952_batt_stat_tu_tag {
	struct __attribute__((packed)){

		U8 cfg_update_b1		:1;
		U8 pchg_mode_b1			:1;

		U8 sleep_en_b1			:1;
		U8 power_on_rst_b1		:1;

		U8 wdg_rst_b1			:1;
		U8 cow_chk_state_b1		:1; /* Status : cell open wire check in process or not*/

		U8 otp_wr_pend_b1		:1; /* Otp write is pending*/
		U8 otp_wr_blk_b1		:1; /* Otp write is blocked*/

		/* Security State of IC - 2bits
		 * 	0 = Device has not initialized yet.
		 * 	1 = Device is in FULLACCESS mode
		 * 	2 = Device is in UNSEALED mode.
		 * 	3 = Device is in SEALED mode.
		 */
		U8 security_state_b2	:2;
		U8 fuse_state_b1		:1;

		/* Sets when any one of the enabled safety fault occurs*/
		U8 safety_fault_b1		:1;

		/* Sets when any one of the enabled permanent fail fault occurs*/
		U8 permnt_fail_fault_b1		:1;

		U8 shut_dwn_cmd_b1			:1; /*shutdown cmd active*/

		U8 resrvd_b1				:1;
		U8 sleep_stat_b1			:1;

	} bits;
	U16 byte_u16;
} bq76952_batt_stat_tu;


/*Alarm Registers #17 0x62(Latching Status), 0x64(Non-Latchinh Status), 0x66(Enable/Disable)*/
typedef union bq76952_alarm_tu_tag {
	struct __attribute__((packed)){

		U8 wake_up_b1		: 1;
		U8 vtg_adc_scan_b1	: 1;
		U8 cb_act_b1		: 1;
		U8 fuse_b1			: 1;

		U8 xdsg_off_b1		: 1;
		U8 xchg_off_b1		: 1;
		U8 shut_vtg_b1		: 1;
		U8 full_scan_b1		: 1;

		U8 resrvd_b1		: 1;
		U8 init_meas_b1		: 1;
		U8 init_pwr_b1		: 1;
		U8 msk_pf_alrt_b1	: 1;

		U8 msk_sf_alrt_b1	: 1;
		U8 pf_flt_b1		: 1;
		U8 safe_stat_a_b1	: 1;
		U8 safe_stat_bc_b1	: 1;

	} bits;
	U16 byte_u16;
} bq76952_alarm_tu;

/*FET Status Register #18 0x7F*/
typedef union bq76952_fet_stat_tu_tag {
	struct __attribute__((packed)){

		U8 chg_fet_b1		: 1;
		U8 pchg_fet_b1		: 1;
		U8 dsg_fet_b1		: 1;
		U8 pdsg_fet_b1		: 1;

		U8 dchg_pin_b1		: 1;
		U8 ddsg_pin_b1		: 1;
		U8 alert_pin_b1		: 1;

		U8 resrvd_b1		: 1;

	} bits;
	U8 byte;
} bq76952_fet_stat_tu;


/*Sub/InDirect-Cmd  Registers*/
/*FET Control Register #1 0x0097*/
typedef union bq76952_fet_ctrl_tu_tag {
	struct __attribute__((packed)){

		U8 dsg_off_b1			:1;
		U8 pdsg_off_b1			:1;
		U8 chg_off_b1			:1;
		U8 pchg_off_b1			:1;

		U8 resrvd_b4		:4;
	} bits;
	U8 byte;
} bq76952_fet_ctrl_tu;


/*Manufacturing status Register #2 0x0057*/
typedef union bq76952_manuf_stat_tu_tag {
	struct __attribute__((packed)){

		U8 pchg_test_b1			:1;
		U8 chg_test_b1			:1;

		U8 dsg_test_b1			:1;
		U8 resrvd1_b1			:1;

		U8 fet_en_b1			:1;

		U8 pdsg_test_b1			:1;
		U8 pf_en_b1				:1;

		U8 otpw_en_b1			:1;

		U8 resrvd2_b8			:8;

	} bits;
	U16 byte_u16;
} bq76952_manuf_stat_tu;

/*Data Memory Registers*/
/*ENABLED Protection1 Status Register #1 0x9261*/
typedef union bq76952_enabled_prot1_stat_tu_tag {
	struct __attribute__((packed)){
		U8 resrvd_b2			:2; /*Reserved*/
		U8 cell_uv_b1           :1;
		U8 cell_ov_b1           :1;
		U8 oc_chg_b1            :1;
		U8 oc_dsg1_b1           :1;
		U8 oc_dsg2_b1           :1;
		U8 scd_b1	            :1;
	} bits;
	U8 byte;
} bq76952_enabled_prot1_stat_tu;


/*ENABLED Protection2 Status Register #2 0x9262*/
typedef union bq76952_enabled_prot2_stat_tu_tag {
	struct __attribute__((packed)){

		U8 chg_ut_b1		:1;
		U8 dsg_ut_b1		:1;

		U8 int_ut_b1		:1;

		U8 resrvd_b1		:1; /*Reserved*/
		U8 chg_ot_b1		:1;
		U8 dsg_ot_b1		:1;

		U8 int_ot_b1		:1;
		U8 fet_ot_b1		:1;
	} bits;
	U8 byte;
} bq76952_enabled_prot2_stat_tu;


/*ENABLED Protection3 Status Register #3 0x9263*/
typedef union bq76952_enabled_prot3_stat_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b1			:1; /* Reserved 1*/
		U8 host_wdg_b1			:1; /* Comm not happening fault -only if feature enabled*/
		U8 pchg_tout_b1			:1;

		U8 resrvd2_b1			:1; /* Reserved 2*/
		U8 cell_ov_lat_b1		:1;

		U8 oc_dsg_lat_b1        :1;
		U8 sc_dsg_lat_b1		:1;
		U8 oc_dsg3_b1           :1;
	} bits;
	U8 byte;
} bq76952_enabled_prot3_stat_tu;


/*CHG-FET Protection1 Status Register #4 0x9265*/
typedef union bq76952_chgfet_prot1_stat_tu_tag {
	struct __attribute__((packed)){
		U8 resrvd1_b3			:3; /*Reserved*/
		U8 cell_ov_b1           :1;
		U8 oc_chg_b1            :1;
		U8 resrvd2_b2			:2; /*Reserved*/
		U8 scd_b1	            :1;
	} bits;
	U8 byte;
} bq76952_chgfet_prot1_stat_tu;


/*CHG-FET Protection2 Status Register #5 0x9266*/
typedef union bq76952_chgfet_prot2_stat_tu_tag {
	struct __attribute__((packed)){

		U8 chg_ut_b1		:1;
		U8 resrvd1_b1		:1; /*Reserved*/

		U8 int_ut_b1		:1;

		U8 resrvd2_b1		:1; /*Reserved*/
		U8 chg_ot_b1		:1;
		U8 resrvd3_b1		:1; /*Reserved*/

		U8 int_ot_b1		:1;
		U8 fet_ot_b1		:1;
	} bits;
	U8 byte;
} bq76952_chgfet_prot2_stat_tu;


/*CHG-FET Protection3 Status Register #6 0x9267*/
typedef union bq76952_chgfet_prot3_stat_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b1			:1; /* Reserved 1*/
		U8 host_wdg_b1			:1; /* Comm not happening fault -only if feature enabled*/
		U8 pchg_tout_b1			:1;

		U8 resrvd2_b1			:1; /* Reserved 2*/
		U8 cell_ov_lat_b1		:1;

		U8 resrvd3_b1			:1; /*Reserved 3*/
		U8 sc_dsg_lat_b1		:1;
		U8 resrvd4_b1			:1; /*Reserved 4*/
	} bits;
	U8 byte;
} bq76952_chgfet_prot3_stat_tu;



/*DSG-FET Protection1 Status Register #7 0x9269*/
typedef union bq76952_dsgfet_prot1_stat_tu_tag {
	struct __attribute__((packed)){
		U8 resrvd1_b2			:2; /*Reserved*/
		U8 cell_uv_b1           :1;
		U8 resrvd2_b2			:2; /*Reserved*/
		U8 oc_dsg1_b1           :1;
		U8 oc_dsg2_b1           :1;
		U8 scd_b1	            :1;
	} bits;
	U8 byte;
} bq76952_dsgfet_prot1_stat_tu;


/*DSG-FET Protection2 Status Register #8 0x926A*/
typedef union bq76952_dsgfet_prot2_stat_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b1		:1; /*Reserved*/
		U8 dsg_ut_b1		:1;

		U8 int_ut_b1		:1;

		U8 resrvd2_b2		:2; /*Reserved*/
		U8 dsg_ot_b1		:1;

		U8 int_ot_b1		:1;
		U8 fet_ot_b1		:1;
	} bits;
	U8 byte;
} bq76952_dsgfet_prot2_stat_tu;


/*DSG-FET Protection3 Status Register #9 0x926B*/
typedef union bq76952_dsgfet_prot3_stat_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b1			:1; /* Reserved 1*/
		U8 host_wdg_b1			:1; /* Comm not happening fault -only if feature enabled*/

		U8 resrvd2_b3			:3; /* Reserved 2*/

		U8 oc_dsg_lat_b1        :1;
		U8 sc_dsg_lat_b1		:1;
		U8 oc_dsg3_b1           :1;
	} bits;
	U8 byte;
} bq76952_dsgfet_prot3_stat_tu;

/*LDO Config Register #10 0x9236*/
typedef union bq76952_ldo_config_tu_tag {
	struct __attribute__((packed)){

		U8 reg1_en_b1		:1;
		U8 reg1_vtg_b3		:3;

		U8 reg2_en_b1		:1;
		U8 reg2_vtg_b3		:3;
	} bits;
	U8 byte;
} bq76952_ldo_config_tu;

/*Pre-Reg Config Register #11 0x9237*/
typedef union bq76952_reg0_config_tu_tag {
	struct __attribute__((packed)){

		U8 reg0_en_b1		:1;

		U8 resrvd_b7		:7; /* Reserved*/
	} bits;
	U8 byte;
} bq76952_reg0_config_tu;

/*FET options Register #12 0x9308*/
typedef union bq76952_fet_options_tu_tag {
	struct __attribute__((packed)){
		U8 series_fet_b		: 1;

		U8 sleep_chg_b		: 1;
		U8 host_fet_en_b	: 1;

		U8 fet_ctrl_en_b	: 1;
		U8 pdsg_en_b		: 1;

		U8 fet_init_off_b	: 1;
		U8 resrvd_b2		: 2; /* Reserved*/
	} bits;
	U8 byte;
} bq76952_fet_options_tu;

/*Power Configuration Register #13 0x9234*/
typedef union bq76952_pwr_conf_tu_tag {
	struct __attribute__((packed)){

		U8 resrvd1_b2		: 2; /* Reserved*/

		U8 loop_slow_b2		: 2;
		U8 cb_loop_slow_b2	: 2;

		U8 fast_adc_b1		: 1;
		U8 ot_shutdwn_b1	: 1;
		U8 sleep_fn_b1		: 1;

		U8 lfo_dp_slp_b1	: 1;
		U8 ldo_dp_slp_b1	: 1;
		U8 ld_dp_slp_b1		: 1;

		U8 shut_ts2_b1		: 1;
		U8 ot_dp_slp_b1		: 1;

		U8 resrvd2_b2		: 2; /* Reserved*/
	} bits;
	U16 byte_u16;
} bq76952_pwr_conf_tu;

/*Pin Configuration Register #14 0x92FC(Alert Pin)*/
typedef union bq76952_pin_config_tu_tag {
	struct __attribute__((packed)){

		U8 pin_fn_b2	: 2;

		U8 opt0_b1		: 1;
		U8 opt1_b1		: 1;
		U8 opt2_b1		: 1;
		U8 opt3_b1		: 1;
		U8 opt4_b1		: 1;
		U8 opt5_b1		: 1;

	} bits;
	U8 byte;
} bq76952_pin_config_tu;

/*Balancing Configuration Register #15 0x9335*/
typedef union bq76952_cbal_config_tu_tag {
	struct __attribute__((packed)){

		U8 cb_chg_b1	: 1;
		U8 cb_rlx_b1	: 1;
		U8 cb_slp_b1	: 1;
		U8 cb_no_slp_b1	: 1;
		U8 cb_no_cmd_b1	: 1;
		U8 resrvd_b3	: 3;

	} bits;
	U8 byte;
} bq76952_cbal_config_tu;



/* Raw value storing Structure*/
/*All voltages and temperatures*/
typedef struct __attribute__((packed)) bq76952_all_values_tst_tag {

	U16 cell_voltage_au16[BQ76952_NO_OF_CELLLS_USED];
	U16 tos_vtg_u16;
	U16 pack_pin_vtg_u16;
	U16 ld_pin_vtg_u16;

	S16 int_therm_1cc_s16;

#if CFET_THERM
	S16 cfet_off_temp_1cc_s16;
#endif

#if DFET_THERM
	S16 dfet_off_temp_1cc_s16;
#endif

#if ALERT_THERM
	S16 alert_temp_1cc_s16;
#endif
	S16 ts1_val_1cc_s16;
	S16 ts2_val_1cc_s16;
	S16 ts3_val_1cc_s16;

#if HDQ_THERM
	S16 hdq_temp_1cc_s16;
#endif

#if DCHG_THERM
	S16 dchg_temp_1cc_s16;
#endif

#if DDSG_THERM
	S16 ddsg_temp_1cc_s16;
#endif

}bq76952_all_values_tst;

/*All recoverable protection's alerts and status*/
typedef struct __attribute__((packed)) bq76952_all_prot_info_tst_tag {

	bq76952_prot1_vtg_alert_tu bq76952_prot1_alert_u;
	bq76952_prot1_vtg_alert_tu bq76952_prot1_stat_u;
	bq76952_prot2_temp_alert_tu bq76952_prot2_alert_u;
	bq76952_prot2_temp_alert_tu bq76952_prot2_stat_u;
	bq76952_prot3_latch_alert_tu bq76952_prot3_alert_u;
	bq76952_prot3_latch_alert_tu bq76952_prot3_stat_u;

}bq76952_all_prot_info_tst;

/*All permanent fail's alerts and status*/
typedef struct __attribute__((packed)) bq76952_all_pf_info_tst_tag {

	bq76952_pf1_alert_tu bq76952_pf1_alert_u;
	bq76952_pf1_stat_tu bq76952_pf1_stat_u;
	bq76952_pf2_alert_tu bq76952_pf2_alert_u;
	bq76952_pf2_stat_tu bq76952_pf2_stat_u;
	bq76952_pf3_alert_tu bq76952_pf3_alert_u;
	bq76952_pf3_stat_tu bq76952_pf3_stat_u;
	bq76952_pf4_alert_tu bq76952_pf4_alert_u;
	bq76952_pf4_stat_tu bq76952_pf4_stat_u;

}bq76952_all_pf_info_tst;

typedef struct bq76952_prot_vtg_params_tst_tag {

	U16 thres_1mv_u16;
	U16 delay_1ms_u16;
	U16 recv_1mv_u16;

}bq76952_prot_vtg_params_tst;

typedef struct bq76952_prot_temp_params_tst_tag {

	S8 thres_1c_s8;
	U8 delay_1s_u8;
	S8 recv_1c_s8;

}bq76952_prot_temp_params_tst;

typedef struct bq76952_prot_vtg_config_tst_tag {

	bq76952_prot_vtg_params_tst cell_ov_st;
	bq76952_prot_vtg_params_tst cell_uv_st;

}bq76952_prot_vtg_config_tst;

typedef struct bq76952_prot_temp_config_tst_tag {

	bq76952_prot_temp_params_tst cell_ot_chg_st;
	bq76952_prot_temp_params_tst cell_ut_chg_st;

	bq76952_prot_temp_params_tst cell_ot_dsg_st;
	bq76952_prot_temp_params_tst cell_ut_dsg_st;

	bq76952_prot_temp_params_tst cell_ot_int_st;
	bq76952_prot_temp_params_tst cell_ut_int_st;

	bq76952_prot_temp_params_tst cell_ot_fet_st;

}bq76952_prot_temp_config_tst;

typedef struct bq76952_cb_config_tst_tag {

	U8 auto_en_u8	;
	U8 max_cells_u8	;
	U8 duration_intvl_u8;	/* CB duration for each Host cmd sent*/

	//Manual/ Autonomous Cell Balancing - Available AFE-IC Control
	// Temperature
	S8 min_cell_temp_s8; //Min Cell Temp – below which CB stops
	S8 max_cell_temp_s8; //Max Cell Temp – above which CB stops
	S8 max_int_temp_s8; //Max Internal AFE Die Temp – above which CB stops

	// Autonomous Cell Balancing
	U8 sleep_en_u8;

	// Voltage
	U16 min_cell_vtg_u16; //Min Cell Voltage Threshold – above which CB occurs
	U8 start_del_val_u8; //Delta Value (Max- Min) – above which CB occurs for all cells
	U8 stop_del_val_u8; //Stop Delta Value (Individual Cell voltage – Min) – below which CB stops for that Individual Cell alone.

	// Current: (Based on CC1 Current)
	//Charging Alone
	S16 start_chg_curr_thres_s16;//CHG Current Threshold – above which CB occurs.
	//Relax State alone - Set all the below configurations
	S16 chg_curr_thres_s16;		//CHG Current Threshold – below which CB occurs /* 1mA but limit space 32767 */
	S16 dsg_curr_thres_s16; 	 //DSG Current Threshold – above which CB occurs /* 1mA but limit space 32767 */

	U8 meas_time_intvl_u8;	//Periodically at this given time interval, all above configured values are evaluated with the measured values and decides whether to proceed with CB or not.

}bq76952_cb_config_tst;

typedef struct bq76952_ldo_configure_tst_tag {

	bq76952_ldo_reg_state_te reg1_state_e;
	bq76952_ldo_regout_te reg1_vtg_e;

	bq76952_ldo_reg_state_te reg2_state_e;
	bq76952_ldo_regout_te reg2_vtg_e;

	bq76952_ldo_regin_src_te regin_src_e;

}bq76952_ldo_configure_tst;

typedef struct bq76952_fet_configure_tst_tag {

	U16 pchg_start_vtg_1mv_u16;
	U16 pchg_stop_vtg_1mv_u16;

	U16 pdsg_timeout_1ms_u16;
	U16 pdsg_delta_1mv_u16; /*500mV -> LDPin ~ TOS*/

	U16 body_diode_thres_1ma_u16;

	BOOL fet_init_state_b;
	BOOL fet_pdsg_en_b;
	BOOL fet_series_en_b;

	BOOL fet_test_mode_b;

}bq76952_fet_configure_tst;

typedef struct bq76952_sleep_configure_tst_tag {

	BOOL dpslp_ldo_en_b;
	BOOL dpslp_lfo_en_b;

	S16 slp_strt_curr_1ma_s16;
	S16 slp_wk_comp_curr_1ma_s16;
	U8  slp_meas_time_1s_u8;
	/* Below two are combined conditions*/
	S16 schgr_thres_1cv_s16; /* TOS value - Below which sleep is blocked in charging*/
	S16 schgr_pk_tos_del_1cv_s16; /* (Pack - TOS) Delta - above which sleep is blocked in charging*/  /*schgr -> Sleep Charger*/
}bq76952_sleep_configure_tst;

typedef struct bq76952_alarm_configure_tst_tag {

	BOOL wakeup_alert_b;
	BOOL full_scan_alert_b;
}bq76952_alarm_configure_tst;

typedef struct bq76952_config_tst_tag {

	BOOL auto_fet_b;
	BOOL auto_cb_b;
	bq76952_prot_vtg_config_tst 	prot_vtg_config_st;
	bq76952_prot_temp_config_tst 	prot_temp_config_st;
	bq76952_fet_configure_tst 		fet_configure_st;
	bq76952_sleep_configure_tst		sleep_configure_st;
	bq76952_ldo_configure_tst		ldo_configure_st;
	bq76952_alarm_configure_tst		alarm_configure_st;
	bq76952_cb_config_tst			cb_config_st;

}bq76952_config_tst;


extern bq76952_chgfet_prot1_stat_tu bq76952_chgfet_prot1_stat_u;
extern bq76952_chgfet_prot2_stat_tu bq76952_chgfet_prot2_stat_u;
extern bq76952_chgfet_prot3_stat_tu bq76952_chgfet_prot3_stat_u;

extern bq76952_dsgfet_prot1_stat_tu bq76952_dsgfet_prot1_stat_u;
extern bq76952_dsgfet_prot2_stat_tu bq76952_dsgfet_prot2_stat_u;
extern bq76952_dsgfet_prot3_stat_tu bq76952_dsgfet_prot3_stat_u;

extern bq76952_control_stat_tu	bq76952_control_stat_u;
extern bq76952_fet_options_tu 	bq76952_fet_options_u;
extern bq76952_fet_ctrl_tu	 	bq76952_fet_ctrl_u;
extern bq76952_pwr_conf_tu 		bq76952_pwr_conf_u;

extern bq76952_manuf_stat_tu		bq76952_manuf_stat_u;

extern bq76952_batt_stat_tu bq76952_batt_stat_u;
extern bq76952_fet_stat_tu	bq76952_fet_stat_u;

extern bq76952_alarm_tu		bq76952_alm_stat_u;			/* Alert Pin Triggers*/
extern bq76952_alarm_tu		bq76952_alm_raw_stat_u;		/* No Alert Pin Triggers*/
extern bq76952_alarm_tu		bq76952_alm_en_u;			/* Turning it on - Makes Alert bit assert*/

extern bq76952_pin_config_tu bq76952_alert_pin_cfg_u;

extern U16 bq76952_init_u16();
extern U16 bq76952_get_all_cell_vtg_u16();
extern U16 bq76952_get_all_cell_temp_u16();

extern U16 bq76952_get_current_u16(bq76952_cc_no_te cc_no_are, S16* rd_curr_arps16);
extern U16 bq76952_get_vtg_u16(bq76952_non_cvtg_types_te non_cvtg_types_are, U16* rd_vtg_arpu16);

extern U8 bq76952_get_cell_vtg_u8(U8 cell_no_aru8, U16* cell_vtg_arpu16);
extern U8 bq76952_get_thermistor_temp_u8(bq76952_thermistor_te thermistor_are, SFP* temp_val_arpsfp );
extern U8 bq76952_get_internal_temp_u8(SFP* int_temp_val_arpsfp);

extern U16 bq76952_configure_ldo_u16(bq76952_ldo_configure_tst* bq76952_ldo_configure_arpst);
extern U16 bq76952_configure_fet_u16(bq76952_fet_configure_tst* bq76952_fet_configure_arpst);

extern U16 bq76952_set_cell_over_vtg_prot_u16(U16 ov_thres_mv_aru16, U16 delay_ms_aru16, U16 ov_recv_mv_aru16);
extern U16 bq76952_set_cell_under_vtg_prot_u16(U16 uv_thres_mv_aru16, U16 delay_ms_aru16, U16 uv_recv_mv_aru16);

extern U16 bq76952_set_cell_over_temp_chg_prot_u16(S8 ot_chg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_chg_recv_1c_ars8);
extern U16 bq76952_set_cell_over_temp_dsg_prot_u16(S8 ot_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_dsg_recv_1c_ars8);
extern U16 bq76952_set_cell_under_temp_chg_prot_u16(S8 ut_chg_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_chg_recv_1c_ars8);
extern U16 bq76952_set_cell_under_temp_dsg_prot_u16(S8 ut_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_dsg_recv_1c_ars8);

extern U16 bq76952_set_int_over_temp_prot_u16(S8 ot_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_dsg_recv_1c_ars8);
extern U16 bq76952_set_int_under_temp_prot_u16(S8 ot_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_dsg_recv_1c_ars8);
extern U16 bq76952_set_fet_over_temp_prot_u16(S8 ot_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_dsg_recv_1c_ars8);

extern U16 bq76952_set_chg_ocp_u16(U16 chg_ocp_ma_aru16, U16 delay_ms_aru16,S16 recv_thres_ma_ars16, S16 pk_tos_del_cv_ars16);
extern U16 bq76952_set_dchg_ocp_u16(U16 dchg_ocp_mv_aru16, U16 delay_ms_aru16);

extern U16 bq76952_set_dischg_scp_u16(bq76952_scd_thresh_te thresh_are, U16 delay_us_aru16, U8 recv_time_1s_aru8 );

extern U16 bq76952_get_protection_alert_u16(bq76952_protect_alert_te bq76952_protect_alert_are, void* bq76952_protect_alert_arpv);
extern U16 bq76952_get_protection_status_u16(bq76952_protect_status_te bq76952_protect_status_are, void* bq76952_protect_status_arpv);

extern U16 bq76952_get_permnt_fail_alert_u16(bq76952_permanentfail_alert_te bq76952_pf_alert_are, void* bq76952_pf_alert_arpv);
extern U16 bq76952_get_permnt_fail_status_u16(bq76952_permanentfail_status_te bq76952_pf_status_are, void* bq76952_pf_status_arpv);

extern U16 bq76952_get_all_prot_info_u16();
extern U16 bq76952_get_all_pf_info_u16();

extern U16 bq76952_get_temperature_status_u16(bq76952_prot2_temp_stat_tu* bq76952_temperature_prot_arpu);

extern U16 bq76952_get_ic_stat_u16(bq76952_ic_sts_te bq76952_ic_sts_are);

extern U16 bq76952_set_fet_u16(bq76952_fet_te fet_type_are, bq76952_fet_state_te fet_state_are);
extern U16 bq76952_set_fet_test_u16(bq76952_fet_te fet_type_are, bq76952_fet_state_te fet_state_are);
extern U16 bq76952_reset_u16();
extern U16 bq76952_dev_id_u16();

extern U16 bq76952_get_all_values_u16(bq76952_all_values_tst* bq76952_all_values_arpst);

extern U16 bq76952_configure_u16(bq76952_config_tst* bq76952_config_arpst);

extern U16 bq76952_slp_config_u16(bq76952_sleep_configure_tst* sleep_configure_arpst);
extern U16 bq76952_alarm_config_u16(bq76952_alarm_configure_tst* alarm_configure_arpst);

extern U16 bq76952_enter_sleep_u16(bq76952_sleep_mode_te sleep_mode_are);
extern U16 bq76952_exit_sleep_u16(bq76952_sleep_mode_te sleep_mode_are);

extern U16 bq76952_wr_cb_sts_u16(U16 cb_act_reg_aru16);
extern U16 bq76952_rd_cb_sts_u16(U16* cb_act_reg_arpu16);

extern U8 bq76952_sleep_int_cnf();
extern U8 bq_slp_chck();
extern U8 bq76952_wr_subcmd_u8(U16 sub_cmd_no_aru16,U8* wr_data_arpu8, U8 wr_size_aru8, U8 delay_aru8);
#endif /* INC_BQ76952_H_ */
