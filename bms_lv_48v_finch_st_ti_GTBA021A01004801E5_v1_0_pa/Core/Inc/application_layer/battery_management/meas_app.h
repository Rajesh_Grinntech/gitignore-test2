/**
========================================================================================================================
@page meas_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	meas_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	07-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_MEAS_APP_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_MEAS_APP_H_

/* Includes for definitions */
#define NTC_SHORT_CIRCUIT_VALUE	(COM_HDR_MAX_S16 - 2)
#define NTC_OPEN_CIRCUIT_VALUE	(COM_HDR_MIN_S16 + 2)


typedef struct meas_app_isense_tst_tag
{
	S32 isense_raw_1mA_s32;
	S32 isense_unfiltered_s32;
	S32 isense_filtered_s32;


}meas_app_isense_tst;
/** @{
 * Type definition */
typedef struct  meas_app_cc_soc_tst_tag
{
	S32 coulomb_cnt_scaled_s32;
	S32 avg_current_1mA_s32;
	S32 delta_1mAmSec_s32;
	S32 cum_capacity_1mAmSec_s32;
	U32 new_sample_time_u32;
	U32 old_sample_time_u32;
}meas_app_cc_soc_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern meas_app_cc_soc_tst meas_app_cc_soc_gst;
extern U8 meas_app_temp_sts_u8;
extern BOOL meas_app_open_ckt_flg;
/** @} */

/* Public Function Prototypes **/
extern U8 meas_app_init_u8(void);
extern U8 meas_app_rd_afe_data_u8(void);
extern U8 meas_app_rd_current_sensor_data_u8(void);
extern void meas_app_update_main_structure_v(void);

extern void meas_app_init_cc(void);
extern void meas_app_compute_coulomb_count(void);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_MEAS_APP_H_ */
