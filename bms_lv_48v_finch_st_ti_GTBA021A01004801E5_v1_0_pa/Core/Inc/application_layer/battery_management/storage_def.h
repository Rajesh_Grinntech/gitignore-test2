/**
========================================================================================================================
@page storage_def_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	storage_def.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	30-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_STORAGE_DEF_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_STORAGE_DEF_H_

/* Includes for definitions */
#include "common_header.h"

/** @{
 * Type definition */

/* Macro Definitions */
#define STORAGE_DEF_DATA_LOG_STARTING_YEAR	2016

/**************************** External Flash Address Location *****************/                        /* maximum sectors 4096 */

#define STORAGE_DEF_CURRENT_CALIBRATION_FLASH_ADDRESS      				0xFF0000                    //Block 255
#define STORAGE_DEF_CURRENT_CALIBRATION_FLASH_SECTOR       				255
#define STORAGE_DEF_FLASH_DEFAULT_VALUE    		 		    			0xFFFFFFFF
#define STORAGE_DEF_SECTOR_SIZE             							4096
#define STORAGE_DEF_SECTORS_PER_BLOCK									16


#define STORAGE_DEF_VIN_UFD_LOG_SECTOR_SIZE 							1
#define STORAGE_DEF_BATTERY_CYCLE_COUNT_LOG_SECTOR_SIZE 				1
#define STORAGE_DEF_BST_LOG_SECTOR_SIZE									2
#define STORAGE_DEF_BIN_VIN_MISMATCH_LOG_SECTOR_SIZE					2
#define STORAGE_DEF_VST_LOG_SECTOR_SIZE									2
#define STORAGE_DEF_BATTERY_ONE_TIME_LOG_SECTOR_SIZE					5
#define STORAGE_DEF_VEHICLE_ONE_TIME_LOG_SECTOR_SIZE					5
#define STORAGE_DEF_VCU_60S_LOG_SECTOR_SIZE								5
#define STORAGE_DEF_VCU_10S_LOG_SECTOR_SIZE								25
#define STORAGE_DEF_BMS_15M_LOG_SECTOR_SIZE								2
#define STORAGE_DEF_BMS_1S_LOG_SECTOR_SIZE								2817
#define STORAGE_DEF_VEHICLE_1S_LOG_SECTOR_SIZE							450
#define STORAGE_DEF_CHARGER_10S_LOG_SECTOR_SIZE							100
#define STORAGE_DEF_CHARGER_60S_LOG_SECTOR_SIZE							100
#define STORAGE_DEF_BOOT_CONFIG_LOG_SECTOR_SIZE							1
#define STORAGE_DEF_BOOT_NEW_FIRMWARE_LOG_SECTOR_SIZE					34


/****************************************************************************************/
/**BLOCK 0 page 1 **/
#define STORAGE_DEF_START_ADDRESS_CURRENT_CALIBRATION			0x00000
#define STORAGE_DEF_END_ADDRESS_CURRENT_CALIBRATION				0x0FFFF

/**BLOCK 1 sector 1**/
#define STORAGE_DEF_START_ADDRESS_BOOT_CONFIG_CHECK				0x10000
#define STORAGE_DEF_END_ADDRESS_BOOT_CONFIG_CHECK            	(STORAGE_DEF_START_ADDRESS_BOOT_CONFIG_CHECK +(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_BOOT_CONFIG_LOG_SECTOR_SIZE))
/*BLOCK 1 Sector 2*/
#define STORAGE_DEF_START_ADDRESS_BOOTLOADER_LOG              	STORAGE_DEF_END_ADDRESS_BOOT_CONFIG_CHECK
#define STORAGE_DEF_END_ADDRESS_BOOTLOADER_LOG               	(STORAGE_DEF_START_ADDRESS_BOOTLOADER_LOG + (STORAGE_DEF_SECTOR_SIZE *\
														  	  	  STORAGE_DEF_BOOT_NEW_FIRMWARE_LOG_SECTOR_SIZE))
/****************************************************************************************/

#define START_ADDRESS_BOOT_CONFIG_CHECK         				STORAGE_DEF_START_ADDRESS_BOOT_CONFIG_CHECK //3686400
#define END_ADDRESS_BOOT_CONFIG_CHECK            				(START_ADDRESS_BOOT_CONFIG_CHECK +(STORAGE_DEF_SECTOR_SIZE*1))

#define START_ADDRESS_BOOTLOADER_LOG              				END_ADDRESS_BOOT_CONFIG_CHECK
#define END_ADDRESS_BOOTLOADER_LOG               				(START_ADDRESS_BOOTLOADER_LOG + (STORAGE_DEF_SECTOR_SIZE *75))



/*** Store BMS VIN and UFD data in external Flash  Address (0-4096) ***/
#define STORAGE_DEF_START_ADDRESS_VIN_UFD_LOG                  0    //Flash Start address
#define STORAGE_DEF_END_ADDRESS_VIN_UFD_LOG                    (STORAGE_DEF_START_ADDRESS_VIN_UFD_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																					STORAGE_DEF_VIN_UFD_LOG_SECTOR_SIZE))

/*** Store BMS battery cycle,soh data in external Flash  Address (4096-8192)****/
#define STORAGE_DEF_START_ADDRES_BATTERY_CYCLE_LOG             STORAGE_DEF_END_ADDRESS_VIN_UFD_LOG
#define STORAGE_DEF_END_ADDRESS_BATTERY_CYCLE_LOG              (STORAGE_DEF_START_ADDRES_BATTERY_CYCLE_LOG +(STORAGE_DEF_SECTOR_SIZE*\
															   STORAGE_DEF_BATTERY_CYCLE_COUNT_LOG_SECTOR_SIZE))

/**** BST during driving protocol data store in external flash Address(8192-16384)**/
#define STORAGE_DEF_START_ADDRESS_BST_LOG                       STORAGE_DEF_END_ADDRESS_BATTERY_CYCLE_LOG
#define STORAGE_DEF_END_ADDRESS_BST_LOG                        (STORAGE_DEF_START_ADDRESS_BST_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_BST_LOG_SECTOR_SIZE))

/*** BMS BIN and VIN mismatch Suspension Log  Address (16384 - 24576) ****/
#define STORAGE_DEF_START_ADDRESS_BIN_VIN_MISMATCH_LOG         STORAGE_DEF_END_ADDRESS_BST_LOG
#define STORAGE_DEF_END_ADDRESS_BIN_VIN_MISMATCH_LOG           (STORAGE_DEF_START_ADDRESS_BIN_VIN_MISMATCH_LOG+(STORAGE_DEF_SECTOR_SIZE*\
															   STORAGE_DEF_BIN_VIN_MISMATCH_LOG_SECTOR_SIZE))

/**** BMS vehicle suspension log  Address (24576 - 32768)*****/
#define STORAGE_DEF_START_ADDRESS_VST_LOG                      STORAGE_DEF_END_ADDRESS_BIN_VIN_MISMATCH_LOG
#define STORAGE_DEF_END_ADDRESS_VST_LOG                        (STORAGE_DEF_START_ADDRESS_VST_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_VST_LOG_SECTOR_SIZE))

/***** BMS battery one time data log Address (32768 - 53248)  ****/
#define STORAGE_DEF_START_ADDRESS_BATTERY_ONETIME_LOG          STORAGE_DEF_END_ADDRESS_VST_LOG
#define STORAGE_DEF_END_ADDRESS_BATTERY_ONETIME_LOG            (STORAGE_DEF_START_ADDRESS_BATTERY_ONETIME_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_BATTERY_ONE_TIME_LOG_SECTOR_SIZE))

/**** Vehicle One Time Data  Address (53248 - 73728) ******/
#define STORAGE_DEF_START_ADDRESS_VEHICLE_ONETIME_LOG          STORAGE_DEF_END_ADDRESS_BATTERY_ONETIME_LOG
#define STORAGE_DEF_END_ADDRESS_VEHICLE_ONETIME_LOG            (STORAGE_DEF_START_ADDRESS_VEHICLE_ONETIME_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_VEHICLE_ONE_TIME_LOG_SECTOR_SIZE))

/***** Vehicle Sixty Second log  address (73728 - 94208)  ****/
#define STORAGE_DEF_START_ADDRESS_VCU_60SEC_LOG                STORAGE_DEF_END_ADDRESS_VEHICLE_ONETIME_LOG
#define STORAGE_DEF_END_ADDRESS_VCU_60SEC_LOG                  (STORAGE_DEF_START_ADDRESS_VCU_60SEC_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_VCU_60S_LOG_SECTOR_SIZE))
/***** Vehicle ten Second log  address (94208 - 196608)  ****/
#define STORAGE_DEF_START_ADDRESS_VCU_10SEC_LOG                STORAGE_DEF_END_ADDRESS_VCU_60SEC_LOG
#define STORAGE_DEF_END_ADDRESS_VCU_10SEC_LOG                  (STORAGE_DEF_START_ADDRESS_VCU_10SEC_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_VCU_10S_LOG_SECTOR_SIZE))

/***** BMS 15 mim. data log address (196608 - 204800)   ****/
#define STORAGE_DEF_START_ADDRESS_BMS_15MIN_LOG                STORAGE_DEF_END_ADDRESS_VCU_10SEC_LOG
#define STORAGE_DEF_END_ADDRESS_BMS_15MIN_LOG                  (STORAGE_DEF_START_ADDRESS_BMS_15MIN_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_BMS_15M_LOG_SECTOR_SIZE))

/***** BMS 1 Sec data log address (204800  - 11,743,232)   ****/
#define STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG                 STORAGE_DEF_END_ADDRESS_BMS_15MIN_LOG
#define STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG				   0xFF0000
#if 0
#define STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG                 STORAGE_DEF_END_ADDRESS_BMS_15MIN_LOG
#define STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG                   (STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_BMS_1S_LOG_SECTOR_SIZE))
#endif

/***** Vehicle  1 Sec data log address (11,743,232  - 13,586,432)   ****/
#define STORAGE_DEF_START_ADDRESS_VEHICLE_1SEC_LOG              STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG
#define STORAGE_DEF_END_ADDRESS_VEHICLE_1SEC_LOG                (STORAGE_DEF_START_ADDRESS_VEHICLE_1SEC_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_VEHICLE_1S_LOG_SECTOR_SIZE))

/***** charger  10 Sec. data log address (13,586,432 - 13996032)   ****/
#define STORAGE_DEF_START_ADDRESS_CHARGER_10SEC_LOG            STORAGE_DEF_END_ADDRESS_VEHICLE_1SEC_LOG
#define STORAGE_DEF_END_ADDRESS_CHARGER_10SEC_LOG              (STORAGE_DEF_START_ADDRESS_CHARGER_10SEC_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_CHARGER_10S_LOG_SECTOR_SIZE))

/***** charger  60 Sec. data log address (13996032 - 14405632)   ****/
#define STORAGE_DEF_START_ADDRESS_CHARGER_60SEC_LOG             STORAGE_DEF_END_ADDRESS_CHARGER_10SEC_LOG
#define STORAGE_DEF_END_ADDRESS_CHARGER_60SEC_LOG              (STORAGE_DEF_START_ADDRESS_CHARGER_60SEC_LOG+(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_CHARGER_60S_LOG_SECTOR_SIZE))
#if 0

/**** Bootloader Config data log address (14405632 -14409728)   ***/
#define STORAGE_DEF_START_ADDRESS_BOOT_CONFIG_CHECK          	STORAGE_DEF_END_ADDRESS_CHARGER_60SEC_LOG
#define STORAGE_DEF_END_ADDRESS_BOOT_CONFIG_CHECK            	(STORAGE_DEF_START_ADDRESS_BOOT_CONFIG_CHECK +(STORAGE_DEF_SECTOR_SIZE*\
																STORAGE_DEF_BOOT_CONFIG_LOG_SECTOR_SIZE))
/**** Bootloader new firmware data  log address (14409728 - 14,548,992)   ***/
#define STORAGE_DEF_START_ADDRESS_BOOTLOADER_LOG              	STORAGE_DEF_END_ADDRESS_BOOT_CONFIG_CHECK
#define STORAGE_DEF_END_ADDRESS_BOOTLOADER_LOG               	(STORAGE_DEF_START_ADDRESS_BOOTLOADER_LOG + (STORAGE_DEF_SECTOR_SIZE *\
														  	  	  STORAGE_DEF_BOOT_NEW_FIRMWARE_LOG_SECTOR_SIZE))
#endif

/* Sectors information */
#define STORAGE_DEF_BATT_1_TIMEDATA_START_SECT					7
#define STORAGE_DEF_BOOTLOADER_START_SECT						3517			(3517 - 1)


/** @} */
/** @{
 *  Public Variable Definitions */
extern U8 bootloader_current_sect_u16;

/** @} */

/* Public Function Prototypes **/
extern U8 storage_def_flash_store_data_u8(U32 *start_log_addr_arpu32,U32 max_log_addr_aru32,U8 *data_arpu8, U32 data_size_aru32);
extern U8 storage_def_flash_read_data_u8(U32 read_log_addr_aru32,U32 max_log_addr_aru32,U8* read_data_arpu8, U32 read_data_size_aru32);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_STORAGE_DEF_H_ */
