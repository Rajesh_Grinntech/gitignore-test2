/**
========================================================================================================================
@page common_var_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	common_var.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	30-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_COMMON_VAR_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_COMMON_VAR_H_

/* Includes for definitions */
#include "common_header.h"

/* Macro Definitions */
#define COMMON_VAR_BMS_VIN_SIZE						17
#define COMMON_VAR_BMS_UFD_SIZE		  				16
#define COMMON_VAR_BMS_BIN_SIZE		  				20
#define COMMON_VAR_BMS_MAC_SIZE						6
#define COMMON_VAR_BMS_DATE_TIME_SIZE				4

#define  COMMON_VAR_CV_MODE_MAX_BATT_VOLTAGE   		66.5            //53.3
#define  COMMON_VAR_BMS_NO_OF_CELLS           		16
#define  COMMON_VAR_BMS_NO_OF_CELL_TEMPERATURE  	9
#define  COMMON_VAR_BAT_DEMAND_CURRENT_0_5 			1500
#define  COMMON_VAR_BAT_DEMAND_CURRENT_0_25			750
#define  COMMON_VAR_MAX_CELL_TEMPERATURE_CUT_OFF    600
#define  COMMON_VAR_MAX_CELL_TEMP_RELEASE       	500
#define  COMMON_VAR_MIN_CELL_TEMP_RELEASE       	150
#define  COMMON_VAR_AVAILABLE_ENERGY				1000


/**** Driving and charging data erased ****/
#define COMMON_VAR_FLASH_NO_OF_ERACE_SECTOR   	 	17
#define COMMON_VAR_FLASH_NO_OF_BLOCKS				256

#if 0
/** @{
 * Type definition */
typedef enum common_var_pack_state_te_tag
{
	COMMON_VAR_AUX_PWR_STATE         =       0,
	COMMON_VAR_DRIVING_STATE         =       1,
	COMMON_VAR_CHARGING_STATE        =       2,
	COMMON_VAR_OFF_STATE             =       3,
	COMMON_VAR_DEBUG_STATE           =       4,
	COMMON_VAR_LOW_ENERGY_OFF_STATE  =       5,
}common_var_pack_state_te;

#endif
typedef enum common_var_state_machines_te_tag
{
	COMMON_VAR_CHARGING_SM	= 0	,
	COMMON_VAR_DRIVING_SM		,
	COMMON_VAR_DEBUG_SM			,
	COMMON_VAR_MAX_SM

}common_var_state_machines_te;


typedef struct __attribute__((packed))
{
  U8 vin_au8[COMMON_VAR_BMS_VIN_SIZE];
  U8 ufd_au8[COMMON_VAR_BMS_UFD_SIZE];
  U8 bin_au8[COMMON_VAR_BMS_BIN_SIZE];
  U8 mac_au8[COMMON_VAR_BMS_MAC_SIZE];
}common_var_bms_id_log_tst;
#if 0
typedef  struct	 __attribute__((packed))
{                             /* Vehicle Suspension Log Structure */
  U32 time_stamp_u32;
  U32 threshold_value_u32;
  U16 error_code_u16;
  U32 breach_value_u32;
}bms_vst_datalog_tst;

typedef  struct	 __attribute__((packed))
{                             /* Battery VIN Suspension Log Structure */
  U32 time_stamp_u32;
  U16 error_code_u16;
  U8  vin_au8[COMMON_VAR_BMS_VIN_SIZE];
}bms_bst_vin_datalog_tst;

typedef  struct	 __attribute__((packed))
{                             /* Battery Suspension Log Structure */
  U32 time_stamp_u32;
  U32 threshold_value_u32;
  U16 error_code_u16;
  U32 breach_value_u32;
}bms_bst_datalog_tst;

typedef  struct	 __attribute__((packed))
{                             /* Battery 15_Minutes Log Structure */
  U32 time_stamp_u32;
  U8  lock_mode_u8;
  U16 amb_temp_u16;
}bms_battery_15min_log_tst;
#endif

typedef  struct	 __attribute__((packed))
{                             /* Vehicle 10_Second Log Structure */
//  U32 time_stamp_u32;
//  U16 effective_res_pwr_path_u16;
//  U32 odometer_reading_u32;
//  U16 available_energy_u16;
//  U8 battery_mode_u8;
//  U16 consumed_energy_u16;
}bms_drive_onetime_log_tst;

#if 0
typedef  struct	 __attribute__((packed))
{
  U32 time_stamp_u32;
  U16 vehicle_speed_u16;
  U32 odometer_reading_u32;
  U16 controller_current_u16;
  U16 controller_voltage_u16;
}bms_vehicle_1second_log_tst;

typedef  struct	 __attribute__((packed))
{                             /* Vehicle 10_Second Log Structure */
  U32 time_stamp_u32;
  U16 oc_2_u16;
  U16 oc_3_u16;
}bms_vehicle_10second_log_tst;

typedef  struct	 __attribute__((packed))
{                             /* Vehicle 60_Second Log Structure */
  U32 time_stamp_u32;
  U16 oc_4_u16;
  U16 oc_5_u16;
}bms_vehicle_60second_log_tst;

/********************** Battery Parameters One_Second structure ***************/
typedef  struct  __attribute__((packed))
{
  U32 time_stamp_u32;
  U16 available_energy_u16;
  U16 max_battery_current_u16;
  U16 battery_instant_voltage_u16;
  SFP battery_instant_current_u16;
  U16 battery_cell_voltage_au8[COMMON_VAR_BMS_NO_OF_CELLS];
  U16 individual_sensor_tempurature_au8[COMMON_VAR_BMS_NO_OF_CELL_TEMPERATURE];
  U16 individual_balancing_cell_sts_u16;
  U32 log_addr_u8;
  U8  pack_fault_u8;
  U8  pack_status_u8;
  U8  mosfet_status_u8;
  U16 soc_u16;
}bms_battery_data_log_tst;


#if 0
typedef  struct __attribute__((packed))
{
	U32 time_stamp_u32;
	U16 battery_instant_voltage_u16;
	U16 max_battery_current_u16;
	U16 battery_cell_voltage_au8[COMMON_VAR_BMS_NO_OF_CELLS];
	U16 individual_sensor_tempurature_au8[COMMON_VAR_BMS_NO_OF_CELL_TEMPERATURE];
	U8  individual_balancing_cell_sts_u16;
	U16 soc_u16;
	U16 available_energy_u16;
	U16 log_addr_u16;
	U8  pack_fault_u8;
	U8  pack_status_u8;
	U8  mosfet_status_u8;
	float Battery_Instant_Current_u16;

}bms_debug_battery_data_log_tst;
#endif
#endif
typedef struct __attribute__((packed))
{
	U32 time_stamp_u32;
	U16 pack_voltage_u16;
	S32 pack_current_s32;
	U16 battery_cell_voltage_au16[COMMON_VAR_BMS_NO_OF_CELLS];
	S16 individual_sensor_tempurature_as16[COMMON_VAR_BMS_NO_OF_CELL_TEMPERATURE];
	S16 mfet_temperature_s16;
	U16 individual_balancing_cell_sts_u16;
	U16 soc_u16;
	U16 available_energy_u16;
	//U32 log_addr_u32;
	U8  pack_fault_u8;
	U8  pack_status_u8;
	U8  mosfet_status_u8;
	U16 pk_hum_u16;
	S16 pk_temp_s16;
	U16 hvadc_pk_vtg_u16;
	U16 hvadc_fuse_vtg_u16;
	U16 hvadc_mfet_vtg_u16;
}bms_debug_battery_data_log_tst;

#if 0
typedef  struct  __attribute__((packed))
{
  U16 rated_Ah_u16;
  U32 cycle_u32 ;
  U16 charge_time_u16 ;
  U16 last_calibrated_bat_cycle_u16;
  U32 adaptive_charge_count_u32;
  U16 soh_u16;
  U8  enter_deepsleep_mode_u8;
  U8  first_time_flash_read;
  U32 last_soh_calibration_time_stamp;
  U16 last_consumed_energy;
}bms_battcapacity_chargetime_cycle_tst;
#endif
typedef  struct  __attribute__((packed))
{
	U8 protocol_state_u8;
	U8 protocol_status_u8;
}common_var_can_state_tst;
#if 0
/** @} */
/** @{
 *  Public Variable Definitions */

extern bms_vst_datalog_tst 	  					bms_vst_datalog_gst;
extern bms_bst_vin_datalog_tst					bms_bst_vin_datalog_gst;
extern bms_bst_datalog_tst						bms_bst_datalog_gst;
extern bms_battery_15min_log_tst				bms_battery_15min_log_gst, rd_bms_battery_15min_log_gst;
extern bms_drive_onetime_log_tst				bms_batt_onetime_log_gst, bms_drive_onetime_log_gst;
extern bms_drive_onetime_log_tst 				rd_bms_batt_onetime_log_gst;
extern bms_vehicle_1second_log_tst				bms_vehicle_1second_log_gst;
extern bms_vehicle_10second_log_tst				bms_vehicle_10second_log_gst,rd_bms_vehicle_10sec_data_log_gst;
extern bms_vehicle_60second_log_tst				bms_vehicle_60second_log_gst,rd_bms_vehicle_60sec_data_log_gst;
extern bms_battery_data_log_tst					bms_battery_data_log_gst,rd_bms_battery_data_log_gst;
extern bms_debug_battery_data_log_tst			bms_debug_battery_data_log_gst,rd_debug_battery_data_log_gst;
extern bms_debug_battery_data_log_tst			flsh_ret_debug_battery_data_log_gst;
extern bms_battcapacity_chargetime_cycle_tst	bms_battcapacity_chargetime_cycle_gst;


extern U32 start_address_bst_log_U32          ;
extern U32 start_address_bmd_id_log_u32	   	  ;
extern U32 start_address_vst_log_u32          ;
extern U32 start_address_drive_onetime_log    ;
extern U32 start_address_battery_onetime_log  ;
extern U32 start_address_vcu_60second_log     ;
extern U32 start_address_VCU_10Second_Log     ;
extern U32 start_address_BMS_15Minute_Log     ;
extern U32 start_address_BMS_1Second_Log      ;
extern U32 start_address_Charger_10Second_Log ;
extern U32 start_address_Vehicle_1Second_Log  ;
extern U32 start_address_Charger_60Second_Log ;

extern U32 rd_addr_bms_1sec_log_u32;;
extern U32 rd_addr_bms_10sec_log_u32;
extern U32 rd_addr_bms_15sec_log_u32;
extern U32 rd_addr_bms_60sec_log_u32;
extern U32 rd_addr_bms_15min_log_u32;
#endif
extern common_var_bms_id_log_tst 				common_var_bms_id_log_gst;

extern common_var_can_state_tst 				common_var_can_state_gst[COMMON_VAR_MAX_SM];

extern bms_drive_onetime_log_tst				bms_batt_onetime_log_gst, bms_drive_onetime_log_gst;

extern bms_debug_battery_data_log_tst			bms_debug_battery_data_log_gst,rd_debug_battery_data_log_gst;
extern bms_debug_battery_data_log_tst			flsh_ret_debug_battery_data_log_gst;
/** @} */

/* Public Function Prototypes **/

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_COMMON_VAR_H_ */
