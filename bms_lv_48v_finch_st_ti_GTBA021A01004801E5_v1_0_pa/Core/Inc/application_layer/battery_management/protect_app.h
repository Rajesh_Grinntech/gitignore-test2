/**
========================================================================================================================
@page protect_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	protect_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	07-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_PROTECT_APP_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_PROTECT_APP_H_

/* Includes for definitions */

#define PROTECT_APP_MAX_RE_TMOUT          (3U)

#define PROTECT_APP_MAX_CV_TMOUT 		(10U)        /* Max Cell Voltage timeout */
#define PROTECT_APP_MIN_CV_TMOUT 		(10U)		 /* Min Cell Voltage timeout */
#define PROTECT_APP_MIN_CDDV_TMOUT 		(1U)		 /* Min Cell Deep Discharge voltage timeout */
#define PROTECT_APP_CV_BAL_ERR_TMOUT 	(100U)		 /* Cell Voltage balancing error timeout */

#define PROTECT_APP_MAX_PV_TMOUT 		(10U)
#define PROTECT_APP_MIN_PV_TMOUT 		(10U)

#define PROTECT_APP_MAX_CT_TMOUT    	(10U)		/* Maximum Cell temperature timeout 	*/
#define PROTECT_APP_MIN_CT_TMOUT    	(10U)		/* Minimum Cell temperature timeout 	*/
#define PROTECT_APP_MAX_PT_TMOUT    	(10U)		/* Maximum Pack temperature timeout 	*/
#define PROTECT_APP_MIN_PT_TMOUT    	(10U)		/* Minimum Pack temperature timeout 	*/
#define PROTECT_APP_MAX_MT_TMOUT		(10U)		/* Maximum MOSFET temperature timeout 	*/
#define PROTECT_APP_MIN_MT_TMOUT		(10U)		/* Minimum MOSFET temperature timeout 	*/

#define PROTECT_APP_MAX_CHG_I_TMOUT     (10U)		/* Maximum charge current timeout 		*/
#define PROTECT_APP_MAX_DCHG_I_TMOUT	(3U)/* 3SEC */		/* Maximum discharge current timeout 	*/
#define PROTECT_APP_CONT_DCHG_I_TMOUT	(32U)/* 32SEC */	/* Cont. discharge current timeout 	*/

#define PROTECT_APP_AFE_ERR_ALLWD_OCCUR		(5U)		/* AFE error allowed occurence 			*/
#define PROTECT_APP_AFE_FET_ERR_ALLWD_OCCUR	(1U)		/* AFE Fet error allowed occurence 		*/

#define PROTECT_APP_RECVRY_OC_DCHG 		(10U)		/* Recovery OC discharge 				*/
#define PROTECT_APP_RECVRY_SC_DCHG 		(10U)		/* Recovery SC discharge 				*/
#define PROTECT_APP_RECVRY_OC_CHG  		(10U)		/* Recovery OC charge 					*/
#define PROTECT_APP_RECVRY_HW_AFE_PRT  	(10U)       /* Recover hardware AFE protect 		*/

#define PROTECT_APP_HVADC_TMOUT        (20U)

#define MAX_WARNING_TIMEOUT (5U)
/** @{
 * Type definition */
typedef enum protect_app_check_state_te_tag
{
	PRT_CELL_OVER_VOLTAGE = 0,
	PRT_CELL_UNDER_VOLTAGE,
	PRT_CELL_DEEP_DISCHARGE,
	PRT_CELL_SHORT_OPEN,
	PRT_CELL_BAL_ERROR,
	PRT_CELL_OVER_TEMP,
	PRT_CELL_UNDER_TEMP,
	PRT_CELL_OPEN_TEMP,
	PRT_PACK_VOLTAGE,
	PRT_PACK_OVER_VOLTAGE,
	PRT_PACK_UNDER_VOLTAGE,
	PRT_PACK_OVER_CHG_CURRENT,
	PRT_PACK_OVER_DSG_CURRENT,
	PRT_PACK_OVER_CONT_DSG_CURRENT,
	PRT_PACK_OVER_LOAD_DSG_CURRENT,
	PRT_PACK_OVER_TEMP,
	PRT_PACK_UNDER_TEMP,
	PRT_FET_OVER_TEMP,
	PRT_FET_UNDER_TEMP,
	PRT_AFE_OVER_TEMP,
	PRT_AFE_UNDER_TEMP,
	PRT_AFE_HW_COUNTER,
	PRT_AFE_HW_RECV_COUNTER,
	PRT_AFE_FET_MALFN,
	PRT_AFE_FET_RECV_MALFN,
	PRT_APP_HVADC_PACK_OV,
	PRT_APP_HVADC_PACK_UV,
	PRT_APP_HVADC_FUSE_VTG,
	PRT_APP_HVADC_MFET_VTG,
	PRT_AFE_FAIL_DURING_OPERATION_ON,
	PRT_SCD_FAULT,
	PRT_PACK_MAX_STATE
}protect_app_check_state_te;

typedef enum protect_app_state_tmr_te_tag
{
	PRT_OC_DSG = 0,
	PRT_OC_CHG,
	PRT_SCD_SW,
	PRT_OC_DSG_WARNING,
		PRT_OC_CHG_WARNING,
		PRT_SCD_WARN,
	PRT_AFE_HW,

	PRT_MAX_STATE
}protect_app_state_tmr_te;

/** @} */
/** @{
 *  Public Variable Definitions */

#if 0
extern U16 prt_app_breach_val_au16[MAX_BWI];
#endif
/** @} */

extern U8 	 prt_app_dchg_flg_u8;
extern U8 	 prt_app_cont_dchg_flg_u8;
extern U8   prt_app_violation_samples_au8[PRT_PACK_MAX_STATE];
/* Public Function Prototypes **/
extern void protect_app_batt_mode_update_v(void);
extern void protect_app_backup_previous_decision_v(void);
extern void protect_app_check_cv_violation_v(void);
extern void protect_app_check_ct_violation_v(void);
extern void protect_app_check_pv_violation_v(void);
extern void protect_app_check_pt_violation_v(void);
extern void protect_app_check_pi_violation_v(void);
extern void protect_app_hw_protection_recovery_v(void);
extern void protect_app_check_hvadc_violation_v(void);
extern void protect_app_check_hvil_violation_v(void);
extern void protect_app_check_pk_hum_violation_v(void);
extern void protect_app_check_afe_violation_v(BOOL occured_arg_b);
extern void protect_app_check_afe_fet_malfn_v(BOOL occured_arg_b);
extern void protect_app_check_fet_violation_v(void);

extern U8 protect_app_safety_monitoring_u8(void);
extern void protect_app_check_warning_flgs_v(void);
extern void protect_app_tmr_1s_update_v(void);
extern void protect_app_check_afe_die_temp_violation_v(void);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_PROTECT_APP_H_ */
