/**
========================================================================================================================
@page batt_param_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	batt_param.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	07-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_BATT_PARAM_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_BATT_PARAM_H_

/* Includes for definitions */
#include "bms_config.h"

#define BATT_PARAM_TOTAL_SLAVES			  	1

#define BATT_PARAM_TEMP_ID_MAX    	    	(PACK_PARAM_THERMISTOR_ID_6 + 1)
#define BATT_PARAM_CELL_ID_MAX           	(PACK_PARAM_CELL_ID_14 + 1)				/*TODO : Change- 16 to 14 Cells*/
#define BATT_PARAM_CELL_TEMP_ID_MAX      	(PACK_PARAM_THERMISTOR_ID_4 + 1)
#define BATT_PARAM_MFET_TEMP_ID_MAX      	(PACK_PARAM_THERMISTOR_ID_1 + 1)
#define BATT_PARAM_PACK_TEMP_ID_MAX      	(PACK_PARAM_THERMISTOR_ID_1 + 1)

#define BATT_PARAM_TOT_CELL_MAX             (14)
#define BATT_PARAM_TOT_CELL_TEMP_ID_MAX     (4)
#define BATT_PARAM_TOT_SLAVE_CELLS			((BATT_PARAM_CELL_ID_MAX) * BATT_PARAM_TOTAL_SLAVES)
#define BATT_PARAM_TOT_CELL_TEMP_SENS		(BATT_PARAM_CELL_TEMP_ID_MAX * BATT_PARAM_TOTAL_SLAVES)

/* NTC Sensors*/
#define BATT_PARAM_TOT_NTC_TEMP_SENS		(PACK_PARAM_THERMISTOR_ID_2 + 1)

#define BATT_PARAM_TEMP_SENS_DIV			(BATT_PARAM_TOT_CELL_TEMP_ID_MAX / BATT_PARAM_TOTAL_SLAVES)
#define BATT_PARAM_TEMP_SENS_REM			(BATT_PARAM_TOT_CELL_TEMP_ID_MAX % BATT_PARAM_TOTAL_SLAVES)

/* This macro is to declare the array variable with max possible thermistor per slave  */
#define BATT_PARAM_MAX_CT_PER_SLV	BATT_PARAM_CELL_TEMP_ID_MAX//(BATT_PARAM_TEMP_SENS_REM) ? (BATT_PARAM_TEMP_SENS_DIV + 1) : (BATT_PARAM_TEMP_SENS_DIV) /* CT -> Cell Temperature*/

/* This macro is to get the total temp per slave  */
#define BATT_PARAM_CT_PER_SLV(MOD_NUM) (BATT_PARAM_TEMP_SENS_REM > MOD_NUM) ? (BATT_PARAM_TEMP_SENS_DIV + 1) : (BATT_PARAM_TEMP_SENS_DIV) /* CT -> Cell Temperature*/




#define BATT_PARAM_AI_INIT_VAL_U16       (0xFFFD)
#define BATT_PARAM_AI_PLAUS_VAL_U16      (0xFFFE)
#define BATT_PARAM_AI_FLT_VAL_U16        (0xFFFF)
#define BATT_PARAM_AI_INIT_VAL_S16       (0x7FFD)
#define BATT_PARAM_AI_PLAUS_VAL_S16      (0x7FFE)
#define BATT_PARAM_AI_FLT_VAL_S16        (0x7FFF)

#define BATT_PARAM_AI_INIT_VAL_U32       (0xFFFFFFFD)
#define BATT_PARAM_AI_PLAUS_VAL_U32      (0xFFFFFFFE)
#define BATT_PARAM_AI_FLT_VAL_U32        (0xFFFFFFFF)
#define BATT_PARAM_AI_INIT_VAL_S32       (0x7FFFFFFE)
#define BATT_PARAM_AI_PLAUS_VAL_S32      (0x7FFFFFFE)
#define BATT_PARAM_AI_FLT_VAL_S32        (0x7FFFFFFF)

/** @{
 * Type definition */

/* The battery limit enum */
typedef enum batt_param_limit_te_tag
{
	PACK_PARAM_LIMIT_NA = 0,
	PACK_PARAM_LIMIT_NOT_ACTIVE,
	PACK_PARAM_LIMIT_ACTIVE,
	PACK_PARAM_LIMIT_ERROR 			/*always leave this at the end*/
}  batt_param_limit_te;

/* 16 cells max in each modules */
typedef enum batt_param_cell_id_te_tag
{
    PACK_PARAM_CELL_ID_1 = 0,
    PACK_PARAM_CELL_ID_2,
    PACK_PARAM_CELL_ID_3,
    PACK_PARAM_CELL_ID_4,
    PACK_PARAM_CELL_ID_5,
    PACK_PARAM_CELL_ID_6,
    PACK_PARAM_CELL_ID_7,
    PACK_PARAM_CELL_ID_8,
    PACK_PARAM_CELL_ID_9,
    PACK_PARAM_CELL_ID_10,
    PACK_PARAM_CELL_ID_11,
    PACK_PARAM_CELL_ID_12,
    PACK_PARAM_CELL_ID_13,
    PACK_PARAM_CELL_ID_14,
	PACK_PARAM_CELL_ID_15,
	PACK_PARAM_CELL_ID_16,
    PACK_PARAM_CELL_ID_MAX_NUM 		/*always leave this at the end*/
} batt_param_cell_id_te;

typedef enum batt_param_mod_id_te_tag
{
	PACK_PARAM_MOD_ID_1 = 0,
	PACK_PARAM_MOD_ID_MAX_NUM
}batt_para_mod_id_te;

typedef enum batt_param_thermistor_id_te_tag
{
    PACK_PARAM_THERMISTOR_ID_1 = 0,
	PACK_PARAM_THERMISTOR_ID_2,
	PACK_PARAM_THERMISTOR_ID_3,
	PACK_PARAM_THERMISTOR_ID_4,
	PACK_PARAM_THERMISTOR_ID_5,
	PACK_PARAM_THERMISTOR_ID_6,
//	PACK_PARAM_THERMISTOR_ID_7,
//	PACK_PARAM_THERMISTOR_ID_8,
//	PACK_PARAM_THERMISTOR_ID_9,
//	PACK_PARAM_THERMISTOR_ID_10,
//	PACK_PARAM_THERMISTOR_ID_11,
//	PACK_PARAM_THERMISTOR_ID_12,
//	PACK_PARAM_THERMISTOR_ID_13,
//	PACK_PARAM_THERMISTOR_ID_14,
//	PACK_PARAM_THERMISTOR_ID_15,
//	PACK_PARAM_THERMISTOR_ID_16,
//	PACK_PARAM_THERMISTOR_ID_17,
//	PACK_PARAM_THERMISTOR_ID_18,

	PACK_PARAM_THERMISTOR_ID_MAX_NUM /*always leave this at the end*/

} batt_param_thermistor_id_te;

typedef enum batt_param_action_te_tag
{
	PACK_PARAM_ACTION_NA = 1,
	PACK_PARAM_ACTION_ONLY_CHG_OFF,
	PACK_PARAM_ACTION_ONLY_DHG_OFF,
	PACK_PARAM_ACTION_BOTH_OFF
} batt_param_action_te;

typedef enum batt_param_all_mfet_sts_te_tag
{
	BATT_PARAM_ALL_MFET_OFF		 = 0x00			,
	BATT_PARAM_CHG_MFET_ONLY_ON  = 0x01			,
	BATT_PARAM_DSG_MFET_ONLY_ON	 = 0x02			,
	BATT_PARAM_BOTH_MAIN_MFET_ON = 0x03			,
	BATT_PARAM_PCHG_MFET_ONLY_ON = 0x04			,
	BATT_PARAM_PDSG_MFET_ONLY_ON = 0x08			,
	BATT_PARAM_CHG_PDSG_MFET_ON	 = 0x09			,
	BATT_PARAM_BOTH_PRE_MFET_ON	 = 0X0C			,
	BATT_PARAM_BOTH_PRE_MFET_ON_CHG_MFET_ON	 = 0X0D			,
	BATT_PARAM_CHG_PCHG_MFET_ON	 = 0x05			,
	BATT_PARAM_DSG_PDSG_MFET_ON	 = 0x0A			,
	BATT_PARAM_ALL_MFET_ON		 = 0x0F
}batt_param_all_mfet_sts_te;

typedef enum pack_param_pack_fault_status_te_tag
{
	FAULT_NONE = 0,

	/* Protection */
	FAULT_CELL_UNDERVOLTAGE,
	FAULT_CELL_OVERVOLTAGE,
	FAULT_PACK_UNDERVOLTAGE,
	FAULT_PACK_OVERVOLTAGE,
	FAULT_PACK_OVERCURRENT_DSG,
	FAULT_PACK_OVERCURRENT_CHG,
	FAULT_PACK_OVERTEMP_CHG,
	FAULT_PACK_OVERTEMP_DSG,
	FAULT_PACK_UNDERTEMP_CHG,
	FAULT_PACK_UNDERTEMP_DSG,
	FAULT_PACK_SHORT_CIRCUIT,
	FAULT_PACK_AFE_COMM_ERROR,

	/* Failure */
	FAULT_PACK_THERMISTOR_FAIL,
	FAULT_PACK_AFE_HW_FAIL,
	FAULT_PACK_SCD_FAIL

}pack_param_pack_fault_status_te;

typedef enum batt_param_hw_flt_te_tag
{
	  HW_FAULT_NONE= 0,
	  HW_FAULT_OCD,
	  HW_FAULT_SCD,
	  HW_FAULT_OV,
	  HW_FAULT_UV,
	  HW_FAULT_OVRD_ALERT
} batt_param_hw_flt_te;

typedef enum batt_param_batt_mode_te_tag
{
	BATT_MODE_IDLE = 0,
	BATT_MODE_CHG,
	BATT_MODE_DSG,
	BATT_MODE_FAULT
} batt_param_batt_mode_te;

typedef enum batt_param_chrg_mode_te_tag
{
	CHARGER_MODE_OFF = 0,
	CAHRGER_MODE_CHARGING

} batt_param_chrg_mode_te;

typedef enum batt_param_load_mode_te_tag
{
	LOAD_MODE_IDLE = 0,
	LOAD_MODE_DISCHARGING

} batt_param_load_mode_te;



typedef struct __attribute__((packed)) batt_param_cell_param_tst_tag
{
    U16 cell_meas_1mV_u16;
    U16 cell_ocv_soc_1cPC_u16;
} batt_param_cell_param_tst;


typedef struct batt_param_temp_param_tst_tag
{
    S16 temp_meas_1cc_s16;                   /* Temperature measured values */
} batt_param_temp_param_tst;

typedef struct __attribute__((packed)) batt_param_pack_param_tst_tag
{
    /* @Pack Voltage */
    U16                     		pack_param_1cV_u16;					 /* Sum of Cell Voltage Pack Voltage eg: 100.10V = 10010 cV */
    U16                     		pack_param_stack_vtg_1cV_u16;		/* Sum of Cell Voltage by AFE*/

    U16                     		pack_param_ld_det_vtg_1cV_u16;			/* Load Detect vtg by AFE*/

    U16                     		pack_param_max_cell_1mV_u16;         /* max V of any one cell eg: 3.1254V = 3125 mV */
    U16                     		pack_param_avg_cell_1mV_u16;         /* avg V of any one cell */
    U16                     		pack_param_min_cell_1mV_u16;         /* min V of any one cell */
    U16 							pack_param_delta_cell_1mV_u16;		 /* delta V = max V - min V */

    batt_param_cell_id_te			pack_param_min_V_cell_index_e;
    batt_param_cell_id_te			pack_param_max_V_cell_index_e;
    batt_para_mod_id_te				pack_param_mod_at_maxV_e;
	batt_para_mod_id_te				pack_param_mod_at_minV_e;

	U16                     		pack_param_bal_err_1cPC_u16;         /* Current Balance Error eg: 10.23 % = 1023 cPC */ //TODO Include cal

    /* @Pack Current */
    S32                     		pack_param_1mA_s32;			 		 /* Pack current */

    /* @Cell Temperature */
    // S16                   		pack_param_1dC_s16;                  /* Pack temperature, will be the max cell temp eg: 12.4 C = 124 dC */
    S16                     		pack_param_min_cell_1cc_s16;         /* min C of any one cell */
    S16                     		pack_param_max_cell_1cc_s16;         /* max T of any one cell */
    S16 							pack_param_avg_cell_1cc_s16;

    batt_param_thermistor_id_te		pack_param_max_T_cell_index_e;
    batt_param_thermistor_id_te		pack_param_min_T_cell_index_e;
    batt_para_mod_id_te				pack_param_mod_at_maxT_e;
    batt_para_mod_id_te			    pack_param_mod_at_minT_e;
    U8								pack_param_tot_therm_per_slv_au8[BATT_PARAM_TOT_SLAVE_CELLS]; /* Holds total thermistors in each slave*/
    U32								pack_param_faulty_thermistor_id_u32;		   				  /* Holds the faulty themistors ID in bits */

    //AFE Die temperature
    S16 pack_param_afe_min_die_temp_1cc_s16;
    S16 pack_param_afe_max_die_temp_1cc_s16;
    batt_para_mod_id_te pack_param_min_die_T_afe_index_e;
	batt_para_mod_id_te pack_param_max_die_T_afe_index_e;

	//MCU Die temperature
	S16 							mcu_internal_die_temp_s16;

    /* @cell fault status*/
    /* Holds the individual cell fault status of every slave */
    U16								pack_param_faulty_cell_id_au16[BATT_PARAM_TOTAL_SLAVES];

    /* @MFET Temperature */
    S16								pack_param_mosfet_1cc_s16[BATT_PARAM_MFET_TEMP_ID_MAX];
    S16                    			pack_param_min_mfet_1cc_s16;                   /* min T of any one mfet */
    S16                   			pack_param_max_mfet_1cc_s16;                   /* max T of any one mfet */
    batt_param_thermistor_id_te	    pack_param_max_T_mfet_index_e;
    batt_param_thermistor_id_te	    pack_param_min_T_mfet_index_e;

    /* @Pack or ambient Temperature */
    S16								pack_param_pk_temp_1cc_s16;					   /* Pack or amb temp */
    S16                    			pack_param_min_pack_1cc_s16;                   /* min temperature of pack */
    S16                   			pack_param_max_pack_1cc_s16;                   /* max temperature of pack */
    batt_param_thermistor_id_te		pack_param_max_T_pack_index_e;
    batt_param_thermistor_id_te		pack_param_min_T_pack_index_e;

#if 0
    /* @Pack or ambient Humidity */
    U16								pack_param_pk_hum_u16;							/* Pack or ambient humidity */
#endif

    /* @Pack Energy */
    S16                     		pack_param_inst_power_1W_s16;                  	/* Inst. Power eg: 1W = 1V * 1I */

    U16                     		pack_param_energy_1cWh_u16;                    	/* Available cWh energy */
    SFP                    		 	pack_param_capacity_1cAhr_f;                   	/* Pack cAh Capacity */
    SFP 							pack_param_cycle_complete_cap_1cAhr_f;

    U32 							pack_param_cum_batt_in_energy_1Wh_u32;
    U32 							pack_param_cum_batt_out_energy_1Wh_u32;

    /* @Pack SOC and SOH */
    U32                     		pack_param_soh_1mPC_u32;                       /* Pack SOH eg: 99.620 PC = 99620 mPC*/
    U16 				    		pack_param_OCV_soc_1cPC_u16;				   /* Pack SOC based on OCV */
    U16                     		pack_param_coulambic_soc_1cPC_u16;             /* Pack Coulambic SOC */
    U16								pack_param_usable_soc_1cPC_u16;				   /* Pack SOC usable based on DOD */
    U16 							pack_param_min_cell_soc_u16;					/* minimum cell soc */
    U16 							pack_param_max_cell_soc_u16;					/* maximum cell soc */

    /** SOP parameters  **/
    U32 							pack_param_sop_available_chg_pwr_u32;
    U32 							pack_param_sop_available_dsg_pwr_u32;
    U32								pack_param_sop_max_dis_charge_current_mA_u32;
    U32								pack_param_sop_max_charge_current_mA_u32;

    /* @Pack Parameters */
    U32								pack_param_cycle_count_u32;					   /* Accumulates the no. of cycles */

    /* @Cell Protection Flags */
    batt_param_limit_te				pack_param_cell_open_short_limit_active;             /* Open cell terminal or shrt cell terminal*/
    batt_param_limit_te        		pack_param_max_cell_v_chg_limit_active_e;   	/* Max Charge cell voltage - Over Voltage violation flag 	  */
    batt_param_limit_te        		pack_param_min_cell_v_dchg_limit_active_e;  	/* Min Discharge voltage - Under Voltage violation flag       */

    batt_param_limit_te       		pack_param_max_cell_v_rechg_limit_active_e;
    batt_param_limit_te        		pack_param_min_cell_v_redchg_limit_active_e;

    batt_param_limit_te   			pack_param_cell_open_temp_limit_active;
    batt_param_limit_te        		pack_param_cell_max_chg_temp_limit_active_e;         /* Cell Max temperature - Over Temperature violation Flag    */
    batt_param_limit_te        		pack_param_cell_min_chg_temp_limit_active_e;	   	 /* Cell Min temperature - Under Temperature violation Flag   */
    batt_param_limit_te        		pack_param_cell_max_dchg_temp_limit_active_e;         /* Cell Max temperature - Over Temperature violation Flag    */
    batt_param_limit_te        		pack_param_cell_min_dchg_temp_limit_active_e;	   	 /* Cell Min temperature - Under Temperature violation Flag   */
    batt_param_limit_te        		pack_param_mfet_max_temp_limit_active_e;         /* Mosfet Max temperature - Over Temperature violation Flag  */
    batt_param_limit_te        		pack_param_mfet_min_temp_limit_active_e;	   	 /* Mosfet Min temperature - Under Temperature violation Flag */
    batt_param_limit_te        		pack_param_pack_max_temp_limit_active_e;         /* Pack Max temperature - Over Temperature violation Flag    */
    batt_param_limit_te        		pack_param_pack_min_temp_limit_active_e;	   	 /* Pack Min temperature - Under Temperature violation Flag   */
    batt_param_limit_te        		pack_param_fet_max_temp_limit_active_e;         /* Pack Max temperature - Over Temperature violation Flag    */
    batt_param_limit_te        		pack_param_fet_min_temp_limit_active_e;	   	 /* Pack Min temperature - Under Temperature violation Flag   */

    batt_param_limit_te             pack_param_pack_max_die_temp_limit_active_e ;
	batt_param_limit_te             pack_param_pack_min_die_temp_limit_active_e ;
#if 0
    batt_param_limit_te				pack_param_pack_max_hum_limit_active_e;			 /* Pack Max temperature - Over Humidity violation Flag  	  */
    batt_param_limit_te				pack_param_pack_min_hum_limit_active_e;			 /* Pack Max temperature - Under Humidity violation Flag  	  */
    batt_param_limit_te				pack_param_pack_max_amb_light_limit_active_e;	 /* Pack Max Amb light   - Over ambient light violation Flag  */
#endif


    batt_param_limit_te		   		pack_param_max_i_scd_surge_dsg_limit_active_e;

    batt_param_limit_te 	   		pack_param_cell_v_deep_dchg_limit_active_e;
    batt_param_limit_te 	   		pack_param_cell_v_bal_error_limit_active_e;

    /* @Pack Protection Flags */
    batt_param_limit_te        		pack_param_max_pack_v_chg_limit_active_e;   	/* Max Charge pack voltage - Over Voltage violation 	*/
    batt_param_limit_te        		pack_param_min_pack_v_dchg_limit_active_e;  	/* Min Discharge pack voltage - Under Voltage violation */
    batt_param_limit_te        		pack_param_max_i_chg_limit_active_e;        	/* Max current during charging violation flag 			*/
    batt_param_limit_te        		pack_param_max_i_dsg_limit_active_e;	       	/* Max current during discharging violation flag 		*/
    batt_param_limit_te        		pack_param_max_i_dsg_level1_limit_active_e;
    batt_param_limit_te	       		pack_param_afe_functionality_error_e;	   		/* AFE error - Comm Error or Malfunctioning */
    batt_param_limit_te	       		pack_param_afe_fet_malfunction_error_e;	   		/* AFE - FET alone Malfunctioning */


    batt_param_limit_te	       		pack_param_afe_malfunction_on_load_error_e;
    batt_param_limit_te	       		pack_param_scd_on_load_error_e;


    /* @Supervisory Trip Decision based on the criticality of the problem */
    batt_param_action_te       		pack_param_prev_trip_action_e;
    batt_param_action_te       		pack_param_trip_action_e;
    batt_param_all_mfet_sts_te		pack_param_all_mfet_sts_e;

    batt_param_batt_mode_te	   		pack_param_batt_mode_e;
    pack_param_pack_fault_status_te pack_param_pack_fault_status_e;
    pack_param_pack_fault_status_te pack_param_prev_pack_fault_status_e;

    /* @Warning Flags */
	BOOL					   		pack_param_max_cell_v_chg_limit_warning_b; 		/* Max Charge cell voltage - Over Voltage violation flag */
	BOOL					   		pack_param_min_cell_v_dchg_limit_warning_b; 	/* Min Discharge voltage - Under Voltage violation flag  */
	BOOL					   		pack_param_max_cell_temp_limit_warn_b; 			/* Max temperature - Over Temperature violation Flag     */
	BOOL					   		pack_param_min_cell_temp_limit_warn_b; 			/* Min temperature - Under Temperature violation Flag    */
	BOOL                            pack_param_max_pack_temp_limit_warn_b;
	BOOL                            pack_param_min_pack_temp_limit_warn_b;

	BOOL                            pack_param_max_afe_die_temp_limit_warn_b;
	BOOL                            pack_param_min_afe_die_temp_limit_warn_b;

	BOOL					   		pack_param_max_i_scd_surge_dsg_limit_warning_b;
	BOOL					   		pack_param_mosfet_max_temp_limit_warning_b; 	/* Mosfet Max temperature - Over Temperature violation Flag */

	BOOL					   		pack_param_cell_v_deep_dchg_limit_warning_b;
	BOOL					   		pack_param_cell_v_bal_error_limit_warning_b;

#if 0
	/* @Sensor Warning Flags */
	BOOL					   		pack_param_hdc2080_ul_temp_warning_b;
	BOOL					   		pack_param_hdc2080_ll_temp_warning_b;
	BOOL					   		pack_param_hdc2080_ul_hum_warning_b;
	BOOL					   		pack_param_hdc2080_ll_hum_warning_b;
	BOOL					   		pack_param_veml6090_ul_light_warning_b;
	BOOL					   		pack_param_veml6090_ll_light_warning_b;
#endif
	/* @Pack Protection Flags */
	BOOL					  	 	pack_param_max_pack_v_chg_limit_warning_b; 				/* Max Charge pack voltage - Over Voltage violation */
	BOOL					   		pack_param_min_pack_v_dchg_limit_warning_b; 			/* Min Discharge pack voltage - Under Voltage violation */
	BOOL					   		pack_param_max_i_chg_limit_warning_b; 					/* Max current during charging violation flag */
	BOOL					   		pack_param_max_i_dsg_limit_warning_b; 					/* Max current during discharging violation flag */

	batt_param_limit_te             pack_param_hvadc_read_error_limit_active_e;

	/* @Hvadc parameters */
    U16 							pack_param_hvadc_pack_vtg_1cv_u16;						/* Pack voltage measured by HVADC IC 			*/
    U16 							pack_param_hvadc_fuse_vtg_1cv_u16;						/* Fuse voltage measured by HVADC IC 	 		*/
    batt_param_limit_te        		pack_param_hvadc_pk_ov_limit_active_e;    				/* Hvadc pack OV - violation Flag  		*/
    batt_param_limit_te        		pack_param_hvadc_pk_uv_limit_active_e;    				/* Hvadc pack UV - violation Flag  		*/
    batt_param_limit_te				pack_param_hvadc_fuse_vtg_limit_active_e;  				/* Hvadc fuse voltage - violation Flag  		*/
    batt_param_limit_te 			pack_param_hvadc_mfet_vtg_limit_active_e;  				/* Hvadc mfet voltage - violation Flag  		*/
    batt_param_limit_te				pack_param_delta_hvadc_betw_pk_mfet_limit_active_e;
    U16								pack_param_delta_hvadc_betw_pk_fuse_1cV_u16;			/* delta V = packV-fuseV difference 			*/
    U16								pack_param_delta_hvadc_betw_pk_mfet_1cV_u16;			/* delta V = packV-mosfetV difference 			*/

	U16 							pack_param_delta_pk_hvadc_pk_1cV_u16;
	batt_param_limit_te             pack_param_pack_v_error_limit_active_e;


} batt_param_pack_param_tst;

#if 0
typedef struct bat_param_pack_details_tst_tag
{
	U16 bms_firmware_version_u16;
	U8  bms_product_id_u8;
	U32 batt_identification_num_u32;
}bat_param_pack_details_tst;
#endif

typedef struct __attribute__((packed)) batt_param_pack_details_tst_tag
{
	/* @Version number 					*/
	U16 bms_firmware_version_num_u16;
	U16 bms_hw_version_num_u16;
	U16 bms_pk_version_num_u16;
	U16 slave_hw_version_num_u16;
	U16 bms_protocol_version_num_u16;

	/* @Batt manufacture details 		*/
	U8 batt_supplier_u8;
	U8 batt_customer_u8;
	U32 date_of_manf_u32;

	/* @Batt specifications 			*/
	U16 pack_dod_u16;
	U16 rated_pack_cycles_u16;
	U16 nominal_voltage_u16;
	U16 rated_capacity_u16;
	U16 cell_type_u16;
	U16 total_cells_u16;
	U16 number_of_slaves_u16;

}batt_param_pack_details_tst;

typedef struct batt_param_mod_param_tst_tag
{
	U16 mod_1mv_u16;
	U16 mod_avg_1mV_u16;

	U16 mod_min_1mv_u16;
	U16 mod_max_1mv_u16;
	batt_param_cell_id_te cell_index_maxV_e;
	batt_param_cell_id_te cell_index_minV_e;

	S16 mod_min_1cc_s16;
	S16 mod_max_1cc_s16;
	batt_param_cell_id_te cell_index_minT_e;
	batt_param_cell_id_te cell_index_maxT_e;


	S16 mod_die_T_S16;

	SFP mod_Ah_1cAhr_f;
	U32 mod_soh_1mPC_u32;
	U16 mod_min_soc_1cPC_u16;
	U16 mod_max_soc_1cPC_u16;
	batt_param_cell_id_te cell_index_minsoc_e;
	batt_param_cell_id_te cell_index_maxsoc_e;

}batt_param_mod_param_tst;

typedef struct batt_param_energy_tst_tag
{
	U64 acc_chg_u64; /* Ah*/
	U64 engy_1m_ah_cwh_u64;
	//U64 engy_tp_u64; /* Wh*/
	U64 tot_cycle_count_u64;  /* Total contactor open*/
}batt_param_energy_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern batt_param_pack_param_tst       batt_param_pack_param_gst;
extern batt_param_mod_param_tst 	   batt_param_mod_param_gst[BATT_PARAM_TOTAL_SLAVES];
extern batt_param_cell_param_tst       batt_param_cell_param_gst[BATT_PARAM_TOTAL_SLAVES][BATT_PARAM_CELL_ID_MAX];
extern batt_param_temp_param_tst       batt_param_temp_param_gst[BATT_PARAM_TOTAL_SLAVES][BATT_PARAM_MAX_CT_PER_SLV];
extern batt_param_pack_details_tst	   batt_param_batt_pack_details_gst;
extern batt_param_energy_tst batt_param_energy_gst;
/** @} */

/* Public Function Prototypes **/
extern void batt_param_var_init_v(void);
extern void batt_param_init_batt_details_v(void);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_BATT_PARAM_H_ */
