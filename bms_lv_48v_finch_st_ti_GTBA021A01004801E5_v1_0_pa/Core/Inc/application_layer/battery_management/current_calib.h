/**
========================================================================================================================
@page current_calib_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	current_calib.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	16-Dec-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_CURRENT_CALIB_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_CURRENT_CALIB_H_

/* Includes for definitions */

/* Macro Definitions */
#define CURRENT_CALIB_RESOLUTION					(50U)
#define CURRENT_CALIB_SAMPLE_RESOLUTION				(20U)
#define CURRENT_CALIB_SAMPLES			 			(7U)
#define CURRENT_CALIB_MAX_CALIB_VALS				CURRENT_CALIB_SAMPLES

#define ADC_SAMPLES_COUNT					(20U)
#define MAX_CURR_CALIB_VALS					(6U)

/** @{
 * Type definition */
typedef enum current_calib_states_te_tag
{
	CURRENT_CALIB_START_ACK_STATE	= 0	,
	CURRENT_CALIB_SET_ACK_STATE			,
	CURRENT_CALIB_COMPUTE_ACK_STATE
}current_calib_states_te;

typedef struct current_calib_params_tst_tag
{
	BOOL current_calib_mfet_sts_b;
	BOOL current_calib_flg_b;
	U32  current_calib_tmr_counter_u32;
	U8	 current_calib_samples_u8;
}current_calib_params_tst;

typedef struct current_calib_equ_tst_tag
{
	S32 current_calib_gain_as32[CURRENT_CALIB_SAMPLES];
	S32 current_calib_offset_as32[CURRENT_CALIB_SAMPLES];
	S32 current_calib_avg_current_as32[CURRENT_CALIB_SAMPLES];				/* Average current */
	//S32 current_calib_raw_adc_as32[CURRENT_CALIB_SAMPLES];
	S32 current_calib_current_as32[CURRENT_CALIB_SAMPLES];
}current_calib_equ_tst;

typedef struct cur_calib_config_ctrl_tst_tag
{
	//BOOL fet_turn_off_b;
	//S32  current_1mA_as32[MAX_CURR_CALIB_VALS];
	//S16  curr_adc_val_as16[MAX_CURR_CALIB_VALS];
	//S32  gain_as32[MAX_CURR_CALIB_VALS];
	//S32  offset_as32[MAX_CURR_CALIB_VALS];
	BOOL current_calibration_done_b;
	//S16 curr_calib_reg_val_s16[MAX_CURR_CALIB_VALS];
}cur_calib_config_ctrl_tst;


/** @} */
/** @{
 *  Public Variable Definitions */
extern U8 current_calib_index_u8;

extern current_calib_params_tst		current_calib_params_gst;
extern current_calib_equ_tst 		current_calib_equ_gst;

/** @} */

/* Public Function Prototypes **/
extern void current_calib_init_v(void);
extern void current_calib_process_v(current_calib_states_te calib_state_are);
extern void current_calib_estm_v(void);
extern U8 current_calib_get_calib_raw_current_val_u8(S32 *raw_current_val_ars32);
extern U8 current_calib_get_calib_current_val_u8(S32 *current_val_ars32);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_CURRENT_CALIB_H_ */
