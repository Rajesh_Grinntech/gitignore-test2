/**
========================================================================================================================
@page bootloader_app_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bootloader_app.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	17-Jul-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_BOOTLOADER_APP_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_BOOTLOADER_APP_H_

/* Includes for definitions */
#include "common_header.h"
#include "storage_task.h"
#include "flash_drv.h"
#include "operating_system.h"

/* Macro Definitions */
#define APPLICATION_ADDRESS_1               	(U32)0x800E000

#define BOOT_LOADER_ENABLE_PGN   	0x85
#define BOOT_LOADER_DATA_PGN     	0x86
#define BOOT_LOADER_EXIT_PGN    	0x87
#define BOOT_LOADER_ERROR_PGN    	0x88
#define BOOT_LOADER_BMS_ADDRESS     (U8)(0x94)
#define BOOT_LOADER_ENABLE_CMD   	0x50

/*Extended ID Bootloader PGNs*/
#define BOOT_LOADER_ENABLE_ID   0x80
#define BOOT_LOADER_ENABLE_ACK  0x81
#define BOOT_LOADER_DATA_ID    0x82
#define BOOT_LOADER_DATA_ACK   0x83
#define BOOT_LOADER_EXIT_ID    0x84
#define BOOT_LOADER_EXIT_ACK   0x85
#define BOOT_LOADER_ERROR_ID   0x86
#define BOOT_LOADER_BMS_VERSION_NO_GET_ID    0x88

#define POLY 						0x8408

#define BMS_CAN_BOOTLOADER_ID       0xDA
#define PCAN2BMS_CAN_ID             0xFA

#define BOOT_LOADER_PRIORITY_DIAGNOSTICS  2
#define BMS_BOOTLOADER_MESSAGE_ID    CAN_APP_GET_ID(BOOT_LOADER_PRIORITY_DIAGNOSTICS,bootloader_pgn_u8,PCAN2BMS_CAN_ID,BMS_CAN_BOOTLOADER_ID)
#define BMS_BOOTLOADER_BASE_ID		CAN_APP_BASE_ID(BMS_BOOTLOADER_MESSAGE_ID)
#define BMS_BOOTLOADER_EXT_ID      	CAN_APP_EXT_ID(BMS_BOOTLOADER_MESSAGE_ID)


#define BMS_RECEIVE_BOOTLOADER_MESSAGE_ID       CAN_APP_GET_ID(PRIORITY_DIAGNOSTICS,BMS_CAN_BOOTLOADER_PGN,PCAN2BMS_CAN_ID,BMS_CAN_BOOTLOADER_ID)
#define BMS_RECEIVE_BOOTLOADER_BASE_ID		CAN_APP_BASE_ID(BMS_RECEIVE_BOOTLOADER_MESSAGE_ID)
#define BMS_RECEIVE_BOOTLOADER_EXT_ID      	CAN_APP_EXT_ID(BMS_RECEIVE_BOOTLOADER_MESSAGE_ID)


#define BOOT_LOADER_START_SECTOR_COUNT            900
#define BOOT_LOADER_END_SECTOR_COUND              976

#define CAN_GET_PGN(ID)                  (U8)(ID>>16)
/** @{
 * Type definition */
/** Boot_Loader Status **/
typedef enum bootloader_app_sts_te_tag
 {

    BOOT_LOADER_SUCCESS = 0			,
    BOOT_LOADER_FAILURE				,
    BOOT_LOADER_N0_APPLICATION		,
    BOOT_LOADDER_JUMP_FAILURE		,
    BOOT_LOADER_RUN_OLD_FIRMWARE	,
    BOOT_LOADER_UPDATE_NEW_FIRMWARE	,
    BOOT_LOADER_STATUS_UNKNOWN
 }bootloader_app_sts_te;

 typedef enum bootloader_app_btldr_comm_channels_te_tag
 {
 	BOOTLOADER_APP_BTLDR_UPDATE_IDLE = 0,
 	BOOTLOADER_APP_CAN_COMM_CHANNEL 	,
 	BOOTLOADER_APP_MODEM_COMM_CHANNEL	,
 	BOOTLOADER_APP_BLE_COMM_CHANNEL
 }bootloader_app_btldr_comm_channels_te;

 typedef enum bootloader_app_btldr_can_id_te_tag
 {
	 BOOTLOADER_APP_CAN_BTLDR_EN_ID = 0x701	,				/* BTLDR -BOOTLOADER */
	 BOOTLOADER_APP_CAN_BTLDR_EN_ACK 		,
	 BOOTLOADER_APP_CAN_BTLDR_DATA_ID		,
	 BOOTLOADER_APP_CAN_BTLDR_DATA_ACK		,
	 BOOTLOADER_APP_CAN_BTLDR_EXIT_ID		,
	 BOOTLOADER_APP_CAN_BTLDR_EXIT_ACK		,
	 BOOTLOADER_APP_CAN_BTLDR_ERROR_ID
 }bootloader_app_btldr_can_id_te;

 typedef enum bootloader_app_btldr_uart_id_te_tag
 {
 	BOOTLOADER_APP_UART_BTLDR_EN_ID = 0		,
	BOOTLOADER_APP_UART_BTLDR_EN_ACK		,
 	BOOTLOADER_APP_UART_BTLDR_DATA_ID		,
	BOOTLOADER_APP_UART_BTLDR_DATA_ACK_ID	,
 	BOOTLOADER_APP_UART_BTLDR_EXIT_ID		,
 	BOOTLOADER_APP_UART_BTLDR_ERROR_ID
 }bootloader_app_btldr_uart_id_te;

 typedef struct  boot_loader_config_tst_tag
 {
     U8 new_firmware_u8;
     U32 new_firmware_size_u32;
     U8 channel_used_for_bootloader_data_u8;
 }boot_loader_config_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern  U8 can_bootloader_cmd_received_gvlu8;
extern volatile U8 uart_modem_bootloader_start_cmd_gu8;
extern volatile U8 uart_ble_bootloader_start_cmd_gu8;

extern U8 boot_loader_spi_flash_data_au8[1024] ;
extern U8 read_boot_loader_buffer_au8[1024];
extern U8 bootloader_app_can_tx_local_buffer_au8[8];
extern U8 bootloader_app_uart_rx_local_buffer_au8[1152];
extern U8 bootloader_pgn_u8;
extern U8 bootloader_checksum_byte_au8[8];

extern U32 total_boot_loader_data_received_u32;
extern U32 boot_loader_data_rec_count_u32;
extern U32 data_reset_count_u32;
extern U32 start_boot_loader_write_address_u32;
extern U32 start_read_boot_loader_address_u32;
extern U32 boot_loader_config_flash_start_address_u32;

extern flash_drv_blocks_erase_tst boot_loader_block_erase_st;
extern storage_task_update_boot_data_tst boot_loader_store_st;
extern boot_loader_config_tst boot_loader_conf_gst,store_boot_rd_conf_st,store_boot_conf_st;
extern os_storage_queue_tst boot_storage_queue_st;
extern os_storage_queue_tst boot_uart_comm_queue_st;
extern bootloader_app_sts_te boot_loader_status_e;

extern U8 btldr_data_channel_source_u8;
extern U8 bootloader_app_os_comm_event_type_gu8;

/** @} */

/* Public Function Prototypes **/
bootloader_app_sts_te bootloader_app_jumping_to_main_app_ue(void);
bootloader_app_sts_te bootloader_app_check_new_firmware_ue(void);
bootloader_app_sts_te bootloader_app_update_new_firmware_and_run_ue(void);
bootloader_app_sts_te bootloader_app_run_old_firmware_ue(void);
void bootloader_app_jump_to_usr_app_v( U32 user_sp_aru32,U32 userstartup_aru32);
void bootloader_app_failure_v(void);
void bootloader_app_sts_can_send_v(U8 command_aru8,U8 cmd_sts_aru8);
//bootloader_internal_flash_sts_te  bootloader_app_wr_internal_flash_ue(U32 bootloader_data_size_aru32);
void bootloader_app_data_erase_v(void);
extern void bootloader_app_can_rx_task_v(U8 pgn_aru8,U8 data_length_aru8,U8 *can_rx_data_arpu8);
extern void bootloader_app_can_tx_task_v(U32 id_aru32,U8 data_length_aru8,U8 *can_data_arpu8);
extern void bootloader_app_uart_modem_rx_task_v(U16 message_id_aru16, U8 *data_array_arpu8, U16 data_length_aru16);
extern void bootloader_app_uart_ble_rx_task_v(U16 message_id_aru16, U8 *data_array_arpu8, U16 data_length_aru16);
extern U16 bootloader_app_crc16_generate_u16(U8 *data_arpu8, U16 length_aru16);
void bootloader_app_checksum_msg_v(U32 can_std_id_aru32,U8 *data_buf_arpu8, U8 data_cnt_aru8);
void bootloader_app_cmd_send_v(U32 can_std_id_aru32, U8 command_aru8, U8 command_status_aru8);

extern void boot_loader_app_vector_remap_v(void);
#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_BOOTLOADER_APP_H_ */
