/*
========================================================================================================================
Title:      *
Filename:   CellBalApp.h
MCU:        S32K Family
Compiler:   ARM GCC Compiler
Author:     Bharath Raj
------------------------------------------------------------------------------------------------------------------------
Description:
------------------------------------------------------------------------------------------------------------------------
MISRA Exceptions:
------------------------------------------------------------------------------------------------------------------------
Notes:
------------------------------------------------------------------------------------------------------------------------
Rev_by      Date        Description
------      ----        -----------
#1_TL#      20-Aug-2019  	Created
------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------
DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.
========================================================================================================================
*/

#ifndef CELL_BAL_APP_H_
#define CELL_BAL_APP_H_

/** Includes for definitions **/
#include "common_header.h"

/** Definitions **/



/** Public Variable Declarations **/
typedef enum CELL_BAL_CELL_TAG_te_tag
{
	BAL_CELL_1 = 0,
	BAL_CELL_2,
	BAL_CELL_3,
	BAL_CELL_4,
	BAL_CELL_5,

	BAL_MAX_CELL
}CELL_BAL_CELL_TAG_te;

typedef enum CELL_BAL_MODE_te_tag
{
	CELL_BAL_START = 0,
	CELL_BAL_FORCE_STOP = 1,
}CELL_BAL_MODE_te;

typedef enum cell_bal_grp_te_tag
{
	BAL_GROUP_1 = 1,
	BAL_GROUP_2,
	BAL_GROUP_3,
	BAL_GROUP_4,
	BAL_GROUP_5,
	BAL_GROUP_6,
	BAL_GROUP_7,
	//BAL_GROUP_8,

	BAL_MAX_GROUP
}cell_bal_grp_te;

typedef struct cell_bal_config_status_tst_tag
{
	BIT bal_completed_b;  /* Balancing completed or not! */
	BIT bal_enabled_b;    /* To turn ON and OFF balancing based on the algorithm */
	U16 bal_register_u16; /* Holds the balancing status of each cell from [0:13] */
	U8 balancing_delta_v_1mV_threshold_u8;
	U16 balancing_cell_voltage_1mV_threshold_u16;
}cell_bal_config_status_tst;

extern cell_bal_config_status_tst cell_bal_config_status_gst;


extern BOOL cb_switch_req_b ;
extern U8 cb_switch_counter_u8 ;
extern cell_bal_grp_te cell_bal_grp_e;

/** Public Function Prototypes **/
extern void cell_bal_app_param_config_v(void);
extern void cell_bal_app_bal_cntrl_algorithm_v(void);
extern U8 cell_bal_app_fet_cntrl_u8();
#endif /* BMS_CELLBALAPP_H_ */
