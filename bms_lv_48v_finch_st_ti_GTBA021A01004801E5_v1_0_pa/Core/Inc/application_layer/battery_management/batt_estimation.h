/**
========================================================================================================================
@page batt_estimation_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	batt_estimation.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	26-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_BATT_ESTIMATION_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_BATT_ESTIMATION_H_

/* Includes for definitions */
/* @{
 * Macro definition */
#define BATT_EST_FLASH_LOG_ADDR 	(0U)

typedef struct batt_estimation_general_param_tst_tag
{
	BOOL current_capacity_inital_estimated_b;
	SFP  voltage_to_AHr_convertion_factor_f;
	SFP  capacity_to_soc_f;
	U16  ocv_min_soc_1mV_u16;
	U16  ocv_max_soc_1mV_u16;
	U16  min_charge_capacity_capacity_u16;
	U16  degradation_health_per_cycle_u16;

}batt_estimation_general_param_tst;

typedef struct batt_estimation_flag_logs_tst_tag
{
	BOOL data_available_b;
	U16 cycle_count_u16;
	U32 soh_1mPC_u32;
}batt_estimation_flag_logs_tst;
/* @} */

/* @{
 * Type definition */

/* @} */

/* @{
 *  Public Variable Definitions */

/* @} */

/* Public Function Prototypes **/
extern void batt_estimation_init_parameters_v(void);
extern void batt_estimation_estimate_all_parameters_v(void);
#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_BATT_ESTIMATION_H_ */
