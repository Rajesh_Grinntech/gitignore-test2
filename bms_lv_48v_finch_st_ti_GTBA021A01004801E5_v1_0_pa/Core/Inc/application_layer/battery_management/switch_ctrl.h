/**
========================================================================================================================
@page switch_ctrl_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	switch_ctrl.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	23-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_SWITCH_CTRL_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_SWITCH_CTRL_H_

/* Includes for definitions */
#include "common_header.h"
//#include "gpio_app.h"

/* Macro Definitions */
#define SWITCH_CTRL_MFET_TMR_DELAY			(10U)
#if 0
#define SWITCH_CTRL_MAIN_CHG_MFET_EN	gpio_app_set_digital_output_v(UC_CFET_AND_PTD12, GPIO_DO_ON)
#define SWITCH_CTRL_MAIN_CHG_MFET_DIS	gpio_app_set_digital_output_v(UC_CFET_AND_PTD12, GPIO_DO_OFF)
#define SWITCH_CTRL_MAIN_DCHG_MFET_EN	gpio_app_set_digital_output_v(UC_DFET_AND_PTD11, GPIO_DO_ON)
#define SWITCH_CTRL_MAIN_DCHG_MFET_DIS	gpio_app_set_digital_output_v(UC_DFET_AND_PTD11, GPIO_DO_OFF)

#define SWITCH_CTRL_PRE_CHG_MFET_EN		gpio_app_set_digital_output_v(UC_PCHG_ISO_PTD9, GPIO_DO_ON)
#define SWITCH_CTRL_PRE_CHG_MFET_DIS	gpio_app_set_digital_output_v(UC_PCHG_ISO_PTD9, GPIO_DO_OFF)
#define SWITCH_CTRL_PRE_DCHG_MFET_EN	gpio_app_set_digital_output_v(UC_PDSG_ISO_PTC13, GPIO_DO_ON)
#define SWITCH_CTRL_PRE_DCHG_MFET_DIS	gpio_app_set_digital_output_v(UC_PDSG_ISO_PTC13, GPIO_DO_OFF)

#define SWITCH_CTRL_MOS_PWR_ON          gpio_app_set_digital_output_v(UC_EN_QDRV_PSW_PTC12, GPIO_DO_ON)
#define SWITCH_CTRL_MOS_PWR_OFF         gpio_app_set_digital_output_v(UC_EN_QDRV_PSW_PTC12, GPIO_DO_OFF)
#define SWITCH_CTRL_MOS_ISO_EN			gpio_app_set_digital_output_v(UC_EN_ISO_PTD10, GPIO_DO_OFF)
#define SWITCH_CTRL_MOS_ISO_DIS			gpio_app_set_digital_output_v(UC_EN_ISO_PTD10, GPIO_DO_ON)
#endif
/** @{
 * Type definition */
typedef enum switch_ctrl_mfet_type_te_tag
{
	SWITCH_CTRL_MAIN_MOSFET = 1,
	SWITCH_CTRL_PRE_MOSFET
}switch_ctrl_mfet_type_te;

typedef enum switch_ctrl_mfet_status_te_tag
{
	SWITCH_CTRL_BOTH_MAIN_MFET_CLOSED = 1,
	SWITCH_CTRL_BOTH_MAIN_MFET_OPENED    ,
	SWITCH_CTRL_ONLY_MAIN_CHG_MFET_CLOSED,
	SWITCH_CTRL_ONLY_MAIN_DCHG_MFET_CLOSED,
}switch_ctrl_mfet_status_te;

typedef enum switch_ctrl_mfet_actions_te_tag
{
	SWITCH_CTRL_ACTION_BOTH_ON = 0	,
	SWITCH_CTRL_ACTION_ONLY_CHG_ON  ,
	SWITCH_CTRL_ACTION_ONLY_DSG_ON	,
	SWITCH_CTRL_ACTION_BOTH_OFF		,
}switch_ctrl_mfet_actions_te;



/** @} */
/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Prototypes **/
extern void switch_ctrl_mfet_ctrl_v(switch_ctrl_mfet_type_te mosfet_type_are, U8 mode_aru8);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_SWITCH_CTRL_H_ */
