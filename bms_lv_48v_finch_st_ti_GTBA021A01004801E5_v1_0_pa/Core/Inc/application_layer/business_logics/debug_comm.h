/**
========================================================================================================================
@page debug_comm_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	debug_comm.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	28-May-2020
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BUSINESS_LOGICS_DEBUG_COMM_H_
#define APPLICATION_LAYER_BUSINESS_LOGICS_DEBUG_COMM_H_

/* Includes for definitions */
#include "common_header.h"
//#include "common_var.h"
#include "can_commtask.h"
//#include "gpio_app.h"
#include "can_app.h"
//#include "uart_app.h"
#include "batt_param.h"

/* Macro Definitions */

/* This should not be changed for any project.*/
#define DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD  		8
#define DEBUG_COMM_MAX_CELLS_PER_MSG_FRAME_ALWD  		4   /*Used in Cell voltage*/
#define DEBUG_COMM_MAX_TEMP_SENS_PER_MSG_FRAME_ALWD  	4	/*Used in Cell temperature*/

/* Project- wise Configuration MACRO starts - Configured for ROBIN*/

/* Battery Pack Params*/
#define DEBUG_COMM_TOTAL_SLAVES								1
#define DEBUG_COMM_TOTAL_CELLS_PER_SLV						16
#define DEBUG_COMM_TOTAL_TMS_CHANNELS						8

/* Switch between Hard-code and real-time run*/
#define DEBUG_COMM_HARD_CODE								COM_HDR_DISABLED

/* Group functions */
#define DEBUG_COMM_SEND_HVADC_BENDER						COM_HDR_ENABLED
#define DEBUG_COMM_SEND_RTC_AMB_LIGHT						COM_HDR_ENABLED
#define DEBUG_COMM_SEND_MOISTURE_IMU						COM_HDR_ENABLED

/* Seperate functions*/
#define DEBUG_COMM_SEND_CELL_VTG							COM_HDR_ENABLED
#define DEBUG_COMM_SEND_CELL_TEMP							COM_HDR_ENABLED
#define DEBUG_COMM_SEND_TMS									COM_HDR_DISABLED
#define DEBUG_COMM_SEND_AFE_SLV_FLT_STS						COM_HDR_DISABLED
#define DEBUG_COMM_SEND_AFE_CB_STS							COM_HDR_ENABLED
#define DEBUG_COMM_SEND_SYS_FLT_STS							COM_HDR_ENABLED
#define DEBUG_COMM_SEND_SYS_RUN_TIME_FLT_STS				COM_HDR_ENABLED
#define DEBUG_COMM_SYS_PRV_RUN_TIME_FLT_STS					COM_HDR_ENABLED
#define DEBUG_COMM_SEND_MCU_ADC								COM_HDR_ENABLED
#define DEBUG_COMM_SEND_CURR_SBC							COM_HDR_ENABLED
#define DEBUG_COMM_SEND_MAX_MIN_CPARAMS						COM_HDR_ENABLED
#define DEBUG_COMM_SEND_AH_CYC_CNT							COM_HDR_ENABLED
#define DEBUG_COMM_SEND_FLASH								COM_HDR_DISABLED

#define DEBUG_COMM_SLEEP_WAKE								COM_HDR_ENABLED
#define DEBUG_COMM_CURR_CALIB								COM_HDR_ENABLED
#define DEBUG_COMM_BOOT_LOADER								COM_HDR_ENABLED
#define DEBUG_COMM_FLASH_DATA_RETRIEVAL						COM_HDR_DISABLED

#if DEBUG_COMM_SEND_HVADC_BENDER
#define DEBUG_COMM_SEND_HVADC								COM_HDR_ENABLED
#define DEBUG_COMM_SEND_BENDER								COM_HDR_DISABLED
#endif

#if DEBUG_COMM_SEND_CURR_SBC
#define DEBUG_COMM_SEND_CURRENT								COM_HDR_DISABLED
#define DEBUG_COMM_SEND_SBC									COM_HDR_DISABLED
#endif

#if DEBUG_COMM_SEND_RTC_AMB_LIGHT
#define DEBUG_COMM_SEND_RTC									COM_HDR_ENABLED
#define DEBUG_COMM_SEND_AMB_LIGHT							COM_HDR_DISABLED
#endif

#if DEBUG_COMM_SEND_MOISTURE_IMU
#define DEBUG_COMM_SEND_MOISTURE							COM_HDR_DISABLED
#define DEBUG_COMM_SEND_IMU									COM_HDR_DISABLED
#endif

#if DEBUG_COMM_SEND_HVADC
#define DEBUG_COMM_HVADC_PCON_VTG							COM_HDR_DISABLED
#define DEBUG_COMM_HVADC_MFT_VTG							COM_HDR_DISABLED
#define DEBUG_COMM_HVADC_LD_VTG								COM_HDR_ENABLED
#endif

#if DEBUG_COMM_SEND_MCU_ADC
#define DEBUG_COMM_MCU_ADC_A								COM_HDR_DISABLED
#define DEBUG_COMM_MCU_ADC_B								COM_HDR_ENABLED
#endif

#if DEBUG_COMM_SEND_RTC
#define DEBUG_COMM_RTC_EXT									COM_HDR_DISABLED
#define DEBUG_COMM_RTC_MCU									COM_HDR_ENABLED
#endif

/* ENABLE/ DISABLE Configuration ends*/
#define DEBUG_COMM_BIN_LEN		20
#define DEBUG_COMM_VIN_LEN		17
#define DEBUG_COMM_MAC_LEN		6

#define DEBUG_COMM_BMS_TO_GUI_PGN 				0x800FADA
#define DEBUG_COMM_GET_PGN(x)					((x & 0xFF0000) >> COM_HDR_SHIFT_16_BITS)
#define DEBUG_COMM_MAKE_PGN(x)					(DEBUG_COMM_BMS_TO_GUI_PGN | ((x << COM_HDR_SHIFT_16_BITS) & 0xFF0000))
/* Configuration MACRO ends*/

#define DEBUG_COMM_TOTAL_CELLS				(DEBUG_COMM_TOTAL_CELLS_PER_SLV * DEBUG_COMM_TOTAL_SLAVES)
#define DEBUG_COMM_TOTAL_CELL_TEMP_SENS		BATT_PARAM_TOT_CELL_TEMP_SENS
#define DEBUG_COMM_TOTAL_NTC_TEMP_SENS		BATT_PARAM_TOT_NTC_TEMP_SENS

#define DEBUG_COMM_GUI_ADDR					0xFA
#define DEBUG_COMM_BMS_ADDR					0xDA
#define DEBUG_COMM_MULTIPACKET_PRIORITY 	2

#define DEBUG_COMM_MULTIPACKET_DATA_PGN 	0xEB
#define DEBUG_COMM_MULTIPACKET_GET_ID      	CAN_APP_GET_ID(DEBUG_COMM_MULTIPACKET_PRIORITY,DEBUG_COMM_MULTIPACKET_DATA_PGN,DEBUG_COMM_GUI_ADDR,DEBUG_COMM_BMS_ADDR)
#define DEBUG_COMM_MULTIPACKET_BASE_ID		CAN_APP_BASE_ID(DEBUG_COMM_MULTIPACKET_GET_ID)
#define DEBUG_COMM_MULTIPACKET_EXT_ID		CAN_APP_EXT_ID(DEBUG_COMM_MULTIPACKET_GET_ID)

#define DEBUG_COMM_MULTIPACKET_TIMEOUT		250
#define DEBUG_COMM_MULTIPACKET_DATA_FRAME	(DEBUG_COMM_MULTIPACKET_BASE_ID,DEBUG_COMM_MULTIPACKET_EXT_ID, 			\
												CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR, 								\
												sizeof(debug_comm_send_multipacket_data_gst.multipacket_buff_au8), 	\
												debug_comm_send_multipacket_data_gst.multipacket_buff_au8)

#define DEBUG_COMM_RTS_GET_ID            	CAN_APP_GET_ID(DEBUG_COMM_MULTIPACKET_PRIORITY,DEBUG_COMM_RTS_CTS_EOF_PGN,DEBUG_COMM_GUI_ADDR,DEBUG_COMM_BMS_ADDR)
#define DEBUG_COMM_RTS_BASE_ID    	     	CAN_APP_BASE_ID(DEBUG_COMM_RTS_GET_ID)
#define DEBUG_COMM_RTS_EXT_ID            	CAN_APP_EXT_ID(DEBUG_COMM_RTS_GET_ID)
#define DEBUG_COMM_RTS_SIZE              	(U8)sizeof(debug_comm_rts_tst)
#define DEBUG_COMM_RTS_TIMEOUT           	250

#define DEBUG_COMM_RTS_FRAME             	(DEBUG_COMM_RTS_BASE_ID,DEBUG_COMM_RTS_EXT_ID, \
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR,		   \
											DEBUG_COMM_RTS_SIZE,						   \
											(U8*)&debug_comm_rts_st)

#define DEBUG_COMM_GET_ID(pgn)				CAN_APP_GET_ID(DEBUG_COMM_MULTIPACKET_PRIORITY,pgn,DEBUG_COMM_GUI_ADDR,DEBUG_COMM_BMS_ADDR)
#define DEBUG_COMM_BASE_ID(pgn)				CAN_APP_BASE_ID(DEBUG_COMM_MULTIPACKET_GET_ID(pgn))
#define DEBUG_COMM_EXT_ID(pgn)				CAN_APP_EXT_ID(DEBUG_COMM_MULTIPACKET_GET_ID(pgn))

#define DEBUG_COMM_FRAME(pgn)				(DEBUG_COMM_BASE_ID(pgn),DEBUG_COMM_EXT_ID(pgn), 	\
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR, 				\
											(U8)sizeof(debug_comm_tx_msg_st.data_au8),				\
											(U8*)&debug_comm_tx_msg_st.data_au8[0])


#define DEBUG_COMM_ONE_TIME_INFO_MSG_SIZE	(U8)sizeof(debug_comm_basic_batt_info_tst)
#define DEBUG_COMM_BIN_MSG_SIZE				(U8)sizeof(common_var_bms_id_log_gst.bin_au8)
#define DEBUG_COMM_VIN_MSG_SIZE				(U8)sizeof(common_var_bms_id_log_gst.vin_au8)
#define DEBUG_COMM_BATT_PARAM_STS_MSG_SIZE	(U8)sizeof(debug_comm_batt_sts_params_tst)
#define DEBUG_COMM_FLASH_RETRIVAL_MSG_SIZE  0//(U8)sizeof(bms_debug_battery_data_log_tst)
#define AXIS 								3 	/* No IMU driver*/

/** @{
 * Type definition */

typedef enum debug_comm_pgn_list_te_tag
{
	DEBUG_COMM_START_STOP_PGN			= 0x40,
	DEBUG_COMM_AUTH_PGN					= 0xBA,
	DEBUG_COMM_BATT_STS_PARAM_PGN		= 0X28,

	DEBUG_COMM_ONE_SEC_PARAM_PGN		= 0xAA,
	DEBUG_COMM_ONE_TIME_INFO_PGN		= 0x6A,


	DEBUG_COMM_SET_RTC_PGN				= 0x41,
	DEBUG_COMM_AFE_CB_CTRL1_PGN			= 0x42,
	DEBUG_COMM_AFE_CB_CTRL2_PGN			= 0x43,
	DEBUG_COMM_AFE_CB_CTRL3_PGN			= 0x44,
	DEBUG_COMM_SET_WARN_CUTOFF_PGN		= 0x4C,

	DEBUG_COMM_EXT_FLASH_ERASE_PGN		= 0X4D,

	DEBUG_COMM_SET_BIN1_PGN				= 0x45,			/* TODO: Planned to send BIN through Multipacket using this PGN */
	DEBUG_COMM_SET_BIN2_PGN				= 0x46,
	DEBUG_COMM_SET_BIN3_PGN				= 0x47,
	DEBUG_COMM_SET_VIN1_PGN				= 0x48,			/* TODO: Planned to send VIN through Multipacket using this PGN */
	DEBUG_COMM_SET_VIN2_PGN				= 0x49,
	DEBUG_COMM_SET_VIN3_PGN				= 0x4A,
	DEBUG_COMM_SET_MAC_PGN				= 0x4B,
	DEBUG_COMM_SYS_RESET_PGN			= 0x4F,
	DEBUG_COMM_GET_BIN_VIN_MAC_PGN		= 0x60,

	DEBUG_COMM_BATT_STS_PGN				= 0x51,
	DEBUG_COMM_CURRENT_CALIB_ST_PGN		= 0x53,
	DEBUG_COMM_CURRENT_CALIB_SET_PGN	= 0x54,
	DEBUG_COMM_CURRENT_CALIB_COMPUTE_PGN= 0x55,
	DEBUG_COMM_SLEEP_WAKE_PGN			= 0x4E,
	DEBUG_COMM_SEND_BIN_PGN				= 0x61,
	DEBUG_COMM_SEND_VIN_PGN				= 0x62,
	DEBUG_COMM_FLASH_DATA_RETRIEVAL_PGN = 0x64,
	DEBUG_COMM_BOOT_START_PGN			= 0x80,
	DEBUG_COMM_BOOT_START_ACK_PGN			  ,
	DEBUG_COMM_BOOT_DATA_PGN			      ,
	DEBUG_COMM_BOOT_DATA_ACK_PGN			  ,
	DEBUG_COMM_BOOT_EOF_PGN					  ,
	DEBUG_COMM_BOOT_EOF_ACK_PGN				  ,
	DEBUG_COMM_BOOT_ERROR_PGN				  ,
	DEBUG_COMM_BOOT_V_NO_PGN			= 0x88,

	DEBUG_COMM_RTS_CTS_EOF_PGN			=0xEC

}debug_comm_pgn_list_te;

#if 1   /* Delete this messages list once pgn list gets confirmed */
typedef enum debug_comm_messages_list_te_tag
{
	DBEUG_COMM_START_STOP_RES 			= 0x8AADAFA,
	DEBUG_COMM_CELL_VOLTAGE_RES			= 0x801FADA, /* 7 Msgs 0x01 to 0x07*/
	DEBUG_COMM_CELL_TEMP_RES			= 0x808FADA, /* 9 Msgs 0x08 to 0x10*/
	DEBUG_COMM_HVADC_BEND_MEAS_RES		= 0x811FADA,
	DEBUG_COMM_TMS_TEMP_RES				= 0x812FADA,
	DEBUG_COMM_IMU_RES					= 0x814FADA,
	DEBUG_COMM_MOSITURE_RES				= 0x818FADA,
	DEBUG_COMM_AFE_SLV_CELL_FLT_RES		= 0x819FADA,
	DEBUG_COMM_AFE_SLV_THERM_FLT_RES	= 0x81CFADA,
	DEBUG_COMM_SYS_FLT_STS_RES			= 0x81DFADA,
	DEBUG_COMM_RTC_AMB_LGHT_RES			= 0x81FFADA,
	DEBUG_COMM_AFE_CB_STS_RES			= 0x820FADA,
	DEBUG_COMM_MCU_ADC_A_RES			= 0x823FADA,
	DEBUG_COMM_MCU_ADC_B_RES			= 0x826FADA, //mosfet and pack temperarure
	DEBUG_COMM_CURR_SBC_RES				= 0x827FADA,
	DEBUG_COMM_BATT_STS_PARAMS_RES		= 0x828FADA,
	DEBUG_COMM_MIN_MAX_CPARAMS_RES		= 0x82AFADA,
	DEBUG_COMM_AH_CYC_CNT_CPARAMS_RES	= 0x835FADA,
	DEBUG_COMM_STS_PARAMS_RES			= 0x836FADA,

	DEBUG_COMM_START_STOP_CMD 			= 0x40,
	DEBUG_COMM_SET_RTC_CMD				= 0x841DAFA,

	DEBUG_COMM_AFE_CB_CTRL1_CMD			= 0x842DAFA,
	DEBUG_COMM_AFE_CB_CTRL2_CMD			= 0x843DAFA,
	DEBUG_COMM_AFE_CB_CTRL3_CMD			= 0x844DAFA,

	DEBUG_COMM_SET_BIN1_CMD				= 0x845DAFA,
	DEBUG_COMM_SET_BIN2_CMD				= 0x846DAFA,
	DEBUG_COMM_SET_BIN3_CMD				= 0x847DAFA,
	DEBUG_COMM_SET_VIN1_CMD				= 0x848DAFA,
	DEBUG_COMM_SET_VIN2_CMD				= 0x849DAFA,
	DEBUG_COMM_SET_VIN3_CMD				= 0x84ADAFA,
	DEBUG_COMM_SET_MAC_CMD				= 0x84BDAFA,

	/*
	DEBUG_COMM_SET_WARN_CUTOFF_CV_CMD	= 0x84CDAFA,
	DEBUG_COMM_SET_WARN_CUTOFF_PV_CMD	= 0x84DDAFA,
	DEBUG_COMM_SET_WARN_CUTOFF_CT_CMD	= 0x84EDAFA,
	DEBUG_COMM_SET_WARN_CUTOFF_PT_CMD	= 0x84FDAFA,
	DEBUG_COMM_SET_WARN_CUTOFF_MT_CMD	= 0x850DAFA,
	DEBUG_COMM_SET_WARN_CUTOFF_PC_CMD	= 0x851DAFA,*/
	DEBUG_COMM_SET_WARN_CUTOFF_CMD		= 0x84CDAFA,

	DEBUG_COMM_CHIP_ERASE_CMD				= 0x84DFADA,
	DEBUG_COMM_BATT_STS_RES					= 0x851FADA,
	DEBUG_COMM_FLASH_ERASE_PERCENT_STS_RES	= 0x852FADA,
	DEBUG_COMM_SLEEP_WAKE_CMD				= 0x84EDAFA,

	DEBUG_COMM_BIN_VIN_MAC_RQT			= 0x860DAFA,
	DEBUG_COMM_BIN_RES					= 0x861FADA,
	DEBUG_COMM_VIN_RES					= 0x862FADA,
	DEBUG_COMM_MAC_RES					= 0x863FADA,

	DEBUG_COMM_GET_FLASH_DATA_RQT		= 0x864DAFA,
	DEBUG_COMM_GET_FLASH_DATA_RES		= 0x864FADA,

	DEBUG_COMM_BASIC_BATT_INFO_RQT		= 0x86ADAFA,
	DEBUG_COMM_BASIC_BATT_INFO_RES		= 0x86BFADA,

	DEBUG_COMM_GET_WARN_CUTOFF_RQT		= 0x86CDAFA,
	DEBUG_COMM_GET_WARN_CUTOFF_CV_RES	= 0x86DFADA,
	DEBUG_COMM_GET_WARN_CUTOFF_PV_RES	= 0x86EFADA,
	DEBUG_COMM_GET_WARN_CUTOFF_CT_RES	= 0x86FFADA,
	DEBUG_COMM_GET_WARN_CUTOFF_PT_RES	= 0x870FADA,
	DEBUG_COMM_GET_WARN_CUTOFF_MT_RES	= 0x871FADA,
	DEBUG_COMM_GET_WARN_CUTOFF_PC_RES	= 0x872FADA,

}debug_comm_messages_list_te;
#endif

typedef enum debug_comm_tx_states_te_tag
{
	DEBUG_COMM_START_ACK_STATE			= 0		,
	DEBUG_COMM_SEND_BATT_STS_PARAM_STATE		,
	DEBUG_COMM_SEND_ONE_TIME_INFO_STATE			,
	DEBUG_COMM_SEND_ONE_SEC_PARAM_STATE			,
	DEBUG_COMM_BIN_SEND_STATE					,
	DEBUG_COMM_VIN_SEND_STATE					,
	DEBUG_COMM_MAC_SEND_STATE					,
	DEBUG_COMM_BIN_ACK_SEND_STATE				,
	DEBUG_COMM_VIN_ACK_SEND_STATE				,
	DEBUG_COMM_MAC_ACK_SEND_STATE				,
	DEBUG_COMM_EXT_FLASH_DATA_RETRIVAL_STATE	,
	DEBUG_COMM_EXT_FLASH_ERASE_PERCENT_STS_SEND_STATE,
	DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE	,
	DEBUG_COMM_CURRENT_CALIB_ST_ACK_STATE			,
	DEBUG_COMM_CURRENT_CALIB_SET_ACK_STATE		,
	DEBUG_COMM_CURRENT_CALIB_COMPUTE_ACK_STATE	,
	DEBUG_COMM_BOOT_START_ACK_STATE				,
	DEBUG_COMM_BOOT_DATA_ACK_STATE				,
	DEBUG_COMM_BOOT_EOF_ACK_STATE				,
	DEBUG_COMM_MULTIPACK_UPDATE_STATE			,
	DEBUG_COMM_MULTIPACK_SEND_STATE				,
	DEBUG_COMM_MULTIPACK_EOF_MSG_STATE			,

}debug_comm_tx_states_te;

typedef enum debug_comm_bin_msg_te_tag
{
	BIN_MSG_1 	= 0,
	BIN_MSG_2 	= 8,
	BIN_MSG_3  	= 16
}debug_comm_bin_msg_te;

typedef enum debug_comm_vin_msg_te_tag
{
	VIN_MSG_1  = 0,
	VIN_MSG_2  = 8,
	VIN_MSG_3  = 16
}debug_comm_vin_msg_te;

typedef enum debug_comm_warn_cutoffs_tag
{
	/* Cell voltage levels */
	DEBUG_COMM_CELL_OV_WARN			= 1	,
	DEBUG_COMM_CELL_UV_WARN				,
	DEBUG_COMM_CELL_OV_CUTOFF			,
	DEBUG_COMM_CELL_UV_CUTOFF			,
	DEBUG_COMM_CELL_DEEP_DSG_WARN		,
	DEBUG_COMM_RELEASE_CELL_UV_CUTOFF	,
	DEBUG_COMM_RELEASE_CELL_OV_CUTOFF	,
	DEBUG_COMM_CELL_DEEP_DSG_CUTOFF		,

	/* Pack voltage levels */
	DEBUG_COMM_PACK_OV_WARN				,
	DEBUG_COMM_PACK_UV_WARN				,
	DEBUG_COMM_PACK_OV_CUTOFF			,
	DEBUG_COMM_PACK_UV_CUTOFF			,
	DEBUG_COMM_RELEASE_PACK_OV_CUTOFF	,
	DEBUG_COMM_RELEASE_PACK_UV_CUTOFF	,

	/* Cell Temperature levels */
	DEBUG_COMM_CELL_OT_WARN				,
	DEBUG_COMM_CELL_UT_WARN				,
	DEBUG_COMM_CELL_OT_CUTOFF			,
	DEBUG_COMM_CELL_UT_CUTOFF			,
	DEBUG_COMM_RELEASE_CELL_OT_CUTOFF	,
	DEBUG_COMM_RELEASE_CELL_UT_CUTOFF	,

	/* Ambient Temperature levels */
	DEBUG_COMM_AMB_OT_WARN				,
	DEBUG_COMM_AMB_UT_WARN				,
	DEBUG_COMM_AMB_OT_CUTOFF			,
	DEBUG_COMM_AMB_UT_CUTOFF			,
	DEBUG_COMM_RELEASE_AMB_OT_CUTOFF	,
	DEBUG_COMM_RELEASE_AMB_UT_CUTOFF	,

	/* Mosfet Temperature levels */
	DEBUG_COMM_MFET_OT_WARN				,
	DEBUG_COMM_MFET_UT_WARN				,
	DEBUG_COMMM_MFET_OT_CUTOFF			,
	DEBUG_COMM_MFET_UT_CUTOFF			,
	DEBUG_COMM_RELEASE_MFET_OT_CUTOFF	,
	DEBUG_COMM_RELEASE_MFET_UT_CUTOFF	,

	/* Pack current levels */
	DEBUG_COMM_PACK_OVER_CHG_WARN		,
	DEBUG_COMM_PACK_OVER_DSG_WARN		,
	DEBUG_COMM_PACK_OVER_CHG_CUTOFF		,
	DEBUG_COMM_PACK_OVER_DSG_CUTOFF		,
	DEBUG_COMM_PACK_SURGE_CURRENT_CUTOFF,

	/* pack Humidity levels */
	DEBUG_COMM_PACK_OH_WARN				,
	DEBUG_COMM_PACK_UH_WARN				,
	DEBUG_COMM_PACK_OH_CUTOFF			,
	DEBUG_COMM_PACK_UH_CUTOFF

}debug_comm_bms_warn_cutoff_list_te;

typedef struct
{
	U16 pack_vtg_1cv_u16;
	U16 fuse_vtg_1cv_u16;
	U16 pcon_vtg_1cv_u16;
	U16 mfet_vtg_1cv_u16;
	U16 ld_vtg_1cv_u16;
	U16 afe_pack_vtg_1cv_u16;
} debug_comm_hvadc_param_tst;

typedef struct debug_comm_slv_flt_sts_tst_tag
{
	U16 cell_fault_sts_au16[DEBUG_COMM_TOTAL_SLAVES];
	U64 therms_fault_sts_u64;
} debug_comm_slv_flt_sts_tst;

typedef struct debug_comm_mcu_adc_param_tst_tag
{
	U16 pchg_vtg_1cv_u16;
	U16 pcon_vtg_1cv_u16;
	U16 ncon_vtg_1cv_u16;
	U16 sbc_mux_out_u16;
	U16 vbat_sens_u16;
	U32 current_u32;
} debug_comm_mcu_adc_param_tst;

typedef enum debug_comm_afe_cb_slv_tag
{
	DEBUG_COMM_CB_SLV_1_4 = 0,
	DEBUG_COMM_CB_SLV_5_8 = 4,
	DEBUG_COMM_CB_SLV_9 = 8,
	DEBUG_COMM_CB_SLV_ALL = 0xF
}debug_comm_afe_cb_slv_te;

typedef enum debug_comm_boot_te_tag
{
	DEBUG_COMM_BOOT_START = 0x80	,
	DEBUG_COMM_BOOT_START_ACK		,
	DEBUG_COMM_BOOT_DATA			,
	DEBUG_COMM_BOOT_DATA_ACK		,
	DEBUG_COMM_BOOT_EOF				,
	DEBUG_COMM_BOOT_EOF_ACK			,
	DEBUG_COMM_BOOT_ERROR			,
	DEBUG_COMM_BMS_BOOT_ERROR
}debug_comm_boot_te;

typedef enum debug_comm_multipacket_controlbytes_te_tag
{
	DEBUG_COMM_RTS_CONTROLBYTE = 0x10	,
	DEBUG_COMM_CTS_CONTROLBYTE = 0x11	,
	DEBUG_COMM_EOF_CONTROLBYTE = 0x13

}debug_comm_multipacket_controlbytes_te;

typedef struct debug_comm_cb_sts_tst_tag
{
	U16 cb_sts_au16[DEBUG_COMM_TOTAL_SLAVES];
	U16 cb_ctrl_au16[DEBUG_COMM_TOTAL_SLAVES];
} debug_comm_cb_sts_tst;

typedef struct debug_comm_basic_batt_info_tst_tag
{
	/* @Version number 					*/
	U16 bms_hw_version_num_u16;
	U16 bms_pk_version_num_u16;
	U16 slave_hw_version_num_u16;
	U16 bms_firmware_version_num_u16;
	U16 bms_protocol_version_num_u16;

	/* @Batt manufacture details 		*/
	U8 batt_supplier_u8;
	U8 batt_customer_u8;
	U32 date_of_manf_u32;

	/* @Batt specifications 			*/
	U16 pack_dod_u16;
	U16 rated_pack_cycles_u16;
	U16 nominal_voltage_u16;
	U16 rated_capacity_u16;
	U16 cell_type_u16;
	U16 total_cells_u16;
	U16 number_of_slaves_u16;

	/* @Cell voltage details 			*/
	U16 cell_ov_warning_u16;
	U16 cell_uv_warning_u16;
	U16 cell_ov_threshold_u16;
	U16 cell_uv_threshold_u16;
	U16 release_cell_ov_threshold_u16;
	U16 release_cell_uv_threshold_u16;
	U16 cell_deep_dsg_warning_u16;
	U16 cell_deep_dsg_threshold_u16;

	/*@Pack voltage details 			*/
	U16 pack_ov_warning_u16;
	U16 pack_uv_warning_u16;
	U16 pack_ov_threshold_u16;
	U16 pack_uv_threshold_u16;
	U16 release_pack_ov_threshold_u16;
	U16 release_pack_uv_threshold_u16;

	/* @Cell Temperature details 		*/
	U16 cell_ot_warning_u16;
	U16 cell_ut_warning_u16;
	U16 cell_ot_threshold_u16;
	U16 cell_ut_threshold_u16;
	U16 release_cell_ot_threshold_u16;
	U16 release_cell_ut_threshold_u16;

	/* @Ambient or pack temperature 	*/
	U16 amb_ot_warning_u16;
	U16 amb_ut_warning_u16;
	U16 amb_ot_threshold_u16;
	U16 amb_ut_threshold_u16;
	U16 release_amb_ot_threshold_u16;
	U16 release_amb_ut_threshold_u16;

	/* @mosfet temperature 				*/
	U16 mfet_ot_warning_u16;
	U16 mfet_ut_warning_u16;
	U16 mfet_ot_threshold_u16;
	U16 mfet_ut_threshold_u16;
	U16 release_mfet_ot_threshold_u16;
	U16 release_mfet_ut_threshold_u16;

	/* @Pack current 					*/
	S32 max_chg_1ma_s32;
	S32 max_dsg_1ma_s32;
	S32 warn_max_chg_1ma_s32;
	S32 warn_max_dsg_1ma_s32;
	S32 warn_max_scd_surge_1ma_s32;

	/* @Pack Humidity */
	U16 pack_oh_warning_u16;
	U16 pack_uh_warning_u16;
	U16 pack_oh_threshold_u16;
	U16 pack_uh_threshold_u16;

} debug_comm_basic_batt_info_tst;

typedef struct debug_comm_sys_flt_sts_tst_tag
{
	U32 init_fault_sts_u32;
	U8 	os_init_fault_sts_u8;
	U16 app_init_fault_sts_u16;
	U64	sys_runtime_fault_sts_u64;
	U64 sys_prv_runtime_fault_sts_u64;
} debug_comm_sys_flt_sts_tst;

typedef struct debug_comm_batt_sts_params_tst_tag
{
	S16 total_power_s16;
	U16 available_discharge_power_u16;
	U16 available_charge_power_u16;
	U16 pack_voltage_u16;
	U16 batt_voltage_u16;
	U16 discharge_energy_u16;
	U16 charge_energy_u16;
	U32 tot_energy_throughput_u32;
	U32 tot_accumulated_charge_u32;
	U16 tot_hrs_in_chg_mode_u16;
	U16 chg_capacity_u16;
	U16 dhg_capacity_u16;
	U16	soh_u16;
	U16	soc_u16;
	U16	sop_u16;
	U16 pack_flt_u16;
	U16 bms_mode_16;
	U16 cell_bal_count_u16;
	U8  mfet_contactor_status_u8;
	U16 afe_die_temperature_u16;
	U16	mcu_die_temperature_u16;
	U16 avg_temperature_u16;
	U8  hvil_sts_u8;
	U8  pack_state_u8;
} debug_comm_batt_sts_params_tst;

typedef struct debug_comm_moisture_param_tst_tag
{
    /* hdc2080 parameters */
    S16 	temp_1degc_u16;
    U16		hum_1rh_u16;
} debug_comm_moisture_param_tst;

typedef struct debug_comm_imu_param_tst_tag
{
	/* imu sensor - icm20948 parameters */
	S32 	gyro_1dps_s32[3];
	S32 	accel_1g_s32[3];
	S32 	mag_1ut_s32[3];
}debug_comm_imu_param_tst;

typedef struct debug_comm_can_rx_tst_tag
{
	U8 data_au8[8];
	U32 id_u32;
}debug_comm_can_rx_tst;

typedef struct debug_comm_max_min_cparams_tst_tag
{
	U16 max_vtg_u16;
	U16 min_vtg_u16;
	U16 max_temp_u16;
	U16 min_temp_u16;

	U8 cell_num_max_vtg_u8;
	U8 slv_num_max_vtg_u8;
	U8 cell_num_min_vtg_u8;
	U8 slv_num_min_vtg_u8;

	U8 cell_num_max_temp_u8;
	U8 slv_num_max_temp_u8;
	U8 cell_num_min_temp_u8;
	U8 slv_num_min_temp_u8;
} debug_comm_max_min_cparams_tst;

typedef struct __attribute__((packed)) debug_comm_rtc_ext_param_tst_tag
{
	/* Should not change the order*/ /* based on  PC8565 */
	U8 seconds_u8;
	U8 minutes_u8;
	U8 hours_u8;
	U8 date_u8;
	U8 weekdays_u8;
	U8 months_u8;
	U8 year_u8;

} debug_comm_rtc_ext_param_tst;

typedef struct  debug_comm_rtc_int_param_tst_tag
{
	/* Should not change the order */ /* based on debug communication protocol*/
	U8 hours_u8;
	U8 minutes_u8;
	U8 seconds_u8;
	U8 date_u8;
	U8 months_u8;
	U16 year_u16;
	U8 weekdays_u8;
} debug_comm_rtc_int_param_tst;


typedef struct __attribute__((packed)) debug_comm_rtc_param_tst_tag
{
	/* Should not change the order */ /* based on debug communication protocol*/
	U8 hours_u8;
	U8 minutes_u8;
	U8 seconds_u8;
	U8 date_u8;
	U8 months_u8;
	U16 year_u16;
	U8 weekdays_u8;
} debug_comm_rtc_param_tst;


typedef struct __attribute__((packed))
{
  U8  control_byte_u8;
  U16 total_byte_u16;
  U8  total_packet_u8;
  U8  max_packet_u8;
  U8  pgn_au8[3];
}debug_comm_rts_tst;

typedef struct __attribute__((packed))
{
    U8 multipacket_request_count_u8;
    U8 tot_num_of_multipacket_u8;
    U8 multipacket_buff_au8[8];
    U16 multipacket_data_size_u16;
}debug_comm_send_multipacket_data_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern BOOL debug_comm_transmit_gb;

extern U8 debug_comm_1sec_tmr_gu8;

extern U8 debug_comm_bin_gau8[DEBUG_COMM_BIN_LEN];
extern U8 debug_comm_vin_gau8[DEBUG_COMM_VIN_LEN];
extern U8 debug_comm_mac_gau8[DEBUG_COMM_MAC_LEN];

extern U16 debug_comm_cell_vtg_1cv_gau16[DEBUG_COMM_TOTAL_CELLS];
extern U16 debug_comm_cell_temp_1cc_gau16[10];//DEBUG_COMM_TOTAL_CELL_TEMP_SENS
extern U16 debug_comm_ntc_temp_1cc_gau16[DEBUG_COMM_TOTAL_NTC_TEMP_SENS];
extern U16 debug_comm_amb_light_gu16;
extern U16 debug_comm_tms_data_gau16[DEBUG_COMM_TOTAL_TMS_CHANNELS];
extern U16 debug_comm_sbc_data_gu16;
extern U16 debug_comm_bender_data_gu16;

extern U32 flsh_retrival_size_u32;

extern debug_comm_hvadc_param_tst 			debug_comm_hvadc_param_gst;
extern debug_comm_mcu_adc_param_tst 		debug_comm_mcu_adc_param_gst;

extern debug_comm_rtc_param_tst				debug_comm_temp_rtc_param_gst;
extern debug_comm_rtc_ext_param_tst 		debug_comm_rtc_ext_param_gst;
extern debug_comm_rtc_ext_param_tst 		debug_comm_rtc_set_ext_param_gst;
extern debug_comm_rtc_int_param_tst 		debug_comm_rtc_int_param_gst;
extern debug_comm_moisture_param_tst 		debug_comm_moisture_param_gst;
extern debug_comm_slv_flt_sts_tst 			debug_comm_slave_flt_sts_gast[DEBUG_COMM_TOTAL_SLAVES];
extern debug_comm_sys_flt_sts_tst 			debug_comm_sys_flt_sts_gst;
extern debug_comm_cb_sts_tst 				debug_comm_cb_sts_gst;
extern debug_comm_max_min_cparams_tst 		debug_comm_max_min_cparams_gst;
extern debug_comm_basic_batt_info_tst 		debug_comm_basic_batt_info_gst;
extern debug_comm_batt_sts_params_tst 		debug_comm_batt_sts_params_gst;
extern debug_comm_can_rx_tst 				debug_comm_can_rx_gst;
extern debug_comm_rts_tst 	 				debug_comm_rts_st;
extern debug_comm_send_multipacket_data_tst debug_comm_send_multipacket_data_gst;

/** @} */

extern U8 debug_gui_u8;
extern U8 debug_comm_j1939_chck_mp_msg_u8();
/* Public Function Prototypes **/
extern void debug_comm_param_init_v();
extern void debug_comm_update_info_v();

extern void debug_comm_1s_callback_v();
extern void debug_comm_canrx_process_v(can_comm_task_can_msg_tst* rx_can_message_arg_st);
extern void debug_comm_cantx_process_v(can_comm_task_can_msg_tst* tx_can_message_arg_st);
#endif /* APPLICATION_LAYER_BUSINESS_LOGICS_DEBUG_COMM_H_ */
