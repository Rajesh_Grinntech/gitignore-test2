/*
 * evq_charger.h
 *
 *  Created on: 17-Aug-2021
 *      Author: SHYAM
 */

#ifndef APPLICATION_LAYER_BUSINESS_LOGICS_EVQ_CHARGER_H_
#define APPLICATION_LAYER_BUSINESS_LOGICS_EVQ_CHARGER_H_

#include "batt_param.h"
#include "protect_app.h"
#include "common_header.h"
#include "gt_memory.h"
#include "can_app.h"

#define BMS_TO_CHARGER_REQ						0x1806E5F4
#define CHARGER_TO_BMS_REQ						0x18FF50E5

#define BMS_VOLTAGE_SET_REQ						(585U)
#define BMS_CURRENT_SET_REQ						(5U)

typedef enum evq_chg_sts_ten_tag
{
	HARDWARE_FAIL = 0,
	OVER_TEMP_FAIL,
	COMMUNICATION_FAIL = 4,
	EVQ_ALL_FLT_STS			/** Must be in last **/
}evq_chg_sts_ten;

typedef struct evq_chg_data_tst_tag
{
	U16 evq_v_set_u16;
	U16 evq_i_set_u16;
	U8  evq_cntrl_set_u8;
	U16 evq_v_get_u16;
	U16 evq_i_get_u16;
	evq_chg_sts_ten evq_chg_sts_gen;
}evq_chg_data_tst;

extern BOOL evq_charger_sts_bool;
extern void evq_charger_init_v();
extern void evq_chg_req_mess_v();
extern U8 evq_chg_rx_mess_v(can_comm_task_can_msg_tst* rx_can_message_arpst);
extern evq_chg_data_tst evq_chg_data_gst;
#endif /* APPLICATION_LAYER_BUSINESS_LOGICS_EVQ_CHARGER_H_ */
