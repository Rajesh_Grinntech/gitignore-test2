/**
========================================================================================================================
@page debug_comm_header_file

@subsection  Details

@n@b Title:
@n@b Filename:   	debug_comm.h
@n@b MCU:
@n@b Compiler:
@n@b Author:     	KRISH

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#
@n@b Date:       	28-May-2020
@n@b Description:  	Created

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BUSINESS_LOGICS_CLIENT_COMM_H_
#define APPLICATION_LAYER_BUSINESS_LOGICS_CLIENT_COMM_H_

/* Includes for definitions */
#include "common_header.h"
#include "common_var.h"
#include "can_commtask.h"
#include "gpio_app.h"
#include "can_app.h"
//#include "uart_app.h"
#include "batt_param.h"

/* Macro Definitions */
/* This should not be changed for any project.*/
#define DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD  		8
#define DEBUG_COMM_MAX_CELLS_PER_MSG_FRAME_ALWD  		4   /*Used in Cell voltage*/
#define DEBUG_COMM_MAX_TEMP_SENS_PER_MSG_FRAME_ALWD  	4	/*Used in Cell temperature*/

#define CLIENT_COMM_DISPLAY_USE				COM_HDR_DISABLED

/* Project- wise Configuration MACRO starts - Configured for ROBIN*/

#define CLIENT_GUI_ENABLED							(client_gui_u32)
/* Battery Pack Params*/
#define DEBUG_COMM_TOTAL_SLAVES								1
#define DEBUG_COMM_TOTAL_CELLS_PER_SLV						16
#define DEBUG_COMM_TOTAL_TMS_CHANNELS						8

#define CLIENT_COMM_GUI_CELL_VOLT								20
#define CLIENT_COMM_GUI_CELL_TEMP								30

/* ENABLE/ DISABLE Configuration ends*/
#define DEBUG_COMM_BIN_LEN		20
#define DEBUG_COMM_VIN_LEN		17
#define DEBUG_COMM_MAC_LEN		6

#define DEBUG_COMM_BMS_TO_GUI_PGN 				0x800FADA
#define DEBUG_COMM_GET_PGN(x)					((x & 0xFF0000) >> COM_HDR_SHIFT_16_BITS)
#define DEBUG_COMM_MAKE_PGN(x)					(DEBUG_COMM_BMS_TO_GUI_PGN | ((x << COM_HDR_SHIFT_16_BITS) & 0xFF0000))
/* Configuration MACRO ends*/

#define DEBUG_COMM_TOTAL_CELLS					(DEBUG_COMM_TOTAL_CELLS_PER_SLV * DEBUG_COMM_TOTAL_SLAVES)
#define DEBUG_COMM_TOTAL_CELL_TEMP_SENS			BATT_PARAM_TOT_CELL_TEMP_SENS
#define DEBUG_COMM_TOTAL_NTC_TEMP_SENS			BATT_PARAM_TOT_NTC_TEMP_SENS

#define CLIENT_COMM_GUI_ADDR					0xFA
#define CLIENT_COMM_BMS_ADDR					0xDA
#define CLIENT_COMM_MULTIPACKET_PRIORITY 	2

#define CLIENT_COMM_MULTIPACKET_DATA_PGN 	client_comm_send_multipacket_data_gst.multipacket_pgn_u8
#define CLIENT_COMM_MULTIPACKET_GET_ID      CAN_APP_GET_ID(DEBUG_COMM_MULTIPACKET_PRIORITY,CLIENT_COMM_MULTIPACKET_DATA_PGN,DEBUG_COMM_GUI_ADDR,DEBUG_COMM_BMS_ADDR)
#define CLIENT_COMM_MULTIPACKET_BASE_ID		CAN_APP_BASE_ID(CLIENT_COMM_MULTIPACKET_GET_ID)
#define CLIENT_COMM_MULTIPACKET_EXT_ID		CAN_APP_EXT_ID(CLIENT_COMM_MULTIPACKET_GET_ID)

#define DEBUG_COMM_MULTIPACKET_TIMEOUT		250
#define CLIENT_COMM_MULTIPACKET_DATA_FRAME	(CLIENT_COMM_MULTIPACKET_BASE_ID,CLIENT_COMM_MULTIPACKET_EXT_ID, 			\
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR, 								\
											sizeof(client_comm_send_multipacket_data_gst.multipacket_buff_au8), 	\
											client_comm_send_multipacket_data_gst.multipacket_buff_au8)

#define DISPLAY_COMM_MULTIPACKET_PRIORITY 	2
#define DISPLAY_COMM_ADDR					0xBA//DEBUG_COMM_BMS_ADDR
#define DISPLAY_COMM_MULTIPACKET_GET_ID		CAN_APP_GET_ID(DISPLAY_COMM_MULTIPACKET_PRIORITY,CLIENT_COMM_MULTIPACKET_DATA_PGN,DISPLAY_COMM_ADDR,DEBUG_COMM_BMS_ADDR)
#define DISPLAY_COMM_MULTIPACKET_BASE_ID    CAN_APP_BASE_ID(DISPLAY_COMM_MULTIPACKET_GET_ID)
#define DISPLAY_COMM_MULTIPACKET_EXT_ID     CAN_APP_EXT_ID(DISPLAY_COMM_MULTIPACKET_GET_ID)
#define DISPLAY_COMM_MULTIPACKET_DATA_FRAME	(DISPLAY_COMM_MULTIPACKET_BASE_ID,DISPLAY_COMM_MULTIPACKET_EXT_ID, 			\
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR, 								\
											sizeof(client_comm_send_multipacket_data_gst.multipacket_buff_au8), 	\
											client_comm_send_multipacket_data_gst.multipacket_buff_au8)

#define DEBUG_COMM_RTS_GET_ID            	CAN_APP_GET_ID(DEBUG_COMM_MULTIPACKET_PRIORITY,DEBUG_COMM_RTS_CTS_EOF_PGN,DEBUG_COMM_GUI_ADDR,DEBUG_COMM_BMS_ADDR)
#define DEBUG_COMM_RTS_BASE_ID    	     	CAN_APP_BASE_ID(DEBUG_COMM_RTS_GET_ID)
#define DEBUG_COMM_RTS_EXT_ID            	CAN_APP_EXT_ID(DEBUG_COMM_RTS_GET_ID)
#define DEBUG_COMM_RTS_SIZE              	(U8)sizeof(debug_comm_rts_tst)
#define DEBUG_COMM_RTS_TIMEOUT           	250

#define CLIENT_COMM_RTS_FRAME             	(DEBUG_COMM_RTS_BASE_ID,DEBUG_COMM_RTS_EXT_ID, \
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR,		   \
											DEBUG_COMM_RTS_SIZE,						   \
											(U8*)&client_comm_rts_st)

#define DISPLAY_COMM_RTS_GET_ID            	CAN_APP_GET_ID(DEBUG_COMM_MULTIPACKET_PRIORITY,DEBUG_COMM_RTS_CTS_EOF_PGN,DISPLAY_COMM_ADDR,DEBUG_COMM_BMS_ADDR)
#define DISPLAY_COMM_RTS_BASE_ID			CAN_APP_BASE_ID(DISPLAY_COMM_RTS_GET_ID)
#define DISPLAY_COMM_RTS_EXT_ID				CAN_APP_EXT_ID(DISPLAY_COMM_RTS_GET_ID)
#define DISPLAY_COMM_RTS_FRAME             	(DISPLAY_COMM_RTS_BASE_ID,DISPLAY_COMM_RTS_EXT_ID, \
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR,		   \
											DEBUG_COMM_RTS_SIZE,						   \
											(U8*)&client_comm_rts_st)

#define CLIENT_COMM_START_STOP_GET_ID         CAN_APP_GET_ID(CLIENT_COMM_MULTIPACKET_PRIORITY,CLIENT_COMM_START_STOP_PGN,CLIENT_COMM_GUI_ADDR,CLIENT_COMM_BMS_ADDR)
#define CLIENT_COMM_START_STOP_BASE_ID    	 CAN_APP_BASE_ID(CLIENT_COMM_START_STOP_GET_ID)
#define CLIENT_COMM_START_STOP_EXT_ID         CAN_APP_EXT_ID(CLIENT_COMM_START_STOP_GET_ID)
#define CLIENT_COMM_START_STOP_SIZE           (U8)sizeof(debug_comm_rts_tst)
#define CLIENT_COMM_START_STOP_TIMEOUT        250

#define CLIENT_COMM_START_STOP_FRAME        (CLIENT_COMM_START_STOP_BASE_ID,CLIENT_COMM_START_STOP_EXT_ID, \
											CAN_APP_RTR,CAN_APP_IDE,CAN_APP_SRR,		   \
											CLIENT_COMM_START_STOP_SIZE,						   \
											(U8*)&client_comm_tx_msg_st.data_au8)

typedef struct __attribute__((packed))
{
	U8 bin_id_no_au8[20];
	U8 vin_id_no_au8[20];
	U8 mac_addr_au8[6];
}client_comm_bms_id_tst;

#define CLIENT_COMM_VIN_BIN_MAC_MSG_SIZE	(U8)sizeof(client_comm_bms_id_tst)
//#define DEBUG_COMM_ONE_TIME_INFO_MSG_SIZE	(U8)sizeof(debug_comm_basic_batt_info_tst)
//#define DEBUG_COMM_BIN_MSG_SIZE				(U8)sizeof(common_var_bms_id_log_gst.bin_au8)
//#define DEBUG_COMM_VIN_MSG_SIZE				(U8)sizeof(common_var_bms_id_log_gst.vin_au8)
//#define DEBUG_COMM_BATT_PARAM_STS_MSG_SIZE	(U8)sizeof(debug_comm_batt_sts_params_tst)
#define CLIENT_COMM_FLASH_RETRIVAL_MSG_SIZE (U8)sizeof(bms_debug_battery_data_log_tst)
#define CLIENT_COMM_OT_BATT_INFO_MSG_SIZE  	(U8)sizeof(client_comm_basic_batt_info_tst)
#define CLIENT_COMM_BATT_PARAM_MSG_SIZE		(U8)sizeof(client_comm_batt_params_tst)
#define CLIENT_COMM_SENSOR_PARAM_MSG_SIZE  	(U8)sizeof(client_comm_sensor_param_tst)
#define CLIENT_COMM_FAULT_STATUS_MSG_SIZE  	(U8)sizeof(client_comm_sys_flt_sts_tst)
#define CLIENT_COMM_BATT_STATUS_MSG_SIZE  	(U16)sizeof(client_comm_batt_sts_params_tst)

#define DISPLAY_COMM_PERIODIC_MESS_SIZE		(U16)sizeof(display_param_period_tst)
#define AXIS 								3 	/* No IMU driver*/

/** @{
 * Type definition */

typedef enum client_comm_pgn_list_te_tag
{
	CLIENT_COMM_START_STOP_PGN			 = 0x00,
	CLIENT_COMM_AUTH_PGN				 = 0x01,
	CLIENT_COMM_BIN_VIN_MAC_PGN          = 0x10,
	CLIENT_COMM_FLASH_RET_PGN         	 = 0x11,
	CLIENT_COMM_BATT_ONE_TIME_INFO_PGN	 = 0x12,
	CLIENT_COMM_BATT_PERIODIC_INFO_PGN   = 0x20,
	CLIENT_COMM_SENSOR_PERIODIC_INFO_PGN = 0x21,
	CLIENT_COMM_FAULT_PERIODIC_INFO_PGN  = 0x22,
	CLIENT_COMM_BATT_STATUS_PERIODIC_PGN = 0x23,
	CLIENT_COMM_RTC_SET_PGN              = 0X40,
	CLIENT_COMM_SET_BIN_PGN				 = 0x41,
	CLIENT_COMM_SLEEP_PGN                = 0x4A,
	CLIENT_COMM_RTS_CTS_EOF_PGN 		 = 0xEC

}client_comm_pgn_list_te;

typedef enum display_comm_pgn_list_te_tag
{
	DISPLAY_COMM_START_STOP_PGN			 = 0x80,
	DISPLAY_COMM_PARAM_REQ_PGN			 = 0x10,
}display_comm_pgn_list_te;

typedef enum display_comm_tx_states_te_tag
{
	DISPLAY_COMM_START_STOP_ACK_STATE			= 0		,
	DISPLAY_COMM_PARAM_REQ_STATE						,
	DISPLAY_COMM_MULTIPACK_SEND_STATE					,
	DISPLAY_COMM_WARNING_SEND_STATE
}display_comm_tx_states_te;

#if 1   /* Delete this messages list once pgn list gets confirmed */
typedef enum client_comm_messages_list_te_tag
{
	CLIENT_COMM_START_STOP_RES 			= 0x800DAFA,
	CLIENT_COMM_SLEEP_ACK_RES           = 0x84AFADA,
}client_comm_messages_list_te;
#endif

typedef enum display_comm_messages_list_te_tag
{
	DISPLAY_COMM_START_STOP_RES 		= 0x980BAFA,
	DISPLAY_COMM_WARN_RES 				= 0x911BAFA,
	DISPLAY_COMM_SLEEP_ACK_RES          = 0x84AFADA,
}display_comm_messages_list_te;
typedef enum client_comm_tx_states_te_tag
{
	CLIENT_COMM_START_ACK_STATE			= 0		,
	CLIENT_COMM_AUTH_ACK_STATE					,
	CLIENT_COMM_SEND_ONE_TIME_INFO_STATE		,
	CLIENT_COMM_SEND_ONE_SEC_PARAM_STATE		,
	CLIENT_COMM_MULTIPACK_SEND_STATE			,
	CLIENT_COMM_MULTIPACK_EOF_MSG_STATE			,
	CLIENT_COMM_SLEEP_ACK_STATE

}client_comm_tx_states_te;

typedef enum client_comm_one_time_mp_req_te_tag
{
	BIN_VIN_MAC_REQ    = 0,
	FLSH_DATA_RET_REQ     ,
	BATT_OT_INFO_REQ	  ,

}client_comm_one_time_mp_req_te;

typedef struct __attribute__((packed))
{
	U16 soc_u16;
		U16 total_power_u16;
		U16 available_discharge_power_u16;
		U16 available_charge_power_u16;
		U16 pack_voltage_u16;
		U32 battery_current_s32;
		U32 tot_energy_throughput_u32;
		U32 tot_accumulated_charge_u32;
		U16 tot_hrs_in_chg_mode_u16;
		U16 chg_capacity_u16;
		U16 dhg_capacity_u16;
		U16 soh_u16; // soc above this
		U16 soe_u16;
		U16 chg_current_lmt_u16;
		U16 dchg_current_lmt_u16;
		U16 pack_flt_u16;
		U16 pack_cycle_count_u16;
		U16 cell_bal_count_u16;
		U16 ambient_temp_u16;
		U16 fet_temp_u16;
		U16 afe_die_temp_u16;
		U16 mcu_die_temp_u16;
		U16 mos_pck_state_u8;

}display_param_period_tst;
typedef struct __attribute__((packed))//debug_comm_batt_params_tst_tag//
{
	U8  num_of_cell_volt_u8;
	U16 battery_cell_voltage_au8[CLIENT_COMM_GUI_CELL_VOLT];
	U8  num_of_cell_temp_u8;
	S16 individual_sensor_tempurature_as16[CLIENT_COMM_GUI_CELL_TEMP];
	U8  cell_bal_state_u8;
	U16 cell_bal_status_1_u16;
	U16 cell_bal_status_2_u16;
	S32 battery_current_s32;
	U16 pack_volt_u16;
	U16 fuse_volt_u16;
	U16 pcon_volt_u16;
	U16 mos_volt_u16; /* TODO LD Voltage not shared due to protocol */
//	U16 ld_volt_u16;
}client_comm_batt_params_tst;

typedef enum client_comm_multipacket_controlbytes_te_tag
{
	CLIENT_COMM_RTS_CONTROLBYTE = 0x10	,
	CLIENT_COMM_CTS_CONTROLBYTE = 0x11	,
	CLIENT_COMM_EOF_CONTROLBYTE = 0x13

}client_comm_multipacket_controlbytes_te;

typedef struct __attribute__((packed))//debug_comm_batt_sts_params_tst_tag
{
	U8 rtc_hours_u8;
	U8 rtc_minutes_u8;
	U8 rtc_seconds_u8;
	U8 rtc_date_u8;
//	U8 rtc_weekdays_u8;
	U8 rtc_months_u8;
	U8 rtc_year_u8;
	S16 total_power_s16;
	U16 available_discharge_power_u16;
	U16 available_charge_power_u16;
	U16 pack_voltage_u16;
	U16 discharge_energy_u16;
	U16 charge_energy_u16;
	U32 tot_energy_throughput_u32;
	U32 tot_accumulated_charge_u32;
	U16 tot_hrs_in_chg_mode_u16;
	U16 chg_capacity_u16;
	U16 dhg_capacity_u16;
	U16	soh_u16;
	U16	soc_u16;
	U16 soe_u16;
	U16	sop_u16;
	U16 chg_current_lmt_u16;
	U16 dchg_current_lmt_u16;
	U16 pack_flt_u16;
	U16 pack_cycle_count_u16;
	U16 cell_bal_count_u16;
	U8  mfet_contactor_status_u8;
	U16 ambient_temp_u16;
	U16 fet_temp_u16;
	U16 afe_die_temp_u16;
	U16	mcu_die_temp_u16;
	U16 avg_temp_u16;
	U8  pack_state_u8;
	U8  gprs_conn_status_u8;
	U8  gprs_sig_strength_u8;
	U8  gps_conn_status_u8;
	U16 gps_accuracy_u16;
	U32 gps_latitude_u32;
	U32 gps_longitude_u32;
	U8  gps_dev_connected_u8;
	U8 	ble_conn_status_u8;
	U16 max_cell_volt_u16;
	U16 min_cell_volt_u16;
	S16 max_cell_temp_s16;
	S16 min_cell_temp_s16;
} client_comm_batt_sts_params_tst;

typedef struct __attribute__((packed))//debug_comm_sensor_param_tst_tag
{
	/* imu sensor - icm20948 parameters */
	S32 	accel_1g_s32[3];
	S32 	gyro_1dps_s32[3];
	S32 	mag_1ut_s32[3];

	/* luminence data */
    U16 	luminence_data_u16;

    /* hdc2080 parameters */
    U16		hum_1rh_u16;
    S16 	temp_1degc_s16;
}client_comm_sensor_param_tst;

typedef struct client_comm_sys_flt_sts_tst_tag
{
	U64 thremistor_flt_sts_u64;
	U16 app_init_fault_sts_u16;
	U8 	os_init_fault_sts_u8;
	U32 init_fault_sts_u32;
	U32	sys_runtime_fault_sts_u32;
} client_comm_sys_flt_sts_tst;

typedef struct __attribute__((packed))//debug_comm_basic_batt_info_tst_tag
{
	/* @Version number 					*/
	U16 bms_hw_version_num_u16;
	U16 bms_pk_version_num_u16;
	U8 	bms_firmware_version_num_u8;
	U8  bms_platform_no_u8;
	U8  bms_model_no_au8[10];
	//U64  bms_model_no_u64;//S32 bms_pack_no_S32;
//	U16 slave_hw_version_num_u16;
//	U8 bms_protocol_version_num_u8;

	/* modue specifications */
	//U8 	gprs_version_num_u8;
	U8 	gprs_imei_num_u8[7];
	U8 	gps_version_num_u8;
	U8 	ble_version_num_u8;
	U8 ble_mac_addr_u8[6];

	/* @Batt manufacture details 		*/
	//U8 batt_supplier_u8;
	//U8 batt_customer_u8;
	U32 date_of_manf_u32;
	U16 pack_dod_u16;

	/* @Batt specifications 			*/
	U16 rated_pack_cycles_u16;
	U16 nominal_voltage_u16;
	U16 rated_capacity_u16;
	U16 cell_type_u16;
	U8 total_cells_u16;
//	U16 number_of_slaves_u16;

	/* @Cell voltage details 			*/
	U16 cell_ov_warning_u16;
	U16 cell_uv_warning_u16;
	U16 cell_ov_threshold_u16;
	U16 cell_uv_threshold_u16;
	U16 cell_deep_dsg_warning_u16;
	U16 cell_deep_dsg_threshold_u16;
	U16 cell_ov_flt_region_u16;
	U16 cell_uv_flt_region_u16;
	U16 release_cell_ov_threshold_u16;
	U16 release_cell_uv_threshold_u16;


	/*@Pack voltage details 			*/
	U16 pack_ov_warning_u16;
	U16 pack_uv_warning_u16;
	U16 pack_ov_threshold_u16;
	U16 pack_uv_threshold_u16;
	U16 pack_ov_flt_region_u16;
	U16 pack_uv_flt_region_u16;
	U16 release_pack_ov_threshold_u16;
	U16 release_pack_uv_threshold_u16;

	/* @Cell Temperature details 		*/
	U16 cell_ot_warning_u16;
	U16 cell_ut_warning_u16;
	U16 cell_ot_threshold_u16;
	U16 cell_ut_threshold_u16;
	U16 cell_ot_flt_region_u16;
	U16 cell_ut_flt_region_u16;
	U16 release_cell_ot_threshold_u16;
	U16 release_cell_ut_threshold_u16;

	/* @Ambient or pack temperature 	*/
	U16 amb_ot_warning_u16;
	U16 amb_ut_warning_u16;
	U16 amb_ot_threshold_u16;
	U16 amb_ut_threshold_u16;
	U16 release_amb_ot_threshold_u16;
	U16 release_amb_ut_threshold_u16;

	/* @mosfet temperature 				*/
	U16 mfet_ot_warning_u16;
	U16 mfet_ut_warning_u16;
	U16 mfet_ot_threshold_u16;
	U16 mfet_ut_threshold_u16;
	U16 release_mfet_ot_threshold_u16;
	U16 release_mfet_ut_threshold_u16;

	/* @Pack current 					*/
	U32 warn_max_chg_1ma_s32;
	U32 warn_max_dsg_1ma_s32;
	U32 max_chg_1ma_s32;
	U32 max_dsg_1ma_s32;
	U32 warn_max_scd_surge_1ma_s32;
	U16 chg_1ma_flt_region_u16;
	U16 dchg_1ma_flt_region_u16;

	/* @Pack Humidity */
	U16 pack_oh_warning_u16;
	U16 pack_uh_warning_u16;
	U16 pack_oh_threshold_u16;
	U16 pack_uh_threshold_u16;
	U16 pack_oh_release_u16;
	U16 pack_uh_release_u16;

} client_comm_basic_batt_info_tst;

typedef enum client_comm_periodic_mp_req_te_tag
{
	ONE_SEC_BATT_MESS_REQ		=0,
	ONE_SEC_SENSOR_MESS_REQ		  ,
	ONE_SEC_FAULT_MESS_REQ		  ,
	ONE_SEC_BATT_STATUS_REQ		  ,
}client_comm_periodic_mp_req_te;

typedef struct __attribute__((packed))
{
    U8 multipacket_request_count_u8;
    U8 tot_num_of_multipacket_u8;
    U8 multipacket_buff_au8[8];
    U16 multipacket_data_size_u16;
    U8 multipacket_pgn_u8;
}client_comm_send_multipacket_data_tst;

typedef struct client_comm_fault_state_tst_tag
{
	U32 thremistor_flt_status_u32;
	U16 app_init_flt_status_u16;
	U8 	os_init_flt_status_u8;
	U32 core_init_flt_status_u32;
	U32 system_flt_status_u32;
}client_comm_fault_state_tst;

typedef struct __attribute__((packed))
{
	client_comm_bms_id_tst 				client_comm_bms_id_gst;
	bms_debug_battery_data_log_tst		flsh_ret_debug_battery_data_log_gst;
	client_comm_basic_batt_info_tst		batt_pack_info_gst;

	client_comm_sensor_param_tst		sensor_param_gst;
	client_comm_fault_state_tst			fault_status_gst;
	client_comm_batt_sts_params_tst     batt_sts_params_gst;
	client_comm_batt_params_tst			batt_params_update_gst;
}client_comm_data_update_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
extern BOOL debug_comm_transmit_gb;

extern U8 debug_comm_1sec_tmr_gu8;

extern U8 debug_comm_bin_gau8[DEBUG_COMM_BIN_LEN];
extern U8 debug_comm_vin_gau8[DEBUG_COMM_VIN_LEN];
extern U8 debug_comm_mac_gau8[DEBUG_COMM_MAC_LEN];

extern U16 debug_comm_cell_vtg_1cv_gau16[DEBUG_COMM_TOTAL_CELLS];
extern U16 debug_comm_cell_temp_1cc_gau16[10];//DEBUG_COMM_TOTAL_CELL_TEMP_SENS
extern U16 debug_comm_ntc_temp_1cc_gau16[DEBUG_COMM_TOTAL_NTC_TEMP_SENS];
extern U16 debug_comm_amb_light_gu16;
extern U16 debug_comm_tms_data_gau16[DEBUG_COMM_TOTAL_TMS_CHANNELS];
extern U16 debug_comm_sbc_data_gu16;
extern U16 debug_comm_bender_data_gu16;

extern U32 flsh_retrival_size_u32;
extern U32 client_gui_u32;
extern client_comm_send_multipacket_data_tst client_comm_send_multipacket_data_gst;
/** @} */

extern U8 client_comm_j1939_chck_mp_msg_u8();
extern U8 display_comm_j1939_chck_mp_msg_u8();
/* Public Function Prototypes **/
extern void debug_comm_param_init_v();

extern void client_comm_canrx_process_v(can_comm_task_can_msg_tst* rx_can_message_arg_st);
extern void client_comm_cantx_process_v(can_comm_task_can_msg_tst* tx_can_message_arg_st);
#endif /* APPLICATION_LAYER_BUSINESS_LOGICS_DEBUG_COMM_H_ */
