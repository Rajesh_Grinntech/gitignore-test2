/**
========================================================================================================================
@page bms_cont_ctrl_task_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_con_ctrl_task.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	24-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_OS_TASK_BMS_CON_CTRL_TASK_H_
#define APPLICATION_LAYER_OS_TASK_BMS_CON_CTRL_TASK_H_

/* Includes for definitions */
/* @{
 * Macro definition */
#define FET_REAL_STATUS COM_HDR_ENABLED


#define FET_TASK_SWITCH_CASE 	COM_HDR_ENABLED
#define FET_TASK_WITH_PRE 		COM_HDR_ENABLED /* This can only be used with FET_TASK_SWITCH_CASE Enabled*/

/* @} */

/* @{
 * Type definition */
typedef enum mfet_task_states_te_tag
{
	STATE_IDLE,
	STATE_CHG_DSG,
	STATE_CHG_DSG_DEEP,
	STATE_FAULT_CHG_ONLY_ON,
	STATE_FAULT_DSG_ONLY_ON,
	STATE_FAULT_BOTH_OFF

}mfet_task_states_te;
/* @} */

/* @{
 *  Public Variable Definitions */
//extern U8 bms_tsk_mfet_idl_flg_u8;
//extern U8 bms_tsk_mfet_idl_counter_u8;

extern BOOL idle_detect_set_timer_b;
extern U16  idle_detect_counter_u16;
extern BOOL pchg_pdsg_mode_b;

/* @} */

/* Public Function Prototypes **/
extern void bms_mfet_ctrl_task_v(void *parameter);
#endif /* APPLICATION_LAYER_OS_TASK_BMS_CON_CTRL_TASK_H_ */
