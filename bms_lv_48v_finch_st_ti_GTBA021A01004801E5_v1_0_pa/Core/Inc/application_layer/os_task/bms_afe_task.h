/**
========================================================================================================================
@page bms_afe_task_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_afe_task.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	11-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_OS_TASK_BMS_AFE_TASK_H_
#define APPLICATION_LAYER_OS_TASK_BMS_AFE_TASK_H_

/* Includes for definitions */
#include "common_header.h"
/* @{
 * Macro definition */
typedef enum bms_afe_task_state_te_tag
{
	BMS_AFE_TASK_START = 1,
	BMS_AFE_TASK_MEASUREMENT_INIT,
	BMS_AFE_TASK_MEASUREMENT_INIT_SUCCESS,
	BMS_AFE_MEASUREMENT,
	BMS_AFE_TASK_PROTECTION_FUNCTIONAL,
	BMS_AFE_FAIL_CHECK,
	BMS_AFE_FAULT_STATE,
	BMS_AFE_TASK_MEASUREMENT_INIT_FAILED = 0x80
}bms_afe_task_state_te;


/* @} */

/* @{
 * Type definition */

/* @} */

/* @{
 *  Public Variable Definitions */
extern bms_afe_task_state_te bms_afe_task_state_ge;

extern U32 timer_check_10ms_au32[100];
extern U32 timer_check_100ms_au32[100];

extern U32 timer_check_10ms_u32;
extern U32 timer_check_100ms_u32;

//extern BOOL vtg_meas_100ms_flag_b;

extern U8 boot_status_u8;
/* @} */

/* Public Function Prototypes **/
extern void bms_afe_management_v(void * parameter);
#endif /* APPLICATION_LAYER_OS_TASK_BMS_AFE_TASK_H_ */
