/**
========================================================================================================================
@page diagmonitor_task_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	diagmonitor_task.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	09-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_OS_TASK_DIAGMONITOR_TASK_H_
#define APPLICATION_LAYER_OS_TASK_DIAGMONITOR_TASK_H_

/* Includes for definitions */
#include "common_header.h"
/* @{
 * Macro definition */

/* @} */

/* @{
 * Type definition */
typedef enum diagmon_app_fault_bits_te_tag
{
	DIAG_BIT_AFE_PASS_SECOND_TRY = 0,
	DIAG_BIT_MEAS_INIT_FAILED,
	DIAG_BIT_ADC_NTC_READY_FAILING,
	DIAG_BIT_AFE_DATA_READY_FAILING,
	DIAG_BIT_AFE_COMM_FAILED,
	DIAG_BIT_AFE_HW_FAILED,

}diagmon_app_fault_bits_te;

typedef enum diagmon_app_can_state_te_tag
{
	DIAG_BIT_CAN_READY,
	DIAG_BIT_CAN_NOT_READY
} diagmon_app_can_state_te;
/* @} */

/* @{
 *  Public Variable Definitions */
extern U32 diagmon_driver_init_fault_code_gu32 ;
extern BOOL diagmon_hold_wdg_refresh_force_reset_b ;
extern U32 diagmon_task_timer_start_agu32[10] ;
extern U16 diagmon_task_timer_stop_agu32[10] ;
extern U16 diagmon_task_timer_algo_stop_agu32[10] ;
extern BOOL diag_flt_rst_flg;
/* @} */

/* Public Function Prototypes **/
extern void diagmonitor_task_monitor_v(void * parameter);
extern void diagmonitor_task_initialization_fault_handler_v(void);
#endif /* APPLICATION_LAYER_OS_TASK_DIAGMONITOR_TASK_H_ */
