/**
========================================================================================================================
@page storage_task_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	storage_task.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	19-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BATTERY_MANAGEMENT_STORAGE_TASK_H_
#define APPLICATION_LAYER_BATTERY_MANAGEMENT_STORAGE_TASK_H_

/* Includes for definitions */
#include "common_header.h"
#include "operating_system.h"


/* @{
 * Macro definition */

/* @} */

/** @{
 * Type definition */
typedef enum storage_task_boot_data_type_te_tag
{
	STORAGE_BOOT_DATA,
	STORAGE_BOOT_CONFIG
}storage_task_boot_data_type_te;

#if 0
typedef struct __attribute__((packed))
{
	U32 start_addr_u32;
	U32 end_addr_u32;
	U8  tot_size_u8;
	U8  particular_param_size_to_change_u8;
	void  *particular_param_addr_to_change_pv;
	void *bms_batt_upd_log_pv;
	void *bms_data_to_change_pv;
}storage_task_update_batt_data_tst;
#endif

typedef struct storage_task_update_batt_data_tst_tag
{
	U32 start_addr_u32;
	U32 end_addr_u32;
	U32 size_u32;
	void *bms_batt_upd_log_st;
}storage_task_update_batt_data_tst;

typedef struct __attribute__((packed))
{
	U32 size_u32;
	storage_task_boot_data_type_te boot_data_e;
	U32 start_addr_u32;
	// void *boot_data_array_au8;
}storage_task_update_boot_data_tst;


/** @} */
/** @{

extern U8 storage_task_spi_flash_wr_data_au8[1024];
extern U8 storage_task_spi_flash_rd_data_au8[1024];
 *  Public Variable Definitions */
extern U8 dummy_data_u8;
//extern U8 one_sec_str_flg;
extern storage_task_update_batt_data_tst storage_task_update_batt_data_st;
extern storage_task_update_batt_data_tst storage_task_flsh_ret_update_batt_data_st;
extern storage_task_update_boot_data_tst storage_task_boot_data_st;
extern operating_system_can_comm_queue_tst storage_tx_can_comm_queue_st;

extern storage_task_update_batt_data_tst storage_task_update_batt_engy_data_st;

/** @} */

/* Public Function Prototypes **/
extern U8  storage_task_start_addr_init_u8();
extern void storage_task_t8_v(void *parameter);

#endif /* APPLICATION_LAYER_BATTERY_MANAGEMENT_STORAGE_TASK_H_ */
