/**
========================================================================================================================
@page bms_protection_task_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_protection_task.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	26-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_OS_TASK_BMS_PROTECTION_TASK_H_
#define APPLICATION_LAYER_OS_TASK_BMS_PROTECTION_TASK_H_

/* Includes for definitions */
/* @{
 * Macro definition */
typedef enum bms_protection_manage_state_te_tag
{
	MANAGE_CELL_V_T_LIMITS = 0x01,
	MANAGE_PACK_V_T_LIMITS,
	MANAGE_CURRENT_LIMITS,
	MANAGE_CELL_BALANCING,
	MANAGE_HV_PROTECTION,
	MANAGE_TMS_PROTECTION,
	MANAGE_RECOVER_HW_PROTECTION,
	MANAGE_POWERFET_DECISION,
	MANAGE_ESTIMATIONS,

	MANAGE_MAX_NUM
}bms_protection_manage_state_te;
/* @} */

/* @{
 * Type definition */

/* @} */

/* @{
 *  Public Variable Definitions */
#if 0 /* Under Voltage Simulation check*/
extern U32 dsg_limit_check_counter_u32;
#endif
/* @} */

/* Public Function Prototypes **/
extern void bms_protection_task_v(void *parameter);

#endif /* APPLICATION_LAYER_OS_TASK_BMS_PROTECTION_TASK_H_ */
