/**
========================================================================================================================
@page can_commtask_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	can_commtask.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	19-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_BUSINESS_LOGICS_CAN_COMMTASK_H_
#define APPLICATION_LAYER_BUSINESS_LOGICS_CAN_COMMTASK_H_

/* Includes for definitions */

/** @{
 * Type definition */
typedef enum can_commtask_starting_ending_id_te_tag
{
	CAN_COMMTASK_DEBUG_STARTING_ID 	= 0x0800DAFA	,
	CAN_COMMTASK_DEBUG_ENDING_ID	= 0x08FFDAFA

	/* Add for charging & driving Starting & End ID's here */

}can_commtask_starting_ending_id_te;

typedef struct  can_comm_task_can_msg_tst_tag   //__attribute__((packed))
{
    U32 id_u32;       /*!< ID of the message */
    U8 data_au8[8];   /*!< Data bytes of the CAN message*/
    U8 length_u8;    /*!< Length of payload in bytes */
} can_comm_task_can_msg_tst;

/** @} */
/** @{
 *  Public Variable Definitions */
typedef enum can_comm_sm_stage_te_tag
{
	DRIVING_SM = 0,
	CHARGING_SM = 1,
	TRACKING_SM = 2,
	DEBUG_SM = 3,
	CLIENT_SM = 4,
	MAX_SM_CAN_APP = 5,
}can_comm_sm_stage_te;


can_comm_sm_stage_te can_comm_stage;
/** @} */

/* Public Function Prototypes **/
extern void can_comm_task_v(void *parameter);
extern U8 can_commtask_identify_comm_type_u8(U32 can_rx_id_aru32);
extern void can_comm_set_tmr_v(can_comm_sm_stage_te tmr_type,U32 tmr_val);
#endif /* APPLICATION_LAYER_BUSINESS_LOGICS_CAN_COMMTASK_H_ */
