/**
========================================================================================================================
@page bms_curr_estimation_task_header_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_curr_estimation_task.h		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   	#1_TL#			
@n@b Date:       	30-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/
#ifndef APPLICATION_LAYER_OS_TASK_BMS_CURR_ESTIMATION_TASK_H_
#define APPLICATION_LAYER_OS_TASK_BMS_CURR_ESTIMATION_TASK_H_

/* Includes for definitions */
/* @{
 * Macro definition */

#include "batt_param.h"
#include "storage_def.h"
#include "operating_system.h"


/* @{
 * Macro definition */
#define START_ADDRESS_ENGY_TP_BLOCK 	0
#define END_ADDRESS_ENGY_TP_BLOCK		(START_ADDRESS_ENGY_TP_BLOCK + 1)

#define START_ADDRESS_ENGY_TP_SECTOR	(START_ADDRESS_ENGY_TP_BLOCK * STORAGE_DEF_SECTORS_PER_BLOCK)
#define END_ADDRESS_ENGY_TP_SECTOR		(END_ADDRESS_ENGY_TP_BLOCK * STORAGE_DEF_SECTORS_PER_BLOCK)

#define START_ADDRESS_ENGY_TP			(START_ADDRESS_ENGY_TP_BLOCK * STORAGE_DEF_SECTOR_SIZE * STORAGE_DEF_SECTORS_PER_BLOCK)
#define END_ADDRESS_ENGY_TP				(END_ADDRESS_ENGY_TP_BLOCK * STORAGE_DEF_SECTOR_SIZE * STORAGE_DEF_SECTORS_PER_BLOCK)

/* @} */

/* @{
 * Type definition */
/* Type definition */
typedef struct energy_cwh_calc_tst_tag
{
	/* kWh Calc Param */
    //S32 engy_50ms_cwh_s32;
    U64 engy_1s_cwh_u64;
   // U64 engy_1m_tp_cwh_u64;
   // U64 engy_1m_ah_cwh_u64;
  //  S32 engy_1h_cwh_s32;
} energy_cwh_calc_tst;
/* @} */

/* @{
 *  Public Variable Definitions */
extern double ocv_soc_sfp;
extern U8 power_limit_chg_flag_u8;
extern U8 power_limit_dsg_flag_u8;

extern batt_param_energy_tst batt_param_update_energy_gst;

extern energy_cwh_calc_tst energy_cwh_calc_gast;

extern BOOL update_1min_values_b;
extern BOOL update_1sec_values_b;

extern BOOL chg_vtg_pwt_lmt_b ;
extern BOOL chg_temp_pwt_lmt_b ;
extern BOOL chg_soc_pwt_lmt_b ;
extern BOOL chg_curr_pwt_lmt_b ;
extern BOOL dsg_vtg_pwt_lmt_b ;
extern BOOL dsg_temp_pwt_lmt_b ;
extern BOOL dsg_soc_pwt_lmt_b ;
extern BOOL dsg_curr_pwt_lmt_b ;
/* @} */

/* Public Function Prototypes **/
extern void bms_soc_estimation_task_v(void * parameter);
extern void bms_curr_soc_task_callback_v(void);
extern U8 engy_tp_flash_init_u8(void);
#endif /* APPLICATION_LAYER_OS_TASK_BMS_CURR_ESTIMATION_TASK_H_ */
