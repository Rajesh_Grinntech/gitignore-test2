/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "common_header.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern void SystemClock_Config(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCU_ALL_EN_PSW_PTA1_Pin GPIO_PIN_1
#define MCU_ALL_EN_PSW_PTA1_GPIO_Port GPIOA
#define MCU_ADC_FUSE_SENS_Pin GPIO_PIN_2
#define MCU_ADC_FUSE_SENS_GPIO_Port GPIOA
#define MCU_WP_FM_PTA3_Pin GPIO_PIN_3
#define MCU_WP_FM_PTA3_GPIO_Port GPIOA
#define MCU_RESET_FM_PTA4_Pin GPIO_PIN_4
#define MCU_RESET_FM_PTA4_GPIO_Port GPIOA
#define FM_SPI1_SCK_Pin GPIO_PIN_5
#define FM_SPI1_SCK_GPIO_Port GPIOA
#define FM_SPI1_MISO_Pin GPIO_PIN_6
#define FM_SPI1_MISO_GPIO_Port GPIOA
#define FM_SPI1_MOSI_Pin GPIO_PIN_7
#define FM_SPI1_MOSI_GPIO_Port GPIOA
#define MCU_CS_FM_PTB0_Pin GPIO_PIN_0
#define MCU_CS_FM_PTB0_GPIO_Port GPIOB
#define MCU_STB_CAN_PTB1_Pin GPIO_PIN_1
#define MCU_STB_CAN_PTB1_GPIO_Port GPIOB
#define MCU_EN_CAN_5V_PTA8_Pin GPIO_PIN_8
#define MCU_EN_CAN_5V_PTA8_GPIO_Port GPIOA
#define AFE_I2C1_SCL_Pin GPIO_PIN_9
#define AFE_I2C1_SCL_GPIO_Port GPIOA
#define AFE_I2C1_SDA_Pin GPIO_PIN_10
#define AFE_I2C1_SDA_GPIO_Port GPIOA
#define MCU_RST_SHUT_AFE_PTB4_Pin GPIO_PIN_4
#define MCU_RST_SHUT_AFE_PTB4_GPIO_Port GPIOB
#define MCU_DFETOFF_AFE_PTB5_Pin GPIO_PIN_5
#define MCU_DFETOFF_AFE_PTB5_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
