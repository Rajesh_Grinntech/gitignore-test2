/**
====================================================================================================================================================================================
@page flash_drv_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	flash_drv.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	16-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */


/* Application Header File */
//#include "boot_loader_app.h"
/* Configuration Header File */


/* This header file */
#include "flash_drv.h"
#include "app_main.h"
#include "gt_system.h"
/* Driver header */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
static void flash_drv_pin_init_prv(void);
static U8 flash_drv_verify_and_config_pru8(void);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
flash_drv_gen_tst flash_drv_general_cfg_st;
U8 flash_drv_status_reg_1_u8;

/** @} */

/** @{
 *  Public Variable Definitions */
U8 flash_drv_write_sts_u8;
U8 flash_drv_sts_check_tmr_cnt_u8;
U8 flash_drv_flash_operation_sts_u8;
U16 sector_count_u16 = 2;
U16 block_count_u16 = 1;
flash_drv_flash_info_tst flash_drv_flash_info_st;
flash_drv_sector_erase_tst flash_drv_sector_erase_st;
flash_drv_blocks_erase_tst flash_drv_blocks_erase_st;
flash_drv_addr_data_tst flash_drv_addr_data_st;

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/

U8 flash_drv_init_u8(void)
{
	U8 ret_val_u8 = 0;

	/* #1 Configure the Pin belongs to flash */
	flash_drv_pin_init_prv();


	/* #2 Validate the IC communication and Update the Global Structures */
	ret_val_u8 = flash_drv_verify_and_config_pru8();

	if(ret_val_u8)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_SPI_FLASH_INIT);
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_SPI_FLASH_INIT);
	}
//	spi_app_master_deinit_u8();
//	jump_to_user_application_v(*((U32*) APPLICATION_ADDRESS_1 ),
//			*((U32*) (APPLICATION_ADDRESS_1 + 4)));

	return(ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void flash_drv_pin_init_prv(void)
{
	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
		W25Q_CHIP_SELECT_DISABLE;
//		W25Q_HW_RESET_DISABLE;
//		W25Q_WRITE_PROTECT_DISABLE;
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
		CS25FL_CHIP_SELECT_DISABLE;
		CS25FL_HW_RESET_DISABLE;
		CS25FL_WRITE_PROTECT_DISABLE;
	#endif
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static U8 flash_drv_verify_and_config_pru8(void)
{
	/* Local Variable */
	U8 ret_val_u8 = 0;
	flash_drv_general_cfg_st.lock_b = COM_HDR_TRUE;

	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
	if(w25q_read_id_and_blocks_u8(&flash_drv_general_cfg_st.id_e, &flash_drv_general_cfg_st.blockcount_u32))
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
	if(cs25fl_read_id_and_blocks_u8(&flash_drv_general_cfg_st.id_e,&flash_drv_general_cfg_st.blockcount_u32))
	#endif
	{
		flash_drv_general_cfg_st.lock_b = COM_HDR_FALSE;
		return ret_val_u8 + 1;
	}

	if(flash_drv_general_cfg_st.blockcount_u32 > 256)
	{
		flash_drv_general_cfg_st.addr_4byte_enable_b = COM_HDR_TRUE;
	}
	else
	{
		flash_drv_general_cfg_st.addr_4byte_enable_b = COM_HDR_FALSE;
	}
	flash_drv_general_cfg_st.pagesize_u16 = 256;
	flash_drv_general_cfg_st.sectorsize_u32 = 0x1000;

	flash_drv_general_cfg_st.sectorcount_u32 = flash_drv_general_cfg_st.blockcount_u32 * 16;
	flash_drv_general_cfg_st.pagecount_u32 = (flash_drv_general_cfg_st.sectorcount_u32
			* flash_drv_general_cfg_st.sectorsize_u32) / flash_drv_general_cfg_st.pagesize_u16;
	flash_drv_general_cfg_st.blocksize_u32 = flash_drv_general_cfg_st.sectorsize_u32 * 16;
	flash_drv_general_cfg_st.capacity_in_kb_u32 = (flash_drv_general_cfg_st.sectorcount_u32
			* flash_drv_general_cfg_st.sectorsize_u32) / 1024;


	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
		ret_val_u8 = w25q_read_uniq_id_u8(flash_drv_general_cfg_st.uniq_id_au8);
		ret_val_u8 += w25q_read_status_registers_u8(1 , &flash_drv_general_cfg_st.status_reg1_u8);
		ret_val_u8 += w25q_read_status_registers_u8(2 , &flash_drv_general_cfg_st.status_reg2_u8);
		ret_val_u8 += w25q_read_status_registers_u8(3 , &flash_drv_general_cfg_st.status_reg3_u8);
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
		ret_val_u8 = cs25fl_read_uniq_id_u8(flash_drv_general_cfg_st.uniq_id_au8);
		ret_val_u8 += cs25fl_read_status_registers_u8(1 , &flash_drv_general_cfg_st.status_reg1_u8);
		ret_val_u8 += cs25fl_read_status_registers_u8(2 , &flash_drv_general_cfg_st.status_reg2_u8);
	#endif

	flash_drv_general_cfg_st.lock_b = COM_HDR_FALSE;

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_erase_flash_u8(flash_drv_erase_mode_te mode_are, U32 addr_aru32,U16 erase_wait_time_in_ms_aru16)
{
	U8 ret_val_u8 = 0;
	while(flash_drv_general_cfg_st.lock_b)
	{
		//vTaskDelay(FLASH_DELAY_TICKS);
	}
	flash_drv_general_cfg_st.lock_b = COM_HDR_TRUE;

	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
		/* ret_val_u8 += w25q_write_enable_u8();
		ret_val_u8 += w25q_wait_for_write_end_u8(); */
		ret_val_u8 += w25q_app_erase_u8(mode_are, addr_aru32, flash_drv_general_cfg_st.addr_4byte_enable_b, erase_wait_time_in_ms_aru16);
		/* Wait until this event complete */
		/* W25Q_Wait_For_Write_End(); //erase wait busy */
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
		ret_val_u8 += cs25fl_app_erase_u8(mode_are,addr_aru32,flash_drv_general_cfg_st.addr_4byte_enable_b,erase_wait_time_in_ms_aru16);
	#endif

	flash_drv_general_cfg_st.lock_b = COM_HDR_FALSE;

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_writebyte_u8(U8 *flash_data_arpu8 ,U32 flash_wr_start_addr_aru32,U32 data_length_aru32)
{
	U8 flash_write_data_au8[400];
	U16 flash_write_data_length_u16 = 0;
	U8 return_write_status_u8 = 0;
	if (flash_drv_general_cfg_st.addr_4byte_enable_b)
	{
		#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
			//TODO:W25Q_PAGE_PROGRAM_CMD should be changed for 4 bytes command
			flash_write_data_au8[flash_write_data_length_u16++] = W25Q_PAGE_PROGRAM_CMD[0];
		#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = CS25FL_4B_PAGE_PROGRAM_CMD[0];
		#endif
		flash_write_data_au8[flash_write_data_length_u16++] = ((flash_wr_start_addr_aru32 & 0xFF000000) >> 24);
	}
	else
	{
		#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = W25Q_PAGE_PROGRAM_CMD[0];
		#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = CS25FL_PAGE_PROGRAM_CMD[0];
		#endif

	}
	flash_write_data_au8[flash_write_data_length_u16++] = ((flash_wr_start_addr_aru32 & 0xFF0000) >> 16);
	flash_write_data_au8[flash_write_data_length_u16++] = ((flash_wr_start_addr_aru32 & 0xFF00) >> 8);
	flash_write_data_au8[flash_write_data_length_u16++] = ((flash_wr_start_addr_aru32 & 0xFF));


	for (U16 data_count_u16 = 0; data_count_u16 < data_length_aru32; data_count_u16++)
	{
		flash_write_data_au8[flash_write_data_length_u16++] = flash_data_arpu8[data_count_u16];
	}
	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
		/* W25Q_Wait_For_Write_End();
		W25Q_Write_Enable(); */
		return_write_status_u8 += w25q_app_write_data_u8(flash_write_data_au8,flash_write_data_length_u16);
		/* W25Q_Wait_For_Write_End();
		W25Q_Write_Disable(); */
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
		return_write_status_u8 += cs25fl_app_write_data_u8(flash_write_data_au8,flash_write_data_length_u16);
	#endif

	return return_write_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_readbyte_u8(U8 *flash_rd_data_arpu8 ,U32 flash_rd_start_addr_aru32,U32 flash_rd_data_length_aru32)
{
	U8 flash_write_data_au8[300];
	U16 flash_write_data_length_u16 = 0;
	U8 return_write_status_u8 = 3;
	if (flash_drv_general_cfg_st.addr_4byte_enable_b)
	{
		#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = W25Q_FAST_READ_CMD[0];        //TODO: Change for 4 bytes command
		#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = CS25FL_4B_FAST_READ_CMD[0];
		#endif
		flash_write_data_au8[flash_write_data_length_u16++] = ((flash_rd_start_addr_aru32 & 0xFF000000) >> 24);
	}
	else
	{
		#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = W25Q_FAST_READ_CMD[0];
		#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
			flash_write_data_au8[flash_write_data_length_u16++] = CS25FL_FAST_READ_CMD[0];
		#endif
	}

	flash_write_data_au8[flash_write_data_length_u16++] = ((flash_rd_start_addr_aru32 & 0xFF0000) >> 16);
	flash_write_data_au8[flash_write_data_length_u16++] = ((flash_rd_start_addr_aru32 & 0xFF00) >> 8);
	flash_write_data_au8[flash_write_data_length_u16++] = ((flash_rd_start_addr_aru32 & 0xFF));
	flash_write_data_au8[flash_write_data_length_u16++] = 0 ; // for fast read cmd
	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
		return_write_status_u8 = w25q_app_read_data_u8(flash_write_data_au8,flash_write_data_length_u16,
															flash_rd_data_arpu8,flash_rd_data_length_aru32);
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
		return_write_status_u8 = cs25fl_app_read_data_u8(flash_write_data_au8,flash_write_data_length_u16,
														 flash_rd_data_arpu8,flash_rd_data_length_aru32);
	#endif
	return return_write_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_app_wr_data_u8(U8 *data_arpu8 ,U32 *write_start_addr_arpu32,U32 data_length_aru32)
{

	U16 temp_no_of_bytes_u16 = 0;
    U8 spi_flash_status_u8 = 0;
    U16 data_counter_u16 = 0;

	U32 page_check_u32 = SPI_FLASH_DEFAULT_VALUE;
	page_check_u32  = (U32)(*write_start_addr_arpu32 & 0xFFFFFF00);
	page_check_u32  =  page_check_u32 + FLASH_DRV_FLASH_PAGE_SIZE;


	if(*write_start_addr_arpu32>(SPI_FLASH_MAX_ADDR - 0xC8))
	{
		spi_flash_status_u8++;
		return spi_flash_status_u8;
	}
	else if( (page_check_u32-*write_start_addr_arpu32)<(data_length_aru32))
	{
		temp_no_of_bytes_u16  = (page_check_u32 - *write_start_addr_arpu32);
		spi_flash_status_u8 = flash_drv_writebyte_u8(data_arpu8,*write_start_addr_arpu32,temp_no_of_bytes_u16);
		*write_start_addr_arpu32 = page_check_u32;
		for(data_counter_u16 = 0;data_counter_u16<temp_no_of_bytes_u16;data_counter_u16++)
			data_arpu8++;
		spi_flash_status_u8 += flash_drv_writebyte_u8(data_arpu8,*write_start_addr_arpu32, (data_length_aru32 - temp_no_of_bytes_u16));
		*write_start_addr_arpu32 = *write_start_addr_arpu32 + (data_length_aru32 - temp_no_of_bytes_u16);
	}
	else
	{
		spi_flash_status_u8 += flash_drv_writebyte_u8(data_arpu8,*write_start_addr_arpu32,data_length_aru32);
		*write_start_addr_arpu32 = *write_start_addr_arpu32 + data_length_aru32;
	}
	return spi_flash_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_app_rd_data_u8(U8 *read_data_arpu8 ,U32 read_start_addr_aru32,U32 read_data_length_aru32)
{
	U16 temp_no_of_bytes_u16 = 0;
    U8 spi_flash_status_u8 = 0;
	U32 page_check_u32 = SPI_FLASH_DEFAULT_VALUE;
	U16 data_counter_u16 = 0;
	page_check_u32  = (U32)(read_start_addr_aru32 & 0xFFFFFF00);
	page_check_u32  =  page_check_u32 + FLASH_DRV_FLASH_PAGE_SIZE;
	if(read_start_addr_aru32>(SPI_FLASH_MAX_ADDR - 0xC8))
	{
		spi_flash_status_u8++;
		return spi_flash_status_u8;
	}
	else if( (page_check_u32-read_start_addr_aru32)<(read_data_length_aru32))
	{
		temp_no_of_bytes_u16  = (page_check_u32 - read_start_addr_aru32);
		spi_flash_status_u8 = flash_drv_readbyte_u8(read_data_arpu8,read_start_addr_aru32,temp_no_of_bytes_u16);
		read_start_addr_aru32 = page_check_u32;
		for(data_counter_u16 = 0;data_counter_u16<temp_no_of_bytes_u16;data_counter_u16++)
			read_data_arpu8++;
		spi_flash_status_u8 += flash_drv_readbyte_u8(read_data_arpu8,read_start_addr_aru32, (read_data_length_aru32 - temp_no_of_bytes_u16));
	}
	else
	{
		spi_flash_status_u8 += flash_drv_readbyte_u8(read_data_arpu8,read_start_addr_aru32,read_data_length_aru32);
	}
	return spi_flash_status_u8;

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void flash_drv_erase_cp_dp_data_v(void)
{
	if(flash_drv_flash_info_st.erase_mode_u8 == FLASH_DRV_ERASE_MODE_SECTOR)
	{
		if((flash_drv_flash_info_st.erase_sts_u8 == FLASH_DRV_SECTOR_ERASE_CYCLE_COMPLETED) && (sector_count_u16 > 1))
		{
			flash_drv_flash_info_st.erase_sts_u8 = FLASH_DRV_SECTOR_ERASE_CYCLE_INPROGRESS;

			flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR, (sector_count_u16 * 4096),MAX_SECTOR_ERASE_TIME_MS);
		}
	}

	if(flash_drv_flash_info_st.erase_mode_u8 == FLASH_DRV_ERASE_MODE_BLOCK)
	{
		if((flash_drv_flash_info_st.erase_sts_u8 == FLASH_DRV_BLOCK_ERASE_CYCLE_COMPLETED) && (block_count_u16 >= 1))
		{
			flash_drv_flash_info_st.erase_sts_u8 = FLASH_DRV_BLOCK_ERASE_CYCLE_INPROGRESS;
			flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_BLOCK, (block_count_u16 * 16 * 4096),MAX_BLOCK_ERASE_TIME_MS);
		}
	}
}


#if 0  /* Blocking method approach */
void flash_drv_erase_cp_dp_data_v(void)
{

	/* TODO: uncomment first for loop & remove 2 nd for loop when common_variable file is included */
	/*Erasing (Block0: From Sector 2 to Sector 16) */
	/* for(sector_count_u16 = 2;sector_count_u16 < COMM_VAR_FLASH_NO_OF_ERACE_SECTOR;sector_count_u16++) */
	 for(sector_count_u16 = 2;sector_count_u16 < 17;sector_count_u16++)
	{
		 flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR, (sector_count_u16 * 4096),MAX_SECTOR_ERASE_TIME_MS);
	}
	/* for (Block_count_u16 = 1; Block_count_u16 < COMM_VAR_FLASH_NO_OF_BLOCKS - 1; Block_count_u16++) */		/* Erasing (Block 1 - Block 255) */
	/* for (Block_count_u16 = 1; Block_count_u16 < 64 - 1; Block_count_u16++) */								/* 64 = Blocks                   */
	for (Block_count_u16 = 1; Block_count_u16 < 512 - 1; Block_count_u16++)										/* 512 = Blocks                  */
	{
		flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_BLOCK, (Block_count_u16* 16 *4096),MAX_BLOCK_ERASE_TIME_MS);
	}
	//taskEXIT_CRITICAL();
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_wait_for_wr_end_no_blocking_u8()
{
	U8 ret_val_u8 = 0;

	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
	ret_val_u8 = w25q_wait_for_wr_end_no_blocking_u8(&flash_drv_status_reg_1_u8);

	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
	ret_val_u8 = cs25fl_wait_for_wr_end_no_blocking_u8(&flash_drv_status_reg_1_u8);
	#endif

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 flash_drv_enable_sleep_u8(void)
{
	U8 ret_val_u8 = 0;

	#if (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_WINBOND_FLASH)
	ret_val_u8 = w25q_power_down_u8();
	#elif (FLASH_DRV_FLASH_IC_USED == FLASH_DRV_CYPRESS_FLASH)
	/*TODO: Need to create power down function for Cypresss */
	#endif

	return (ret_val_u8);
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
