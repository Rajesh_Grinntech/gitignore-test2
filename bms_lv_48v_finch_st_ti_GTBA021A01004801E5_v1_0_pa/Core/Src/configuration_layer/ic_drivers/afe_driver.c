/**
========================================================================================================================
@page afe_driver_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	afe_driver.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	30-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

#include "gt_system.h"
//#include "diagmonitor_task.h"
#include "protect_app.h"
#include "cell_bal_app.h"
#include "batt_param.h"

/* This header file */
#include "afe_driver.h"

/* Driver header */
#include "gt_timer.h"
#include "gt_memory.h"
#include "task.h"


/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

afe_driver_diag_status_tun afe_driver_diag_status_un;
/** @} */

/** @{
 *  Public Variable Definitions */
afe_driver_data_tst afe_driver_data_gst;

afe_driver_cc_soc_tst afe_driver_cc_soc_gst;

bq76952_all_values_tst bq76952_all_values_st;

bq76952_config_tst bq76952_config_st;

U16 cb_rx_reg_u16 = 0;
/** @} */

/* Public Function Definitions */

U8 afe_driver_init_u8(void);
U8 afe_driver_configuration_u8(void);
U8 afe_driver_read_all_parameter_u8(void);
U8 afe_driver_read_current_u8(S16* curr_val_arps16);

U8 afe_driver_balancing_fet_control_u8(U16 cb_bal_reg_aru16);
U8 afe_driver_fet_control_u8(afe_driver_fet_te afe_driver_fet_are,afe_driver_fet_state_te afe_driver_fet_state_are);

U8 afe_driver_enter_sleep_mode_u8(void);
U8 afe_driver_wakeup_u8(void);

U8 afe_driver_read_reg_u8(U8 mode_arg_au8);

//U8 afe_driver_diagnostics_u8(void);
/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_init_u8(void)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	/* Clear the data structure */
	//memory_copy_u8_array_v((U8*)afe_driver_data_gst, 0, sizeof(afe_driver_data_tst));

	/* Update status registers from AFE */
	ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);
	ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);

	/*To wake from Deep Sleep - Mandatory*/
	if(bq76952_control_stat_u.bits.deep_sleep_b1 == COM_HDR_TRUE)
	{
		ret_val_u8 += bq76952_exit_sleep_u16(SLEEP_DEEP);

		/* Update status registers from AFE */

		ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);
		ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);
	}



	/* Initialize and up the AFE module */
	ret_val_u8 += bq76952_init_u16();

	if(ret_val_u8 != 0)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_AFE_START);
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_AFE_START);
	}

	ret_val_u8 +=  afe_driver_configuration_u8();
#endif
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_configuration_u8(void)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	bq76952_config_st.prot_vtg_config_st.cell_ov_st.thres_1mv_u16 = 0;//afe_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16;
	bq76952_config_st.prot_vtg_config_st.cell_ov_st.recv_1mv_u16 = 0;//afe_config_nv_cell_cfg_gst.release_max_cell_1mV_u16;

	bq76952_config_st.prot_vtg_config_st.cell_uv_st.thres_1mv_u16 = 0;//afe_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	bq76952_config_st.prot_vtg_config_st.cell_uv_st.recv_1mv_u16 = 0;//afe_config_nv_cell_cfg_gst.release_min_cell_1mV_u16;


	/*LDO Configure*/
	bq76952_config_st.ldo_configure_st.reg1_state_e = LDO_ON;
	bq76952_config_st.ldo_configure_st.reg1_vtg_e = REGOUT_3V3;
	bq76952_config_st.ldo_configure_st.reg2_state_e = LDO_OFF;
	bq76952_config_st.ldo_configure_st.reg2_vtg_e = REGOUT_3V3;
	bq76952_config_st.ldo_configure_st.regin_src_e = REGIN_PREREG;

	/* Fet Configure*/
	bq76952_config_st.fet_configure_st.pchg_start_vtg_1mv_u16 =  0;//3300;
	bq76952_config_st.fet_configure_st.pchg_stop_vtg_1mv_u16 = 0;//3350;
	bq76952_config_st.fet_configure_st.pdsg_delta_1mv_u16 = 0;//500;
	bq76952_config_st.fet_configure_st.pdsg_timeout_1ms_u16 = 0;//50;
	bq76952_config_st.fet_configure_st.body_diode_thres_1ma_u16 = 100;
	bq76952_config_st.fet_configure_st.fet_init_state_b = COM_HDR_FALSE;//COM_HDR_TRUE;
	bq76952_config_st.fet_configure_st.fet_pdsg_en_b = COM_HDR_FALSE;//COM_HDR_TRUE;
	bq76952_config_st.fet_configure_st.fet_series_en_b = COM_HDR_FALSE;

	bq76952_config_st.fet_configure_st.fet_test_mode_b = COM_HDR_TRUE;

	/* Sleep Configure*/
	bq76952_config_st.sleep_configure_st.dpslp_ldo_en_b 			= COM_HDR_TRUE;
	bq76952_config_st.sleep_configure_st.dpslp_lfo_en_b				= COM_HDR_FALSE;
	bq76952_config_st.sleep_configure_st.schgr_pk_tos_del_1cv_s16	= 200;
	bq76952_config_st.sleep_configure_st.schgr_thres_1cv_s16		= 7000;
	bq76952_config_st.sleep_configure_st.slp_meas_time_1s_u8		= 5;
	bq76952_config_st.sleep_configure_st.slp_strt_curr_1ma_s16		= 500;
	bq76952_config_st.sleep_configure_st.slp_wk_comp_curr_1ma_s16	= 500;

	/* Alarm Configure*/
	bq76952_config_st.alarm_configure_st.full_scan_alert_b 			= COM_HDR_FALSE;
	bq76952_config_st.alarm_configure_st.wakeup_alert_b				= COM_HDR_TRUE;

	/* Cell Balancing Configure*/
	bq76952_config_st.cb_config_st.auto_en_u8						= COM_HDR_FALSE;
	bq76952_config_st.cb_config_st.max_cells_u8						= 16;
	bq76952_config_st.cb_config_st.duration_intvl_u8				= 80; /* 80 seconds*/
	bq76952_config_st.cb_config_st.min_cell_temp_s8					= -20; /* -20C*/ /* Temperature*/
	bq76952_config_st.cb_config_st.max_cell_temp_s8					= 60;  /* 60C */
	bq76952_config_st.cb_config_st.max_int_temp_s8					= 70;  /* 70C */

	if(COM_HDR_TRUE == bq76952_config_st.cb_config_st.auto_en_u8)
	{
		bq76952_config_st.cb_config_st.sleep_en_u8						= COM_HDR_FALSE;
		bq76952_config_st.cb_config_st.min_cell_vtg_u16					= 3000; /*3000mV*/
		bq76952_config_st.cb_config_st.start_del_val_u8					= 50;
		bq76952_config_st.cb_config_st.stop_del_val_u8					= 20;
		bq76952_config_st.cb_config_st.start_chg_curr_thres_s16			= 500; /*500mA - limit 32767mA*/
		bq76952_config_st.cb_config_st.chg_curr_thres_s16				= 500; /*500mA - limit 32767mA*/
		bq76952_config_st.cb_config_st.dsg_curr_thres_s16				= 500; /*500mA - limit 32767mA*/ // ????
		bq76952_config_st.cb_config_st.meas_time_intvl_u8				= 10;
	}

	/* Configuration API call */
	ret_val_u8 += bq76952_configure_u16(&bq76952_config_st);

	/*To wake from Normal Sleep*/
	/* This condition checks whether Sleep is permitted or not. Not checks whether in sleep or not*/
	if(bq76952_batt_stat_u.bits.sleep_en_b1 == COM_HDR_TRUE)
	{
		ret_val_u8 += bq76952_exit_sleep_u16(SLEEP_NORMAL);
	}

#endif
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_read_all_parameter_u8(void)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	/* Update status registers from AFE */
 	ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);
	/*To wake from Normal Sleep*/
	/* This condition checks whether Sleep is permitted or not. Not checks whether in sleep or not*/
	if(bq76952_batt_stat_u.bits.sleep_en_b1 == COM_HDR_TRUE)
	{
		ret_val_u8 += bq76952_exit_sleep_u16(SLEEP_NORMAL);
	}
	ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);
	ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);
	ret_val_u8 += bq76952_get_ic_stat_u16(FET_STS);
	ret_val_u8 += bq76952_get_ic_stat_u16(ALRM_STS);

	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
	ret_val_u8 += bq76952_get_all_prot_info_u16();
	ret_val_u8 += bq76952_get_all_pf_info_u16();

	if(ret_val_u8 != 0)
	{
		return ret_val_u8;
	}

	/* Get all the measuring parameters from AFE */
	ret_val_u8 += bq76952_get_all_values_u16(&bq76952_all_values_st);


//	bq76952_all_values_st.cell_voltage_au16[12] = bq76952_all_values_st.cell_voltage_au16[15];
//	bq76952_all_values_st.cell_voltage_au16[13] = bq76952_all_values_st.cell_voltage_au16[15];
	bq76952_all_values_st.cell_voltage_au16[13] = bq76952_all_values_st.cell_voltage_au16[15];
//	bq76952_all_values_st.pack_pin_vtg_u16 =
//	bq76952_all_values_st.cell_voltage_au16[1] = 4260;  //Test for cell over voltage
//	bq76952_all_values_st.cell_voltage_au16[2] = 2500;
	memory_copy_u8_array_v((U8*)afe_driver_data_gst.cell_voltage_au16, (U8*)bq76952_all_values_st.cell_voltage_au16,  (2 * AFE_NO_OF_CELLLS_USED));

	afe_driver_data_gst.int_temp_1cc_s16 = bq76952_all_values_st.int_therm_1cc_s16;

	afe_driver_data_gst.cell_temp_1cc_as16[0] = bq76952_all_values_st.ts3_val_1cc_s16;
	afe_driver_data_gst.cell_temp_1cc_as16[1] = bq76952_all_values_st.ts2_val_1cc_s16;
	afe_driver_data_gst.cell_temp_1cc_as16[2] = bq76952_all_values_st.ddsg_temp_1cc_s16;//bq76952_all_values_st.ts3_val_1cc_s16;
	afe_driver_data_gst.cell_temp_1cc_as16[3] = bq76952_all_values_st.cfet_off_temp_1cc_s16;//bq76952_all_values_st.cfet_off_temp_1cc_s16;

	afe_driver_data_gst.mfet_temp_1cc_s16 = bq76952_all_values_st.ts1_val_1cc_s16;//bq76952_all_values_st.ddsg_temp_1cc_s16;
	afe_driver_data_gst.pack_temp_1cc_s16 = 0;//bq76952_all_values_st.dchg_temp_1cc_s16;

	afe_driver_data_gst.pack_pin_vtg_u16 = bq76952_all_values_st.pack_pin_vtg_u16;
	afe_driver_data_gst.tos_vtg_u16 = bq76952_all_values_st.tos_vtg_u16;
	afe_driver_data_gst.ld_pin_vtg_u16 = bq76952_all_values_st.ld_pin_vtg_u16;

	//afe_driver_data_gst.fuse_pin_vtg_u16 = 0;
#endif
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_read_current_u8(S16* curr_val_arps16)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE

	/*#1 Update Current - CC2  from BQ76952 */
	ret_val_u8 += bq76952_get_current_u16(CC2, curr_val_arps16 );

#endif
	return (ret_val_u8);
}

#if 0
/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_start_adc_measurement_u8(afe_driver_adc_mode_te mode_arg_te)
{
	U8 ret_val_u8 = 0;

	switch (mode_arg_te)
	{
		/**********************************************************************************************************************/
		case ADC_CELL_VOLTAGE:
		{
			/* TODO: Cell voltage measurement API can be called here */
		}
		break;
		/**********************************************************************************************************************/
		case ADC_GPIO_VOLTAGE:
		{
			/* TODO: Gpio voltage measurement API can be called here */
		}
		break;
		/**********************************************************************************************************************/
		case ADC_ADDITIONAL_PARAMETERS:
		{
			/* TODO: Other additional parameters measurement API can be called here */
		}
		break;
		/**********************************************************************************************************************/
		case ADC_ALL_PARAMETERS:
		{
			if(mc33772_meas_start_conv_u8())
			{
				ret_val_u8++;
			}
		}
		break;
		/**********************************************************************************************************************/
		default :
		{
			ret_val_u8++;
		}
		break;
		/**********************************************************************************************************************/
	}

	return (ret_val_u8);
}
#endif
/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_balancing_fet_control_u8(U16 cb_bal_reg_aru16)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	U16 wr_cb_reg_u16 = 0;
	U16 rd_cb_sts_u16 = 0;

switch(cell_bal_grp_e)
{
case BAL_GROUP_1:
	if(((cb_bal_reg_aru16 & 0x3) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = cb_bal_reg_aru16 & 0x3;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_2;
	}
	break;
case BAL_GROUP_2:
	if(((cb_bal_reg_aru16 & 0xC) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = cb_bal_reg_aru16 & 0xC;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_3;
	}
	break;
case BAL_GROUP_3:
	if(((cb_bal_reg_aru16 & 0x30) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = cb_bal_reg_aru16 & 0x30;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_4;
	}
	break;
case BAL_GROUP_4:
	if(((cb_bal_reg_aru16 & 0xC0) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = cb_bal_reg_aru16 & 0xC0;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_5;
	}
	break;
case BAL_GROUP_5:
	if(((cb_bal_reg_aru16 & 0x300) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = cb_bal_reg_aru16 & 0x300;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_6;
	}
	break;
case BAL_GROUP_6:
	if(((cb_bal_reg_aru16 & 0xC00) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = cb_bal_reg_aru16 & 0xC00;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_7;
	}
	break;
case BAL_GROUP_7: /* Only 1 cell in this grp - 13th cell mapped to 16th cell*/
	if(((cb_bal_reg_aru16 & 0x1000) != 0) && !cb_switch_req_b)
	{
		wr_cb_reg_u16 = /*cb_bal_reg_aru16 & */ 0x8000;
	}
	else
	{
		cb_switch_counter_u8 = 0;
		cb_switch_req_b = COM_HDR_FALSE;
		cell_bal_grp_e = BAL_GROUP_1;
	}
	break;
default:
	break;
}

	ret_val_u8 = bq76952_wr_cb_sts_u16(wr_cb_reg_u16);

	if(ret_val_u8 != 0)
	{
		return ret_val_u8;
	}

	ret_val_u8 = bq76952_rd_cb_sts_u16(&rd_cb_sts_u16);

	cb_rx_reg_u16 = rd_cb_sts_u16;

	if(rd_cb_sts_u16 != wr_cb_reg_u16)
	{
		ret_val_u8++;
	}

#endif
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_fet_control_u8(afe_driver_fet_te afe_driver_fet_are,afe_driver_fet_state_te afe_driver_fet_state_are)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	vTaskSuspendAll();
	if(bq76952_config_st.fet_configure_st.fet_test_mode_b)
	{
		ret_val_u8 = bq76952_set_fet_test_u16(afe_driver_fet_are, afe_driver_fet_state_are);
	}
	else
	{
		ret_val_u8 = bq76952_set_fet_u16(afe_driver_fet_are, afe_driver_fet_state_are);
	}

	if(ret_val_u8 == 0xFE)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_AFE_FET_MALFN);
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_AFE_READ);
		protect_app_check_afe_fet_malfn_v(COM_HDR_TRUE);
	}
	else if((ret_val_u8 == 0xFF) || (ret_val_u8 != 0))
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_AFE_READ);
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_AFE_FET_MALFN);
		protect_app_check_afe_violation_v(COM_HDR_TRUE);
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_AFE_READ);
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_AFE_FET_MALFN);

		protect_app_check_afe_violation_v(COM_HDR_FALSE);
		protect_app_check_afe_fet_malfn_v(COM_HDR_FALSE);
	}
	 xTaskResumeAll();
#endif
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_enter_sleep_mode_u8(void)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	ret_val_u8 += bq76952_enter_sleep_u16(SLEEP_NORMAL);//SLEEP_DEEP

	ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);
	ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);
	ret_val_u8 += bq76952_get_ic_stat_u16(FET_STS);

#endif
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_wakeup_u8(void)
{
	U8 ret_val_u8 = 0;
#if AFE_DRIVER_USE
	ret_val_u8 += bq76952_exit_sleep_u16(SLEEP_DEEP);
#endif
	return ret_val_u8;
}

#if 0
/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_read_flt_wakeup_reg_u8(void)
{
	U8 ret_val_u8 = 0;
	U8 slave_id_u8 = 0;

	/* Get all the fault register values from AFE */
	for (slave_id_u8 = SLAVE_IC_1; slave_id_u8 < SLAVE_NUM; slave_id_u8++)
	{
		ret_val_u8 = mc33772_rd_flt_mask_reg_u8(slave_id_u8,&afe_driver_data_gst[slave_id_u8-1].fault_wakeup_au16[0]);

		fault_register_tst[slave_id_u8 - 1].fault_status1_tun.fault_status1_byte_u16 =	afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[0];
		fault_register_tst[slave_id_u8 - 1].fault_status2_tun.fault_status2_byte_u16 =  afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[1];
		fault_register_tst[slave_id_u8 - 1].fault_status3_tun.fault_status3_byte_u16 =	afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[2];

		fault_mask_tst[slave_id_u8 - 1].fault_status1_tun.fault_status1_byte_u16 =	afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[3];
		fault_mask_tst[slave_id_u8 - 1].fault_status2_tun.fault_status2_byte_u16 =	afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[4];
		fault_mask_tst[slave_id_u8 - 1].fault_status3_tun.fault_status3_byte_u16 =	afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[5];

		fault_wakeup_tst[slave_id_u8 - 1].fault_status1_tun.fault_status1_byte_u16 = afe_driver_data_gst[slave_id_u8 -1 ].fault_wakeup_au16[6];
		fault_wakeup_tst[slave_id_u8 - 1].fault_status2_tun.fault_status2_byte_u16 = afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[7];
		fault_wakeup_tst[slave_id_u8 - 1].fault_status3_tun.fault_status3_byte_u16 = afe_driver_data_gst[slave_id_u8 - 1].fault_wakeup_au16[8];



	}
	afe_driver_clear_flt_flags_u8();
	return (ret_val_u8);
}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_clear_flt_flags_u8(void)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = mc33772_clr_flt_sts_reg_u8();

	return (ret_val_u8);
}
#endif

#if 0
/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
U8 afe_driver_read_reg_u8(U8 mode_arg_au8)
{
	U8 ret_val_u8 = 0;

	switch (mode_arg_au8)
	{
		/**********************************************************************************************************************/
		case BATT_STS:
		{
			ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);

		}
		break;
		/**********************************************************************************************************************/
		case CTRL_STS:
		{
			ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);
		}
		break;
		/**********************************************************************************************************************/
		case FET_STS:
		{
			ret_val_u8 += bq76952_get_ic_stat_u16(FET_STS);
		}
		break;
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}
	return (ret_val_u8);
}
#endif

#if 0
U8 afe_driver_fault_check_u8(void)
{
   U8 ret_val_u8 = 0;
   U8 slave_id_u8;

	/*Read all fault and mask registers*/
	afe_driver_read_flt_wakeup_reg_u8();
	U16 fault_register1_u16 = 0;
	U16 fault_register2_u16 = 0;
	U16 fault_register3_u16 = 0;
	U16 mask_register1_u16 = 0;
	U16 mask_register3_u16 = 0;

	/*read fault pin status*/


	for (slave_id_u8 = SLAVE_IC_1; slave_id_u8 < SLAVE_NUM; slave_id_u8++)
		{
		    mask_register1_u16 = fault_mask_tst[slave_id_u8 - 1].fault_status1_tun.fault_status1_byte_u16;
		    mask_register1_u16 = ~mask_register1_u16;
		    mask_register3_u16 = ~fault_mask_tst[slave_id_u8 - 1].fault_status3_tun.fault_status3_byte_u16;
			fault_register1_u16 = (fault_register_tst[slave_id_u8 - 1].fault_status1_tun.fault_status1_byte_u16 & mask_register1_u16);
			fault_register2_u16 = fault_register_tst[slave_id_u8 - 1].fault_status2_tun.fault_status2_byte_u16;
			fault_register3_u16 = fault_register_tst[slave_id_u8 - 1].fault_status3_tun.fault_status3_byte_u16  & mask_register3_u16;

			if(AFE_NO_FAULT != fault_register1_u16 || AFE_NO_FAULT != fault_register2_u16 || AFE_NO_FAULT != fault_register3_u16)
			{
				ret_val_u8 += AFE_FAULT;
				protect_app_check_afe_violation_v(COM_HDR_TRUE);
				diagmon_driver_init_fault_code_gu32 |= (1 << DIAG_BIT_AFE_HW_FAILED);

			}

		}


return ret_val_u8;
}

U8 afe_driver_diagnostics_u8(void)
{
	 U8 ret_val_u8 = 0;
	 U8 slave_id_u8 = 0;
	 static U8 status_u8 = SLAVE_IC_1;


//	for (slave_id_u8 = SLAVE_IC_1; slave_id_u8 < SLAVE_NUM; slave_id_u8++)
//	{
#ifdef DIAG_ADC1
		if ((ret_val_u8 = mc33772_test_diag_adc1(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}

		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.adc_a_b_fn_b = status_u8;
#endif
#ifdef DIAG_CELL_OV_UV
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_ovuv(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.cell_ov_uv_b = status_u8;
#endif

#ifdef DIAG_CTX_OPEN
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_ctx_open(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.cell_open_detect_b = status_u8;
#endif

#ifdef DIAG_CELL_VOLT
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_cellvolt(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.cell_volt_meas_b = status_u8;
#endif

#ifdef DIAG_CELL_LEAK
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_cellleak(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.cell_leak_b = status_u8;
#endif

#ifdef DIAG_CURRENT_MEAS
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_currentmeas(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.curr_meas_b = status_u8;
#endif

#ifdef DIAG_SHUNT_CONN
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_shuntconn(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.shunt_conn_b  = status_u8;
#endif

#ifdef DIAG_GPIOX_OT_UT
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_gpiox_otut(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.gpio_ot_ut_b = status_u8;
#endif

#ifdef DIAG_GPIOX_OPEN
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_gpiox_open(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.gpio_open_detect_b = status_u8;
#endif

#ifdef DIAG_CBX_OPEN
		status_u8 = 0;
		if ((ret_val_u8 = mc33772_test_diag_cbx_open(slave_id_u8,&status_u8)) != BCC_STATUS_SUCCESS)
		{
			return ret_val_u8;
		}
		afe_driver_diag_status_un[slave_id_u8-1].diag_status_st.cb_open_detect_b = status_u8;
#endif

		slave_id_u8++;
				if(slave_id_u8 < SLAVE_NUM)
				{
					status_u8 = SLAVE_IC_1;
				}

//	}
	return ret_val_u8;
}
#endif
