/**
====================================================================================================================================================================================
@page w25q_source_file

@subsection  Details

@n@b Title:
@n@b Filename:   	w25q.c
@n@b MCU:
@n@b Compiler:
@n@b Author:     	KRISH

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#
@n@b Date:       	22-Jul-2020
@n@b Description:  	Created

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */

/* Application Header File */

/* Configuration Header File */
#include "spi_app.h"

/* This header file */
#include "w25q.h"

/* Driver header */
#include "gt_timer.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
U32 ext_flash_id_u32;
/** @} */

/* Public Function Definitions */

/**
=====================================================================================================================================================

@fn Name			: w25q_read_id_and_blocks_u8
@b Scope            : Public
@n@n@b Description  : Read ID and block size
@param Input Data   : id_arpe(out), block_size_arpu32(out)
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_read_id_and_blocks_u8(w25qxx_id_te *id_arpe, U32* block_size_arpu32)
{
	U8 ret_val_u8 = 0 ;
	U32 id_u32 = 0;
	/* first byte to gold the dummy byte and last three byte will hold the ID */
	U8 rx_buf_au8[4];
	U32 new_baudrate_u32 = 0;
	/* spi_app_spi_master_reconfig(CS25FL_SPI_INSTANCE,0); */
	/*if(LPSPI_DRV_MasterConfigureBus(SPI_COM_1,&spi_com_1_master_config_0,&new_baudrate_u32))
	{
		ret_val_u8++;
	}*/

	W25Q_CHIP_SELECT_ENABLE;
	ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_READ_ID, 1, rx_buf_au8, 3);
	//ret_val_u8 =ucSpi_Master_Flash_Transmit_Receive(W25Q_SPI_INSTANCE, W25Q_READ_ID, 1, rx_buf_au8, 3);
	W25Q_CHIP_SELECT_DISABLE;

	ext_flash_id_u32 = (rx_buf_au8[0] << 16) | (rx_buf_au8[1]  << 8) | rx_buf_au8[2];

	switch (ext_flash_id_u32 & COM_HDR_MASK_BITS_15_0)
	{
	/***********************************************************************************************************************************************/
		case W25Q_W25Q512_:// 	w25q512
		{
			*id_arpe = W25Q_W25Q512;
			*block_size_arpu32 = 1024;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q256_:// 	w25q256
		{
			*id_arpe = W25Q_W25Q256;
			*block_size_arpu32 = 512;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q128_:// 	w25q128
		{
			*id_arpe = W25Q_W25Q128;
			*block_size_arpu32 = 256;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q64_://	w25q64
		{
			*id_arpe = W25Q_W25Q64;
			*block_size_arpu32 = 128;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q32_://	w25q32
		{
			*id_arpe = W25Q_W25Q32;
			*block_size_arpu32 = 64;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q16_://	w25q16
		{
			*id_arpe = W25Q_W25Q16;
			*block_size_arpu32 = 32;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q80_://	w25q80
		{
			*id_arpe = W25Q_W25Q80;
			*block_size_arpu32 = 16;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q40_://	w25q40
		{
			*id_arpe = W25Q_W25Q40;
			*block_size_arpu32 = 8;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q20_://	w25q20
		{
			*id_arpe = W25Q_W25Q20;
			*block_size_arpu32 = 4;
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_W25Q10_://	w25q10
		{
			*id_arpe = W25Q_W25Q10;
			*block_size_arpu32 = 2;
		}
		break;
		/***********************************************************************************************************************************************/
		default :
		{
			*id_arpe = 0;
			*block_size_arpu32 = 0;
			ret_val_u8 = 1; // Flash Mmeory  Config Reg Read Failed
		}
		break;
		/***********************************************************************************************************************************************/
	}

	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			: w25q_read_uniq_id_u8
@b Scope            : Public
@n@n@b Description  : Read the unique id of W25Q
@param Input Data   : uniq_id_arpu8
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_read_uniq_id_u8(U8* uniq_id_arpu8)
{
	U8 ret_val_u8 = 0;

	U8 tx_buffer_au8[5] = { 0 };

	/* Cmd + 4 dummy bytes */
	tx_buffer_au8[0] = W25Q_READ_UNIQ_ID[0];
	W25Q_CHIP_SELECT_ENABLE;
	ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, tx_buffer_au8, 5, uniq_id_arpu8, 8);
	W25Q_CHIP_SELECT_DISABLE;

	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			: w25q_read_status_registers_u8
@b Scope            : Public
@n@n@b Description  : Read status register of W25Q
@param Input Data   : reg_number_aru8, status_register_arpu8
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_read_status_registers_u8(U8 reg_number_aru8, U8* status_register_arpu8)
{
	U8 ret_val_u8 = 0;

	W25Q_CHIP_SELECT_ENABLE;
	if(1 == reg_number_aru8)
	{
		ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_RDSR1_CMD, 1, status_register_arpu8, 1);
	}
	else if(2 == reg_number_aru8)
	{
		ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_RDSR2_CMD, 1, status_register_arpu8, 1);
	}
	else if(3 == reg_number_aru8)
	{
		ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_RDSR3_CMD, 1, status_register_arpu8, 1);
	}
	W25Q_CHIP_SELECT_DISABLE;

	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			: w25q_write_enable_u8
@b Scope            : Public
@n@n@b Description  : Enable the write in W25Q
@param Input Data   : NULL
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_write_enable_u8(void)
{
	U8 ret_val_u8 = 0;
	W25Q_CHIP_SELECT_ENABLE;
	ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_WRITE_ENABLE_CMD, 1, NULL, 0);
	W25Q_CHIP_SELECT_DISABLE;
	return (ret_val_u8);
}

/* Not mandatory for Write Status Register, Erase/Program Security Registers,
 * Page Program, Quad Page Program, Sector Erase, Block Erase, Chip Erase and
 * Reset instructions. */


/**
=====================================================================================================================================================

@fn Name			: w25q_write_disable_u8
@b Scope            : Public
@n@n@b Description  : Disable the write W25Q
@param Input Data   : NULL
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/

U8 w25q_write_disable_u8(void)
{
	U8 ret_val_u8 = 0;
	W25Q_CHIP_SELECT_ENABLE;
	ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_WRITE_DISABLE_CMD, 1, NULL, 0);
	W25Q_CHIP_SELECT_DISABLE;
	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			: w25q_erase_u8
@b Scope            : Public
@n@n@b Description  : W25Q Erase data based on modes
@param Input Data   : mode_are, address_aru32, addr_4byte_en_arb
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_erase_u8(w25q_erase_mode_te mode_are, U32 address_aru32, BOOL addr_4byte_en_arb)
{
	U8 ret_val_u8 = 0;
	U8 tx_buffer_au8[5] = {0};
	/* Bytes starts with address later the command is appended */
	U8 tx_len_u8 = 1;

	/* set address if memory less than 16Mbytes - 3Byte address mode otherwise 4 Byte address mode */
	if (addr_4byte_en_arb)
	{
		tx_buffer_au8[tx_len_u8++] = (address_aru32 & COM_HDR_MASK_HI31_24_BITS_U32) >> COM_HDR_SHIFT_24_BITS;
	}

	tx_buffer_au8[tx_len_u8++] = (address_aru32 & COM_HDR_MASK_HI23_16_BITS_U32) >> COM_HDR_SHIFT_16_BITS;
	tx_buffer_au8[tx_len_u8++] = (address_aru32 & COM_HDR_MASK_LO15_8_BITS_U32) >> COM_HDR_SHIFT_8_BITS;
	tx_buffer_au8[tx_len_u8++] = (address_aru32 & COM_HDR_MASK_LO8_BITS_U32);
	W25Q_CHIP_SELECT_ENABLE;

	switch(mode_are)
	{
	/***********************************************************************************************************************************************/
		case W25Q_ERASE_MODE_FULL_CHIP:
		{
			tx_buffer_au8[0] = W25Q_CHIP_ERASE_CMD[0];
			ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, tx_buffer_au8, 1, NULL, 0);
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_ERASE_MODE_BLOCK:
		{
			tx_buffer_au8[0] = W25Q_BLOCK_ERASE_CMD[0];
			ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, tx_buffer_au8, tx_len_u8, NULL, 0);
		}
		break;
		/***********************************************************************************************************************************************/
		case W25Q_ERASE_MODE_SECTOR:
		{
			tx_buffer_au8[0] = W25Q_SECTOR_ERASE_CMD[0];
			ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, tx_buffer_au8, tx_len_u8, NULL, 0);
		}
		break;
		/***********************************************************************************************************************************************/
		default:
		{
			ret_val_u8++;
		}
		break;
		/***********************************************************************************************************************************************/
	}
	W25Q_CHIP_SELECT_DISABLE;
	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			: w25q_wait_for_write_end_u8
@b Scope            : Public
@n@n@b Description  : W25Q Wait for the write process to complete
@param Input Data   : wait_counter_ms_aru16
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_wait_for_write_end_u8(U16 wait_counter_ms_aru16)
{
	U8 ret_val_u8 = 0;
	U8 status_reg_1_u8 = 1;
    do
    {
    	W25Q_CHIP_SELECT_ENABLE;
    	spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_RDSR1_CMD, 1, &status_reg_1_u8, 1);
    	W25Q_CHIP_SELECT_DISABLE;
    	gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 1);
    	wait_counter_ms_aru16--;
    }while (((status_reg_1_u8 & 0x01) == 0x01) && wait_counter_ms_aru16 < 0);

    return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			: w25q_wait_for_write_end_no_blocking_v
@b Scope            : Public
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 w25q_wait_for_wr_end_no_blocking_u8(U8 *status_reg_1_u8)
{
	U8 ret_val_u8;

   	W25Q_CHIP_SELECT_ENABLE;
   	ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_RDSR1_CMD, 1, status_reg_1_u8, 1);
    W25Q_CHIP_SELECT_DISABLE;

    return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			: w25q_write_data_u8
@b Scope            : Public
@n@n@b Description  : W25Q write n bytes
@param Input Data   : data_arpu8, data_length_aru32
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_write_data_u8(U8 *data_arpu8,U32 data_length_aru32)
{
	U8 write_status_u8 = 0;
	W25Q_CHIP_SELECT_ENABLE;
	write_status_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, data_arpu8, data_length_aru32, NULL,0);
	W25Q_CHIP_SELECT_DISABLE;
	return write_status_u8;
}


/**
=====================================================================================================================================================

@fn Name			: w25q_read_data_u8
@b Scope            : Public
@n@n@b Description  : W25Q read n bytes
@param Input Data   : write_data_arpu8, write_data_length_aru32, read_data_arpu8, read_data_length_aru32
@return Return Value: Unsigned 8-bit  - 0 if success, non-zero if failed.

=====================================================================================================================================================
*/
U8 w25q_read_data_u8(U8 *write_data_arpu8,U32 write_data_length_aru32,U8 *read_data_arpu8,U32 read_data_length_aru32)
{
	U8 write_status_u8 = 0;
	U8 read_status_u8 = 0;
	W25Q_CHIP_SELECT_ENABLE;
	write_status_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, write_data_arpu8, write_data_length_aru32, NULL,0);

	if(write_status_u8 == 0)
	{
		read_status_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, NULL, 0, read_data_arpu8,read_data_length_aru32);
	}
	W25Q_CHIP_SELECT_DISABLE;

	return read_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 w25q_app_write_data_u8(U8 *flash_write_data_arpu8,U32 flash_write_data_length_aru16)
{
	U8 write_status_u8 = 0;

	w25q_wait_for_write_end_u8(flash_write_data_length_aru16);
	w25q_write_enable_u8();
	write_status_u8 = w25q_write_data_u8(flash_write_data_arpu8,flash_write_data_length_aru16);
	w25q_wait_for_write_end_u8(flash_write_data_length_aru16);
	w25q_write_disable_u8();

	return write_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 w25q_app_read_data_u8(U8 *write_data_aru8,U32 write_data_length_aru32,U8 *read_data_arpu8,U32 read_data_length_aru32)
{
	U8 read_status_u8 = 0;
	read_status_u8 = w25q_read_data_u8(write_data_aru8,write_data_length_aru32,read_data_arpu8,read_data_length_aru32);
	return read_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 w25q_app_erase_u8(w25q_erase_mode_te mode_are, U32 address_aru32, BOOL addr_4byte_en_arb,U16 erase_wait_time_in_ms_aru16)
{
	U8 ret_val_u8 = 0;
	U8 sts_reg_1_u8 = 0;

	ret_val_u8 += w25q_write_enable_u8();
	 ret_val_u8 += w25q_wait_for_write_end_u8(erase_wait_time_in_ms_aru16);   			 	/* Blocking approach 		*/
	//w25q_wait_for_wr_end_no_blocking_u8(&sts_reg_1_u8);									 	/* Non Blocking approach 	*/
	ret_val_u8 += w25q_erase_u8(mode_are, address_aru32, addr_4byte_en_arb);
	 ret_val_u8 += w25q_wait_for_write_end_u8(erase_wait_time_in_ms_aru16);   			/* Blocking approach 		*/
	/*w25q_wait_for_wr_end_no_blocking_u8(&sts_reg_1_u8);			*/							 	/* Non Blocking approach 	*/
	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 w25q_power_down_u8(void)
{
	U8 ret_val_u8 = 0;
	W25Q_CHIP_SELECT_ENABLE;
	ret_val_u8 = spi_app_master_tx_rx_u8(SPI_APP_FLASH, W25Q_POWER_DOWN, 1, NULL, 0);
	W25Q_CHIP_SELECT_DISABLE;
	return (ret_val_u8);
}
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
