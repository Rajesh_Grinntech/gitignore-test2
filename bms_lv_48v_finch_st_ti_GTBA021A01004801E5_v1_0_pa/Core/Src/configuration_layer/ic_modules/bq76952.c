/*
 * bq76952.c
 *
 *  Created on: Nov 27, 2020
 *      Author: glads
 */


#include "main.h"
#include "bq76952.h"
#include "i2c_app.h"
#include "afe_driver.h"
#include "math.h"

/*** Thermistor ****/
#define VIN  				VREF*1000
#define	VREF 				3.29
#define CONST_RESISTANCE 	10000
#define BETA 				3950

SFP Thermistor_f= 0.0;

bq76952_enabled_prot1_stat_tu bq76952_enabled_prot1_stat_u;
bq76952_enabled_prot2_stat_tu bq76952_enabled_prot2_stat_u;
bq76952_enabled_prot3_stat_tu bq76952_enabled_prot3_stat_u;

bq76952_chgfet_prot1_stat_tu bq76952_chgfet_prot1_stat_u;
bq76952_chgfet_prot2_stat_tu bq76952_chgfet_prot2_stat_u;
bq76952_chgfet_prot3_stat_tu bq76952_chgfet_prot3_stat_u;

bq76952_dsgfet_prot1_stat_tu bq76952_dsgfet_prot1_stat_u;
bq76952_dsgfet_prot2_stat_tu bq76952_dsgfet_prot2_stat_u;
bq76952_dsgfet_prot3_stat_tu bq76952_dsgfet_prot3_stat_u;

bq76952_ldo_config_tu		bq76952_ldo_config_u;
bq76952_reg0_config_tu  	bq76952_reg0_config_u;

bq76952_fet_options_tu 		bq76952_fet_options_u;
bq76952_fet_ctrl_tu 		bq76952_fet_ctrl_u;
bq76952_manuf_stat_tu		bq76952_manuf_stat_u;
bq76952_pwr_conf_tu 		bq76952_pwr_conf_u;

bq76952_fet_stat_tu			bq76952_fet_stat_u;
bq76952_batt_stat_tu 		bq76952_batt_stat_u;

bq76952_alarm_tu			bq76952_alm_stat_u;			/* Alert Pin Triggers*/
bq76952_alarm_tu			bq76952_alm_raw_stat_u;		/* No Alert Pin Triggers*/
bq76952_alarm_tu			bq76952_alm_en_u;			/* Turning it on - Makes Alert bit assert*/

bq76952_control_stat_tu	bq76952_control_stat_u;
bq76952_cbal_config_tu bq76952_cbal_config_u;

bq76952_pin_config_tu bq76952_alert_pin_cfg_u;
bq76952_pin_config_tu bq76952_temp_pin_cfg_u;


bq76952_all_prot_info_tst bq76952_all_prot_info_st;
bq76952_all_pf_info_tst bq76952_all_pf_info_st;

U16 bq76952_init_u16();
U16 bq76952_get_all_cell_vtg_u16();
U16 bq76952_get_all_cell_temp_u16();
U16 bq76952_get_current_u16(bq76952_cc_no_te cc_no_are, S16* rd_curr_arps16);
U16 bq76952_get_vtg_u16(bq76952_non_cvtg_types_te non_cvtg_types_are, U16* rd_vtg_arpu16);
U16 bq76952_get_all_values_u16(bq76952_all_values_tst* bq76952_all_values_arpst);
U16 bq76952_configure_u16(bq76952_config_tst* bq76952_config_arpst);

U8 bq76952_get_cell_vtg_u8(U8 cell_no_aru8, U16* cell_vtg_arpu16);
U8 bq76952_get_thermistor_temp_u8(bq76952_thermistor_te thermistor_are, SFP* temp_val_arpsfp );
U8 bq76952_get_internal_temp_u8(SFP* int_temp_val_arpsfp);

U16 bq76952_configure_ldo_u16(bq76952_ldo_configure_tst* bq76952_ldo_configure_arpst);
U16 bq76952_configure_fet_u16(bq76952_fet_configure_tst* bq76952_fet_configure_arpst);

U16 bq76952_set_cell_over_vtg_prot_u16(U16 ov_thres_mv_aru16, U16 delay_ms_aru16, U16 ov_recv_mv_aru16);
U16 bq76952_set_cell_under_vtg_prot_u16(U16 uv_thres_mv_aru16, U16 delay_ms_aru16, U16 uv_recv_mv_aru16);

U16 bq76952_set_cell_over_temp_chg_prot_u16(S8 ot_chg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_chg_recv_1c_ars8);
U16 bq76952_set_cell_over_temp_dsg_prot_u16(S8 ot_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_dsg_recv_1c_ars8);
U16 bq76952_set_cell_under_temp_chg_prot_u16(S8 ut_chg_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_chg_recv_1c_ars8);
U16 bq76952_set_cell_under_temp_dsg_prot_u16(S8 ut_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_dsg_recv_1c_ars8);

U16 bq76952_set_int_over_temp_prot_u16(S8 ot_int_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_int_recv_1c_ars8);
U16 bq76952_set_int_under_temp_prot_u16(S8 ut_int_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_int_recv_1c_ars8);
U16 bq76952_set_fet_over_temp_prot_u16(S8 ot_fet_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_fet_recv_1c_ars8);

U16 bq76952_set_chg_ocp_u16(U16 chg_ocp_ma_aru16, U16 delay_ms_aru16,S16 recv_thres_ma_ars16, S16 pk_tos_del_cv_ars16);
U16 bq76952_set_dchg_ocp_u16(U16 dchg_ocp_ma_aru16, U16 delay_ms_aru16);

U16 bq76952_set_dischg_scp_u16(bq76952_scd_thresh_te thresh_are, U16 delay_us_aru16, U8 recv_time_1s_aru8 );

U16 bq76952_get_protection_status_u16(bq76952_protect_status_te bq76952_protect_status_are, void* bq76952_protect_status_arpv);

U16 bq76952_get_ic_stat_u16(bq76952_ic_sts_te bq76952_ic_sts_are);

U16 bq76952_reset_u16();
U16 bq76952_dev_id_u16();

U16 bq76952_set_fet_u16(bq76952_fet_te fet_type_are, bq76952_fet_state_te fet_state_are);
U16 bq76952_set_fet_test_u16(bq76952_fet_te fet_type_are, bq76952_fet_state_te fet_state_are);

U16 bq76952_cb_config_u16();
U16 bq76952_slp_config_u16(bq76952_sleep_configure_tst* sleep_configure_arpst);
U16 bq76952_alarm_config_u16(bq76952_alarm_configure_tst* alarm_configure_arpst);

U16 bq76952_enter_sleep_u16(bq76952_sleep_mode_te sleep_mode_are);
U16 bq76952_exit_sleep_u16(bq76952_sleep_mode_te sleep_mode_are);

U16 bq76952_rd_cb_sts_u16(U16* cb_act_reg_arpu16);
U16 bq76952_wr_cb_sts_u16(U16 cb_act_reg_aru16);

U8 bq76952_wr_subcmd_u8(U16 sub_cmd_no_aru16,U8* wr_data_arpu8, U8 wr_size_aru8,U8 delay_aru8);
U8 bq76952_rd_subcmd_u8(U16 sub_cmd_no_u16, U8* rd_data_arpu8 ,U8 rd_addr_u8, U8 rd_size_u8,U8 delay_aru8);

U8 bq76952_rd_dir_cmd_u8(U8 dir_cmd_reg_aru8, U8* rd_data_arpu8, U8 rd_size_aru8);
U8 bq76952_wr_dir_cmd_u8(U8 dir_cmd_reg_aru8, U8* wr_data_arpu8, U8 wr_size_aru8);

U8 bq76952_write_data_memory_u8(U16 addr_aru16, U8* data_arpu8, U8 num_bytes_aru8);
U8 bq76952_read_data_memory_u8(U16 addr_aru16, U8* rd_data_arpu8, U8 num_bytes_aru8);


U8 bq76952_config_update_u8(bq76952_configmode_te bq76952_configmode_are);


U8 bq76952_crc_u8(U8 *ptr, U8 len);

S16 temeperature_get(U16 temp_raw_value,U16 beta_value_u16);

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_init_u16()
{
	U16 ret_val_u16 =0;

	//ret_val_u16 += bq76952_reset_u16();

	ret_val_u16 += bq76952_dev_id_u16();



	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_dev_id_u16()
{
	U16 ret_val_u16 = 0;

	U8 dev_id_au8[2] = {0,};
	U16 dev_id_u16;

	U8 temp_id_au8[6] = {0,};

	ret_val_u16 = bq76952_rd_subcmd_u8(SCMD_INDIR_DEV_ID,dev_id_au8,CMD_DIR_RESP_START,2,BQ76952_SUB_CMD_DLY_MS);

	dev_id_u16 = (dev_id_au8[1] << 8) | dev_id_au8[0];

	if(dev_id_u16 != 0x7695)
	{
		ret_val_u16++;
	}

	ret_val_u16 = bq76952_rd_subcmd_u8(SCMD_INDIR_FWVER_NO,temp_id_au8,CMD_DIR_RESP_START,6,BQ76952_SUB_CMD_DLY_MS);


	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_reset_u16()
{
	U16 ret_val_u16 = 0;

	/* Reset command bypasses write check- As all registers get zero*/
	U8 bq76952_tx_au8[3];

	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/

	/* RESET SUB CMD -0x0012*/
	bq76952_tx_au8[1] = 0x12;
	bq76952_tx_au8[2] = 0x00;

//	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	ret_val_u16 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 3, 0);

	HAL_Delay(1000); /* Reset delay 20ms*/

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_configure_u16(bq76952_config_tst* bq76952_config_arpst)
{
	U16 ret_val_u16 = 0;
S16 cell_voltage_shutdown_thres_s16 = 0;
S16 stack_voltag_shutdown_thres_s16 = 0;
U8 int_temp_shutdown_thres_u8 = 0;
U8 auto_shutdown_feature_u8 = 0;
U8 shutdown_cmd_delay_u8 = 254;
U8 shutdown_cmd_fetoff_delay_u8 = 100;
U8 temperature_snsor_enable_u8 = 0x07;
U8 da_config_u8 = 0x06;
U16 vcell_mode_u16 = 0x8FFF;

U16 cell_bal_min_volt_u16 = 1;
U16 temp_cb_min_volt_u16 = 0;

bq76952_temp_pin_cfg_u.bits.pin_fn_b2 = 0x3;
bq76952_temp_pin_cfg_u.bits.opt0_b1 = 0x1;
bq76952_temp_pin_cfg_u.bits.opt1_b1 = 0x0;
bq76952_temp_pin_cfg_u.bits.opt2_b1 = 0x0;
bq76952_temp_pin_cfg_u.bits.opt3_b1 = 0x0;
bq76952_temp_pin_cfg_u.bits.opt4_b1 = 0x0;
bq76952_temp_pin_cfg_u.bits.opt5_b1 = 0x0;


	bq76952_enabled_prot1_stat_u.byte = 0;
	bq76952_enabled_prot2_stat_u.byte = 0;
	bq76952_enabled_prot3_stat_u.byte = 0;

	bq76952_chgfet_prot1_stat_u.byte = 0;
	bq76952_chgfet_prot2_stat_u.byte = 0;
	bq76952_chgfet_prot3_stat_u.byte = 0;

	bq76952_dsgfet_prot1_stat_u.byte = 0;
	bq76952_dsgfet_prot2_stat_u.byte = 0;
	bq76952_dsgfet_prot3_stat_u.byte = 0;
	ret_val_u16 += bq76952_write_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte , 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte , 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9263, &bq76952_enabled_prot3_stat_u.byte , 1);

	ret_val_u16 += bq76952_write_data_memory_u8(0x9265, &bq76952_chgfet_prot1_stat_u.byte , 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte , 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9267, &bq76952_chgfet_prot3_stat_u.byte , 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9269, &bq76952_dsgfet_prot1_stat_u.byte, 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x926B, &bq76952_dsgfet_prot3_stat_u.byte, 1);

	ret_val_u16 += bq76952_read_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte , 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte , 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x9263, &bq76952_enabled_prot3_stat_u.byte , 1);

	ret_val_u16 += bq76952_read_data_memory_u8(0x9265, &bq76952_chgfet_prot1_stat_u.byte , 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte , 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x9267, &bq76952_chgfet_prot3_stat_u.byte , 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x9269, &bq76952_dsgfet_prot1_stat_u.byte, 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x926B, &bq76952_dsgfet_prot3_stat_u.byte, 1);


	ret_val_u16 += bq76952_write_data_memory_u8(0x923F,(U8*)&cell_voltage_shutdown_thres_s16,2);
	ret_val_u16 +=  bq76952_write_data_memory_u8(0x9241,(U8*)&stack_voltag_shutdown_thres_s16,2);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9254,&auto_shutdown_feature_u8,1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9253,&shutdown_cmd_delay_u8,1);

	U8 curr_range = 0x40;
	ret_val_u16 += bq76952_write_data_memory_u8(0x9218,&curr_range,2);

//		  shutdown_cmd_fetoff_delay_u8 = 0;
//		  ret_val_u16 += bq76952_read_data_memory_u8(0x9252,&shutdown_cmd_fetoff_delay_u8,1);
//		  shutdown_cmd_fetoff_delay_u8 = 100;
//		  ret_val_u16 += bq76952_write_data_memory_u8(0x9252,&shutdown_cmd_fetoff_delay_u8,1);
//		  shutdown_cmd_fetoff_delay_u8 = 0;
//		  ret_val_u16 += bq76952_read_data_memory_u8(0x9252,&shutdown_cmd_fetoff_delay_u8,1);


		  ret_val_u16 += bq76952_write_data_memory_u8(0x9303,&da_config_u8,1);
		  ret_val_u16 += bq76952_read_data_memory_u8(0x9303,&da_config_u8,1);


		  /* Enabling Temperature sensors*/
		  		  ret_val_u16 += bq76952_write_data_memory_u8(0x92FE,&bq76952_temp_pin_cfg_u.byte,1); /*TS2*/

		  		  ret_val_u16 += bq76952_write_data_memory_u8(0x9301,&bq76952_temp_pin_cfg_u.byte,1); /* DCHG Pin*/
		  		  ret_val_u16 += bq76952_write_data_memory_u8(0x9302,&bq76952_temp_pin_cfg_u.byte,1); /* DDSG Pin*/
		  		  ret_val_u16 += bq76952_write_data_memory_u8(0x92FA,&bq76952_temp_pin_cfg_u.byte,1); /*CFET_OFF */

		  		temperature_snsor_enable_u8 = 0;
		  		  ret_val_u16 += bq76952_write_data_memory_u8(0x92FF,&bq76952_temp_pin_cfg_u.byte,1); /* TS3 */

		  		bq76952_temp_pin_cfg_u.byte = 0;
		  		  ret_val_u16 += bq76952_read_data_memory_u8(0x92FF,&bq76952_temp_pin_cfg_u.byte,1); /* TS3 */


		  		  ret_val_u16 += bq76952_write_data_memory_u8(0x9304,&vcell_mode_u16,2);
		  		vcell_mode_u16 = 0;
		  		  ret_val_u16 += bq76952_read_data_memory_u8(0x9304,&vcell_mode_u16,2);

		  		//S16 pwr_shut_val_s16 = 0;
		  		U8 pwr_shut_delay_u8 = 0;
		  		  //ret_val_u16 += bq76952_read_data_memory_u8(0x9241,(U8*)&pwr_shut_val_s16,2);
		  		 ret_val_u16 += bq76952_read_data_memory_u8(0x9243,(U8*)&pwr_shut_delay_u8,1);
		  		//pwr_shut_val_s16 = 100;
		  		pwr_shut_delay_u8 = 50;
		  	//	  ret_val_u16 += bq76952_write_data_memory_u8(0x9241,(U8*)&pwr_shut_val_s16,2);
		  		 ret_val_u16 += bq76952_write_data_memory_u8(0x9243,(U8*)&pwr_shut_delay_u8,1);
		  		//pwr_shut_val_s16= 0;
		  		pwr_shut_delay_u8 = 0;
		  		//  ret_val_u16 += bq76952_read_data_memory_u8(0x9241,(U8*)&pwr_shut_val_s16,2);
		  		ret_val_u16 += bq76952_read_data_memory_u8(0x9243,(U8*)&pwr_shut_delay_u8,1);

/* Configure Permanent Fails - Cell Open Wire */
//		  U16 permanent_fail_u16 = 0;
//		  ret_val_u16 += bq76952_write_data_memory_u8(0x925F,&permanent_fail_u16,2);
//		  permanent_fail_u16 = 0xFFFF;
//		  ret_val_u16 += bq76952_read_data_memory_u8(0x925F,&permanent_fail_u16,2);  // Check if the read back correctly
		  U8 cell_open_wire_interval_u8 = 0;
		ret_val_u16 += bq76952_read_data_memory_u8(0x9314,(U8*)&cell_open_wire_interval_u8,1);
		if (cell_open_wire_interval_u8 != 5)
		{
			cell_open_wire_interval_u8 = 5;
			ret_val_u16 += bq76952_write_data_memory_u8(0x9314,(U8*)&cell_open_wire_interval_u8,1);
		}

	ret_val_u16 += bq76952_configure_ldo_u16(&(bq76952_config_arpst->ldo_configure_st));

	ret_val_u16 += bq76952_configure_fet_u16(&(bq76952_config_arpst->fet_configure_st));

	ret_val_u16 += bq76952_slp_config_u16(&(bq76952_config_arpst->sleep_configure_st));

	ret_val_u16 += bq76952_alarm_config_u16(&(bq76952_config_arpst->alarm_configure_st));

	ret_val_u16 += bq76952_cb_config_u16(&(bq76952_config_arpst->cb_config_st));

	/** Alert Pin config **/
#if 0
	U8 val = 1;
	ret_val_u16 += bq76952_write_data_memory_u8(0x2812,&val,1);
	U8 alrt_pin_cfg_reg_u8 = 0x10;//0X82;
	ret_val_u16 += bq76952_write_data_memory_u8(0x92FC,&alrt_pin_cfg_reg_u8,1);
#endif
//	ret_val_u16 += bq76952_wr_subcmd_u8(0x0084,(U8*)&cell_bal_min_volt_u16, 2 , BQ76952_SUB_CMD_DLY_MS);

	//while(1){
//	ret_val_u16 += bq76952_rd_subcmd_u8(0x0084,(U8*)&temp_cb_min_volt_u16,CMD_DIR_RESP_START, 2 , BQ76952_SUB_CMD_DLY_MS);
	//}

	ret_val_u16 += bq76952_get_ic_stat_u16(BATT_STS);
	ret_val_u16 += bq76952_get_ic_stat_u16(CTRL_STS);





	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_cb_config_u16(bq76952_cb_config_tst* cb_config_arpst)
{
	U16 ret_val_u16 = 0;
	S8 temp_data_s8 = 0;


	U8 temp_cb_val_u8 = 0;

	ret_val_u16 += bq76952_read_data_memory_u8(0x9335, (U8*)&bq76952_cbal_config_u.byte, 1);

	ret_val_u16 += bq76952_read_data_memory_u8(0x9336, (U8*)&temp_data_s8, 1); /* Min Cell Temp    */ /* 1C*/

	if((temp_data_s8 != cb_config_arpst->min_cell_temp_s8) && (ret_val_u16 == 0))
	{
		ret_val_u16 += bq76952_write_data_memory_u8(0x9336, (U8*)&cb_config_arpst->min_cell_temp_s8, 1);
	}

	ret_val_u16 += bq76952_write_data_memory_u8(0x933A, (U8*)&cb_config_arpst->max_cells_u8, 1);

	ret_val_u16 += bq76952_write_data_memory_u8(0x933B, (U8*)&cb_config_arpst->min_cell_vtg_u16, 2);

	ret_val_u16 += bq76952_write_data_memory_u8(0x933D, (U8*)&cb_config_arpst->start_del_val_u8, 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x933E, (U8*)&cb_config_arpst->start_del_val_u8, 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x933F, (U8*)&cb_config_arpst->min_cell_vtg_u16, 2);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9341, (U8*)&cb_config_arpst->start_del_val_u8, 1);
	ret_val_u16 += bq76952_write_data_memory_u8(0x9342, (U8*)&cb_config_arpst->start_del_val_u8, 1);

	ret_val_u16 += bq76952_write_data_memory_u8(0x9339, (U8*)&cb_config_arpst->duration_intvl_u8, 1);
	ret_val_u16 += bq76952_read_data_memory_u8(0x9339, (U8*)&temp_cb_val_u8, 1);

	ret_val_u16 += bq76952_read_data_memory_u8(0x9337, (U8*)&temp_data_s8, 1); /* Max Cell Temp    */ /* 1C*/

	if((temp_data_s8 != cb_config_arpst->max_cell_temp_s8) && (ret_val_u16 == 0))
	{
		ret_val_u16 += bq76952_write_data_memory_u8(0x9337, (U8*)&cb_config_arpst->max_cell_temp_s8, 1);
	}

	ret_val_u16 += bq76952_read_data_memory_u8(0x9338, (U8*)&temp_data_s8, 1); /* Max Internal Temp    */ /* 1C*/

	if((temp_data_s8 != cb_config_arpst->max_int_temp_s8) && (ret_val_u16 == 0))
	{
		ret_val_u16 += bq76952_write_data_memory_u8(0x9338, (U8*)&cb_config_arpst->max_int_temp_s8, 1);
	}

	if(cb_config_arpst->auto_en_u8 == COM_HDR_FALSE) /* Host- controlled mode*/
	{
		if(bq76952_cbal_config_u.byte != 0x04)
		{
			bq76952_cbal_config_u.byte = 0x04;
			ret_val_u16 += bq76952_write_data_memory_u8(0x9335, (U8*)&bq76952_cbal_config_u.byte, 1);
		}
	}
	else
	{
		bq76952_cbal_config_u.bits.cb_no_cmd_b1 =0;
		bq76952_cbal_config_u.bits.cb_rlx_b1 = 1;
		bq76952_cbal_config_u.bits.cb_chg_b1 = 1;
		ret_val_u16 += bq76952_write_data_memory_u8(0x9335, (U8*)&bq76952_cbal_config_u.byte, 1);

	}

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_slp_config_u16(bq76952_sleep_configure_tst* sleep_configure_arpst)
{
	U16 ret_val_u16 = 0;
	S16 temp_data_s16;
	U8 temp_data_u8;

	ret_val_u16 += bq76952_read_data_memory_u8(0x9234, (U8*)&bq76952_pwr_conf_u.byte_u16, 2);

	if((bq76952_pwr_conf_u.bits.ldo_dp_slp_b1 != (U8)sleep_configure_arpst->dpslp_ldo_en_b)||
			(bq76952_pwr_conf_u.bits.lfo_dp_slp_b1 != (U8)sleep_configure_arpst->dpslp_lfo_en_b))
	{
		bq76952_pwr_conf_u.bits.cb_loop_slow_b2 = 3;
		bq76952_pwr_conf_u.bits.ldo_dp_slp_b1 = sleep_configure_arpst->dpslp_ldo_en_b;
		bq76952_pwr_conf_u.bits.lfo_dp_slp_b1 = sleep_configure_arpst->dpslp_lfo_en_b;
		ret_val_u16 += bq76952_write_data_memory_u8(0x9234, (U8*)&bq76952_pwr_conf_u.byte_u16, 2);
	}

	if(bq76952_read_data_memory_u8(0x9248, (U8*)&temp_data_s16, 2) == 0) /* Sleep Current    */ /* 1mA*/
	{
		if(temp_data_s16 != sleep_configure_arpst->slp_strt_curr_1ma_s16)
		{
			ret_val_u16 += bq76952_write_data_memory_u8(0x9248, (U8*)&sleep_configure_arpst->slp_strt_curr_1ma_s16, 2);
		}
	}
	else
	{
		return 1;
	}

	if(bq76952_read_data_memory_u8(0x924A, &temp_data_u8, 1) == 0) /* Voltage Time     */ /* 1s*/
	{
		if(temp_data_u8 != sleep_configure_arpst->slp_meas_time_1s_u8)
		{
			ret_val_u16 += bq76952_write_data_memory_u8(0x924A, &sleep_configure_arpst->slp_meas_time_1s_u8, 1);
		}
	}
	else
	{
		return 1;
	}

	if(bq76952_read_data_memory_u8(0x924B, (U8*)&temp_data_s16, 2) == 0) /* Wake Comparator Current */ /*1mA*/
	{
		if(temp_data_s16 != sleep_configure_arpst->slp_wk_comp_curr_1ma_s16)
		{
			bq76952_write_data_memory_u8(0x924B, (U8*)&sleep_configure_arpst->slp_wk_comp_curr_1ma_s16, 2);
		}
	}
	else
	{
		return 1;
	}

	if(bq76952_read_data_memory_u8(0x924E, (U8*)&temp_data_s16, 2) == 0) /* Sleep Chg TOS Voltage Threshold */ /*1cV*/
	{
		if(temp_data_s16 != sleep_configure_arpst->schgr_thres_1cv_s16)
		{
			bq76952_write_data_memory_u8(0x924E, (U8*)&sleep_configure_arpst->schgr_thres_1cv_s16, 2);
		}
	}
	else
	{
		return 1;
	}

	if(bq76952_read_data_memory_u8(0x9250, (U8*)&temp_data_s16, 2) == 0) /* Sleep Chg (Pack- TOS) Delta */ /* 1cV */
	{
		if(temp_data_s16 != sleep_configure_arpst->schgr_pk_tos_del_1cv_s16)
		{
			bq76952_write_data_memory_u8(0x9250, (U8*)&sleep_configure_arpst->schgr_pk_tos_del_1cv_s16, 2);
		}
	}
	else
	{
		return 1;
	}

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_alarm_config_u16(bq76952_alarm_configure_tst* alarm_configure_arpst)
{
	U16 ret_val_u16 = 0;
	S16 temp_data_s16;
	U8 temp_data_u8;

#if 1
	ret_val_u16 += bq76952_rd_dir_cmd_u8(CMD_DIR_ALRM_EN, (U8*)&bq76952_alm_en_u, 2);

	if((bq76952_alm_en_u.bits.wake_up_b1 != (U8)alarm_configure_arpst->wakeup_alert_b)
			|| (bq76952_alm_en_u.bits.full_scan_b1 != (U8)alarm_configure_arpst->full_scan_alert_b))
	{
		bq76952_alm_en_u.bits.wake_up_b1        = alarm_configure_arpst->wakeup_alert_b;
		bq76952_alm_en_u.bits.full_scan_b1 		= alarm_configure_arpst->full_scan_alert_b;

		ret_val_u16 += bq76952_wr_dir_cmd_u8(CMD_DIR_ALRM_EN, (U8*)&bq76952_alm_en_u, 2);
	}
#endif
	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_enter_sleep_u16(bq76952_sleep_mode_te sleep_mode_are)
{
	U16 ret_val_u16 = 0;

	switch(sleep_mode_are)
	{
		case SLEEP_NORMAL:
		{
			/*Only in Low current as user prefers*/
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_ALLOW_SLP, 0, 0,BQ76952_SUB_CMD_DLY_MS);

		}
		break;
		case SLEEP_DEEP:
		{
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_ENTER_DEEP, 0, 0,20);
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_ENTER_DEEP, 0, 0,20);
			HAL_Delay(120);
		}
		break;
		case SLEEP_SHUT:
		{
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_ENTER_SHUT , 0, 0,BQ76952_SUB_CMD_DLY_MS);
		}
		break;
		default:
		{

		}
	}

	ret_val_u16 += bq76952_read_data_memory_u8(0x9236, &bq76952_ldo_config_u.byte, 1);

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_exit_sleep_u16(bq76952_sleep_mode_te sleep_mode_are)
{
	U16 ret_val_u16 = 0;

	switch(sleep_mode_are)
	{
		case SLEEP_NORMAL:
		{
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_EXIT_SLP, 0, 0,BQ76952_SUB_CMD_DLY_MS);
			HAL_Delay(1000); /* One second needed delay for all IC processes starting*/

		}
		break;
		case SLEEP_DEEP:
		{
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_EXIT_DEEP, 0, 0,BQ76952_SUB_CMD_DLY_MS);
			HAL_Delay(250); /* 250 needed*/
		}
		break;
		case SLEEP_SHUT:
		{

		}
		break;
		default:
		{

		}
	}
	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_rd_cb_sts_u16(U16* cb_act_reg_arpu16)
{
	U16 ret_val_u16 = 0;
	U8 temp_data_au8[2] = {0,0};

	//ret_val_u16 = bq76952_rd_subcmd_u8(SCMD_INDIR_CB_ACT_STS, temp_data_au8,CMD_DIR_RESP_START, 2, BQ76952_SUB_CMD_DLY_MS);


	U8 bq76952_tx_au8[3];
	U8 bq76952_rx_u8[1];

	//U8 temp_val_au8[36];
	//U8 bq76952_sub_cmd_wait_au8[2] = {0xFF,0xFF};
//	U16 bq76952_sub_cmd_wait_u16 = 0xFFFF;

	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/

	bq76952_tx_au8[1] = (SCMD_INDIR_CB_ACT_STS & 0xFF);
	bq76952_tx_au8[2] = ((SCMD_INDIR_CB_ACT_STS >> 8) & 0xFF);

	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	ret_val_u16 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 3, 0);

//	ret_val_e = HAL_I2C_Mem_Write(&hi2c1, BQ_I2C_ADDR, bq76952_tx_u8[0], 1, &bq76952_tx_u8[1], 2, BQ76952_TIMEOUT_MS);

	/* SUB-Cmd write completion check*/
#if 0
	U8 time_out_u8 = 100;
	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/
	while((bq76952_sub_cmd_wait_u16 != sub_cmd_no_aru16) && (time_out_u8 != 0))
	{
		HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_u8 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, bq76952_sub_cmd_wait_au8, 2, 0);
#if 0
		ret_val_e += HAL_I2C_Master_Transmit(&hi2c1, BQ_I2C_ADDR, bq76952_tx_au8, 1, BQ76952_TIMEOUT_MS);
	//	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_e += HAL_I2C_Master_Receive(&hi2c1, BQ_I2C_ADDR, bq76952_sub_cmd_wait_au8, 2, BQ76952_TIMEOUT_MS);
#endif

		bq76952_sub_cmd_wait_u16 = ((bq76952_sub_cmd_wait_au8[1] << 8) | bq76952_sub_cmd_wait_au8[0]);
		time_out_u8--;
	}

	if(bq76952_sub_cmd_wait_u16 != sub_cmd_no_aru16)
	{
		return 1;
	}
#endif
	bq76952_rx_u8[0] = 0x40; /* Return - read reg address*/

	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	ret_val_u16 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_rx_u8, 1, 0, temp_data_au8, 2, 0);
	//HAL_Delay(BQ76952_SUB_CMD_DLY_MS);

#if 0
	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	bq76952_tx_au8[0] = 0x3E;
	ret_val_u16 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, temp_val_au8, 36, 0);
#endif



	*cb_act_reg_arpu16 = ((temp_data_au8[1] << 8) | temp_data_au8[0]);
	return ret_val_u16;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_wr_cb_sts_u16(U16 cb_act_reg_aru16)
{
	U16 ret_val_u16 = 0;
	U8 temp_data_au8[2];

	temp_data_au8[0] = (cb_act_reg_aru16 & 0xFF);
	temp_data_au8[1] = (cb_act_reg_aru16 >> 8) & 0xFF;

//	ret_val_u16 = bq76952_wr_subcmd_u8(SCMD_INDIR_CB_ACT_STS, temp_data_au8, 2, BQ76952_SUB_CMD_DLY_MS);

	U8 bq76952_tx_au8[5];
	U8 bq76952_tx2_au8[3];
	U8 bq76952_sub_cmd_wait_au8[2] = {0xFF,0xFF};
	U8 temp_val_au8[6];
	U16 bq76952_sub_cmd_wait_u16 = 0xFFFF;
	U8 loop_var_u8 = 0;
#if 0
	U8 temp_crc_au8[3];

	temp_crc_au8[0] = BQ_I2C_ADDR;
	temp_crc_au8[1] = 0x3E;
	temp_crc_au8[2] =
#endif

	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/

	bq76952_tx_au8[1] = (SCMD_INDIR_CB_ACT_STS & 0xFF);
	bq76952_tx_au8[2] = ((SCMD_INDIR_CB_ACT_STS >> 8) & 0xFF);

	for(loop_var_u8 = 0; loop_var_u8 < 2; loop_var_u8++)
	{
		bq76952_tx_au8[3 + loop_var_u8] = temp_data_au8[loop_var_u8];
	}

	HAL_Delay(BQ76952_SUB_CMD_DLY_MS/2);

	ret_val_u16 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 5, 0);
	HAL_Delay(BQ76952_SUB_CMD_DLY_MS/2);

#if 1
	bq76952_tx2_au8[0] = 0x60;
	bq76952_tx2_au8[1] = bq76952_crc_u8(&bq76952_tx_au8[1],4);
	bq76952_tx2_au8[2] = 0x6;

	ret_val_u16 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx2_au8, 3, 0);
	HAL_Delay(BQ76952_SUB_CMD_DLY_MS/2);
#endif

#if 0
	bq76952_tx_au8[0] = 0x3E;
	ret_val_u16 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, temp_val_au8, 6, 0);
	HAL_Delay(BQ76952_SUB_CMD_DLY_MS/2);
#endif

#if 0
	/* SUB-Cmd write completion check*/
	U8 time_out_u8 = 100;
	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/
	while((bq76952_sub_cmd_wait_u16 != SCMD_INDIR_CB_ACT_STS) && (time_out_u8 != 0))
	{
		HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_u16 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, bq76952_sub_cmd_wait_au8, 2, 0);
#if 0
		ret_val_e += HAL_I2C_Master_Transmit(&hi2c1, BQ_I2C_ADDR, bq76952_tx_au8, 1, BQ76952_TIMEOUT_MS);
		HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_e += HAL_I2C_Master_Receive(&hi2c1, BQ_I2C_ADDR, bq76952_sub_cmd_wait_au8, 2, BQ76952_TIMEOUT_MS);
#endif
		bq76952_sub_cmd_wait_u16 = ((bq76952_sub_cmd_wait_au8[1] << 8) | bq76952_sub_cmd_wait_au8[0]);
		time_out_u8--;
	}
	//HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
//	ret_val_e = HAL_I2C_Mem_Write(&hi2c1, BQ_I2C_ADDR, bq76952_tx_u8[0], 1, &bq76952_tx_u8[1], 2, BQ76952_TIMEOUT_MS);
//	HAL_Delay(10);


	if(bq76952_sub_cmd_wait_u16 != SCMD_INDIR_CB_ACT_STS)
	{
		return 1;
	}
#endif



	return ret_val_u16;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_ic_stat_u16(bq76952_ic_sts_te bq76952_ic_sts_are)
{
	U16 ret_val_u16 = 0;

	switch(bq76952_ic_sts_are)
	{
		case BATT_STS:
		{
			ret_val_u16 = bq76952_rd_dir_cmd_u8(CMD_DIR_BATT_STAT,(U8*)&bq76952_batt_stat_u, 2);
		}
		break;
		case CTRL_STS:
		{
			ret_val_u16 = bq76952_rd_dir_cmd_u8(CMD_DIR_CTRL_STAT,(U8*)&bq76952_control_stat_u, 1);
		}
		break;
		case FET_STS:
		{
			ret_val_u16 += bq76952_rd_dir_cmd_u8(CMD_DIR_FET_STAT,&bq76952_fet_stat_u.byte,1);
		}
		break;
		case ALRM_STS:
		{
			ret_val_u16 += bq76952_rd_dir_cmd_u8(CMD_DIR_ALRM_RAW_STAT,(U8*)&bq76952_alm_stat_u,2);
			if(ret_val_u16 == 0)
			{
				if(bq76952_alm_stat_u.byte_u16 != 0)
				{
					ret_val_u16 = 0;
				}

				if(bq76952_alm_stat_u.bits.safe_stat_bc_b1 != 0)
				{
					ret_val_u16 = 0;
				}

				if(bq76952_alm_stat_u.bits.safe_stat_a_b1 != 0)
				{
					ret_val_u16 = 0;
				}

				if(bq76952_alm_stat_u.bits.pf_flt_b1 != 0)
				{
					ret_val_u16 = 0;
				}


				//ret_val_u16 += bq76952_wr_dir_cmd_u8(CMD_DIR_ALRM_STAT,(U8*)&bq76952_alm_stat_u,2);
			}
			else
			{
				ret_val_u16 = 1;
			}
		}
		break;
		default:
		{

		}
		break;
	}


	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_protection_alert_u16(bq76952_protect_alert_te bq76952_protect_alert_are, void* bq76952_protect_alert_arpv)
{
	U16 ret_val_u16 = 0;
	U8 rx_data_au8[1];
	U8 dir_cmd_addr_u8 = 0;

	switch(bq76952_protect_alert_are)
	{
	case PROT_VTG_SA1:
		dir_cmd_addr_u8 = CMD_DIR_SA_PRT_VTG;
		break;
	case PROT_TEMP_SA2:
		dir_cmd_addr_u8 = CMD_DIR_SA_PRT_TEMP;
		break;
	case PROT_LAT_SA3:
		dir_cmd_addr_u8 = CMD_DIR_SA_PRT_LAT;
		break;
	default:

		break;
	}

	ret_val_u16 = bq76952_rd_dir_cmd_u8(dir_cmd_addr_u8,rx_data_au8, 1);

	*((U8*)bq76952_protect_alert_arpv) = rx_data_au8[0];

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_protection_status_u16(bq76952_protect_status_te bq76952_protect_status_are, void* bq76952_protect_status_arpv)
{
	U16 ret_val_u16 = 0;
	U8 rx_data_au8[1];
	U8 dir_cmd_addr_u8 = 0;

	switch(bq76952_protect_status_are)
	{
	case PROT_VTG_FS1:
		dir_cmd_addr_u8 = CMD_DIR_FS_PRT_VTG;
		break;
	case PROT_TEMP_FS2:
		dir_cmd_addr_u8 = CMD_DIR_FS_PRT_TEMP;
		break;
	case PROT_LAT_FS3:
		dir_cmd_addr_u8 = CMD_DIR_FS_PRT_LAT;
		break;
	default:

		break;
	}

	ret_val_u16 = bq76952_rd_dir_cmd_u8(dir_cmd_addr_u8,rx_data_au8, 1);

	*((U8*)bq76952_protect_status_arpv) = rx_data_au8[0];

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_permnt_fail_alert_u16(bq76952_permanentfail_alert_te bq76952_pf_alert_are, void* bq76952_pf_alert_arpv)
{
	U16 ret_val_u16 = 0;
	U8 rx_data_au8[1];
	U8 dir_cmd_addr_u8 = 0;

	switch(bq76952_pf_alert_are)
	{
	case PF_ALERT_A:
		dir_cmd_addr_u8 = CMD_DIR_FA_PFA;
		break;
	case PF_ALERT_B:
		dir_cmd_addr_u8 = CMD_DIR_FA_PFB;
		break;
	case PF_ALERT_C:
		dir_cmd_addr_u8 = CMD_DIR_FA_PFC;
		break;
	case PF_ALERT_D:
		dir_cmd_addr_u8 = CMD_DIR_FA_PFD;
		break;
	default:
		break;
	}

	ret_val_u16 = bq76952_rd_dir_cmd_u8(dir_cmd_addr_u8,rx_data_au8, 1);

	*((U8*)bq76952_pf_alert_arpv) = rx_data_au8[0];

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_permnt_fail_status_u16(bq76952_permanentfail_status_te bq76952_pf_status_are, void* bq76952_pf_status_arpv)
{
	U16 ret_val_u16 = 0;
	U8 rx_data_au8[1];
	U8 dir_cmd_addr_u8 = 0;

	switch(bq76952_pf_status_are)
	{
	case PF_STATUS_A:
		dir_cmd_addr_u8 = CMD_DIR_FS_PFA;
		break;
	case PF_STATUS_B:
		dir_cmd_addr_u8 = CMD_DIR_FS_PFB;
		break;
	case PF_STATUS_C:
		dir_cmd_addr_u8 = CMD_DIR_FS_PFC;
		break;
	case PF_STATUS_D:
		dir_cmd_addr_u8 = CMD_DIR_FS_PFD;
		break;
	default:

		break;
	}

	ret_val_u16 = bq76952_rd_dir_cmd_u8(dir_cmd_addr_u8,rx_data_au8, 1);

	*((U8*)bq76952_pf_status_arpv) = rx_data_au8[0];

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_all_prot_info_u16()
{
	U16 ret_val_u16 = 0;

	ret_val_u16 = bq76952_rd_dir_cmd_u8(CMD_DIR_SA_PRT_VTG,(U8*)&bq76952_all_prot_info_st, 6);

//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte , 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte , 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9263, &bq76952_enabled_prot3_stat_u.byte , 1);
//
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9265, &bq76952_chgfet_prot1_stat_u.byte , 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte , 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9267, &bq76952_chgfet_prot3_stat_u.byte , 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x9269, &bq76952_dsgfet_prot1_stat_u.byte, 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
//	  ret_val_u16 += bq76952_read_data_memory_u8(0x926B, &bq76952_dsgfet_prot3_stat_u.byte, 1);

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_all_pf_info_u16()
{
	U16 ret_val_u16 = 0;

	ret_val_u16 = bq76952_rd_dir_cmd_u8(CMD_DIR_FA_PFA,(U8*)&bq76952_all_pf_info_st, 8);

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/


U8 bq76952_wr_subcmd_u8(U16 sub_cmd_no_aru16,U8* wr_data_arpu8, U8 wr_size_aru8,U8 delay_aru8)
{
	U8 ret_val_u8 = 0;

	U8 bq76952_tx_au8[3 + wr_size_aru8];
	U8 bq76952_tx2_au8[3];
	//U8 bq76952_sub_cmd_wait_au8[2] = {0xFF,0xFF};
	//U16 bq76952_sub_cmd_wait_u16 = 0xFFFF;
	U8 loop_var_u8 = 0;

	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/

	bq76952_tx_au8[1] = (sub_cmd_no_aru16 & 0xFF);
	bq76952_tx_au8[2] = ((sub_cmd_no_aru16 >> 8) & 0xFF);

	for(loop_var_u8 = 0; loop_var_u8 < wr_size_aru8; loop_var_u8++)
	{
		bq76952_tx_au8[3 + loop_var_u8] = wr_data_arpu8[loop_var_u8];
	}

	HAL_Delay(delay_aru8/2);

	ret_val_u8 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, (3 + wr_size_aru8), 0);
	HAL_Delay(delay_aru8/2);

#if 1
	if(wr_size_aru8 != 0)
	{
	bq76952_tx2_au8[0] = 0x60;
	bq76952_tx2_au8[1] = bq76952_crc_u8(&bq76952_tx_au8[1],2+wr_size_aru8);
	bq76952_tx2_au8[2] = 0x6;

	ret_val_u8 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx2_au8, 3, 0);
	HAL_Delay(BQ76952_SUB_CMD_DLY_MS/2);
	}
#endif

#if 0
	/* SUB-Cmd write completion check*/
	U8 time_out_u8 = 100;
	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/
	while((bq76952_sub_cmd_wait_u16 != sub_cmd_no_aru16) && (time_out_u8 != 0))
	{
		HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_u8 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, bq76952_sub_cmd_wait_au8, 2, 0);
#if 0
		ret_val_e += HAL_I2C_Master_Transmit(&hi2c1, BQ_I2C_ADDR, bq76952_tx_au8, 1, BQ76952_TIMEOUT_MS);
		HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_e += HAL_I2C_Master_Receive(&hi2c1, BQ_I2C_ADDR, bq76952_sub_cmd_wait_au8, 2, BQ76952_TIMEOUT_MS);
#endif
		bq76952_sub_cmd_wait_u16 = ((bq76952_sub_cmd_wait_au8[1] << 8) | bq76952_sub_cmd_wait_au8[0]);
		time_out_u8--;
	}
	//HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
//	ret_val_e = HAL_I2C_Mem_Write(&hi2c1, BQ_I2C_ADDR, bq76952_tx_u8[0], 1, &bq76952_tx_u8[1], 2, BQ76952_TIMEOUT_MS);
//	HAL_Delay(10);


	if(bq76952_sub_cmd_wait_u16 != sub_cmd_no_aru16)
	{
		return 1;
	}
#endif
	return ret_val_u8;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 bq76952_rd_subcmd_u8(U16 sub_cmd_no_aru16,U8* rd_data_arpu8 , U8 rd_addr_u8, U8 rd_size_aru8,U8 delay_aru8)
{
	U8 ret_val_u8 = 0;

	U8 bq76952_tx_au8[3];
	U8 bq76952_rx_u8[1];
	//U8 bq76952_sub_cmd_wait_au8[2] = {0xFF,0xFF};
//	U16 bq76952_sub_cmd_wait_u16 = 0xFFFF;

	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/

	bq76952_tx_au8[1] = (sub_cmd_no_aru16 & 0xFF);
	bq76952_tx_au8[2] = ((sub_cmd_no_aru16 >> 8) & 0xFF);

	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	ret_val_u8 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 3, 0);

//	ret_val_e = HAL_I2C_Mem_Write(&hi2c1, BQ_I2C_ADDR, bq76952_tx_u8[0], 1, &bq76952_tx_u8[1], 2, BQ76952_TIMEOUT_MS);

	/* SUB-Cmd write completion check*/
#if 0
	U8 time_out_u8 = 100;
	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/
	while((bq76952_sub_cmd_wait_u16 != sub_cmd_no_aru16) && (time_out_u8 != 0))
	{
		HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_u8 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, bq76952_sub_cmd_wait_au8, 2, 0);
#if 0
		ret_val_e += HAL_I2C_Master_Transmit(&hi2c1, BQ_I2C_ADDR, bq76952_tx_au8, 1, BQ76952_TIMEOUT_MS);
	//	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
		ret_val_e += HAL_I2C_Master_Receive(&hi2c1, BQ_I2C_ADDR, bq76952_sub_cmd_wait_au8, 2, BQ76952_TIMEOUT_MS);
#endif

		bq76952_sub_cmd_wait_u16 = ((bq76952_sub_cmd_wait_au8[1] << 8) | bq76952_sub_cmd_wait_au8[0]);
		time_out_u8--;
	}

	if(bq76952_sub_cmd_wait_u16 != sub_cmd_no_aru16)
	{
		return 1;
	}
#endif
	bq76952_rx_u8[0] = rd_addr_u8; /* Return - read reg address*/

	HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	ret_val_u8 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_rx_u8, 1, 0, rd_data_arpu8, rd_size_aru8, 0);
	//HAL_Delay(BQ76952_SUB_CMD_DLY_MS);

	return ret_val_u8;

}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 bq76952_wr_dir_cmd_u8(U8 dir_cmd_reg_aru8, U8* wr_data_arpu8, U8 wr_size_aru8)
{
	U8 ret_val_u8 = 0;

	//U8 bq76952_tx_u8[1];

	U8 bq76952_tx_au8[1 + wr_size_aru8];
	U8 loop_var_u8 = 0;

	bq76952_tx_au8[0] = dir_cmd_reg_aru8; /* Direct- Cmd Regiser*/

	for(loop_var_u8 = 1; loop_var_u8 <= wr_size_aru8; loop_var_u8++)
	{
		bq76952_tx_au8[loop_var_u8] = wr_data_arpu8[loop_var_u8-1];
	}

//	bq76952_tx_u8[1] = (sub_cmd_no_u16 & 0xFF);
//	bq76952_tx_u8[2] = ((sub_cmd_no_u16 >> 8) & 0xFF);

	ret_val_u8 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, (1 + wr_size_aru8), 0);
	//ret_val_e = HAL_I2C_Mem_Write(&hi2c1, BQ_I2C_ADDR, bq76952_tx_u8[0], 1, wr_data_arpu8, wr_size_aru8, BQ76952_TIMEOUT_MS);

	return ret_val_u8;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 bq76952_rd_dir_cmd_u8(U8 dir_cmd_reg_aru8, U8* rd_data_arpu8, U8 rd_size_aru8)
{
	U8 ret_val_u8 = 0;

	U8 bq76952_tx_au8[1];

	bq76952_tx_au8[0] = dir_cmd_reg_aru8; /* Direct- Cmd Regiser*/

	ret_val_u8 = i2c_app_tx_rx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 1, 0, rd_data_arpu8, rd_size_aru8, 0);

#if 0
	ret_val_e += HAL_I2C_Master_Transmit(&hi2c1, BQ_I2C_ADDR, bq76952_tx_au8, 1, BQ76952_TIMEOUT_MS);
	ret_val_e += HAL_I2C_Master_Receive(&hi2c1, BQ_I2C_ADDR, rd_data_arpu8, rd_size_aru8, BQ76952_TIMEOUT_MS);
#endif

	return ret_val_u8;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Measure CC2 current alone with this API for now
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_current_u16(bq76952_cc_no_te cc_no_are, S16* rd_curr_arps16)
{
	U16 ret_val_u16 = 0;
	U8 rd_curr_au8[2];

	switch(cc_no_are)
	{
		case CC1:
			/*Not needed for now*/
			break;
		case CC2:
			ret_val_u16 = bq76952_rd_dir_cmd_u8(CMD_DIR_CC2_CUR, rd_curr_au8, 2);
			break;
		case CC3:
			/* Not needed for now*/
			break;
		default:
			break;
	}


	*rd_curr_arps16 =  (S16)((rd_curr_au8[1] << 8) | rd_curr_au8[0]);

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_vtg_u16(bq76952_non_cvtg_types_te non_cvtg_types_are, U16* rd_vtg_arpu16)
{
	U16 ret_val_u16 = 0;
	U8 rd_vtg_au8[2];
	U8 vtg_dir_cmd_reg_u8 = 0;

	switch(non_cvtg_types_are)
	{
		case TOS_VTG:
			vtg_dir_cmd_reg_u8 = 0x34;
			break;
		case PK_VTG:
			vtg_dir_cmd_reg_u8 = 0x36;
			break;
		case LD_VTG:
			vtg_dir_cmd_reg_u8 = 0x38;
			break;
		default:
			break;
	}


	ret_val_u16 = bq76952_rd_dir_cmd_u8(vtg_dir_cmd_reg_u8, rd_vtg_au8, 2);

	*rd_vtg_arpu16 = ((rd_vtg_au8[1] << 8) | rd_vtg_au8[0]);


	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 bq76952_get_cell_vtg_u8(U8 cell_no_aru8, U16* cell_vtg_arpu16)
{
	U8 ret_val_u8 = 0;

	U8 rd_vtg_au8[2];
	ret_val_u8 = bq76952_rd_dir_cmd_u8(CELL_NO_TO_ADDR(cell_no_aru8), rd_vtg_au8, 2);

	*cell_vtg_arpu16 = ((rd_vtg_au8[1] << 8) | rd_vtg_au8[0]);

	return ret_val_u8;

}

#if 0

SFP ADCConvertedVoltage_f = 0.0;
	SFP ThermistorResistanceValue_f = 0.00;
	SFP Thermistor_f = 0.00;

	ADCConvertedVoltage_f = (ADC_Value*VIN)/0xFFF;
	ADCConvertedVoltage_f = ADCConvertedVoltage_f/1000;
	ThermistorResistanceValue_f = (((VREF*CONST_RESISTANCE)/(VREF-ADCConvertedVoltage_f))-CONST_RESISTANCE);
	Thermistor_f = (1/((1/298.15)+((log(ThermistorResistanceValue_f/10000))/BETA)))-273.15;
	//Printf_RS485("the temperature value is %f",Thermistor_f);
	return Thermistor_f;


#endif

S16 temeperature_get(U16 temp_raw_value,U16 beta_value_u16)
{
	S16 thermister_value_s16 = 0;
	SFP value_1_sfp = 1/298.15;
	SFP value_2_sfp = (1/(temp_raw_value/10.0));
	DFP temp_r2_sfp = exp(3435 *(value_1_sfp - value_2_sfp));
	//temp_r2_sfp = exp(temp_r2_sfp);
	temp_r2_sfp = 10000/temp_r2_sfp;
    Thermistor_f = (1/((1/298.15)+((log(temp_r2_sfp/10000))/beta_value_u16))) -273.15;
    thermister_value_s16 = (S16)(Thermistor_f *100);
    return thermister_value_s16;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_all_values_u16(bq76952_all_values_tst* bq76952_all_values_arpst)
{
	U16 ret_val_u16 = 0;

	U16 temp_raw_au16[20];

	ret_val_u16 += bq76952_rd_dir_cmd_u8(0x14, (U8*)bq76952_all_values_arpst, 38);

	ret_val_u16 += bq76952_rd_dir_cmd_u8(0x68, (U8*)temp_raw_au16 , 20);

	bq76952_all_values_arpst->int_therm_1cc_s16 = ((temp_raw_au16[0] * 10) - 27315) ;
	/** AFE Die Temperature **/
#if CFET_THERM
	bq76952_all_values_arpst->cfet_off_temp_1cc_s16 = ((S16)( temp_raw_au16[1]* 10) - 27315);//temp_raw_au16[4]
	/** Mosfet Temperature **/
#endif

#if DFET_THERM
	bq76952_all_values_arpst->dfet_off_temp_1cc_s16 = ((S16)(temp_raw_au16[2] * 10) - 27315) ;
#endif
#if ALERT_THERM
	bq76952_all_values_arpst->alert_temp_1cc_s16 = ((S16)(temp_raw_au16[3] * 10) - 27315) ;
#endif

	/** thermistor **/
	bq76952_all_values_arpst->ts1_val_1cc_s16 = ((S16)(temp_raw_au16[4] * 10) - 27315) ;//((S16)(temp_raw_au16[1] * 10) - 27315) ;
	bq76952_all_values_arpst->ts2_val_1cc_s16 = ((S16)(temp_raw_au16[5] * 10) - 27315);
	bq76952_all_values_arpst->ts3_val_1cc_s16 = ((S16)(temp_raw_au16[6] * 10) - 27315) ;//

#if HDQ_THERM
	bq76952_all_values_arpst->hdq_temp_1cc_s16 = ((S16)(temp_raw_au16[7] * 10) - 27315) ;
#endif

#if DCHG_THERM
	//bq76952_all_values_arpst->dchg_temp_1cc_s16 = ((S16)(temp_raw_au16[8] * 10) - 27315) ;
	bq76952_all_values_arpst->dchg_temp_1cc_s16 = temeperature_get(temp_raw_au16[8],4250);
	/** pack temperature **/
#endif

#if DDSG_THERM
	//bq76952_all_values_arpst->ddsg_temp_1cc_s16 = ((S16)(temp_raw_au16[9] * 10) - 27315) ;
	bq76952_all_values_arpst->ddsg_temp_1cc_s16  = temeperature_get(temp_raw_au16[9],4250);
	/** average temperature **/
#endif
	if((bq76952_all_values_arpst->ts1_val_1cc_s16 == -27315)
			||(bq76952_all_values_arpst->ts2_val_1cc_s16 == -27315)
			||(bq76952_all_values_arpst->ts3_val_1cc_s16 == -27315)
			||(bq76952_all_values_arpst->cfet_off_temp_1cc_s16 == -27315)
			||(bq76952_all_values_arpst->dchg_temp_1cc_s16 == -27315)
			||(bq76952_all_values_arpst->ddsg_temp_1cc_s16 == -27315))
	{
		bq76952_configure_u16(&bq76952_config_st);
	}
	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_all_cell_vtg_u16(U16* cell_vtg_array_arpu16)
{
	U16 ret_val_u16 = 0;
	U8 loop_var_u8 = 0;
	for(loop_var_u8 = 1; loop_var_u8 <= 16 ; loop_var_u8++)
	{
		ret_val_u16 += bq76952_get_cell_vtg_u8(loop_var_u8, &cell_vtg_array_arpu16[loop_var_u8-1] );
	}

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_get_all_cell_temp_u16(SFP* cell_temp_array_arpsfp)
{

	U16 ret_val_u16 = 0;

	ret_val_u16 += bq76952_get_thermistor_temp_u8(TS1, &cell_temp_array_arpsfp[0]);
	ret_val_u16 += bq76952_get_thermistor_temp_u8(TS2, &cell_temp_array_arpsfp[1]);

	ret_val_u16 += bq76952_get_thermistor_temp_u8(TS3, &cell_temp_array_arpsfp[2]);
	ret_val_u16 += bq76952_get_thermistor_temp_u8(HDQ, &cell_temp_array_arpsfp[3]);

	ret_val_u16 += bq76952_get_thermistor_temp_u8(DCHG, &cell_temp_array_arpsfp[4]);
	ret_val_u16 += bq76952_get_thermistor_temp_u8(DDSG, &cell_temp_array_arpsfp[5]);

	ret_val_u16 += bq76952_get_thermistor_temp_u8(CFET_OFF, &cell_temp_array_arpsfp[6]);
	ret_val_u16 += bq76952_get_thermistor_temp_u8(DFET_OFF, &cell_temp_array_arpsfp[7]);

	ret_val_u16 += bq76952_get_thermistor_temp_u8(ALERT, &cell_temp_array_arpsfp[8]);
	ret_val_u16 += bq76952_get_internal_temp_u8(&cell_temp_array_arpsfp[9]);

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Configure LDO - Reg1 and Reg2
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_configure_ldo_u16(bq76952_ldo_configure_tst* bq76952_ldo_configure_arpst)
{
	U16 ret_val_u16 = 0;
	/* Threshold in mv -> 1012mv to 5566mv, Threshold in register -> 20 to 110; where 1 -> 50.6mv*/

	bq76952_ldo_config_tu ldo_config_temp_u;
	bq76952_reg0_config_tu reg0_config_temp_u;

	ldo_config_temp_u.bits.reg1_en_b1 = bq76952_ldo_configure_arpst->reg1_state_e;
	ldo_config_temp_u.bits.reg1_vtg_b3 = bq76952_ldo_configure_arpst->reg1_vtg_e;
	ldo_config_temp_u.bits.reg2_en_b1 = bq76952_ldo_configure_arpst->reg2_state_e;
	ldo_config_temp_u.bits.reg2_vtg_b3 = bq76952_ldo_configure_arpst->reg2_vtg_e;

	ret_val_u16 += bq76952_read_data_memory_u8(0x9236, &bq76952_ldo_config_u.byte, 1);

	if(ret_val_u16 != 0)
	{
		return ret_val_u16;
	}

	if(ldo_config_temp_u.byte != bq76952_ldo_config_u.byte)
	{
		ret_val_u16 += bq76952_write_data_memory_u8(0x9236, &ldo_config_temp_u.byte, 1);
		ret_val_u16 += bq76952_read_data_memory_u8(0x9236, &bq76952_ldo_config_u.byte, 1);

		if(ldo_config_temp_u.byte != bq76952_ldo_config_u.byte)
		{
			return 1;
		}
	}

	reg0_config_temp_u.byte = 0;

	ret_val_u16 += bq76952_read_data_memory_u8(0x9237, &bq76952_reg0_config_u.byte, 1);
	if(ret_val_u16 != 0)
	{
		return ret_val_u16;
	}

	if(bq76952_reg0_config_u.bits.reg0_en_b1 != bq76952_ldo_configure_arpst->regin_src_e)
	{
		reg0_config_temp_u.bits.reg0_en_b1 = bq76952_ldo_configure_arpst->regin_src_e;

		ret_val_u16 += bq76952_write_data_memory_u8(0x9237, &reg0_config_temp_u.byte, 1);
		ret_val_u16 += bq76952_read_data_memory_u8(0x9237, &bq76952_reg0_config_u.byte, 1);

		if(ldo_config_temp_u.byte != bq76952_ldo_config_u.byte)
		{
			return 1;
		}
	}

  return ret_val_u16;
}

U16 bq76952_configure_fet_u16(bq76952_fet_configure_tst* bq76952_fet_configure_arpst)
{
	U16 ret_val_u16 = 0;

	ret_val_u16 += bq76952_write_data_memory_u8(0x930A, (U8*)&bq76952_fet_configure_arpst->pchg_start_vtg_1mv_u16, 2);
	ret_val_u16 += bq76952_write_data_memory_u8(0x930C, (U8*)&bq76952_fet_configure_arpst->pchg_stop_vtg_1mv_u16, 2);

	ret_val_u16 += bq76952_write_data_memory_u8(0x930E, (U8*)&bq76952_fet_configure_arpst->pdsg_delta_1mv_u16, 2);
	ret_val_u16 += bq76952_write_data_memory_u8(0x930F, (U8*)&bq76952_fet_configure_arpst->pdsg_timeout_1ms_u16, 2);

	ret_val_u16 += bq76952_write_data_memory_u8(0x9273, (U8*)&bq76952_fet_configure_arpst->body_diode_thres_1ma_u16, 2);

	ret_val_u16 += bq76952_read_data_memory_u8(0x9308, (U8*)&bq76952_fet_options_u.byte, 1);

	if((bq76952_fet_options_u.bits.fet_init_off_b != (U8)bq76952_fet_configure_arpst->fet_init_state_b)
		|| (bq76952_fet_options_u.bits.pdsg_en_b != (U8)bq76952_fet_configure_arpst->fet_pdsg_en_b)
		|| (bq76952_fet_options_u.bits.series_fet_b != (U8)bq76952_fet_configure_arpst->fet_series_en_b))
	{
		bq76952_fet_options_u.bits.fet_init_off_b   = bq76952_fet_configure_arpst->fet_init_state_b;
		bq76952_fet_options_u.bits.pdsg_en_b 		= bq76952_fet_configure_arpst->fet_pdsg_en_b;
		bq76952_fet_options_u.bits.series_fet_b 	= bq76952_fet_configure_arpst->fet_series_en_b;

		ret_val_u16 += bq76952_write_data_memory_u8(0x9308, &bq76952_fet_options_u.byte, 1);
	}

	ret_val_u16 += bq76952_rd_subcmd_u8(SCMD_INDIR_MANF_STS,(U8*)&bq76952_manuf_stat_u.byte_u16,CMD_DIR_RESP_START,2,BQ76952_SUB_CMD_DLY_MS);

	if(bq76952_manuf_stat_u.bits.fet_en_b1 == (U8)bq76952_fet_configure_arpst->fet_test_mode_b)
	{
		ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_FET_EN,0,0, BQ76952_SUB_CMD_DLY_MS);
		ret_val_u16 += bq76952_rd_subcmd_u8(SCMD_INDIR_MANF_STS,(U8*)&bq76952_manuf_stat_u.byte_u16,CMD_DIR_RESP_START,2, BQ76952_SUB_CMD_DLY_MS);
	}

	return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined over voltage protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_cell_over_vtg_prot_u16(U16 ov_thres_mv_aru16, U16 delay_ms_aru16, U16 ov_recv_mv_aru16)
{
	U16 ret_val_u16 = 0;
	/* Threshold in mv -> 1012mv to 5566mv, Threshold in register -> 20 to 110; where 1 -> 50.6mv*/
  U8 thresh_u8 = (U8)(ov_thres_mv_aru16/50.6);

  /* Delay in ms -> 3.3ms to 6755ms, Delay in register -> 1 to 2047; where 1 -> 3.3ms*/
  U16 delay_reg_u16 = (U16)(delay_ms_aru16/3.3)-2; /* TODO: Check Why -2 required*/

	/* Recovery Delta in mv -> 101.2mv to 1012mv, Recovery in register -> 2 to 20; where 1 -> 50.6mv*/
  U8 recv_u8 = (U8)(ov_recv_mv_aru16/50.6);

  if((thresh_u8 < 20) || (thresh_u8 > 110))
  {
	  thresh_u8 = 86;
  }

	  ret_val_u16 += bq76952_write_data_memory_u8(0x9278, &thresh_u8, 1);

  if((delay_reg_u16 < 1) || (delay_reg_u16 > 2047))
  {
	  delay_reg_u16 = 74;
  }

	  U8 delay_reg_au8[2];

	  delay_reg_au8[0] = (delay_reg_u16 & 0xFF);
	  delay_reg_au8[1] = ((delay_reg_u16 >> 8) & 0xFF);

	  ret_val_u16 += bq76952_write_data_memory_u8(0x9279, delay_reg_au8, 2);


  if((recv_u8 < 2) || (recv_u8 > 20))
  {
	  recv_u8 = 2;
  }

  ret_val_u16 += bq76952_write_data_memory_u8(0x927C, &recv_u8, 1);

  /*Check Enabled protection1*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte, 1);

  if(bq76952_enabled_prot1_stat_u.bits.cell_ov_b1 != 1)
  {
	  bq76952_enabled_prot1_stat_u.bits.cell_ov_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte, 1);
  }

  /* Check Chg-FET OFF in protection1*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9265, &bq76952_chgfet_prot1_stat_u.byte, 1);

  if(bq76952_chgfet_prot1_stat_u.bits.cell_ov_b1 != 1)
  {
	  bq76952_chgfet_prot1_stat_u.bits.cell_ov_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9265, &bq76952_chgfet_prot1_stat_u.byte, 1);
  }



  return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined undervoltage protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_cell_under_vtg_prot_u16(U16 uv_thres_mv_aru16, U16 delay_ms_aru16, U16 uv_recv_mv_aru16)
{

	U16 ret_val_u16 = 0;
	/* Threshold in mv -> 1012mv to 4554mv, Threshold in register -> 20 to 90; where 1 -> 50.6mv*/
  U8 thresh_u8 = (U8)(uv_thres_mv_aru16/50.6);

  /* Delay in ms -> 3.3ms to 6755ms, Delay in register -> 1 to 2047; where 1 -> 3.3ms*/
  U16 delay_reg_u16 = (U16)(delay_ms_aru16/3.3)-2; /* TODO: Check Why -2 required*/

	/* Recovery Delta in mv -> 101.2mv to 1012mv, Recovery in register -> 2 to 20; where 1 -> 50.6mv*/
  U8 recv_u8 = (U8)(uv_recv_mv_aru16/50.6);

  if((thresh_u8 < 20) || (thresh_u8 > 90))
  {
	  thresh_u8 = 50;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9275, &thresh_u8, 1);
  }

  if((delay_reg_u16 < 1) || (delay_reg_u16 > 2047))
  {
	  delay_reg_u16 = 74;
  }
  else
  {
	  U8 delay_reg_au8[2];

	  delay_reg_au8[0] = ((delay_reg_u16 >> 8) & 0xFF);
	  delay_reg_au8[1] = (delay_reg_u16 & 0xFF);

	  ret_val_u16 += bq76952_write_data_memory_u8(0x9276, delay_reg_au8, 2);
  }

  if((recv_u8 < 2) || (recv_u8 > 20))
  {
	  recv_u8 = 2;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x927B, &recv_u8, 1);
  }

  /*Check Enabled protection1*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte, 1);

  if(bq76952_enabled_prot1_stat_u.bits.cell_uv_b1 != 1)
  {
	  bq76952_enabled_prot1_stat_u.bits.cell_uv_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9261, &bq76952_enabled_prot1_stat_u.byte, 1);
  }

  /* Check Dsg-FET OFF in protection1*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9269, &bq76952_dsgfet_prot1_stat_u.byte, 1);

  if(bq76952_dsgfet_prot1_stat_u.bits.cell_uv_b1 != 1)
  {
	  bq76952_dsgfet_prot1_stat_u.bits.cell_uv_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9269, &bq76952_dsgfet_prot1_stat_u.byte, 1);
  }


  return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined overtemperature- chg protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_cell_over_temp_chg_prot_u16(S8 ot_chg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_chg_recv_1c_ars8)
{
	U16 ret_val_u16 = 0;
	/* Threshold in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ot_chg_thres_1c_ars8 < -40) || (ot_chg_thres_1c_ars8 > 120))
  {
	  ot_chg_thres_1c_ars8= 55;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x929A, (U8*)&ot_chg_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_reg_u16 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x929B, &delay_1s_aru8, 1);
#if 0
  }
#endif
  if((ot_chg_recv_1c_ars8 < -40) || (ot_chg_recv_1c_ars8 > 120))
  {
	  ot_chg_recv_1c_ars8 = 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x929C, (U8*)&ot_chg_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.chg_ot_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.chg_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Chg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);

  if(bq76952_chgfet_prot2_stat_u.bits.chg_ot_b1 != 1)
  {
	  bq76952_chgfet_prot2_stat_u.bits.chg_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);
  }


  return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined over temperature dis-charging protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_cell_over_temp_dsg_prot_u16(S8 ot_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_dsg_recv_1c_ars8)
{

	U16 ret_val_u16 = 0;
	/* Threshold in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ot_dsg_thres_1c_ars8 < -40) || (ot_dsg_thres_1c_ars8 > 120))
  {
	  ot_dsg_thres_1c_ars8= 55;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x929D, (U8*)&ot_dsg_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_1s_aru8 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x929E, &delay_1s_aru8, 1);
#if 0
  }
#endif
  if((ot_dsg_recv_1c_ars8 < -40) || (ot_dsg_recv_1c_ars8 > 120))
  {
	  ot_dsg_recv_1c_ars8 = 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x929F, (U8*)&ot_dsg_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.dsg_ot_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.dsg_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Dsg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);

  if(bq76952_dsgfet_prot2_stat_u.bits.dsg_ot_b1 != 1)
  {
	  bq76952_dsgfet_prot2_stat_u.bits.dsg_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
  }


  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined under temperature CHG protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_cell_under_temp_chg_prot_u16(S8 ut_chg_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_chg_recv_1c_ars8)
{

	U16 ret_val_u16 = 0;
	/* Threshold & Recovery in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ut_chg_thres_1c_ars8 < -40) || (ut_chg_thres_1c_ars8 > 120))
  {
	  ut_chg_thres_1c_ars8= 55;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A6, (U8*)&ut_chg_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_1s_aru8 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A7, &delay_1s_aru8, 1);
#if 0
  }
#endif
  if((ut_chg_recv_1c_ars8 < -40) || (ut_chg_recv_1c_ars8 > 120))
  {
	  ut_chg_recv_1c_ars8 = 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A8, (U8*)&ut_chg_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.chg_ut_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.chg_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Chg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);

  if(bq76952_chgfet_prot2_stat_u.bits.chg_ut_b1 != 1)
  {
	  bq76952_chgfet_prot2_stat_u.bits.chg_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);
  }

  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined under temperature DSG protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_cell_under_temp_dsg_prot_u16(S8 ut_dsg_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_dsg_recv_1c_ars8)
{

	U16 ret_val_u16 = 0;
	/* Threshold & Recovery in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ut_dsg_thres_1c_ars8 < -40) || (ut_dsg_thres_1c_ars8 > 120))
  {
	  ut_dsg_thres_1c_ars8= 0;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A9, (U8*)&ut_dsg_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_1s_aru8 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AA, &delay_1s_aru8, 1);
#if 0
  }
#endif

  if((ut_dsg_recv_1c_ars8 < -40) || (ut_dsg_recv_1c_ars8 > 120))
  {
	  ut_dsg_recv_1c_ars8= 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AB, (U8*)&ut_dsg_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.dsg_ut_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.dsg_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Dsg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);

  if(bq76952_dsgfet_prot2_stat_u.bits.dsg_ut_b1 != 1)
  {
	  bq76952_dsgfet_prot2_stat_u.bits.dsg_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
  }
  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined internal over temperature protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_int_over_temp_prot_u16(S8 ot_int_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_int_recv_1c_ars8)
{
	U16 ret_val_u16 = 0;
	/* Threshold & Recovery in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ot_int_thres_1c_ars8 < -40) || (ot_int_thres_1c_ars8 > 120))
  {
	  ot_int_thres_1c_ars8= 0;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A9, (U8*)&ot_int_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_1s_aru8 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AA, &delay_1s_aru8, 1);
#if 0
  }
#endif

  if((ot_int_recv_1c_ars8 < -40) || (ot_int_recv_1c_ars8 > 120))
  {
	  ot_int_recv_1c_ars8= 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AB, (U8*)&ot_int_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.int_ot_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.int_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Chg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);

  if(bq76952_chgfet_prot2_stat_u.bits.int_ot_b1 != 1)
  {
	  bq76952_chgfet_prot2_stat_u.bits.int_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);
  }

  /* Check Dsg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);

  if(bq76952_dsgfet_prot2_stat_u.bits.int_ot_b1 != 1)
  {
	  bq76952_dsgfet_prot2_stat_u.bits.int_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
  }
  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined internal under temperature protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_int_under_temp_prot_u16(S8 ut_int_thres_1c_ars8, U8 delay_1s_aru8, S8 ut_int_recv_1c_ars8)
{
	U16 ret_val_u16 = 0;
	/* Threshold & Recovery in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ut_int_thres_1c_ars8 < -40) || (ut_int_thres_1c_ars8 > 120))
  {
	  ut_int_thres_1c_ars8= 0;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A9, (U8*)&ut_int_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_1s_aru8 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AA, &delay_1s_aru8, 1);
#if 0
  }
#endif

  if((ut_int_recv_1c_ars8 < -40) || (ut_int_recv_1c_ars8 > 120))
  {
	  ut_int_recv_1c_ars8= 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AB, (U8*)&ut_int_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.int_ut_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.int_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Chg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);

  if(bq76952_chgfet_prot2_stat_u.bits.int_ut_b1 != 1)
  {
	  bq76952_chgfet_prot2_stat_u.bits.int_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);
  }

  /* Check Dsg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);

  if(bq76952_dsgfet_prot2_stat_u.bits.int_ut_b1 != 1)
  {
	  bq76952_dsgfet_prot2_stat_u.bits.int_ut_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
  }

  return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined FET over temperature protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_fet_over_temp_prot_u16(S8 ot_fet_thres_1c_ars8, U8 delay_1s_aru8, S8 ot_fet_recv_1c_ars8)
{
	U16 ret_val_u16 = 0;
	/* Threshold in degC ( in register also same, as register units 1degC) -> -40 to 120C */

  /* Delay in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/

  if((ot_fet_thres_1c_ars8 < -40) || (ot_fet_thres_1c_ars8 > 120))
  {
	  ot_fet_thres_1c_ars8= 0;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92A9, (U8*)&ot_fet_thres_1c_ars8, 1);
  }

#if 0 /*Since U8 variable cant hold less than 0 and greater than 255 - This check is skipped*/
  if((delay_1s_aru8 < 0) || (delay_1s_aru8 > 255))
  {
	  delay_1s_aru8 = 2;
  }
  else
  {
#endif
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AA, &delay_1s_aru8, 1);
#if 0
  }
#endif

  if((ot_fet_recv_1c_ars8 < -40) || (ot_fet_recv_1c_ars8 > 120))
  {
	  ot_fet_recv_1c_ars8 = 5;
  }
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x92AB, (U8*)&ot_fet_recv_1c_ars8, 1);
  }

  /*Check Enabled protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);

  if(bq76952_enabled_prot2_stat_u.bits.fet_ot_b1 != 1)
  {
	  bq76952_enabled_prot2_stat_u.bits.fet_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9262, &bq76952_enabled_prot2_stat_u.byte, 1);
  }

  /* Check Chg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);

  if(bq76952_chgfet_prot2_stat_u.bits.fet_ot_b1 != 1)
  {
	  bq76952_chgfet_prot2_stat_u.bits.fet_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9266, &bq76952_chgfet_prot2_stat_u.byte, 1);
  }

  /* Check Dsg-FET OFF in protection2*/
  ret_val_u16 += bq76952_read_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);

  if(bq76952_dsgfet_prot2_stat_u.bits.fet_ot_b1 != 1)
  {
	  bq76952_dsgfet_prot2_stat_u.bits.fet_ot_b1 = 1;
	  ret_val_u16 += bq76952_write_data_memory_u8(0x926A, &bq76952_dsgfet_prot2_stat_u.byte, 1);
  }

  return ret_val_u16;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
	Set user-defined  recovery time for following protections
	1. cell under voltage
	2. cell over voltage
	3. cell over voltage- latch
	4. over current charging
	5. over current Discharging 1, 2, 3
	6. over temperature charging
	7. over temperature discharging
	8. over temperature FET
	9. over temperature Internal
	10. over temperature charging
	11. over temperature discharging
	12. over temperature Internal
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_recv_time_u16(U8 delay_s_aru8)
{
	U16 ret_val_u16 = 0;

	ret_val_u16 += bq76952_write_data_memory_u8(0x92AF, &delay_s_aru8, 1);

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined  charging over current protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_chg_ocp_u16(U16 chg_ocp_ma_aru16, U16 delay_ms_aru16,S16 recv_thres_ma_ars16, S16 pk_tos_del_cv_ars16)
{
	U16 ret_val_u16 = 0;
	/* Threshold in mv -> 4mv to 124mv, Threshold in register -> 2 to 62; where 1 -> 2mv
	 *
	 *
	 * Threshold in mA ->
	 *  MIN 0.2-mOhm Resistor - 20A to 620A
	 *  MAX 1-mOhm Resistor - 4A to 124A
	 *
	 * */

	U8 thresh_u8 = (U8)((chg_ocp_ma_aru16 * BQ76952_CURR_SNS_RESISTOR )/2);

	/* Delay in ms -> 3.3ms to 419.1ms, Delay in register -> 1 to 127; where 1 -> 3.3ms*/
	U8 delay_reg_u8 = (U8)(delay_ms_aru16/3.3)-2;/* TODO: Check Why -2 required*/

	/* Recovery Threshold in mA(same in register) -> -32768mA to 32767mA,  where 1 -> 1mA*/
	S16 recv_reg_s16 = recv_thres_ma_ars16;

	/* Pack - TOS Delta in cv(same in register)-> 10cV to 8500cV,  where 1 -> 1cV or 10mV*/
	S16 pk_tos_reg_s16 = pk_tos_del_cv_ars16;

  if((thresh_u8 < 2) || (thresh_u8 > 62))
  {
	  thresh_u8 = 2;
  }

  ret_val_u16 += bq76952_write_data_memory_u8(0x9280, &thresh_u8, 1);

  if((delay_reg_u8 < 1) || (delay_reg_u8 > 127))
  {
	  delay_reg_u8 = 4;
  }

  ret_val_u16 += bq76952_write_data_memory_u8(0x9281, &delay_reg_u8, 1);

  U8 recv_reg_au8[2];

  recv_reg_au8[0] = (recv_reg_s16 & 0xFF);
  recv_reg_au8[1] = ((recv_reg_s16 >> 8) & 0xFF);

  ret_val_u16 += bq76952_write_data_memory_u8(0x9288, recv_reg_au8, 2);

  U8 ptos_del_reg_au8[2];

  ptos_del_reg_au8[0] = (pk_tos_reg_s16 & 0xFF);
  ptos_del_reg_au8[1] = ((pk_tos_reg_s16 >> 8) & 0xFF);

  ret_val_u16 += bq76952_write_data_memory_u8(0x92B0, ptos_del_reg_au8, 2);

  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined Dis-charging over current protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_dchg_ocp_u16(U16 dchg_ocp_ma_aru16, U16 delay_ms_aru16)
{
	U16 ret_val_u16 = 0;
	/* Threshold in mv -> 4mv to 200mv, Threshold in register -> 2 to 100; where 1 -> 2mv
	 *
	 *
	 * Threshold in mA ->
	 *  MIN 0.2mOhm Resistor - 20A to 1000A
	 *  MAX 1Ohm Resistor - 4A to 100A
	 *
	 * */

	U8 thresh_u8 = (U8)((dchg_ocp_ma_aru16 * BQ76952_CURR_SNS_RESISTOR )/2);

  /* Delay in ms -> 3.3ms to 419.1ms, Delay in register -> 1 to 127; where 1 -> 3.3ms*/
  U8 delay_reg_u8 = (U8)(delay_ms_aru16/3.3)-2; /* TODO: Check Why -2 required*/
  if((thresh_u8 < 2) || (thresh_u8 > 100))
  {
	  thresh_u8 = 2;
  }
  else
  {
    ret_val_u16 += bq76952_write_data_memory_u8(0x9282, &thresh_u8, 1);
  }
  if((delay_reg_u8 < 1) || (delay_reg_u8 > 127))
  {
	  delay_reg_u8 = 1;
  }
  else
  {
    ret_val_u16 += bq76952_write_data_memory_u8(0x9283, &delay_reg_u8, 1);
  }

  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined Short Circuit in Dis-charge protection
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_dischg_scp_u16(bq76952_scd_thresh_te thresh_are, U16 delay_us_aru16, U8 recv_time_1s_aru8 )
{
	U16 ret_val_u16 = 0;
	/* Delay in us -> 15us to 465us*/
	/* delay in reg value -> 1 to 31 where 1 = 15us*/
	  U8 delay_reg_u8 = (U8)(delay_us_aru16/15)+1;

	 /* Recovery time in Sec ( in register also same, as register units 1Sec) -> 0s to 255s*/
	  U8 recv_delay_reg_u8 = recv_time_1s_aru8;



  ret_val_u16 += bq76952_write_data_memory_u8(0x9286, &thresh_are, 1);

  if((delay_reg_u8 < 1) || (delay_reg_u8 > 31))
	  delay_reg_u8 = 2;	/* default value as per bq76952 datasheet. But here this logic doesnt change delay*/
  else
  {
	  ret_val_u16 += bq76952_write_data_memory_u8(0x9287, &delay_reg_u8, 1);
  }

  ret_val_u16 += bq76952_write_data_memory_u8(0x9294, &recv_delay_reg_u8, 1);

  return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined FET State
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_fet_u16(bq76952_fet_te fet_type_are, bq76952_fet_state_te fet_state_are)
{
	U16 ret_val_u16 = 0;
	U8 rd_check_u8 = 0;

	ret_val_u16 += bq76952_rd_subcmd_u8(SCMD_INDIR_FET_CTRL,&bq76952_fet_ctrl_u.byte,CMD_DIR_RESP_START,1, BQ76952_SUB_CMD_DLY_MS);

	switch(fet_type_are)
	{
	  case CHG:
		  if(bq76952_fet_ctrl_u.bits.chg_off_b1 == fet_state_are)
		  {
			  return ret_val_u16;
		  }
		  bq76952_fet_ctrl_u.bits.chg_off_b1 = fet_state_are;
		  break;
	  case DSG:
		  if(bq76952_fet_ctrl_u.bits.dsg_off_b1 == fet_state_are)
		  {
			  return ret_val_u16;
		  }
		  bq76952_fet_ctrl_u.bits.dsg_off_b1 = fet_state_are;
		  break;
	  case PCHG:
		  if(bq76952_fet_ctrl_u.bits.pchg_off_b1 == fet_state_are)
		  {
			  return ret_val_u16;
		  }
		  bq76952_fet_ctrl_u.bits.pchg_off_b1 = fet_state_are;
		  break;
	  case PDSG:
		  if(bq76952_fet_ctrl_u.bits.pdsg_off_b1 == fet_state_are)
		  {
			  return ret_val_u16;
		  }
		  bq76952_fet_ctrl_u.bits.pdsg_off_b1 = fet_state_are;
		  break;
	  case ALL:
		  if(ON == fet_state_are)
		  {
			ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_ALL_FETS_ON,0,0, BQ76952_SUB_CMD_DLY_MS);
		  }
		  else
		  {
			 ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_ALL_FETS_OFF,0,0, BQ76952_SUB_CMD_DLY_MS);
		  }

		  break;
	  default:
		  break;
	}

	if(ALL != fet_type_are)
	{
		ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_FET_CTRL,&bq76952_fet_ctrl_u.byte,1, BQ76952_SUB_CMD_DLY_MS);
	}

	ret_val_u16 += bq76952_rd_dir_cmd_u8(CMD_DIR_FET_STAT,&bq76952_fet_stat_u.byte,1);

	ret_val_u16 += bq76952_rd_subcmd_u8(SCMD_INDIR_FET_CTRL,&rd_check_u8,CMD_DIR_RESP_START,1, BQ76952_SUB_CMD_DLY_MS);

	ret_val_u16 += bq76952_get_ic_stat_u16(BATT_STS);
	ret_val_u16 += bq76952_get_ic_stat_u16(CTRL_STS);

	if((ret_val_u16 == 0) && (rd_check_u8 != bq76952_fet_ctrl_u.byte))
	{
		ret_val_u16 = 0xFF; /*Firmware FET Control - not properly enabled/ initialized*/
		return ret_val_u16;
	}

	if ((ret_val_u16 == 0) && ((bq76952_fet_stat_u.bits.chg_fet_b1 == bq76952_fet_ctrl_u.bits.chg_off_b1) ||
			(bq76952_fet_stat_u.bits.dsg_fet_b1 == bq76952_fet_ctrl_u.bits.dsg_off_b1) ||
			(bq76952_fet_stat_u.bits.pchg_fet_b1 == bq76952_fet_ctrl_u.bits.pchg_off_b1) ||
			(bq76952_fet_stat_u.bits.pdsg_fet_b1 == bq76952_fet_ctrl_u.bits.pdsg_off_b1)))
	{
		ret_val_u16 = 0xFE; /*Firmware FET Control malfunctioning*/
	}

	return ret_val_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  : Set user-defined FET State
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 bq76952_set_fet_test_u16(bq76952_fet_te fet_type_are, bq76952_fet_state_te fet_state_are)
{
	U16 ret_val_u16 = 0;
	U8 rd_check_u16 = 0;

	ret_val_u16 += bq76952_rd_subcmd_u8(SCMD_INDIR_MANF_STS,(U8*)&bq76952_manuf_stat_u.byte_u16,CMD_DIR_RESP_START,2, BQ76952_SUB_CMD_DLY_MS);

	switch(fet_type_are)
	{
	  case CHG:
		  if(bq76952_manuf_stat_u.bits.chg_test_b1 == (U8)(fet_state_are))
		  {
			  ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_CHGFET_TEST,0,0, BQ76952_SUB_CMD_DLY_MS);
		  }

		  bq76952_manuf_stat_u.bits.chg_test_b1 =  (!fet_state_are);
		  break;
	  case DSG:
		  if(bq76952_manuf_stat_u.bits.dsg_test_b1 == (U8)(fet_state_are))
		  {
			  ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_DSGFET_TEST,0,0, BQ76952_SUB_CMD_DLY_MS);
		  }

		  bq76952_manuf_stat_u.bits.dsg_test_b1 =  (!fet_state_are);

		  break;
	  case PCHG:
		  if(bq76952_manuf_stat_u.bits.pchg_test_b1 == (U8)(fet_state_are))
		  {
			  ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_PCHGFET_TEST,0,0, BQ76952_SUB_CMD_DLY_MS);
		  }

		  bq76952_manuf_stat_u.bits.pchg_test_b1 =  (!fet_state_are);

		  break;
	  case PDSG:
		  if(bq76952_manuf_stat_u.bits.pdsg_test_b1 == (U8)(fet_state_are))
		  {
			  ret_val_u16 += bq76952_wr_subcmd_u8(SCMD_INDIR_PDSGFET_TEST,0,0, BQ76952_SUB_CMD_DLY_MS);
		  }

		  bq76952_manuf_stat_u.bits.pdsg_test_b1 =  (!fet_state_are);

		  break;
	  default:
		  break;
	}

	ret_val_u16 += bq76952_rd_subcmd_u8(SCMD_INDIR_MANF_STS,(U8*)&rd_check_u16,CMD_DIR_RESP_START,1, BQ76952_SUB_CMD_DLY_MS);

	if((ret_val_u16 == 0) && (rd_check_u16 != bq76952_manuf_stat_u.byte_u16))
	{
		ret_val_u16 = 0xFE;
		bq76952_manuf_stat_u.byte_u16 = rd_check_u16;
	}


	return ret_val_u16;
}
// Measure chip temperature in °C
U8 bq76952_get_internal_temp_u8(SFP* int_temp_val_arpsfp)
{
	U8 ret_val_u8 = 0;
	U8 rd_int_temp_au8[2];
  U16 raw_temp_u16 = 0;

  ret_val_u8 = bq76952_rd_dir_cmd_u8(CMD_DIR_INT_TEMP, rd_int_temp_au8, 2);

  if(ret_val_u8)
  {
	 return ret_val_u8;
  }

  raw_temp_u16 = ((rd_int_temp_au8[1] << 8) | rd_int_temp_au8[0]);

  float raw = raw_temp_u16/10.0;
  *int_temp_val_arpsfp =  (raw - 273.15);

  return ret_val_u8;
}

// Measure thermistor temperature in °C
U8 bq76952_get_thermistor_temp_u8(bq76952_thermistor_te thermistor_are, SFP* temp_val_arpsfp )
{
	U8 ret_val_u8 = 0;
  U8 cmd_u8;
  U8 rd_temp_au8[2] = {0,0};
  U16 raw_temp_u16 = 0;
  switch(thermistor_are)
  {
    case TS1:
    	cmd_u8 = 0x70;
      break;
    case TS2:
    	cmd_u8 = 0x72;
      break;
    case TS3:
    	cmd_u8 = 0x74;
      break;
    case HDQ:
    	cmd_u8 = 0x76;
      break;
    case DCHG:
    	cmd_u8 = 0x78;
      break;
    case DDSG:
    	cmd_u8 = 0x7A;
      break;
    case CFET_OFF:
    	cmd_u8 = 0x6A;
      break;
    case DFET_OFF:
    	cmd_u8 = 0x6C;
      break;
    case ALERT:
    	cmd_u8 = 0x6E;
      break;
    default:
    {

    }
  }

  ret_val_u8 = bq76952_rd_dir_cmd_u8(cmd_u8, rd_temp_au8, 2);

  if(ret_val_u8)
  {
	 return ret_val_u8;
  }

  raw_temp_u16 = ((rd_temp_au8[1] << 8) | rd_temp_au8[0]);

  float raw = raw_temp_u16/10.0;
  *temp_val_arpsfp =  (raw - 273.15);

  return ret_val_u8;
}


// Compute checksome = ~(sum of all bytes)
U8 bq76952_compute_checksum_u8(U8 old_check_sum_aru8, U8 data_aru8)
{
  if(!old_check_sum_aru8)
	  old_check_sum_aru8 = data_aru8;
  else
	  old_check_sum_aru8 = ~(old_check_sum_aru8) + data_aru8;

  return ~(old_check_sum_aru8);
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 bq76952_crc_u8(U8 *ptr, U8 len)
{
	U8 i;
	U8 checksum = 0;

	for(i=0; i<len; i++)
		checksum += ptr[i];

	checksum = 0xff & ~checksum;

	return(checksum);
}


// Write Byte to Data memory of BQ76952
U8 bq76952_write_data_memory_u8(U16 addr_aru16, U8* data_arpu8, U8 num_bytes_aru8)
{
	U8 ret_val_u8 = 0;
	U8 check_sum_u8 = 0;
	U8 loop_var_u8 = 0;
	U32 check_sum_u32 =0;
//	check_sum_u8 = bq76952_compute_checksum_u8(check_sum_u8, BQ_I2C_ADDR_WRITE);
//	check_sum_u8 = bq76952_compute_checksum_u8(check_sum_u8, CMD_DIR_SUBCMD_LOW);
//	check_sum_u8 = bq76952_compute_checksum_u8(check_sum_u8, LOW_BYTE(addr_aru16));
//	check_sum_u8 = bq76952_compute_checksum_u8(check_sum_u8, HIGH_BYTE(addr_aru16));

	check_sum_u32 = LOW_BYTE(addr_aru16) + HIGH_BYTE(addr_aru16);
	for(loop_var_u8 = 0;loop_var_u8 < num_bytes_aru8;loop_var_u8++)
	{
		check_sum_u32 += data_arpu8[loop_var_u8];
		//check_sum_u8 = bq76952_compute_checksum_u8(check_sum_u8, data_arpu8[loop_var_u8]);
	}

	check_sum_u8 = (U8)(~(check_sum_u32) & 0xFF);

	ret_val_u8 += bq76952_config_update_u8(ENTER_CONFIG_MODE);

	if(ret_val_u8 != 0)
	{
		return ret_val_u8;
	}

	ret_val_u8 += bq76952_wr_subcmd_u8(addr_aru16,data_arpu8,num_bytes_aru8,1);

	if(ret_val_u8 != 0)
	{
		return ret_val_u8;
	}
	U8 check_sum_data_au8[2];
	check_sum_data_au8[0] = check_sum_u8;
	check_sum_data_au8[1] = 4 + num_bytes_aru8; /* Data length that used to calculate Check Sum*/

	ret_val_u8 += bq76952_wr_dir_cmd_u8(CMD_DIR_RESP_CHKSUM, check_sum_data_au8, 2);

	ret_val_u8 += bq76952_config_update_u8(EXIT_CONFIG_MODE);

	return ret_val_u8;
}

// Read Byte from Data memory of BQ76952
U8 bq76952_read_data_memory_u8(U16 addr_aru16, U8* rd_data_arpu8, U8 num_bytes_aru8)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 += bq76952_rd_subcmd_u8(addr_aru16,rd_data_arpu8,CMD_DIR_RESP_START,num_bytes_aru8, 1);

	return ret_val_u8;
}

// Enter/Exit config update mode
U8 bq76952_config_update_u8(bq76952_configmode_te bq76952_configmode_are)
{
	U8 ret_val_u8 = 0;
	U8 bq76952_tx_au8[3];
	bq76952_batt_stat_tu batt_stat_u;
	U8 time_out_u8 = 100;
	BOOL req_state_b = 0;

	bq76952_tx_au8[0] = 0x3E; /* Sub- Cmd write Regiser*/

	bq76952_tx_au8[1] = bq76952_configmode_are;
	bq76952_tx_au8[2] = 0x00;

	ret_val_u8 = bq76952_rd_dir_cmd_u8(0x12, (U8*)&batt_stat_u, 2);

	//HAL_Delay(BQ76952_SUB_CMD_DLY_MS);
	ret_val_u8 = i2c_app_tx_u8(I2C_APP_INSTANCE, BQ_I2C_ADDR, bq76952_tx_au8, 3, 0);

	if(ret_val_u8 != 0)
	{
		return 1;
	}


	if(ENTER_CONFIG_MODE == bq76952_configmode_are)
	{
		req_state_b = 1;
	}
	else
	{
		req_state_b = 0;
	}
	while((batt_stat_u.bits.cfg_update_b1 != req_state_b) && (time_out_u8 != 0))
	{
		ret_val_u8 = bq76952_rd_dir_cmd_u8(0x12, (U8*)&batt_stat_u, 2);
		time_out_u8--;
	}

	if(batt_stat_u.bits.cfg_update_b1 != req_state_b)
	{
	return 1;
	}


  return ret_val_u8;
}

U8 bq_slp_chck()
{
	U8 val;
	U8 ret_val_u8;
	bq76952_alarm_tu   bq76952_alarm_read;
	ret_val_u8 += bq76952_rd_dir_cmd_u8(CMD_DIR_ALRM_RAW_STAT,(U8*)&bq76952_alarm_read.byte_u16,2);
	if(bq76952_alarm_read.bits.wake_up_b1)
	{
		val++;
	}
	return(ret_val_u8);
}
U8 bq76952_sleep_int_cnf()
{
	U8 ret_val_u8;
	U16 temp_sensor_en_u16 = 0xB10;
//	U8 temp_u8 = temp_sensor_en_u16;
//	bq76952_alm_stat_u;			/* Alert Pin Triggers*/
//	bq76952_alarm_tu			bq76952_alm_raw_stat_u;		/* No Alert Pin Triggers*/
//	bq76952_alarm_tu			bq76952_alm_en_u;

	bq76952_alarm_tu   bq76952_alarm_read;
	bq76952_alarm_tu   bq76952_alarm_read_stat;

	bq76952_alm_stat_u.bits.wake_up_b1 = 1;
//	ret_val_u8 += bq76952_write_data_memory_u8(0x92FC,&temp_sensor_en_u16,1);
	ret_val_u8 += bq76952_wr_dir_cmd_u8(CMD_DIR_ALRM_RAW_STAT,(U8*)&bq76952_alm_stat_u.byte_u16,2);
	ret_val_u8 += bq76952_rd_dir_cmd_u8(CMD_DIR_ALRM_EN,(U8*)&bq76952_alarm_read.byte_u16,2);
#if 0
	bq76952_write_data_memory_u8(0x92FC,&bq76952_alm_stat_u.byte_u16,1); /*Alert Pin */
	ret_val_u16 += bq76952_read_data_memory_u8(0x92FC,&bq76952_alarm_read.byte_u16,2); /*Alert Pin */

	bq76952_alm_stat_u.bits.wake_up_b1 = 0;
	bq76952_alm_stat_u.byte_u16 = 0x01;
//	ret_val_u16 += bq76952_write_data_memory_u8(0x92FC,&bq76952_alm_stat_u.byte_u16,1); /*Alert Pin */
//	temp_sensor_en_u8 = 0;
	ret_val_u16 += bq76952_read_data_memory_u8(0x92FC,&bq76952_alarm_read.byte_u16,2); /*Alert Pin */
	ret_val_u16 += bq76952_rd_dir_cmd_u8(CMD_DIR_ALRM_STAT,(U8*)&bq76952_alarm_read_stat,2);
	if (temp_sensor_en_u8 !=temp_u8 )
	{
		ret_val_u16++;
	}
#endif
#if 0
	temp_u8 = 0;
	bq76952_alm_en_u.byte_u16 = 0xF801;
	ret_val_u16 += bq76952_write_data_memory_u8(0x926D,
				&bq76952_alm_en_u.byte_u16, 2);
	bq76952_alm_en_u.byte_u16 = 0;
	ret_val_u16 += bq76952_read_data_memory_u8(0x926D,
					&bq76952_alm_en_u.byte_u16, 2);
	if ((bq76952_alm_en_u.byte_u16 == 0xF800) || (bq76952_alm_en_u.byte_u16 == 0))
	{
		ret_val_u16++;
	}
	bq76952_alm_stat_u.byte_u16 = 0xFFFF;
			ret_val_u16 += bq76952_wr_dir_cmd_u8(CMD_DIR_ALRM_STAT,(U8*)&bq76952_alm_stat_u,2);
			bq76952_alm_stat_u.byte_u16 = 0xFFFF;

	ret_val_u16 += bq76952_rd_dir_cmd_u8(CMD_DIR_ALRM_STAT,(U8*)&bq76952_alm_stat_u,2);
	if(bq76952_alm_stat_u.bits.wake_up_b1)
	{

	}
#endif
	return(ret_val_u8);
}

