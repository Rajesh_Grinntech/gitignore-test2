/**
========================================================================================================================
@page adc_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	adc_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	20-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

/* Configuration Header File */
#include "ai_app.h"
#include "gt_system.h"

/* This header file */
#include "adc_app.h"

/* Driver header */
#include "gt_adc.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */

/** @{
 * Private Variable Definitions */
adc_app_tst adc_app_config_gst = /* adc Details */
{
   2000000, /* voltage_div_resistor_ohm_u32 */
   3300,  /* adc_ref_voltage_1mV_u16 */
   4096,  /* adc_resolution_expanded_u16 */ /* 12 bit ADC = 2 ^ 12 = 4096 */
   56000    /* adc_filterseries_resistor_1ohm_u16 */
};

static U16 adc_raw_val_au16[ADC_APP_NUM_DIRECT];

/** @} */

/** @{
 *  Public Variable Definitions */
adc_app_data_tst   adc_app_data_gast[ADC_APP_NUM_AI];
/** @} */



/* Public Function Definitions */
U8 adc_app_init_adc_u8(adc_app_instances_te instances_are);
U8 adc_app_start_conversion_u8(void);

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 adc_app_init_adc_u8(adc_app_instances_te instances_are)
{
	U8 ret_val_u8 = 0;
	
	ret_val_u8 = gt_adc_init_u8(instances_are);
	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 adc_app_start_conversion_u8(void)
{
	U8 ret_val_u8 = 0;
	
	ret_val_u8 = gt_adc_start_conversion_u8();

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 adc_app_read_ai_u8(void)
{
	U8 conversion_result_u8 = ADC_APP_FAIL;
	U8 channel_u8 = 0;

	conversion_result_u8 = gt_adc_read_data_u8(adc_raw_val_au16);

	if (ADC_APP_SUCCESS == conversion_result_u8)
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_ADC_READ);
		//adc_app_data_gast[channel_u8++].raw_u16 = adc_raw_val_au16[0];
		//adc_app_data_gast[channel_u8++].raw_u16 = adc_raw_val_au16[1];
		for (channel_u8 = ADC_CHANNEL_START; channel_u8 < ADC_APP_NUM_DIRECT; channel_u8++)
		{
			adc_app_data_gast[channel_u8].raw_u16 = adc_raw_val_au16[channel_u8];

			if(ADC_APP_INT_TEMP_AI_C == channel_u8)
			{
				ai_app_status_gast[channel_u8].scaled_s32 = __LL_ADC_CALC_TEMPERATURE(TEMPSENSOR_CAL_VREFANALOG,adc_app_data_gast[channel_u8].raw_u16, LL_ADC_RESOLUTION_12B);
			}
			else
			{
#if 0
					ai_app_status_gast[channel_u8].state_e = AI_READING;
					if ((adc_app_data_gast[channel_u8].raw_u16 > 10) &&
						(adc_app_data_gast[channel_u8].raw_u16 < (adc_app_config_gst.adc_resolution_expanded_u16 - 50)))
					{
						adc_app_data_gast[channel_u8].scaled_s32 = (S32)ntc_adc_compute_store_temperature_s16(
																						adc_app_data_gast[channel_u8].raw_u16);


					}
					else
					{
						ai_app_status_gast[channel_u8].state_e = AI_FAULT;
						adc_app_data_gast[channel_u8].scaled_s32 = ADC_FAULT_VALUE_S32;
					}
#endif

#if 1
					ai_app_status_gast[channel_u8].state_e = AI_READING;
					adc_app_data_gast[channel_u8].raw_u16 = adc_app_data_gast[channel_u8].raw_u16+80;
					if (adc_app_data_gast[channel_u8].raw_u16 > 10
							&& adc_app_data_gast[channel_u8].raw_u16 < (adc_app_config_gst.adc_resolution_expanded_u16 - 50))

					{
						/* TODO Name Change of aux_path_resistance_f32 variable*/
						SFP aux_path_resistance_f32 = ((SFP) (adc_app_config_gst.voltage_div_resistor_1ohm_u32
								+ adc_app_config_gst.adc_filter_series_resistor_1ohm_u16))
								/ adc_app_config_gst.adc_filter_series_resistor_1ohm_u16;

#if 1
						adc_app_data_gast[channel_u8].scaled_u32 = adc_app_config_gst.adc_ref_voltage_1mV_u16
								* adc_app_data_gast[channel_u8].raw_u16
								/ adc_app_config_gst.adc_resolution_expanded_u16;

						adc_app_data_gast[channel_u8].scaled_s32 =
								(adc_app_data_gast[channel_u8].scaled_u32) * aux_path_resistance_f32
										* 100/ COM_HDR_SCALE_1000;
#endif
					}
					else
					{
						ai_app_status_gast[channel_u8].state_e = AI_FAULT;
						adc_app_data_gast[channel_u8].scaled_s32 = ADC_FAULT_VALUE_S32;
					}
#endif
					ai_app_status_gast[channel_u8].scaled_s32 = adc_app_data_gast[channel_u8].scaled_s32;
			}

		}
	}
	else
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_ADC_READ);
	}

	return conversion_result_u8;
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
