/**
====================================================================================================================================================================================
@page i2c_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	i2c_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	18-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

/* This header file */
#include "i2c_app.h"
//#include "gt_system.h"
/* Driver header */
#include "gt_i2c.h"
#include "task.h"
//#include "app_main.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 i2c_app_init_u8(i2c_app_inst_type_te inst_type_are)
{
	U8 return_status_u8 = 0;

	if(gt_i2c_init_u16(inst_type_are))
	{
		//app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_I2C_INIT);
		return_status_u8++;
	}

	return return_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 i2c_app_tx_u8(i2c_app_inst_type_te inst_type_are, U8 slave_addr_aru8, U8 *data_arpu8, U32 data_len_aru32, U8 is_send_stop_aru8)
{
   U8 return_status_u8 = 0;
   U8 retry_times_u8 = 2;

   switch(inst_type_are)
   {
   	   /*******************************************************************************************************************************/
   	   case I2C_APP_INSTANCE:
   	   {
    		   vTaskSuspendAll();
   		   do
   		   {
   			   return_status_u8 = 0;
   			   if(!return_status_u8)
   			   {
   				   if(gt_i2c_set_slave_address_u16(inst_type_are,slave_addr_aru8,COM_HDR_FALSE))
   				   {
   					   return_status_u8++;
   				   }
   			   }

   			   if(!return_status_u8)
   			   {
   			   	   if(gt_i2c_write_data_blocking_u16(inst_type_are,data_arpu8,data_len_aru32,is_send_stop_aru8))
   			   	   {
   				   	   return_status_u8++;
   			   	   }
   			   }
   		   retry_times_u8--;
   		   }while(return_status_u8 && retry_times_u8);
    		   xTaskResumeAll();
   	   }
   	   break;
   	   /*******************************************************************************************************************************/
   	   case I2C_APP_INSTANCE_FLEXIO:
   	   {
   		  // vTaskSuspendAll();
   		   do
   		   {
   			   return_status_u8 = 0;
   			   if(!return_status_u8)
   			   {
   				   if(gt_i2c_set_slave_address_u16(inst_type_are,slave_addr_aru8,COM_HDR_FALSE))
   				   {
   					   return_status_u8++;
   				   }
   			   }

   			   if(!return_status_u8)
   			   {
   			   	   if(gt_i2c_write_data_blocking_u16(inst_type_are,data_arpu8,data_len_aru32,is_send_stop_aru8))
   			   	   {
   				   	   return_status_u8++;
   			   	   }
   			   }

   		   retry_times_u8--;
   		   }while(return_status_u8 && retry_times_u8);
   		  // xTaskResumeAll();
   	   }
   	   break;
   	   /*******************************************************************************************************************************/
   	   default :
   	   {

   	   }
   	   break;
   }


   return return_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 i2c_app_rx_u8(i2c_app_inst_type_te inst_type_are, U8 slave_addr_aru8, U8 *rx_data_arpu8, U32 data_len_aru32, U8 is_send_stop_aru8)
{
	U8 return_status_u8 = 0;
	U8 retry_times_u8 = 2;

	switch(inst_type_are)
	{
		/*******************************************************************************************************************************/
		case I2C_APP_INSTANCE:
		{
	   		   vTaskSuspendAll();
			do
			{
				return_status_u8 = 0;

				   if(!return_status_u8)
				   {
					   if(gt_i2c_set_slave_address_u16(inst_type_are,slave_addr_aru8,COM_HDR_FALSE))
					   {
						   return_status_u8++;
					   }
				   }

				   if(!return_status_u8)
				   {
					   if(gt_i2c_read_data_blocking_u16(inst_type_are, rx_data_arpu8, data_len_aru32, is_send_stop_aru8))
					   {
						   return_status_u8++;
					   }
				   }

			 retry_times_u8--;
			}while(return_status_u8 && retry_times_u8);
 		   xTaskResumeAll();
		}
		break;
		/*******************************************************************************************************************************/
		case I2C_APP_INSTANCE_FLEXIO:
		{
			//vTaskSuspendAll();
			do
			{
				return_status_u8 = 0;

				   if(!return_status_u8)
				   {
					   if(gt_i2c_set_slave_address_u16(inst_type_are,slave_addr_aru8,COM_HDR_FALSE))
					   {
						   return_status_u8++;
					   }
				   }

				   if(!return_status_u8)
				   {
					   if(gt_i2c_read_data_blocking_u16(inst_type_are, rx_data_arpu8, data_len_aru32, is_send_stop_aru8))
					   {
						   return_status_u8++;
					   }
				   }

			 retry_times_u8--;
			}while(return_status_u8 && retry_times_u8);
			//xTaskResumeAll();
		}
		break;
		/*******************************************************************************************************************************/
		default :
		{

		}
		break;
		/*******************************************************************************************************************************/
	}
    return return_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/

U8 i2c_app_tx_rx_u8(i2c_app_inst_type_te inst_type_are, U8 slave_addr_aru8, U8 *data_arpu8,U32 data_len_tx_aru32,
							U8 is_send_stop_tx_aru8,U8 *rx_data_arpu8, U32 data_len_rx_aru32, U8 is_send_stop_rx_aru8)
{
	U8 return_status_u8 = 0;
	U8 retry_times_u8 = 2;

	switch(inst_type_are)
	{
	  	/*******************************************************************************************************************************/
		case I2C_APP_INSTANCE:
		{
	   		   vTaskSuspendAll();
			do
			{
				return_status_u8 = 0;

				   if(!return_status_u8)
				   {
					   if(gt_i2c_set_slave_address_u16(inst_type_are,slave_addr_aru8,COM_HDR_FALSE))
					   {
						   return_status_u8++;
					   }
				   }

				   if(!return_status_u8)
				   {
				   	   if(gt_i2c_write_data_blocking_u16(inst_type_are,data_arpu8,data_len_tx_aru32,is_send_stop_tx_aru8))
				   	   {
					   	   return_status_u8++;
				   	   }
				   }

				   if(!return_status_u8)
				   {
					   if(gt_i2c_read_data_blocking_u16(inst_type_are, rx_data_arpu8, data_len_rx_aru32, is_send_stop_rx_aru8))
					   {
						   return_status_u8++;
					   }
				   }

			   retry_times_u8--;
		     }while(return_status_u8 && retry_times_u8);
 		   xTaskResumeAll();

		}
		break;
		/*******************************************************************************************************************************/
		case I2C_APP_INSTANCE_FLEXIO:
		{
			//vTaskSuspendAll();
			do
			{
				return_status_u8 = 0;

				   if(!return_status_u8)
				   {
					   if(gt_i2c_set_slave_address_u16(inst_type_are,slave_addr_aru8,COM_HDR_FALSE))
					   {
						   return_status_u8++;
					   }
				   }

				   if(!return_status_u8)
				   {
				   	   if(gt_i2c_write_data_blocking_u16(inst_type_are,data_arpu8,data_len_tx_aru32,is_send_stop_tx_aru8))
				   	   {
					   	   return_status_u8++;
				   	   }
				   }

				   if(!return_status_u8)
				   {
					   if(gt_i2c_read_data_blocking_u16(inst_type_are, rx_data_arpu8, data_len_rx_aru32, is_send_stop_rx_aru8))
					   {
						   return_status_u8++;
					   }
				   }

			   retry_times_u8--;
		     }while(return_status_u8 && retry_times_u8);
			//xTaskResumeAll();

		}
		break;
		/*******************************************************************************************************************************/
		default :
		{

		}
		break;
		/*******************************************************************************************************************************/
	}
    return return_status_u8;
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
