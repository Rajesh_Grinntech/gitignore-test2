/*
 * LowPowerApp.c
 *
 *  Created on: 15-Oct-2019
 *      Author: Dell
 */

/** Common Header Files **/
#include "common_header.h"

/** Application Header File **/
#include "operating_system.h"
#include "afe_driver.h"
#include "gpio_app.h"
//#include "ntc.h"
#include "switch_ctrl.h"
#include "batt_param.h"

//#include "debug_comm.h"
#include "cell_bal_app.h"
#include "w25q.h"
#include "flash_drv.h"
#include "bms_afe_task.h"
#include "bms_op_cmd_task.h"
#include "debug_comm.h"
#include "rtc_int.h"
#include "bq76952.h"
/** This header file **/
#include "low_pwr_app.h"

/** Driver header **/
#include "gt_nvic.h"
#include "gt_rtc.h"
#include "gt_wdg.h"
#include "can_app.h"

#include "gt_can.h"
#include "gt_timer.h"
#include "gt_i2c.h"
#include "gt_spi.h"
#include "gt_adc.h"
#include "gt_gpio.h"
#include "gt_clock_pwr.h"


/** Private Definitions **/
#define VLPS  (0u) /* Very low power stop */
#define RUN   (1u) /* Run                 */
#define VLPR  (2u) /* Very Low Power Run  */

#define DportNVIC_SYSTICK_CTRL			(( volatile uint32_t * ) 0xe000e010 )
#define DportNVIC_SYSTICK_CLK			0x00000004
#define DportNVIC_SYSTICK_INT			0x00000002
#define DportNVIC_SYSTICK_ENABLE		0x00000001

/** Type Definitions **/

//flexcan_pn_config_t flexcan_pn_config_st;

/** Private Variable Definitions **/
can_app_message_tst lp_app_can_tx_buf_st;

/** Private Function Prototypes **/
U8 low_pwr_app_check_task_rdy_u8(void);
#if 0
static inline void low_pwr_app_set_pms_bias_v(PMC_Type* const baseAddr, BOOL enable_b);
#endif
void low_pwr_app_change_sleep_flg_v(BOOL enable_arg_b);
/** Public Variable Definitions **/
lpapp_mode_status_tst lpapp_mode_status_gst =
						{.enable_low_power_b = COM_HDR_FALSE,
						 .enable_resleep_rtc_b = COM_HDR_FALSE,
						 .woke_up_b = COM_HDR_FALSE,
						 .LPAPP_current_mode_e = SLEEP_NOT_INIT,
						 .LPAPP_current_requesting_resource_e = SLEEP_NOT_INIT,
						 .LPAPP_previous_mode_e = SLEEP_NOT_INIT};

volatile U8 lpapp_task_states_agu8[TASK_MAX_NUM] = {0,};

#if 0 /* Under Voltage Simulation*/
U32 sleep_mode_check_counter_u32 = 0;
U32 sleep_check_flag_b = COM_HDR_FALSE;
#endif

/** Public Function Definitions **/
U8 low_pwr_can_mess_flg_u8 = 0;
U8 low_pwr_app_enter_low_pwr_mode_u8(void);
void low_pwr_app_wakeup_cantx_v(void);
void low_pwr_app_change_act_v(lpapp_different_requester_te req_arg_e, lpapp_different_requester_te prev_arg_e);
void low_pwr_app_sm_v(void);
void low_pwr_mode_check_v(void);
/*
========================================================================================================================
Function Name  :
------------------------------------------------------------------------------------------------------------------------
Scope          :
------------------------------------------------------------------------------------------------------------------------
Description    :
------------------------------------------------------------------------------------------------------------------------
Input Data     :
------------------------------------------------------------------------------------------------------------------------
Return Value   :
========================================================================================================================
*/

void low_pwr_app_sm_v(void)
{
	low_pwr_mode_check_v();

#if 0
	sleep_mode_check_counter_u32++;
	if(sleep_mode_check_counter_u32 > 100)
	{
		sleep_mode_check_counter_u32 = 100;
		sleep_check_flag_b = COM_HDR_TRUE;
	}
#endif
#if 0
	/* Check for PUV or CUV */
		if(//batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE ||
			//batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
				batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
				//|| sleep_check_flag_b
			)
		//	if(1)
		{
			if(batt_param_pack_param_gst.pack_param_batt_mode_e != BATT_MODE_CHG)
			{
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CUV;
			}
			else
			{
				if(lpapp_mode_status_gst.LPAPP_current_mode_e == SLEEP_ENABLE_CUV)
				{
					lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_NORMAL_AFTER_CUV;
				}
			}
		}
		else if(!lpapp_mode_status_gst.enable_resleep_rtc_b)
		{
			if(lpapp_mode_status_gst.LPAPP_current_mode_e == SLEEP_ENABLE_CUV)
			{
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_NORMAL_AFTER_CUV;
			}
		}
#endif

	/* Change current mode*/
	if(lpapp_mode_status_gst.LPAPP_current_mode_e != lpapp_mode_status_gst.LPAPP_current_requesting_resource_e)
	{
		lpapp_mode_status_gst.LPAPP_previous_mode_e = lpapp_mode_status_gst.LPAPP_current_mode_e;

		/* */
		if(lpapp_mode_status_gst.LPAPP_current_requesting_resource_e == SLEEP_DISABLE_NORMAL_AFTER_CUV &&
				lpapp_mode_status_gst.LPAPP_previous_mode_e == SLEEP_ENABLE_CAN)
		{
			lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CAN;
		}

		lpapp_mode_status_gst.LPAPP_current_mode_e = lpapp_mode_status_gst.LPAPP_current_requesting_resource_e;

		low_pwr_app_change_act_v(lpapp_mode_status_gst.LPAPP_current_mode_e, lpapp_mode_status_gst.LPAPP_previous_mode_e);
	}

	/* Sleep Mode Call Function */
	if(lpapp_mode_status_gst.enable_low_power_b)
	{
		low_pwr_app_enter_low_pwr_mode_u8();
	}
	else
	{
		if(lpapp_mode_status_gst.enable_resleep_rtc_b)
		{
			if(gt_timer_get_tick_u32() > lpapp_mode_status_gst.goto_sleep_timer_u32)
			{
				lpapp_mode_status_gst.enable_low_power_b = COM_HDR_TRUE;
				lpapp_mode_status_gst.enable_resleep_rtc_b = COM_HDR_FALSE;
			}
			else
			{
				/*if(low_pwr_can_mess_flg_u8 == COM_HDR_TRUE)
				{
					lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_CAN;
				}*/
				if((batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
						|| (batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG))
				{
					lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_CAN;
				}
			}
		}
	}
}

U8 low_pwr_app_enter_low_pwr_mode_u8(void)
{
	U8 ret_val_u8 = 0;
	if (low_pwr_app_check_task_rdy_u8())
	{
		vTaskSuspendAll();
		wdg_refresh_counter_v();

		HAL_PWR_EnableBkUpAccess();
		WRITE_REG(RTC->BKP29R, SLEEP_MODE_RUNNING);

		/*#1 Before Entering Low Power Mode */
		/*(a) Save Power by turning off other resources */

		/* LED turn off */
		//F_LED_DISABLE;

		/* NTC turn off */
 		//NTC_ADC_FET_DISABLE; TODO Uncomment in real board

		/* Put Flash in Low Power Mode */
		ret_val_u8 += flash_drv_enable_sleep_u8(); //TODO Uncomment in real board

		//FLASH->OPTR &= ~(FLASH_OPTR_IWDG_STDBY);

 		/* AFE FET OFF*/
 		//ret_val_u8 += afe_driver_fet_control_u8(AFE_ALL_FET, AFE_FET_OFF);
//		switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
//		switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

		ret_val_u8 += bq76952_get_ic_stat_u16(FET_STS);
		//HAL_Delay(10000);


		/* Turn off the AFE- Cell balancing driver */
		cell_bal_config_status_gst.bal_enabled_b = COM_HDR_FALSE;
		if (cell_bal_app_fet_cntrl_u8())
		{
			ret_val_u8 += 1;
		}


		/* Put AFE in Low Power Mode */
		nvic_enable_interrupt_v(EXTI0_IRQn);
		bq76952_sleep_int_cnf();
//		bq76952_sleep_int_cnf();
		ret_val_u8 += bq76952_get_ic_stat_u16(BATT_STS);
		ret_val_u8 += afe_driver_enter_sleep_mode_u8();

		ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);

		//HAL_Delay(10000);
		ret_val_u8 += bq76952_get_ic_stat_u16(CTRL_STS);

#if 1
		/* Clear the fault status to avoid wakeup
		if (ucAfeDriver_Clear_Fault_Flags())
		{
			ret_val_u8 += 1;
		}
		if (ucAfeDriver_Read_Fault_WakeUp_Register())
		{
			ret_val_u8 += 1;
		}
		if (afe_driver_enter_sleep_mode_v())
		{
			ret_val_u8 += 1;
		}*/

		/*(b) Disable the peripherals
		 * 	  CAN is must because it causes system reset with "RCM_STOP_MODE_ACK_ERR"
		 * 	  WDG is must because it causes system reset with "RCM_WATCH_DOG" */
		/*if(can_app_deinit_u8(CAN_APP_DISO_INST_0))
		{
			ret_val_u8 += 1;
		}*/
		/* CAN_IC turn off */
		//CAN_APP_PWR_SW_DISABLE; TODO Uncomment in real board

/*		if (ucWdg_Deinit())
		{
			ret_val_u8 += 1;
		}*/

		/*All used ADC De-Init*/
		if(gt_adc_deinit_u8(ADC_0_INSTANCE))
		{
			ret_val_u8 += 1;
		}

		/* All used SPI De-Init*/
		if(gt_spi_master_deinit_u8(SPI_0_INSTANCE))
		{
			ret_val_u8 += 1;
		}

		/* All used I2C De-Init*/
		if(gt_i2c_deinit_u16(I2C_INSTANCE))
		{
			ret_val_u8 += 1;
		}

		//rtc_deinit_u8();

		//vGpio_PinMux_Deinit();

		/*(c) Disable all Interrupt */
		nvic_disable_all_interrupts_v();

		low_pwr_can_mess_flg_u8 = COM_HDR_FALSE;

		/* Disable Systick */
		HAL_SuspendTick();
		SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;

		/* Enable PMC Bias Mode for VLP */
		//low_pwr_app_set_pms_bias_v(PMC, COM_HDR_TRUE);

		if(lpapp_mode_status_gst.LPAPP_current_mode_e == SLEEP_ENABLE_CAN)
		{

//			nvic_enable_interrupt_v(CAN1_RX0_IRQn);

#if 0
			flexcan_pn_config_st.filterComb = FLEXCAN_FILTER_ID;
			flexcan_pn_config_st.idFilterType = FLEXCAN_FILTER_MATCH_EXACT;
			flexcan_pn_config_st.idFilter1.extendedId = 1;
			flexcan_pn_config_st.idFilter1.id = DEBUG_COMM_SLEEP_WAKE_CMD;
			flexcan_pn_config_st.wakeUpMatch = COM_HDR_TRUE;
			flexcan_pn_config_st.numMatches = 1;
			//		flexcan_pn_config_st.payloadFilter.dlcLow = 1;
			//		flexcan_pn_config_st.payloadFilter.payload1[0] = 0xFF;

			FLEXCAN_DRV_ConfigPN(CAN_APP_DISO_INST_0,COM_HDR_TRUE, &flexcan_pn_config_st);
#endif
			/*#2 Enter Standby Mode  */
/*			while(1)
			{
				static U32 counter = 0;
				counter++;
				if(counter >= 0xFFFFF)
				{
					bq_slp_chck();
					counter = 0;
				}
			}*/
			HAL_PWR_EnterSTANDBYMode();
//			HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, uint8_t SLEEPEntry);
			/* Alerts Configuration */
			/* Configuring Alert Pin for Alert and interrupt */
#if 1

#endif
		}

		xTaskResumeAll();

		if(lpapp_mode_status_gst.LPAPP_current_mode_e == SLEEP_ENABLE_CUV)
		{
			/*#1 PreChecks to Set RTC Wakeup interrupt */
			__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

			if(rtc_int_wakeup_timer_deinit_u8())
			{
				HAL_Delay(1);
			}

			__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);


			/*#2 Enable only the Wakeup RTC ISR */
			nvic_enable_interrupt_v(RTC_WKUP_IRQn);
#if 1
			/*#3 Set RTC Wakeup interrupt */
			ret_val_u8 = rtc_int_set_wakeup_timer_u8();

#endif
			/*#4 Enter Standby Mode  */
			rtc_wakeup_flag_b = 1;
			 HAL_PWR_EnterSTANDBYMode();
		}

		 //  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON,PWR_STOPENTRY_WFI); //5.73
		 // HAL_PWR_EnterSTANDBYMode(); //4.75
		 //  HAL_PWREx_EnterSTOP0Mode(PWR_STOPENTRY_WFI);//5.73
		 //  HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFI);//5.71
		 //  HAL_PWREx_EnterSTOP2Mode (PWR_STOPENTRY_WFI);//5.72
		 //  HAL_PWREx_EnterSHUTDOWNMode();//4.77

		/*#3 After Wake-Up from Sleep   (except for Shutdown/Standby - Code doesnt reach here for these modes) */
#if 0
		/*(a) Change the power mode */
		if (POWER_SYS_GetCurrentMode() == POWER_MANAGER_RUN)
		{
			/* Proper wake-up */
			POWER_SYS_SetMode(RUN, POWER_MANAGER_POLICY_FORCIBLE);
		}
		else
		{
			/* Though wrong wake-up move to RUN mode */
			POWER_SYS_SetMode(RUN, POWER_MANAGER_POLICY_FORCIBLE);
		}
#endif
	    /*Enable systick*/
		SysTick->CTRL  |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk |SysTick_CTRL_TICKINT_Msk;
		HAL_ResumeTick();

		/* Configure the system clock with required specifications */
		ret_val_u8 = clock_pwr_system_clock_config_u8();
		/*(b) Re-Init the drivers */
/*		if(ucGpio_PinMux_Init())
		{
			ret_val_u8 += 1;
		}*/

		if(gt_adc_init_u8(ADC_0_INSTANCE))
		{
			ret_val_u8 += 1;
		}

		if(gt_spi_master_init_u8(SPI_0_INSTANCE))
		{
			ret_val_u8 += 1;
		}

		if(gt_i2c_init_u16(I2C_INSTANCE))
		{
			ret_val_u8 += 1;
		}

		if(can_app_init_u8(CAN_APP_DISO_INST_0))
		{
			ret_val_u8 += 1;
		}
		/* CAN_IC turn on */
		//CAN_APP_PWR_SW_ENABLE; TODO Uncomment in real board

		//ret_val_u8 += can_app_receive_data_u8(CAN_APP_DISO_INST_0, CAN_RX_MAILBOX0);

		/*(c) Turn-ON all the devices */
		/* Turn ON LED */
		//gpio_app_set_digital_output_v(UC_B_LED_PTD17,GPIO_DO_ON);

		/* Reset Ideal sleep mode feature */
		bms_tsk_sleep_flg_u8 = COM_HDR_TRUE;
		bms_tsk_sleep_counter_u32 = 0;

		/* Turn ON NTC */
		//NTC_ADC_FET_ENABLE; TODO Uncomment in real board
		//FLASH_DRV_PSW_ENABLE;

//		ret_val_u8 += ina226_init_u8();
//		ret_val_u8 += ina226_app_init_u8();

		//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
		switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
		//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
		//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
		/*(d) Enable all the Interrupts */
#if 0
		/* Systick */
		*(DportNVIC_SYSTICK_CTRL) = DportNVIC_SYSTICK_CLK | DportNVIC_SYSTICK_INT | DportNVIC_SYSTICK_ENABLE;
#endif
		nvic_enable_all_interrupts_v();

		/*(e) Wake-Up the ICs */
		/* Wake Up AFE */

//		if (afe_driver_fault_check_u8())
//		{
//			ret_val_u8 += 1;
//		}

		/*(f) Set goto sleep resource Timer (except for Shutdown/Standby) */
		lpapp_mode_status_gst.goto_sleep_timer_u32 = gt_timer_get_tick_u32() + LPAPP_GOTO_SLEEP_AFTER_1MS;//
		lpapp_mode_status_gst.enable_resleep_rtc_b = COM_HDR_TRUE;
		lpapp_mode_status_gst.enable_low_power_b = COM_HDR_FALSE;
//		debug_comm_auth_success_b = COM_HDR_FALSE;
	}
#endif
	return 0;
}

void low_pwr_mode_check_v(void)
{
	HAL_PWR_EnableBkUpAccess();
	 if(READ_REG(RTC->BKP29R) == SLEEP_MODE_RUNNING)
	 {
		 WRITE_REG(RTC->BKP29R,0x00);
		 lpapp_mode_status_gst.goto_sleep_timer_u32 = gt_timer_get_tick_u32() + LPAPP_GOTO_SLEEP_AFTER_1MS;
		 lpapp_mode_status_gst.enable_resleep_rtc_b = COM_HDR_TRUE;
		 lpapp_mode_status_gst.enable_low_power_b = COM_HDR_FALSE;

	//	 sleep_check_flag_b = COM_HDR_TRUE;

		 lpapp_mode_status_gst.LPAPP_current_mode_e = SLEEP_ENABLE_CUV;
		 lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CUV;
	 }
}

U8 low_pwr_app_check_task_rdy_u8(void)
{
	if(lpapp_task_states_agu8[TASK_BMS_AFE] &&
	   lpapp_task_states_agu8[TASK_BMS_PROT] &&
	   lpapp_task_states_agu8[TASK_FET_CTRL] &&
	   lpapp_task_states_agu8[TASK_COMM_CAN] &&
	   lpapp_task_states_agu8[TASK_STORAGE] &&
	   lpapp_task_states_agu8[TASK_CURR_SOC_EST])
	{
		return (COM_HDR_TRUE);
	}
	else
	{
		return (COM_HDR_FALSE);
	}
}

#if 0
void low_pwr_app_wakeup_cantx_v(void)
{
	U8 state_u8 = 0;
	if(lpapp_mode_status_gst.enable_resleep_rtc_b == COM_HDR_TRUE)
	{
		lp_app_can_tx_buf_st.can_id_u32 = LPAPP_SLEEP_CAN_ID;
		lp_app_can_tx_buf_st.data_au8[0] = 0xBB; /* Wake from RTC and will go back to sleep */
		lp_app_can_tx_buf_st.length_u8 = 1;
//		state_u8 += ucCan_Transmit_Data(TX_MAILBOX0, CAN_TX_BLOCKING, &lp_app_can_tx_buf_st);
	}

	if(lpapp_mode_status_gst.woke_up_b)
	{
		lp_app_can_tx_buf_st.can_id_u32 = LPAPP_SLEEP_CAN_ID;
		lp_app_can_tx_buf_st.data_au8[0] = 0xCC; /* Wake from sleep completely */
//		state_u8 += ucCan_Transmit_Data(TX_MAILBOX0, CAN_TX_BLOCKING, &lp_app_can_tx_buf_st);
		lpapp_mode_status_gst.woke_up_b = COM_HDR_FALSE;
	}
}
#endif

#if 0
static inline void low_pwr_app_set_pms_bias_v(PMC_Type* const baseAddr, BOOL enable_b)
{
	U8 regValue;

    regValue = baseAddr->REGSC;
    regValue &= (uint8_t)(~(PMC_REGSC_BIASEN_MASK));
    regValue |= (uint8_t)PMC_REGSC_BIASEN(enable_b);
    baseAddr->REGSC = regValue;
}
#endif

void low_pwr_app_change_act_v(lpapp_different_requester_te req_arg_e, lpapp_different_requester_te prev_arg_e)
{
	if(req_arg_e == SLEEP_ENABLE_CAN || req_arg_e == SLEEP_ENABLE_CUV)
	{
		low_pwr_app_change_sleep_flg_v(COM_HDR_TRUE);
	}
	else
	{
		low_pwr_app_change_sleep_flg_v(COM_HDR_FALSE);
	}
}

void low_pwr_app_change_sleep_flg_v(BOOL enable_arg_b)
{
	if(enable_arg_b)
	{
		lpapp_mode_status_gst.enable_low_power_b = COM_HDR_TRUE;
	}
	else
	{
		if (lpapp_mode_status_gst.enable_resleep_rtc_b)
		{
			lpapp_mode_status_gst.woke_up_b = COM_HDR_TRUE;
		}
		lpapp_mode_status_gst.enable_low_power_b = COM_HDR_FALSE;
		lpapp_mode_status_gst.enable_resleep_rtc_b = COM_HDR_FALSE;
	}
}
