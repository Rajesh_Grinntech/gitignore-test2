/**
========================================================================================================================
 @page app_main_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	app_main.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	24-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include "batt_param.h"
//#include "cell_bal_app.h"
#include "bms_config.h"
#include "meas_app.h"
//#include "batt_estimt.h"
//#include "storage_task.h"
#include "bms_soc_estimation_task.h"

/* Configuration Header File */
#include "afe_driver.h"
#include "flash_drv.h"
#include "bms_config.h"
#include "ai_app.h"
#include "spi_app.h"
#include "i2c_app.h"
#include "can_app.h"
#include "adc_app.h"
#include "bootsm.h"
#include "rtc_int.h"

#include "bq76952.h"
#include "switch_ctrl.h"
//#include "gpio_app.h"
#include "task.h"
//#include "estimate.h"
//#include "gt_rtc.h"
//#include "curr_calib.h"
/* This header file */
#include "app_main.h"

/* Driver header */
#include "gt_gpio.h"
#include "gt_timer.h"
//#include "gt_adc.h"
#include "gt_can.h"
#include "gt_i2c.h"
#include "gt_nvic.h"
//#include "gt_spi.h"
#include "gt_system.h"
#include "gt_wdg.h"
/** @{
 * Private Definitions **/

/** @} */
/* Private Function Prototypes */
static U8 app_main_peripherals_init_pru8(void);
static U8 app_main_pre_os_periph_config_pru8(void);
void app_main_sensor_on_v(void);
void app_main_sensor_off_v(void);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 app_main_start_u8(void)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 =  app_main_peripherals_init_pru8();
	ret_val_u8 += app_main_pre_os_periph_config_pru8();

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static U8 app_main_peripherals_init_pru8(void)
{
	U8 ret_val_u8 = 0;

	bootsm_boot_status_e = BOOTSM_SYS_APP_PERP_INIT;

	/* Pin Mux Initialization - Done based on the pins used in the schematics */
	ret_val_u8 = gt_gpio_pin_mux_init_u8(); /* No core state change - because no fault case*/

	/* CAN Engine Initialization - CAN1 at 500Kbps */
	bootsm_init_state_e = BOOTSM_APPCORE_CAN_STATE;
//	CAN_STB_ENABLE;
	CAN_STB_DISABLE;
	ret_val_u8 += can_app_init_u8(CAN_APP_DISO_INST_0);

	/* ADC Engine Initialization - Both ADCs are configured to 12 bit resolution */
	bootsm_init_state_e = BOOTSM_APPCORE_ADC_STATE;
	ret_val_u8 += adc_app_init_adc_u8(ADC_APP_0_INSTANCE);

	/**************** Communication APP Init *********************/
	/* i2c_app init */
	bootsm_init_state_e = BOOTSM_APPCORE_I2C_STATE;
	ret_val_u8 += i2c_app_init_u8(I2C_APP_INSTANCE);

    /* spi_app init */
	bootsm_init_state_e = BOOTSM_APPCORE_SPI_STATE;
	ret_val_u8 += spi_app_master_init_u8(SPI_APP_FLASH);

	/* UART Initialization */

	/* TIMER Initialization  */
	/* Core init states are assigned in core layer - TIM6, TIM7, TIM15*/
	gt_timer_init_v();

	bootsm_init_state_e = BOOTSM_APPCORE_STATE_ENDS;

	/* NVIC Initialization - Disable all the interrupts and set the appropriate priority */
	nvic_init_v();

	return (ret_val_u8);
}

U8 system_init_flt_update_u8()
{
	U8 ret_val_u8 = 0;
	switch(bootsm_init_state_e)
	{
		case BOOTSM_APPCORE_CLOCK_STATE:
		{
			ret_val_u8 = gt_gpio_pin_mux_init_u8();
			ret_val_u8 += can_app_init_u8(CAN_APP_DISO_INST_0);
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CLOCK_INIT);
		}
		break;
		case BOOTSM_APPCORE_ADC_STATE:
		{
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_ADC_INIT);
		}
		break;
		case BOOTSM_APPCORE_I2C_STATE:
		{
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_I2C_INIT);
		}
		break;
		case BOOTSM_APPCORE_SPI_STATE:
		{
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_SPI_INIT);
		}
		break;
		case BOOTSM_APPCORE_TIM6_STATE:
		{
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIMER6);
		}
		break;
		case BOOTSM_APPCORE_TIM7_STATE:
		{
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIMER7);
		}
		break;
		case BOOTSM_APPCORE_TIM15_STATE:
		{
			app_main_system_flt_status_u64 = 0;
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIMER15);
		}
		break;
		case BOOTSM_CORE_STATE_ENDS:
		{
			app_main_system_flt_status_u64 &= ~((U64)0x1FFF);
		}
		break;
		default:
		{

		}
	}

	if(bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
	{
		HAL_Delay(1000);
	}
	can_app_message_tst can_msg_tx_st;
	can_msg_tx_st.can_id_u32 = 0x81DFADA;
	can_msg_tx_st.length_u8 = 8;
	can_msg_tx_st.data_au8[0] = (app_main_system_flt_status_u64);
	can_msg_tx_st.data_au8[1] = (app_main_system_flt_status_u64 >> 8);
	can_msg_tx_st.data_au8[2] = (app_main_system_flt_status_u64 >> 16);
	can_msg_tx_st.data_au8[3] = (app_main_system_flt_status_u64 >> 24);
	can_msg_tx_st.data_au8[4] = (app_main_system_flt_status_u64 >> 32);
	can_msg_tx_st.data_au8[5] = (app_main_system_flt_status_u64 >> 40);
	can_msg_tx_st.data_au8[6] = (app_main_system_flt_status_u64 >> 48);
	can_msg_tx_st.data_au8[7] = (app_main_system_flt_status_u64 >> 56);
	ret_val_u8  += can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &can_msg_tx_st);

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 sys_runtime_flt_u8(void)
{

}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static U8 app_main_pre_os_periph_config_pru8(void)
{
	U8 ret_val_u8 = 0;
	/* TODO: Enable this once boot state machine file is included */
	bootsm_boot_status_e = BOOTSM_SYS_APP_PREOS_CONFIGURATION;

#if 0	/* Disabled because power for all I2C sensors should be enabled from GUI */
	#if !TEST_COMM_HARD_CODE
	/** Powering all the sensor after core initialisation */
	app_main_sensor_on_v();
	#endif
#endif
	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

=====================================================================================================================================================
*/
void app_main_sensor_on_v()
{

}

/**
=====================================================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

=====================================================================================================================================================
*/
void app_main_sensor_off_v()
{

}
/**
=====================================================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

=====================================================================================================================================================
*/
/* All generalized peripheral are trigger here */
U8 app_main_post_os_periph_config_u8(void)
{
	U8 ret_val_u8 = 0;

	bootsm_boot_status_e = BOOTSM_SYS_APP_POSTOS_CONFIGURATION;

	/***************************************************************************************************************************************/
	/* #1 Gpio Initialization */
	/***************************************************************************************************************************************/
	/* GPIO Configuration for Default States */

	/** Powering all the sensor after core initialization */

	/***************************************************************************************************************************************/
	/* #2 NVIC interrupts enabling */
	/***************************************************************************************************************************************/
	/* NVIC - PostOS Configuration */
	nvic_enable_all_interrupts_v();

	/***************************************************************************************************************************************/
	/* #3 IC checks */
	/***************************************************************************************************************************************/
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/* @AFE section */
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/* AFE - LTC */
	ret_val_u8 += afe_driver_init_u8();
#if 0
	while(1)
	{
		switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
		switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

		ret_val_u8 += bq76952_get_ic_stat_u16(FET_STS);
		ret_val_u8 += bq76952_get_ic_stat_u16(ALRM_STS);
		batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
				| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
				| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
				| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));

		HAL_Delay(5000);
	}
#endif

	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/* @External Flash section 	 */
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/* Enabling External Flash rail switch */

	/* Initialize External Flash  */
	ret_val_u8 += flash_drv_init_u8();



	ret_val_u8 += engy_tp_flash_init_u8();

	/* Flash - W25Q */

	/* Timer - Start Base */
	gt_timer_start_base_v();



/*	flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR,0x00,500);
	arr[10] = {1,56,34,85,21,56,23,98,23,67};
	arr_r[10] = {0};
		flash_drv_writebyte_u8(arr, 0x20, 256);
		flash_drv_readbyte_u8(arr_r, 0x20, 256);*/

	/* RTC- External */

	/*RTC - Internal Initialization*/
	/* Added in POST-OS because this initializes the counter too*/
	ret_val_u8 += rtc_int_init_u8();

	/*Watchdog Initialization*/
//	ret_val_u8 += wdg_init_u8();

	/********* NV Parameters Configuration ***********/
	/* BATT Parameters */
	//batt_param_var_init_v();
	bms_config_non_volatile_param_init_v();

	batt_param_init_batt_details_v(); /* for bootloader */
	#if 0 //Commented by krish - 23092020
	bms_config_non_volatile_backup_reg_recovery_v();
	batt_estim_init_param_v();

	/* CELL BAL*/
	cell_bal_app_param_config_v();

	/* Read from external flash */
	batt_param_init_batt_details_v();
	batt_estim_recover_batt_estimation_v();

	/*TODO LED - StateMachine Start */
	#endif

	//ret_val_u8 += ntc_adc_init_u8();

	//ai_app_init_ai_u8();
	#if 0
	 /*RTC - Start*/
	ret_val_u8 += rtc_ext_start_u8(RTC_EXT_MCP7940M);
	#endif

	/* Moisture sensor init */

	/* IMU sensor init */

	/* Ambient light sensor init */

	/* Ext.Current sensor init */

	/*external adc init */


	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/* @CAN section */
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

	/* Enabling CAN rail switch */
	//CAN_APP_PWR_SW_ENABLE; TODO Uncomment in real board

	 /**** Can Receive interrupt Enable  *****/

	//ret_val_u8 += storage_task_start_addr_init_u8();

	//curr_calib_init_v();

	/**** SOC and SOP inclusion ****/
	/** as of now included falcon values will change according to finch48 inputs **/
	//soc_sop_cell_param_init_v(0.95, 0.99,27, 0.05,47,0.001, 0.00105,16000);

	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/* @UART section */
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

	/* UART receive interrupt enable */

	return (ret_val_u8);
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
