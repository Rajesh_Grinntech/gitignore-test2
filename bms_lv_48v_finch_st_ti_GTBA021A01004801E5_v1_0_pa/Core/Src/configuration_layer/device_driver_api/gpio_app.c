/**
========================================================================================================================
@page gpio_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	gpio_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	24-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */

/* Application Header File */

/* This header file */
#include "gpio_app.h"

/* Driver header */
#include "gt_gpio.h"
#include "app_main.h"
#include "gt_system.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
const gpio_do_map_dir_tst gpio_app_gpio_do_map_dir_gast[GPIO_MAX_NUM_DO]=
{
		/** GPIO_PORT    GPIO_PINS     DEFAULT_STATE **/
		{ GPIO_PORT_PTA, GPIO_PINS_1,  GPIO_DO_OFF },   //0
//		{ GPIO_PORT_PTA, GPIO_PINS_3,  GPIO_DO_OFF },   //1
//		{ GPIO_PORT_PTA, GPIO_PINS_4,  GPIO_DO_OFF },  //2
		{ GPIO_PORT_PTA, GPIO_PINS_8,  GPIO_DO_OFF },  //2

		{ GPIO_PORT_PTB, GPIO_PINS_0,  GPIO_DO_OFF },
//		{ GPIO_PORT_PTB, GPIO_PINS_1,  GPIO_DO_OFF },
		{ GPIO_PORT_PTB, GPIO_PINS_4,  GPIO_DO_OFF },
		{ GPIO_PORT_PTB, GPIO_PINS_5,  GPIO_DO_OFF },

};

const gpio_do_map_dir_tst gpio_app_gpio_di_map_dir_gast[DI_GPIO_MAX_NUM_DI]=
{
		{ GPIO_PORT_PTB, GPIO_PINS_5, GPIO_DO_OFF }

};

/** @} */

/* Public Function Definitions */

#if 0 /* This function not used*/
/**
========================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

========================================================================================================================
*/
U8 gpio_app_io_init_u8(void)
{
	U8 i_u8,ret_val_u8 =0;

	//ret_val_u8 = gpio_pin_mux_init_u8();

	if(ret_val_u8 == 0)
	{
		/* Configure the digital output pins */
		for(i_u8 = 0; i_u8 < GPIO_MAX_NUM_DO; i_u8++)
		{
			gpio_app_set_digital_output_v(i_u8, gpio_app_gpio_do_map_dir_gast[i_u8].gpio_default_state_e);
		}

		/** configuring the interrupt to enabled to which port **/
		//gpio_digital_interrupt_init_v(GPIO_PORT_PTB);

		/* TODO: Configure the digital input pins */
	}
	else
	{
		//app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_GPIO_INIT);
	}
	return(ret_val_u8);
}
#endif

/**
========================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

========================================================================================================================
*/
void gpio_app_set_digital_output_v(const U16 pin_number_u16, gpio_do_di_state_te state_e)
{
	gt_gpio_set_direct_output_v(pin_number_u16, state_e, gpio_app_gpio_do_map_dir_gast);
}

/**
========================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

========================================================================================================================
*/
void gpio_app_toggle_direct_output_v(const U16 pin_number_u16)
{
	gt_gpio_toggle_direct_output_v(pin_number_u16, gpio_app_gpio_do_map_dir_gast);
}

/**
========================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

========================================================================================================================
*/
void gpio_app_read_update_digital_inputs_v(const U16 pin_number_u16, gpio_do_di_state_te *state_e)
{
	//TODO Read_Update_Digital_Inputs
	gt_gpio_get_direct_input_v(pin_number_u16, state_e, gpio_app_gpio_di_map_dir_gast);
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
