/**
====================================================================================================================================================================================
@page rtc_int_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	rtc_int.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	14-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */


/* Application Header File */
#include "gt_system.h"

/* This header file */
#include "rtc_int.h"

/* Driver header */


/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
U8 rtc_date_time_flag_u8 = 0;

rtc_int_time_date_tst rtc_time_date_gst;

/** @} */

/* Public Function Definitions */
U8 rtc_int_set_wakeup_timer_u8();
U8 rtc_int_wakeup_timer_deinit_u8();

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_init_u8()
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_init_u8();

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_start_u8()
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_start_base_u8();

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_stop_u8()
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_stop_base_u8();

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_set_time_date_u8(void *time_date_info_arpv)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_set_time_date_u8((rtc_timedate_tst *)time_date_info_arpv);

	return ret_val_u8;

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_get_time_date_u8(void *time_date_info_arpv)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_get_time_date_u8((rtc_timedate_tst *)time_date_info_arpv);

	if(ret_val_u8 != 0)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RTC_FAILED_RUN);
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_RTC_FAILED_RUN);
	}

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_set_alarm_u8(void *time_date_info_arpv, BOOL repeat_enable_arb)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_set_alarm_u8((rtc_timedate_tst*) time_date_info_arpv, 0); /*arg 2 - no use*/

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_set_wakeup_timer_u8()
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_set_wakeup_timer_u8();

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_wakeup_timer_deinit_u8()
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_rtc_wakeup_timer_deinit_u8();

	return ret_val_u8;
}


#if 0
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 rtc_int_incremnt_calendar_u8(void *time_date_info_arpv,U8 incr_sec_aru8)
{
	U8 ret_val_u8 = 0;
	U8 min_incr_flag_u8 = 0;
	U8 hr_incr_flag_u8 = 0;
	U8 date_incr_flag_u8 = 0;

	((rtc_timedate_tst*) time_date_info_arpv)->seconds_u8 = ((rtc_timedate_tst*) time_date_info_arpv)->seconds_u8

	U16 new_temp_seconds_u16 = ((rtc_timedate_tst*) time_date_info_arpv)->seconds_u8 + incr_sec_aru8;

	if( new_temp_seconds_u16 > 60)
	{
		((rtc_timedate_tst*) time_date_info_arpv)->seconds_u8 = (new_temp_seconds_u16 % 60);
		min_incr_flag_u8 = new_temp_seconds_u16 / 60;
	}

	if(min_incr_flag > 0)
	{
		U16 new_temp_mins_u16 = ((rtc_timedate_tst*) time_date_info_arpv)->minutes_u8 + min_incr_flag_u8;

		((rtc_timedate_tst*) time_date_info_arpv)->minutes_u8 = (new_temp_mins_u16 % 60);
		hr_incr_flag_u8 = new_temp_mins_u16 / 60;
	}

	if(hr_incr_flag_u8 > 0)
	{
		U16 new_temp_hr_u16 = ((rtc_timedate_tst*) time_date_info_arpv)->hours_u8 + hr_incr_flag_u8;

		((rtc_timedate_tst*) time_date_info_arpv)->minutes_u8 = (new_temp_hr_u16 % 24);
		date_incr_flag_u8 = new_temp_hr_u16 / 24;
	}

	if(date_incr_flag_u8 > 0)
	{
		U16 new_temp_date_u16 = ((rtc_timedate_tst*) time_date_info_arpv)->date_u8 + date_incr_flag_u8;

		((rtc_timedate_tst*) time_date_info_arpv)->minutes_u8 = (new_temp_hr_u16 % 24);
		date_incr_flag_u8 = new_temp_hr_u16 / 24;
	}

	return ret_val_u8;
}
#endif
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
