/**
========================================================================================================================
@page can_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	can_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	30-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "common_header.h"
/* Application Header File */
#include "can_commtask.h"
#include "gt_system.h"
#include "debug_comm.h"
#include "client_comm.h"
/* Configuration Header File */
#include "operating_system.h"

/* This header file */
#include "can_app.h"

/* Driver header */
#include "gt_can.h"
#include "gt_memory.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
U8 can_app_rx_msg_index_gu8 = 0;

can_app_message_tst can_app_rx_msg_index_agst[CAN_APP_RX_BUFFER_LEN];
can_app_message_tst can_app_last_tx_msg_st;

can_app_message_tst can_app_rx_message_st;

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_app_init_u8(can_app_device_te can_app_device_arg_te)
{
	U8 ret_val_u8 = 0;

	if( gt_can_init_u8(can_app_device_arg_te))
	{
		//app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CAN_INIT);
		ret_val_u8 += 1;
	}

	return ret_val_u8;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_app_frame_and_tx_msg_u8(U32 base_id_aru32,U32 extd_id_aru32,U32 rtr_aru32,U32 ide_aru32,BOOL srr_arb,U8 dlc_bytes_aru8,U8 *data_arpu8)
{
	U8 can_tx_status_u8 = 0;
	can_app_message_tst can_msg_tx_st;

	can_msg_tx_st.can_id_u32 = (base_id_aru32 <<18) | extd_id_aru32;
	can_msg_tx_st.length_u8 = dlc_bytes_aru8;
	memory_copy_u8_array_v(can_msg_tx_st.data_au8, (void*) data_arpu8, can_msg_tx_st.length_u8);

	can_app_last_tx_msg_st.length_u8 = can_msg_tx_st.length_u8;
	can_app_last_tx_msg_st.can_id_u32 = can_msg_tx_st.can_id_u32;
	memory_copy_u8_array_v(can_app_last_tx_msg_st.data_au8, (void*)can_msg_tx_st.data_au8, can_msg_tx_st.length_u8);

	/*STATUS_TIMEOUT is used to avoid CAN transmission is getting stopped due to can wire gets disconnected*/
	if(can_app_transmit_data_u8(CAN_APP_DISO_INST_0,  CAN_TX_MAILBOX0,CAN_TX_BLOCKING,&can_msg_tx_st) != SUCCESS)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CAN_TX);
	}
	else
	{
		can_tx_status_u8 = 1;
	}

	return can_tx_status_u8;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_app_transmit_data_u8(can_app_device_te can_app_device_are,U8 bank_num_aru8, can_tx_type_te type_are, can_app_message_tst *msg_arpst)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = gt_can_transmit_data_u8(can_app_device_are,bank_num_aru8,type_are,(can_message_tst*)msg_arpst);

	if(ret_val_u8)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CAN_TX);
	}
	return ret_val_u8;

}

#if 0
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_app_receive_data_u8(can_app_device_te can_app_device_are, U8 bank_num_aru8)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = can_receive_data_u8(can_app_device_are,bank_num_aru8);
	if(ret_val_u8)
	{
		app_main_system_flt_status_u64 |= (app_main_system_flt_status_u64 << FAULT_BIT_CAN_RX_CONFIG);
	}
	return ret_val_u8;
}
#endif
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void can_app_0_rx_callback()
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	operating_system_can_comm_queue_tst os_can_rx_queue_st;
	can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].length_u8 = can_app_rx_message_st.length_u8;
	can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].can_id_u32 = can_app_rx_message_st.can_id_u32;
	memory_copy_u8_array_v(can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].data_au8, can_app_rx_message_st.data_au8,
			can_app_rx_message_st.length_u8);


	if(COMMON_VAR_DEBUG_SM == can_commtask_identify_comm_type_u8(can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].can_id_u32))
	{
		if(can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].can_id_u32  == CLIENT_COMM_START_STOP_RES)
		{
			client_gui_u32 = 1;
			debug_gui_u8 = 0;
		}
		if((can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].can_id_u32  == DBEUG_COMM_START_STOP_RES)
			|| (can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8].can_id_u32  == 0x888DAFA))
		{
			client_gui_u32 = 0;
			debug_gui_u8 = 1;
		}
		if(client_gui_u32)
		{
			os_can_rx_queue_st.comm_event_e = OS_CAN_CLIENT_RX;
		}
		if(debug_gui_u8)
		{
			os_can_rx_queue_st.comm_event_e = OS_CAN_DEBUG_RX;
		}
	}
	/* TODO: Split the below else condition for Charging & driving to send the queue with different event	 */
	else
	{
		os_can_rx_queue_st.comm_event_e = OS_CAN_RX;
	}
	os_can_rx_queue_st.can_rx_msg_pv = &can_app_rx_msg_index_agst[can_app_rx_msg_index_gu8];

	if (can_app_rx_msg_index_gu8 < (CAN_APP_RX_BUFFER_LEN-1)) /* Note: Don't remove -1 due to some memory illegal overwrite problem */
	{
		can_app_rx_msg_index_gu8++;
	}
	else
	{
		can_app_rx_msg_index_gu8 = 0;
	}

	xQueueSendFromISR(os_can_comm_queue_handler_ge, &os_can_rx_queue_st, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_app_deinit_u8(can_app_device_te can_app_device_are)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = can_deinit_u8(can_app_device_are);

	return ret_val_u8;
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
