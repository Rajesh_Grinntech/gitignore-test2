/**
====================================================================================================================================================================================
@page ai_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	ai_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	21-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"
#include "app_main.h"
#include "gt_system.h"
/* Application Header File */

/* Configuration Header File */
#include "adc_app.h"

/* This header file */
#include "ai_app.h"

/* Driver Header File */
#include "gt_adc.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
static void ai_app_init_v(void);
/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

ai_app_status_tst    ai_app_status_gast[AI_APP_NUM_AI];

/* Public Function Definitions */
U8 ai_app_init_u8(void);
U8 ai_app_read_ai_u8(void);

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 ai_app_init_ai_u8(void)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 = adc_app_init_adc_u8(ADC_APP_0_INSTANCE);
	ai_app_init_v();
	ret_val_u8 += adc_app_start_conversion_u8();
	if(ret_val_u8)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_ADC_INIT);
	}
	return ret_val_u8;

}


/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 ai_app_read_ai_u8(void)
{
	U8 ret_val_u8 = 0;

	ret_val_u8 += adc_app_read_ai_u8();

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void ai_app_init_v(void)
{
	U8 chan_u8;

	for (chan_u8 = 0; chan_u8 < AI_APP_NUM_AI; chan_u8++)
	{

			ai_app_status_gast[chan_u8].state_e = AI_INIT;
			ai_app_status_gast[chan_u8].raw_u16 = 0;
			ai_app_status_gast[chan_u8].scaled_s32 = ADC_INIT_S32;
			ai_app_status_gast[chan_u8].scaled_u32 = ADC_INIT_U32;

	}
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
