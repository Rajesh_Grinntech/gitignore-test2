/**
====================================================================================================================================================================================
@page operating_system_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	operating_system.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	Gladson

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	09-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "bms_afe_task.h"
#include "bms_soc_estimation_task.h"
#include "bms_protection_task.h"
#include "bms_mfet_ctrl_task.h"
#include "can_commtask.h"
#include "storage_task.h"
#include "diagmonitor_task.h"

#include "operating_system.h"
#include "common_header.h"
#include "gt_system.h"

/* Application Header File */
#include "task.h"
#include "queue.h"
#include "semphr.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
static U8 operating_system_create_queue_pru8(void);
static U8 operating_system_create_semaphore_pru8(void);
static U8 operating_system_create_task_pru8(void);
static U8 operating_system_create_mutex_pru8(void);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
QueueHandle_t os_bms_afe_queue_handler_ge   = NULL;
QueueHandle_t os_can_comm_queue_handler_ge = NULL;
QueueHandle_t os_storage_t7_qhandler_ge     = NULL;
QueueHandle_t os_curr_soc_queue_handler_ge  = NULL;

SemaphoreHandle_t os_battparm_bmutex_ge = NULL;
SemaphoreHandle_t OS_afe_spi_bsmpr_ge   = NULL;

TaskHandle_t os_diagmon_handler_ge   = NULL;
TaskHandle_t os_bmstask1_handler_ge  = NULL;
TaskHandle_t os_bmsprotect_handler_ge = NULL;
TaskHandle_t os_mfet_ctrl_handler_ge = NULL;
TaskHandle_t os_can_comm_handler_ge = NULL;
TaskHandle_t os_bms_storage_t7_handler_ge   = NULL;
TaskHandle_t os_curr_soc_handler_ge = NULL;


//TODO : Do a stack memory analysis to optimize the RAM space

#define OS_DIAGMONTASK_STACK_SIZE		(768U) /* 256 words = 1024B - 744 Used */
#define OS_BMSTASK1_STACK_SIZE		    (1024U) /* 192 words = 768B  - 560 Used */
#define OS_BMS_PROTECT_STACK_SIZE     	(256U)
#define OS_MFETCTRL_STACK_SIZE 			(256U)
#define OS_CANTASK_STACK_SIZE 			(1024U)
#define OS_STORAGE_TASK_STACK_SIZE 		(1024U)
#define OS_CURR_SOC_STACK_SIZE      	(512U)

/* Sytem monitoring and configuration */
/* Measurement of all the raw data */
#define OS_DIAGMONTASK_PRIORITY			(osPriorityRealtime)
#define OS_BMSTASK1_PRIORITY			(osPriorityAboveNormal)
#define OS_UARTTASK_PRIORITY  			(osPriorityNormal) //
#define OS_HVTASK_PRIORITY				(osPriorityRealtime) //
#define OS_OPERATION_PRIORITY           (osPriorityAboveNormal) //
#define OS_MFETCTRL_PRIORITY			(osPriorityRealtime)
#define OS_CURR_SOC_PRIORITY            (osPriorityAboveNormal)
#define OS_CANTASK_PRIORITY  			(osPriorityRealtime)
#define OS_BMS_STORAGE_T7_PRIORITY		(osPriorityAboveNormal)
#define OS_PROTECT_PRIORITY       		(osPriorityAboveNormal)

/** @} */
/* bms task 1 Queue Size */
#define OS_BMS_TASK1_QUEUE_SIZE			(10U)
#define OS_UART_TASK_QUEUE_SIZE       	(10U)
#define OS_CURR_SOC_TASK_QUEUE_SIZE   	(10U)
#define OS_CAN_TASK_QUEUE_SIZE       	(20U)
#define OS_BMS_STRG_T7_QUEUE_SIZE  		(5U)


os_storage_queue_tst os_storage_queue_st;
operating_system_can_comm_queue_tst os_can_comm_queue_st;

/* Public Function Definitions */
U8 operating_system_init_u8(void);
void operating_system_start_scheduler_v(void);

/**
=====================================================================================================================================================

@fn Name	    :operating_system_init_u8
@b Scope            :public
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 operating_system_init_u8(void)
{
	U8 return_val_u8 = 0;


//	if (operating_system_create_semaphore_pru8())
//	{
//		return_val_u8 += 1;
//		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_OS_CREATE_SEMPR);
//	}
	if (operating_system_create_queue_pru8())
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_OS_CREATE_QUEUE);
		return_val_u8 += 1;
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_OS_CREATE_QUEUE);
	}
//	if (operating_system_create_mutex_pru8())
//	{
//		return_val_u8 += 1;
//		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_OS_CREATE_MUTEX);
//	}

	if (operating_system_create_task_pru8())
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_OS_CREATE_TASK);
		return_val_u8 += 1;
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_OS_CREATE_TASK);
	}

	return (return_val_u8);
}

/**
=====================================================================================================================================================

@fn Name	    :operating_system_start_scheduler_v
@b Scope            :Public
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void operating_system_start_scheduler_v(void)
{

	/*start tasks*/
	vTaskStartScheduler();
}
/**
=====================================================================================================================================================

@fn Name	    :operating_system_create_semaphore_pru8
@b Scope            : Private
@n@n@b Description  :Creates all required Semaphore flags
@param Input Data   :void
@return Return Value:U8

=====================================================================================================================================================
*/
static U8 operating_system_create_semaphore_pru8(void)
{
	U8 return_val_u8 = 0;

	/* Semaphore for SPI afe*/
//	OS_afe_spi_bsmpr_ge = xSemaphoreCreateBinary();
//		if (OS_afe_spi_bsmpr_ge == NULL)
//		{
//			return_val_u8++;
//		}
//		else
//		{
//			xSemaphoreGive(OS_afe_spi_bsmpr_ge);
//		}

	 return (return_val_u8);
}

/**
=====================================================================================================================================================

@fn Name	    :operating_system_create_queue_pru8
@b Scope            : Private
@n@n@b Description  :Creates all required queue
@param Input Data   :void
@return Return Value:U8

=====================================================================================================================================================
*/
static U8 operating_system_create_queue_pru8(void)
{
	U8 return_val_u8 = 0;
	/* BMS Queue Creation*/
	os_bms_afe_queue_handler_ge = xQueueCreate(OS_BMS_TASK1_QUEUE_SIZE, sizeof(operating_system_bms_afe_queue_tst));

	if (os_bms_afe_queue_handler_ge == NULL)
	{
		return_val_u8++;
	}

	os_can_comm_queue_handler_ge = xQueueCreate(OS_CAN_TASK_QUEUE_SIZE, sizeof(operating_system_can_comm_queue_tst));

	if (os_can_comm_queue_handler_ge == NULL)
	{
		return_val_u8++;
	}

	os_storage_t7_qhandler_ge = xQueueCreate(OS_BMS_STRG_T7_QUEUE_SIZE, sizeof(os_storage_queue_tst));

	if (os_storage_t7_qhandler_ge == NULL)
	{
		return_val_u8++;
	}

	os_curr_soc_queue_handler_ge = xQueueCreate(OS_CURR_SOC_TASK_QUEUE_SIZE, sizeof(os_curr_soc_queue_tst));

	if (os_storage_t7_qhandler_ge == NULL)
	{
		return_val_u8++;
	}

	return (return_val_u8);
}

/**
=====================================================================================================================================================

@fn Name	    :operating_system_create_mutex_pru8
@b Scope            : Private
@n@n@b Description  :
@param Input Data   :void
@return Return Value:U8

=====================================================================================================================================================
*/
static U8 operating_system_create_mutex_pru8(void)
{
	U8 return_val_u8 = COM_HDR_MAX_U8;


	   return_val_u8 = 0;
	   os_battparm_bmutex_ge = xSemaphoreCreateMutex();
	   if(os_battparm_bmutex_ge == NULL)
	   {
		   return_val_u8++;
	   }
	   else
	   xSemaphoreGive(os_battparm_bmutex_ge);

	   return (return_val_u8);
}

/**
=====================================================================================================================================================

@fn Name	    : vApplicationStackOverflowHook
@b Scope            : Private
@n@n@b Description  :
@param Input Data   :void
@return Return Value:U8

=====================================================================================================================================================
*/
void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
 U8 tt = 0;

 tt++;
}
/**
=====================================================================================================================================================

@fn Name	    :operating_system_create_task_pru8
@b Scope            : Private
@n@n@b Description  :
@param Input Data   :void
@return Return Value:U8

=====================================================================================================================================================
*/
static U8 operating_system_create_task_pru8(void)
{
	U8 return_val_u8 = 0;


#if 1
	xTaskCreate(diagmonitor_task_monitor_v,      /* The function that implements the task. */
	"DiagMon", 									 /* The text name assigned to the task - for debug only as it is not used by the kernel. */
	OS_DIAGMONTASK_STACK_SIZE,                   /* The size of the stack to allocate to the task. */
	NULL, 										 /* The parameter passed to the task - just to check the functionality. */
	OS_DIAGMONTASK_PRIORITY, 					 /* The priority assigned to the task. */
	&os_diagmon_handler_ge); 					 /* The task handle is not required, so NULL is passed. */

	if (os_diagmon_handler_ge == NULL)
	{
		return_val_u8++;
	}

#endif
#if 1
	/**********************************************************************************************************************/
	/* definition and creation of bmsTask */
    xTaskCreate(bms_afe_management_v,					/* The function that implements the task. */
				"bms_afe",									/* The text name assigned to the task - for debug only as it is not used by the kernel. */
				OS_BMSTASK1_STACK_SIZE, 				/* The size of the stack to allocate to the task. */
				NULL,                              		/* The parameter passed to the task - just to check the functionality. */
				OS_BMSTASK1_PRIORITY, 					/* The priority assigned to the task. */
				&os_bmstask1_handler_ge );				/* The task handle is not required, so NULL is passed. */

	if (os_bmstask1_handler_ge == NULL)
	{
		return_val_u8++;
	}
	/**********************************************************************************************************************/
#endif

#if 1
	/**********************************************************************************************************************/
	xTaskCreate(bms_protection_task_v,					/* The function that implements the task. */
					"protec_task",									/* The text name assigned to the task - for debug only as it is not used by the kernel. */
					OS_BMS_PROTECT_STACK_SIZE, 				/* The size of the stack to allocate to the task. */
					NULL,                              		/* The parameter passed to the task - just to check the functionality. */
					OS_PROTECT_PRIORITY, 					/* The priority assigned to the task. */
					&os_bmsprotect_handler_ge);				/* The task handle is not required, so NULL is passed. */

		if (os_bmsprotect_handler_ge == NULL)
		{
			return_val_u8++;
		}
	/**********************************************************************************************************************/
#endif

#if 1
	/**********************************************************************************************************************/
	xTaskCreate(bms_mfet_ctrl_task_v, 				/* The function that implements the task. */
				"mfet_control", 					/* The text name assigned to the task - for debug only as it is not used by the kernel. */
				OS_MFETCTRL_STACK_SIZE, 				/* The size of the stack to allocate to the task. */
				NULL, 								/* The parameter passed to the task - just to check the functionality. */
				OS_MFETCTRL_PRIORITY, 				/* The priority assigned to the task. */
				&os_mfet_ctrl_handler_ge); 			/* The task handle is not required, so NULL is passed. */

	if (os_mfet_ctrl_handler_ge == NULL)
	{
		return_val_u8++;
	}
	/**********************************************************************************************************************/
#endif


#if 1
	/**********************************************************************************************************************/
	xTaskCreate(can_comm_task_v,					/* The function that implements the task. */
					"can_task",									/* The text name assigned to the task - for debug only as it is not used by the kernel. */
					OS_CANTASK_STACK_SIZE, 				/* The size of the stack to allocate to the task. */
					NULL,                              		/* The parameter passed to the task - just to check the functionality. */
					OS_CANTASK_PRIORITY, 					/* The priority assigned to the task. */
					&os_can_comm_handler_ge);				/* The task handle is not required, so NULL is passed. */

		if (os_can_comm_handler_ge == NULL)
		{
			return_val_u8++;
		}
	/**********************************************************************************************************************/
#endif

#if 1
	/**********************************************************************************************************************/
	/* definition and creation of bms storage task */
	xTaskCreate(storage_task_t8_v,							/* The function that implements the task. */
					"BMS_STORAGE_TSK",						/* The text name assigned to the task - for debug only as it is not used by the kernel. */
					OS_STORAGE_TASK_STACK_SIZE, 			/* The size of the stack to allocate to the task. */
					NULL,                              		/* The parameter passed to the task - just to check the functionality. */
					OS_BMS_STORAGE_T7_PRIORITY,				/* The priority assigned to the task. */
					&os_bms_storage_t7_handler_ge );		/* The task handle is not required, so NULL is passed. */

	if (os_bms_storage_t7_handler_ge == NULL)
	{
		return_val_u8 ++;
	}
	/**********************************************************************************************************************/
#endif

#if 1
	/**********************************************************************************************************************/
	xTaskCreate(bms_soc_estimation_task_v, 				/* The function that implements the task. */
				"soc_esti", 					/* The text name assigned to the task - for debug only as it is not used by the kernel. */
				OS_CURR_SOC_STACK_SIZE, 				/* The size of the stack to allocate to the task. */
				NULL, 								/* The parameter passed to the task - just to check the functionality. */
				OS_CURR_SOC_PRIORITY, 				/* The priority assigned to the task. */
				&os_curr_soc_handler_ge); 			/* The task handle is not required, so NULL is passed. */

	if (os_curr_soc_handler_ge == NULL)
	{
		return_val_u8++;
	}
	/**********************************************************************************************************************/
#endif


	return (return_val_u8);
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
