/**
========================================================================================================================
@page spi_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	spi.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "main.h"

/* Application Header File */
/* #include "bcc_peripheries.h" */

/* This header file */
#include "gt_spi.h"

/* Driver header */
#include "gt_memory.h"
#include "gt_timer.h"
#include "spi.h"


/** @{
 * Private Definitions **/
#define SPI_TX_BLOCKING_MAX_TIMEOUT_MS 		(200U)

#define SPI_OS_AWARE						(0U)

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_spi_master_init_u8(spi_instances_te spi_instance_are)
{
	U8 ret_val_u8 = 0;

	switch(spi_instance_are)
	{
		/**********************************************************************************************************************/
		case SPI_0_INSTANCE:
		{
			#if SPI_MASTER_INSTANCE_0
			MX_SPI1_Init();
//			{
//				ret_val_u8 ++;
//			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_1_INSTANCE:
		{
			#if SPI_MASTER_INSTANCE_1
			MX_SPI2_Init();
//			{
//				ret_val_u8 ++;
//			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_2_INSTANCE:
		{
			#if SPI_MASTER_INSTANCE_2
			MX_SPI3_Init();
//			{
//				ret_val_u8 ++;
//			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return(ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_spi_master_transmit_receive_u8(const U8 instance_aru8,const void* tx_buff_arpv,uint16_t tx_num_of_frames_aru16,
								  void* rx_buff_arpv, uint16_t rx_num_of_frames_aru16)
{
	U8 ret_val_u8 = 0;

#if SPI_OS_AWARE
/********************************************************************************************************************************************/
		if (SPI_0_INSTANCE == instance_aru8)		/*If spi instance is 0 */
		{
			/* Need to change semaphore handle */
			if (xSemaphoreTake(OS_afe_spi_bsmpr_ge, SPI_TX_BLOCKING_MAX_TIMEOUT_MS) == pdFALSE)
			{
				/* Timeout had happened */
				return (ret_val_u8 + 1);
			}

			if (NULL != tx_buff_arpv)
			{
				memory_copy_u8_array_v((U8*) spi_handle_ast[instance_aru8].tx_buffer_au8, (U8*) tx_buff_arpv, (U32) tx_num_of_frames_aru16);
			}
			/* Update the Busy status and proceed */
			spi_handle_ast[instance_aru8].busy_b = COM_HDR_TRUE;
			ret_val_u8 = LPSPI_DRV_MasterTransfer(SPI_COM_0, spi_handle_ast[instance_aru8].tx_buffer_au8,
					spi_handle_ast[instance_aru8].rx_buffer_au8, (tx_num_of_frames_aru16 + rx_num_of_frames_aru16));

			/* TODO: Need to change semaphore handle */
			if (xSemaphoreTake(OS_afe_spi_bsmpr_ge, SPI_TX_BLOCKING_MAX_TIMEOUT_MS) = pdFALSE)
			{
				/* Timeout had happened */
				//TODO RTOS: Don't know why it could happen
				xSemaphoreGive(OS_afe_spi_bsmpr_ge);
			}
			else
			{
				/* Successful completion of the SPI process. Give it for the next transaction to take place */
				xSemaphoreGive(OS_afe_spi_bsmpr_ge);
			}

			if (NULL != rxBuffer)
			{
				memory_copy_u8_array_v((U8*) rx_buff_arpv, (U8*) &spi_handle_ast[instance_aru8].rx_buffer_au8[tx_num_of_frames_aru16],
						(U32) rx_num_of_frames_aru16);
			}
		}
		/***********************************************************************************************************************************/
		if (SPI_1_INSTANCE == instance_aru8)		/*If spi instance is 1 */
		{
			/* TODO: Need to change semaphore handle */
			if (xSemaphoreTake(OS_afe_spi_bsmpr_ge, SPI_TX_BLOCKING_MAX_TIMEOUT_MS) == pdFALSE)
			{
				/* Timeout had happened */
				return (ret_val_u8 + 1);
			}

			if (NULL != tx_buff_arpv)
			{
				memory_copy_u8_array_v((U8*) spi_handle_ast[instance_aru8].tx_buffer_au8, (U8*) tx_buff_arpv, (U32) tx_num_of_frames_aru16);
			}
			/* Update the Busy status and proceed */
			spi_handle_ast[instance_aru8].busy_b = COM_HDR_TRUE;
			ret_val_u8 = LPSPI_DRV_MasterTransfer(SPI_COM_1, spi_handle_ast[instance_aru8].tx_buffer_au8,
					spi_handle_ast[instance_aru8].rx_buffer_au8, (tx_num_of_frames_aru16 + rx_num_of_frames_aru16));

			/* TODO: Need to change semaphore handle */
			if (xSemaphoreTake(OS_afe_spi_bsmpr_ge, SPI_TX_BLOCKING_MAX_TIMEOUT_MS) = pdFALSE)
			{
				/* Timeout had happened */
				//TODO RTOS: Don't know why it could happen
				xSemaphoreGive(OS_afe_spi_bsmpr_ge);
			}
			else
			{
				/* Successful completion of the SPI process. Give it for the next transaction to take place */
				xSemaphoreGive(OS_afe_spi_bsmpr_ge);
			}

			if (NULL != rx_buff_arpv)
			{
				memory_copy_u8_array_v((U8*) rx_buff_arpv, (U8*) &spi_handle_ast[instance_aru8].rx_buffer_au8[tx_num_of_frames_aru16],
						(U32) rx_num_of_frames_aru16);
			}
		}
		/***********************************************************************************************************************************/
		if (SPI_2_INSTANCE == instance_aru8)		/*If spi instance is 2 */
		{
			/* TODO: Need to change semaphore handle */
			if (xSemaphoreTake(OS_afe_spi_bsmpr_ge, SPI_TX_BLOCKING_MAX_TIMEOUT_MS) == pdFALSE)
			{
				/* Timeout had happened */
				return (ret_val_u8 + 1);
			}

			if (NULL != tx_buff_arpv)
			{
				memory_copy_u8_array_v((U8*) spi_handle_ast[instance_aru8].tx_buffer_au8, (U8*) tx_buff_arpv, (U32) tx_num_of_frames_aru16);
			}
			/* Update the Busy status and proceed */
			spi_handle_ast[instance_aru8].busy_b = COM_HDR_TRUE;
			ret_val_u8 = LPSPI_DRV_MasterTransfer(SPI_COM_2, spi_handle_ast[instance_aru8].tx_buffer_au8,
					spi_handle_ast[instance_aru8].rx_buffer_au8, (tx_num_of_frames_aru16 + rx_num_of_frames_aru16));

			/* TODO: Need to change semaphore handle */
			if (xSemaphoreTake(OS_afe_spi_bsmpr_ge, SPI_TX_BLOCKING_MAX_TIMEOUT_MS) = pdFALSE)
			{
				/* Timeout had happened */
				//TODO RTOS: Don't know why it could happen
				xSemaphoreGive(OS_afe_spi_bsmpr_ge);
			}
			else
			{
				/* Successful completion of the SPI process. Give it for the next transaction to take place */
				xSemaphoreGive(OS_afe_spi_bsmpr_ge);
			}

			if (NULL != rx_buff_arpv)
			{
				memory_copy_u8_array_v((U8*) rx_buff_arpv, (U8*) &spi_handle_ast[instance_aru8].rx_buffer_au8[tx_num_of_frames_aru16],
						(U32) rx_num_of_frames_aru16);
			}
		}

		/************************************************************************************************************************************/
		else
		{
			ret_val_u8++;
		}
	#else
			U16 wait_time_in_ms_u16 = tx_num_of_frames_aru16 + rx_num_of_frames_aru16+2;

			U16 no_of_frames_u16 = tx_num_of_frames_aru16+rx_num_of_frames_aru16;
			/***************************************************************************************************************************************************/
			U8 tx_buff_au8[no_of_frames_u16];

			U8 rx_buff_au8[no_of_frames_u16];

			if (NULL != tx_buff_arpv)
			{
				memory_copy_u8_array_v(tx_buff_au8, (U8*)tx_buff_arpv, tx_num_of_frames_aru16);
				memory_set_u8_array_v(&tx_buff_au8[tx_num_of_frames_aru16], 0xA5, rx_num_of_frames_aru16 );
			}

			if(NULL != rx_buff_arpv)
			{
				memory_set_u8_array_v(rx_buff_au8, 0xA5, no_of_frames_u16 );
			}

			switch(instance_aru8)
			{
				/**********************************************************************************************************************/
				case SPI_0_INSTANCE:
				{
					#if SPI_MASTER_INSTANCE_0
					ret_val_u8 = HAL_SPI_TransmitReceive(SPI_INST_0, tx_buff_au8, rx_buff_au8, no_of_frames_u16 ,wait_time_in_ms_u16);

					#endif
				}
				break;
				/**********************************************************************************************************************/
				case SPI_1_INSTANCE:
				{
					#if SPI_MASTER_INSTANCE_1
					MX_SPI1_Init();
		//			{
		//				ret_val_u8 ++;
		//			}
					#endif
				}
				break;

				/**********************************************************************************************************************/
				case SPI_2_INSTANCE:
				{
					#if SPI_MASTER_INSTANCE_2
					MX_SPI1_Init();
		//			{
		//				ret_val_u8 ++;
		//			}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				default:
				{
					ret_val_u8 += 1;
				}
				break;
				/**********************************************************************************************************************/
			}

			if (NULL != tx_buff_arpv)
			{
				memory_set_u8_array_v(tx_buff_au8, 0xA5, no_of_frames_u16 );
			}

			if(NULL != rx_buff_arpv)
			{
				memory_copy_u8_array_v(rx_buff_arpv, &rx_buff_au8[tx_num_of_frames_aru16], rx_num_of_frames_aru16 );
			}

	#endif

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_spi_slave_init_u8(spi_instances_te spi_instance_are)
{
	U8 ret_val_u8 = 0;

	switch(spi_instance_are)
	{
		/**********************************************************************************************************************/
		case SPI_0_INSTANCE:
		{
			#if SPI_SLAVE_INSTANCE_0
			{
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_1_INSTANCE:
		{
			#if SPI_SLAVE_INSTANCE_1
			{
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_2_INSTANCE:
		{
			#if SPI_SLAVE_INSTANCE_2
			{
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return(ret_val_u8);
}

#if 0
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 spi_slave_transfer_u8(spi_instances_te spi_instance_are,const U8 *tx_buffer_arpu8,U8 *rx_buffer_arpu8,U16 tx_byte_count_aru16)
{
	U8 ret_val_u8 = 0;

	//if(LPSPI_DRV_SlaveTransfer(spi_instance_are,tx_buffer_arpu8,rx_buffer_arpu8,tx_byte_count_aru16))
	{
		ret_val_u8++;
	}

	return(ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 spi_slave_abort_transfer_u8(spi_instances_te spi_instance_are)
{
	U8 ret_val_u8 = 0;

	//if(LPSPI_DRV_SlaveAbortTransfer(spi_instance_are))
	{
		ret_val_u8++;
	}

	return(ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 spi_slave_get_transfer_sts_u8(spi_instances_te spi_instance_are,U32 *bytes_remained_arpu32)
{
	U8 ret_val_u8 = 0;

	if (LPSPI_DRV_SlaveGetTransferStatus(spi_instance_are, bytes_remained_arpu32))
	{
		ret_val_u8++;
	}

	return ret_val_u8;
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_spi_master_deinit_u8(spi_instances_te spi_instance_are)
{
	U8 ret_val_u8 = 0;

	switch(spi_instance_are)
	{
		/**********************************************************************************************************************/
		case SPI_0_INSTANCE:
		{
			#if SPI_MASTER_INSTANCE_0
			if(HAL_SPI_DeInit(SPI_INST_0) != HAL_OK)
			{
				ret_val_u8++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_1_INSTANCE:
		{
			#if SPI_MASTER_INSTANCE_1
			if(HAL_SPI_DeInit(SPI_INST_1) != HAL_OK)
			{
				ret_val_u8++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_2_INSTANCE:
		{
			#if SPI_MASTER_INSTANCE_2
			if(HAL_SPI_DeInit(SPI_INST_2) != HAL_OK)
			{
				ret_val_u8++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return ret_val_u8;
}

#if 0
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 spi_slave_deinit_u8(spi_instances_te spi_instance_are)
{
	U8 ret_val_u8 = 0;

	switch(spi_instance_are)
	{
		/**********************************************************************************************************************/
		case SPI_0_INSTANCE:
		{
			#if SPI_SLAVE_INSTANCE_0
			if(LPSPI_DRV_SlaveDeinit(SPI_COM_1))
			{
				ret_val_u8++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_1_INSTANCE:
		{
			#if SPI_SLAVE_INSTANCE_1
			if(LPSPI_DRV_SlaveDeinit(SPI_COM_1))
			{
				ret_val_u8++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case SPI_2_INSTANCE:
		{
			#if SPI_SLAVE_INSTANCE_2
			if(LPSPI_DRV_SlaveDeinit(SPI_COM_1))
			{
				ret_val_u8++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return(ret_val_u8);
}

#endif

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/

