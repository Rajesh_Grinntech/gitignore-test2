/**
========================================================================================================================
@page can_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	can.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

/* Configuration Header File */
#include "can_app.h"
#include "bootsm.h"

/* This header file */
#include "gt_can.h"

/* Driver header */
#include "gt_system.h"
#include "can.h"

//#include "can_pal_0.h"
/* #include "can_pal_2.h" */




/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
#if 0

void can_0_callback_event_v(uint8_t instance, flexcan_event_type_t eventType,uint32_t buffIdx, flexcan_state_t *flexcanState);
void can_2_callback_event_v(uint8_t instance, flexcan_event_type_t eventType,uint32_t buffIdx, flexcan_state_t *flexcanState);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

can_buff_config_t can_0_buff_tx_config_tst,can_0_buff_rx_config_tst;
can_buff_config_t can_2_buff_tx_config_tst,can_2_buff_rx_config_tst;

can_message_t can_0_rx_message_buf_tst;
can_message_t can_2_rx_message_buf_tst;
#endif
/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */
void gt_can_filter_init_v(CAN_HandleTypeDef* canHandle);
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_can_init_u8(U8 can_instance_aru8)
{
	U8 ret_val_u8 = 0;

	switch(can_instance_aru8)
	{
		/**********************************************************************************************************************/
		case CAN_0_INSTANCE:
		{
			#if CAN_INSTANCE_0
			MX_CAN1_Init();
			gt_can_filter_init_v(&hcan1);

			/*##-4- Activate CAN RX notification #######################################*/
			if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
			{
				/* Notification Error */
				ret_val_u8++;
				Error_Handler();
			}

			/*##-3- Start the CAN Periperal #########*/
			if (HAL_CAN_Start(&hcan1) != HAL_OK)
			{
				/* Start Error */
				ret_val_u8++;
				Error_Handler();
			}

			#endif
		}
		break;
		/**********************************************************************************************************************/
		case CAN_1_INSTANCE:
		{
			#if CAN_INSTANCE_1
			MX_CAN2_Init();
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case CAN_2_INSTANCE:
		{
			#if CAN_INSTANCE_2
			MX_CAN3_Init();
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return(ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_can_filter_init_v(CAN_HandleTypeDef* canHandle)
{
	CAN_FilterTypeDef sFilterConfig;

	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0;
	sFilterConfig.FilterIdLow = 0;
	sFilterConfig.FilterMaskIdHigh = 0;
	sFilterConfig.FilterMaskIdLow = 0;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.FilterBank = 1;

	if (HAL_CAN_ConfigFilter(canHandle, &sFilterConfig) != HAL_OK)
	{
		/* Filter configuration Error */
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_can_transmit_data_u8(can_instances_te can_instance_are,U8 bank_num_aru8, can_tx_type_te type_are, can_message_tst *msg_arpst)
{
	U8 ret_val_u8 = 0;

	/* Transmit the CAN message based on the required mode */
	switch (type_are)
	{
		/**********************************************************************************************************************/
		case CAN_TX_BLOCKING:
		{
			if(CAN_0_INSTANCE == can_instance_are)
			{
				#if CAN_INSTANCE_0
				CAN_TxHeaderTypeDef tx_header_st;
				U32 tx_mail_box_u32 = 0;

				tx_header_st.DLC = msg_arpst->length_u8;
				tx_header_st.IDE = CAN_0_IDE;
				if(CAN_0_IDE == CAN_ID_EXT)
				{
					tx_header_st.ExtId = msg_arpst->can_id_u32;
				}
				else if (CAN_0_IDE == CAN_ID_STD)
				{
					tx_header_st.StdId = msg_arpst->can_id_u32;
				}

				tx_header_st.RTR = CAN_RTR_DATA;

				if(HAL_CAN_AddTxMessage(&hcan1, &tx_header_st, msg_arpst->data_au8, &tx_mail_box_u32) != HAL_OK)
				{
					ret_val_u8 ++;
				}

				while( HAL_CAN_IsTxMessagePending(&hcan1, tx_mail_box_u32));


				#endif
			}
			else if((CAN_1_INSTANCE == can_instance_are))
			{
				#if CAN_INSTANCE_1
					CAN_TxHeaderTypeDef tx_header_st;
					U32 tx_mail_box_u32 = 0;

					tx_header_st.DLC = msg_arpst->length_u8;
					tx_header_st.IDE = CAN_1_IDE;
					if(CAN_1_IDE == CAN_ID_EXT)
					{
						tx_header_st.ExtId = msg_arpst->can_id_u32;
					}
					else if (CAN_1_IDE == CAN_ID_STD)
					{
						tx_header_st.StdId = msg_arpst->can_id_u32;
					}

					tx_header_st.RTR = CAN_RTR_DATA;

					if(HAL_CAN_AddTxMessage(&hcan2, &tx_header_st, msg_arpst->data_au8, &tx_mail_box_u32) != HAL_OK)
					{
						ret_val_u8 ++;
					}
				#endif
			}
			else if((CAN_2_INSTANCE == can_instance_are))
			{
				#if CAN_INSTANCE_2
				CAN_TxHeaderTypeDef tx_header_st;
				U32 tx_mail_box_u32 = 0;

				tx_header_st.DLC = msg_arpst->length_u8;
				tx_header_st.IDE = CAN_2_IDE;
				if(CAN_2_IDE == CAN_ID_EXT)
				{
					tx_header_st.ExtId = msg_arpst->can_id_u32;
				}
				else if (CAN_2_IDE == CAN_ID_STD)
				{
					tx_header_st.StdId = msg_arpst->can_id_u32;
				}

				tx_header_st.RTR = CAN_RTR_DATA;

				if(HAL_CAN_AddTxMessage(&hcan3, &tx_header_st, msg_arpst->data_au8, &tx_mail_box_u32) != HAL_OK)
				{
					ret_val_u8 ++;
				}
				#endif
			}

		}
		break;
		/**********************************************************************************************************************/
		case CAN_TX_NB_DMA:
		{

		}
		break;
		/**********************************************************************************************************************/
		case CAN_TX_NB_IT:
		{
			if(CAN_0_INSTANCE == can_instance_are)
			{
				#if CAN_INSTANCE_0 /* Same code lines as for non-interrupt method but with TX-interrupt enabled*/
				CAN_TxHeaderTypeDef tx_header_st;
				U32 tx_mail_box_u32 = 0;

				tx_header_st.DLC = msg_arpst->length_u8;
				tx_header_st.IDE = CAN_0_IDE;
				if(CAN_0_IDE == CAN_ID_EXT)
				{
					tx_header_st.ExtId = msg_arpst->can_id_u32;
				}
				else if (CAN_0_IDE == CAN_ID_STD)
				{
					tx_header_st.StdId = msg_arpst->can_id_u32;
				}

				tx_header_st.RTR = CAN_RTR_DATA;

				if(HAL_CAN_AddTxMessage(&hcan1, &tx_header_st, msg_arpst->data_au8, &tx_mail_box_u32) != HAL_OK)
				{
					ret_val_u8 ++;
				}
				#endif
			}
			else if((CAN_1_INSTANCE == can_instance_are))
			{
				#if CAN_INSTANCE_1
				if(CAN_Send(&can_pal_1_instance, bank_num_aru8, msg_arpst))
				{
					ret_val_u8 ++;
				}
				#endif
			}
			else if((CAN_2_INSTANCE == can_instance_are))
			{
				#if CAN_INSTANCE_2
				if(CAN_Send(&can_pal_2_instance, bank_num_aru8, msg_arpst))
				{
					ret_val_u8 ++;
				}
				#endif
			}
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 ++;
		}
		/**********************************************************************************************************************/
	}

	return ret_val_u8;
}

#if 0 /* In STM32 Not required*/
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_receive_data_u8(can_instances_te can_instance_are, U8 bank_num_aru8)
{
	U8 ret_val_u8 = 0;

	switch(can_instance_are)
	{
		/**********************************************************************************************************************/
		case CAN_0_INSTANCE:
		{
			#if CAN_INSTANCE_0
			if(CAN_Receive(&can_pal_0_instance, bank_num_aru8, &can_0_rx_message_buf_tst))
			{
				/* TODO: Once system file is included, uncomment the SYSTEM_diag_app_fault_code_gu32 statement */
				app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CAN_RX_START);

				ret_val_u8 += 1;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case CAN_1_INSTANCE:
		{
			#if CAN_INSTANCE_1
			if(CAN_Receive(&can_pal_1_instance, bank_num_aru8, &can_0_rx_message_buf_tst))
			{
				/* TODO: Once system file is included, uncomment the SYSTEM_diag_app_fault_code_gu32 statement */
				/* SYSTEM_diag_app_fault_code_gu32 |= ((U64)1 << FAULT_BIT_CAN_RX_START); */

				ret_val_u8 += 1;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case CAN_2_INSTANCE:
		{
			#if CAN_INSTANCE_2
			if(CAN_Receive(&can_pal_2_instance, bank_num_aru8, &can_2_rx_message_buf_tst))
			{
				/* TODO: Once system file is included, uncomment the SYSTEM_diag_app_fault_code_gu32 statement */
				/* SYSTEM_diag_app_fault_code_gu32 |= ((U64)1 << FAULT_BIT_CAN_RX_START); */

				ret_val_u8 += 1;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return ret_val_u8;
}

#endif
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
#if CAN_INSTANCE_0
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	CAN_RxHeaderTypeDef rx_header_st;

	if(HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &rx_header_st, can_app_rx_message_st.data_au8) == HAL_OK)
	{
		if(rx_header_st.IDE == CAN_ID_STD)
		{
			can_app_rx_message_st.can_id_u32 = rx_header_st.StdId;
		}
		else if(rx_header_st.IDE == CAN_ID_EXT)
		{
			can_app_rx_message_st.can_id_u32 = rx_header_st.ExtId;
		}
		can_app_rx_message_st.length_u8 =  rx_header_st.DLC;

		can_app_0_rx_callback();
	}

}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_deinit_u8(U8 can_instance_aru8)
{
	U8 ret_val_u8 = 0;

	switch(can_instance_aru8)
	{
		/**********************************************************************************************************************/
		case CAN_0_INSTANCE:
		{
			#if CAN_INSTANCE_0
			if( HAL_CAN_DeInit(&hcan1) != HAL_OK)
			{
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case CAN_1_INSTANCE:
		{
			#if CAN_INSTANCE_1
			if(CAN_Deinit(&can_pal_1_instance))
			{
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case CAN_2_INSTANCE:
		{
			#if CAN_INSTANCE_2
			if(CAN_Deinit(&can_pal_2_instance))
			{
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}
	return ret_val_u8;
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
