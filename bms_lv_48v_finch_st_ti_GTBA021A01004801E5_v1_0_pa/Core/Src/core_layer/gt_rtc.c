/**
====================================================================================================================================================================================
@page rtc_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	rtc.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	14-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

/* This header file */
#include "gt_rtc.h"

/* Driver header */
#include "gt_system.h"
#include "rtc.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
rtc_timedate_tst rtc_timedate_gst = {
		.hours_u8 = 14,
		.minutes_u8 = 27,
		.seconds_u8 = 45,
		.date_u8 = 28,
		.weekdays_u8 = RTC_WEEKDAY_MONDAY,
		.months_u8 = RTC_MONTH_JUNE,
		.year_u16 = 20, /*2020*/ /* Allotted two bytes to follow generic*/
};

BOOL rtc_wakeup_flag_b = COM_HDR_FALSE;


/** @} */

/* Public Function Definitions */

U8 gt_rtc_init_u8(void);
U8 gt_rtc_set_time_date_u8(rtc_timedate_tst * rtc_time_arpst);
U8 gt_rtc_get_time_date_u8(rtc_timedate_tst * rtc_time_arpst);
U8 gt_rtc_set_wakeup_timer_u8();
U8 gt_rtc_wakeup_timer_deinit_u8();
U8 gt_rtc_deinit_u8(void);

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_init_u8(void)
{
	U8 ret_val_u8 = 0;

	#if RTC_MODULE_ENABLE
	//if(RTC_DRV_Init(RTC_TIMER, &rtc_timer_cfg_0))

	app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RTC_INIT); /* Fail Set at start*/
	MX_RTC_Init();
	app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_RTC_INIT); /* Fail Removed if successfully returned*/
//	{
//		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RTC_INIT);
//		ret_val_u8 += 1;
//	}

	/* TODO: If Seconds Interrupt of alarm is used,do uncomment the below line */
	/* RTC_DRV_ConfigureSecondsInt(RTCTIMER, &rtcTimer_SecIntConfig0); */

	/**Initialize RTC and set the Time and Date
	 */
	if(gt_rtc_set_time_date_u8(&rtc_timedate_gst))
	{
		ret_val_u8++;
	}

	//RTC->CR &= ~(RTC_CR_WUTE);

	if(HAL_RTCEx_DeactivateWakeUpTimer(&hrtc))
	{
		HAL_Delay(1);
	}

	RTC->ISR &= ~(RTC_ISR_WUTF);

	#endif

	return(ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_start_base_u8(void)
{
	U8 ret_val_u8 = 0;

	#if 0
	if(RTC_DRV_StartCounter(RTC_TIMER))
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RTC_FAILED_TO_START);
		ret_val_u8 += 1;
	}
	#endif

	return ret_val_u8 ;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_stop_base_u8(void)
{
	U8 ret_val_u8 = 0;

	#if 0
	if(RTC_DRV_StopCounter(RTC_TIMER))
	{
		ret_val_u8 += 1;
	}
	#endif

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_set_time_date_u8(rtc_timedate_tst * rtc_time_arpst)
{
	U8 ret_val_u8 = 0;

	#if RTC_MODULE_ENABLE
	RTC_DateTypeDef rtc_date_set_st;
	RTC_TimeTypeDef rtc_time_set_st;

	rtc_date_set_st.Date = rtc_time_arpst->date_u8;
	rtc_date_set_st.Month = rtc_time_arpst->months_u8;
	rtc_date_set_st.WeekDay = rtc_time_arpst->weekdays_u8;
	rtc_date_set_st.Year = rtc_time_arpst->year_u16;

	rtc_time_set_st.Hours = rtc_time_arpst->hours_u8;
	rtc_time_set_st.Minutes =rtc_time_arpst->minutes_u8;
	rtc_time_set_st.Seconds = rtc_time_arpst->seconds_u8;

	rtc_time_set_st.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	rtc_time_set_st.StoreOperation = RTC_STOREOPERATION_RESET;

	if (HAL_RTC_SetDate(RTC_INST_0, &rtc_date_set_st, RTC_FORMAT_BIN) != HAL_OK)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RTC_FAILED_SET_TIME);
		ret_val_u8 += 1;
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_RTC_FAILED_SET_TIME);
	}

	if (HAL_RTC_SetTime(RTC_INST_0, &rtc_time_set_st, RTC_FORMAT_BIN) != HAL_OK)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RTC_FAILED_SET_DATE);
		ret_val_u8 += 1;
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_RTC_FAILED_SET_DATE);
	}
	#endif

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_get_time_date_u8(rtc_timedate_tst * rtc_time_arpst)
{
	U8 ret_val_u8 = 0;

	#if RTC_MODULE_ENABLE

	RTC_DateTypeDef rtc_date_get_st;
	RTC_TimeTypeDef rtc_time_get_st;

	if (HAL_RTC_GetDate(RTC_INST_0, &rtc_date_get_st, RTC_FORMAT_BIN) != HAL_OK)
	{
		ret_val_u8 += 1;
	}

	if (HAL_RTC_GetTime(RTC_INST_0, &rtc_time_get_st, RTC_FORMAT_BIN) != HAL_OK)
	{
		ret_val_u8 += 1;
	}

	rtc_time_arpst->date_u8 	= rtc_date_get_st.Date;
	rtc_time_arpst->months_u8 	= rtc_date_get_st.Month ;
	rtc_time_arpst->weekdays_u8	= rtc_date_get_st.WeekDay;
	rtc_time_arpst->year_u16 	= rtc_date_get_st.Year;

	rtc_time_arpst->hours_u8   = rtc_time_get_st.Hours ;
	rtc_time_arpst->minutes_u8 = rtc_time_get_st.Minutes ;
	rtc_time_arpst->seconds_u8 = rtc_time_get_st.Seconds ;

	#endif

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_set_alarm_u8(rtc_timedate_tst* rtc_alrm_arpst, BOOL repeat_enable_arb)
{
	U8 ret_val_u8 = 0;

	#if RTC_MODULE_ENABLE
		RTC_AlarmTypeDef rtc_alarm_set_st;
		rtc_alarm_set_st.Alarm = RTC_ALARM_A;
		rtc_alarm_set_st.AlarmDateWeekDay = rtc_alrm_arpst->date_u8;
	  rtc_alarm_set_st.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
	  rtc_alarm_set_st.AlarmMask = RTC_ALARMMASK_NONE;
	  rtc_alarm_set_st.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
	  //rtc_alarm_set_st.AlarmTime.TimeFormat = RTC_HOURFORMAT12_AM;
	  rtc_alarm_set_st.AlarmTime.Hours = rtc_alrm_arpst->hours_u8;
	  rtc_alarm_set_st.AlarmTime.Minutes = rtc_alrm_arpst->minutes_u8;
	  rtc_alarm_set_st.AlarmTime.Seconds = rtc_alrm_arpst->seconds_u8;
	  rtc_alarm_set_st.AlarmTime.SubSeconds = 0;

	if(HAL_RTC_SetAlarm_IT(RTC_INST_0,&rtc_alarm_set_st,RTC_FORMAT_BIN) != HAL_OK)
	{
	   /* Initialization Error */
		ret_val_u8 += 1;
	}

	#endif

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_set_wakeup_timer_u8()
{
	U8 ret_val_u8 = 0;

	if (HAL_RTCEx_SetWakeUpTimer_IT(RTC_INST_0, RTC_SLEEP_TIME_SEC, RTC_SLEEP_CLK) != HAL_OK)
	{
		ret_val_u8 += 1;
	}

	return ret_val_u8;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_wakeup_timer_deinit_u8()
{
	U8 ret_val_u8 = 0;

	if(HAL_RTCEx_DeactivateWakeUpTimer(&hrtc) != HAL_OK)
	{
		ret_val_u8++;
	}

	return ret_val_u8;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
	rtc_wakeup_flag_b = COM_HDR_TRUE;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_rtc_deinit_u8(void)
{
	U8 ret_val_u8 = 0;

	if(HAL_RTC_DeInit(RTC_INST_0) != HAL_OK)
	{
	   /* De-Initialization Error */
		ret_val_u8 += 1;
	}

	return ret_val_u8;
}


U32 gt_rtc_rd_bkup_reg_u32(U32 rtc_bkup_dir_u32)
{
  __IO uint32_t tmp = 0;

  /* Check the parameters */
  assert_param(IS_RTC_BKP(rtc_bkup_dir_u32));

  tmp = RTC_BASE + 0x50;
  tmp += (rtc_bkup_dir_u32 * 4);

  /* Read the specified register */
  return (*(__IO uint32_t *)tmp);
}
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
