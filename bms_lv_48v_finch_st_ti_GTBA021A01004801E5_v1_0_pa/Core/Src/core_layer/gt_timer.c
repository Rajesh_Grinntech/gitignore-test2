/**
========================================================================================================================
@page timer_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	timer.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	23-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "main.h"

/* Application Header File */
#include "operating_system.h"
#include "bms_soc_estimation_task.h"
#include "protect_app.h"
#include "bms_mfet_ctrl_task.h"
#include "client_comm.h"
#include "debug_comm.h"
#include "current_calib.h"
#include "cell_bal_app.h"
#include "bootloader_app.h"
#include "bootsm.h"
#include "gt_system.h"
#include "bq76952.h"
/* Driver header */
#include "gt_timer.h"


/** @{
 * Private Definitions **/
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;
TIM_HandleTypeDef htim15;
TIM_HandleTypeDef htim16;


/** @} */
U8 engy_tp_cnt_u8 = 0;

/* Private Function Prototypes */

static U8 soc_count_u8 = 0;


/* Public Function Definitions */
inline void gt_timer_delay_v(timer_resolution_te res_e, U32 delay_us_arg_u32);
U32 gt_timer_get_tick_u32(void);

void gt_timer_set_one_shot_delay_v(U32 comp_timer_u32);



/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_timer_init_v(void)
{
	bootsm_init_state_e = BOOTSM_APPCORE_TIM6_STATE;
	MX_TIM6_Init();  /* 10ms 	*/
	bootsm_init_state_e = BOOTSM_APPCORE_TIM7_STATE;
	MX_TIM7_Init();  /* 100ms 	*/
	bootsm_init_state_e = BOOTSM_APPCORE_TIM15_STATE;
	MX_TIM15_Init(); /* 1s		*/
//	MX_TIM16_Init(); /* 1ms	 One-Shot Timer	*/
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_timer_start_base_v(void)
{
	/* Starts all timer channels interrupts*/
	/* Timer 6 */
	if (HAL_TIM_Base_Start_IT(&htim6) != HAL_OK)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIM6_IT);
		Error_Handler();
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_TIM6_IT);
	}
	/* Timer 7 */
	if (HAL_TIM_Base_Start_IT(&htim7) != HAL_OK)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIM7_IT);
		Error_Handler();
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_TIM7_IT);
	}

	/* Timer 15 */
	if (HAL_TIM_Base_Start_IT(&htim15) != HAL_OK)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIM15_IT);
		Error_Handler();
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_TIM15_IT);
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
inline void gt_timer_delay_v(timer_resolution_te res_e, U32 delay_us_arg_u32)
{
	switch (res_e)
	{
#if 0
		/*******************************************************************************************************************/
		case TIMER_RESOLUTION_MICROSECOND:
		{

		}
		break;
#endif
		/*******************************************************************************************************************/
		case TIMER_RESOLUTION_MILISECOND:
		{
			HAL_Delay(delay_us_arg_u32);
		}
		break;
		/*******************************************************************************************************************/
		default:
		{
		}
		/*******************************************************************************************************************/
	}
}




/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U32 gt_timer_get_tick_u32()
{
	return uwTick;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_timer_set_one_shot_delay_v(U32 comp_timer_u32)
{
	/* Timer 16 */
	MX_TIM16_Init(comp_timer_u32);

	if (HAL_TIM_Base_Start_IT(&htim16) != HAL_OK)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIM16_IT);
		Error_Handler();
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_TIM16_IT);
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */
	//operating_system_bms_afe_queue_tst bms_afe_queue_st;

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  	else if (htim->Instance == TIM6) /*10ms*/
  	{
  		if(can_comm_set_timer_gst[DEBUG_SM].can_set_time_en_u8 == TIMEOUT_SET)
  		{
 			can_comm_set_timer_gst[DEBUG_SM].can_set_time_en_u8 = TIMEOUT_CLEAR;
 			debug_comm_j1939_chck_mp_msg_u8();
  		}

  		if(can_comm_set_timer_gst[CLIENT_SM].can_set_time_en_u8 == TIMEOUT_SET)
  		{
  			can_comm_set_timer_gst[CLIENT_SM].can_set_time_en_u8 = TIMEOUT_CLEAR;
  		  	client_comm_j1939_chck_mp_msg_u8();
  		}
//		if(timer_check_10ms_u32 == 100)
//		{
//			timer_check_10ms_u32 = 0;
//		}
//
//		timer_check_10ms_au32[timer_check_10ms_u32] = HAL_GetTick();
//		timer_check_10ms_u32++;

  		if(current_calib_params_gst.current_calib_flg_b == COM_HDR_ENABLED)
  		{
  			current_calib_params_gst.current_calib_tmr_counter_u32 += 10;  /* 10ms */
  		}

  		if(!can_bootloader_cmd_received_gvlu8)
  		{
		operating_system_bms_afe_queue_tst bms_afe_queue1_st;
  		bms_afe_queue1_st.event_e = BMS_MEASURE_CURRENT;
  	//	bms_afe_queue_st.time_stamp_1ms_u32 = HAL_GetTick();

  		xQueueSendFromISR(os_bms_afe_queue_handler_ge, &bms_afe_queue1_st, COM_HDR_NULL);
  		}

  		soc_count_u8++;
  		if((!can_bootloader_cmd_received_gvlu8) && (soc_count_u8 >=5))
  		{
  			bms_curr_soc_task_callback_v();
  			soc_count_u8 = 0;
  		}
  	}
  	else if(htim->Instance == TIM7) /*100ms*/
  	{

//		if(timer_check_100ms_u32 == 100)
//		{
//			timer_check_100ms_u32 = 0;
//		}
//
//		timer_check_100ms_au32[timer_check_100ms_u32] = HAL_GetTick();
//		timer_check_100ms_u32++;

  		if(!can_bootloader_cmd_received_gvlu8)
  		{
		operating_system_bms_afe_queue_tst bms_afe_queue2_st;
  		bms_afe_queue2_st.event_e = BMS_MEASURE_VTG;
  	//	bms_afe_queue_st.time_stamp_1ms_u32 = HAL_GetTick();

  		xQueueSendFromISR(os_bms_afe_queue_handler_ge, &bms_afe_queue2_st, COM_HDR_NULL);
  		}
  	}
  	else if(htim->Instance == TIM15) /*1s*/
  	{
  		/* Update Protect App 1s events */
  		protect_app_tmr_1s_update_v();


  		if(COM_HDR_TRUE == idle_detect_set_timer_b)
  		{
  			idle_detect_counter_u16++;
  		}

  		cb_switch_counter_u8++;
  		if(cb_switch_counter_u8 >= 10)
  		{
  			cb_switch_counter_u8 = 10;
  			cb_switch_req_b = COM_HDR_TRUE;
  		}

  		if(prt_app_dchg_flg_u8 == COM_HDR_ENABLED)
  		{
  			prt_app_violation_samples_au8[PRT_PACK_OVER_DSG_CURRENT]++;
  		}
  		if(prt_app_cont_dchg_flg_u8 == COM_HDR_ENABLED)
  		{
  			prt_app_violation_samples_au8[PRT_PACK_OVER_CONT_DSG_CURRENT]++;
  		}
  		/*static U8 slepp_chck_u8;
  		slepp_chck_u8++;*/
  		/*if(slepp_chck_u8 > 15)
  		{
  			bq76952_sleep_int_cnf();
  			slepp_chck_u8 = 0;
  		}*/
#if 0
  		if(!can_bootloader_cmd_received_gvlu8)
  		{
  		bms_curr_soc_task_callback_v();
  		}
#endif
		engy_tp_cnt_u8++;
		if((engy_tp_cnt_u8 % 60) == 0)
		{
			update_1min_values_b = COM_HDR_TRUE;
			engy_tp_cnt_u8 = 0;
		}
		else
		{
			update_1sec_values_b = COM_HDR_TRUE;
		}

  	}
  	else if(htim->Instance == TIM16) /*1ms*/
	{
  		if (HAL_TIM_Base_Stop_IT(&htim16) != HAL_OK)
  		{
  			Error_Handler();
  		}

  		if (HAL_TIM_Base_DeInit(&htim16) != HAL_OK)
  		{
  			Error_Handler();
  		}

#if 0
  		if(can_comm_set_timer_gst[DEBUG_SM].can_set_time_en_u8 == TIMEOUT_SET)
  		{
  			can_comm_set_timer_gst[DEBUG_SM].can_set_time_en_u8 = TIMEOUT_CLEAR;
  			debug_comm_j1939_chck_mp_msg_u8();
  		}

  		if(can_comm_set_timer_gst[CLIENT_SM].can_set_time_en_u8 == TIMEOUT_SET)
  		{
  			can_comm_set_timer_gst[CLIENT_SM].can_set_time_en_u8 = TIMEOUT_CLEAR;
  			client_comm_j1939_chck_mp_msg_u8();
  		}
#endif

	}
}

/**
=====================================================================================================================================================

@fn Name			: MX_TIM6_Init
@b Scope            :
@n@n@b Description  : TIM6 init function
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void MX_TIM6_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig = {0};
	 uint32_t              uwTimclock = 0;
	 uint32_t              uwPrescalerValue = 0;
	  uwTimclock = HAL_RCC_GetPCLK1Freq();

	  //100KHz
	 uwPrescalerValue = (uint32_t) ((uwTimclock / 100000) - 1);
	 /* Configured as 10ms Timer */

	 htim6.Instance = TIM6;
	 htim6.Init.Prescaler = uwPrescalerValue;
	 htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
	 htim6.Init.Period = 1000-1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

/**
=====================================================================================================================================================

@fn Name			: MX_TIM7_Init
@b Scope            :
@n@n@b Description  : TIM7 init function
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void MX_TIM7_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig = {0};

	 uint32_t              uwTimclock = 0;
	 uint32_t              uwPrescalerValue = 0;
	  uwTimclock = HAL_RCC_GetPCLK1Freq();

	  //10KHz
	 uwPrescalerValue = (uint32_t) ((uwTimclock / 10000) - 1);
	 /* Configured as 100ms Timer */

	 htim7.Instance = TIM7;
	 htim7.Init.Prescaler = uwPrescalerValue;
	 htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
	 htim7.Init.Period = 1000-1;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}


/**
=====================================================================================================================================================

@fn Name			: MX_TIM15_Init
@b Scope            :
@n@n@b Description  : TIM15 init function
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void MX_TIM15_Init(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

	 uint32_t              uwTimclock = 0;
	 uint32_t              uwPrescalerValue = 0;
	  uwTimclock = HAL_RCC_GetPCLK2Freq();

	  //10KHz
	 uwPrescalerValue = (uint32_t) ((uwTimclock / 10000) - 1);
	 /* Configured as 1s Timer */

  htim15.Instance = TIM15;
  htim15.Init.Prescaler = uwPrescalerValue;
  htim15.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim15.Init.Period = 10000-1;;
  htim15.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim15.Init.RepetitionCounter = 0;
  htim15.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim15) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim15, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim15, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

/**
=====================================================================================================================================================

@fn Name			: MX_TIM16_Init
@b Scope            :
@n@n@b Description  : TIM16 init function
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void MX_TIM16_Init(U32 timer_1ms_u32)
{
		 uint32_t              uwTimclock = 0;
		 uint32_t              uwPrescalerValue = 0;
		  uwTimclock = HAL_RCC_GetPCLK2Freq();

  //100KHz
  uwPrescalerValue = (uint32_t) ((uwTimclock / 100000) - 1);

  /* Configured as 1ms Timer */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = uwPrescalerValue;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 100 * timer_1ms_u32;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
	  app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_TIMER16);
    Error_Handler();
  }
  else
  {
	  app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_TIMER16);
  }

}

/**
=====================================================================================================================================================

@fn Name			: HAL_TIM_Base_MspInit
@b Scope            :
@n@n@b Description  : TIM Base Msp Init
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM6)
  {
  /* USER CODE BEGIN TIM6_MspInit 0 */

  /* USER CODE END TIM6_MspInit 0 */
    /* TIM6 clock enable */
	  __HAL_RCC_TIM6_CLK_ENABLE();
  /* USER CODE BEGIN TIM6_MspInit 1 */

    HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 6, 0);
    //HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  /* USER CODE END TIM6_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM7)
  {
  /* USER CODE BEGIN TIM7_MspInit 0 */

  /* USER CODE END TIM7_MspInit 0 */
    /* TIM7 clock enable */
    __HAL_RCC_TIM7_CLK_ENABLE();
  /* USER CODE BEGIN TIM7_MspInit 1 */

    HAL_NVIC_SetPriority(TIM7_IRQn, 6, 0);
    //HAL_NVIC_EnableIRQ(TIM7_IRQn);
  /* USER CODE END TIM7_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM15)
  {
  /* USER CODE BEGIN TIM15_MspInit 0 */

  /* USER CODE END TIM15_MspInit 0 */
    /* TIM15 clock enable */
	    __HAL_RCC_TIM15_CLK_ENABLE();
  /* USER CODE BEGIN TIM15_MspInit 1 */

    HAL_NVIC_SetPriority(TIM1_BRK_TIM15_IRQn, 6, 0);
    //HAL_NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn);
  /* USER CODE END TIM15_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM16)
  {
  /* USER CODE BEGIN TIM16_MspInit 0 */

  /* USER CODE END TIM16_MspInit 0 */
    /* TIM16 clock enable */
	    __HAL_RCC_TIM16_CLK_ENABLE();

    /* TIM16 interrupt Init */
    HAL_NVIC_SetPriority(TIM1_UP_TIM16_IRQn, 6, 0);
    //HAL_NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
  /* USER CODE BEGIN TIM16_MspInit 1 */

  /* USER CODE END TIM16_MspInit 1 */
  }
}

/**
=====================================================================================================================================================

@fn Name			: HAL_TIM_Base_MspDeInit
@b Scope            :
@n@n@b Description  : TIM Base Msp De-Init
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM6)
  {
  /* USER CODE BEGIN TIM6_MspDeInit 0 */

  /* USER CODE END TIM6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM6_CLK_DISABLE();
  /* USER CODE BEGIN TIM6_MspDeInit 1 */

  /* USER CODE END TIM6_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM7)
  {
  /* USER CODE BEGIN TIM7_MspDeInit 0 */

  /* USER CODE END TIM7_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM7_CLK_DISABLE();
  /* USER CODE BEGIN TIM7_MspDeInit 1 */

  /* USER CODE END TIM7_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM15)
  {
  /* USER CODE BEGIN TIM15_MspDeInit 0 */

  /* USER CODE END TIM15_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM15_CLK_DISABLE();
  /* USER CODE BEGIN TIM15_MspDeInit 1 */

  /* USER CODE END TIM15_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM16)
  {
  /* USER CODE BEGIN TIM16_MspDeInit 0 */

  /* USER CODE END TIM16_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM16_CLK_DISABLE();

    /* TIM16 interrupt Deinit */
    HAL_NVIC_DisableIRQ(TIM1_UP_TIM16_IRQn);
  /* USER CODE BEGIN TIM16_MspDeInit 1 */

  /* USER CODE END TIM16_MspDeInit 1 */
  }
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
