/**
========================================================================================================================
@page clock_pwr_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	clock_pwr.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	24-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "main.h"

/* Application Header File */

/* This header file */
#include "gt_clock_pwr.h"

/* Driver header */
#include "gt_system.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 clock_pwr_system_clock_config_u8(void)
{
	U8 ret_val_u8 = 0;

	/* Update the clock configuration variable with the required configuration */
	SystemClock_Config();
#if 0
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CLOCK_INIT);
		ret_val_u8 += 1;
	}
#endif

	/* Update the clock configuration */
#if 0
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CLOCK_UPDATE);
		ret_val_u8 += 1;
	}

#endif
	/*TODO: Enable this once sleep is implemented  */


	return(ret_val_u8);
}
/**
========================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

========================================================================================================================
*/
#if 0
void clock_pwr_get_reset_cause_v(void)
{
	/* Get the reason for the system start */
		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_WATCH_DOG) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_WDG);
		}

		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_POWER_ON) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_POWER_ON);
		}

		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_SJTAG) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_SJTAG);
		}

		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_LOW_VOLT_DETECT) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_LV_DETECT);
		}

		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_STOP_MODE_ACK_ERR) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_ACK_ERR);
		}

		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_EXTERNAL_PIN) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_NRST_EXT);
		}

		if (POWER_SYS_GetResetSrcStatusCmd(RCM, RCM_SOFTWARE) == true)
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_RESET_SOFT);
		}
}
#endif
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
