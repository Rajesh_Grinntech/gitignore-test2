/**
========================================================================================================================
@page i2c_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	i2c.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */


/* Application Header File */

/* This header file */
#include "gt_i2c.h"

/* Driver header */
#include "gt_memory.h"

/** @{
 * Private Definitions **/
#define I2C_MODULE_ENABLE  						(1U)
#define I2C_BLOCKING_MAX_TIMEOUT_MS 			(100U)

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
/** @} */

/** @{
 *  Public Variable Definitions */
U8 i2c_inst0_slv_addr_u8 = 0;


/** @} */

/* Public Function Definitions */

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 gt_i2c_init_u16(i2c_inst_type_te inst_type_are)
{
	U16 ret_val_u16 = 0;

	switch(inst_type_are)
	{
		  /********************************************************************************************************************************/
		  case I2C_INSTANCE:
		  {
				#if I2C_PAL_INSTANCE_0
			  	MX_I2C1_Init();

			   // ret_val_u16 ;
				#endif
		  }
		  break;
		  /********************************************************************************************************************************/
		  case I2C_INSTANCE_FLEXIO:
		  {
				#if I2C_PAL_FLEXIO_INSTANCE
			  	i2c_pal_flexio_MasterConfig0.extension = &extension_flexio_st;
			  //	ret_val_u16 = I2C_MasterInit(&i2c_pal_flexio_instance, &i2c_pal_flexio_MasterConfig0);


		        flexioI2CConfig.slaveAddress = i2c_pal_flexio_MasterConfig0.slaveAddress;
		        flexioI2CConfig.driverType = FLEXIO_DRIVER_TYPE_POLLING;

		        flexioI2CConfig.sdaPin = ((extension_flexio_for_i2c_t*)(i2c_pal_flexio_MasterConfig0.extension))->sdaPin;
		        flexioI2CConfig.sclPin = ((extension_flexio_for_i2c_t*)(i2c_pal_flexio_MasterConfig0.extension))->sclPin;
		        flexioI2CConfig.callback = i2c_pal_flexio_MasterConfig0.callback;
		        flexioI2CConfig.callbackParam = i2c_pal_flexio_MasterConfig0.callbackParam;
		        flexioI2CConfig.baudRate = i2c_pal_flexio_MasterConfig0.baudRate;
		        ret_val_u16 = FLEXIO_DRV_InitDevice(0U, &flexio_device_state_st);

			  	ret_val_u16 = FLEXIO_I2C_DRV_MasterInit(0U, &flexioI2CConfig, &flexio_i2c_master_state_st[0]);
				#endif
		  }
		  break;
		  /*******************************************************************************************************************************/
		  default :
		  {
			  ret_val_u16++;
		  }
		  break;
		  /*******************************************************************************************************************************/
	}

	return (ret_val_u16);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 gt_i2c_write_data_blocking_u16(i2c_inst_type_te inst_type_are, U8 *tx_buff_arpu8, U32 tx_size_aru32,U8 is_send_stop_aru8)
{
	U16 ret_val_u16 = 0;

	switch(inst_type_are)
	{
		/********************************************************************************************************************************/
		case I2C_INSTANCE:
		{
			#if I2C_PAL_INSTANCE_0

			if(HAL_I2C_Master_Transmit(I2C_INST_0, i2c_inst0_slv_addr_u8, tx_buff_arpu8, tx_size_aru32, I2C_BLOCKING_MAX_TIMEOUT_MS) != HAL_OK)
			{
				ret_val_u16++;
			}

			#endif

		}
		break;
		/********************************************************************************************************************************/
		case I2C_INSTANCE_FLEXIO:
		{
			#if I2C_PAL_FLEXIO_INSTANCE
//			ret_val_u16 = I2C_MasterSendDataBlocking(&i2c_pal_flexio_instance, tx_buff_arpu8, tx_size_aru32, is_send_stop_aru8,
//					   																				      I2C_BLOCKING_MAX_TIMEOUT_MS);

			ret_val_u16 = FLEXIO_I2C_DRV_MasterSendDataBlocking(&flexio_i2c_master_state_st[0], tx_buff_arpu8, tx_size_aru32, is_send_stop_aru8, I2C_BLOCKING_MAX_TIMEOUT_MS);
			#endif
		}
		break;
		/********************************************************************************************************************************/
		default :
		{
			ret_val_u16 ++;
	    }
		break;
		/********************************************************************************************************************************/
	}

	return (ret_val_u16);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 gt_i2c_read_data_blocking_u16(i2c_inst_type_te inst_type_are,U8 *rx_buff_arpu8, U32 rx_size_aru32,U8 is_send_stop_aru8)
{
	U16 ret_val_u16 = 0;

	switch(inst_type_are)
	{
		/********************************************************************************************************************************/
		case I2C_INSTANCE:
		{
			#if I2C_PAL_INSTANCE_0
			if(HAL_I2C_Master_Receive(I2C_INST_0, i2c_inst0_slv_addr_u8, rx_buff_arpu8, rx_size_aru32, I2C_BLOCKING_MAX_TIMEOUT_MS) != HAL_OK)
			{
				ret_val_u16++;
			}
			#endif

		}
		break;
		/********************************************************************************************************************************/
		case I2C_INSTANCE_FLEXIO:
		{
			#if I2C_PAL_FLEXIO_INSTANCE
			//ret_val_u16 = I2C_MasterReceiveDataBlocking(&i2c_pal_flexio_instance, rx_buff_arpu8, rx_size_aru32, is_send_stop_aru8,
				//	   																				      I2C_BLOCKING_MAX_TIMEOUT_MS);

			ret_val_u16 = FLEXIO_I2C_DRV_MasterReceiveDataBlocking(&flexio_i2c_master_state_st[0], rx_buff_arpu8, rx_size_aru32, is_send_stop_aru8, I2C_BLOCKING_MAX_TIMEOUT_MS);
			#endif
		}
		break;
		/********************************************************************************************************************************/
		default :
		{
			ret_val_u16 ++;
	    }
		break;
		/********************************************************************************************************************************/
	}

	return ret_val_u16 ++;

}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 gt_i2c_set_slave_address_u16(i2c_inst_type_te inst_type_are,U8 slave_addr_aru8,BOOL is_10bit_addr_arb)
{
	U16 return_status_u16 = 0;

	//U8 temp_slave_addr_u8 = 0;

	//temp_slave_addr_u8 = slave_addr_aru8 << 1;

	switch(inst_type_are)
	{
		  /********************************************************************************************************************************/
		  case I2C_INSTANCE:
		  {
			#if I2C_PAL_INSTANCE_0

		   		i2c_inst0_slv_addr_u8 = slave_addr_aru8;
		   	#endif

		  }
		  break;
		  /********************************************************************************************************************************/
		  case I2C_INSTANCE_FLEXIO:
		  {
			#if I2C_PAL_FLEXIO_INSTANCE
			//return_status_u16 = I2C_MasterSetSlaveAddress(&i2c_pal_flexio_instance,slave_addr_aru8,is_10bit_addr_arb);

			return_status_u16 = FLEXIO_I2C_DRV_MasterSetSlaveAddr(&flexio_i2c_master_state_st[0], slave_addr_aru8);

			(void)is_10bit_addr_arb;
			#endif
		  }
		  break;
		  /********************************************************************************************************************************/
		  default :
		  {
			  return_status_u16 ++;
		  }
		  break;
		  /*******************************************************************************************************************************/
	}

	return return_status_u16;
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U16 gt_i2c_deinit_u16(i2c_inst_type_te inst_type_are)
{
	U16 ret_val_u16 = 0;

	switch(inst_type_are)
	{
		  /********************************************************************************************************************************/
		  case I2C_INSTANCE:
		  {
			#if I2C_PAL_INSTANCE_0
			  if(HAL_I2C_DeInit(I2C_INST_0) != HAL_OK)
			  {
				  ret_val_u16++;
			  }
			#endif
		  }
		  break;
		  /********************************************************************************************************************************/
		  case I2C_INSTANCE_FLEXIO:
		  {
			#if I2C_PAL_FLEXIO_INSTANCE
			//ret_val_u16 = I2C_MasterDeinit(&i2c_pal_flexio_instance);
			  ret_val_u16=   FLEXIO_I2C_DRV_MasterDeinit(&flexio_i2c_master_state_st[0]);
			  #endif
		  }
		  break;
		  /*******************************************************************************************************************************/
		  default :
		  {
			  ret_val_u16++;
		  }
		  break;
		  /*******************************************************************************************************************************/
	}

	return (ret_val_u16);
}



/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
