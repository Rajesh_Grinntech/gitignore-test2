/**
====================================================================================================================================================================================
@page nvic_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	nvic.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	09-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */


/* Application Header File */

/* Configuration Header File */

/* This header file */
#include "gt_nvic.h"

/* Driver Header File */

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
U8 nvic_num_of_slots_configured_gu8 = 0;

/** @} */

/** @{
 *  Public Variable Definitions */
nvic_init_struct_st nvic_init_st[] =
{

	/* CAN1 Interrupts */
	{ CAN1_RX0_IRQn, 			 2, NVIC_IRQ_LOCK_ENABLE,   NVIC_IRQ_ENABLE  },

	/* SPI Interrupts */

	/* ADC Interrupts */

	/* TIMER Interrupts */
	{ TIM6_DAC_IRQn, 			 4,  NVIC_IRQ_LOCK_ENABLE,  NVIC_IRQ_ENABLE	 },
	{ TIM7_IRQn,				 4,  NVIC_IRQ_LOCK_ENABLE,  NVIC_IRQ_ENABLE  },
	{ TIM1_BRK_TIM15_IRQn, 		 4,  NVIC_IRQ_LOCK_ENABLE,  NVIC_IRQ_ENABLE  },
	{ TIM1_UP_TIM16_IRQn,		 4,  NVIC_IRQ_LOCK_DISABLE,  NVIC_IRQ_ENABLE }, /* Always On*/

	/* REALTIME Interrupts */
	{ RTC_Alarm_IRQn,            3,  NVIC_IRQ_LOCK_ENABLE,  NVIC_IRQ_ENABLE  },
	{ RTC_WKUP_IRQn,             3,  NVIC_IRQ_LOCK_DISABLE,  NVIC_IRQ_ENABLE  },

	/* GPIO Interrupts */
	{ EXTI9_5_IRQn,              5,  NVIC_IRQ_LOCK_ENABLE,  NVIC_IRQ_ENABLE  },
};
/** @} */


/* Public Function Definitions */
void nvic_init_v(void);
void nvic_enable_all_interrupts_v(void);
void nvic_disable_all_interrupts_v(void);
void nvic_enable_interrupt_v(IRQn_Type interrupt_arg_e);
void nvic_disable_interrupt_v(IRQn_Type interrupt_arg_e);

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void nvic_init_v(void)
{
    U8 index_u8 = 0;

    /* Get the number of interrupts to be configured from the structure table */
    nvic_num_of_slots_configured_gu8 = sizeof(nvic_init_st) / sizeof(nvic_init_st[0]);

	/* Disables all Interrupts from 0 to max existing interrupts */
	for (index_u8 = 0; index_u8 < FPU_IRQn; index_u8++)
	{
		NVIC_DisableIRQ(index_u8);
	}

	    /* Set Interrupt priority */
	    for (index_u8 = 0; index_u8 < nvic_num_of_slots_configured_gu8; index_u8++)
	    {
	        HAL_NVIC_SetPriority(nvic_init_st[index_u8].irq_num_e, nvic_init_st[index_u8].priority_u8, 0);
	    }

	    /* Enable Interrupts - Unlocked */
	    for (index_u8 = 0; index_u8 < nvic_num_of_slots_configured_gu8; index_u8++)
	    {
			if (nvic_init_st[index_u8].preos_irqlock_e == NVIC_IRQ_LOCK_DISABLE)
			{
				if (nvic_init_st[index_u8].state_e == NVIC_IRQ_ENABLE)
				{
					NVIC_EnableIRQ(nvic_init_st[index_u8].irq_num_e);
				}
			}
	    }
}

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void nvic_enable_interrupt_v(IRQn_Type interrupt_are)
{
    for (U8 index_u8 = 0; index_u8 < nvic_num_of_slots_configured_gu8; index_u8++)
    {
        if (nvic_init_st[index_u8].irq_num_e == interrupt_are)
        {
        	if (nvic_init_st[index_u8].state_e == NVIC_IRQ_ENABLE)
        	{
        		NVIC_EnableIRQ(interrupt_are);
        	}
            break;
        }
    }
}

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void nvic_disable_interrupt_v(IRQn_Type interrupt_are)
{
	for (U8 index_u8 = 0; index_u8 < nvic_num_of_slots_configured_gu8; index_u8++)
	{
		if (nvic_init_st[index_u8].irq_num_e == interrupt_are)
		{
			NVIC_DisableIRQ(interrupt_are);
			break;
		}
	}
}

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void nvic_enable_all_interrupts_v(void)
{
	for (U8 index_u8 = 0; index_u8 < nvic_num_of_slots_configured_gu8; index_u8++)
	{
		if (nvic_init_st[index_u8].state_e == NVIC_IRQ_ENABLE)
		{
			NVIC_EnableIRQ(nvic_init_st[index_u8].irq_num_e);
		}
	}
}

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void nvic_disable_all_interrupts_v(void)
{
	for (U8 index_u8 = 0; index_u8 < nvic_num_of_slots_configured_gu8; index_u8++)
	{
		NVIC_DisableIRQ(nvic_init_st[index_u8].irq_num_e);
	}
}

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void HardFault_Handler(void)
{

	while(1);
}

/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void NMI_Handler(void)
{

	while(1);
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
