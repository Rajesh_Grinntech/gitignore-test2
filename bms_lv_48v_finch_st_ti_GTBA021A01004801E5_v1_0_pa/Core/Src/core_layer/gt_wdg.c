/**
====================================================================================================================================================================================
@page wdg_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	wdg.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	08-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "main.h"
/* Application Header File */

/* Configuration Header File */

/* This header file */
#include "gt_wdg.h"

/* Driver Header File */
#include "gt_system.h"
/** @{
 * Private Definitions **/
/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
#if WDG_MODULE_ENABLE

#if WDG_IDPT_ENABLE /* Independent Watchdog*/
IWDG_HandleTypeDef hiwdg;
#endif

#if WDG_WINDOW_ENABLE /* Window Watchdog*/
WWDG_HandleTypeDef hwwdg;
#endif

#endif
/** @} */


/* Public Function Definitions */
U8 wdg_init_u8(void);
U8 wdg_deinit_u8(void);
void wdg_refresh_counter_v(void);

#if WDG_MODULE_ENABLE

#if WDG_IDPT_ENABLE /* Independent Watchdog*/
void MX_IWDG_Init(void);
#endif

#if WDG_WINDOW_ENABLE /* Window Watchdog*/
void MX_WWDG_Init(void);
#endif

#endif
/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 wdg_init_u8(void)
{
	U8 ret_val_u8 = 0;

	#if WDG_MODULE_ENABLE
		#if WDG_WINDOW_ENABLE
			MX_WWDG_Init();
		#endif

		#if WDG_IDPT_ENABLE
			MX_IWDG_Init();
		#endif


		#if 0
		{
			app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_WDG_INIT);
			ret_val_u8 += 1;
		}
		#endif
	#endif
		return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void wdg_refresh_counter_v(void)
{
#if WDG_MODULE_ENABLE

#if WDG_WINDOW_ENABLE
	HAL_WWDG_Refresh(&hwwdg);
#endif

#if WDG_IDPT_ENABLE
	HAL_IWDG_Refresh(&hiwdg);
#endif
#endif
}

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 wdg_deinit_u8(void)
{
	U8 ret_val_u8 = 0;

	/* No Deinit for watch dog in this MCU. only system reset*/
	return (ret_val_u8);
}

#if WDG_MODULE_ENABLE

#if WDG_IDPT_ENABLE /* Independent Watchdog*/

/* IWDG init function */
void MX_IWDG_Init(void)
{
	U16 refresh_reqd_ms_u16 = 350; /* 300ms*/
	U8 prescal_val_u8	= 32; /* Possible values : 4, 8, 16, 32, 64, 128, 256*/

	/* IWDG gets frequency of RTC source mux*/
	U32 freq_u32 = LSI_VALUE;

	U16 reload_u16 = ((freq_u32 * refresh_reqd_ms_u16) / (1000 * prescal_val_u8)) - 1;
	if(reload_u16 > 4095)
	{
		 reload_u16 = 4095;
	}

  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
  hiwdg.Init.Window = 4095; 		/* Max value : 4095*/ /* Note: keep this in max value to */
  hiwdg.Init.Reload = reload_u16; /* Max value : 4095*/
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }

 if((FLASH->OPTR & (FLASH_OPTR_IWDG_STDBY | FLASH_OPTR_IWDG_STOP)) != 0x00000)
 {
  HAL_FLASH_Unlock();
  HAL_FLASH_OB_Unlock();
  FLASH->OPTR &= ~(FLASH_OPTR_IWDG_STDBY | FLASH_OPTR_IWDG_STOP );
  FLASH_WaitForLastOperation(FLASH_TIMEOUT_VALUE);
  FLASH->CR |= FLASH_CR_OPTSTRT;
  FLASH_WaitForLastOperation(FLASH_TIMEOUT_VALUE);
  FLASH->CR &= ~(FLASH_CR_OPTSTRT);
//  NVIC_SystemReset();
  FLASH_WaitForLastOperation(FLASH_TIMEOUT_VALUE);
  HAL_FLASH_OB_Launch();
  HAL_FLASH_OB_Lock();
  HAL_FLASH_Lock();
 }

  DBGMCU->APB1FZR1 |= DBGMCU_APB1FZR1_DBG_IWDG_STOP;

}
#endif

#if WDG_WINDOW_ENABLE /* Window Watchdog*/

/* WWDG init function */
void MX_WWDG_Init(void)
{

  hwwdg.Instance = WWDG;
  hwwdg.Init.Prescaler = WWDG_PRESCALER_1;
  hwwdg.Init.Window = 64;
  hwwdg.Init.Counter = 64;
  hwwdg.Init.EWIMode = WWDG_EWI_DISABLE;
  if (HAL_WWDG_Init(&hwwdg) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_WWDG_MspInit(WWDG_HandleTypeDef* wwdgHandle)
{

  if(wwdgHandle->Instance==WWDG)
  {
  /* USER CODE BEGIN WWDG_MspInit 0 */

  /* USER CODE END WWDG_MspInit 0 */
    /* WWDG clock enable */
    __HAL_RCC_WWDG_CLK_ENABLE();
  /* USER CODE BEGIN WWDG_MspInit 1 */

  /* USER CODE END WWDG_MspInit 1 */
  }
}
#endif
#endif
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
