/**
========================================================================================================================
@page adc_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	adc.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	29-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */
#include "common_header.h"
#include "gt_timer.h"
/* Application Header File */

/* This header file */
#include "gt_adc.h"

/* Driver header */
#include "gt_system.h"
#include "adc.h"

/** @{
 * Private Definitions **/
#define ADC0_GROUP_0			0
#define ADC0_GROUP_1 			1
#define ADC1_GROUP_0			0

#define ADC_BLOCKING_MAX_TIMEOUT_MS		(50U)
/** @} */

/* Private Function Prototypes */
U16 gt_adc_get_injected_value_u16(ADC_HandleTypeDef* hadc, U32 rnk_num_aru16);
U16 gt_adc_get_value_u16(ADC_HandleTypeDef* hadc);
/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
BOOL adc_pal_0_grp_0_ready_b = COM_HDR_FALSE;
BOOL adc_pal_0_grp_1_ready_b = COM_HDR_FALSE;
BOOL adc_pal_1_grp_0_ready_b = COM_HDR_FALSE;


/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_init_u8(adc_instances_te adc_instances_are)
{
	U8 ret_val_u8 = 0;

	switch(adc_instances_are)
	{
		/**********************************************************************************************************************/
		case ADC_0_INSTANCE:
		{
			#if ADC_INSTANCE_0
			MX_ADC1_Init();
			{
				//app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_ADC_INIT);
				//ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case ADC_1_INSTANCE:
		{
			#if ADC_INSTANCE_1
			if(ADC_Init(&adc_pal_1_instance, &adc_pal_1_InitConfig0))
			{
				app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_ADC_INIT);
				ret_val_u8 ++;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}



	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_start_conversion_u8(void)
{
	U8 ret_val_u8 = 0;

	#if ADC_INSTANCE_0
		ret_val_u8 = HAL_ADC_Start(ADC_INST_0);
		if(ret_val_u8 != HAL_OK)
		{
				/* Start Conversation Error */
			ret_val_u8++;
		}

	#endif

	#if ADC_INSTANCE_1
		ret_val_u8 = HAL_ADC_Start(ADC_INST_1);
		if(ret_val_u8 != HAL_OK)
		{
				/* Start Conversation Error */
			ret_val_u8++;
		}
	#endif

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_injected_start_conversion_u8(void)
{
	U8 ret_val_u8 = 0;

	#if ADC_INSTANCE_0
		ret_val_u8 = HAL_ADCEx_InjectedStart(ADC_INST_0);
		if(ret_val_u8 != HAL_OK)
		{
				/* Start Conversation Error */
			ret_val_u8++;
		}

	#endif

	#if ADC_INSTANCE_1
		ret_val_u8 = HAL_ADCEx_InjectedStart(ADC_INST_1);
		if(ret_val_u8 != HAL_OK)
		{
				/* Start Conversation Error */
			ret_val_u8++;
		}
	#endif

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_stop_conversion_u8(void)
{
	U8 ret_val_u8 = 0;

#if ADC_INSTANCE_0
	ret_val_u8 = HAL_ADC_Stop(ADC_INST_0);
	if(ret_val_u8 != HAL_OK)
	{
			/* Stop Conversation Error */
		ret_val_u8++;
	}
#endif

#if ADC_INSTANCE_1
	ret_val_u8 = HAL_ADC_Stop(ADC_INST_1);
	if(ret_val_u8 != HAL_OK)
	{
			/* Stop Conversation Error */
		ret_val_u8++;
	}
#endif

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_injected_stop_conversion_u8(void)
{
	U8 ret_val_u8 = 0;

	#if ADC_INSTANCE_0
		ret_val_u8 = HAL_ADCEx_InjectedStop(ADC_INST_0);
		if(ret_val_u8 != HAL_OK)
		{
				/* Start Conversation Error */
			ret_val_u8++;
		}

	#endif

	#if ADC_INSTANCE_1
		ret_val_u8 = HAL_ADCEx_InjectedStop(ADC_INST_1);
		if(ret_val_u8 != HAL_OK)
		{
				/* Start Conversation Error */
			ret_val_u8++;
		}
	#endif

	return (ret_val_u8);
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_read_data_u8(U16* raw_adc_val_arg_pu16)
{
	U8 ret_val_u8 = 0;
	U8 loop_var_u8 = 0;

	/* Update only if the adc conversion are done - polling method */
		#if ADC_INSTANCE_0

			for(loop_var_u8 = ADC_0_CHANNEL0; loop_var_u8 < ADC_0_NUM_CHANNELS-1; loop_var_u8++)
			{
				raw_adc_val_arg_pu16[loop_var_u8] = gt_adc_get_value_u16(ADC_INST_0);
			}

			#if 1 /* Enable only if Internal  MCU die temperature Sensor is available in this ADC instance*/
			ret_val_u8 += gt_adc_stop_conversion_u8();

			ret_val_u8 += gt_adc_injected_start_conversion_u8();

			raw_adc_val_arg_pu16[loop_var_u8] = gt_adc_get_injected_value_u16(ADC_INST_0 ,ADC_INJECTED_RANK_1);

			ret_val_u8 += gt_adc_injected_stop_conversion_u8();

			ret_val_u8 += gt_adc_start_conversion_u8();
			#endif
		#endif

		#if ADC_INSTANCE_1

			for(loop_var_u8 = ADC_1_CHANNEL0; loop_var_u8 < ADC_1_NUM_CHANNELS; loop_var_u8++)
			{
				raw_adc_val_arg_pu16[loop_var_u8] = adc_get_value_u16(ADC_INST_1);
			}

			#if 0 /* Enable only if Internal  MCU die temperature Sensor is available in this ADC instance*/
			ret_val_u8 += adc_stop_conversion_u8();

			ret_val_u8 += adc_injected_start_conversion_u8();

			raw_adc_val_arg_pu16[loop_var_u8] = adc_get_injected_value_u16(ADC_INST_1 ,ADC_INJECTED_RANK_1);

			ret_val_u8 += adc_injected_stop_conversion_u8();

			ret_val_u8 += gt_adc_start_conversion_u8();
			#endif
		#endif

	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_adc_deinit_u8(adc_instances_te adc_instances_are)
{
	U8 ret_val_u8 = 0;

	switch(adc_instances_are)
	{
		/**********************************************************************************************************************/
		case ADC_0_INSTANCE:
		{
			#if ADC_INSTANCE_0
			/* Deinitialization of ADC 0 */
			if(HAL_ADC_DeInit(ADC_INST_0) != HAL_OK)
			{
				ret_val_u8 += 1;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case ADC_1_INSTANCE:
		{
			#if ADC_INSTANCE_1
			/* Deinitialization of ADC 0 */
			if(HAL_ADC_DeInit(ADC_INST_1) != HAL_OK)
			{
				ret_val_u8 += 1;
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			ret_val_u8 += 1;
		}
		break;
		/**********************************************************************************************************************/
	}

	return (ret_val_u8);
}


/*Returns Converted ADC Value*/
U16 gt_adc_get_value_u16(ADC_HandleTypeDef* hadc)
{
	U16 return_value_u16;

	return_value_u16 = HAL_ADC_PollForConversion(hadc, 100);
	if (return_value_u16 != HAL_OK)
	{
		return return_value_u16;
	}

	return_value_u16 = HAL_ADC_GetError(hadc);
	if (return_value_u16 != HAL_ADC_ERROR_NONE)
	{
		return return_value_u16;
	}
	else
	{
		return_value_u16 = HAL_ADC_GetValue(hadc);
	}

	return return_value_u16;
}


/*Returns Converted ADC Value*/
U16 gt_adc_get_injected_value_u16(ADC_HandleTypeDef* hadc, U32 rnk_num_aru16)
{
	U16 return_value_u16;

	return_value_u16 = HAL_ADCEx_InjectedGetValue(hadc,rnk_num_aru16);

	return return_value_u16;
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
