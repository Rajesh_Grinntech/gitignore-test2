/**
====================================================================================================================================================================================
@page system_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	system.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	08-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "main.h"

/* Application Header File */

/* Configuration Header File */

/* This header file */
#include "gt_system.h"

/* Driver Header File */
/** Driver header **/


/** Private Definitions **/
/** Type Definitions **/


/** Private Variable Definitions **/
U32 g_sysClk = 0; /* System clock frequency */

/** Private Function Prototypes **/
static U32 system_mcu_get_clk_freq(void);

/** Public Variable Definitions **/
#if 0
U32 system_diag_init_fault_code_gu32;
U8  system_diag_os_init_fault_codes_gu8;
U16 system_diag_app_init_fault_code_gu16;
#endif
U64 system_diag_app_runtime_flt_code_gu64;
U64 system_diag_app_runtime_prev_flt_gu64;
U64 app_main_system_flt_status_u64 = 0x00;

/** Public Function Definitions **/
void system_mcu_wait_us_v(U32 delay);
void system_mcu_wait_ms_v(U32 delay);
U16  system_generate_crc_u16(U8 *debug_data_array_au8, U16 length_u16);


#if 0
/**
 =====================================================================================================================================================

 @fn Name			 : system_mcu_wait_us_v
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void system_mcu_wait_us_v(U32 delay)
{
    U32 cycles;
    if (delay > 1000)
    {
    	system_mcu_wait_ms_v(delay / 1000);
        delay %= 1000;
    }

    g_sysClk = (g_sysClk) ? g_sysClk : system_mcu_get_clk_freq();

    /* Correction for 48 MHz core clock. */
    delay = (delay > 2) ? delay - 2 : 0;

    cycles = (U32) GET_CYCLES_FOR_US(delay, g_sysClk);

    /* Advance to next multiple of 4. Value 0x04U ensures that the number
     * is not zero. */
    cycles = (cycles & 0xFFFFFFFCU) | 0x04U;
    WAIT_FOR_MUL4_CYCLES(cycles);
}

/**
 =====================================================================================================================================================

 @fn Name			 : system_mcu_wait_ms_v
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void system_mcu_wait_ms_v(U32 delay)
{
    g_sysClk = (g_sysClk) ? g_sysClk : system_mcu_get_clk_freq();

    U32 cycles = (U32) GET_CYCLES_FOR_MS(1U, g_sysClk);

    /* Advance to next multiple of 4. Value 0x04U ensures that the number
     * is not zero. */
    cycles = (cycles & 0xFFFFFFFCU) | 0x04U;

    for (; delay > 0U; delay--) {
        WAIT_FOR_MUL4_CYCLES(cycles);
    }
}
#endif
/**
 =====================================================================================================================================================

 @fn Name			 : system_mcu_get_clk_freq
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static U32 system_mcu_get_clk_freq(void)
{
    U32 freq = 0;
    freq = HAL_RCC_GetSysClockFreq();
    return freq;
}

/**
 =====================================================================================================================================================

 @fn Name			 : system_generate_crc_u16
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
U16 system_generate_crc_u16(U8 *debug_data_array_au8, U16 length_u16)
{

	U8 i;
	U32 data;
	U16 crc = COM_HDR_MAX_U16;

	if (length_u16 == 0)
		return (~crc);

	do
	{
		for (i = 0, data = 0xFF & *debug_data_array_au8++; i < 8; i++, data >>= 1)
		{
			if ((crc & 0x0001) ^ (data & 0x0001))
				crc = (crc >> 1) ^ 0x8408;
			else
				crc >>= 1;
		}
	}
	while (--length_u16);

	crc = ~crc;
	data = crc;
	crc = (crc << 8) | (data >> 8 & 0xff);

	return (crc);
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/

