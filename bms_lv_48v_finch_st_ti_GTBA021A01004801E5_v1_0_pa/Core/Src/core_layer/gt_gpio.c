/**
========================================================================================================================
@page gpio_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	gpio.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	24-Apr-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

========================================================================================================================
*/

/* Common Header Files */

#include "main.h"

/* Application Header File */
#include "bms_afe_task.h"

/* Configuration Header File */
#include "operating_system.h"
//#include "low_pwr_app.h"
/* This header file */
#include "gt_gpio.h"

/* Driver header */
#include "gt_system.h"


/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
void gt_gpio_digital_interrupt_init_v(gpio_port_te port_state);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 gt_gpio_pin_mux_init_u8(void)
{
	U8 ret_val_u8 = 0;

	MX_GPIO_Init();
	
	  GPIO_InitTypeDef GPIO_InitStruct = {0};
#if 0
	  GPIO_InitStruct.Pin = AFE_ALERT_MCU_PTA0_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	  HAL_GPIO_Init(AFE_ALERT_MCU_PTA0_GPIO_Port, &GPIO_InitStruct);

	  HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_0);
	  HAL_PWREx_EnablePullUpPullDownConfig();
#endif

	  HAL_GPIO_WritePin(GPIOB, MCU_RST_SHUT_AFE_PTB4_Pin, GPIO_PIN_RESET);

	  HAL_GPIO_WritePin(GPIOA, MCU_ALL_EN_PSW_PTA1_Pin, GPIO_PIN_SET);//|MCU_EN_CAN_5V_PTA8_Pin
	  HAL_GPIO_WritePin(GPIOA, MCU_EN_CAN_5V_PTA8_Pin, GPIO_PIN_RESET);

	  HAL_GPIO_WritePin(GPIOB, MCU_STB_CAN_PTB1_Pin, GPIO_PIN_RESET);
	return(ret_val_u8);
}
#if 0
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_gpio_digital_interrupt_init_v(gpio_port_te port_state_are)
{
	switch(port_state_are)
	{
		/**********************************************************************************************************************/
		case GPIO_PORT_PTA:
		{
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTB:
		{
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTC:
		{
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTD:
		{
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTE:
		{
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
	
		}
		break;
		/**********************************************************************************************************************/
	}
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_gpio_set_direct_output_v(	const U16 pin_number_aru16, gpio_do_di_state_te state_are,	const gpio_do_map_dir_tst *gpio_io_map_dir_arpst)
{
	U16 i_u16 = pin_number_aru16;
	U16 port_u16;
	U32 pin_mask_u32;
	U16 pin_u16;
	port_u16 = gpio_io_map_dir_arpst[i_u16].gpio_port_reg_e;
	pin_u16 = gpio_io_map_dir_arpst[i_u16].gpio_pins_e;
	pin_mask_u32 = ((U32) COM_HDR_MASK_LO1_BIT << pin_u16);

	switch (port_u16)
	{
		/**********************************************************************************************************************/
		case GPIO_PORT_PTA:
		{
			if (GPIO_DO_ON == state_are)
				HAL_GPIO_WritePin(GPIOA, pin_mask_u32, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(GPIOA, pin_mask_u32, GPIO_PIN_RESET);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTB:
		{
			if (GPIO_DO_ON == state_are)
				HAL_GPIO_WritePin(GPIOB, pin_mask_u32, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(GPIOB, pin_mask_u32, GPIO_PIN_RESET);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTC:
		{
			if (GPIO_DO_ON == state_are)
				HAL_GPIO_WritePin(GPIOC, pin_mask_u32, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(GPIOC, pin_mask_u32, GPIO_PIN_RESET);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTD:
		{
			if (GPIO_DO_ON == state_are)
				HAL_GPIO_WritePin(GPIOD, pin_mask_u32, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(GPIOD, pin_mask_u32, GPIO_PIN_RESET);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTE:
		{
			if (GPIO_DO_ON == state_are)
				HAL_GPIO_WritePin(GPIOE, pin_mask_u32, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(GPIOE, pin_mask_u32, GPIO_PIN_RESET);
		}
		break;
		/**********************************************************************************************************************/
		default:
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_gpio_toggle_direct_output_v(const U16 pin_number_u16_aru16, const gpio_do_map_dir_tst *gpio_io_map_dir_arpst)
{
	U16 i_u16 = pin_number_u16_aru16;
	U16 port_u16;
	U32 pin_mask_u32;
	U16 pin_u16;
	port_u16 = gpio_io_map_dir_arpst[i_u16].gpio_port_reg_e;
	pin_u16 = gpio_io_map_dir_arpst[i_u16].gpio_pins_e;
	pin_mask_u32 = ((U32) COM_HDR_MASK_LO1_BIT << pin_u16);


	switch (port_u16)
	{
		/**********************************************************************************************************************/
		case GPIO_PORT_PTA:
		{
			HAL_GPIO_TogglePin(GPIOA, pin_mask_u32);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTB:
		{
			HAL_GPIO_TogglePin(GPIOB, pin_mask_u32);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTC:
		{
			HAL_GPIO_TogglePin(GPIOC, pin_mask_u32);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTD:
		{
			HAL_GPIO_TogglePin(GPIOD, pin_mask_u32);
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTE:
		{
			HAL_GPIO_TogglePin(GPIOE, pin_mask_u32);
		}
		break;
		/**********************************************************************************************************************/
		default:
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void gt_gpio_get_direct_input_v(const U16 pin_number_aru16, gpio_do_di_state_te *state_are, const gpio_do_map_dir_tst *gpio_io_map_dir_arpst)
{
	U16 i_u16 = pin_number_aru16;
		U16 port_u16;
		U16 pin_u16;
		BOOL pin_state_b;
		U32 pin_mask_u32;
		port_u16 = gpio_io_map_dir_arpst[i_u16].gpio_port_reg_e;
		pin_u16 = gpio_io_map_dir_arpst[i_u16].gpio_pins_e;
		pin_mask_u32 = ((U32) COM_HDR_MASK_LO1_BIT << pin_u16);

		switch (port_u16)
		{
		/**********************************************************************************************************************/
		case GPIO_PORT_PTA:
		{
			pin_state_b = HAL_GPIO_ReadPin(GPIOA, pin_mask_u32);
			if(pin_state_b  == 0 )
			{
				*state_are = GPIO_DO_OFF;
			}
			else
			{
				*state_are = GPIO_DO_ON;
			}

		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTB:
		{
			pin_state_b = HAL_GPIO_ReadPin(GPIOB, pin_mask_u32);
			if(pin_state_b  == 0 )
			{
				*state_are = GPIO_DO_OFF;
			}
			else
			{
				*state_are = GPIO_DO_ON;
			}
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTC:
		{
			pin_state_b = HAL_GPIO_ReadPin(GPIOC, pin_mask_u32);
			if(pin_state_b  == 0 )
			{
				*state_are = GPIO_DO_OFF;
			}
			else
			{
				*state_are = GPIO_DO_ON;
			}
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTD:
		{
			pin_state_b = HAL_GPIO_ReadPin(GPIOD, pin_mask_u32);
			if(pin_state_b  == 0 )
			{
				*state_are = GPIO_DO_OFF;
			}
			else
			{
				*state_are = GPIO_DO_ON;
			}
		}
		break;
		/**********************************************************************************************************************/
		case GPIO_PORT_PTE:
		{
			pin_state_b = HAL_GPIO_ReadPin(GPIOE, pin_mask_u32);
			if(pin_state_b  == 0 )
			{
				*state_are = GPIO_DO_OFF;
			}
			else
			{
				*state_are = GPIO_DO_ON;
			}
		}
		break;
		/**********************************************************************************************************************/
		default:
		{

		}
		break;
		/**********************************************************************************************************************/
		}
}

/**
=====================================================================================================================================================

@fn Name     		:
@b Scope			:
@n@n@b Description	:
@param Input Data	:
@return Return Value:

=====================================================================================================================================================
*/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	operating_system_bms_afe_queue_tst bms_afe_queue_st;
	if(GPIO_Pin & 0x80)
	{
		bms_afe_queue_st.event_e = BMS_FAULT_HIT;
	//	bms_afe_queue_st.time_stamp_1ms_u32 = HAL_GetTick();

		xQueueSendFromISR(os_bms_afe_queue_handler_ge, &bms_afe_queue_st, COM_HDR_NULL);
	}
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
