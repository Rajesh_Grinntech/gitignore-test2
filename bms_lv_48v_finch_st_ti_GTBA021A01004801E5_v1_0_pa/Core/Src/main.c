/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bq76952.h"
#include "bootloader_app.h"

#include "bms_afe_task.h"
#include "operating_system.h"

#include "protect_app.h"

#include "can_app.h"
#include "app_main.h"
#include "core_main.h"

#include "gt_system.h"
#include "bootsm.h"

#include "batt_param.h"
#include "switch_ctrl.h"
#include "afe_driver.h"
#include "i2c_app.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	boot_loader_app_vector_remap_v();
	__enable_irq();
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN SysInit */
	#if 0
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  /* USER CODE BEGIN 2 */
	#endif

	  U8 error_code_u8 = 0;

	  error_code_u8 = core_main_start_u8();

#if 0
	  error_code_u8 += i2c_app_init_u8(I2C_APP_INSTANCE);
	  error_code_u8 += afe_driver_init_u8();
		while(1)
		{
			meas_app_rd_afe_data_u8();

#if 0
			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

			error_code_u8 += bq76952_get_ic_stat_u16(FET_STS);
			error_code_u8 += bq76952_get_ic_stat_u16(ALRM_STS);
			batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
					| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
					| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
					| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));

			HAL_Delay(1000);


			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);
			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

			error_code_u8 += bq76952_get_ic_stat_u16(FET_STS);
			error_code_u8 += bq76952_get_ic_stat_u16(ALRM_STS);
			batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
					| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
					| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
					| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));

			HAL_Delay(1000);
#endif

			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

			error_code_u8 += bq76952_get_ic_stat_u16(FET_STS);
			error_code_u8 += bq76952_get_ic_stat_u16(ALRM_STS);
			batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
					| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
					| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
					| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));

			HAL_Delay(10000);

			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
			switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

			error_code_u8 += bq76952_get_ic_stat_u16(FET_STS);
			error_code_u8 += bq76952_get_ic_stat_u16(ALRM_STS);
			batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
					| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
					| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
					| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));

			HAL_Delay(10000);
		}
#endif

	  error_code_u8 += app_main_start_u8();
	  error_code_u8 += operating_system_init_u8();

	  if((error_code_u8 == 0) && (app_main_system_flt_status_u64 == 0))
	  {
		  operating_system_start_scheduler_v();
	  }
	  else
	  {
		  system_init_flt_update_u8();
	  }

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	  while (1)
	  {
		  /* TODO Some LED stuffs if provided*/
		  //if(led_timer_u8 >= 1)
		  {
			  //led_timer_u8 = 0;
			  //gpio_app_toggle_direct_output_v(UC_B_LED_PTD17);
		  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_7;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 2;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable MSI Auto calibration
  */
  HAL_RCCEx_EnableMSIPLLMode();
}

/* USER CODE BEGIN 4 */
#if 0
/* 4 Clock Error Codes*/
app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CLOCK_INIT);
app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CLOCK_UPDATE);
app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_PCLOCK_UPDATE);
app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_PWR_MAN_INIT);

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
#endif
  /* USER CODE END Callback 1 */
//}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
 if(bootsm_init_state_e == BOOTSM_APPCORE_CAN_STATE)
 {
	 app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CAN_INIT);
	  __disable_irq();
  }
 else
 {
	 while(1)
	 {
		 system_init_flt_update_u8();
	 }
 }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
