/**
====================================================================================================================================================================================
@page debug_comm_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	debug_comm.c
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	17-Jul-2020
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include  "bootloader_app.h"
#include "can_commtask.h"
#include "storage_task.h"
#include "storage_def.h"
#include "common_var.h"
#include "cell_bal_app.h"
//#include "low_pwr_app.h"
//#include "batt_estimt.h"

/* Configuration Header File */
#include "bms_config.h"
#include "bms_afe_task.h"
#include "flash_drv.h"
#include "low_pwr_app.h"
#include "afe_driver.h"

#include "rtc_int.h"
#include "can_app.h"
#include "operating_system.h"
//#include "client_comm.h"
#include "gt_rtc.h"
/* #include "can_q_rx.h" */
#include "current_calib.h"
/* This header file */
#include "debug_comm.h"

/* Driver header */
#include "gt_memory.h"
#include "gt_system.h"
#include "gt_timer.h"
#include "batt_param.h"
#include "battery_management/bootloader_app.h"
#include "gt_nvic.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
U8 debug_gui_u8 = 0;
static void debug_comm_send_ack_prv();

static void debug_comm_send_cell_vtg_prv();
static void debug_comm_update_cell_vtg_prv();
static void debug_comm_send_cell_temp_prv();
static void debug_comm_update_cell_temp_prv();

static void debug_comm_send_hvadc_bend_prv();
static void debug_comm_update_hvadc_prv();
static void debug_comm_update_bend_prv();

static void debug_comm_send_imu_mois_data_prv();
static void debug_comm_update_imu_data_prv();
static void debug_comm_update_mois_prv();

static void debug_comm_send_tms_prv();
static void debug_comm_update_tms_prv();

static void debug_comm_send_sys_flt_sts_prv();
static void debug_comm_send_prv_sys_flt_sts_prv();
static void debug_comm_update_sys_flt_sts_prv();

static void debug_comm_set_date_time_prv(debug_comm_rtc_param_tst *debug_comm_temp_rtc_param_arpst);
static void debug_comm_send_rtc_amb_lght_prv();
static void debug_comm_update_rtc_prv();
static void debug_comm_update_amb_lght_prv();

static void debug_comm_send_afe_slave_flt_sts_prv();
static void debug_comm_update_afe_slave_flt_sts_prv();

static void debug_comm_set_afe_cb_ctrl_prv(debug_comm_afe_cb_slv_te slv_index_are);

static void debug_comm_send_afe_cb_sts_prv(debug_comm_afe_cb_slv_te slv_index_are);
static void debug_comm_update_afe_cb_sts_prv();

static void debug_comm_send_mcu_adc_prv();
static void debug_comm_update_mcu_adc_prv();
static void debug_comm_send_curr_sbc_data_prv();
static void debug_comm_update_curr_sbc_data_prv();

static void debug_comm_send_max_min_cparams_prv();
static void debug_comm_update_max_min_cparams_prv();

static void debug_comm_send_ah_cyc_cnt_prv();
static void debug_comm_update_ah_cyc_cnt_prv();


static void debug_comm_set_bin_prv(debug_comm_bin_msg_te debug_comm_bin_msg_are);
static void debug_comm_set_vin_prv(debug_comm_vin_msg_te debug_comm_vin_msg_are);
static void debug_comm_set_mac_prv();

static void debug_comm_send_bin_prv();
static void debug_comm_send_vin_prv();
static void debug_comm_mac_send_prv();

static void debug_comm_set_warning_cutoff_prv(U8 *warning_cutoff_data_arpu8);
static void debug_comm_update_warn_cutoff_param_prv();

static void debug_comm_bin_ack_send_prv();
static void debug_comm_vin_ack_send_prv();
static void debug_comm_mac_ack_send_prv();
#if DEBUG_COMM_FLASH_DATA_RETRIEVAL
static void debug_comm_ext_flash_erase_percentage_sts_send_prv();
static void debug_comm_ext_flash_erase_ack_send_prv();
#endif
static void debug_comm_send_batt_sts_prv();
static void debug_comm_send_sts_params_prv();

static void debug_comm_j1939_rts_send_prv(U8 pgn_aru8,U16 data_size_aru16);
static void debug_comm_j1939_multipacket_can_msg_update_prv(void);
static U8 	debug_comm_j1939_multipacket_can_msg_send_pru8(void);
static void debug_comm_j1939_eof_msg_handling_prv(void);
/** @{
 * Type definition */

/** @} */

/** @{
 * Private Variable Definitions */
U8 debug_comm_temp_bin_au8[DEBUG_COMM_BIN_LEN];
U8 debug_comm_temp_vin_au8[DEBUG_COMM_VIN_LEN];
U8 debug_comm_can_rx_local_buff_au8[8] = {0,};
U8 *dbg_comm_multipacket_data_gpu8;
U8 debug_comm_flash_operation_sts_u8;

U16 temp_cb_ctrl_au16[DEBUG_COMM_TOTAL_SLAVES];

#if 0 /* TODO Flash*/
U32 flsh_retrival_size_u32 = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;
#endif

debug_comm_rtc_param_tst				debug_comm_temp_rtc_param_st;
debug_comm_send_multipacket_data_tst 	debug_comm_send_multipacket_data_gst;
debug_comm_can_rx_tst 					debug_comm_can_rx_gst;
debug_comm_rts_tst 	 					debug_comm_rts_st;
can_app_message_tst 					debug_comm_tx_msg_st;
debug_comm_rtc_param_tst				debug_comm_temp_rtc_param_st;

/** @} */

/** @{
 *  Public Variable Definitions */
BOOL debug_comm_transmit_gb;
BOOL debug_comm_send_flash_data_gb;

U8 debug_comm_1sec_tmr_gu8 = 0;
U16 debug_comm_cell_vtg_1cv_gau16[DEBUG_COMM_TOTAL_CELLS];
U16 debug_comm_cell_temp_1cc_gau16[10];//DEBUG_COMM_TOTAL_CELL_TEMP_SENS
U16 debug_comm_ntc_temp_1cc_gau16[DEBUG_COMM_TOTAL_NTC_TEMP_SENS];

U8 debug_comm_bin_gau8[DEBUG_COMM_BIN_LEN];
U8 debug_comm_vin_gau8[DEBUG_COMM_VIN_LEN];
U8 debug_comm_mac_gau8[DEBUG_COMM_MAC_LEN];

U8 debug_comm_flash_data_au8[DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD];

U16 debug_comm_amb_light_gu16 = 0;
U16 debug_comm_tms_data_gau16[DEBUG_COMM_TOTAL_TMS_CHANNELS];
U16 debug_comm_sbc_data_gu16;
U16 debug_comm_bender_data_gu16;

debug_comm_hvadc_param_tst 			debug_comm_hvadc_param_gst;
debug_comm_mcu_adc_param_tst 		debug_comm_mcu_adc_param_gst;
debug_comm_rtc_ext_param_tst 		debug_comm_rtc_ext_param_gst;
debug_comm_rtc_ext_param_tst 		debug_comm_rtc_set_ext_param_gst;
debug_comm_rtc_int_param_tst 		debug_comm_rtc_int_param_gst;
debug_comm_imu_param_tst 			debug_comm_imu_param_gst;
debug_comm_moisture_param_tst 		debug_comm_moisture_param_gst;
debug_comm_slv_flt_sts_tst 			debug_comm_slave_flt_sts_gst;
debug_comm_sys_flt_sts_tst 			debug_comm_sys_flt_sts_gst;
debug_comm_max_min_cparams_tst 		debug_comm_max_min_cparams_gst;
U64 debug_comm_cycle_cnt_u64 = 0;
U8 debug_comm_ah_val_u8 = 0;
debug_comm_basic_batt_info_tst 		debug_comm_basic_batt_info_gst;  /* One time information */
debug_comm_batt_sts_params_tst 		debug_comm_batt_sts_params_gst;

debug_comm_cb_sts_tst 				debug_comm_cb_sts_gst;

storage_task_update_batt_data_tst 	debug_com_update_batt_data_st;
os_storage_queue_tst 				debug_comm_ext_flash_queue_st;

/** @} */

/* Public Function Definitions */
void debug_comm_param_init_v();
void debug_comm_update_info_v();

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void debug_comm_param_init_v()
{
	U8 loop_var_u8 = 0;

	/* Initializes Cell voltage*/
	for (loop_var_u8 = 0 ; loop_var_u8 < DEBUG_COMM_TOTAL_CELLS; loop_var_u8++)
	{
		debug_comm_cell_vtg_1cv_gau16[loop_var_u8] = 0xFFFF;
	}

	/* Initializes cell temperature */
	for (loop_var_u8 = 0 ; loop_var_u8 < DEBUG_COMM_TOTAL_CELL_TEMP_SENS; loop_var_u8++)
	{
		debug_comm_cell_temp_1cc_gau16[loop_var_u8] = 0xFFFF;
	}

	/* Initializes NTC temperature - MCU- ADC- B */
	for (loop_var_u8 = 0 ; loop_var_u8 < DEBUG_COMM_TOTAL_NTC_TEMP_SENS; loop_var_u8++)
	{
		debug_comm_ntc_temp_1cc_gau16[loop_var_u8] = 0xFFFF;
	}

	/* Initializes AFE cell fault status */
	for (loop_var_u8 = 0 ; loop_var_u8 < DEBUG_COMM_TOTAL_SLAVES; loop_var_u8++)
	{
		debug_comm_slave_flt_sts_gst.cell_fault_sts_au16[loop_var_u8] = 0xFFFF;

	}

	/* Initializes  Thermistor fault status  */
	debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 = 0xFFFFFFFF;

	/* Initializes  TMS*/
	for (loop_var_u8 = 0 ; loop_var_u8 < DEBUG_COMM_TOTAL_TMS_CHANNELS; loop_var_u8++)
	{
		debug_comm_tms_data_gau16[loop_var_u8] = 0xFFFF;
	}

	/* Initializes HV-ADC */
	debug_comm_hvadc_param_gst.fuse_vtg_1cv_u16 		= 0xFFFF;
	debug_comm_hvadc_param_gst.pack_vtg_1cv_u16 		= 0xFFFF;
	debug_comm_hvadc_param_gst.pcon_vtg_1cv_u16 		= 0xFFFF;
	debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 		= 0xFFFF;

	/* Initializes MCU- ADC-A */
	debug_comm_mcu_adc_param_gst.ncon_vtg_1cv_u16 		= 0xFFFF;
	debug_comm_mcu_adc_param_gst.pcon_vtg_1cv_u16 		= 0xFFFF;
	debug_comm_mcu_adc_param_gst.pchg_vtg_1cv_u16 		= 0xFFFF;
	debug_comm_mcu_adc_param_gst.sbc_mux_out_u16  		= 0xFFFF;

	/* Initializes current and Vbat sens*/
	debug_comm_mcu_adc_param_gst.vbat_sens_u16 	 		= 0xFFFF;
	debug_comm_mcu_adc_param_gst.current_u32 	 		= 0xFFFFFFFF;

	/* Initializes external RTC */
	debug_comm_rtc_ext_param_gst.hours_u8 				= 0xFF;
	debug_comm_rtc_ext_param_gst.minutes_u8 			= 0xFF;
	debug_comm_rtc_ext_param_gst.seconds_u8 			= 0xFF;
	debug_comm_rtc_ext_param_gst.weekdays_u8 			= 0xFF;
	debug_comm_rtc_ext_param_gst.date_u8 				= 0xFF;
	debug_comm_rtc_ext_param_gst.months_u8 				= 0xFF;
	debug_comm_rtc_ext_param_gst.year_u8 				= 0xFF;

	/* Initializes internal RTC */
	debug_comm_rtc_int_param_gst.hours_u8 				= 0xFF;
	debug_comm_rtc_int_param_gst.minutes_u8 			= 0xFF;
	debug_comm_rtc_int_param_gst.seconds_u8 			= 0xFF;
	debug_comm_rtc_int_param_gst.date_u8 				= 0xFF;
	debug_comm_rtc_int_param_gst.months_u8				= 0xFF;
	debug_comm_rtc_int_param_gst.year_u16 				= 0xFFFF;

	/* Initializes  IMU*/
	for (loop_var_u8 = 0 ; loop_var_u8 < AXIS; loop_var_u8++)
	{
		/* Assigned -1 as reserved value */
		debug_comm_imu_param_gst.accel_1g_s32[loop_var_u8]	= 0xFFFFFFFF;
		debug_comm_imu_param_gst.gyro_1dps_s32[loop_var_u8]	= 0xFFFFFFFF;
		debug_comm_imu_param_gst.mag_1ut_s32[loop_var_u8]	= 0xFFFFFFFF;
	}

	/* Initializes HDC moisture sensor */
	debug_comm_moisture_param_gst.hum_1rh_u16				= 0xFFFF;
	debug_comm_moisture_param_gst.temp_1degc_u16			= 0xFFFF;

	/* Initializes Cell balancing Control and Status */
	for (loop_var_u8 = 0 ; loop_var_u8 < DEBUG_COMM_TOTAL_SLAVES; loop_var_u8++)
	{
		debug_comm_cb_sts_gst.cb_ctrl_au16[loop_var_u8] 	= 0x0000; /* initially to be given no fault status -> When values Hardcoded*/
		debug_comm_cb_sts_gst.cb_sts_au16[loop_var_u8] 		= 0xFFFF;
	}

	/* Initializes  Ambient light sensor*/
	debug_comm_amb_light_gu16 	= 0xFFFF;

	/* Initializes  SBC data*/
	debug_comm_sbc_data_gu16  	= 0xFFFF;

	/* Initializes  Bender board data*/
	debug_comm_bender_data_gu16	= 0xFFFF;

	/* Initialize with  the global param bin */
	for (loop_var_u8 = 0; loop_var_u8 < DEBUG_COMM_BIN_LEN ; loop_var_u8++)
	{
		#if DEBUG_COMM_HARD_CODE
		debug_comm_bin_gau8[loop_var_u8] = 0x12;
		#else
		debug_comm_bin_gau8[loop_var_u8] = common_var_bms_id_log_gst.bin_au8[loop_var_u8];
		#endif

	}

	/* Initialize with the global param vin */
	for (loop_var_u8 = 0; loop_var_u8 < DEBUG_COMM_VIN_LEN ; loop_var_u8++)
	{
		#if DEBUG_COMM_HARD_CODE
		debug_comm_vin_gau8[loop_var_u8] = 0x34;
		#else
		debug_comm_vin_gau8[loop_var_u8] = common_var_bms_id_log_gst.vin_au8[loop_var_u8];
		#endif
	}

	/* Initialize with the global param mac */
	for (loop_var_u8 = 0; loop_var_u8 < DEBUG_COMM_VIN_LEN ; loop_var_u8++)
	{
		#if DEBUG_COMM_HARD_CODE
		debug_comm_mac_gau8[loop_var_u8] = 0x56;
		#else
		debug_comm_mac_gau8[loop_var_u8];/* TODO: Initialize MAC addr */
		#endif
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void debug_comm_update_info_v()
{

	#if DEBUG_COMM_SEND_CELL_VTG
	/* Update Cell Voltages 					  */
	debug_comm_update_cell_vtg_prv();
	#endif

	#if DEBUG_COMM_SEND_CELL_TEMP
	/* Update Cell Temperatures 				  */
	debug_comm_update_cell_temp_prv();
	#endif

	#if DEBUG_COMM_SEND_HVADC
	/* Update HVADC measurements 				  */
	debug_comm_update_hvadc_prv();
	#endif

	#if DEBUG_COMM_SEND_MCU_ADC
	/* Update Mcu ADC & current					  */
	debug_comm_update_mcu_adc_prv();

	#endif

	#if DEBUG_COMM_SEND_CURR_SBC
	/* Update Mcu ADC & current					  */
	debug_comm_update_curr_sbc_data_prv();
	#endif

	#if DEBUG_COMM_SEND_TMS
	/* Update TMS temperature measurements 		  */
	debug_comm_update_tms_prv();
	#endif

	#if DEBUG_COMM_SEND_MOISTURE
	/* Update Humidity & Temperature measurements */
	debug_comm_update_mois_prv();
	#endif

	#if DEBUG_COMM_SEND_AMB_LIGHT
	/* Update Ambient Light sensor measurements */
	debug_comm_update_amb_lght_prv();
	#endif

	#if DEBUG_COMM_SEND_BENDER
	/* Update Bender board data measurements */
	debug_comm_update_bend_prv();
	#endif

	#if DEBUG_COMM_SEND_IMU
	/* Update IMU sensor measurements             */
	debug_comm_update_imu_data_prv();
	#endif

	#if DEBUG_COMM_SEND_RTC
	/* Update time & date						  */
	debug_comm_update_rtc_prv();
	#endif

	#if DEBUG_COMM_SEND_AFE_SLV_FLT_STS
	/* Updates Date & Time */
	debug_comm_update_afe_slave_flt_sts_prv();
	#endif

	#if DEBUG_COMM_SEND_AFE_CB_STS
	/* Updates AFE Cell Balancing status */
	debug_comm_update_afe_cb_sts_prv();
	#endif

	#if DEBUG_COMM_SEND_SYS_FLT_STS
	/* Sends system fault status */
	//debug_comm_update_sys_flt_sts_prv(); /* Not Used this one*/
	#endif

	#if DEBUG_COMM_SEND_MAX_MIN_CPARAMS
	/* Sends Max Min Cell parameters */
	debug_comm_update_max_min_cparams_prv();
	#endif


	#if DEBUG_COMM_SEND_AH_CYC_CNT
	/* Sends AH and Cycle counts parameters */
	debug_comm_update_ah_cyc_cnt_prv();
	#endif

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void debug_comm_1s_callback_v()
{
	U8 empty_data = 0x0;

	operating_system_can_comm_queue_tst os_can_queue_1s_st;
	os_can_queue_1s_st. comm_event_e = OS_CAN_DEBUG_TX;
	os_can_queue_1s_st.can_rx_msg_pv = &empty_data;
	xQueueSendFromISR(os_can_comm_queue_handler_ge, &os_can_queue_1s_st,COM_HDR_NULL);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void debug_comm_canrx_process_v(can_comm_task_can_msg_tst* rx_can_message_arpst)
{
	U32 can_received_msg_id_u32 = 0;
	U8 can_data_length_u8 = 0;
	U8 j1939_pgn_u8 = 0;

	can_received_msg_id_u32 = rx_can_message_arpst->id_u32;
	can_data_length_u8 = rx_can_message_arpst->length_u8;
	memory_copy_u8_array_v(&debug_comm_can_rx_local_buff_au8[0], rx_can_message_arpst->data_au8, can_data_length_u8);

	j1939_pgn_u8 = CAN_APP_GET_PGN(can_received_msg_id_u32);

	switch (j1939_pgn_u8)
	{
		/**********************************************************************************************************************/
		case DEBUG_COMM_START_STOP_PGN:
		{
			if(can_bootloader_cmd_received_gvlu8 == 1)
			{
				can_bootloader_cmd_received_gvlu8 = 0;
				nvic_enable_all_interrupts_v();

			}
			/* Start/Stop the debug mode from UI */
			if(0xAA == rx_can_message_arpst->data_au8[0])
			{
				can_bootloader_cmd_received_gvlu8 = 0;
				debug_comm_transmit_gb = 1;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_START_ACK_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			else if(0xFF == rx_can_message_arpst->data_au8[0])
			{
				debug_comm_transmit_gb = 0;
			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AUTH_PGN:
		{

		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BATT_STS_PARAM_PGN:
		{
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_SEND_BATT_STS_PARAM_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_ONE_SEC_PARAM_PGN:
		{
			can_bootloader_cmd_received_gvlu8 = 0;
			/* Update happens here */
			debug_comm_update_info_v();

			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_SEND_ONE_SEC_PARAM_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_ONE_TIME_INFO_PGN:
		{
			debug_comm_update_warn_cutoff_param_prv();
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_SEND_ONE_TIME_INFO_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_BIN1_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_1], &debug_comm_can_rx_local_buff_au8[0],
																	DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
			debug_comm_set_bin_prv(BIN_MSG_1);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_BIN2_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_2], &debug_comm_can_rx_local_buff_au8[0],
																	DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
			debug_comm_set_bin_prv(BIN_MSG_2);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_BIN3_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_3],&debug_comm_can_rx_local_buff_au8[0],
																(DEBUG_COMM_BIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD));
			debug_comm_set_bin_prv(BIN_MSG_3);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_VIN1_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_1], &debug_comm_can_rx_local_buff_au8[0],
																DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
			debug_comm_set_vin_prv(VIN_MSG_1);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_VIN2_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_2], &debug_comm_can_rx_local_buff_au8[0],
																DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
			debug_comm_set_vin_prv(VIN_MSG_2);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_VIN3_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_3],&debug_comm_can_rx_local_buff_au8[0],
																(DEBUG_COMM_VIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD));
			debug_comm_set_vin_prv(VIN_MSG_3);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_MAC_PGN:
		{
			memory_copy_u8_array_v(&debug_comm_mac_gau8[0], &debug_comm_can_rx_local_buff_au8[0], DEBUG_COMM_MAC_LEN);
			debug_comm_set_mac_prv();
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_GET_BIN_VIN_MAC_PGN:
		{
			/* BIN request from GUI */
			if(0xAA == debug_comm_can_rx_local_buff_au8[0])
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BIN_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			/* VIN request from GUI */
			else if(0xBB == debug_comm_can_rx_local_buff_au8[0])
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_VIN_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

			}
			/* MAC request from GUI */
			else if(0xCC == debug_comm_can_rx_local_buff_au8[0])
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MAC_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_RTC_PGN:
		{
			memory_copy_u8_array_v((U8*)&debug_comm_temp_rtc_param_st, &debug_comm_can_rx_local_buff_au8[0] ,
																sizeof(debug_comm_temp_rtc_param_st));

			debug_comm_temp_rtc_param_st.year_u16 = ((debug_comm_can_rx_local_buff_au8[5] << 8)|debug_comm_can_rx_local_buff_au8[6]);
			debug_comm_set_date_time_prv(&debug_comm_temp_rtc_param_st);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AFE_CB_CTRL1_PGN:
		{
			U8 cb_data_length_u8;

			#if DEBUG_COMM_SEND_AFE_CB_STS
			if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_1_4)
			{
				cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) > 8) ? (8) : (DEBUG_COMM_TOTAL_SLAVES * 2);
				memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_1_4],&debug_comm_can_rx_local_buff_au8[0] ,
																									cb_data_length_u8);

				debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_1_4);

				debug_comm_update_afe_cb_sts_prv();

				/* debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_1_4); */

			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AFE_CB_CTRL2_PGN:
		{
			U8 cb_data_length_u8;

			#if DEBUG_COMM_SEND_AFE_CB_STS
			if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_5_8)
			{
				cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) > 16) ? (8) : ((DEBUG_COMM_TOTAL_SLAVES * 2) % 8);
				memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_5_8], &debug_comm_can_rx_local_buff_au8[0] ,
																										cb_data_length_u8);

				debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_5_8);

				debug_comm_update_afe_cb_sts_prv();

				/* debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_5_8); */
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AFE_CB_CTRL3_PGN:
		{
			U8 cb_data_length_u8;

			#if DEBUG_COMM_SEND_AFE_CB_STS
			if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_9)
			{
				cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) % 8);
				memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_9], &debug_comm_can_rx_local_buff_au8[0] ,
																								cb_data_length_u8);

				debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_9);

				debug_comm_update_afe_cb_sts_prv();

				/* debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_9); */
			}
			#endif
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SYS_RESET_PGN:
		{
			NVIC_SystemReset();
		}break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SET_WARN_CUTOFF_PGN:
		{
			debug_comm_set_warning_cutoff_prv(&debug_comm_can_rx_local_buff_au8[0]);
		}
		break;
		/**********************************************************************************************************************/
#if DEBUG_COMM_SEND_FLASH
		case DEBUG_COMM_FLASH_DATA_RETRIEVAL_PGN:
		{
			/* Whenever start retrieval command received, reading data from flash should start from its initial address */
			//flsh_retrival_size_u32 = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;

			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_DATA_RETRIVAL_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_EXT_FLASH_ERASE_PGN:
		{
			/* Checking whether any previous flash operation is completed */
			if(FLASH_DRV_OPERATION_COMPLETED == flash_drv_flash_operation_sts_u8)
			{
				debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;

				flash_drv_blocks_erase_st.start_block_cnt_u16 = 4;
				flash_drv_blocks_erase_st.end_block_cnt_u16 = 512;
				flash_drv_blocks_erase_st.total_blocks_to_erase_u16 = flash_drv_blocks_erase_st.end_block_cnt_u16 - flash_drv_blocks_erase_st.start_block_cnt_u16;
				flash_drv_blocks_erase_st.current_block_cnt_erased_u16 = 0;
				flash_drv_blocks_erase_st.total_blocks_erased_in_percentage_u8 = 0;

				debug_comm_ext_flash_queue_st.event_e = OS_STORAGE_DATA_BLOCK_ERASE;
				debug_comm_ext_flash_queue_st.mode_e = OS_FLASH_ERASE_BLOCK;
				debug_comm_ext_flash_queue_st.payload_pv = &flash_drv_blocks_erase_st;	 /* dummy data must send */
				xQueueSend(os_storage_t7_qhandler_ge, &debug_comm_ext_flash_queue_st, 2);
			}
			/* Checking whether any previous flash operation is in process */
			else if (FLASH_DRV_OPERATION_INPROGRESS == flash_drv_flash_operation_sts_u8)
			{
				debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			/* if external flash has fault */
			else if(FLASH_DRV_OPERATION_FAILED == flash_drv_flash_operation_sts_u8)
			{
				debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
		}
		break;
#endif
		/**********************************************************************************************************************/
#if DEBUG_COMM_SLEEP_WAKE
		case DEBUG_COMM_SLEEP_WAKE_PGN:
		{
			/* Sleep/Wake the MCU from UI */
			if(0xAA == debug_comm_can_rx_local_buff_au8[0])
			{
				/* Implement MCU sleep here*/
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CAN;
			}
			else if(0xFF == debug_comm_can_rx_local_buff_au8[0])
			{
				/* Implement MCU wake here*/
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_CAN;
			}
		}
		break;
#endif
		/**********************************************************************************************************************/
#if DEBUG_COMM_CURR_CALIB
		case DEBUG_COMM_CURRENT_CALIB_ST_PGN:
		{
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_CURRENT_CALIB_ST_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CURRENT_CALIB_SET_PGN:
		{
			current_calib_params_gst.current_calib_flg_b = COM_HDR_ENABLED;
		}break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CURRENT_CALIB_COMPUTE_PGN:
		{
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_CURRENT_CALIB_COMPUTE_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
#endif
		/**********************************************************************************************************************/
#if DEBUG_COMM_BOOT_LOADER
		case DEBUG_COMM_BOOT_START_PGN:
		{
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BOOT_START_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			bootloader_app_can_rx_task_v(DEBUG_COMM_BOOT_START_PGN, can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);

		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BOOT_DATA_PGN:
		{
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BOOT_DATA_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			bootloader_app_can_rx_task_v(DEBUG_COMM_BOOT_DATA_PGN, can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);

		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BOOT_EOF_PGN:
		{
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BOOT_EOF_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			bootloader_app_can_rx_task_v(DEBUG_COMM_BOOT_EOF_PGN,can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);

		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BOOT_ERROR_PGN:
		{
			/* TODO: Need to check whether it is EOF_ACK state */
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BOOT_EOF_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			bootloader_app_can_rx_task_v(DEBUG_COMM_BOOT_ERROR_PGN,can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);

		}
		break;
#endif
		/**********************************************************************************************************************/
		case DEBUG_COMM_RTS_CTS_EOF_PGN:
		{
			/*** J1939  MULTIPCAKET CTS HANDLE ***/
			if(rx_can_message_arpst->data_au8[0] == DEBUG_COMM_CTS_CONTROLBYTE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_UPDATE_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			/*** J1939  MULTIPCAKET EOF HANDLE ***/
			else if(rx_can_message_arpst->data_au8[0] == DEBUG_COMM_EOF_CONTROLBYTE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8  = DEBUG_COMM_MULTIPACK_EOF_MSG_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8  = CAN_APP_STATE_TX;
			}
			/*** J1939  MULTIPCAKET RTS HANDLE ***/
			else if(rx_can_message_arpst->data_au8[0] == DEBUG_COMM_RTS_CONTROLBYTE)
			{

			}
		}
		/**********************************************************************************************************************/
#if DEBUG_COMM_BOOT_LOADER
		case DEBUG_COMM_BOOT_V_NO_PGN:
		{
			//memory_copy_u8_array_v(debug_comm_can_rx_local_buff_au8,&batt_param_batt_pack_details_gst,2);
			bootloader_app_can_tx_task_v(DEBUG_COMM_BOOT_V_NO_PGN,2, &debug_comm_can_rx_local_buff_au8[0]);
		}
		break;
#endif
		/**********************************************************************************************************************/
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}


#if 0
void debug_comm_rx_msg_v(can_app_message_tst * rx_can_message_arpst)
{
#if 0
	if(debug_comm_can_rx_gst.id_u32 != NULL)
	{
		memory_set_u8_array_v(debug_comm_can_rx_gst.data_au8, 0, 8);
		debug_comm_can_rx_gst.id_u32 = NULL;

	}
	/* if COM_HDR_TRUE, then the rx queue is empty*/
	if(COM_HDR_TRUE != can_rx_array_queue_get_u8(debug_comm_can_rx_gst.data_au8, &debug_comm_can_rx_gst.id_u32))
	{
#endif

		if(rx_can_message_arpst->can_id_u32 != NULL)
		{
			switch(rx_can_message_arpst->can_id_u32)
			{
				/**********************************************************************************************************************/
				case DEBUG_COMM_START_STOP_CMD:
				{
					/* Start/Stop the debug mode from UI */
					if(0xAA == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_transmit_gb = 1;
					}
					else if(0xFF == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_transmit_gb = 0;
					}
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_BIN1_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_1], rx_can_message_arpst->data_au8,
																			DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_bin_prv(BIN_MSG_1);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_BIN2_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_2], rx_can_message_arpst->data_au8,
																			DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_bin_prv(BIN_MSG_2);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_BIN3_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_3],rx_can_message_arpst->data_au8,
																		(DEBUG_COMM_BIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD));
					debug_comm_set_bin_prv(BIN_MSG_3);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_VIN1_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_1], rx_can_message_arpst->data_au8,
																		DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_vin_prv(VIN_MSG_1);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_VIN2_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_2], rx_can_message_arpst->data_au8,
																		DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_vin_prv(VIN_MSG_2);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_VIN3_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_3],rx_can_message_arpst->data_au8,
																		(DEBUG_COMM_VIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD));
					debug_comm_set_vin_prv(VIN_MSG_3);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_MAC_CMD:
				{
					memory_copy_u8_array_v(debug_comm_mac_gau8, rx_can_message_arpst->data_au8, DEBUG_COMM_MAC_LEN);
					debug_comm_set_mac_prv();
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_RTC_CMD:
				{
					memory_copy_u8_array_v((U8*)&debug_comm_temp_rtc_param_st, rx_can_message_arpst->data_au8 ,
																		sizeof(debug_comm_temp_rtc_param_st));
					debug_comm_set_date_time_prv(&debug_comm_temp_rtc_param_st);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_AFE_CB_CTRL1_CMD:
				{
					U8 cb_data_length_u8;
					#if DEBUG_COMM_SEND_AFE_CB_STS
					if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_1_4)
					{
						cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) > 8) ? (8) : (DEBUG_COMM_TOTAL_SLAVES * 2);
						memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_1_4], rx_can_message_arpst->data_au8 ,
																											cb_data_length_u8);

						debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_1_4);

						debug_comm_update_afe_cb_sts_prv();

						debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_1_4);

					}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_AFE_CB_CTRL2_CMD:
				{
					U8 cb_data_length_u8;
					#if DEBUG_COMM_SEND_AFE_CB_STS
					if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_5_8)
					{
						cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) > 16) ? (8) : ((DEBUG_COMM_TOTAL_SLAVES * 2) % 8);
						memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_5_8], rx_can_message_arpst->data_au8 ,
																												cb_data_length_u8);

						debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_5_8);

						debug_comm_update_afe_cb_sts_prv();

						debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_5_8);
					}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_AFE_CB_CTRL3_CMD:
				{
					U8 cb_data_length_u8;
					#if DEBUG_COMM_SEND_AFE_CB_STS
					if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_9)
					{
						cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) % 8);
						memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_9], rx_can_message_arpst->data_au8 ,
																										cb_data_length_u8);

						debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_9);

						debug_comm_update_afe_cb_sts_prv();

						debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_9);
					}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SLEEP_WAKE_CMD:
				{
					/* Sleep/Wake the MCU from UI */
					if(0xAA == rx_can_message_arpst->data_au8[0])
					{
						/* Implement MCU sleep here*/
					}
					else if(0xFF == rx_can_message_arpst->data_au8[0])
					{
						/* Implement MCU wake here*/
					}
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_BIN_VIN_MAC_RQT:
				{
					/* Rqt Bin Vin Mac */
					if(0xAA == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_send_bin_prv();
					}
					else if(0xBB == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_send_vin_prv();
					}
					else if(0xCC == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_send_mac_prv();
					}
				}
				break;
				/**********************************************************************************************************************/
				default :
				{

				}
				break;
				/**********************************************************************************************************************/
			}

		}

		rx_can_message_arpst->can_id_u32 = NULL;
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void debug_comm_cantx_process_v(can_comm_task_can_msg_tst* tx_can_message_arg_st)
{
	U32 can_received_msg_id_u32 = 0;
	U8 can_data_length_u8 = 0;
	//static U32 flsh_retrival_size_u32 = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;

	can_received_msg_id_u32 = tx_can_message_arg_st->id_u32;
	can_data_length_u8 = tx_can_message_arg_st->length_u8;
	memory_copy_u8_array_v(&debug_comm_can_rx_local_buff_au8[0], tx_can_message_arg_st->data_au8,
																		can_data_length_u8);

	if(CAN_APP_STATE_TX == common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8)
	{
		switch (common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8)
		{
			/**********************************************************************************************************************/
			case DEBUG_COMM_START_ACK_STATE:
			{
				debug_comm_send_ack_prv();
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			/* TODO: For authentication */


			/**********************************************************************************************************************/
#if DEBUG_COMM_FLASH_DATA_RETRIEVAL
			case DEBUG_COMM_EXT_FLASH_DATA_RETRIVAL_STATE:
			{
				debug_comm_j1939_rts_send_prv(DEBUG_COMM_FLASH_DATA_RETRIEVAL_PGN,DEBUG_COMM_FLASH_RETRIVAL_MSG_SIZE);

				os_storage_queue_st.event_e = OS_STORAGE_DATA_RETRIVAL;
				os_storage_queue_st.mode_e = OS_FLASH_MODE_RD;
				storage_task_flsh_ret_update_batt_data_st.start_addr_u32 = flsh_retrival_size_u32;
				storage_task_flsh_ret_update_batt_data_st.end_addr_u32 = (storage_task_flsh_ret_update_batt_data_st.start_addr_u32 +
																		sizeof(bms_debug_battery_data_log_tst));
				flsh_retrival_size_u32 = storage_task_flsh_ret_update_batt_data_st.end_addr_u32;
				xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);
			}
			break;
#endif
			/**********************************************************************************************************************/
			case DEBUG_COMM_SEND_BATT_STS_PARAM_STATE:
			{
				debug_comm_j1939_rts_send_prv(DEBUG_COMM_BATT_STS_PARAM_PGN,DEBUG_COMM_BATT_PARAM_STS_MSG_SIZE);
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_SEND_ONE_SEC_PARAM_STATE:
			{
				#if DEBUG_COMM_SEND_CELL_VTG
				/* Sends cell voltages */
				debug_comm_send_cell_vtg_prv();
				#endif

				#if DEBUG_COMM_SEND_CELL_TEMP
				/* Sends cells Temperature */
				debug_comm_send_cell_temp_prv();
				#endif

				#if DEBUG_COMM_SEND_HVADC_BENDER
				/* Sends HVADC measurements */
				debug_comm_send_hvadc_bend_prv();
				#endif

				#if DEBUG_COMM_SEND_MCU_ADC
				/* Sends mcu adc measurements */
				debug_comm_send_mcu_adc_prv();
				#endif

				#if DEBUG_COMM_SEND_CURR_SBC
				/* Sends current and sbc measurements */
				debug_comm_send_curr_sbc_data_prv();
				#endif

				#if DEBUG_COMM_SEND_TMS
				/* Sends TMS(Thermal Management System) measurements */
				debug_comm_send_tms_prv();
				#endif

				#if DEBUG_COMM_SEND_MOISTURE_IMU
				/* Sends Moisture(Humidity & Temperature) sensor measurements */
				debug_comm_send_imu_mois_data_prv();
				#endif

				#if DEBUG_COMM_SEND_RTC_AMB_LIGHT
				/* Sends Ambient Light sensor measurements */
				debug_comm_send_rtc_amb_lght_prv();
				#endif

				#if DEBUG_COMM_SEND_SYS_FLT_STS
				/* Sends system fault status */
				system_init_flt_update_u8();
				#endif

				debug_comm_update_sys_flt_sts_prv();
				#if DEBUG_COMM_SEND_SYS_RUN_TIME_FLT_STS
				debug_comm_send_sys_flt_sts_prv();
				#endif

				#if DEBUG_COMM_SYS_PRV_RUN_TIME_FLT_STS
				debug_comm_send_prv_sys_flt_sts_prv();
				#endif
				#if DEBUG_COMM_SEND_AFE_SLV_FLT_STS
				/* Sends afe slave fault status */
				debug_comm_send_afe_slave_flt_sts_prv();
				#endif

				#if DEBUG_COMM_SEND_AFE_CB_STS
				/* Sends AFE Cell Balancing status */
				debug_comm_update_afe_cb_sts_prv();
				debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_ALL);
				#endif

				#if DEBUG_COMM_SEND_MAX_MIN_CPARAMS
				/* Sends Max Min Cell parameters */
				debug_comm_send_max_min_cparams_prv();
				#endif

				#if DEBUG_COMM_SEND_AH_CYC_CNT
				/* Sends AH and Cycle counts parameters */
				debug_comm_send_ah_cyc_cnt_prv();
				#endif

				debug_comm_send_batt_sts_prv();

				debug_comm_send_sts_params_prv();

				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;

			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_SEND_ONE_TIME_INFO_STATE:
			{
				debug_comm_j1939_rts_send_prv(DEBUG_COMM_ONE_TIME_INFO_PGN,DEBUG_COMM_ONE_TIME_INFO_MSG_SIZE);
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_BIN_SEND_STATE:
			{
				debug_comm_j1939_rts_send_prv(DEBUG_COMM_SEND_BIN_PGN,DEBUG_COMM_BIN_MSG_SIZE);
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_VIN_SEND_STATE:
			{
				debug_comm_j1939_rts_send_prv(DEBUG_COMM_SEND_VIN_PGN,DEBUG_COMM_VIN_MSG_SIZE);
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_BIN_ACK_SEND_STATE:
			{
				debug_comm_bin_ack_send_prv();
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_VIN_ACK_SEND_STATE:
			{
				debug_comm_vin_ack_send_prv();
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_MAC_ACK_SEND_STATE:
			{
				debug_comm_mac_ack_send_prv();
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
#if DEBUG_COMM_SEND_FLASH
			/**********************************************************************************************************************/
			case DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE:
			{
				debug_comm_ext_flash_erase_ack_send_prv();
				//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_EXT_FLASH_ERASE_PERCENT_STS_SEND_STATE:
			{
				debug_comm_ext_flash_erase_percentage_sts_send_prv();

				/* Making it idle because DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE message is not sent - after 100% erase
				 * completed need to send it */
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_ERASE_ACK_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
#endif
			/**********************************************************************************************************************/
			case DEBUG_COMM_MAC_SEND_STATE:
			{
				debug_comm_mac_send_prv();
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
#if DEBUG_COMM_BOOT_LOADER
			/**********************************************************************************************************************/
			case DEBUG_COMM_BOOT_START_ACK_STATE:
			{
				bootloader_app_can_tx_task_v(can_received_msg_id_u32,can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_BOOT_DATA_ACK_STATE:
			{
				bootloader_app_can_tx_task_v(can_received_msg_id_u32,can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_BOOT_EOF_ACK_STATE:
			{
				bootloader_app_can_tx_task_v(can_received_msg_id_u32,can_data_length_u8, &debug_comm_can_rx_local_buff_au8[0]);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
#endif
			/**********************************************************************************************************************/
#if DEBUG_COMM_CURR_CALIB
			case DEBUG_COMM_CURRENT_CALIB_ST_ACK_STATE:
			{
				current_calib_process_v(CURRENT_CALIB_START_ACK_STATE);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_CURRENT_CALIB_SET_ACK_STATE:
			{
				current_calib_process_v(CURRENT_CALIB_SET_ACK_STATE);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_CURRENT_CALIB_COMPUTE_ACK_STATE:
			{
				current_calib_process_v(CURRENT_CALIB_COMPUTE_ACK_STATE);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
#endif
			/**********************************************************************************************************************/
			case DEBUG_COMM_MULTIPACK_UPDATE_STATE:
			{
				debug_comm_j1939_multipacket_can_msg_update_prv();
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_MULTIPACK_SEND_STATE:
			{
				debug_comm_j1939_multipacket_can_msg_send_pru8();
			}
			break;
			/**********************************************************************************************************************/
			case DEBUG_COMM_MULTIPACK_EOF_MSG_STATE:
			{
				debug_comm_j1939_eof_msg_handling_prv();
			}
			break;
			/**********************************************************************************************************************/
			default :
			{

			}
			break;
			/**********************************************************************************************************************/
		}
	}
}

#if 0
void debug_comm_tx_msg_v()
{
	if(1 == debug_comm_transmit_gb)
	{
		/* Timer check condition */
		/* if(debug_comm_1sec_tmr_gu8 > 0) */  /* TODO: Should uncomment this when RTOS is not used */
		{
			/* Resets the timer flag */
			debug_comm_1sec_tmr_gu8 = 0;

			#if DEBUG_COMM_SEND_CELL_VTG
			/* Sends cell voltages */
			debug_comm_send_cell_vtg_prv();
			#endif

			#if DEBUG_COMM_SEND_CELL_TEMP
			/* Sends cells Temperature */
			debug_comm_send_cell_temp_prv();
			#endif

			#if DEBUG_COMM_SEND_HVADC_BENDER
			/* Sends HVADC measurements */
			debug_comm_send_hvadc_bend_prv();
			#endif

			#if DEBUG_COMM_SEND_MCU_ADC
			/* Sends mcu adc measurements */
			debug_comm_send_mcu_adc_prv();
			#endif

			#if DEBUG_COMM_SEND_CURR_SBC
			/* Sends current and sbc measurements */
			debug_comm_send_curr_sbc_data_prv();
			#endif

			#if DEBUG_COMM_SEND_TMS
			/* Sends TMS(Thermal Management System) measurements */
			debug_comm_send_tms_prv();
			#endif

			#if DEBUG_COMM_SEND_MOISTURE_IMU
			/* Sends Moisture(Humidity & Temperature) sensor measurements */
			debug_comm_send_imu_mois_data_prv();
			#endif

			#if DEBUG_COMM_SEND_RTC_AMB_LIGHT
			/* Sends Ambient Light sensor measurements */
			debug_comm_send_rtc_amb_lght_prv();
			#endif

			#if DEBUG_COMM_SEND_SYS_FLT_STS
			/* Sends system fault status */
			debug_comm_send_sys_flt_sts_prv();
			#endif

			#if DEBUG_COMM_SEND_AFE_SLV_FLT_STS
			/* Sends afe slave fault status */
			debug_comm_send_afe_slave_flt_sts_prv();
			#endif

			#if DEBUG_COMM_SEND_AFE_CB_STS
			/* Sends AFE Cell Balancing status */
			debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_ALL);
			#endif

			#if DEBUG_COMM_SEND_MAX_MIN_CPARAMS
			/* Sends Max Min Cell parameters */
			debug_comm_send_max_min_cparams_prv();
			#endif
		}
	}
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_ack_prv()
{

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_update_cell_vtg_prv()
{
	U8 current_slv_num_u8;			/* Current Slave number   */
	U8 current_cell_num_u8;			/* Current channel number */

	/* Initializing the variables to start with zero*/
	U8 cell_vtg_idx_u8 = 0;				/* Cell Voltage index     */

	for (current_slv_num_u8 = 0; current_slv_num_u8 < DEBUG_COMM_TOTAL_SLAVES; current_slv_num_u8++)
	{
		for (current_cell_num_u8 = 0;current_cell_num_u8 < DEBUG_COMM_TOTAL_CELLS_PER_SLV;current_cell_num_u8++)
		{
			#if DEBUG_COMM_HARD_CODE
			debug_comm_cell_vtg_1cv_gau16[cell_vtg_idx_u8] = 0x10C + cell_vtg_idx_u8; /* 268 cV -> 2680mV */
			#else
			if(cell_vtg_idx_u8 > 13)
			{
				debug_comm_cell_vtg_1cv_gau16[cell_vtg_idx_u8] = 0;//batt_param_cell_param_gst[current_slv_num_u8][current_cell_num_u8].cell_meas_1mV_u16;

			}
			else
			{
				debug_comm_cell_vtg_1cv_gau16[cell_vtg_idx_u8] = batt_param_cell_param_gst[current_slv_num_u8][current_cell_num_u8].cell_meas_1mV_u16;

			}

			#endif



			cell_vtg_idx_u8 ++;
		}
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_cell_vtg_prv()
{
	/* Initializing the variables to start with zero*/
	U8 cell_vtg_index_u8 = 0;
	U8 tx_msg_index_u8 = 0;
	U8 cell_msg_index_u8 = 0;
	U8 current_sending_msg_u8 = 0;
	/* Initializing the variable to start with one because msg count is not less than zero*/
	BOOL msg_send_again_b = 1;
	U8 curr_id_u8 = 0;

	/* Constant for this message - voltage param*/
	const U8 cell_max_per_msg_cu8 = 4;

	/* Always DLC -8*/
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Sending total messages that needs to send - Cell voltage*/
	while(msg_send_again_b)
	{
		/* Initializing & Incrementing with Cell voltage response ID according to protocol */
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_CELL_VOLTAGE_RES) + current_sending_msg_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		for(cell_msg_index_u8 = 0; ((cell_vtg_index_u8 < DEBUG_COMM_TOTAL_CELLS) &&  (cell_msg_index_u8 < cell_max_per_msg_cu8)); cell_vtg_index_u8++ , cell_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_cell_vtg_1cv_gau16[cell_vtg_index_u8] & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_cell_vtg_1cv_gau16[cell_vtg_index_u8] >> 8) & 0xFF);
		}

		/* Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	    }

		if(DEBUG_COMM_TOTAL_CELLS == cell_vtg_index_u8)
		{
			msg_send_again_b = 0;
		}

		/* Resetting the values so that it can start storing form 0th index for next CAN message to send*/
		tx_msg_index_u8 = 0;
		/* Incrementing the loop variable for moving to next CAN ID as per protocol */
		current_sending_msg_u8++;

		/* CAN transmit message */
		/* TODO: Need to use can_app_frame_and_tx_msg_u8 instead of can_app_transmit_data_u8
		can_app_frame_and_tx_msg_u8 DEBUG_COMM_FRAME(pgn); */
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

	}


}
/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_cell_temp_prv()
{
	U8 cur_slv_num_u8; /* Current Slave number   */
	U8 cur_temp_idx_u8; /* Current channel number */

	/* Initializing the variables to start with zero*/
	U8 temp_idx_u8 = 0; /* Cell temp index     */

	for (cur_slv_num_u8 = 0; cur_slv_num_u8 < DEBUG_COMM_TOTAL_SLAVES; cur_slv_num_u8++)
	{
		for (cur_temp_idx_u8 = 0; cur_temp_idx_u8 < (BATT_PARAM_CELL_TEMP_ID_MAX); cur_temp_idx_u8++)
		{//batt_param_pack_param_gst.pack_param_tot_therm_per_slv_au8[cur_slv_num_u8]
			#if DEBUG_COMM_HARD_CODE
			debug_comm_cell_temp_1cc_gau16[temp_idx_u8] =  0xD48 + temp_idx_u8; /* 3400 cc = 34degC */
			#else
			debug_comm_cell_temp_1cc_gau16[cur_temp_idx_u8] =
									 batt_param_temp_param_gst[cur_slv_num_u8][cur_temp_idx_u8].temp_meas_1cc_s16;
			//if(cur_temp_idx_u8 == 3)
				//debug_comm_cell_temp_1cc_gau16[cur_temp_idx_u8] = batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[ 0];

			#endif

			//cur_temp_idx_u8++;
		}
	}
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_cell_temp_prv()
{
	/* Initializing the variables to start with zero*/
	U8 temp_index_u8 = 0;
	U8 cell_msg_index_u8 = 0;
	U8 tx_msg_index_u8 = 0;
	U8 current_sending_msg_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing the variable to start with one because msg count is not less than zero*/
	BOOL msg_send_again_b = 1;

	/* Constant for this message - temperature param*/
	const U8 cell_max_per_msg_cu8 = 4;

	/* Always DLC -8*/
	debug_comm_tx_msg_st.length_u8 = 8;
	/* Sending total messages that needs to send - Cell temperature */
	while(msg_send_again_b)
	{
		/* Initializing & Incrementing with Cell temperature response ID according to protocol */
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_CELL_TEMP_RES) + current_sending_msg_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);
		//DEBUG_COMM_TOTAL_CELL_TEMP_SENS
		for(cell_msg_index_u8 = 0; ((temp_index_u8 < 10) &&  (cell_msg_index_u8 < cell_max_per_msg_cu8)); temp_index_u8++ , cell_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_cell_temp_1cc_gau16[temp_index_u8] & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_cell_temp_1cc_gau16[temp_index_u8] >> 8) & 0xFF);
		}

/*		 Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF;  //Reserved byte
	    }
//DEBUG_COMM_TOTAL_CELL_TEMP_SENS
		if(10 == temp_index_u8)
		{
			msg_send_again_b = 0;
		}

		/* Resetting the values so that it can start storing form 0th index for next CAN message to send*/
		tx_msg_index_u8 = 0;
		/* Incrementing the loop variable for moving to next CAN ID as per protocol */
		current_sending_msg_u8++;

		/* CAN transmit message */
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	}

}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_hvadc_prv()
{

	debug_comm_hvadc_param_gst.ld_vtg_1cv_u16 	= 0x0000; /* reserved value when not needed -> 0x00 */
	debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 = 0x0000; /* reserved value when not needed -> 0x00 */
	debug_comm_hvadc_param_gst.pcon_vtg_1cv_u16 = 0x0000; /* reserved value when not needed -> 0x00 */
	#if DEBUG_COMM_HARD_CODE
	/* Hardcoded values*/
	debug_comm_hvadc_param_gst.pack_vtg_1cv_u16 = 0x1C1C; /* 7196 cV -> 71960mV */
	debug_comm_hvadc_param_gst.fuse_vtg_1cv_u16 = 0x1C1E; /* 7198 cV -> 71980mV */

		#if DEBUG_COMM_HVADC_PCON_VTG
		debug_comm_hvadc_param_gst.pcon_vtg_1cv_u16 = 0x1C09; /* 7177cV -> 7177cV */
		#elif DEBUG_COMM_HVADC_MFT_VTG
		debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 = 0x1C0A; /* 7178cV -> 7178cV */
		#elif DEBUG_COMM_HVADC_LD_VTG
		debug_comm_hvadc_param_gst.ld_vtg_1cv_u16	= 0x1C0B; /* 7179cV -> 7179cV */
		#endif

	#else
	debug_comm_hvadc_param_gst.pack_vtg_1cv_u16 = batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16;
	debug_comm_hvadc_param_gst.fuse_vtg_1cv_u16 = batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16;
		#if DEBUG_COMM_HVADC_PCON_VTG
		debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 = 0x0000; /* reserved value when not needed -> 0x00 */
		debug_comm_hvadc_param_gst.pcon_vtg_1cv_u16 = batt_param_pack_param_gst.pack_param_hvadc_poscon_vtg_1cv_u16 / 10;
		#elif DEBUG_COMM_HVADC_MFT_VTG
		debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 = batt_param_pack_param_gst.pack_param_hvadc_mfet_vtg_1cv_u16;
		#elif DEBUG_COMM_HVADC_LD_VTG
		debug_comm_hvadc_param_gst.ld_vtg_1cv_u16	= batt_param_pack_param_gst.pack_param_ld_det_vtg_1cV_u16;
		#endif

		debug_comm_hvadc_param_gst.afe_pack_vtg_1cv_u16 = batt_param_pack_param_gst.pack_param_1cV_u16;

	#endif



}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_bend_prv()
{

	/* updating bender board data*/
	#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_BENDER)
	debug_comm_bender_data_gu16 = 0x5678; /* Decimal -> 7343*/
	#elif(DEBUG_COMM_SEND_BENDER)
	/* TODO: Later assign bender board data in the place of 0xFFFF */
	debug_comm_bender_data_gu16 = 0xFFFF;
	#else
	debug_comm_bender_data_gu16 = 0x0000; /* reserved value when not needed -> 0x00 */
	#endif


}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_hvadc_bend_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;

	/* Only one message sent for hvadc */

	/* Initializing with hvadc and Bender board response ID and length*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_HVADC_BEND_MEAS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Filling data - HV-ADC*/
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_hvadc_param_gst.pack_vtg_1cv_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_hvadc_param_gst.pack_vtg_1cv_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_hvadc_param_gst.fuse_vtg_1cv_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_hvadc_param_gst.fuse_vtg_1cv_u16 >> 8) & 0xFF);

	#if DEBUG_COMM_HVADC_PCON_VTG
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_hvadc_param_gst.pcon_vtg_1cv_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_hvadc_param_gst.pcon_vtg_1cv_u16 >> 8) & 0xFF);
	#elif DEBUG_COMM_HVADC_MFT_VTG
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_hvadc_param_gst.mfet_vtg_1cv_u16 >> 8) & 0xFF);
	#elif DEBUG_COMM_HVADC_LD_VTG
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_hvadc_param_gst.ld_vtg_1cv_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_hvadc_param_gst.ld_vtg_1cv_u16 >> 8) & 0xFF);
	#endif

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_hvadc_param_gst.afe_pack_vtg_1cv_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_hvadc_param_gst.afe_pack_vtg_1cv_u16 >> 8) & 0xFF);

	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_mcu_adc_prv()
{
		#if DEBUG_COMM_MCU_ADC_A
		/* Not for robin*/
		#if DEBUG_COMM_HARD_CODE
		debug_comm_mcu_adc_param_gst.pchg_vtg_1cv_u16 =  0x10C; /* 268 cV -> 2680mV */
		debug_comm_mcu_adc_param_gst.pcon_vtg_1cv_u16 =  0x10C; /* 268 cV -> 2680mV */
		debug_comm_mcu_adc_param_gst.ncon_vtg_1cv_u16 =  0x10C; /* 268 cV -> 2680mV */
		debug_comm_mcu_adc_param_gst.sbc_mux_out_u16  =  0xFFFF; /* TODO : To know what kind of value to hard-code*/
		#else	/* TODO : All below to be done when work with Falcon*/
		debug_comm_mcu_adc_param_gst.pchg_vtg_1cv_u16;
		debug_comm_mcu_adc_param_gst.pcon_vtg_1cv_u16;
		debug_comm_mcu_adc_param_gst.ncon_vtg_1cv_u16;
		debug_comm_mcu_adc_param_gst.sbc_mux_out_u16;
		#endif
		#endif

		#if DEBUG_COMM_MCU_ADC_B
		U8 cur_slv_num_u8; 			/* Current slave number */
		U8 cur_ct_idx_u8; 			/* Current thermistor used in cells*/ /* ct -> cell temperature*/
		U8 cur_temp_idx_u8; 		/* Current thermistor used in mosfets/Pack*/ /* mfet -> mosfet temperature*/

		/* Initializing the variables to start with zero*/
		U8 ntc_temp_idx_u8 = 0; /* ntc temp index     */

		for (cur_slv_num_u8 = 0; cur_slv_num_u8 < DEBUG_COMM_TOTAL_SLAVES; cur_slv_num_u8++)
		{
			for (cur_ct_idx_u8 = 0; cur_ct_idx_u8 < batt_param_pack_param_gst.pack_param_tot_therm_per_slv_au8[cur_slv_num_u8]; cur_ct_idx_u8++)
			{
				#if DEBUG_COMM_HARD_CODE
				debug_comm_cell_temp_1cc_gau16[ntc_temp_idx_u8++] =  0x2B5; /* 693 cc */
				#else
				debug_comm_cell_temp_1cc_gau16[ntc_temp_idx_u8++] =
										 batt_param_temp_param_gst[cur_slv_num_u8][cur_ct_idx_u8].temp_meas_1cc_s16;
				#endif
			}
		}

		/* Mosfet Temperature*/
		for (cur_temp_idx_u8 = 0; cur_temp_idx_u8 < BATT_PARAM_MFET_TEMP_ID_MAX ; cur_temp_idx_u8++)
		{
			#if DEBUG_COMM_HARD_CODE
			debug_comm_ntc_temp_1cc_gau16[ntc_temp_idx_u8++] = 0xFF64; /* -156 -> -15.6 degC */
			#else
			debug_comm_ntc_temp_1cc_gau16[ntc_temp_idx_u8++] = batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[cur_temp_idx_u8];
			#endif
		}

		/* Pack Temperature*/
//		for (cur_temp_idx_u8 = 0; cur_temp_idx_u8 < BATT_PARAM_PACK_TEMP_ID_MAX ; cur_mfet_idx_u8++)
//		{
			#if DEBUG_COMM_HARD_CODE
			debug_comm_ntc_temp_1cc_gau16[ntc_temp_idx_u8++] = 0xFF64; /* -156 -> -15.6 degC */
			#else
			debug_comm_ntc_temp_1cc_gau16[ntc_temp_idx_u8++] = batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16;
			#endif
//		}

		#endif
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_mcu_adc_prv()
{

	#if DEBUG_COMM_MCU_ADC_A

		/* Yet to be defined*/
		/* Initializing the variables to start with zero*/
		U8 tx_msg_index_u8 = 0;

		/* Only one message sent for mcu-adc-A */

		/* Initializing with MCU ADC requested ID*/
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MCU_ADC_A_RES;
		debug_comm_tx_msg_st.length_u8 = 8;

		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_mcu_adc_param_gst.pchg_vtg_1cv_u16 & 0xFF);
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.pchg_vtg_1cv_u16 >> 8) & 0xFF);

		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_mcu_adc_param_gst.pcon_vtg_1cv_u16 & 0xFF);
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.pcon_vtg_1cv_u16 >> 8) & 0xFF);

		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_mcu_adc_param_gst.ncon_vtg_1cv_u16 & 0xFF);
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.ncon_vtg_1cv_u16 >> 8) & 0xFF);

		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_mcu_adc_param_gst.sbc_mux_out_u16 & 0xFF);
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.sbc_mux_out_u16 >> 8) & 0xFF);

		/* CAN transmit message 1*/
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

	#endif

	#if DEBUG_COMM_MCU_ADC_B

		/* Initializing the variables to start with zero*/
		U8 ntc_msg_idx_u8 = 0;
		U8 tx_msg_index_u8 = 0;
		U8 temp_index_u8 = 0;
		U8 current_sending_msg_u8 = 0;
		U8 curr_id_u8 = 0;

		/* Initializing the variable to start with one because msg count is not less than zero*/
		BOOL msg_send_again_b = 1;

		/* Constant for this message - MCU ADC- B param*/
		const U8 ntc_max_per_msg_cu8 = 4;

		/* Always DLC -8*/
		debug_comm_tx_msg_st.length_u8 = 8;
		debug_comm_tx_msg_st.data_au8[0] = (batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[ 0] & 0xFF);
		debug_comm_tx_msg_st.data_au8[1] = ((batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[ 0] >> 8) & 0xFF);
		debug_comm_tx_msg_st.data_au8[2] = (batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16 & 0xFF);
	    debug_comm_tx_msg_st.data_au8[3] = ((batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16 >> 8) & 0xFF);
		debug_comm_tx_msg_st.data_au8[4] = (batt_param_pack_param_gst.mcu_internal_die_temp_s16 & 0xFF);
		debug_comm_tx_msg_st.data_au8[5] = ((batt_param_pack_param_gst.mcu_internal_die_temp_s16 >> 8) & 0xFF);
		debug_comm_tx_msg_st.data_au8[6] = (batt_param_mod_param_gst[0].mod_die_T_S16 & 0xFF);
	    debug_comm_tx_msg_st.data_au8[7] = ((batt_param_mod_param_gst[0].mod_die_T_S16 >> 8) & 0xFF);
		/* CAN transmit message */
	    /* Initializing & Incrementing with MCU -ADC- B response ID according to protocol */
	    curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_MCU_ADC_B_RES) + current_sending_msg_u8;
	    debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);
        can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

#if 0
		/* Sending total messages that needs to send - MCU -ADC - B */
		while(msg_send_again_b)
		{
			/* Initializing & Incrementing with MCU -ADC- B response ID according to protocol */
			curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_MCU_ADC_B_RES) + current_sending_msg_u8;
			debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

			for(ntc_msg_idx_u8 = 0; ((temp_index_u8 < DEBUG_COMM_TOTAL_NTC_TEMP_SENS) &&  (ntc_msg_idx_u8 < ntc_max_per_msg_cu8)); temp_index_u8++ , ntc_msg_idx_u8++)
			{
				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_ntc_temp_1cc_gau16[temp_index_u8] & 0xFF);
				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_ntc_temp_1cc_gau16[temp_index_u8] >> 8) & 0xFF);
			}

			/* Assigning reserved values upto last byte of particular message */
			while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
			{
				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
		    }

			if(DEBUG_COMM_TOTAL_NTC_TEMP_SENS == temp_index_u8)
			{
				msg_send_again_b = 0;
			}

			/* Resetting the values so that it can start storing form 0th index for next CAN message to send*/
			tx_msg_index_u8 = 0;
			/* Incrementing the loop variable for moving to next CAN ID as per protocol */
			current_sending_msg_u8++;

			/* CAN transmit message */
			can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
		}
#endif
	#endif

}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_curr_sbc_data_prv()
{

	/* TODO: Later assign vbat sens data  and  in the place of 0xFFFF value */
	debug_comm_mcu_adc_param_gst.vbat_sens_u16 = 0xFFFF;

	#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_CURRENT)
		debug_comm_mcu_adc_param_gst.current_u32 = 0x1362; /* 4962mA -> 4.962A */
	#elif(DEBUG_COMM_SEND_CURRENT)
		debug_comm_mcu_adc_param_gst.current_u32 = batt_param_pack_param_gst.pack_param_1mA_s32 ;
	#else
		debug_comm_mcu_adc_param_gst.current_u32 = batt_param_pack_param_gst.pack_param_1mA_s32 ;
		//0x00000000;
	#endif


	#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_SBC)
		debug_comm_sbc_data_gu16 = 0x1CAF ; /* Decimal -> 7343 */
	#elif(DEBUG_COMM_SEND_SBC)
		/* TODO: Later assign sbc data  and  in the place of 0xFFFF value */
		debug_comm_sbc_data_gu16 = 0xFFFF;
	#else
		debug_comm_sbc_data_gu16 = 0x0000; /* reserved value when not needed -> 0x00 */
	#endif

}
/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_send_curr_sbc_data_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;

	/* Only one message sent for Current and SBC data */

	/* Initializing with SBC  data response ID*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_CURR_SBC_RES;

	/* Always DLC -8*/
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Sending total messages that needs to send */
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_mcu_adc_param_gst.vbat_sens_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.vbat_sens_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_mcu_adc_param_gst.current_u32 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.current_u32 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.current_u32 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_mcu_adc_param_gst.current_u32 >> 24) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sbc_data_gu16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sbc_data_gu16 >> 8) & 0xFF);

	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_update_tms_prv()
{
	U8 current_chn_num_u8;			/* Current Slave number   */

	for (current_chn_num_u8 = 0; current_chn_num_u8 < DEBUG_COMM_TOTAL_TMS_CHANNELS; current_chn_num_u8++)
	{
		#if DEBUG_COMM_HARD_CODE
			debug_comm_tms_data_gau16[current_chn_num_u8] = 0x2B5 + current_chn_num_u8; /* Decimal -> 693cc */
		#else
			/* TODO assign batt_param value*/
			debug_comm_tms_data_gau16[current_chn_num_u8] = 0xFFFF; /* Reserved bytes*/
		#endif

	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_tms_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;
	U8 tms_msg_index_u8 = 0;
	U8 chn_index_u8 = 0;
	U8 current_sending_msg_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing the variable to start with one because msg count is not less than zero*/
	BOOL msg_send_again_b = 1;

	/* Constant for this message - TMS param*/
	const U8 chn_max_per_msg_cu8 = 4;

	/* Always DLC -8*/
	debug_comm_tx_msg_st.length_u8 = 8;
	/* Sending total messages that needs to send - TMS */
	while(msg_send_again_b)
	{
		/* Initializing & Incrementing with TMS response ID according to protocol */
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_TMS_TEMP_RES) + current_sending_msg_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		for(tms_msg_index_u8 = 0; ((chn_index_u8 < DEBUG_COMM_TOTAL_TMS_CHANNELS) &&  (tms_msg_index_u8 < chn_max_per_msg_cu8)); chn_index_u8++ , tms_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_tms_data_gau16[chn_index_u8] & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_tms_data_gau16[chn_index_u8] >> 8) & 0xFF);
		}

		/* Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	    }

		if(DEBUG_COMM_TOTAL_TMS_CHANNELS == chn_index_u8)
		{
			msg_send_again_b = 0;
		}

		/* Resetting the values so that it can start storing form 0th index for next CAN message to send*/
		tx_msg_index_u8 = 0;
		/* Incrementing the loop variable for moving to next CAN ID as per protocol */
		current_sending_msg_u8++;

		/* CAN transmit message */
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	}
}


/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_sys_flt_sts_prv()
{

	#if DEBUG_COMM_HARD_CODE
	/* TODO assign hardcode values based on assumed faults*/
	debug_comm_sys_flt_sts_gst.init_fault_sts_u32 = 0x00000000;
	debug_comm_sys_flt_sts_gst.os_init_fault_sts_u8 = 0x00;
	debug_comm_sys_flt_sts_gst.app_init_fault_sts_u16 = 0x0000;
	debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u32 = 0x00000000;
	#else
	debug_comm_sys_flt_sts_gst.init_fault_sts_u32 = (app_main_system_flt_status_u64 & 0xFFFFFFFF);
	debug_comm_sys_flt_sts_gst.os_init_fault_sts_u8 = ((app_main_system_flt_status_u64 >> 32) & 0xFF);
	debug_comm_sys_flt_sts_gst.app_init_fault_sts_u16 = ((app_main_system_flt_status_u64 >> 40) & 0xFF);
	debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 = system_diag_app_runtime_flt_code_gu64;//((app_main_system_flt_status_u64 >> 48) & 0xFF);
	debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 = system_diag_app_runtime_prev_flt_gu64;
	#endif


}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_send_sys_flt_sts_prv()
{
	U8 tx_msg_index_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_SYS_FLT_STS_RES) + 1;
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

	debug_comm_tx_msg_st.length_u8 = 8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 24) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U64)((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 32) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((U64)(debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 40) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((U64)(debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 48) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((U64)(debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u64 >> 56) & 0xFF);

	/* CAN transmit message2 */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	#if 0
	/* Sending total messages that needs to send */
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_SYS_FLT_STS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sys_flt_sts_gst.init_fault_sts_u32 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.init_fault_sts_u32 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.init_fault_sts_u32 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.init_fault_sts_u32 >> 24) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sys_flt_sts_gst.os_init_fault_sts_u8 & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sys_flt_sts_gst.app_init_fault_sts_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.app_init_fault_sts_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	/* CAN transmit message1 */
	can_app_transmit_data_u8(CAN_DISO, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_SYS_FLT_STS_RES) + 1;
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

	debug_comm_tx_msg_st.length_u8 = 8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u32 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u32 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u32 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_runtime_fault_sts_u32 >> 24) & 0xFF);

	/* Assigning reserved values upto last byte of particular message */
	while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
	{
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	}

	/* CAN transmit message2 */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	#endif
}

static void debug_comm_send_prv_sys_flt_sts_prv()
{
	U8 tx_msg_index_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MCU_ADC_A_RES;

	debug_comm_tx_msg_st.length_u8 = 8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 24) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U64)((debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 32) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((U64)(debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 40) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((U64)(debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 48) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((U64)(debug_comm_sys_flt_sts_gst.sys_prv_runtime_fault_sts_u64 >> 56) & 0xFF);

	/* CAN transmit message2 */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
}
/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_set_bin_prv(debug_comm_bin_msg_te debug_comm_bin_msg_are)
{
	U8 loop_var_u8;
	static BOOL bin_msg1_set_sb = 0;
	static BOOL bin_msg2_set_sb = 0;
	static BOOL bin_msg3_set_sb = 0;

	if(BIN_MSG_1 == debug_comm_bin_msg_are)
	{
		bin_msg1_set_sb = 1;
	}
	else if(BIN_MSG_2 == debug_comm_bin_msg_are)
	{
		bin_msg2_set_sb = 1;
	}
	else if(BIN_MSG_3 == debug_comm_bin_msg_are)
	{
		bin_msg3_set_sb = 1;
	}

	if(bin_msg1_set_sb && bin_msg2_set_sb && bin_msg3_set_sb)
	{
		memory_copy_u8_array_v(debug_comm_bin_gau8, debug_comm_temp_bin_au8, DEBUG_COMM_BIN_LEN);

#if DEBUG_COMM_SEND_FLASH /* TODO Flash*/
		debug_com_update_batt_data_st.start_addr_u32					 = STORAGE_DEF_START_ADDRESS_VIN_UFD_LOG;
		debug_com_update_batt_data_st.end_addr_u32   					 = STORAGE_DEF_END_ADDRESS_VIN_UFD_LOG;
		debug_com_update_batt_data_st.tot_size_u8    					 = sizeof(common_var_bms_id_log_tst);
		debug_com_update_batt_data_st.particular_param_size_to_change_u8 = DEBUG_COMM_BIN_LEN;
		debug_com_update_batt_data_st.particular_param_addr_to_change_pv = &common_var_bms_id_log_gst.bin_au8;
		debug_com_update_batt_data_st.bms_batt_upd_log_pv			     = &common_var_bms_id_log_gst;
		debug_com_update_batt_data_st.bms_data_to_change_pv				 = &debug_comm_bin_gau8[0];

		/* Checking whether any previous flash operation is completed */
		if(FLASH_DRV_OPERATION_COMPLETED == flash_drv_flash_operation_sts_u8)
		{
			debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BIN_ACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;

			debug_comm_ext_flash_queue_st.mode_e 	 = OS_FLASH_RD_AND_SECTOR_ERASE;
			debug_comm_ext_flash_queue_st.event_e 	 = OS_STORAGE_UPDATE_BATT_DATA;
			debug_comm_ext_flash_queue_st.payload_pv = &debug_com_update_batt_data_st;
			xQueueSend(os_storage_t7_qhandler_ge, &debug_comm_ext_flash_queue_st, 2);
		}
		/* Checking whether any previous flash operation is in process */
		else if (FLASH_DRV_OPERATION_INPROGRESS == flash_drv_flash_operation_sts_u8)
		{
			debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BIN_ACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

		}
		/* if external flash has fault */
		else if(FLASH_DRV_OPERATION_FAILED == flash_drv_flash_operation_sts_u8)
		{
			debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_BIN_ACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
#endif

		#if 0
		/* Set bin number to the battery global variable */
		for (loop_var_u8 = 0; loop_var_u8 < DEBUG_COMM_BIN_LEN ; loop_var_u8++)
		{
			common_var_bms_id_log_gst.bin_au8[loop_var_u8] = debug_comm_bin_gau8[loop_var_u8];
		}

		/* reset the msg set variables*/
		bin_msg1_set_sb = 0;
		bin_msg2_set_sb = 0;
		bin_msg3_set_sb = 0;

		debug_comm_send_bin_prv();
		#endif
	}

}
/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_set_vin_prv(debug_comm_vin_msg_te debug_comm_vin_msg_are)
{
	U8 loop_var_u8;
	static BOOL vin_msg1_set_sb = 0;
	static BOOL vin_msg2_set_sb = 0;
	static BOOL vin_msg3_set_sb = 0;

	if(VIN_MSG_1 == debug_comm_vin_msg_are)
	{
		vin_msg1_set_sb = 1;
	}
	else if(VIN_MSG_2 == debug_comm_vin_msg_are)
	{
		vin_msg2_set_sb = 1;
	}
	else if(VIN_MSG_3 == debug_comm_vin_msg_are)
	{
		vin_msg3_set_sb = 1;
	}

	if(vin_msg1_set_sb && vin_msg2_set_sb && vin_msg3_set_sb)
	{
		memory_copy_u8_array_v(debug_comm_vin_gau8, debug_comm_temp_vin_au8, DEBUG_COMM_VIN_LEN);

#if DEBUG_COMM_SEND_FLASH /* TODO Flash*/
		/* Set vin number to the battery global variable */
		debug_com_update_batt_data_st.start_addr_u32					 = STORAGE_DEF_START_ADDRESS_VIN_UFD_LOG;
		debug_com_update_batt_data_st.end_addr_u32   					 = STORAGE_DEF_END_ADDRESS_VIN_UFD_LOG;
		debug_com_update_batt_data_st.tot_size_u8    					 = sizeof(common_var_bms_id_log_tst);
		debug_com_update_batt_data_st.particular_param_size_to_change_u8 = DEBUG_COMM_VIN_LEN;
		debug_com_update_batt_data_st.particular_param_addr_to_change_pv = &common_var_bms_id_log_gst.vin_au8;
		debug_com_update_batt_data_st.bms_batt_upd_log_pv			     = &common_var_bms_id_log_gst;
		debug_com_update_batt_data_st.bms_data_to_change_pv				 = &debug_comm_vin_gau8[0];

		/* Checking whether any previous flash operation is completed */
		if(FLASH_DRV_OPERATION_COMPLETED == flash_drv_flash_operation_sts_u8)
		{
			debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_VIN_ACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;

			debug_comm_ext_flash_queue_st.mode_e 	 = OS_FLASH_RD_AND_SECTOR_ERASE;
			debug_comm_ext_flash_queue_st.event_e 	 = OS_STORAGE_UPDATE_BATT_DATA;
			debug_comm_ext_flash_queue_st.payload_pv = &debug_com_update_batt_data_st;
			xQueueSend(os_storage_t7_qhandler_ge, &debug_comm_ext_flash_queue_st, 2);
		}
		/* Checking whether any previous flash operation is in process */
		else if (FLASH_DRV_OPERATION_INPROGRESS == flash_drv_flash_operation_sts_u8)
		{
			debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_VIN_ACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

		}
		/* if external flash has fault */
		else if(FLASH_DRV_OPERATION_FAILED == flash_drv_flash_operation_sts_u8)
		{
			debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_VIN_ACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
#endif

		#if 0
		for (loop_var_u8 = 0; loop_var_u8 < DEBUG_COMM_VIN_LEN ; loop_var_u8++)
		{
			common_var_bms_id_log_gst.vin_au8[loop_var_u8] = debug_comm_vin_gau8[loop_var_u8];
		}

		/* reset the msg set variables*/
		vin_msg1_set_sb = 0;
		vin_msg2_set_sb = 0;
		vin_msg3_set_sb = 0;

		debug_comm_send_vin_prv();
		#endif
	}
}
/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_set_mac_prv()
{
#if DEBUG_COMM_SEND_FLASH //TODO

	debug_com_update_batt_data_st.start_addr_u32					 = STORAGE_DEF_START_ADDRESS_VIN_UFD_LOG;
	debug_com_update_batt_data_st.end_addr_u32   					 = STORAGE_DEF_END_ADDRESS_VIN_UFD_LOG;
	debug_com_update_batt_data_st.tot_size_u8    					 = sizeof(common_var_bms_id_log_tst);
	debug_com_update_batt_data_st.particular_param_size_to_change_u8 = DEBUG_COMM_MAC_LEN;
	debug_com_update_batt_data_st.particular_param_addr_to_change_pv = &common_var_bms_id_log_gst.mac_au8;
	debug_com_update_batt_data_st.bms_batt_upd_log_pv			     = &common_var_bms_id_log_gst;
	debug_com_update_batt_data_st.bms_data_to_change_pv				 = &debug_comm_mac_gau8[0];

	/* Checking whether any previous flash operation is completed */
	if(FLASH_DRV_OPERATION_COMPLETED == flash_drv_flash_operation_sts_u8)
	{
		debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MAC_ACK_SEND_STATE;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;

		debug_comm_ext_flash_queue_st.mode_e 	 = OS_FLASH_RD_AND_SECTOR_ERASE;
		debug_comm_ext_flash_queue_st.event_e 	 = OS_STORAGE_UPDATE_BATT_DATA;
		debug_comm_ext_flash_queue_st.payload_pv = &debug_com_update_batt_data_st;
		xQueueSend(os_storage_t7_qhandler_ge, &debug_comm_ext_flash_queue_st, 2);
	}
	/* Checking whether any previous flash operation is in process */
	else if (FLASH_DRV_OPERATION_INPROGRESS == flash_drv_flash_operation_sts_u8)
	{
		debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MAC_ACK_SEND_STATE;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
	}
	/* if external flash has fault */
	else if(FLASH_DRV_OPERATION_FAILED == flash_drv_flash_operation_sts_u8)
	{
		debug_comm_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MAC_ACK_SEND_STATE;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
	}
#endif
}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_send_bin_prv()
{
	U8* temp_pu8;
	U8 msg_len_u8;
	U8 tx_msg_index_u8 = 0;
	U8 msg_len_remaining_u8;
	U8 loop_var_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	temp_pu8 = debug_comm_bin_gau8;
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_BIN_RES;
	debug_comm_tx_msg_st.length_u8 = 8;
	msg_len_u8 = DEBUG_COMM_BIN_LEN / DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD;
	msg_len_remaining_u8 = DEBUG_COMM_BIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD;

	/* Algorithm for multi-packet (especially when more than 2 packets)*/


	while(0 < msg_len_u8)
	{
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_BIN_RES) + loop_var_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		memory_copy_u8_array_v(debug_comm_tx_msg_st.data_au8, temp_pu8, DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
		temp_pu8 += DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD;
		msg_len_u8--;
		loop_var_u8++;
	}

	/* If remaining data present - non-full can msg*/
	if(0 != msg_len_remaining_u8)
	{
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_BIN_RES) + loop_var_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		memory_copy_u8_array_v(debug_comm_tx_msg_st.data_au8, temp_pu8 , msg_len_remaining_u8);
		tx_msg_index_u8 = msg_len_remaining_u8;

		/* Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
		}
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
		loop_var_u8++;

	}


}
/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_send_vin_prv()
{
	U8* temp_pu8;
	U8 msg_len_u8;
	U8 tx_msg_index_u8;
	U8 msg_len_remaining_u8;
	U8 loop_var_u8;
	U8 curr_id_u8 = 0;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;
	temp_pu8 = debug_comm_vin_gau8;
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_VIN_RES;
	debug_comm_tx_msg_st.length_u8 = 8;
	msg_len_u8 = DEBUG_COMM_VIN_LEN / DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD;
	msg_len_remaining_u8 = DEBUG_COMM_VIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD;

	/* Algorithm for multi-packet (especially when more than 2 packets)*/


	while(0 < msg_len_u8)
	{
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_VIN_RES) + loop_var_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		memory_copy_u8_array_v(debug_comm_tx_msg_st.data_au8, temp_pu8, DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
		temp_pu8 += DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD;
		msg_len_u8--;

		loop_var_u8++;
	}

	/* If remaining data present - non-full can msg*/
	if(0 != msg_len_remaining_u8)
	{
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_VIN_RES) + loop_var_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		memory_copy_u8_array_v(debug_comm_tx_msg_st.data_au8, temp_pu8 , msg_len_remaining_u8);
		tx_msg_index_u8 = msg_len_remaining_u8;

		/* Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
		}
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

		loop_var_u8++;
	}

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_set_date_time_prv(debug_comm_rtc_param_tst *debug_comm_temp_rtc_param_arpst)
{
#if 0
	rtc_ext_time_date_st.year_u16 = debug_comm_temp_rtc_param_arpst->year_u8;
	rtc_ext_time_date_st.seconds_u8 = debug_comm_temp_rtc_param_arpst->seconds_u8;
	rtc_ext_time_date_st.months_u16 = debug_comm_temp_rtc_param_arpst->months_u8;
	rtc_ext_time_date_st.minutes_u16 = debug_comm_temp_rtc_param_arpst->minutes_u8;
	rtc_ext_time_date_st.hours_u16 = debug_comm_temp_rtc_param_arpst->hours_u8;
	rtc_ext_time_date_st.date_u8 = debug_comm_temp_rtc_param_arpst->date_u8;
	rtc_deinit_u8();
	rtc_init_u8();
	rtc_set_date_time_u8();
	rtc_get_date_time_u8();
#endif
	#if DEBUG_COMM_RTC_EXT
	/* Setting the rtc parameter structure with given rx msg*/
	debug_comm_rtc_set_ext_param_gst.hours_u8    = debug_comm_temp_rtc_param_arpst->hours_u8;
	debug_comm_rtc_set_ext_param_gst.minutes_u8  = debug_comm_temp_rtc_param_arpst->minutes_u8;
	debug_comm_rtc_set_ext_param_gst.seconds_u8  = debug_comm_temp_rtc_param_arpst->seconds_u8;
	debug_comm_rtc_set_ext_param_gst.weekdays_u8 = debug_comm_temp_rtc_param_arpst->weekdays_u8;
	debug_comm_rtc_set_ext_param_gst.date_u8     = debug_comm_temp_rtc_param_arpst->date_u8;
	debug_comm_rtc_set_ext_param_gst.months_u8   = debug_comm_temp_rtc_param_arpst->months_u8;
	debug_comm_rtc_set_ext_param_gst.year_u8     = debug_comm_temp_rtc_param_arpst->year_u8;

	#if !DEBUG_COMM_HARD_CODE
	/* Set Ext  RTC module */
	rtc_ext_set_time_date_u8(RTC_EXT_MCP7940M, (rtc_ext_time_date_tst*) &debug_comm_rtc_set_ext_param_gst);
	#endif

	#elif defined(DEBUG_COMM_RTC_MCU)
	/* Set Mcu(internal) RTC module */
	/*
	debug_comm_rtc_int_param_gst.hours_u16   = debug_comm_temp_rtc_param_arpst->hours_u8;
	debug_comm_rtc_int_param_gst.minutes_u16 = debug_comm_temp_rtc_param_arpst->minutes_u8;
	debug_comm_rtc_int_param_gst.seconds_u8  = debug_comm_temp_rtc_param_arpst->seconds_u8;
	debug_comm_rtc_int_param_gst.day_u16     = debug_comm_temp_rtc_param_arpst->date_u8;
	debug_comm_rtc_int_param_gst.months_u16  = debug_comm_temp_rtc_param_arpst->months_u8;
	debug_comm_rtc_int_param_gst.year_u16    = 2000 + debug_comm_temp_rtc_param_arpst->year_u8;
*/
#if !DEBUG_COMM_HARD_CODE
	rtc_int_set_time_date_u8(debug_comm_temp_rtc_param_arpst);
#endif

	#endif


}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_rtc_prv()
{
	#if DEBUG_COMM_RTC_EXT

		#if DEBUG_COMM_HARD_CODE
		/* rtc external */
		debug_comm_rtc_ext_param_gst.hours_u8 	 = debug_comm_rtc_set_ext_param_gst.hours_u8;
		debug_comm_rtc_ext_param_gst.minutes_u8  = debug_comm_rtc_set_ext_param_gst.minutes_u8;
		debug_comm_rtc_ext_param_gst.seconds_u8  = debug_comm_rtc_set_ext_param_gst.seconds_u8;
		debug_comm_rtc_ext_param_gst.date_u8 	 = debug_comm_rtc_set_ext_param_gst.date_u8;
		debug_comm_rtc_ext_param_gst.months_u8 	 = debug_comm_rtc_set_ext_param_gst.months_u8;
		debug_comm_rtc_ext_param_gst.year_u8	 = debug_comm_rtc_set_ext_param_gst.year_u8; /* 21 -> i.e 2021 */
		#else
		/* rtc external */
		rtc_ext_get_time_date_u8(RTC_EXT_PA8565, (rtc_ext_time_date_tst*) &debug_comm_rtc_ext_param_gst);
		#endif

	#elif DEBUG_COMM_RTC_MCU
		#if DEBUG_COMM_HARD_CODE
		debug_comm_rtc_int_param_gst.hours_u16    = (U16) debug_comm_rtc_set_ext_param_gst.hours_u8;
		debug_comm_rtc_int_param_gst.minutes_u16  = (U16) debug_comm_rtc_set_ext_param_gst.minutes_u8;
		debug_comm_rtc_int_param_gst.seconds_u8	  = (U16) debug_comm_rtc_set_ext_param_gst.seconds_u8;
		debug_comm_rtc_int_param_gst.day_u16	  = (U16) debug_comm_rtc_set_ext_param_gst.date_u8;
		debug_comm_rtc_int_param_gst.months_u16	  = (U16) debug_comm_rtc_set_ext_param_gst.months_u8;
		debug_comm_rtc_int_param_gst.year_u16	  = 2000 - (U16) debug_comm_rtc_set_ext_param_gst.year_u8;
		#else
		/* rtc internal(MCU) */
		rtc_int_get_time_date_u8((rtc_int_time_date_tst*) &debug_comm_rtc_int_param_gst);
		#endif
	#endif

}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_amb_lght_prv()
{

	/*Updating Ambient Light sensor data*/
	#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_AMB_LIGHT)
	debug_comm_amb_light_gu16 = 0x2710; 	/* 10,000lx */
	#elif(DEBUG_COMM_SEND_AMB_LIGHT)
	debug_comm_amb_light_gu16 = batt_param_pack_param_gst.pack_param_illuminance_u16;
	#else
	debug_comm_amb_light_gu16 = 0x0000; /* reserved value when not needed -> 0x00 */
	#endif


}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_rtc_amb_lght_prv()
{
	const U8 rtc_max_alloc_bytes_cu8 = 6;
	U8 tx_msg_index_u8;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	/* Initializing with rtc & ambient light sensor ID*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_RTC_AMB_LGHT_RES;
	debug_comm_tx_msg_st.length_u8 = 8;
	#if DEBUG_COMM_SEND_RTC
	/* 6 bytes*/
		#if DEBUG_COMM_RTC_EXT

		/* RTC data*/
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_rtc_ext_param_gst.hours_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_rtc_ext_param_gst.minutes_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_rtc_ext_param_gst.seconds_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_rtc_ext_param_gst.date_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_rtc_ext_param_gst.months_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_rtc_ext_param_gst.year_u8;

		#elif defined(DEBUG_COMM_RTC_MCU)
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8) debug_comm_rtc_int_param_gst.hours_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8) debug_comm_rtc_int_param_gst.minutes_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] =		debug_comm_rtc_int_param_gst.seconds_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8) debug_comm_rtc_int_param_gst.date_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8) debug_comm_rtc_int_param_gst.months_u8;
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_rtc_int_param_gst.year_u16 % 2000) & 0xFF; /* Gets the value: 2021 -> 21*/

		#endif
	#else
		/* Assigning reserved values upto max allocated bytes for RTC */
		while(tx_msg_index_u8 < rtc_max_alloc_bytes_cu8)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF;   					 /* Reserved byte */
		}
	#endif

	#if DEBUG_COMM_SEND_AMB_LIGHT
		/* Ambient Light sensor data*/
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_amb_light_gu16 & 0xFF);
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_amb_light_gu16 >> 8) & 0xFF);


	#else
		/* Assigning value - 0,  upto max bytes for CAN */
		while(tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0x00;   					 /* Reserved byte */
		}
	#endif



	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_mois_prv()
{
	/* Updating moisture sensor data*/
	#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_MOISTURE)
		debug_comm_moisture_param_gst.hum_1rh_u16 = 0x62; /* 98 %RH */
		debug_comm_moisture_param_gst.temp_1degc_u16 = 0xFE94; /* -358 -> -35.8 degC */
	#elif(DEBUG_COMM_SEND_MOISTURE)
		debug_comm_moisture_param_gst.hum_1rh_u16 	= batt_param_pack_param_gst.pack_param_pk_hum_u16;
		debug_comm_moisture_param_gst.temp_1degc_u16	= batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16;
	#else
		debug_comm_moisture_param_gst.hum_1rh_u16 	= 0x0000; /* reserved value when not needed -> 0x00 */
		debug_comm_moisture_param_gst.temp_1degc_u16	= 0x0000; /* reserved value when not needed -> 0x00 */
	#endif

}
/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_imu_data_prv()
{
	U8 axis_index_u8;
	for(axis_index_u8 = 0; axis_index_u8 < AXIS; axis_index_u8++)
	{
		#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_IMU)
		debug_comm_imu_param_gst.accel_1g_s32[axis_index_u8] = 0xFFFFF9D5; /* -1579 -> -1.579g */
		debug_comm_imu_param_gst.gyro_1dps_s32[axis_index_u8] = 0xFFFC1047; /* -257977 -> -257.977dps */
		debug_comm_imu_param_gst.mag_1ut_s32[axis_index_u8] = 0xFFC2770E; /* -4032754 -> -4032.754uT */
		#elif(DEBUG_COMM_SEND_IMU)
		debug_comm_imu_param_gst.accel_1g_s32[axis_index_u8] = batt_param_pack_param_gst.pack_param_icm20948_accel_s32[axis_index_u8];
		debug_comm_imu_param_gst.gyro_1dps_s32[axis_index_u8] = batt_param_pack_param_gst.pack_param_icm20948_gyro_s32[axis_index_u8];
		debug_comm_imu_param_gst.mag_1ut_s32[axis_index_u8] = batt_param_pack_param_gst.pack_param_icm20948_mag_s32[axis_index_u8];
		#else
		debug_comm_imu_param_gst.accel_1g_s32[axis_index_u8] = 0xFFFFFFFF;
		debug_comm_imu_param_gst.gyro_1dps_s32[axis_index_u8] = 0xFFFFFFFF;
		debug_comm_imu_param_gst.mag_1ut_s32[axis_index_u8] = 0xFFFFFFFF;
		#endif
	}
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_imu_mois_data_prv()
{
	/* Initializing the variables to start with zero*/
	U8 curr_sens_num_u8 = 0;
	U8 axis_index_u8 = 0;
	U8 tx_msg_index_u8 = 0;
	U8 imu_msg_index_u8 = 0;
	U8 current_sending_msg_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing constant variables - IMU param*/
	const U8 imu_max_data_per_msg_cu8 = 2;

	/* Initializing the variable to start with one because msg count is not less than zero */
	BOOL msg_send_again_b = 1;

	/* Pointer to hold the current sensor address */
	S32* curr_sens_ps32;

	/* Always DLC -8*/
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Assigning the sensor read data address to the current sensor pointer. Starts with accelerometer as per comm-protocol */
	curr_sens_ps32 = debug_comm_imu_param_gst.accel_1g_s32;

	/* Sending total messages that needs to send - IMU */
	while(msg_send_again_b)
	{
		/* Initializing & Incrementing with IMU response ID according to protocol */
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_IMU_RES) + current_sending_msg_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);


		for(imu_msg_index_u8 = 0; ((axis_index_u8 < AXIS) &&  (imu_msg_index_u8 < imu_max_data_per_msg_cu8)); axis_index_u8++ , imu_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (curr_sens_ps32[axis_index_u8] & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((curr_sens_ps32[axis_index_u8] >> 8) & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((curr_sens_ps32[axis_index_u8] >> 16) & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((curr_sens_ps32[axis_index_u8] >> 24) & 0xFF);
		}

		if (AXIS == axis_index_u8) /* if 3- AXIS of a sensor updated, then sensor changed to next */
		{
			axis_index_u8 = 0;
			if(curr_sens_num_u8 == 0 )
			{
				curr_sens_ps32 = debug_comm_imu_param_gst.gyro_1dps_s32;
				curr_sens_num_u8 = 1;
			}
			else if(curr_sens_num_u8 == 1)
			{
				curr_sens_ps32 = debug_comm_imu_param_gst.mag_1ut_s32;
				curr_sens_num_u8 = 2;
			}
			else if(curr_sens_num_u8 == 2)
			{
				curr_sens_num_u8 = 3;
			}
			else if(curr_sens_num_u8 == 3)
			{
				msg_send_again_b = 0;

				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_moisture_param_gst.hum_1rh_u16 & 0xFF);
				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_moisture_param_gst.hum_1rh_u16 >> 8 ) & 0xFF);
				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_moisture_param_gst.temp_1degc_u16 & 0xFF);
				debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_moisture_param_gst.temp_1degc_u16 >> 8) & 0xFF);
			}
		}

		if(CAN_APP_MAX_DATA_BYTES >= tx_msg_index_u8)
		{
			continue;
		}

		/* Resetting the values so that it can start storing form 0th index for next CAN message to send */
		tx_msg_index_u8 = 0;
		/* Incrementing the loop variable for moving to next CAN ID as per protocol */
		current_sending_msg_u8++;

		/* CAN transmit message */
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	}


}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_afe_slave_flt_sts_prv()
{
	U8 current_slv_num_u8;			/* Current Slave number   */

	for (current_slv_num_u8 = 0; current_slv_num_u8 < DEBUG_COMM_TOTAL_SLAVES; current_slv_num_u8++)
	{
		#if DEBUG_COMM_HARD_CODE
		/* slave 1 & 2 -> Cells in fault status -> 1,5,10*/
		debug_comm_slave_flt_sts_gst.cell_fault_sts_au16[current_slv_num_u8] = 0x211;
		#else
		debug_comm_slave_flt_sts_gst.cell_fault_sts_au16[current_slv_num_u8] = batt_param_pack_param_gst.pack_param_faulty_cell_id_au16[current_slv_num_u8];
		#endif
	}

	#if DEBUG_COMM_HARD_CODE
		/* Thermistors in fault status -> 1, 4 */
		debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 = 0x9;
	#else
		debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 = batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u32;
	#endif
}

/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_send_afe_slave_flt_sts_prv()
{

	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;
	U8 slv_index_u8 = 0;
	U8 slv_msg_index_u8 = 0;
	U8 loop_var_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Initializing the boolean variable to start with one*/
	BOOL msg_send_again_b = 1;

	/* Initializing constant variables with values as per comm protocol */
	const U8 slv_max_per_msg_u8 = 4;

	/* Assigning the length parameter of CAN TX*/
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Sending total messages that needs to send - AFE fault status - cell fault */
	while(msg_send_again_b)
	{
		/* Initializing & Incrementing with AFE fault status status response ID according to protocol */
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_AFE_SLV_CELL_FLT_RES) + loop_var_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		for(slv_msg_index_u8 = 0; ((slv_index_u8 < DEBUG_COMM_TOTAL_SLAVES) &&  (slv_msg_index_u8 < slv_max_per_msg_u8)); slv_index_u8++ , slv_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_slave_flt_sts_gst.cell_fault_sts_au16[slv_index_u8] & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_slave_flt_sts_gst.cell_fault_sts_au16[slv_index_u8] >> 8) & 0xFF);
		}

		/* Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	    }

		if(DEBUG_COMM_TOTAL_SLAVES == slv_index_u8)
		{
			msg_send_again_b = 0;
		}

		/* Resetting the values so that it can start storing form 0th index for next CAN message to send*/
		tx_msg_index_u8 = 0;
		/* Incrementing the loop variable for moving to next CAN ID as per protocol */
		loop_var_u8++;

		/* CAN transmit message */
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	}

	/* Sending msg - AFE fault status - thermistor fault status*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_AFE_SLV_THERM_FLT_RES;

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_slave_flt_sts_gst.therms_fault_sts_u64 >> 24) & 0xFF);

	/* Assigning reserved values upto last byte of particular message */
	while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
	{
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
    }

	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}



/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_set_afe_cb_ctrl_prv(debug_comm_afe_cb_slv_te slv_index_are)
{
	U8 slv_msg_index_u8 = 0;					/* current slave index */
	const U8 slv_max_per_msg_cu8 = 4;
	U8 rx_msg_index_u8 = 0;
	U8 slv_index_u8 = slv_index_are;

	/* Updates newly turned on cell balance ctrl. Affects previous ctrls set in variables */
	for (; ((slv_index_u8 < DEBUG_COMM_TOTAL_SLAVES) && (slv_msg_index_u8 < slv_max_per_msg_cu8)) ; slv_index_u8++ ,slv_msg_index_u8++ )
	{
		debug_comm_cb_sts_gst.cb_ctrl_au16[slv_index_u8] = debug_comm_can_rx_gst.data_au8[rx_msg_index_u8++];
		debug_comm_cb_sts_gst.cb_ctrl_au16[slv_index_u8] |= ((debug_comm_can_rx_gst.data_au8[rx_msg_index_u8++] << 8) & 0xFF00);
	}

	if(DEBUG_COMM_CB_SLV_1_4 == slv_index_are)
	{
		/*TODO to call the set cb ctrl api here for first 4 slaves */
		afe_driver_balancing_fet_control_u8((U16)(debug_comm_cb_sts_gst.cb_ctrl_au16[0] & 0x3FFF)); /* Only One Slave - 14 cells Available*/

	}
	else if(DEBUG_COMM_CB_SLV_5_8 == slv_index_are)
	{
		/*TODO to call the set cb ctrl api here for slaves 5 - 8 */

	}
	else if(DEBUG_COMM_CB_SLV_9 == slv_index_are)
	{
		/*TODO to call the set cb ctrl api here for slave 9  */

	}

}
/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_update_afe_cb_sts_prv()
{
	U8 current_slv_num_u8 = 0;			/* Current Slave number   */

	for (current_slv_num_u8 = PACK_PARAM_MOD_ID_1; current_slv_num_u8 < PACK_PARAM_MOD_ID_MAX_NUM; current_slv_num_u8++)
	{

		#if DEBUG_COMM_HARD_CODE
		/* Hard-coded to the cell balancing control variable which gets updated by user. On init it is 0x0000  */
		debug_comm_cb_sts_gst.cb_sts_au16[current_slv_num_u8] = debug_comm_cb_sts_gst.cb_ctrl_au16[current_slv_num_u8];
		#else
		/* TODO: Later assign current cell balancing which is happening in the place of 0xFFFF value which is assigned*/
		debug_comm_cb_sts_gst.cb_sts_au16[current_slv_num_u8] = cell_bal_config_status_gst.bal_register_u16;
		#endif


	}

}
/**
 =====================================================================================================================================================

 @fn Name			:
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void debug_comm_send_afe_cb_sts_prv(debug_comm_afe_cb_slv_te slv_index_are)
{

	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;
	U8 slv_msg_index_u8 = 0;
	U8 loop_var_u8 = 0;
	U8 curr_id_u8 = 0;
	U8 slv_index_u8 = 0;
	/* Initializing the boolean variable to start with one*/
	BOOL msg_send_again_b = 1;

	/* Initializing with the user given slave index (i.e from the argument) */
	if( DEBUG_COMM_CB_SLV_ALL != slv_index_are)
	{
	     slv_index_u8 = slv_index_are;
	}

	/* Initializing constant variables with values as per comm protocol*/
	const U8 slv_max_per_msg_u8 = 4;

	/* Sending total messages that needs to send */
	while(msg_send_again_b)
	{
		/* Initializing & Incrementing with cell balance status response ID according to protocol */
		curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_AFE_CB_STS_RES) + loop_var_u8;
		debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

		debug_comm_tx_msg_st.length_u8 = 8;
		for(slv_msg_index_u8 = 0; ((slv_index_u8 < DEBUG_COMM_TOTAL_SLAVES) &&  (slv_msg_index_u8 < slv_max_per_msg_u8)); slv_index_u8++ , slv_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_cb_sts_gst.cb_sts_au16[slv_index_u8] & 0xFF);
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_cb_sts_gst.cb_sts_au16[slv_index_u8] >> 8) & 0xFF);
		}

		/* Assigning reserved values upto last byte of particular message */
		while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	    }

		if((DEBUG_COMM_TOTAL_SLAVES == slv_index_u8) || (DEBUG_COMM_CB_SLV_ALL != slv_index_are))
		{
			msg_send_again_b = 0;
		}

		tx_msg_index_u8 = 0;
		loop_var_u8++;

		/* CAN transmit message */
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
	}

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_update_batt_sts_params_prv()
{
	/* TODO: Missing RHS*/
	debug_comm_batt_sts_params_gst.total_power_s16 = batt_param_pack_param_gst.pack_param_inst_power_1W_s16;
	debug_comm_batt_sts_params_gst.chg_capacity_u16;
	debug_comm_batt_sts_params_gst.dhg_capacity_u16;
	debug_comm_batt_sts_params_gst.soc_u16 = batt_param_pack_param_gst.pack_param_OCV_soc_1cPC_u16;
	debug_comm_batt_sts_params_gst.soh_u16 = batt_param_pack_param_gst.pack_param_soh_1mPC_u32;
	debug_comm_batt_sts_params_gst.sop_u16;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_batt_sts_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;

	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_BATT_STS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Filling data - HV-ADC*/
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (batt_param_pack_param_gst.pack_param_all_mfet_sts_e & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (batt_param_pack_param_gst.pack_param_batt_mode_e & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (batt_param_pack_param_gst.pack_param_trip_action_e & 0xFF);

	/* Assigning reserved values upto last byte of particular message */
	while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
	{
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	}

	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_sts_params_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;

	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_STS_PARAMS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Filling data - HV-ADC*/
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 >> 8) & 0xFF);

	/* Assigning reserved values upto last byte of particular message */
	while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
	{
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
	}

	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
}

#if 0
static void debug_comm_send_batt_sts_params_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;
	U8 curr_id_u8 = 0;

	/* Only one message sent for Battery status parameters */

	/* Initializing with Battery status parameters response ID and length*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_BATT_STS_PARAMS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Filling data1 - Battery status parameters*/
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_batt_sts_params_gst.total_power_s16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_batt_sts_params_gst.total_power_s16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_batt_sts_params_gst.chg_capacity_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_batt_sts_params_gst.chg_capacity_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_batt_sts_params_gst.dhg_capacity_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_batt_sts_params_gst.dhg_capacity_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_batt_sts_params_gst.soh_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_batt_sts_params_gst.soh_u16 >> 8) & 0xFF);

	/* CAN transmit message 1*/
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

	/* Initializing with Battery status parameters response ID and length*/
	curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_BATT_STS_PARAMS_RES) + 1;
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

	/* Filling data2 - Battery status parameters*/
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_batt_sts_params_gst.soc_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_batt_sts_params_gst.soc_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_batt_sts_params_gst.sop_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_batt_sts_params_gst.sop_u16 >> 8) & 0xFF);

	/* Assigning reserved values upto last byte of particular message */
	while (tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES)
	{
		debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = 0xFF; /* Reserved byte */
    }

	/* CAN transmit message 2*/
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_flash_data_prv()
{

	U8 tx_msg_index_u8;

	/* Initializing with Get Flash data ID*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_GET_FLASH_DATA_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	while(debug_comm_send_flash_data_gb)
	{
		/*Call flash get data api*/

		for (tx_msg_index_u8 = 0; tx_msg_index_u8 <= CAN_APP_MAX_DATA_BYTES; tx_msg_index_u8++)
		{
			debug_comm_tx_msg_st.data_au8[tx_msg_index_u8] = debug_comm_flash_data_au8[tx_msg_index_u8];
		}

		/* CAN transmit message*/
		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_batt_info_prv()
{
	/* Initializing the variables to start with zero*/
	U8 tx_msg_index_u8 = 0;

	/* Only one message sent for Basic Battery info */

	/* Initializing with Basic battery info response ID and length*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_BASIC_BATT_INFO_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	/* Filling data - Basic battery info*/
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_basic_batt_info_gst.bms_firmware_version_num_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_basic_batt_info_gst.bms_firmware_version_num_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_basic_batt_info_gst.total_cells_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_basic_batt_info_gst.total_cells_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_basic_batt_info_gst.bms_hw_version_num_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_basic_batt_info_gst.bms_hw_version_num_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_basic_batt_info_gst.cell_type_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_basic_batt_info_gst.cell_type_u16 >> 8) & 0xFF);

	/* CAN transmit message*/
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/

static void debug_comm_set_warning_cutoff_prv(U8 *warning_cutoff_data_arpu8)
{
	U8 param_to_change_u8 = 0;
	U8 threshold_val_u8[7];

	param_to_change_u8 = *(warning_cutoff_data_arpu8 + 0);
	memory_copy_u8_array_v(&threshold_val_u8[0], (warning_cutoff_data_arpu8 + 1), 7);

	switch(param_to_change_u8)
	{
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_OV_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_UV_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_OV_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_UV_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_CELL_OV_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_max_cell_1mV_u16  = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_CELL_UV_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_DEEP_DSG_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_deep_discharge_1mV_u16 = ((threshold_val_u8[1] << 8) |
																		(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_DEEP_DSG_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.deep_dsg_cell_1mV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OV_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_UV_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OV_CUTOFF:
		{
			bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16 = ((threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_UV_CUTOFF:
		{
			bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16 = ((threshold_val_u8[1] << 8) |
																	(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_PACK_OV_CUTOFF:
		{
			bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_PACK_UV_CUTOFF:
		{
			bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_OT_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_UT_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_OT_CUTOFF:			/* Cell Over Temperature */
		{
			bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_CELL_UT_CUTOFF:			/* Cell Under Temperature */
		{
			bms_config_nv_cell_cfg_gst.abs_min_cell_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_CELL_OT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_CELL_UT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_min_cell_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AMB_OT_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_pack_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AMB_UT_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_min_pack_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AMB_OT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_max_pack_1cc_s16	= ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_AMB_UT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_min_pack_1cc_s16	= ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_AMB_OT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_max_pack_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_AMB_UT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_min_pack_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_MFET_OT_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_mosfet_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_MFET_UT_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_min_mosfet_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMMM_MFET_OT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_max_mosfet_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_MFET_UT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_min_mosfet_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_MFET_OT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_max_mosfet_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_RELEASE_MFET_UT_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.release_min_mosfet_1cc_s16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OVER_CHG_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_chg_1mA_s32 = ((threshold_val_u8[3] << 24) | (threshold_val_u8[2] << 16) |
															   (threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
//			bms_config_nv_warn_mon_gst.warn_max_chg_1mA_s32 = (bms_config_nv_warn_mon_gst.warn_max_chg_1mA_s32/1000);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OVER_DSG_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_dsg_1mA_s32 = ((threshold_val_u8[3] << 24) | (threshold_val_u8[2] << 16) |
															   (threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
//			bms_config_nv_warn_mon_gst.warn_max_dsg_1mA_s32 = ((-1)*(bms_config_nv_warn_mon_gst.warn_max_dsg_1mA_s32/1000));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OVER_CHG_CUTOFF:
		{
			bms_config_nv_pi_cfg_gst.max_chg_1mA_s32 = ((threshold_val_u8[3] << 24) | (threshold_val_u8[2] << 16) |
														(threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
//			bms_config_nv_pi_cfg_gst.max_chg_1mA_s32 = (bms_config_nv_pi_cfg_gst.max_chg_1mA_s32/1000);
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OVER_DSG_CUTOFF:
		{
			bms_config_nv_pi_cfg_gst.max_dsg_1mA_s32 = ((threshold_val_u8[3] << 24) | (threshold_val_u8[2] << 16) |
														(threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
//			bms_config_nv_pi_cfg_gst.max_dsg_1mA_s32 = ((-1)*(bms_config_nv_pi_cfg_gst.max_dsg_1mA_s32/1000));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_SURGE_CURRENT_CUTOFF:
		{
			bms_config_nv_pi_cfg_gst.max_scd_surge_1mA_s32 = ((threshold_val_u8[3] << 24) | (threshold_val_u8[2] << 16) |
																	(threshold_val_u8[1] << 8) | (threshold_val_u8[0] & 0xFF));
//			bms_config_nv_warn_mon_gst.warn_max_scd_surge_1mA_s32 = ((-1)*(bms_config_nv_warn_mon_gst.warn_max_scd_surge_1mA_s32/1000));
		}
		break;
		/**********************************************************************************************************************/
#if DEBUG_COMM_SEND_MOISTURE
		case DEBUG_COMM_PACK_OH_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_min_pack_hum_u16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_UH_WARN:
		{
			bms_config_nv_warn_mon_gst.warn_max_pack_hum_u16 = ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_OH_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_max_pack_hum_u16	= ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_PACK_UH_CUTOFF:
		{
			bms_config_nv_cell_cfg_gst.abs_min_pack_hum_u16	= ((threshold_val_u8[1] << 8) |
																			(threshold_val_u8[0] & 0xFF));
		}
		break;
#endif
		/**********************************************************************************************************************/
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_update_warn_cutoff_param_prv()
{
	/* @Version number update happens here */
	debug_comm_basic_batt_info_gst.bms_hw_version_num_u16 			= batt_param_batt_pack_details_gst.bms_hw_version_num_u16;
	debug_comm_basic_batt_info_gst.bms_pk_version_num_u16 			= batt_param_batt_pack_details_gst.bms_pk_version_num_u16;
	debug_comm_basic_batt_info_gst.slave_hw_version_num_u16 		= batt_param_batt_pack_details_gst.slave_hw_version_num_u16;
	debug_comm_basic_batt_info_gst.bms_firmware_version_num_u16 	= batt_param_batt_pack_details_gst.bms_firmware_version_num_u16;
	debug_comm_basic_batt_info_gst.bms_protocol_version_num_u16 	= batt_param_batt_pack_details_gst.bms_protocol_version_num_u16;

	/* @Batt manufacture details update happens here */
	debug_comm_basic_batt_info_gst.batt_supplier_u8 				= batt_param_batt_pack_details_gst.batt_supplier_u8;
	debug_comm_basic_batt_info_gst.batt_customer_u8 				= batt_param_batt_pack_details_gst.batt_customer_u8;
	debug_comm_basic_batt_info_gst.date_of_manf_u32 				= batt_param_batt_pack_details_gst.date_of_manf_u32;

	/* @Batt specifications update happens here	*/
	debug_comm_basic_batt_info_gst.pack_dod_u16 					= batt_param_batt_pack_details_gst.pack_dod_u16;
	debug_comm_basic_batt_info_gst.rated_pack_cycles_u16 			= batt_param_batt_pack_details_gst.rated_pack_cycles_u16;
	debug_comm_basic_batt_info_gst.nominal_voltage_u16 				= batt_param_batt_pack_details_gst.nominal_voltage_u16;
	debug_comm_basic_batt_info_gst.rated_capacity_u16 				= batt_param_batt_pack_details_gst.rated_capacity_u16;
	debug_comm_basic_batt_info_gst.cell_type_u16 					= batt_param_batt_pack_details_gst.cell_type_u16;
	debug_comm_basic_batt_info_gst.total_cells_u16 					= batt_param_batt_pack_details_gst.total_cells_u16;
	debug_comm_basic_batt_info_gst.number_of_slaves_u16 			= batt_param_batt_pack_details_gst.number_of_slaves_u16;

	/* @Cell voltage level update happens here */
	debug_comm_basic_batt_info_gst.cell_ov_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_uv_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_ov_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_uv_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.release_cell_ov_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_max_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.release_cell_uv_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_deep_dsg_warning_u16     	= bms_config_nv_warn_mon_gst.warn_deep_discharge_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_deep_dsg_threshold_u16 		= bms_config_nv_cell_cfg_gst.deep_dsg_cell_1mV_u16;

	/* @Pack voltage level update happens here */
	debug_comm_basic_batt_info_gst.pack_ov_warning_u16 				= bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.pack_uv_warning_u16 				= bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.pack_ov_threshold_u16 			= bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.pack_uv_threshold_u16 			= bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.release_pack_ov_threshold_u16 	= bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.release_pack_uv_threshold_u16 	= bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16;

	/* @Cell temperature level update happens here */
	debug_comm_basic_batt_info_gst.cell_ot_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.cell_ut_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.cell_ot_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.cell_ut_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.release_cell_ot_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.release_cell_ut_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_cell_1cc_s16;

	/* @Ambient temperature level update happens here */
	debug_comm_basic_batt_info_gst.amb_ot_warning_u16 				= 0xFFFF;
	debug_comm_basic_batt_info_gst.amb_ut_warning_u16 				= 0xFFFF;
	debug_comm_basic_batt_info_gst.amb_ot_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_pack_1cc_s16;
	debug_comm_basic_batt_info_gst.amb_ut_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_pack_1cc_s16;
	debug_comm_basic_batt_info_gst.release_amb_ot_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_max_pack_1cc_s16;
	debug_comm_basic_batt_info_gst.release_amb_ut_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_pack_1cc_s16;

	/* @Mosfet temperature level update happens here */
	debug_comm_basic_batt_info_gst.mfet_ot_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.mfet_ut_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_mosfet_1cc_s16; /*TODO*/
	debug_comm_basic_batt_info_gst.mfet_ot_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.mfet_ut_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.release_mfet_ot_threshold_u16	= bms_config_nv_cell_cfg_gst.release_max_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.release_mfet_ut_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_mosfet_1cc_s16;

	/* @Packet current level update happens here */
	debug_comm_basic_batt_info_gst.max_chg_1ma_s32 					= bms_config_nv_pi_cfg_gst.max_chg_1mA_s32;
	debug_comm_basic_batt_info_gst.max_dsg_1ma_s32 					= ((-1)*(bms_config_nv_pi_cfg_gst.max_dsg_1mA_s32));
	debug_comm_basic_batt_info_gst.warn_max_chg_1ma_s32 			= bms_config_nv_warn_mon_gst.warn_max_chg_1mA_s32;
	debug_comm_basic_batt_info_gst.warn_max_dsg_1ma_s32 			= bms_config_nv_warn_mon_gst.warn_max_dsg_1mA_s32;
	debug_comm_basic_batt_info_gst.warn_max_scd_surge_1ma_s32 		= ((-1)*(bms_config_nv_pi_cfg_gst.max_scd_surge_1mA_s32));

	/* @Ambient(or)Pack Humidity level update happens here */
	debug_comm_basic_batt_info_gst.pack_oh_warning_u16 				= 0xFFFF;
	debug_comm_basic_batt_info_gst.pack_uh_warning_u16				= 0xFFFF;
	debug_comm_basic_batt_info_gst.pack_oh_threshold_u16			= 0xFFFF;
	debug_comm_basic_batt_info_gst.pack_uh_threshold_u16			= 0xFFFF;

}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_update_max_min_cparams_prv()
{

	#if (DEBUG_COMM_HARD_CODE & DEBUG_COMM_SEND_MAX_MIN_CPARAMS)

	debug_comm_max_min_cparams_gst.max_vtg_u16  = 0x138 ; 	/* 312cV  -> 3120mV */
	debug_comm_max_min_cparams_gst.min_vtg_u16  = 0x130 ; 	/* 304cV  -> 3040mV */
	debug_comm_max_min_cparams_gst.max_temp_u16 = 0x7C;
	debug_comm_max_min_cparams_gst.min_temp_u16 = 0x7D;


	debug_comm_max_min_cparams_gst.cell_num_max_vtg_u8 = 0x04;
	debug_comm_max_min_cparams_gst.slv_num_max_vtg_u8  = 0x06;
	debug_comm_max_min_cparams_gst.cell_num_min_vtg_u8 = 0x02;
	debug_comm_max_min_cparams_gst.slv_num_min_vtg_u8  = 0x08;
	debug_comm_max_min_cparams_gst.cell_num_max_temp_u8  = 0x05;
	debug_comm_max_min_cparams_gst.slv_num_max_temp_u8   = 0x06;
	debug_comm_max_min_cparams_gst.cell_num_min_temp_u8  = 0x02;
	debug_comm_max_min_cparams_gst.slv_num_min_temp_u8   = 0x08;

	#elif(DEBUG_COMM_SEND_MAX_MIN_CPARAMS)

	/* TODO : To assign with global battery params*/
	debug_comm_max_min_cparams_gst.max_vtg_u16 		= batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 ;
	debug_comm_max_min_cparams_gst.min_vtg_u16 		= batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 ;
	debug_comm_max_min_cparams_gst.max_temp_u16 	= batt_param_pack_param_gst.pack_param_max_cell_1cc_s16; /* Signed Value assigned to unsigned */
	debug_comm_max_min_cparams_gst.min_temp_u16 	= batt_param_pack_param_gst.pack_param_min_cell_1cc_s16;

	debug_comm_max_min_cparams_gst.cell_num_max_vtg_u8 = batt_param_pack_param_gst.pack_param_max_V_cell_index_e;
	debug_comm_max_min_cparams_gst.slv_num_max_vtg_u8  = batt_param_pack_param_gst.pack_param_mod_at_maxV_e;
	debug_comm_max_min_cparams_gst.cell_num_min_vtg_u8 = batt_param_pack_param_gst.pack_param_min_V_cell_index_e;
	debug_comm_max_min_cparams_gst.slv_num_min_vtg_u8  = batt_param_pack_param_gst.pack_param_mod_at_minV_e;
	debug_comm_max_min_cparams_gst.cell_num_max_temp_u8  = batt_param_pack_param_gst.pack_param_max_T_cell_index_e;
	debug_comm_max_min_cparams_gst.slv_num_max_temp_u8   = batt_param_pack_param_gst.pack_param_mod_at_maxT_e;
	debug_comm_max_min_cparams_gst.cell_num_min_temp_u8  = batt_param_pack_param_gst.pack_param_min_T_cell_index_e;
	debug_comm_max_min_cparams_gst.slv_num_min_temp_u8   = batt_param_pack_param_gst.pack_param_mod_at_minT_e;

	//memory_set_u8_array_v((U8*)&debug_comm_max_min_cparams_gst, 0xFF, sizeof(debug_comm_max_min_cparams_gst));

	#else


	#endif

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_max_min_cparams_prv()
{

	U8 tx_msg_index_u8;
	U8 curr_id_u8 = 0;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	/* Initializing with max min cell params ID*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MIN_MAX_CPARAMS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_max_min_cparams_gst.max_vtg_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_max_min_cparams_gst.max_vtg_u16 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_max_min_cparams_gst.min_vtg_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_max_min_cparams_gst.min_vtg_u16 >> 8) & 0xFF);

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_max_min_cparams_gst.max_temp_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_max_min_cparams_gst.max_temp_u16 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (debug_comm_max_min_cparams_gst.min_temp_u16 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = ((debug_comm_max_min_cparams_gst.min_temp_u16 >> 8) & 0xFF);

	/* CAN transmit message 1*/
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

	/* Incrementing with max min cell params ID*/
	curr_id_u8 = DEBUG_COMM_GET_PGN(DEBUG_COMM_MIN_MAX_CPARAMS_RES) + 1;
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(curr_id_u8);

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.cell_num_max_vtg_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.slv_num_max_vtg_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.cell_num_min_vtg_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.slv_num_min_vtg_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.cell_num_max_temp_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.slv_num_max_temp_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.cell_num_min_temp_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_max_min_cparams_gst.slv_num_min_temp_u8;

	/* CAN transmit message 2 */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_update_ah_cyc_cnt_prv()
{
	debug_comm_ah_val_u8 = (U8)(batt_param_energy_gst.acc_chg_u64 & 0xFF);
	debug_comm_cycle_cnt_u64 = batt_param_energy_gst.tot_cycle_count_u64;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_send_ah_cyc_cnt_prv()
{
	U8 tx_msg_index_u8;

	/* Initializing the values so that it can start storing form 0th index for CAN message to send */
	tx_msg_index_u8 = 0;

	/* Initializing with max min cell params ID*/
	debug_comm_tx_msg_st.can_id_u32 = DEBUG_COMM_AH_CYC_CNT_CPARAMS_RES;
	debug_comm_tx_msg_st.length_u8 = 8;

	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = debug_comm_ah_val_u8;
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)(debug_comm_cycle_cnt_u64 & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)((debug_comm_cycle_cnt_u64 >> 8) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)((debug_comm_cycle_cnt_u64 >> 16) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)((debug_comm_cycle_cnt_u64 >> 24) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)((debug_comm_cycle_cnt_u64 >> 32) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)((debug_comm_cycle_cnt_u64 >> 40) & 0xFF);
	debug_comm_tx_msg_st.data_au8[tx_msg_index_u8++] = (U8)((debug_comm_cycle_cnt_u64 >> 48) & 0xFF);

	/* CAN transmit message 2 */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/

static void debug_comm_bin_ack_send_prv()
{
	can_app_message_tst send_bin_ack_can_msg_st;
	U8 byte_count_u8 = 0;

#if DEBUG_COMM_SEND_FLASH /*TODO*/
	/* If current flash operation is completed */
	if(FLASH_DRV_OPERATION_COMPLETED == debug_comm_flash_operation_sts_u8)
	{
		/* Current Write & Read operation Success - no mismatch occurs with Write & read */
		if(FLASH_DRV_WR_SUCCESS == flash_drv_write_sts_u8)
		{
			send_bin_ack_can_msg_st.data_au8[0] = 0xAA;  /* External Flash Write Success */
		}
		/* Current Write & Read operation failed - mismatch occurs with Write & read */
		else
		{
			send_bin_ack_can_msg_st.data_au8[0] = 0xCC;  /* Current Write & write operation failure due to mismatch of data*/
		}

		send_bin_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_BIN1_CMD;
		send_bin_ack_can_msg_st.length_u8 = 8;
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_BIN_SIZE;byte_count_u8++)
		{
			send_bin_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_bin_ack_can_msg_st);

	}
	/* If any previous flash operation is in process */
	else if(FLASH_DRV_OPERATION_INPROGRESS == debug_comm_flash_operation_sts_u8)
	{
	 	send_bin_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_BIN1_CMD;
		send_bin_ack_can_msg_st.length_u8 = 8;
		send_bin_ack_can_msg_st.data_au8[0] = 0xBB;  /* External Flash busy with other operation */
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_BIN_SIZE;byte_count_u8++)
		{
			send_bin_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_bin_ack_can_msg_st);
	}
	/* If external flash has fault */
	else if (FLASH_DRV_OPERATION_FAILED == debug_comm_flash_operation_sts_u8)
	{
		send_bin_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_BIN1_CMD;
		send_bin_ack_can_msg_st.length_u8 = 8;
		send_bin_ack_can_msg_st.data_au8[0] = 0xFF;  /* External Flash Fault */
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_BIN_SIZE;byte_count_u8++)
		{
			send_bin_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_bin_ack_can_msg_st);
	}
#endif

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_vin_ack_send_prv()
{
	can_app_message_tst send_vin_ack_can_msg_st;
	U8 byte_count_u8 = 0;
#if DEBUG_COMM_SEND_FLASH /*TODO*/
	/* If current flash operation is completed */
	if(FLASH_DRV_OPERATION_COMPLETED == debug_comm_flash_operation_sts_u8)
	{
		/* Current Write & Read operation Success - no mismatch occurs with Write & read */
		if(FLASH_DRV_WR_SUCCESS == flash_drv_write_sts_u8)
		{
			send_vin_ack_can_msg_st.data_au8[0] = 0xAA;  /* External Flash Write Success */
		}
		/* Current Write & Read operation failed - mismatch occurs with Write & read */
		else
		{
			send_vin_ack_can_msg_st.data_au8[0] = 0xCC;  /* Current Write & write operation failure due to mismatch of data*/
		}

		send_vin_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_VIN1_CMD;
		send_vin_ack_can_msg_st.length_u8 = 8;
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_VIN_SIZE;byte_count_u8++)
		{
			send_vin_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_vin_ack_can_msg_st);

	}
	/* If any previous flash operation is in process */
	else if(FLASH_DRV_OPERATION_INPROGRESS == debug_comm_flash_operation_sts_u8)
	{
	 	send_vin_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_VIN1_CMD;
		send_vin_ack_can_msg_st.length_u8 = 8;
		send_vin_ack_can_msg_st.data_au8[0] = 0xBB;  /* External Flash busy with other operation */
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_VIN_SIZE;byte_count_u8++)
		{
			send_vin_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_vin_ack_can_msg_st);
	}
	/* If external flash has fault */
	else if (FLASH_DRV_OPERATION_FAILED == debug_comm_flash_operation_sts_u8)
	{
		send_vin_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_VIN1_CMD;
		send_vin_ack_can_msg_st.length_u8 = 8;
		send_vin_ack_can_msg_st.data_au8[0] = 0xFF;  /* External Flash Fault */
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_VIN_SIZE;byte_count_u8++)
		{
			send_vin_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_vin_ack_can_msg_st);
	}
#endif
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_mac_ack_send_prv()
{
	can_app_message_tst send_mac_ack_can_msg_st;
	U8 byte_count_u8 = 0;
#if DEBUG_COMM_SEND_FLASH /*TODO*/
	if(FLASH_DRV_OPERATION_COMPLETED == debug_comm_flash_operation_sts_u8)
	{
		if(FLASH_DRV_WR_SUCCESS == flash_drv_write_sts_u8)
		{
			send_mac_ack_can_msg_st.data_au8[0] = 0xAA;  /* External Flash Write Success */
		}
		else  /* Current Write & Read operation failed - mismatch occurs with Write & read */
		{
			send_mac_ack_can_msg_st.data_au8[0] = 0xCC;  /* Current Write & write operation failure due to mismatch of data*/
		}

		send_mac_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_MAC_CMD;
		send_mac_ack_can_msg_st.length_u8 = 8;
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_MAC_SIZE;byte_count_u8++)
		{
			send_mac_ack_can_msg_st.data_au8[byte_count_u8] = common_var_bms_id_log_gst.mac_au8[byte_count_u8-1];
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_mac_ack_can_msg_st);

	}
	else if(FLASH_DRV_OPERATION_INPROGRESS == debug_comm_flash_operation_sts_u8)
	{
	 	send_mac_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_MAC_CMD;
		send_mac_ack_can_msg_st.length_u8 = 8;
		send_mac_ack_can_msg_st.data_au8[0] = 0xBB;  /* External Flash busy with other operation */
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_MAC_SIZE;byte_count_u8++)
		{
			send_mac_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_mac_ack_can_msg_st);
	}
	else if (FLASH_DRV_OPERATION_FAILED == debug_comm_flash_operation_sts_u8)
	{
		send_mac_ack_can_msg_st.can_id_u32 = DEBUG_COMM_SET_MAC_CMD;
		send_mac_ack_can_msg_st.length_u8 = 8;
		send_mac_ack_can_msg_st.data_au8[0] = 0xFF;  /* External Flash Fault */
		for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_MAC_SIZE;byte_count_u8++)
		{
			send_mac_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_mac_ack_can_msg_st);
	}
#endif
}

#if DEBUG_COMM_SEND_FLASH
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_ext_flash_erase_percentage_sts_send_prv()
{
	can_app_message_tst send_flash_erase_percent_sts_st;
	U8 byte_count_u8 = 0;

	//if(FLASH_DRV_OPERATION_INPROGRESS == debug_comm_flash_operation_sts_u8)
	{
		send_flash_erase_percent_sts_st.can_id_u32 = DEBUG_COMM_FLASH_ERASE_PERCENT_STS_RES;
		send_flash_erase_percent_sts_st.length_u8 = 8;
		send_flash_erase_percent_sts_st.data_au8[0] = flash_drv_blocks_erase_st.total_blocks_erased_in_percentage_u8;

		for(byte_count_u8 = 1;byte_count_u8 <= 7;byte_count_u8++)
		{
			send_flash_erase_percent_sts_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_flash_erase_percent_sts_st);
	}
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_ext_flash_erase_ack_send_prv()
{
	can_app_message_tst send_flash_erase_ack_can_msg_st;
	U8 byte_count_u8 = 0;

	if(FLASH_DRV_OPERATION_COMPLETED == debug_comm_flash_operation_sts_u8)
	{
		send_flash_erase_ack_can_msg_st.data_au8[0] = 0xAA;  /* External Flash chip Erase Success */

		send_flash_erase_ack_can_msg_st.can_id_u32 = DEBUG_COMM_CHIP_ERASE_CMD;
		send_flash_erase_ack_can_msg_st.length_u8 = 8;
		for(byte_count_u8 = 1;byte_count_u8 <= 7;byte_count_u8++)
		{
			send_flash_erase_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_flash_erase_ack_can_msg_st);

	}
	else if(FLASH_DRV_OPERATION_INPROGRESS == debug_comm_flash_operation_sts_u8)
	{
	 	send_flash_erase_ack_can_msg_st.can_id_u32 = DEBUG_COMM_CHIP_ERASE_CMD;
		send_flash_erase_ack_can_msg_st.length_u8 = 8;
		send_flash_erase_ack_can_msg_st.data_au8[0] = 0xBB;  /* External Flash busy with other operation */
		for(byte_count_u8 = 1;byte_count_u8 <= 7;byte_count_u8++)
		{
			send_flash_erase_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_flash_erase_ack_can_msg_st);
	}
	else if (FLASH_DRV_OPERATION_FAILED == debug_comm_flash_operation_sts_u8)
	{
		send_flash_erase_ack_can_msg_st.can_id_u32 = DEBUG_COMM_CHIP_ERASE_CMD;
		send_flash_erase_ack_can_msg_st.length_u8 = 8;
		send_flash_erase_ack_can_msg_st.data_au8[0] = 0xFF;  /* External Flash Fault */
		for(byte_count_u8 = 1;byte_count_u8 <= 7;byte_count_u8++)
		{
			send_flash_erase_ack_can_msg_st.data_au8[byte_count_u8] = 0x00;
		}

		can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_flash_erase_ack_can_msg_st);
	}
}
#endif
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_mac_send_prv()
{
	can_app_message_tst send_mac_can_msg_st;
	U8 byte_count_u8;

	send_mac_can_msg_st.can_id_u32 = DEBUG_COMM_MAC_RES;
	send_mac_can_msg_st.length_u8 = 8;

	for(byte_count_u8 = 1;byte_count_u8 <= COMMON_VAR_BMS_MAC_SIZE;byte_count_u8++)
	{
		send_mac_can_msg_st.data_au8[byte_count_u8] = common_var_bms_id_log_gst.mac_au8[byte_count_u8];
	}

	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &send_mac_can_msg_st);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_j1939_rts_send_prv(U8 pgn_aru8,U16 data_size_aru16)
{
    U8 multi_pkt_frame_size_u8 = 0;

    debug_comm_rts_st.control_byte_u8 = DEBUG_COMM_RTS_CONTROLBYTE;
    debug_comm_rts_st.total_byte_u16  = data_size_aru16;
    multi_pkt_frame_size_u8			  = data_size_aru16 /7;

    if(data_size_aru16 % 7 != 0)
        multi_pkt_frame_size_u8++;

    debug_comm_rts_st.total_packet_u8 = multi_pkt_frame_size_u8;
    debug_comm_rts_st.max_packet_u8   = multi_pkt_frame_size_u8;
    debug_comm_rts_st.pgn_au8[0] 	  = 0;
    debug_comm_rts_st.pgn_au8[1] 	  = pgn_aru8;
    debug_comm_rts_st.pgn_au8[2] 	  = 0;

    if(can_app_frame_and_tx_msg_u8 DEBUG_COMM_RTS_FRAME )
    {
    	#if 0		/* TODO: If we need to take care of timeout scenarious - enable this & rethink the logic */
    	can_app_comm_task_set_timer_v(COMMON_VAR_DEBUG_SM,DEBUG_COMM_RTS_TIMEOUT,DEBUG_COMM_RTS_CTS_EOF_PGN);
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_WAIT;
    	#endif

    	common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
    }
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_j1939_multipacket_can_msg_update_prv(void)
{
	switch(debug_comm_can_rx_local_buff_au8[6])
	{
		/**********************************************************************************************************************/
		case DEBUG_COMM_ONE_TIME_INFO_PGN:
		{
			debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 = DEBUG_COMM_ONE_TIME_INFO_MSG_SIZE;
			dbg_comm_multipacket_data_gpu8 = (U8*) &debug_comm_basic_batt_info_gst;
			debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 = debug_comm_can_rx_local_buff_au8[2];
			debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
															debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SEND_BIN_PGN:
		{
			debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 = DEBUG_COMM_BIN_MSG_SIZE;
			dbg_comm_multipacket_data_gpu8 = (U8*) &common_var_bms_id_log_gst.bin_au8[0];
			debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 = debug_comm_can_rx_local_buff_au8[2];
			debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
															debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SEND_VIN_PGN:
		{
			debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 = DEBUG_COMM_VIN_MSG_SIZE;
			dbg_comm_multipacket_data_gpu8 = (U8*) &common_var_bms_id_log_gst.vin_au8[0];
			debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 = debug_comm_can_rx_local_buff_au8[2];
			debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
															debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BATT_STS_PARAM_PGN:
		{
			debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 = DEBUG_COMM_BATT_PARAM_STS_MSG_SIZE;
			dbg_comm_multipacket_data_gpu8 = (U8*) &debug_comm_batt_sts_params_gst;
			debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 = debug_comm_can_rx_local_buff_au8[2];
			debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
															debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
#if DEBUG_COMM_FLASH_DATA_RETRIEVAL
		case DEBUG_COMM_FLASH_DATA_RETRIEVAL_PGN:
		{
			debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 = DEBUG_COMM_FLASH_RETRIVAL_MSG_SIZE;
			dbg_comm_multipacket_data_gpu8 = (U8*) &flsh_ret_debug_battery_data_log_gst;
			debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 = debug_comm_can_rx_local_buff_au8[2];
			debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
													debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

			/*if(flsh_ret_debug_battery_data_log_gst.time_stamp_u32 != 0xFFFF)
			{
				dbg_comm_multipacket_data_gpu8 = (U8*) &flsh_ret_debug_battery_data_log_gst;
				debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 = debug_comm_can_rx_local_buff_au8[2];
				debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
																debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
				if(debug_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
				{
					debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
				}
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			else
			{
				storage_task_flsh_ret_update_batt_data_st.start_addr_u32 = (storage_task_flsh_ret_update_batt_data_st.end_addr_u32+1);
				storage_task_flsh_ret_update_batt_data_st.end_addr_u32 = (storage_task_flsh_ret_update_batt_data_st.start_addr_u32 +
																		sizeof(bms_debug_battery_data_log_tst));
				xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_UPDATE_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}*/
			//data_log = (bms_debug_battery_data_log_tst *)dbg_comm_multipacket_data_gpu8;

		}break;
#endif
		/**********************************************************************************************************************/
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}
	debug_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 debug_comm_j1939_chck_mp_msg_u8(void)
{

	U8 dummy_byte_u8 = 0xAA;
	if(can_comm_set_timer_gst[DEBUG_SM].can_set_time_en_u8 == TIMEOUT_CLEAR)
	{
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_MULTIPACK_SEND_STATE;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		os_can_comm_queue_st.comm_event_e = OS_CAN_DEBUG_TX;
		os_can_comm_queue_st.can_rx_msg_pv = (U8 *)&dummy_byte_u8;
		xQueueSendFromISR(os_can_comm_queue_handler_ge,  &os_can_comm_queue_st, NULL );
	}
	return 0;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static U8 debug_comm_j1939_multipacket_can_msg_send_pru8(void)
{
	U8 return_status_u8 = 0;

	if(debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 <= debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8)
	{
		debug_comm_send_multipacket_data_gst.multipacket_buff_au8[0] = debug_comm_send_multipacket_data_gst.multipacket_request_count_u8;
		memory_copy_u8_array_v(&debug_comm_send_multipacket_data_gst.multipacket_buff_au8[1],
								(dbg_comm_multipacket_data_gpu8 + 7*(debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 - 1)),
								7);

		if(can_app_frame_and_tx_msg_u8 DEBUG_COMM_MULTIPACKET_DATA_FRAME)
		{
			if(debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 == debug_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8)
			{
				return_status_u8 = 0;
				return return_status_u8;
			}
			can_comm_set_tmr_v(DEBUG_SM,1);

			debug_comm_send_multipacket_data_gst.multipacket_request_count_u8 ++;
		}
		return_status_u8 = 1;
	}
	else   /* When EOF has not been received by BMS , last multipacket packet data which is previously sent will send it again to get EOF*/
	{
		if(can_app_frame_and_tx_msg_u8 DEBUG_COMM_MULTIPACKET_DATA_FRAME)
		{
			return_status_u8 = 0;
		}
	}
	return return_status_u8;

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void debug_comm_j1939_eof_msg_handling_prv(void)
{
	switch(debug_comm_can_rx_local_buff_au8[6])
	{
		/**********************************************************************************************************************/
		case DEBUG_COMM_ONE_TIME_INFO_PGN:
		{
			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_ONE_TIME_INFO_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SEND_BIN_PGN:
		{
			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_BIN_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SEND_VIN_PGN:
		{
			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_VIN_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BATT_STS_PARAM_PGN:
		{
			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_BATT_PARAM_STS_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
#if DEBUG_COMM_FLASH_DATA_RETRIEVAL
		case DEBUG_COMM_FLASH_DATA_RETRIEVAL_PGN:
		{
			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_FLASH_RETRIVAL_MSG_SIZE)
			{
				if(flsh_ret_debug_battery_data_log_gst.time_stamp_u32 == 0xFF)
				{
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
				}
				else
				{
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_DATA_RETRIVAL_STATE;
					os_can_queue_st.event_e = OS_CAN_DEBUG_RX;
					os_can_queue_st.can_rx_msg_pv = (U8 *)&dummy_data_u8;
					xQueueSend(os_can_comm_t9_qhandler_ge,  &os_can_queue_st, NULL );
				}
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
#endif
		/**********************************************************************************************************************/
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
