/**
====================================================================================================================================================================================
@page debug_comm_source_file

@subsection  Details

@n@b Title:
@n@b Filename:   	debug_comm.c
@n@b MCU:
@n@b Compiler:
@n@b Author:     	SHYAM

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#
@n@b Date:       	17-Jul-2020
@n@b Description:  	Created

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include  "bootloader_app.h"
#include "can_commtask.h"
#include "storage_task.h"
#include "storage_def.h"
#include "common_var.h"

/* Configuration Header File */
#include "bms_config.h"
#include "bms_afe_task.h"
#include "gt_rtc.h"
#include "rtc_int.h"
//#include "rtc_int.h"
#include "flash_drv.h"
#include "can_app.h"
#include "operating_system.h"
#include "queue.h"
/* #include "can_q_rx.h" */
/* This header file */
#include "debug_comm.h"
#include "client_comm.h"
/* Driver header */
#include "gt_memory.h"
#include "gt_system.h"
#include "gt_timer.h"
//#include "system_S32K144.h"
//#include "cstring"
#include "low_pwr_app.h"
#include "afe_driver.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
static void client_comm_send_ack_prv();
static void client_comm_auth_prv();
U32 client_gui_u32 = 0;
//static void debug_comm_update_warn_cutoff_param_prv();


static void client_comm_j1939_rts_send_prv(U8 pgn_aru8,U16 data_size_aru16);
static void display_comm_j1939_rts_send_prv(U8 pgn_aru8,U16 data_size_aru16);
static void client_comm_j1939_multipacket_can_msg_update_prv(client_comm_pgn_list_te msg_update_pgn_en);
static U8 client_comm_j1939_multipacket_can_msg_send_pru8(void);
static U8 display_comm_j1939_multipacket_can_msg_send_pru8(void);
static void client_comm_update_ot_batt_info_prv(void);
static void client_comm_update_periodic_batt_info_prv(void);
static void client_comm_update_periodic_sensor_info_prv(void);
static void client_comm_update_periodic_fault_info_prv(void);
static void client_comm_update_periodic_batt_sts_prv(void);
static void client_comm_j1939_eof_msg_handling_prv(void);
static void client_comm_set_date_time_prv(debug_comm_rtc_param_tst *client_comm_temp_rtc_param_arpst);
static void display_comm_period_mess_update_prv(void);
/** @{
 * Type definition */

/** @} */

/** @{
 * Private Variable Definitions */
U8 debug_comm_temp_bin_au8[DEBUG_COMM_BIN_LEN];
U8 debug_comm_temp_vin_au8[DEBUG_COMM_VIN_LEN];
U8 client_comm_can_rx_local_buff_au8[8] = {0,};
U8 display_comm_can_rx_local_buff_au8[8] = {0,};
U8 *cli_comm_multipacket_data_gpu8;
U8 debug_comm_flash_operation_sts_u8;
U32 client_comm_mac_gui_u32;

U16 temp_cb_ctrl_au16[DEBUG_COMM_TOTAL_SLAVES];

debug_comm_rtc_param_tst				debug_comm_temp_rtc_param_st;
client_comm_send_multipacket_data_tst 	client_comm_send_multipacket_data_gst;
debug_comm_rts_tst 	 					client_comm_rts_st;
can_app_message_tst 							client_comm_tx_msg_st;
debug_comm_rtc_param_tst				debug_comm_temp_rtc_param_st;

/* message type enums are defined here */
client_comm_one_time_mp_req_te client_comm_one_time_mp_req_gen;
client_comm_periodic_mp_req_te client_comm_periodic_mp_req_gen;

/* common structure for all the data */
client_comm_data_update_tst client_comm_data_update_gst;
display_param_period_tst  display_param_period_gst;
/** @} */

/** @{
 *  Public Variable Definitions */


//ebug_comm_imu_param_tst 			debug_comm_imu_param_gst;
//debug_comm_moisture_param_tst 		debug_comm_moisture_param_gst;
debug_comm_slv_flt_sts_tst 			debug_comm_slave_flt_sts_gst;
debug_comm_sys_flt_sts_tst 			debug_comm_sys_flt_sts_gst;
debug_comm_basic_batt_info_tst 		debug_comm_basic_batt_info_gst;  /* One time information */
debug_comm_batt_sts_params_tst 		debug_comm_batt_sts_params_gst;
debug_comm_rtc_param_tst			client_comm_temp_rtc_param_st;

storage_task_update_batt_data_tst 	debug_com_update_batt_data_st;
os_storage_queue_tst 				debug_comm_ext_flash_queue_st;

/** @} */

/* Public Function Definitions */
void client_comm_param_init_v();

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void client_comm_param_init_v()
{
//	client_comm_data_update_gst.batt_pack_info_gst.bms_firmware_version_num_u16 = 90;
	client_comm_data_update_gst.batt_pack_info_gst.bms_hw_version_num_u16 = 80;
	client_comm_data_update_gst.batt_pack_info_gst.bms_pk_version_num_u16 = 70;
//	client_comm_data_update_gst.batt_pack_info_gst.bms_protocol_version_num_u16 = 60;
	client_comm_data_update_gst.batt_pack_info_gst.bms_platform_no_u8 = 1;
//	client_comm_data_update_gst.batt_pack_info_gst.slave_hw_version_num_u16 = 99;
//	client_comm_data_update_gst.batt_pack_info_gst.gprs_version_num_u8 = 45;
//	client_comm_data_update_gst.batt_pack_info_gst.gprs_imei_num_u64 = 83;
	client_comm_data_update_gst.batt_pack_info_gst.gps_version_num_u8 = 66;
	client_comm_data_update_gst.batt_pack_info_gst.ble_version_num_u8 = 55;
//	client_comm_data_update_gst.batt_pack_info_gst.ble_mac_addr_u64 = 50;
	//client_comm_data_update_gst.batt_pack_info_gst.batt_supplier_u8 = 1;
	//client_comm_data_update_gst.batt_pack_info_gst.batt_customer_u8 = 45;
	client_comm_data_update_gst.batt_pack_info_gst.date_of_manf_u32 = 77;
	client_comm_data_update_gst.batt_pack_info_gst.pack_dod_u16 = 93;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ov_warning_u16 = bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_uv_warning_u16 = bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_uv_threshold_u16 = bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ov_threshold_u16 = bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16;

	/* has to be changed in future */
	client_comm_data_update_gst.batt_pack_info_gst.cell_deep_dsg_threshold_u16 = bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_deep_dsg_warning_u16 = bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ov_flt_region_u16 = 77;
	client_comm_data_update_gst.batt_pack_info_gst.cell_uv_flt_region_u16 = 28;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_ov_threshold_u16 = bms_config_nv_cell_cfg_gst.release_max_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_uv_threshold_u16 = 3700;
	client_comm_data_update_gst.batt_pack_info_gst.pack_ov_warning_u16 = bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uv_warning_u16 = 42;
	//													bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.release_pack_ov_threshold_u16 = 89;
	//													bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.release_pack_uv_threshold_u16	= 45;
	//													bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.pack_ov_flt_region_u16 = 66;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uv_flt_region_u16 = 96;
	client_comm_data_update_gst.batt_pack_info_gst.pack_ov_threshold_u16 =  95;
	//													bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uv_threshold_u16 = 40;
	//													bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16;

	//													bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ot_warning_u16 = 265;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ut_warning_u16 = 245;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ot_threshold_u16 = 263;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ut_threshold_u16 = 262;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ot_flt_region_u16 = 259;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ut_flt_region_u16 = 261;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_ot_threshold_u16 = 268;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_ut_threshold_u16 = 249;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ot_warning_u16 = 272;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ut_warning_u16 = 279;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ot_threshold_u16 = 232;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ut_threshold_u16 = 235;
	client_comm_data_update_gst.batt_pack_info_gst.release_amb_ot_threshold_u16 = 209;
	client_comm_data_update_gst.batt_pack_info_gst.release_amb_ut_threshold_u16 = 207;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ot_warning_u16 = 229;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ut_warning_u16 = 231;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ot_threshold_u16 = 181;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ut_threshold_u16 = 171;
	client_comm_data_update_gst.batt_pack_info_gst.release_mfet_ot_threshold_u16 = 188;
	client_comm_data_update_gst.batt_pack_info_gst.release_mfet_ut_threshold_u16 = 189;
	client_comm_data_update_gst.batt_pack_info_gst.warn_max_chg_1ma_s32 = 115;
	client_comm_data_update_gst.batt_pack_info_gst.warn_max_dsg_1ma_s32 = 118;
	client_comm_data_update_gst.batt_pack_info_gst.max_chg_1ma_s32 = 121;
	client_comm_data_update_gst.batt_pack_info_gst.max_dsg_1ma_s32 = 126;
	client_comm_data_update_gst.batt_pack_info_gst.warn_max_scd_surge_1ma_s32 = 128;
	client_comm_data_update_gst.batt_pack_info_gst.chg_1ma_flt_region_u16 = 82;
	client_comm_data_update_gst.batt_pack_info_gst.dchg_1ma_flt_region_u16 = 84;
	client_comm_data_update_gst.batt_pack_info_gst.pack_oh_warning_u16 = 141;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uh_warning_u16 = 142;
	client_comm_data_update_gst.batt_pack_info_gst.pack_oh_threshold_u16 = 143;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uh_threshold_u16 = 144;
	client_comm_data_update_gst.batt_pack_info_gst.pack_oh_release_u16 = 145;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uh_release_u16 = 146;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void client_comm_canrx_process_v(can_comm_task_can_msg_tst* rx_can_message_arpst)
{
	U32 can_received_msg_id_u32 = 0;
	U8 can_data_length_u8 = 0;
	U8 j1939_pgn_u8 = 0;
	//mcp7940m_time_date_tst	rtc_set_dt_tm_lst;

if(client_gui_u32)
{
	can_received_msg_id_u32 = rx_can_message_arpst->id_u32;
	can_data_length_u8 = rx_can_message_arpst->length_u8;
	memory_copy_u8_array_v(&client_comm_can_rx_local_buff_au8[0], rx_can_message_arpst->data_au8, can_data_length_u8);

	j1939_pgn_u8 = CAN_APP_GET_PGN(can_received_msg_id_u32);

	switch (j1939_pgn_u8)
	{
		/**********************************************************************************************************************/
		case CLIENT_COMM_START_STOP_PGN:
		{
			/* Start/Stop the debug mode from UI */
			if(0xAA == rx_can_message_arpst->data_au8[0])
			{
	//				debug_comm_transmit_gb = 1; /* why used */
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_START_ACK_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			else if(0xFF == rx_can_message_arpst->data_au8[0])
			{
	//				debug_comm_transmit_gb = 0;
			}
		}
		break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_AUTH_PGN:
		{
			client_comm_mac_gui_u32 = (client_comm_can_rx_local_buff_au8[3] <<24 |
										client_comm_can_rx_local_buff_au8[2] <<16 |
										client_comm_can_rx_local_buff_au8[1] <<8 |
										client_comm_can_rx_local_buff_au8[0]);
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_AUTH_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BIN_VIN_MAC_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_TIME_INFO_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_one_time_mp_req_gen = BIN_VIN_MAC_REQ;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_FLASH_RET_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_TIME_INFO_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_one_time_mp_req_gen = FLSH_DATA_RET_REQ;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_SLEEP_PGN:
		{
			/* Sleep/Wake the MCU from UI */
			if(0xAA == client_comm_can_rx_local_buff_au8[0])
			{
				/* Implement MCU sleep here*/
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CAN;
				//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SLEEP_ACK_STATE;
				//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			else if(0xFF == client_comm_can_rx_local_buff_au8[0])
			{
				/* Implement MCU wake here*/
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_CAN;
				//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SLEEP_ACK_STATE;
				//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BATT_ONE_TIME_INFO_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_TIME_INFO_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_one_time_mp_req_gen = BATT_OT_INFO_REQ;
			}
		}break;

		/**********************************************************************************************************************/
		case CLIENT_COMM_BATT_PERIODIC_INFO_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_SEC_PARAM_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_periodic_mp_req_gen = ONE_SEC_BATT_MESS_REQ;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_SENSOR_PERIODIC_INFO_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_SEC_PARAM_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_periodic_mp_req_gen = ONE_SEC_SENSOR_MESS_REQ;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_FAULT_PERIODIC_INFO_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_SEC_PARAM_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_periodic_mp_req_gen = ONE_SEC_FAULT_MESS_REQ;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BATT_STATUS_PERIODIC_PGN:
		{
			if(rx_can_message_arpst->data_au8[0] == 0xAA)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_SEND_ONE_SEC_PARAM_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
				client_comm_periodic_mp_req_gen = ONE_SEC_BATT_STATUS_REQ;
			}
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_RTC_SET_PGN:
		{
			memory_copy_u8_array_v((U8*)&client_comm_temp_rtc_param_st, &client_comm_can_rx_local_buff_au8[0] ,
													sizeof(client_comm_temp_rtc_param_st));

			client_comm_temp_rtc_param_st.year_u16 = (client_comm_can_rx_local_buff_au8[5] + 2000);
			client_comm_set_date_time_prv(&client_comm_temp_rtc_param_st);
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_SET_BIN_PGN:
		{
			/*memory_copy_u8_array_v(&bin_id_no_u8[0], &client_comm_can_rx_local_buff_au8[1], 7);
			debug_comm_set_bin_prv(BIN_MSG_1);

			memory_copy_u8_array_v(&bin_id_no_u8[7], &client_comm_can_rx_local_buff_au8[1], 7);
			debug_comm_set_bin_prv(BIN_MSG_2);

			memory_copy_u8_array_v(&bin_id_no_u8[15], &client_comm_can_rx_local_buff_au8[1], 7);
			debug_comm_set_bin_prv(BIN_MSG_1);

			if(client_comm_can_rx_local_buff_au8[0] == 1)
			{

			}
			if(client_comm_can_rx_local_buff_au8[0] == 2)
			{
				debug_comm_set_bin_prv(BIN_MSG_2);
			}
			if(client_comm_can_rx_local_buff_au8[0] == 3)
			{
				debug_comm_set_bin_prv(BIN_MSG_3);
			}*/
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_RTS_CTS_EOF_PGN:
		{
			/*** J1939  MULTIPCAKET CTS HANDLE ***/
			if(rx_can_message_arpst->data_au8[0] == CLIENT_COMM_CTS_CONTROLBYTE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
			}
			/*** J1939  MULTIPCAKET EOF HANDLE ***/
			else if(rx_can_message_arpst->data_au8[0] == CLIENT_COMM_EOF_CONTROLBYTE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8  = CLIENT_COMM_MULTIPACK_EOF_MSG_STATE;
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8  = CAN_APP_STATE_TX;
			}
			/*** J1939  MULTIPCAKET RTS HANDLE ***/
			else if(rx_can_message_arpst->data_au8[0] == CLIENT_COMM_RTS_CONTROLBYTE)
			{
				client_comm_j1939_multipacket_can_msg_update_prv(rx_can_message_arpst->data_au8[6]);
			}
		}break;
		/**********************************************************************************************************************/
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}

}
}

static void client_comm_set_date_time_prv(debug_comm_rtc_param_tst *client_comm_temp_rtc_param_arpst)
{
	rtc_int_set_time_date_u8(client_comm_temp_rtc_param_arpst);
	rtc_int_get_time_date_u8(client_comm_temp_rtc_param_arpst);
}
#if 0
void debug_comm_rx_msg_v(can_app_message_tst * rx_can_message_arpst)
{
#if 0
	if(debug_comm_can_rx_gst.id_u32 != NULL)
	{
		memory_set_u8_array_v(debug_comm_can_rx_gst.data_au8, 0, 8);
		debug_comm_can_rx_gst.id_u32 = NULL;

	}
	/* if COM_HDR_TRUE, then the rx queue is empty*/
	if(COM_HDR_TRUE != can_rx_array_queue_get_u8(debug_comm_can_rx_gst.data_au8, &debug_comm_can_rx_gst.id_u32))
	{
#endif

		if(rx_can_message_arpst->can_id_u32 != NULL)
		{
			switch(rx_can_message_arpst->can_id_u32)
			{
				/**********************************************************************************************************************/
				case DEBUG_COMM_START_STOP_CMD:
				{
					/* Start/Stop the debug mode from UI */
					if(0xAA == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_transmit_gb = 1;
					}
					else if(0xFF == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_transmit_gb = 0;
					}
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_BIN1_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_1], rx_can_message_arpst->data_au8,
																			DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_bin_prv(BIN_MSG_1);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_BIN2_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_2], rx_can_message_arpst->data_au8,
																			DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_bin_prv(BIN_MSG_2);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_BIN3_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_bin_au8[BIN_MSG_3],rx_can_message_arpst->data_au8,
																		(DEBUG_COMM_BIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD));
					debug_comm_set_bin_prv(BIN_MSG_3);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_VIN1_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_1], rx_can_message_arpst->data_au8,
																		DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_vin_prv(VIN_MSG_1);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_VIN2_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_2], rx_can_message_arpst->data_au8,
																		DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD);
					debug_comm_set_vin_prv(VIN_MSG_2);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_VIN3_CMD:
				{
					memory_copy_u8_array_v(&debug_comm_temp_vin_au8[VIN_MSG_3],rx_can_message_arpst->data_au8,
																		(DEBUG_COMM_VIN_LEN % DEBUG_COMM_MAX_BYTES_PER_MSG_FRAME_ALWD));
					debug_comm_set_vin_prv(VIN_MSG_3);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_MAC_CMD:
				{
					memory_copy_u8_array_v(debug_comm_mac_gau8, rx_can_message_arpst->data_au8, DEBUG_COMM_MAC_LEN);
					debug_comm_set_mac_prv();
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SET_RTC_CMD:
				{
					memory_copy_u8_array_v((U8*)&debug_comm_temp_rtc_param_st, rx_can_message_arpst->data_au8 ,
																		sizeof(debug_comm_temp_rtc_param_st));
					debug_comm_set_date_time_prv(&debug_comm_temp_rtc_param_st);
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_AFE_CB_CTRL1_CMD:
				{
					U8 cb_data_length_u8;
					#if DEBUG_COMM_SEND_AFE_CB_STS
					if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_1_4)
					{
						cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) > 8) ? (8) : (DEBUG_COMM_TOTAL_SLAVES * 2);
						memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_1_4], rx_can_message_arpst->data_au8 ,
																											cb_data_length_u8);

						debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_1_4);

						debug_comm_update_afe_cb_sts_prv();

						debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_1_4);

					}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_AFE_CB_CTRL2_CMD:
				{
					U8 cb_data_length_u8;
					#if DEBUG_COMM_SEND_AFE_CB_STS
					if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_5_8)
					{
						cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) > 16) ? (8) : ((DEBUG_COMM_TOTAL_SLAVES * 2) % 8);
						memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_5_8], rx_can_message_arpst->data_au8 ,
																												cb_data_length_u8);

						debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_5_8);

						debug_comm_update_afe_cb_sts_prv();

						debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_5_8);
					}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_AFE_CB_CTRL3_CMD:
				{
					U8 cb_data_length_u8;
					#if DEBUG_COMM_SEND_AFE_CB_STS
					if(DEBUG_COMM_TOTAL_SLAVES > DEBUG_COMM_CB_SLV_9)
					{
						cb_data_length_u8 = ((DEBUG_COMM_TOTAL_SLAVES * 2) % 8);
						memory_copy_u8_array_v((U8*)&temp_cb_ctrl_au16[DEBUG_COMM_CB_SLV_9], rx_can_message_arpst->data_au8 ,
																										cb_data_length_u8);

						debug_comm_set_afe_cb_ctrl_prv(DEBUG_COMM_CB_SLV_9);

						debug_comm_update_afe_cb_sts_prv();

						debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_9);
					}
					#endif
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_SLEEP_WAKE_CMD:
				{
					/* Sleep/Wake the MCU from UI */
					if(0xAA == rx_can_message_arpst->data_au8[0])
					{
						/* Implement MCU sleep here*/
					}
					else if(0xFF == rx_can_message_arpst->data_au8[0])
					{
						/* Implement MCU wake here*/
					}
				}
				break;
				/**********************************************************************************************************************/
				case DEBUG_COMM_BIN_VIN_MAC_RQT:
				{
					/* Rqt Bin Vin Mac */
					if(0xAA == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_send_bin_prv();
					}
					else if(0xBB == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_send_vin_prv();
					}
					else if(0xCC == rx_can_message_arpst->data_au8[0])
					{
						debug_comm_send_mac_prv();
					}
				}
				break;
				/**********************************************************************************************************************/
				default :
				{

				}
				break;
				/**********************************************************************************************************************/
			}

		}

		rx_can_message_arpst->can_id_u32 = NULL;
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void client_comm_cantx_process_v(can_comm_task_can_msg_tst* tx_can_message_arg_st)
{
	U32 can_received_msg_id_u32 = 0;
	U8 can_data_length_u8 = 0;
	static U32 flsh_retrival_size_u32 = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;
	can_received_msg_id_u32 = tx_can_message_arg_st->id_u32;
	can_data_length_u8 = tx_can_message_arg_st->length_u8;
	memory_copy_u8_array_v(&client_comm_can_rx_local_buff_au8[0], tx_can_message_arg_st->data_au8,
																		can_data_length_u8);

	if(CAN_APP_STATE_TX == common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8)
	{
		switch (common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8)
		{
			/**********************************************************************************************************************/
			case CLIENT_COMM_START_ACK_STATE:
			{
				client_comm_send_ack_prv(); /* must include #define frame */
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			break;
			/**********************************************************************************************************************/
			/* TODO: For authentication */
			case CLIENT_COMM_AUTH_ACK_STATE:
			{
				client_comm_auth_prv();
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}break;

			case CLIENT_COMM_SEND_ONE_TIME_INFO_STATE:
			{
				switch(client_comm_one_time_mp_req_gen)
				{
					case BIN_VIN_MAC_REQ:
					{
						client_comm_j1939_rts_send_prv(CLIENT_COMM_BIN_VIN_MAC_PGN,CLIENT_COMM_VIN_BIN_MAC_MSG_SIZE);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_BIN_VIN_MAC_PGN);
					}break;
					/**********************************************************************************************************************/
					case FLSH_DATA_RET_REQ:
					{
						client_comm_j1939_rts_send_prv(CLIENT_COMM_FLASH_RET_PGN,CLIENT_COMM_FLASH_RETRIVAL_MSG_SIZE);
						os_storage_queue_st.event_e = OS_STORAGE_DATA_RETRIVAL;
						os_storage_queue_st.mode_e = OS_FLASH_MODE_RD;
						storage_task_flsh_ret_update_batt_data_st.start_addr_u32 = flsh_retrival_size_u32;
						storage_task_flsh_ret_update_batt_data_st.end_addr_u32 = (storage_task_flsh_ret_update_batt_data_st.start_addr_u32 +
																				sizeof(bms_debug_battery_data_log_tst));
						flsh_retrival_size_u32 = storage_task_flsh_ret_update_batt_data_st.end_addr_u32;
						xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_FLASH_RET_PGN);
					}break;
					/**********************************************************************************************************************/
					case BATT_OT_INFO_REQ:
					{
						client_comm_j1939_rts_send_prv(CLIENT_COMM_BATT_ONE_TIME_INFO_PGN,CLIENT_COMM_OT_BATT_INFO_MSG_SIZE);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_BATT_ONE_TIME_INFO_PGN);
					}break;
				}
			}break;
			/**********************************************************************************************************************/
			case CLIENT_COMM_SEND_ONE_SEC_PARAM_STATE:
			{
				switch(client_comm_periodic_mp_req_gen)
				{
					/**********************************************************************************************************************/
					case ONE_SEC_BATT_MESS_REQ:
					{
						/* size has to be included when structure formed */
						client_comm_j1939_rts_send_prv(CLIENT_COMM_BATT_PERIODIC_INFO_PGN,CLIENT_COMM_BATT_PARAM_MSG_SIZE);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_BATT_PERIODIC_INFO_PGN);
					}break;
					/**********************************************************************************************************************/
					case ONE_SEC_SENSOR_MESS_REQ:
					{
						/* size has to be included when structure formed */
						client_comm_j1939_rts_send_prv(CLIENT_COMM_SENSOR_PERIODIC_INFO_PGN,CLIENT_COMM_SENSOR_PARAM_MSG_SIZE);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_SENSOR_PERIODIC_INFO_PGN);
					}break;
					/**********************************************************************************************************************/
					case ONE_SEC_FAULT_MESS_REQ:
					{
						/* size has to be included when structure formed */
						client_comm_j1939_rts_send_prv(CLIENT_COMM_FAULT_PERIODIC_INFO_PGN,CLIENT_COMM_FAULT_STATUS_MSG_SIZE);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_FAULT_PERIODIC_INFO_PGN);
					}break;
					/**********************************************************************************************************************/
					case ONE_SEC_BATT_STATUS_REQ:
					{
						/* size has to be included when structure formed */
						client_comm_j1939_rts_send_prv(CLIENT_COMM_BATT_STATUS_PERIODIC_PGN,CLIENT_COMM_BATT_STATUS_MSG_SIZE);
						client_comm_j1939_multipacket_can_msg_update_prv(CLIENT_COMM_BATT_STATUS_PERIODIC_PGN);
					}break;
				}
			}break;
			/**********************************************************************************************************************/
			case CLIENT_COMM_MULTIPACK_SEND_STATE:
			{
				client_comm_j1939_multipacket_can_msg_send_pru8();
			}
			break;
			/**********************************************************************************************************************/
			case CLIENT_COMM_MULTIPACK_EOF_MSG_STATE:
			{
				client_comm_j1939_eof_msg_handling_prv();
			}
			break;
			/**********************************************************************************************************************/
			case CLIENT_COMM_SLEEP_ACK_STATE:
			{
				if(0xFF == client_comm_can_rx_local_buff_au8[0])
				{
					memory_set_u8_array_v(client_comm_tx_msg_st.data_au8,0,8);
					client_comm_tx_msg_st.data_au8[0] = 0xFF;
					client_comm_tx_msg_st.can_id_u32 = CLIENT_COMM_SLEEP_ACK_RES;
					client_comm_tx_msg_st.length_u8 = 1;

					if(can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &client_comm_tx_msg_st))
					{

					}
					else
					{
						common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
					}
				}
				if(0xAA == client_comm_can_rx_local_buff_au8[0])
				{
					memory_set_u8_array_v(client_comm_tx_msg_st.data_au8,0,8);
					client_comm_tx_msg_st.data_au8[0] = 0xAA;
					client_comm_tx_msg_st.can_id_u32 = CLIENT_COMM_SLEEP_ACK_RES;
					client_comm_tx_msg_st.length_u8 = 1;
					if(can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &client_comm_tx_msg_st))
					{

					}
					else
					{
						common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
					}
				}
			}break;
			/**********************************************************************************************************************/
			default :
			{

			}
			break;
			/**********************************************************************************************************************/
		}
	}
}






#if 0
void debug_comm_tx_msg_v()
{
	if(1 == debug_comm_transmit_gb)
	{
		/* Timer check condition */
		/* if(debug_comm_1sec_tmr_gu8 > 0) */  /* TODO: Should uncomment this when RTOS is not used */
		{
			/* Resets the timer flag */
			debug_comm_1sec_tmr_gu8 = 0;

			#if DEBUG_COMM_SEND_CELL_VTG
			/* Sends cell voltages */
			debug_comm_send_cell_vtg_prv();
			#endif

			#if DEBUG_COMM_SEND_CELL_TEMP
			/* Sends cells Temperature */
			debug_comm_send_cell_temp_prv();
			#endif

			#if DEBUG_COMM_SEND_HVADC_BENDER
			/* Sends HVADC measurements */
			debug_comm_send_hvadc_bend_prv();
			#endif

			#if DEBUG_COMM_SEND_MCU_ADC
			/* Sends mcu adc measurements */
			debug_comm_send_mcu_adc_prv();
			#endif

			#if DEBUG_COMM_SEND_CURR_SBC
			/* Sends current and sbc measurements */
			debug_comm_send_curr_sbc_data_prv();
			#endif

			#if DEBUG_COMM_SEND_TMS
			/* Sends TMS(Thermal Management System) measurements */
			debug_comm_send_tms_prv();
			#endif

			#if DEBUG_COMM_SEND_MOISTURE_IMU
			/* Sends Moisture(Humidity & Temperature) sensor measurements */
			debug_comm_send_imu_mois_data_prv();
			#endif

			#if DEBUG_COMM_SEND_RTC_AMB_LIGHT
			/* Sends Ambient Light sensor measurements */
			debug_comm_send_rtc_amb_lght_prv();
			#endif

			#if DEBUG_COMM_SEND_SYS_FLT_STS
			/* Sends system fault status */
			debug_comm_send_sys_flt_sts_prv();
			#endif

			#if DEBUG_COMM_SEND_AFE_SLV_FLT_STS
			/* Sends afe slave fault status */
			debug_comm_send_afe_slave_flt_sts_prv();
			#endif

			#if DEBUG_COMM_SEND_AFE_CB_STS
			/* Sends AFE Cell Balancing status */
			debug_comm_send_afe_cb_sts_prv(DEBUG_COMM_CB_SLV_ALL);
			#endif

			#if DEBUG_COMM_SEND_MAX_MIN_CPARAMS
			/* Sends Max Min Cell parameters */
			debug_comm_send_max_min_cparams_prv();
			#endif
		}
	}
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void client_comm_send_ack_prv()
{
//	debug_comm_tx_msg_st.data = {0};

	memory_set_u8_array_v(client_comm_tx_msg_st.data_au8,0,8);
	client_comm_tx_msg_st.data_au8[0] = 0xAA;
	client_comm_tx_msg_st.can_id_u32 = CLIENT_COMM_START_STOP_RES;
	client_comm_tx_msg_st.length_u8 = 1;
	if(can_app_frame_and_tx_msg_u8 CLIENT_COMM_START_STOP_FRAME)
	{
    	common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
	}
//	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &debug_comm_tx_msg_st);
}

/*
*
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================

static void debug_comm_update_warn_cutoff_param_prv()
{
	 @Version number update happens here
	debug_comm_basic_batt_info_gst.bms_hw_version_num_u16 			= batt_param_batt_pack_details_gst.bms_hw_version_num_u16;
	debug_comm_basic_batt_info_gst.bms_pk_version_num_u16 			= batt_param_batt_pack_details_gst.bms_pk_version_num_u16;
	debug_comm_basic_batt_info_gst.slave_hw_version_num_u16 		= batt_param_batt_pack_details_gst.slave_hw_version_num_u16;
	debug_comm_basic_batt_info_gst.bms_firmware_version_num_u16 	= batt_param_batt_pack_details_gst.bms_firmware_version_num_u16;
	debug_comm_basic_batt_info_gst.bms_protocol_version_num_u16 	= batt_param_batt_pack_details_gst.bms_protocol_version_num_u16;

	 @Batt manufacture details update happens here
	debug_comm_basic_batt_info_gst.batt_supplier_u8 				= batt_param_batt_pack_details_gst.batt_supplier_u8;
	debug_comm_basic_batt_info_gst.batt_customer_u8 				= batt_param_batt_pack_details_gst.batt_customer_u8;
	debug_comm_basic_batt_info_gst.date_of_manf_u32 				= batt_param_batt_pack_details_gst.date_of_manf_u32;

	 @Batt specifications update happens here
	debug_comm_basic_batt_info_gst.pack_dod_u16 					= batt_param_batt_pack_details_gst.pack_dod_u16;
	debug_comm_basic_batt_info_gst.rated_pack_cycles_u16 			= batt_param_batt_pack_details_gst.rated_pack_cycles_u16;
	debug_comm_basic_batt_info_gst.nominal_voltage_u16 				= batt_param_batt_pack_details_gst.nominal_voltage_u16;
	debug_comm_basic_batt_info_gst.rated_capacity_u16 				= batt_param_batt_pack_details_gst.rated_capacity_u16;
	debug_comm_basic_batt_info_gst.cell_type_u16 					= batt_param_batt_pack_details_gst.cell_type_u16;
	debug_comm_basic_batt_info_gst.total_cells_u16 					= batt_param_batt_pack_details_gst.total_cells_u16;
	//debug_comm_basic_batt_info_gst.number_of_slaves_u16 			= batt_param_batt_pack_details_gst.number_of_slaves_u16;

	 @Cell voltage level update happens here
	debug_comm_basic_batt_info_gst.cell_ov_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_uv_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_ov_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_uv_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.release_cell_ov_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_max_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.release_cell_uv_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_deep_dsg_warning_u16     	= bms_config_nv_warn_mon_gst.warn_deep_discharge_1mV_u16;
	debug_comm_basic_batt_info_gst.cell_deep_dsg_threshold_u16 		= bms_config_nv_cell_cfg_gst.deep_dsg_cell_1mV_u16;

	 @Pack voltage level update happens here
	debug_comm_basic_batt_info_gst.pack_ov_warning_u16 				= bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.pack_uv_warning_u16 				= bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.pack_ov_threshold_u16 			= bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.pack_uv_threshold_u16 			= bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.release_pack_ov_threshold_u16 	= bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16;
	debug_comm_basic_batt_info_gst.release_pack_uv_threshold_u16 	= bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16;

	 @Cell temperature level update happens here
	debug_comm_basic_batt_info_gst.cell_ot_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.cell_ut_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.cell_ot_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.cell_ut_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.release_cell_ot_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16;
	debug_comm_basic_batt_info_gst.release_cell_ut_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_cell_1cc_s16;

	 @Ambient temperature level update happens here
	debug_comm_basic_batt_info_gst.amb_ot_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_amb_1cc_s16;
	debug_comm_basic_batt_info_gst.amb_ut_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_amb_1cc_s16;
	debug_comm_basic_batt_info_gst.amb_ot_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_pack_1cc_s16;
	debug_comm_basic_batt_info_gst.amb_ut_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_pack_1cc_s16;
	debug_comm_basic_batt_info_gst.release_amb_ot_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_max_pack_1cc_s16;
	debug_comm_basic_batt_info_gst.release_amb_ut_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_pack_1cc_s16;

	 @Mosfet temperature level update happens here
	debug_comm_basic_batt_info_gst.mfet_ot_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.mfet_ut_warning_u16 				= bms_config_nv_warn_mon_gst.warn_min_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.mfet_ot_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_max_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.mfet_ut_threshold_u16 			= bms_config_nv_cell_cfg_gst.abs_min_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.release_mfet_ot_threshold_u16	= bms_config_nv_cell_cfg_gst.release_max_mosfet_1cc_s16;
	debug_comm_basic_batt_info_gst.release_mfet_ut_threshold_u16 	= bms_config_nv_cell_cfg_gst.release_min_mosfet_1cc_s16;

	 @Packet current level update happens here
	debug_comm_basic_batt_info_gst.max_chg_1ma_s32 					= bms_config_nv_pi_cfg_gst.max_chg_1mA_s32;
	debug_comm_basic_batt_info_gst.max_dsg_1ma_s32 					= bms_config_nv_pi_cfg_gst.max_dsg_1mA_s32;
	debug_comm_basic_batt_info_gst.warn_max_chg_1ma_s32 			= bms_config_nv_warn_mon_gst.warn_max_chg_1mA_s32;
	debug_comm_basic_batt_info_gst.warn_max_dsg_1ma_s32 			= bms_config_nv_warn_mon_gst.warn_max_dsg_1mA_s32;
	debug_comm_basic_batt_info_gst.warn_max_scd_surge_1ma_s32 		= bms_config_nv_warn_mon_gst.warn_max_scd_surge_1mA_s32;

	 @Ambient(or)Pack Humidity level update happens here
	debug_comm_basic_batt_info_gst.pack_oh_warning_u16 				= bms_config_nv_warn_mon_gst.warn_max_pack_hum_u16;
	debug_comm_basic_batt_info_gst.pack_uh_warning_u16				= bms_config_nv_warn_mon_gst.warn_min_pack_hum_u16;
	debug_comm_basic_batt_info_gst.pack_oh_threshold_u16			= bms_config_nv_cell_cfg_gst.abs_max_pack_hum_u16;
	debug_comm_basic_batt_info_gst.pack_uh_threshold_u16			= bms_config_nv_cell_cfg_gst.abs_min_pack_hum_u16;

}
*/

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void client_comm_j1939_rts_send_prv(U8 pgn_aru8,U16 data_size_aru16)
{
    U8 multi_pkt_frame_size_u8 = 0;

    client_comm_rts_st.control_byte_u8 = DEBUG_COMM_RTS_CONTROLBYTE;
    client_comm_rts_st.total_byte_u16  = data_size_aru16;
    multi_pkt_frame_size_u8			  = data_size_aru16 /7;

    if(data_size_aru16 % 7 != 0)
        multi_pkt_frame_size_u8++;

    client_comm_rts_st.total_packet_u8 = multi_pkt_frame_size_u8;
    client_comm_rts_st.max_packet_u8   = multi_pkt_frame_size_u8;
    client_comm_rts_st.pgn_au8[0] 	  = 0;
    client_comm_rts_st.pgn_au8[1] 	  = pgn_aru8;
    client_comm_rts_st.pgn_au8[2] 	  = 0;

    if(can_app_frame_and_tx_msg_u8 CLIENT_COMM_RTS_FRAME )
    {
    	#if 0		/* TODO: If we need to take care of timeout scenarious - enable this & rethink the logic */
    	can_app_comm_task_set_timer_v(COMMON_VAR_DEBUG_SM,DEBUG_COMM_RTS_TIMEOUT,DEBUG_COMM_RTS_CTS_EOF_PGN);
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_WAIT;
    	#endif

    	common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
    }
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void display_comm_j1939_rts_send_prv(U8 pgn_aru8,U16 data_size_aru16)
{
#if CLIENT_COMM_DISPLAY_USE
    U8 multi_pkt_frame_size_u8 = 0;

    client_comm_rts_st.control_byte_u8 = DEBUG_COMM_RTS_CONTROLBYTE;
    client_comm_rts_st.total_byte_u16  = data_size_aru16;
    multi_pkt_frame_size_u8			  = data_size_aru16 /7;

    if(data_size_aru16 % 7 != 0)
        multi_pkt_frame_size_u8++;

    client_comm_rts_st.total_packet_u8 = multi_pkt_frame_size_u8;
    client_comm_rts_st.max_packet_u8   = multi_pkt_frame_size_u8;
    client_comm_rts_st.pgn_au8[0] 	  = 0;
    client_comm_rts_st.pgn_au8[1] 	  = pgn_aru8;
    client_comm_rts_st.pgn_au8[2] 	  = 0;

    if(can_app_frame_and_tx_msg_u8 DISPLAY_COMM_RTS_FRAME )
    {
    	#if 0		/* TODO: If we need to take care of timeout scenarious - enable this & rethink the logic */
    	can_app_comm_task_set_timer_v(COMMON_VAR_DEBUG_SM,DEBUG_COMM_RTS_TIMEOUT,DEBUG_COMM_RTS_CTS_EOF_PGN);
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_WAIT;
    	#endif

    	common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
    }
#endif
}
static void display_comm_j1939_multipacket_can_msg_update_prv(display_comm_pgn_list_te msg_update_pgn_en)
{

#if CLIENT_COMM_DISPLAY_USE
	switch(msg_update_pgn_en)
	{
		/**********************************************************************************************************************/
		case DISPLAY_COMM_START_STOP_PGN:
		{

		}break;
		/**********************************************************************************************************************/
		case DISPLAY_COMM_PARAM_REQ_PGN:
		{
			display_comm_period_mess_update_prv();
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 = sizeof(display_param_period_tst);
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = DISPLAY_COMM_PARAM_REQ_PGN;
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
														client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			cli_comm_multipacket_data_gpu8 = (U8*) &display_param_period_gst;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}

		}break;
	}
	can_comm_set_tmr_v(DISPLAY_SM,10);
#endif
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void client_comm_j1939_multipacket_can_msg_update_prv(client_comm_pgn_list_te msg_update_pgn_en)
{
	switch(msg_update_pgn_en)
	{
		/**********************************************************************************************************************/
		case CLIENT_COMM_START_STOP_PGN:
		{

		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_AUTH_PGN:
		{

		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BIN_VIN_MAC_PGN:
		{
			//0x47 0x54 0x30 0x31 0x52 0x4e 0x37 0x48 0x41 0x31 0x32 0x33 0x34 0x35 0x44 0x49 0x4e

			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[0] = 0x47;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[1] = 0x54;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[2] = 0x30;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[3] = 0x31;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[4] = 0x52;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[5] = 0x4E;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[6] = 0x37;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[7] = 0x48;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[8] = 0x41;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[9] = 0x31;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[10] = 0x32;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[11] = 0x33;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[12] = 0x34;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[13] = 0x35;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[14] = 0x44;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[15] = 0x49;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[16] = 0x4E;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[17] = 0x00;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[18] = 0x00;
			client_comm_data_update_gst.client_comm_bms_id_gst.bin_id_no_au8[19] = 0x00;

			client_comm_data_update_gst.client_comm_bms_id_gst.mac_addr_au8[0] = 0x88;
			client_comm_data_update_gst.client_comm_bms_id_gst.mac_addr_au8[1] = 0x88;
			client_comm_data_update_gst.client_comm_bms_id_gst.mac_addr_au8[2] = 0x88;
			client_comm_data_update_gst.client_comm_bms_id_gst.mac_addr_au8[3] = 0x88;
			client_comm_data_update_gst.client_comm_bms_id_gst.mac_addr_au8[4] = 0x88;
			client_comm_data_update_gst.client_comm_bms_id_gst.mac_addr_au8[5] = 0x88;

			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[0] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[1] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[2] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[3] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[4] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[5] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[6] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[7] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[8] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[9] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[10] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[11] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[12] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[13] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[14] = 0x78;
			client_comm_data_update_gst.client_comm_bms_id_gst.vin_id_no_au8[15] = 0x78;
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 = sizeof(client_comm_bms_id_tst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.client_comm_bms_id_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_BIN_VIN_MAC_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
															client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_FLASH_RET_PGN:
		{
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 =
														sizeof(client_comm_data_update_gst.flsh_ret_debug_battery_data_log_gst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.flsh_ret_debug_battery_data_log_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_FLASH_RET_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
												client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BATT_ONE_TIME_INFO_PGN:
		{
			client_comm_update_ot_batt_info_prv();
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 =
													sizeof(client_comm_data_update_gst.batt_pack_info_gst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.batt_pack_info_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_BATT_ONE_TIME_INFO_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
											client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BATT_PERIODIC_INFO_PGN:
		{
			client_comm_update_periodic_batt_info_prv();
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 =
												sizeof(client_comm_data_update_gst.batt_params_update_gst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.batt_params_update_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_BATT_PERIODIC_INFO_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
													client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_SENSOR_PERIODIC_INFO_PGN:
		{
			client_comm_update_periodic_sensor_info_prv();
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 =
												sizeof(client_comm_data_update_gst.sensor_param_gst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.sensor_param_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_SENSOR_PERIODIC_INFO_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
												client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_FAULT_PERIODIC_INFO_PGN:
		{
			client_comm_update_periodic_fault_info_prv();
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 =
															sizeof(client_comm_data_update_gst.fault_status_gst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.fault_status_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_FAULT_PERIODIC_INFO_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
					client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		case CLIENT_COMM_BATT_STATUS_PERIODIC_PGN:
		{
			client_comm_update_periodic_batt_sts_prv();
			client_comm_send_multipacket_data_gst.multipacket_data_size_u16 =
															sizeof(client_comm_data_update_gst.batt_sts_params_gst);
			cli_comm_multipacket_data_gpu8 = (U8*) &client_comm_data_update_gst.batt_sts_params_gst;
			client_comm_send_multipacket_data_gst.multipacket_pgn_u8 = CLIENT_COMM_BATT_STATUS_PERIODIC_PGN;
			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 = 1;//debug_comm_can_rx_local_buff_au8[2];
			client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 =
					client_comm_send_multipacket_data_gst.multipacket_data_size_u16 / 7;
			if(client_comm_send_multipacket_data_gst.multipacket_data_size_u16 % 7 != 0)
			{
				client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8 ++;
			}
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
			//common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}break;
		/**********************************************************************************************************************/
		default:
		{

		}
		/**********************************************************************************************************************/
	}
	//debug_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);
	can_comm_set_tmr_v(CLIENT_SM,10);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 display_comm_j1939_chck_mp_msg_u8(void)
{
#if CLIENT_COMM_DISPLAY_USE
	if(can_comm_set_timer_gst[DISPLAY_SM].can_set_time_en_u8 == TIMEOUT_CLEAR)
	{
		common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_state_u8 = DISPLAY_COMM_MULTIPACK_SEND_STATE;
		common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		os_can_comm_queue_st.comm_event_e = OS_CAN_0_DISPLAY_TX;
		os_can_comm_queue_st.can_rx_msg_pv = (U8 *)&dummy_data_u8;
		xQueueSendFromISR(os_can_comm_queue_handler_ge,  &os_can_comm_queue_st, COM_HDR_NULL);
	}
#endif
	return 0;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 client_comm_j1939_chck_mp_msg_u8(void)
{
	if(can_comm_set_timer_gst[CLIENT_SM].can_set_time_en_u8 == TIMEOUT_CLEAR)
	{
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = CLIENT_COMM_MULTIPACK_SEND_STATE;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		os_can_comm_queue_st.comm_event_e = OS_CAN_CLIENT_TX;
		os_can_comm_queue_st.can_rx_msg_pv = (U8 *)&dummy_data_u8;
		xQueueSendFromISR(os_can_comm_queue_handler_ge,  &os_can_comm_queue_st, COM_HDR_NULL);
	}
	return 0;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static U8 client_comm_j1939_multipacket_can_msg_send_pru8(void)
{
	U8 return_status_u8 = 0;

	if(client_comm_send_multipacket_data_gst.multipacket_request_count_u8 <= client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8)
	{
		client_comm_send_multipacket_data_gst.multipacket_buff_au8[0] = client_comm_send_multipacket_data_gst.multipacket_request_count_u8;
		memory_copy_u8_array_v(&client_comm_send_multipacket_data_gst.multipacket_buff_au8[1],
								(cli_comm_multipacket_data_gpu8 + 7*(client_comm_send_multipacket_data_gst.multipacket_request_count_u8 - 1)),
								7);

		if(can_app_frame_and_tx_msg_u8 CLIENT_COMM_MULTIPACKET_DATA_FRAME)
		{
			if(client_comm_send_multipacket_data_gst.multipacket_request_count_u8 == client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8)
			{
				return_status_u8 = 0;
				return return_status_u8;
			}
			can_comm_set_tmr_v(CLIENT_SM,2);

			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 ++;
		}
		return_status_u8 = 1;
	}
	else   /* When EOF has not been received by BMS , last multipacket packet data which is previously sent will send it again to get EOF*/
	{
		if(can_app_frame_and_tx_msg_u8 CLIENT_COMM_MULTIPACKET_DATA_FRAME)
		{
			return_status_u8 = 0;
		}
	}
	return return_status_u8;

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static U8 display_comm_j1939_multipacket_can_msg_send_pru8(void)
{
	U8 return_status_u8 = 0;
#if CLIENT_COMM_DISPLAY_USE
	if(client_comm_send_multipacket_data_gst.multipacket_request_count_u8 <= client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8)
	{
		client_comm_send_multipacket_data_gst.multipacket_buff_au8[0] = client_comm_send_multipacket_data_gst.multipacket_request_count_u8;
		memory_copy_u8_array_v(&client_comm_send_multipacket_data_gst.multipacket_buff_au8[1],
								(cli_comm_multipacket_data_gpu8 + 7*(client_comm_send_multipacket_data_gst.multipacket_request_count_u8 - 1)),
								7);

		if(can_app_frame_and_tx_msg_u8 DISPLAY_COMM_MULTIPACKET_DATA_FRAME)
		{
			if(client_comm_send_multipacket_data_gst.multipacket_request_count_u8 == client_comm_send_multipacket_data_gst.tot_num_of_multipacket_u8)
			{
				return_status_u8 = 0;
				return return_status_u8;
			}
			can_comm_set_tmr_v(DISPLAY_SM,2);

			client_comm_send_multipacket_data_gst.multipacket_request_count_u8 ++;
		}
		return_status_u8 = 1;
	}
	else   /* When EOF has not been received by BMS , last multipacket packet data which is previously sent will send it again to get EOF*/
	{
		if(can_app_frame_and_tx_msg_u8 DISPLAY_COMM_MULTIPACKET_DATA_FRAME)
		{
			return_status_u8 = 0;
		}
	}
#endif
	return return_status_u8;

}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void client_comm_j1939_eof_msg_handling_prv(void)
{
	switch(client_comm_can_rx_local_buff_au8[6])
	{
		/**********************************************************************************************************************/
		case DEBUG_COMM_ONE_TIME_INFO_PGN:
		{
			if(client_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_ONE_TIME_INFO_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SEND_BIN_PGN:
		{
/*			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_BIN_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else TODO: Actions to be included if there is invalid size in EOF message
			{

			}*/
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_SEND_VIN_PGN:
		{
/*			if(debug_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_VIN_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else TODO: Actions to be included if there is invalid size in EOF message
			{

			}*/
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_BATT_STS_PARAM_PGN:
		{
			if(client_comm_can_rx_local_buff_au8[1] == DEBUG_COMM_BATT_PARAM_STS_MSG_SIZE)
			{
				common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
		case DEBUG_COMM_FLASH_DATA_RETRIEVAL_PGN:
		{
			if(client_comm_can_rx_local_buff_au8[1] == sizeof(client_comm_data_update_gst.flsh_ret_debug_battery_data_log_gst))
			{
				if(client_comm_data_update_gst.flsh_ret_debug_battery_data_log_gst.time_stamp_u32 == 0xFF)
				{
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
				}
				else
				{
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_DATA_RETRIVAL_STATE;
					os_can_comm_queue_st.comm_event_e = OS_CAN_DEBUG_RX;
					os_can_comm_queue_st.can_rx_msg_pv = (U8 *)&dummy_data_u8;
					xQueueSend(os_can_comm_queue_handler_ge,  &os_can_comm_queue_st, COM_HDR_NULL );
				}
			}
			else /*TODO: Actions to be included if there is invalid size in EOF message */
			{

			}
		}
		break;
		/**********************************************************************************************************************/
		default :
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}

static void display_comm_period_mess_update_prv(void)
{
	display_param_period_gst.total_power_u16 = 11;
	display_param_period_gst.available_discharge_power_u16 = 12;
	display_param_period_gst.available_charge_power_u16 = 13;
	display_param_period_gst.pack_voltage_u16 = 66;
	display_param_period_gst.battery_current_s32 = 33;
	display_param_period_gst.cell_bal_count_u16 = 15;
	display_param_period_gst.tot_energy_throughput_u32 = 16;
	display_param_period_gst.tot_accumulated_charge_u32 = 17;
	//			batt_param_pack_param_gst.pack_param_energy_1cWh_u16;
	display_param_period_gst.tot_hrs_in_chg_mode_u16 = 18;
	display_param_period_gst.chg_capacity_u16 = 19;
	display_param_period_gst.dhg_capacity_u16 = 20;
	display_param_period_gst.soh_u16 = 21;
	display_param_period_gst.soc_u16 = 22;
	display_param_period_gst.soe_u16 = 23;
	display_param_period_gst.chg_current_lmt_u16 = 25;
	display_param_period_gst.dchg_current_lmt_u16 = 26;
	display_param_period_gst.ambient_temp_u16 = 27;
	display_param_period_gst.fet_temp_u16 = 28;
	display_param_period_gst.afe_die_temp_u16 = 29;
	display_param_period_gst.mcu_die_temp_u16 = 30;
	display_param_period_gst.mos_pck_state_u8 = 3;//(batt_param_pack_param_gst.pack_param_batt_mode_e);

}
static void client_comm_update_ot_batt_info_prv(void)
{
	client_comm_data_update_gst.batt_pack_info_gst.bms_firmware_version_num_u8 = 90;
	client_comm_data_update_gst.batt_pack_info_gst.bms_hw_version_num_u16 = 80;
	client_comm_data_update_gst.batt_pack_info_gst.bms_pk_version_num_u16 = 70;
	client_comm_data_update_gst.batt_pack_info_gst.bms_platform_no_u8 = 1;
//	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_u64 = 0x53484B01001001;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[0]= 0x53;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[1]= 0x48;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[2]= 0x4B;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[3]= 0X30;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[4]= 0X31;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[5]= 0X30;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[6]= 0X30;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[7]= 0X31;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[8]= 0X30;
	client_comm_data_update_gst.batt_pack_info_gst.bms_model_no_au8[9]= 0X31;

	//client_comm_data_update_gst.batt_pack_info_gst.bms_protocol_version_num_u8 = 01;
	//	client_comm_data_update_gst.batt_pack_info_gst.slave_hw_version_num_u16 = 99;

	//client_comm_data_update_gst.batt_pack_info_gst.gprs_version_num_u8 = 45;
	client_comm_data_update_gst.batt_pack_info_gst.gprs_imei_num_u8[0] = 83;
	client_comm_data_update_gst.batt_pack_info_gst.gps_version_num_u8 = 66;
	client_comm_data_update_gst.batt_pack_info_gst.ble_version_num_u8 = 55;
	client_comm_data_update_gst.batt_pack_info_gst.ble_mac_addr_u8[0] = 50;

	//client_comm_data_update_gst.batt_pack_info_gst.batt_supplier_u8 = 1;
	//client_comm_data_update_gst.batt_pack_info_gst.batt_customer_u8 = 45;
	client_comm_data_update_gst.batt_pack_info_gst.date_of_manf_u32 = 77;
	client_comm_data_update_gst.batt_pack_info_gst.pack_dod_u16 = 93;
	client_comm_data_update_gst.batt_pack_info_gst.rated_pack_cycles_u16 = 0x90;
	client_comm_data_update_gst.batt_pack_info_gst.nominal_voltage_u16 = 5000;
	client_comm_data_update_gst.batt_pack_info_gst.rated_capacity_u16 = 0x88;
	client_comm_data_update_gst.batt_pack_info_gst.cell_type_u16 = 0x33;
	client_comm_data_update_gst.batt_pack_info_gst.total_cells_u16 = 0x67;

	client_comm_data_update_gst.batt_pack_info_gst.cell_ov_warning_u16 = bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_uv_warning_u16 = bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_uv_threshold_u16 =
													bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ov_threshold_u16 =
													bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16;

	/* has to be changed in future */
	client_comm_data_update_gst.batt_pack_info_gst.cell_deep_dsg_threshold_u16 = (bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16 - 2000);
	client_comm_data_update_gst.batt_pack_info_gst.cell_deep_dsg_warning_u16 = (bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16 - 1000);

	client_comm_data_update_gst.batt_pack_info_gst.cell_ov_flt_region_u16 = (bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16 + 500);
	client_comm_data_update_gst.batt_pack_info_gst.cell_uv_flt_region_u16 = (bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16 - 500);
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_ov_threshold_u16 = bms_config_nv_cell_cfg_gst.release_max_cell_1mV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_uv_threshold_u16 = bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16;


	client_comm_data_update_gst.batt_pack_info_gst.pack_ov_warning_u16 =
													bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uv_warning_u16 =
													bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.release_pack_ov_threshold_u16 =
													bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.release_pack_uv_threshold_u16	=
													bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.pack_ov_flt_region_u16 = bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16 + 200;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uv_flt_region_u16 = bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16 - 200;
	client_comm_data_update_gst.batt_pack_info_gst.pack_ov_threshold_u16 = bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uv_threshold_u16 = bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16;


//													bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16;

	client_comm_data_update_gst.batt_pack_info_gst.cell_ot_warning_u16 = 265;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ut_warning_u16 = 245;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ot_threshold_u16 = 263;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ut_threshold_u16 = 262;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ot_flt_region_u16 = 259;
	client_comm_data_update_gst.batt_pack_info_gst.cell_ut_flt_region_u16 = 261;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_ot_threshold_u16 = 268;
	client_comm_data_update_gst.batt_pack_info_gst.release_cell_ut_threshold_u16 = 249;

	client_comm_data_update_gst.batt_pack_info_gst.amb_ot_warning_u16 = 272;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ut_warning_u16 = 279;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ot_threshold_u16 = 232;
	client_comm_data_update_gst.batt_pack_info_gst.amb_ut_threshold_u16 = 235;
	client_comm_data_update_gst.batt_pack_info_gst.release_amb_ot_threshold_u16 = 209;
	client_comm_data_update_gst.batt_pack_info_gst.release_amb_ut_threshold_u16 = 207;

	client_comm_data_update_gst.batt_pack_info_gst.mfet_ot_warning_u16 = 229;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ut_warning_u16 = 231;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ot_threshold_u16 = 181;
	client_comm_data_update_gst.batt_pack_info_gst.mfet_ut_threshold_u16 = 171;
	client_comm_data_update_gst.batt_pack_info_gst.release_mfet_ot_threshold_u16 = 188;
	client_comm_data_update_gst.batt_pack_info_gst.release_mfet_ut_threshold_u16 = 189;

	client_comm_data_update_gst.batt_pack_info_gst.warn_max_chg_1ma_s32 = 115;
	client_comm_data_update_gst.batt_pack_info_gst.warn_max_dsg_1ma_s32 = 118;
	client_comm_data_update_gst.batt_pack_info_gst.max_chg_1ma_s32 = 121;
	client_comm_data_update_gst.batt_pack_info_gst.max_dsg_1ma_s32 = 126;
	client_comm_data_update_gst.batt_pack_info_gst.warn_max_scd_surge_1ma_s32 = 128;
	client_comm_data_update_gst.batt_pack_info_gst.chg_1ma_flt_region_u16 = 82;
	client_comm_data_update_gst.batt_pack_info_gst.dchg_1ma_flt_region_u16 = 84;

	client_comm_data_update_gst.batt_pack_info_gst.pack_oh_warning_u16 = 141;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uh_warning_u16 = 142;
	client_comm_data_update_gst.batt_pack_info_gst.pack_oh_threshold_u16 = 143;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uh_threshold_u16 = 144;
	client_comm_data_update_gst.batt_pack_info_gst.pack_oh_release_u16 = 145;
	client_comm_data_update_gst.batt_pack_info_gst.pack_uh_release_u16 = 146;
}

static void client_comm_update_periodic_batt_info_prv(void)
{
	U8 temp_indx_u8 = 0;
	U8 cell_v_indx_u8 = 0;

	client_comm_data_update_gst.batt_params_update_gst.num_of_cell_volt_u8 = BATT_PARAM_CELL_ID_MAX;
	for(cell_v_indx_u8 = 0; cell_v_indx_u8 < BATT_PARAM_CELL_ID_MAX; cell_v_indx_u8++)
	{
		client_comm_data_update_gst.batt_params_update_gst.battery_cell_voltage_au8[cell_v_indx_u8] =
				batt_param_cell_param_gst[0][cell_v_indx_u8].cell_meas_1mV_u16;
	}
	for(cell_v_indx_u8 = BATT_PARAM_CELL_ID_MAX; cell_v_indx_u8 < CLIENT_COMM_GUI_CELL_VOLT; cell_v_indx_u8++)
	{
		client_comm_data_update_gst.batt_params_update_gst.battery_cell_voltage_au8[cell_v_indx_u8] = 0xFF;
	}

	client_comm_data_update_gst.batt_params_update_gst.num_of_cell_temp_u8 = BATT_PARAM_TEMP_ID_MAX;
	for(temp_indx_u8 = 0; temp_indx_u8 <(BATT_PARAM_TEMP_ID_MAX); temp_indx_u8++)
	{
		client_comm_data_update_gst.batt_params_update_gst.individual_sensor_tempurature_as16[temp_indx_u8] =
				batt_param_temp_param_gst[0][temp_indx_u8].temp_meas_1cc_s16;
	}
	for(temp_indx_u8 = BATT_PARAM_TEMP_ID_MAX; temp_indx_u8 < CLIENT_COMM_GUI_CELL_TEMP; temp_indx_u8++)
	{
		client_comm_data_update_gst.batt_params_update_gst.individual_sensor_tempurature_as16[temp_indx_u8] = 0xFF;
	}

	client_comm_data_update_gst.batt_params_update_gst.cell_bal_state_u8 = batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_active_e;
	client_comm_data_update_gst.batt_params_update_gst.cell_bal_status_1_u16 = 44;
	client_comm_data_update_gst.batt_params_update_gst.cell_bal_status_2_u16 = 45;
	client_comm_data_update_gst.batt_params_update_gst.battery_current_s32 = batt_param_pack_param_gst.pack_param_1mA_s32;
	client_comm_data_update_gst.batt_params_update_gst.pcon_volt_u16 = 0x00;
	client_comm_data_update_gst.batt_params_update_gst.pack_volt_u16 = batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16;
	client_comm_data_update_gst.batt_params_update_gst.mos_volt_u16 = 0x00;
//	client_comm_data_update_gst.batt_params_update_gst.ld_volt_u16  = 0x00;
	client_comm_data_update_gst.batt_params_update_gst.fuse_volt_u16 = batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16;




/*	debug_comm_data_update_gst.batt_params_update_gst.
	debug_comm_data_update_gst.batt_params_update_gst.
	debug_comm_data_update_gst.batt_params_update_gst.
	debug_comm_data_update_gst.batt_params_update_gst.*/
}

static void client_comm_update_periodic_sensor_info_prv(void)
{
#if 0
	client_comm_data_update_gst.sensor_param_gst.accel_1g_s32[0] = batt_param_pack_param_gst.pack_param_icm20948_accel_s32[0];
	client_comm_data_update_gst.sensor_param_gst.accel_1g_s32[1] = batt_param_pack_param_gst.pack_param_icm20948_accel_s32[1];
	client_comm_data_update_gst.sensor_param_gst.accel_1g_s32[2] = batt_param_pack_param_gst.pack_param_icm20948_accel_s32[2];
	client_comm_data_update_gst.sensor_param_gst.gyro_1dps_s32[0] =batt_param_pack_param_gst.pack_param_icm20948_gyro_s32[0];
	client_comm_data_update_gst.sensor_param_gst.gyro_1dps_s32[1] = batt_param_pack_param_gst.pack_param_icm20948_gyro_s32[1];
	client_comm_data_update_gst.sensor_param_gst.gyro_1dps_s32[2] = batt_param_pack_param_gst.pack_param_icm20948_gyro_s32[2];
	client_comm_data_update_gst.sensor_param_gst.mag_1ut_s32[0] = batt_param_pack_param_gst.pack_param_icm20948_mag_s32[0];
	client_comm_data_update_gst.sensor_param_gst.mag_1ut_s32[1] = batt_param_pack_param_gst.pack_param_icm20948_mag_s32[1];
	client_comm_data_update_gst.sensor_param_gst.mag_1ut_s32[2] = batt_param_pack_param_gst.pack_param_icm20948_mag_s32[2];
	client_comm_data_update_gst.sensor_param_gst.hum_1rh_u16 = batt_param_pack_param_gst.pack_param_pk_hum_u16;
	client_comm_data_update_gst.sensor_param_gst.temp_1degc_s16 = batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16;
	client_comm_data_update_gst.sensor_param_gst.luminence_data_u16 = batt_param_pack_param_gst.pack_param_illuminance_u16;
#endif
}

static void client_comm_update_periodic_fault_info_prv(void)
{

}

static void client_comm_update_periodic_batt_sts_prv(void)
{
	rtc_int_get_time_date_u8(&debug_comm_rtc_int_param_gst);
	client_comm_data_update_gst.batt_sts_params_gst.rtc_hours_u8 = (U8)debug_comm_rtc_int_param_gst.hours_u8;
	client_comm_data_update_gst.batt_sts_params_gst.rtc_minutes_u8 = (U8)debug_comm_rtc_int_param_gst.minutes_u8  ;
	client_comm_data_update_gst.batt_sts_params_gst.rtc_seconds_u8 = (U8)debug_comm_rtc_int_param_gst.seconds_u8;
	client_comm_data_update_gst.batt_sts_params_gst.rtc_date_u8 = (U8)debug_comm_rtc_int_param_gst.date_u8 ;
	client_comm_data_update_gst.batt_sts_params_gst.rtc_months_u8 = (U8)debug_comm_rtc_int_param_gst.months_u8;
	client_comm_data_update_gst.batt_sts_params_gst.rtc_year_u8 = ((U8)debug_comm_rtc_int_param_gst.year_u16 - 2000);

	client_comm_data_update_gst.batt_sts_params_gst.total_power_s16 = 11;
	client_comm_data_update_gst.batt_sts_params_gst.available_discharge_power_u16 = 12;
	client_comm_data_update_gst.batt_sts_params_gst.available_charge_power_u16 = 13;
	client_comm_data_update_gst.batt_sts_params_gst.pack_voltage_u16 =
				batt_param_pack_param_gst.pack_param_1cV_u16;
	client_comm_data_update_gst.batt_sts_params_gst.discharge_energy_u16 = 14;
	client_comm_data_update_gst.batt_sts_params_gst.charge_energy_u16 = 15;
	client_comm_data_update_gst.batt_sts_params_gst.tot_energy_throughput_u32 = 16;
	client_comm_data_update_gst.batt_sts_params_gst.tot_accumulated_charge_u32 = 17;
//			batt_param_pack_param_gst.pack_param_energy_1cWh_u16;
	client_comm_data_update_gst.batt_sts_params_gst.tot_hrs_in_chg_mode_u16 = 18;
	client_comm_data_update_gst.batt_sts_params_gst.chg_capacity_u16 = 19;
	client_comm_data_update_gst.batt_sts_params_gst.dhg_capacity_u16 = 20;

	client_comm_data_update_gst.batt_sts_params_gst.soh_u16 = 21;
	client_comm_data_update_gst.batt_sts_params_gst.soc_u16 = 22;
	client_comm_data_update_gst.batt_sts_params_gst.soe_u16 = 23;
	client_comm_data_update_gst.batt_sts_params_gst.sop_u16 = 24;

	client_comm_data_update_gst.batt_sts_params_gst.chg_current_lmt_u16 = 25;
	client_comm_data_update_gst.batt_sts_params_gst.dchg_current_lmt_u16 = 26;
	client_comm_data_update_gst.batt_sts_params_gst.pack_flt_u16 = 27;
	client_comm_data_update_gst.batt_sts_params_gst.pack_cycle_count_u16 = 28;
	client_comm_data_update_gst.batt_sts_params_gst.cell_bal_count_u16 = 29;
	client_comm_data_update_gst.batt_sts_params_gst.mfet_contactor_status_u8 = 30;
	client_comm_data_update_gst.batt_sts_params_gst.ambient_temp_u16 = 0; /* TODO PAck Temperature*/
	client_comm_data_update_gst.batt_sts_params_gst.fet_temp_u16 = 0;/* TODO */ // batt_param_temp_param_gst[0][9].temp_meas_1cc_s16;
	client_comm_data_update_gst.batt_sts_params_gst.afe_die_temp_u16 = 0;//(U16)afe_driver_data_gst[SLAVE_IC_1].ic_internal_die_temp_s32;
	client_comm_data_update_gst.batt_sts_params_gst.mcu_die_temp_u16 = 34;
	client_comm_data_update_gst.batt_sts_params_gst.avg_temp_u16 = 35;
	client_comm_data_update_gst.batt_sts_params_gst.pack_state_u8 = 36;
	client_comm_data_update_gst.batt_sts_params_gst.gprs_conn_status_u8 = 37;
	client_comm_data_update_gst.batt_sts_params_gst.gprs_sig_strength_u8 = 38;
	client_comm_data_update_gst.batt_sts_params_gst.gps_accuracy_u16 = 39;
	client_comm_data_update_gst.batt_sts_params_gst.gps_latitude_u32 = 40;
	client_comm_data_update_gst.batt_sts_params_gst.gps_longitude_u32 = 41;
	client_comm_data_update_gst.batt_sts_params_gst.gps_dev_connected_u8 = 42;
	client_comm_data_update_gst.batt_sts_params_gst.ble_conn_status_u8 = 43;
	/* must fill all the data inside structure */
	client_comm_data_update_gst.batt_sts_params_gst.max_cell_temp_s16 =
							batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
	client_comm_data_update_gst.batt_sts_params_gst.min_cell_temp_s16 =
							batt_param_pack_param_gst.pack_param_min_cell_1cc_s16;
	client_comm_data_update_gst.batt_sts_params_gst.min_cell_volt_u16 =
							batt_param_pack_param_gst.pack_param_min_cell_1mV_u16;
	client_comm_data_update_gst.batt_sts_params_gst.max_cell_volt_u16 =
							batt_param_pack_param_gst.pack_param_max_cell_1mV_u16;

//
//	debug_comm_data_update_gst.batt_sts_params_gst.soc_u16 =
}
static void client_comm_auth_prv()
{
	if(client_comm_mac_gui_u32 != 0xFFFF)
	{
		client_comm_tx_msg_st.data_au8[0] = 0xAA;
	}
	else
	{
		client_comm_tx_msg_st.data_au8[0] = 0xFF;
	}
	client_comm_tx_msg_st.can_id_u32 = 0x801FADA;
	client_comm_tx_msg_st.length_u8 = sizeof(client_comm_tx_msg_st.data_au8);
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &client_comm_tx_msg_st);
}
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/

void display_comm_canrx_process_v(can_comm_task_can_msg_tst* rx_can_message_arpst)
{
#if CLIENT_COMM_DISPLAY_USE
	U32 can_received_msg_id_u32 = 0;
	U8 can_data_length_u8 = 0;
	U8 j1939_pgn_u8 = 0;

	can_received_msg_id_u32 = rx_can_message_arpst->id_u32;
	can_data_length_u8 = rx_can_message_arpst->length_u8;
	memory_copy_u8_array_v(&display_comm_can_rx_local_buff_au8[0], rx_can_message_arpst->data_au8, can_data_length_u8);

	j1939_pgn_u8 = CAN_APP_GET_PGN(can_received_msg_id_u32);
	switch (j1939_pgn_u8)
	{
	case DISPLAY_COMM_START_STOP_PGN:
	{
		if(display_comm_can_rx_local_buff_au8[0] == 0xAA)
		{
			common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_state_u8 = DISPLAY_COMM_START_STOP_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
		if(display_comm_can_rx_local_buff_au8[0] == 0xFF)
		{
			common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_state_u8 = DISPLAY_COMM_START_STOP_ACK_STATE;
			common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		}
	}break;
	case DISPLAY_COMM_PARAM_REQ_PGN:
	{
		display_comm_j1939_rts_send_prv(DISPLAY_COMM_PARAM_REQ_PGN,DISPLAY_COMM_PERIODIC_MESS_SIZE);
		display_comm_j1939_multipacket_can_msg_update_prv(DISPLAY_COMM_PARAM_REQ_PGN);

	}break;

	}
#endif
}

void display_comm_cantx_process_v(can_comm_task_can_msg_tst* tx_can_message_arg_st)
{
#if CLIENT_COMM_DISPLAY_USE
	U32 can_received_msg_id_u32 = 0;
	U8 can_data_length_u8 = 0;
	can_received_msg_id_u32 = tx_can_message_arg_st->id_u32;
	can_data_length_u8 = tx_can_message_arg_st->length_u8;
	memory_copy_u8_array_v(&display_comm_can_rx_local_buff_au8[0], tx_can_message_arg_st->data_au8,
																		can_data_length_u8);

	if(CAN_APP_STATE_TX == common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8)
	{
		switch (common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_state_u8)
		{
		case DISPLAY_COMM_START_STOP_ACK_STATE:
		{
			if(display_comm_can_rx_local_buff_au8[0] == 0xAA)
			{
				memset(client_comm_tx_msg_st.data_au8,0,8);
				client_comm_tx_msg_st.data_au8[0] = 0xAA;
				client_comm_tx_msg_st.can_id_u32 = DISPLAY_COMM_START_STOP_RES;
				client_comm_tx_msg_st.length_u8 = 1;
				client_comm_tx_msg_st.cs = 0;
				if(can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &client_comm_tx_msg_st))
				{
				}
				else
				{
					common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
				}
			}

			if(display_comm_can_rx_local_buff_au8[0] == 0xFF)
			{
				memset(client_comm_tx_msg_st.data_au8,0,8);
				client_comm_tx_msg_st.data_au8[0] = 0xFF;
				client_comm_tx_msg_st.can_id_u32 = DISPLAY_COMM_START_STOP_RES;
				client_comm_tx_msg_st.length_u8 = 1;
				client_comm_tx_msg_st.cs = 0;
				if(can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &client_comm_tx_msg_st))
				{
				}
				else
				{
					common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
				}
			}
		}break;

		case DISPLAY_COMM_MULTIPACK_SEND_STATE:
		{
			display_comm_j1939_multipacket_can_msg_send_pru8();
		}break;
		case DISPLAY_COMM_WARNING_SEND_STATE:
		{
			client_comm_tx_msg_st.data_au8[0] = system_diag_warn_code_gu32;
			client_comm_tx_msg_st.data_au8[1] = (system_diag_warn_code_gu32 >> 8);
			client_comm_tx_msg_st.data_au8[2] = (system_diag_warn_code_gu32 >> 16);
			client_comm_tx_msg_st.data_au8[3] = (system_diag_warn_code_gu32 >> 24);
			system_diag_warn_code_gu32 = 0;
			//memset(client_comm_tx_msg_st.data_au8,system_diag_warn_code_gu32,8);
			client_comm_tx_msg_st.can_id_u32 = DISPLAY_COMM_WARN_RES;
			client_comm_tx_msg_st.length_u8 = 8;
			client_comm_tx_msg_st.cs = 0;
			if(can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &client_comm_tx_msg_st))
			{
			}
			else
			{
				common_var_can_state_gst[COMMON_VAR_DISPLAY_SM].protocol_status_u8 = CAN_APP_STATE_IDLE;
			}
		}break;
		}
	}
#endif
}
