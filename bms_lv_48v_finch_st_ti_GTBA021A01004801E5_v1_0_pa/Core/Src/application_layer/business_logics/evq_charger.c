/*
 * evq_charger.c
 *
 *  Created on: 17-Aug-2021
 *      Author: SHYAM
 */
#include "evq_charger.h"

evq_chg_data_tst evq_chg_data_gst;
can_app_message_tst 	evq_chg_tx_msg_st;
BOOL evq_charger_sts_bool = COM_HDR_DISABLED;
void evq_charger_init_v(void)
{
	evq_chg_data_gst.evq_v_set_u16 = 568;
	evq_chg_data_gst.evq_i_set_u16 = 120;
	evq_chg_data_gst.evq_cntrl_set_u8 = 1;
}

void evq_chg_req_mess_v(void)
{
	U8 tx_msg_index_u8 = 0;
	evq_chg_tx_msg_st.can_id_u32 = BMS_TO_CHARGER_REQ;

	evq_charger_sts_bool = COM_HDR_DISABLED;
	/* Always DLC -8*/
	evq_chg_tx_msg_st.length_u8 = 8;

	/* Voltage Set Req */
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = ((evq_chg_data_gst.evq_v_set_u16 >> 8)& 0xFF);
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = (evq_chg_data_gst.evq_v_set_u16 & 0xFF);

	/* Current Set Req */
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = ((evq_chg_data_gst.evq_i_set_u16 >> 8)& 0xFF);
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = (evq_chg_data_gst.evq_i_set_u16 & 0xFF);

	/* Control Set Req */
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = evq_chg_data_gst.evq_cntrl_set_u8;

	/* Reserverd */
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = 0x00;
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = 0x00;
	evq_chg_tx_msg_st.data_au8[tx_msg_index_u8++] = 0x00;

	/* CAN transmit message */
	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &evq_chg_tx_msg_st);
}

U8 evq_chg_rx_mess_v(can_comm_task_can_msg_tst* rx_can_message_arpst)
{
	U8 can_rx_local_buff_au8[8];
	if(rx_can_message_arpst->id_u32 == CHARGER_TO_BMS_REQ)
	{
		memory_copy_u8_array_v(&can_rx_local_buff_au8[0], rx_can_message_arpst->data_au8, 8);
		if((can_rx_local_buff_au8[4] & 0x01) == 1)
		{
			evq_chg_data_gst.evq_cntrl_set_u8 |= (1 << HARDWARE_FAIL);
		}
		else
		{
			evq_chg_data_gst.evq_cntrl_set_u8 &= ~(1 << HARDWARE_FAIL);
		}
		if((can_rx_local_buff_au8[4] & 0x02) == 1)
		{
			evq_chg_data_gst.evq_cntrl_set_u8 |= (1 << OVER_TEMP_FAIL);
		}
		else
		{
			evq_chg_data_gst.evq_cntrl_set_u8 &= ~(1 << OVER_TEMP_FAIL);
		}
		if((can_rx_local_buff_au8[4] & 0x10) == 1)
		{
			evq_chg_data_gst.evq_cntrl_set_u8 |= (1 << COMMUNICATION_FAIL);
		}
		else
		{
			evq_chg_data_gst.evq_cntrl_set_u8 &= ~(1 << COMMUNICATION_FAIL);
		}
	}
}
