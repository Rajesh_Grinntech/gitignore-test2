/**
====================================================================================================================================================================================
@page batt_param_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	batt_param.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	07-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include "bms_config.h"

/* This header file */
#include "batt_param.h"

/* Driver header */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

batt_param_pack_param_tst       batt_param_pack_param_gst;
batt_param_mod_param_tst 		batt_param_mod_param_gst[BATT_PARAM_TOTAL_SLAVES];
batt_param_cell_param_tst       batt_param_cell_param_gst[BATT_PARAM_TOTAL_SLAVES][BATT_PARAM_CELL_ID_MAX];
batt_param_temp_param_tst       batt_param_temp_param_gst[BATT_PARAM_TOTAL_SLAVES][BATT_PARAM_MAX_CT_PER_SLV];
batt_param_pack_details_tst		batt_param_batt_pack_details_gst;
batt_param_energy_tst   		batt_param_energy_gst;

/** @} */

/* Public Function Definitions */


/**
====================================================================================================================================================================================

@fn Name			: batt_param_var_init_v
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
void batt_param_var_init_v(void)
{
    U8 cell_num_u8 = 0;
    U8 id_num_u8 = 0;
    U8 mod_u8 = 0;
    U8 mfet_therm_index_u8 = 0;

	for (mod_u8 = PACK_PARAM_MOD_ID_1; mod_u8 < PACK_PARAM_MOD_ID_MAX_NUM; mod_u8++)
	{
	    for (cell_num_u8 = PACK_PARAM_CELL_ID_1; cell_num_u8 < BATT_PARAM_CELL_ID_MAX; cell_num_u8++)
	    {
			batt_param_cell_param_gst[mod_u8][cell_num_u8].cell_meas_1mV_u16 =	BATT_PARAM_AI_INIT_VAL_U16;
			//batt_param_cell_param_gst[mod_u8][cell_num_u8].cell_ocv_soc_1cPC_u16 = BATT_PARAM_AI_INIT_VAL_U16;
	    }
	}

	for (mod_u8 = PACK_PARAM_MOD_ID_1; mod_u8 < PACK_PARAM_MOD_ID_MAX_NUM; mod_u8++)
	{
		/* Algorithm to equally distribute total temperature sensors allocated for pack between slaves */
		batt_param_pack_param_gst.pack_param_tot_therm_per_slv_au8[mod_u8] = BATT_PARAM_CT_PER_SLV(mod_u8);

		for (id_num_u8 = PACK_PARAM_THERMISTOR_ID_1; id_num_u8 < batt_param_pack_param_gst.pack_param_tot_therm_per_slv_au8[mod_u8]; id_num_u8++)
		{
			batt_param_temp_param_gst[mod_u8][id_num_u8].temp_meas_1cc_s16 = BATT_PARAM_AI_INIT_VAL_S16;
		}
	}

    batt_param_pack_param_gst.pack_param_1mA_s32 							= BATT_PARAM_AI_INIT_VAL_S32;
    batt_param_pack_param_gst.pack_param_1cV_u16 							= BATT_PARAM_AI_INIT_VAL_U16;
    batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 					= BATT_PARAM_AI_INIT_VAL_U16;
    batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16 					= BATT_PARAM_AI_INIT_VAL_U16;
    batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 					= BATT_PARAM_AI_INIT_VAL_U16;
    batt_param_pack_param_gst.pack_param_bal_err_1cPC_u16 					= BATT_PARAM_AI_INIT_VAL_U16;

    batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 					= BATT_PARAM_AI_INIT_VAL_S16;
    batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 					= BATT_PARAM_AI_INIT_VAL_S16;
    batt_param_pack_param_gst.pack_param_avg_cell_1cc_s16					= BATT_PARAM_AI_INIT_VAL_S16;

	for (mfet_therm_index_u8 = 0; mfet_therm_index_u8 < BATT_PARAM_MFET_TEMP_ID_MAX ; mfet_therm_index_u8++)
	{
		batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[mfet_therm_index_u8]  = BATT_PARAM_AI_INIT_VAL_S16;
	}

	batt_param_pack_param_gst. pack_param_afe_min_die_temp_1cc_s16					= BATT_PARAM_AI_INIT_VAL_S16;
	batt_param_pack_param_gst. pack_param_afe_max_die_temp_1cc_s16					= BATT_PARAM_AI_INIT_VAL_S16;

    batt_param_pack_param_gst.pack_param_inst_power_1W_s16 					= 0;
    batt_param_pack_param_gst.pack_param_energy_1cWh_u16 					= 0;
    batt_param_pack_param_gst.pack_param_capacity_1cAhr_f 					= 0;
    batt_param_pack_param_gst.pack_param_cycle_complete_cap_1cAhr_f 		= 0;

    batt_param_pack_param_gst.pack_param_soh_1mPC_u32 						= 100000;
    batt_param_pack_param_gst.pack_param_OCV_soc_1cPC_u16 					= BATT_PARAM_AI_INIT_VAL_U16;
    batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 			= BATT_PARAM_AI_INIT_VAL_U16;
    batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 				= BATT_PARAM_AI_INIT_VAL_U16;

    batt_param_pack_param_gst.pack_param_cycle_count_u32 					= COM_HDR_MIN_U32;

	batt_param_pack_param_gst.pack_param_delta_pk_hvadc_pk_1cV_u16 = BATT_PARAM_AI_INIT_VAL_U16 ;

	/* Protection */
    batt_param_pack_param_gst.pack_param_cell_open_short_limit_active		= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e 		= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e 	= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_max_cell_v_rechg_limit_active_e 	= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e 	= PACK_PARAM_LIMIT_NA;

    batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active		= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e 		= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e 		= PACK_PARAM_LIMIT_NA;

    batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e 	= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_active_e 	= PACK_PARAM_LIMIT_NA;

    batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e 		= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e 	= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e 			= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_max_i_dsg_level1_limit_active_e 	= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e 			= PACK_PARAM_LIMIT_NA;
    batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e = PACK_PARAM_LIMIT_NA;
	batt_param_pack_param_gst.pack_param_afe_functionality_error_e 			= PACK_PARAM_LIMIT_NA;

	batt_param_pack_param_gst.pack_param_pack_v_error_limit_active_e= PACK_PARAM_LIMIT_NA;

	/* Faults */
	batt_param_pack_param_gst.pack_param_afe_malfunction_on_load_error_e 	= PACK_PARAM_LIMIT_NA;
	batt_param_pack_param_gst.pack_param_scd_on_load_error_e 				= PACK_PARAM_LIMIT_NA;

	/* Switches and Status */
	batt_param_pack_param_gst.pack_param_prev_trip_action_e 				= PACK_PARAM_ACTION_NA;
	batt_param_pack_param_gst.pack_param_trip_action_e 						= PACK_PARAM_ACTION_NA;
	batt_param_pack_param_gst.pack_param_batt_mode_e 						= BATT_MODE_IDLE;


	/* Warnings */
	batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b 		= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b 		= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_warning_b= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_mosfet_max_temp_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_warning_b 	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_max_i_chg_limit_warning_b 			= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_max_i_dsg_limit_warning_b 			= COM_HDR_FALSE;

	batt_param_pack_param_gst.pack_param_max_pack_temp_limit_warn_b	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_min_pack_temp_limit_warn_b	= COM_HDR_FALSE;

	batt_param_pack_param_gst.pack_param_max_afe_die_temp_limit_warn_b	= COM_HDR_FALSE;
	batt_param_pack_param_gst.pack_param_min_afe_die_temp_limit_warn_b	= COM_HDR_FALSE;




}

/**
====================================================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

====================================================================================================================================================================================
*/
void batt_param_init_batt_details_v(void)
{
	batt_param_batt_pack_details_gst.bms_hw_version_num_u16 = 1.0;
	batt_param_batt_pack_details_gst.bms_pk_version_num_u16 = 1;
	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 100;
	batt_param_batt_pack_details_gst.slave_hw_version_num_u16 = 0;
	batt_param_batt_pack_details_gst.bms_protocol_version_num_u16;
	batt_param_batt_pack_details_gst.batt_supplier_u8 = 0;
	batt_param_batt_pack_details_gst.batt_customer_u8 = 0;
	batt_param_batt_pack_details_gst.date_of_manf_u32 = 0;

	batt_param_batt_pack_details_gst.pack_dod_u16 = 100;
	batt_param_batt_pack_details_gst.rated_pack_cycles_u16 = 650;
	batt_param_batt_pack_details_gst.nominal_voltage_u16 = 48;
	batt_param_batt_pack_details_gst.rated_capacity_u16 = 25;
	batt_param_batt_pack_details_gst.cell_type_u16 = 0;
	batt_param_batt_pack_details_gst.total_cells_u16 = 6;
	batt_param_batt_pack_details_gst.number_of_slaves_u16;
//	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 203; //Bootloader tested 48Mhz
//	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 204; //Bootloader tested 8Mhz
//	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 205; //Bootloader tested 8Mhz + Mfet_solvd_merged
//	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 206; //Bootloader tested 8Mhz + Mfet_solvd_merged + batry1
//	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 207; //Bootloader tested 8Mhz + Mfet_solvd_merged + batry2

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 208; //Bootloader tested 8Mhz + Mfet_solvd_merged + without pre
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 209; //Bootloader tested 8Mhz + Mfet_solvd_merged + without pre + mosfet tempe value and pack temp value changed+bootloader bug fixed
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 210; //msfet_temp_algo_changed
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 211; //msfet_temp_algo_changed + hang issue find bug fixed need to test long run
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 212; //+precharge +predischarge included
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 213; //+precharge +predischarge DISABLED
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 214; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 215; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final + cleaned_ntc&afe_config
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 216; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 217; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + release04052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 218; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + release04052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 219; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + testing_sleep_disabled

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 220; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change+ release06052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 221; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change + release06052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 222; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + release06052021beta
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 223; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + release06052021beta

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 224; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + release06052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 225; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + release06052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 226; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + release06052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 227; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + release06052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 228; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + release10052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 229; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc +  release11052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 230; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc + release11052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 231; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved +  release11052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 232; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc_issueSolved + release11052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 233; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 +  release12052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 234; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc_issueSolved + soc_ah4_51 + release12052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 235; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved +  release13052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 236; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + release13052021

//	batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 180; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + fusevtg_viol_fix + release15052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 238; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + fusevtg_viol_fix + release15052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 207; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + testingOnly_deep_rtc+ afe_die_gui

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 101; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc + deepdsg3V + test_Release110521
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 102; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc + deepdsg3V + test_Release110521

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 103; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc + puv39V + test_Release110521
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 104; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + with_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc + puv39V + test_Release110521

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 105; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change  + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter +afe_die_gui + soc_issueSolved + occ_15_ocd_9  + test_Release110521
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 106; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + pov53_9_rel53_5 +  test_release12052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 107; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + temp_spl_cutoffs +  test_release12052021

	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 108; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + mftemp_splcutoff +  test_release13052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 109; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + ctemp_splcutoff +  test_release13052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 110; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + ptemp_splcutoff +  test_release13052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 111; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + afedtemp_splcutoff +  test_release13052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 112; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + cov4_12_rel4_02 +  test_release15052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 113; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + fusevtg_viol_fix + cuv3_7_rel4 +  release21052021
	//batt_param_batt_pack_details_gst.bms_firmware_version_num_u16 = 114; //+precharge +predischarge DISABLED + fault status + AH & Cycle cnt + SleepCheck + without_pre_final_new + cleaned_ntc&afe_config + threshold_change + Only3CellTemp + Mosfettemp-cell4 + cbdelta_thres30 + Fet_ot_95 + Only_deep_rtc + protsm_logics_alter + afe_die_gui + soc_issueSolved + soc_ah4_51 + ftemp_rel_solved + fusevtg_viol_fix + cuv2_7_rel3 +  release21052021

}

void batt_param_recover_batt_details(void)
{
#if 0  //after adding flash
	U32 flash_addr_u32 = BATT_PARAM_FLASH_LOG_ADDR;
    ucFlash_Driver_App_Read_Data((U8*) &Battparam_flash_log_gst, flash_addr_u32, sizeof(Battparam_flash_log_gst));

	if(Battparam_flash_log_gst.batt_data_available_b == TRUE)
	{
		if(Battparam_flash_log_gst.batt_identification_num_log_u32 != MAX_U32)
		{
			BATT_PARM_batt_pack_details_gst.batt_identification_num_u32 = Battparam_flash_log_gst.batt_identification_num_log_u32;
		}
	}
#endif
}

void vBattParam_Update_Batt_Details(void)
{

#if 0  //after adding flash
	U32 flash_addr_u32 = BATT_PARAM_FLASH_LOG_ADDR;

	Battparam_flash_log_gst.batt_data_available_b = TRUE;
	Battparam_flash_log_gst.batt_identification_num_log_u32 = BATT_PARM_batt_pack_details_gst.batt_identification_num_u32;
	ucFlashDriver_Erase_Flash(ERASE_MODE_SECTOR, flash_addr_u32);
	ucFlash_Driver_App_Write_Data((U8*) &Battparam_flash_log_gst, &flash_addr_u32, sizeof(Battparam_flash_log_gst));
	flash_addr_u32 = BATT_PARAM_FLASH_LOG_ADDR;
	ucFlash_Driver_App_Read_Data((U8*) &Battparam_flash_log_gst, flash_addr_u32, sizeof(Battparam_flash_log_gst));
#endif

}


/***********************************************************************************************************************************************************************************
 * EOF
 **********************************************************************************************************************************************************************************/
