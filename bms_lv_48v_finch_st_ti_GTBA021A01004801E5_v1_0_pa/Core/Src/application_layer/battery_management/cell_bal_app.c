/*
========================================================================================================================
Title:      *
Filename:   CellBalApp.c
MCU:        S32K Family
Compiler:   ARM GCC Compiler
Author:     Bharath Raj
------------------------------------------------------------------------------------------------------------------------
Description:
------------------------------------------------------------------------------------------------------------------------
MISRA Exceptions:
------------------------------------------------------------------------------------------------------------------------
Notes:
------------------------------------------------------------------------------------------------------------------------
Rev_by      Date        Description
------      ----        -----------
#1_TL#      20-Aug-2019     Created
------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------
DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.
========================================================================================================================
*/

/** Common Header Files **/
#include "common_header.h"

/** Application Header File **/
#include "bms_config.h"
#include "batt_param.h"
#include "afe_driver.h"
#include "gt_system.h"

/** This header file **/
#include "cell_bal_app.h"

#define CELL_BAL_DEFAULT_DELTA_mV			(30)
#define CELL_BAL_HYSTERESIS_mV				(100)

/** Private Variable Definitions **/
cell_bal_config_status_tst cell_bal_config_status_gst =
{
	COM_HDR_TRUE, /* bal_completed_b */
	COM_HDR_FALSE,/* bal_enabled_b  */
	0x00, 			/* bal_register_u16 */
	30,	  /* balancing_delta_v_1mV_threshold_u8 */
	3400  /* balancing_cell_voltage_1mV_threshold_u16 */
};

U8 cell_bal_app_fet_cntrl_call_sts_u8;

BOOL cb_switch_req_b = COM_HDR_FALSE;
U8 cb_switch_counter_u8 = 0;
cell_bal_grp_te cell_bal_grp_e = BAL_GROUP_1;
/** Private Function Prototypes **/
static void cell_bal_app_balancing_algorithm_prv(void);

/** Public Variable Definitions **/

/** Public Function Definitions **/
void cell_bal_app_param_config_v(void);
void cell_bal_app_bal_cntrl_algorithm_v(void);
U8 cell_bal_app_fet_cntrl_u8();

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void cell_bal_app_param_config_v(void)
{
	/* If required update from the global structure */
}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void cell_bal_app_bal_cntrl_algorithm_v(void)
{
	/* max_cell voltage > threshold_cell_bal
	 * BATT shouldn't discharge
	 * BATT error : cell UV & cell OT
	 */
	if ((batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 > cell_bal_config_status_gst.balancing_cell_voltage_1mV_threshold_u16)
			&& (batt_param_pack_param_gst.pack_param_batt_mode_e != BATT_MODE_DSG)
			&& ((batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
			|| (batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_NA))
			&& ((batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				|| (batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE))) /* TODO: Need to uncommment once when thermistor is connected -- by krish 22102020 */
	{
		cell_bal_app_balancing_algorithm_prv();

		if(cell_bal_config_status_gst.bal_completed_b == COM_HDR_FALSE)
		{
			cell_bal_app_fet_cntrl_u8();
			cell_bal_config_status_gst.bal_enabled_b = COM_HDR_FALSE;
			/* Once complete then push the threshold to a bit higher value to avoid effect of hysteresis */
			//cell_bal_config_status_gst.balancing_delta_v_1mV_threshold_u8 = CELL_BAL_DEFAULT_DELTA_mV + CELL_BAL_HYSTERESIS_mV;
			cell_bal_config_status_gst.balancing_delta_v_1mV_threshold_u8 = CELL_BAL_DEFAULT_DELTA_mV;
		}

	}
	else
	{
		cell_bal_config_status_gst.bal_enabled_b = COM_HDR_FALSE;
		cell_bal_config_status_gst.bal_register_u16 = 0;

		if(cell_bal_app_fet_cntrl_call_sts_u8 == COM_HDR_ENABLED)
		{
			cell_bal_app_fet_cntrl_call_sts_u8 = COM_HDR_DISABLED;
			cell_bal_app_fet_cntrl_u8();
		}
	}
}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
U8 cell_bal_app_fet_cntrl_u8()
{
	U8 ret_val_u8 = 0;

	if(cell_bal_config_status_gst.bal_enabled_b == COM_HDR_FALSE)
	{
		cell_bal_config_status_gst.bal_register_u16 = 0x0000;
	}
	//cell_bal_config_status_gst.bal_register_u16 = 0x8FFF;
	ret_val_u8 = afe_driver_balancing_fet_control_u8(cell_bal_config_status_gst.bal_register_u16);

	if(ret_val_u8 != 0)
	{
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_CB_FET_MALFN );
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_CB_FET_MALFN );
	}

	return (ret_val_u8);
}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
static void cell_bal_app_balancing_algorithm_prv(void)
{
	U8 cell_id_u8 = 0;
	U8 current_slave_num_u8;
	U16 minimum_cell_voltage_u16 = 0;

	cell_bal_config_status_gst.bal_register_u16 = 0;


	if(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < 3600)
	{
		minimum_cell_voltage_u16 = 3600;
	}
	else
	{
	minimum_cell_voltage_u16 = batt_param_pack_param_gst.pack_param_min_cell_1mV_u16;
	}
	for (current_slave_num_u8 = PACK_PARAM_MOD_ID_1; current_slave_num_u8 < PACK_PARAM_MOD_ID_MAX_NUM; current_slave_num_u8++)
	{
		for (cell_id_u8 = 0; cell_id_u8 < BATT_PARAM_CELL_ID_MAX ; cell_id_u8++)
		{
			if (batt_param_cell_param_gst[current_slave_num_u8][cell_id_u8].cell_meas_1mV_u16
					> (minimum_cell_voltage_u16 + cell_bal_config_status_gst.balancing_delta_v_1mV_threshold_u8))
			{
				cell_bal_config_status_gst.bal_register_u16 |= (1 << cell_id_u8);
				cell_bal_config_status_gst.balancing_delta_v_1mV_threshold_u8 = CELL_BAL_DEFAULT_DELTA_mV;

				/* Still unbalanced */
				cell_bal_config_status_gst.bal_completed_b = COM_HDR_FALSE;

				/* Enable balancing */
				cell_bal_config_status_gst.bal_enabled_b = COM_HDR_TRUE;

				/*Flag is used to call cell balancing Mfet's only one time when all cells gets balanced*/
				cell_bal_app_fet_cntrl_call_sts_u8 = COM_HDR_ENABLED;
			}
			else
			{
				cell_bal_config_status_gst.bal_register_u16 &= ~(1 << cell_id_u8);
			}
		}
	}

}
