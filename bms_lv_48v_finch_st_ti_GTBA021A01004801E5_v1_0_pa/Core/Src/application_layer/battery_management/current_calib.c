/**
====================================================================================================================================================================================
@page current_calib_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	current_calib.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	16-Dec-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include "debug_comm.h"
#include "storage_def.h"

/* Configuration Header File */
#include "operating_system.h"
#include "flash_drv.h"
#include "can_app.h"
#include "afe_driver.h"

/* This header file */
#include "current_calib.h"

/* Driver header */
#include "gt_timer.h"
#include "gt_memory.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
can_app_message_tst 	current_calib_tx_msg_st;

/** @} */

/** @{
 *  Public Variable Definitions */
U8 current_calib_index_u8 = 0;

const S32 current_calib_const_i_as32[7] = {0,500,2500,10000,19000,28000,37000};
//const S32 current_calib_const_i_as32[7] = {0,500,1000,5000,10000,15000,20000};

current_calib_params_tst	current_calib_params_gst;
current_calib_equ_tst 		current_calib_equ_gst;

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void current_calib_init_v(void)
{
	U8 init_val_u8 = 0;

	flash_drv_readbyte_u8((U8*)&current_calib_equ_gst ,STORAGE_DEF_START_ADDRESS_CURRENT_CALIBRATION, sizeof(current_calib_equ_gst));

	if(current_calib_equ_gst.current_calib_gain_as32[2] <= 0 || current_calib_equ_gst.current_calib_gain_as32[2] == 0xFFFFFFFF)
	{
		memory_set_u8_array_v((U8*)current_calib_equ_gst.current_calib_offset_as32, init_val_u8, CURRENT_CALIB_SAMPLES);
		init_val_u8 = 1;
		memory_set_u8_array_v((U8*)current_calib_equ_gst.current_calib_gain_as32, init_val_u8, CURRENT_CALIB_SAMPLES);

		bms_config_nv_pi_cfg_gst.calib_config_st.current_calibration_done_b = 0;
	}
	else
	{
		bms_config_nv_pi_cfg_gst.calib_config_st.current_calibration_done_b = 1;
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void current_calib_process_v(current_calib_states_te calib_state_are)
{
	U8 calib_index_lu8;
	S32 slope_s32;
	current_calib_equ_tst curr_calib_equ_lst;

	switch(calib_state_are)
	{
		/**********************************************************************************************************************/
		case CURRENT_CALIB_START_ACK_STATE:
		{
			if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_FAULT)
			{
				current_calib_tx_msg_st.data_au8[0] = 0xFF;
			}
			else
			{
				current_calib_params_gst.current_calib_mfet_sts_b = COM_HDR_TRUE;
				gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 300);
				if((batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_BOTH_OFF
					|| batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_NA)
					&& batt_param_pack_param_gst.pack_param_batt_mode_e != BATT_MODE_FAULT)
				{
					//current_calib_tx_msg_st.data_au8[0] = 0xAA;
					current_calib_tx_msg_st.data_au8[0] = 0xFF;
				}
				else
				{
					//current_calib_tx_msg_st.data_au8[0] = 0xFF;
					current_calib_tx_msg_st.data_au8[0] = 0xAA;
				}
				current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] = current_calib_const_i_as32[0];//batt_param_pack_param_gst.pack_param_1mA_s32;;
				current_calib_equ_gst.current_calib_avg_current_as32[current_calib_index_u8] = 0;//ina226_csa_raw_data_gst.curr_s16;
				current_calib_index_u8 = current_calib_index_u8+1;
/*				for(current_calib_index_u8 = 0; current_calib_index_u8 < CURRENT_CALIB_SAMPLES; current_calib_index_u8++)
				{
					current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] = 0xFF;
					current_calib_equ_gst.current_calib_raw_adc_as32[current_calib_index_u8] = 0xFF;
				}*/
				current_calib_params_gst.current_calib_mfet_sts_b = COM_HDR_FALSE;
			}
			current_calib_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(DEBUG_COMM_CURRENT_CALIB_ST_PGN);
			current_calib_tx_msg_st.length_u8 = 1;
			can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &current_calib_tx_msg_st);
		}
		break;
		/**********************************************************************************************************************/
		case CURRENT_CALIB_SET_ACK_STATE:
		{
			//current_calib_index_u8 = debug_comm_can_rx_local_buff_au8[0];
			current_calib_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(DEBUG_COMM_CURRENT_CALIB_SET_PGN);
			current_calib_tx_msg_st.data_au8[0] = 0xAA;
			current_calib_tx_msg_st.data_au8[1] = current_calib_index_u8;
			current_calib_tx_msg_st.data_au8[2] = (current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8]);
			current_calib_tx_msg_st.data_au8[3] = (current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] >> 8);
			current_calib_tx_msg_st.data_au8[4] = (current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] >> 16);
			current_calib_tx_msg_st.data_au8[5] = (current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] >> 24);
			current_calib_tx_msg_st.length_u8 = 6;
			current_calib_index_u8++;
			can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &current_calib_tx_msg_st);
		}
		break;
		/**********************************************************************************************************************/
		case CURRENT_CALIB_COMPUTE_ACK_STATE:
		{
			for(calib_index_lu8 = 1; calib_index_lu8 < CURRENT_CALIB_SAMPLES; calib_index_lu8++)
			{
				if(current_calib_equ_gst.current_calib_current_as32[calib_index_lu8] < 0)
				{
					//current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] = (-1) * current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8];
					//current_calib_equ_gst.current_calib_raw_adc_as32[calib_index_lu8] = (-1) * current_calib_equ_gst.current_calib_raw_adc_as32[calib_index_lu8];
					current_calib_equ_gst.current_calib_avg_current_as32[calib_index_lu8] = (-1) * current_calib_equ_gst.current_calib_avg_current_as32[calib_index_lu8];
				}
			}
			for(calib_index_lu8 = 0; calib_index_lu8 < CURRENT_CALIB_SAMPLES-1; calib_index_lu8++)
			{
//				slope_s32 = (current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8+1] -
//							current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8]) * 1000;

				slope_s32 = (current_calib_const_i_as32[calib_index_lu8 + 1] - current_calib_const_i_as32[calib_index_lu8]) * 1000;
				slope_s32 = slope_s32 /(S32) (current_calib_equ_gst.current_calib_avg_current_as32[calib_index_lu8 + 1] -
							current_calib_equ_gst.current_calib_avg_current_as32[calib_index_lu8]);

				current_calib_equ_gst.current_calib_gain_as32[calib_index_lu8] = slope_s32;
//				current_calib_equ_gst.curr_calib_offset_as32[current_calib_index_u8] = (current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8]*1000)
//									- (slope_s32 * (S32) current_calib_equ_gst.current_calib_raw_adc_as32[current_calib_index_u8]);
				current_calib_equ_gst.current_calib_offset_as32[calib_index_lu8] = (current_calib_const_i_as32[calib_index_lu8]*1000)
									- (slope_s32 * (S32) current_calib_equ_gst.current_calib_avg_current_as32[calib_index_lu8]);
			}

			current_calib_equ_gst.current_calib_gain_as32[CURRENT_CALIB_SAMPLES - 1] = current_calib_equ_gst.current_calib_gain_as32[CURRENT_CALIB_SAMPLES - 2];
			current_calib_equ_gst.current_calib_offset_as32[CURRENT_CALIB_SAMPLES - 1] = current_calib_equ_gst.current_calib_offset_as32[CURRENT_CALIB_SAMPLES - 2];
			bms_config_nv_pi_cfg_gst.calib_config_st.current_calibration_done_b = 1;

			flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR, STORAGE_DEF_START_ADDRESS_CURRENT_CALIBRATION,500);
			gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 300);

			flash_drv_readbyte_u8((U8*)&curr_calib_equ_lst ,STORAGE_DEF_START_ADDRESS_CURRENT_CALIBRATION, sizeof(curr_calib_equ_lst));
			gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 200);

			flash_drv_writebyte_u8((U8*)&current_calib_equ_gst ,STORAGE_DEF_START_ADDRESS_CURRENT_CALIBRATION, sizeof(current_calib_equ_gst));
			gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 500);

			flash_drv_readbyte_u8((U8*)&curr_calib_equ_lst ,STORAGE_DEF_START_ADDRESS_CURRENT_CALIBRATION, sizeof(curr_calib_equ_lst));
			gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 200);

			current_calib_tx_msg_st.can_id_u32 = DEBUG_COMM_MAKE_PGN(DEBUG_COMM_CURRENT_CALIB_COMPUTE_PGN);
			current_calib_tx_msg_st.data_au8[0] = 0xAA;
			current_calib_tx_msg_st.length_u8 = 1;
			can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &current_calib_tx_msg_st);
		}
		break;
		/**********************************************************************************************************************/
		default:
		{

		}
		break;
		/**********************************************************************************************************************/
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void current_calib_estm_v(void)
{
	static S64 curr_val_s64 = 0;
	//static S64 adc_raw_val_s64 = 0;

//	if(battestimation_current_calib_gst.current_calib_tmr_counter_u32 >= BATT_EST_CURRENT_CALIB_RESOLUTION)
//	{
	current_calib_params_gst.current_calib_tmr_counter_u32 = 0;
	current_calib_params_gst.current_calib_samples_u8++;
	curr_val_s64 += batt_param_pack_param_gst.pack_param_1mA_s32;
	//adc_raw_val_s64 += ina226_csa_raw_data_gst.curr_s16;
	//adc_raw_val_s64 += afe_driver_raw_current_adc_val_s32;

	if(current_calib_params_gst.current_calib_samples_u8 >= CURRENT_CALIB_SAMPLE_RESOLUTION)
	{
		current_calib_equ_gst.current_calib_current_as32[current_calib_index_u8] = current_calib_const_i_as32[current_calib_index_u8];

		//current_calib_equ_gst.current_calib_avg_current_as32[current_calib_index_u8] = adc_raw_val_s64/CURRENT_CALIB_SAMPLE_RESOLUTION;
		current_calib_equ_gst.current_calib_avg_current_as32[current_calib_index_u8] = curr_val_s64/CURRENT_CALIB_SAMPLE_RESOLUTION;
		curr_val_s64 = 0;
		//adc_raw_val_s64 = 0;
		current_calib_params_gst.current_calib_samples_u8 = 0;

		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_CURRENT_CALIB_SET_ACK_STATE;
		common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;
		os_can_comm_queue_st.comm_event_e = OS_CAN_DEBUG_TX;
		os_can_comm_queue_st.can_rx_msg_pv = (U8 *)&current_calib_index_u8;
		//xQueueSendFromISR(os_can_comm_t9_qhandler_ge,  &os_can_comm_queue_st, NULL );
		xQueueSend(os_can_comm_queue_handler_ge,  &os_can_comm_queue_st, 0 );

		current_calib_params_gst.current_calib_flg_b = COM_HDR_DISABLED;
	}
	if(current_calib_index_u8 >= CURRENT_CALIB_SAMPLES)
	{
		current_calib_index_u8 = 0;
	}

//	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 current_calib_get_calib_raw_current_val_u8(S32 *raw_current_val_ars32)
{
	U8 ret_val_u8 = 0;
	S32 raw_current_val_local_s64 = *raw_current_val_ars32;

	if(bms_config_nv_pi_cfg_gst.calib_config_st.current_calibration_done_b == 1)
	{
		for (U8 index_u8 = 0; index_u8 < CURRENT_CALIB_MAX_CALIB_VALS; index_u8++)
		{
			if (index_u8 < (CURRENT_CALIB_MAX_CALIB_VALS - 1))
			{
				//	bms_config_nv_pi_cfg_gst.calib_config_st.curr_adc_val_as16[index_u8])
				if ((raw_current_val_local_s64 >= current_calib_equ_gst.current_calib_avg_current_as32[index_u8])
			        && (raw_current_val_local_s64 < current_calib_equ_gst.current_calib_avg_current_as32[index_u8+1]))
				{
					//temp_curr_1mA_s32 = 0;
					raw_current_val_local_s64 = ((current_calib_equ_gst.current_calib_gain_as32[index_u8] * raw_current_val_local_s64) \
					                		+ current_calib_equ_gst.current_calib_offset_as32[index_u8]);
					*raw_current_val_ars32 = raw_current_val_local_s64 / 1000;
					break;
				}
			}

			else
			{
				if (raw_current_val_local_s64 >= current_calib_equ_gst.current_calib_avg_current_as32[index_u8])
				{
					//temp_curr_1mA_s32 = 0;
					raw_current_val_local_s64 = (current_calib_equ_gst.current_calib_gain_as32[index_u8] * raw_current_val_local_s64)
					                		+ current_calib_equ_gst.current_calib_offset_as32[index_u8];
					*raw_current_val_ars32 = raw_current_val_local_s64 / 1000;
					break;
				}
				else
				{

					ret_val_u8 ++;
					return (ret_val_u8);
				}
			}
		}
	}

	return ret_val_u8;
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 current_calib_get_calib_current_val_u8(S32 *current_val_ars32)
{
	U8 ret_val_u8 = 0;
	S32 current_val_local_s32 = *current_val_ars32;

	if(bms_config_nv_pi_cfg_gst.calib_config_st.current_calibration_done_b == 1)
	{
		for (U8 index_u8 = 0; index_u8 < CURRENT_CALIB_MAX_CALIB_VALS; index_u8++)
		{
			if (index_u8 < (CURRENT_CALIB_MAX_CALIB_VALS - 1))
			{
				//	bms_config_nv_pi_cfg_gst.calib_config_st.curr_adc_val_as16[index_u8])
				if ((current_val_local_s32 >= current_calib_equ_gst.current_calib_avg_current_as32[index_u8])
			        && (current_val_local_s32 < current_calib_equ_gst.current_calib_avg_current_as32[index_u8+1]))
				{
					//temp_curr_1mA_s32 = 0;
					current_val_local_s32 = ((current_calib_equ_gst.current_calib_gain_as32[index_u8] * current_val_local_s32) \
					                		+ current_calib_equ_gst.current_calib_offset_as32[index_u8]);
					*current_val_ars32 = current_val_local_s32 / 1000;
					break;
				}
			}

			else
			{
				if (current_val_local_s32 >= current_calib_equ_gst.current_calib_avg_current_as32[index_u8])
				{
					//temp_curr_1mA_s32 = 0;
					current_val_local_s32 = (current_calib_equ_gst.current_calib_gain_as32[index_u8] * current_val_local_s32)
					                		+ current_calib_equ_gst.current_calib_offset_as32[index_u8];
					*current_val_ars32 = current_val_local_s32 / 1000;
					break;
				}
				else
				{

					ret_val_u8 ++;
					return (ret_val_u8);
				}
			}
		}
	}

	return ret_val_u8;
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
