/**
====================================================================================================================================================================================
@page common_var_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	common_var.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	30-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
//#include "storage_def.h"

/* This header file */
#include "common_var.h"

/* Driver header */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
#if 0

bms_vst_datalog_tst 	  				bms_vst_datalog_gst;
bms_bst_vin_datalog_tst					bms_bst_vin_datalog_gst;
bms_bst_datalog_tst						bms_bst_datalog_gst;
bms_battery_15min_log_tst				bms_battery_15min_log_gst, rd_bms_battery_15min_log_gst;
bms_drive_onetime_log_tst				bms_batt_onetime_log_gst,bms_drive_onetime_log_gst;
bms_drive_onetime_log_tst 				rd_bms_batt_onetime_log_gst;
bms_vehicle_1second_log_tst				bms_vehicle_1second_log_gst;
bms_vehicle_10second_log_tst			bms_vehicle_10second_log_gst,rd_bms_vehicle_10sec_data_log_gst;
bms_vehicle_60second_log_tst			bms_vehicle_60second_log_gst,rd_bms_vehicle_60sec_data_log_gst;
bms_battery_data_log_tst				bms_battery_data_log_gst,rd_bms_battery_data_log_gst;
bms_debug_battery_data_log_tst			bms_debug_battery_data_log_gst,rd_debug_battery_data_log_gst;
bms_debug_battery_data_log_tst			flsh_ret_debug_battery_data_log_gst;
bms_battcapacity_chargetime_cycle_tst	bms_battcapacity_chargetime_cycle_gst;


U32 start_address_bst_log_U32             = STORAGE_DEF_START_ADDRESS_BST_LOG;               //8192
U32 start_address_bmd_id_log_u32	  	  = STORAGE_DEF_START_ADDRESS_BIN_VIN_MISMATCH_LOG;  //16384
U32 start_address_vst_log_u32             = STORAGE_DEF_START_ADDRESS_VST_LOG;               //24576
U32 start_address_drive_onetime_log       = STORAGE_DEF_START_ADDRESS_VEHICLE_ONETIME_LOG;   //32768
U32 start_address_battery_onetime_log     = STORAGE_DEF_START_ADDRESS_BATTERY_ONETIME_LOG;   //32768
U32 start_address_vcu_60second_log        = STORAGE_DEF_START_ADDRESS_VCU_60SEC_LOG;         //57344
U32 start_address_VCU_10Second_Log        = STORAGE_DEF_START_ADDRESS_VCU_10SEC_LOG;         //77824
U32 start_address_BMS_15Minute_Log        = STORAGE_DEF_START_ADDRESS_BMS_15MIN_LOG;         //184320
U32 start_address_BMS_1Second_Log         = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;          //188416
U32 start_address_Charger_10Second_Log    = STORAGE_DEF_START_ADDRESS_CHARGER_10SEC_LOG;     //12111872
U32 start_address_Vehicle_1Second_Log     = STORAGE_DEF_START_ADDRESS_VEHICLE_1SEC_LOG;
U32 start_address_Charger_60Second_Log 	  = STORAGE_DEF_START_ADDRESS_CHARGER_60SEC_LOG;

U32 rd_addr_bms_1sec_log_u32;
U32 rd_addr_bms_10sec_log_u32;
U32 rd_addr_bms_60sec_log_u32;
U32 rd_addr_bms_15min_log_u32;
#endif

common_var_bms_id_log_tst 				common_var_bms_id_log_gst;
common_var_can_state_tst 				common_var_can_state_gst[COMMON_VAR_MAX_SM];

bms_drive_onetime_log_tst				bms_batt_onetime_log_gst,bms_drive_onetime_log_gst;

bms_debug_battery_data_log_tst			bms_debug_battery_data_log_gst,rd_debug_battery_data_log_gst;
bms_debug_battery_data_log_tst			flsh_ret_debug_battery_data_log_gst;
/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
