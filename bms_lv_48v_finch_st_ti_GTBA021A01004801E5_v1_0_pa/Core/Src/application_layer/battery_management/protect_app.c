/**
====================================================================================================================================================================================
@page protect_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	protect_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	07-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "bms_soc_estimation_task.h"
#include "common_header.h"

/* Application Header File */
#include "batt_param.h"
#include "bms_config.h"
#include "current_calib.h"
#include "gt_system.h"
#include "meas_app.h"
/* This header file */
#include "protect_app.h"

/* Driver header */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
U8 	 prt_app_dchg_flg_u8 = COM_HDR_DISABLED;
U8 	 prt_app_cont_dchg_flg_u8 = COM_HDR_DISABLED;
U8   prt_app_violation_samples_au8[PRT_PACK_MAX_STATE] = {0 ,};
U8   prt_app_warning_samples_au8[PRT_PACK_MAX_STATE] ={0,};
U8   prt_app_warning_violation_samples_au8[PRT_PACK_MAX_STATE] = {0 ,};
U8   prt_app_tmr_1s_au8[PRT_MAX_STATE]      = {0, };
BOOL prt_app_tmr_1s_flg_ab[PRT_MAX_STATE]   = {0, };

#if 0
U16 prt_app_breach_val_au16[MAX_BWI] = {0, }; /* NOTE : MAX_BWI = MAX_BFI*/
#endif
/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */
void protect_app_check_afe_violation_v(BOOL occured_arg_b);
void protect_app_check_afe_fet_malfn_v(BOOL occured_arg_b);

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_batt_mode_update_v(void)
{
	if(batt_param_pack_param_gst.pack_param_1mA_s32 > bms_config_nv_pi_cfg_gst.idle_chg_1mA_s16)
	{
		batt_param_pack_param_gst.pack_param_batt_mode_e = BATT_MODE_CHG;
	}
	else if(batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_pi_cfg_gst.idle_dsg_1mA_s16)
	{
		batt_param_pack_param_gst.pack_param_batt_mode_e = BATT_MODE_DSG;
	}
	else
	{
		batt_param_pack_param_gst.pack_param_batt_mode_e = BATT_MODE_IDLE;
	}

	if(batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_BOTH_OFF)
	{
		batt_param_pack_param_gst.pack_param_batt_mode_e = BATT_MODE_FAULT;
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_backup_previous_decision_v(void)
{
	static batt_param_action_te last_state_e = PACK_PARAM_ACTION_NA;

	if(last_state_e != batt_param_pack_param_gst.pack_param_trip_action_e)
	{
		batt_param_pack_param_gst.pack_param_prev_trip_action_e = last_state_e;
		last_state_e = batt_param_pack_param_gst.pack_param_trip_action_e;
	}
}

/************** Check functions - Monitors the failure & recovery of the failure  ******************/
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_cv_violation_v(void)
{



	if((batt_param_pack_param_gst.pack_param_max_cell_1mV_u16>bms_config_nv_cell_cfg_gst.cell_open_upper_threshold_u16)
			||(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16<bms_config_nv_cell_cfg_gst.cell_open_low_threshold_u16 ))
		{
			if(prt_app_violation_samples_au8[PRT_CELL_SHORT_OPEN]  > 5)
			{
				batt_param_pack_param_gst.pack_param_cell_open_short_limit_active = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HV);
//				return;
				prt_app_violation_samples_au8[PRT_CELL_SHORT_OPEN] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_SHORT_OPEN] ++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_cell_open_short_limit_active == PACK_PARAM_LIMIT_ACTIVE)
			{
				if((batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 < bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16)
						&&(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 > bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16))
				{
					batt_param_pack_param_gst.pack_param_cell_open_short_limit_active = PACK_PARAM_LIMIT_NOT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HV);
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_cell_open_short_limit_active = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HV);
			}
			prt_app_violation_samples_au8[PRT_CELL_SHORT_OPEN] = 0;
		}
	/* Cell OverVoltage */
#if 1
	if((batt_param_pack_param_gst.pack_param_max_cell_v_rechg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			&& (batt_param_pack_param_gst.pack_param_1mA_s32 > bms_config_nv_pi_cfg_gst.idle_chg_1mA_s16))
		{
			if (prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE] >= PROTECT_APP_MAX_RE_TMOUT)
					{
						batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
						system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_CI);
#if 0
						prt_app_breach_val_au16[CELL_OVER_VOLTAGE] = batt_param_pack_param_gst.pack_param_max_cell_1mV_u16;
#endif
						prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE] = 0;
					}
			prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE]++;
		}
	/* Cell OverVoltage */ /*TODO Enable SOC based voltage ocv*/
	else if (/*(ocv_soc_sfp*1000) > (bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16) ||*/ (batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 > bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16))
	{
		if (prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE] >= PROTECT_APP_MAX_CV_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					||(batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				batt_param_pack_param_gst.pack_param_max_cell_v_rechg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_CI);
#if 0
				prt_app_breach_val_au16[CELL_OVER_VOLTAGE] = batt_param_pack_param_gst.pack_param_max_cell_1mV_u16;
#endif

			}
			prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE] = 0;
		}

		prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE]++;
	}
	else
	{
		if(batt_param_pack_param_gst.pack_param_max_cell_v_rechg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if ((batt_param_pack_param_gst.pack_param_max_cell_1mV_u16) < bms_config_nv_cell_cfg_gst.release_max_cell_1mV_u16)
			{
				batt_param_pack_param_gst.pack_param_max_cell_v_rechg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_CI);
			}
		}

		batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_CI);

		prt_app_violation_samples_au8[PRT_CELL_OVER_VOLTAGE] = 0;
	}
#endif
	/****************************************************************************************************************/

	/* Cell UnderVoltage */
#if 1
	if((batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		&& (batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_pi_cfg_gst.idle_dsg_1mA_s16))
		{
			if (prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE] >= PROTECT_APP_MAX_RE_TMOUT)
			{
				batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_LV);
#if 0
				prt_app_breach_val_au16[CELL_UNDER_VOLTAGE] = batt_param_pack_param_gst.pack_param_min_cell_1mV_u16;
#endif
				prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE]++;
		}
	/*TODO: Add ocv based under voltage cutoff*/
	else if (/*((ocv_soc_sfp*1000) < bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16+150) || */(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < bms_config_nv_cell_cfg_gst.abs_min_cell_1mV_u16))
		{
			if (prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE] >= PROTECT_APP_MIN_CV_TMOUT)
			{
				if((batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE) || (batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_NA))
				{
					batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_LV);
#if 0
					prt_app_breach_val_au16[CELL_UNDER_VOLTAGE] = batt_param_pack_param_gst.pack_param_min_cell_1mV_u16;
#endif

				}
				prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE]++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				if ((batt_param_pack_param_gst.pack_param_min_cell_1mV_u16) > bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16)
				{
					batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_LV);
				}
			}
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_LV);
			batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;

			prt_app_violation_samples_au8[PRT_CELL_UNDER_VOLTAGE] = 0;
		}

#endif
	/****************************************************************************************************************/

	/* Cell Deep Discharge Voltage */
	//if ((ocv_soc_sfp*1000) < (bms_config_nv_cell_cfg_gst.deep_dsg_cell_1mV_u16+180) || (bms_config_nv_cell_cfg_gst.abs_max_cell_1mV_u16 < bms_config_nv_cell_cfg_gst.deep_dsg_cell_1mV_u16-100))
	if (batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < bms_config_nv_cell_cfg_gst.deep_dsg_cell_1mV_u16)
	{
		if (prt_app_violation_samples_au8[PRT_CELL_DEEP_DISCHARGE] >= PROTECT_APP_MIN_CDDV_TMOUT)
		{
			system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_DDCI);
			batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_CELL_DEEP_DISCHARGE] = 0;
		}
		prt_app_violation_samples_au8[PRT_CELL_DEEP_DISCHARGE]++;
	}
	else
	{
		//TODO: Shall we have the pre-charge always ON
		if (batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (/*(ocv_soc_sfp*1000)*/batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 > bms_config_nv_cell_cfg_gst.release_min_cell_1mV_u16)
			{
				batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DDCI);
			}
		}
		else
		{
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DDCI);
			batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		prt_app_violation_samples_au8[PRT_CELL_DEEP_DISCHARGE] = 0;
	}

	/****************************************************************************************************************/

	/* Cell voltage balancing error */
	if (bms_config_nv_cell_cfg_gst.bal_err_enable_flag_b == COM_HDR_TRUE)
	{
		if (batt_param_pack_param_gst.pack_param_bal_err_1cPC_u16 > bms_config_nv_cell_cfg_gst.max_bal_err_1cPC_u16)
		{
			if (prt_app_violation_samples_au8[PRT_CELL_BAL_ERROR] >= PROTECT_APP_CV_BAL_ERR_TMOUT)
			{
				batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_BAL);
				prt_app_violation_samples_au8[PRT_CELL_BAL_ERROR] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_BAL_ERROR]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				if (batt_param_pack_param_gst.pack_param_bal_err_1cPC_u16 < bms_config_nv_cell_cfg_gst.release_bal_err_1cPC_u16)
				{
					batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_BAL);
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_cell_v_bal_error_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_BAL);
			}
			prt_app_violation_samples_au8[PRT_CELL_BAL_ERROR] = 0;
		}
	}

	/****************************************************************************************************************/

}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_ct_violation_v(void)
{

	if(batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 < bms_config_nv_cell_cfg_gst.temp_open_thershold)
	{
		if (prt_app_violation_samples_au8[PRT_CELL_OPEN_TEMP] >= 5)
		{
			if(meas_app_temp_sts_u8 >= 2)
			{
				batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HCT);
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HDT);
				prt_app_violation_samples_au8[PRT_CELL_OPEN_TEMP] = 0;
			}
			else
			{
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
				batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
		}
		prt_app_violation_samples_au8[PRT_CELL_OPEN_TEMP]++;
	}
	else
	{
		if(batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active == PACK_PARAM_LIMIT_ACTIVE)
		{
			if(batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > bms_config_nv_cell_cfg_gst.release_min_cell_1cc_s16 )
			{
				batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
		}
		prt_app_violation_samples_au8[PRT_CELL_OPEN_TEMP] = 0;
	}

	if((batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG))
	{
		/* Cell OverTemperature Charging */
		if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 >= (bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16 - 1000)) /*50degC*/
		{
			if (prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] >= PROTECT_APP_MAX_CT_TMOUT)
			{
				if(batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				{
					batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HCT);
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
				}
#endif

				}
				prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 < (bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16 - 800)) /*47degC*/
				{
					batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
			}
			prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;
		}
	}
#if 1
	else if ((batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG))
	{
		/* Cell OverTemperature Dis-Charging */
		if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 > (bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16))//60
		{
			if (prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] >= PROTECT_APP_MAX_CT_TMOUT)
			{
				if(batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				{
					batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HDT);
#if 0
					if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
					{
						prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
					}
					else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
					{
						prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
					}
#endif
				}
				prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 < bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16)
				{
					batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
			}
				prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;
		}
	}
	else if ((batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_IDLE) ||(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_FAULT))
	{
		/* Cell OverTemperature Charging */
		if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 >= (bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16 - 1000)) /*50degC*/
		{
	//		if (prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] >= PROTECT_APP_MAX_CT_TMOUT)
	//		{
				if(batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				{
					batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HCT);
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
				}
#endif

				}
	//			prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;
	//		}
	//		prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP]++;
		}

		/* Cell OverTemperature Dis-Charging */
		if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 > (bms_config_nv_cell_cfg_gst.abs_max_cell_1cc_s16))//60
		{
	//		if (prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] >= PROTECT_APP_MAX_CT_TMOUT)
	//		{
				if(batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				{
					batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_C_HDT);
		#if 0
					if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
					{
						prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
					}
					else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
					{
						prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_max_cell_1cc_s16;
					}
		#endif
				}
	//			prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;
	//		}
	//		prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP]++;
		}
		if (batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 < (bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16 - 800)) /*47degC*/
			{
				batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HCT);
		}
//		prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;


		if (batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 < bms_config_nv_cell_cfg_gst.release_max_cell_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
		}
	//		prt_app_violation_samples_au8[PRT_CELL_OVER_TEMP] = 0;

	}
#endif
#if 0
	/****************************************************************************************************************/
	if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
	{
		/* Cell UnderTemperature Charging */
		if ((batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 < (bms_config_nv_cell_cfg_gst.abs_min_cell_1cc_s16 + 0))
			&& (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > (bms_config_nv_cell_cfg_gst.temp_open_thershold)))/*0degC*/
		{
			if (prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP] >= PROTECT_APP_MAX_CT_TMOUT)
			{
				if(batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				{
//					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_C_HDT);
					batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_min_cell_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_min_cell_1cc_s16;
				}
#endif

				}
				prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				if (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > (bms_config_nv_cell_cfg_gst.release_min_cell_1cc_s16 + 0)) /*5degC*/
				{
					batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
			prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
		}
	}
	else
	{
		/* Cell UnderTemperature Dis-Charging */
		if ((batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 < (bms_config_nv_cell_cfg_gst.abs_min_cell_1cc_s16 ))
			&& (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > (bms_config_nv_cell_cfg_gst.temp_open_thershold)))/*0degC*/
		{
			if (prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP] >= PROTECT_APP_MAX_CT_TMOUT)
			{
				if(batt_param_pack_param_gst.pack_param_cell_min_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				{
					batt_param_pack_param_gst.pack_param_cell_min_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
#if 0
					if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
					{
						prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_min_cell_1cc_s16;
					}
					else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
					{
						prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_min_cell_1cc_s16;
					}
#endif
				}
				prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
			}
			prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_cell_min_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				if (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > bms_config_nv_cell_cfg_gst.release_min_cell_1cc_s16) /*5degC*/
				{
					batt_param_pack_param_gst.pack_param_cell_min_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_cell_min_dchg_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
			prt_app_violation_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
		}
	}
#endif
	/****************************************************************************************************************/
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_pv_violation_v(void)
{


	/*Compare packvoltage by sum of cells and hvadc pack voltage */ //TODO check which is higher and find what range

	if (batt_param_pack_param_gst.pack_param_1cV_u16 > batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16)
	{
	   batt_param_pack_param_gst.pack_param_delta_pk_hvadc_pk_1cV_u16 = batt_param_pack_param_gst.pack_param_1cV_u16 - batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16;
	}
	else
	{
		batt_param_pack_param_gst.pack_param_delta_pk_hvadc_pk_1cV_u16 = batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16 - batt_param_pack_param_gst.pack_param_1cV_u16;
	}

	if (batt_param_pack_param_gst.pack_param_delta_pk_hvadc_pk_1cV_u16	> bms_config_nv_pv_cfg_gst.PV_delta_hvadc_pk_threshold_1cV_u16)
	{
		if (prt_app_violation_samples_au8[PRT_PACK_VOLTAGE] >= PROTECT_APP_MAX_PV_TMOUT)
		{
//			system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_DELTA_PV);
			batt_param_pack_param_gst.pack_param_pack_v_error_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_PACK_VOLTAGE] = 0;
		}
		prt_app_violation_samples_au8[PRT_PACK_VOLTAGE]++;
	}
	else
	{
		if (PACK_PARAM_LIMIT_ACTIVE == batt_param_pack_param_gst.pack_param_pack_v_error_limit_active_e)
		{
			if (batt_param_pack_param_gst.pack_param_delta_pk_hvadc_pk_1cV_u16 <
					bms_config_nv_pv_cfg_gst.PV_delta_hvadc_pk_threshold_1cV_u16)
			{
				batt_param_pack_param_gst.pack_param_pack_v_error_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DELTA_PV);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_pack_v_error_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DELTA_PV);
		}
		prt_app_violation_samples_au8[PRT_PACK_VOLTAGE] = 0;
	}


	/****************************************************************************************************************/
	/* Pack OverVoltage */
	if (batt_param_pack_param_gst.pack_param_1cV_u16 > bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16)
	{
		if (prt_app_violation_samples_au8[PRT_PACK_OVER_VOLTAGE] >= PROTECT_APP_MAX_PV_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					||(batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_P_HV);
#if 0
				prt_app_breach_val_au16[PACK_OVER_VOLTAGE] = batt_param_pack_param_gst.pack_param_1cV_u16;
#endif


			}
			prt_app_violation_samples_au8[PRT_PACK_OVER_VOLTAGE] = 0;
		}

		prt_app_violation_samples_au8[PRT_PACK_OVER_VOLTAGE]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_1cV_u16 < bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16)
			{
				batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_HV);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_HV);
		}
		prt_app_violation_samples_au8[PRT_PACK_OVER_VOLTAGE] = 0;
	}

	/****************************************************************************************************************/

	/* Pack UnderVoltage */
	if (batt_param_pack_param_gst.pack_param_1cV_u16 < bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16)
	{
		if (prt_app_violation_samples_au8[PRT_PACK_UNDER_VOLTAGE] > PROTECT_APP_MIN_PV_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				||(batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_P_LV);
#if 0
				prt_app_breach_val_au16[PACK_UNDER_VOLTAGE] = batt_param_pack_param_gst.pack_param_1cV_u16;
#endif
			}
			prt_app_violation_samples_au8[PRT_PACK_UNDER_VOLTAGE] = 0;
		}
		prt_app_violation_samples_au8[PRT_PACK_UNDER_VOLTAGE]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_1cV_u16 > bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16)
			{
				batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_LV);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_LV);
		}
		prt_app_violation_samples_au8[PRT_PACK_UNDER_VOLTAGE] = 0;
	}

	/****************************************************************************************************************/
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_fet_violation_v(void)
{
	/* Ambient Temperature Check */

	/* PCM Temperature Check */

	/* Thermal Management related parameter check */

	/* FET Over Temperature */
	if (batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16 > bms_config_nv_cell_cfg_gst.abs_max_mosfet_1cc_s16)
	{
		if (prt_app_violation_samples_au8[PRT_FET_OVER_TEMP] >= PROTECT_APP_MAX_PT_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					|| (batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_M_HT);
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16;
				}
#endif
			}
			prt_app_violation_samples_au8[PRT_FET_OVER_TEMP] = 0;
		}
		prt_app_violation_samples_au8[PRT_FET_OVER_TEMP]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16 < bms_config_nv_cell_cfg_gst.release_max_mosfet_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_M_HT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_M_HT);
		}
		prt_app_violation_samples_au8[PRT_FET_OVER_TEMP] = 0;
	}
	/****************************************************************************************************************/

	/* FET Under Temperature */
	if (batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16 < bms_config_nv_cell_cfg_gst.abs_min_mosfet_1cc_s16)
	{
		if (prt_app_violation_samples_au8[PRT_FET_UNDER_TEMP] > PROTECT_APP_MIN_PT_TMOUT)
		{

			if((batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					||(batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_M_LT);
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16;
				}
#endif

			}
			prt_app_violation_samples_au8[PRT_FET_UNDER_TEMP] = 0;
		}
		prt_app_violation_samples_au8[PRT_FET_UNDER_TEMP]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16 > bms_config_nv_cell_cfg_gst.release_min_mosfet_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_M_LT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_M_LT);
		}
		prt_app_violation_samples_au8[PRT_FET_UNDER_TEMP] = 0;
	}

	/****************************************************************************************************************/
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_pt_violation_v(void)
{
	/* Ambient Temperature Check */

	/* PCM Temperature Check */

	/* Thermal Management related parameter check */

	/* pack Over Temperature */
	if (batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 > bms_config_nv_cell_cfg_gst.abs_max_pack_1cc_s16)
	{
		if (prt_app_violation_samples_au8[PRT_PACK_OVER_TEMP] >= PROTECT_APP_MAX_PT_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					|| (batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_P_HT);
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_max_pack_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[OVER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_max_pack_1cc_s16;
				}
#endif
			}
			prt_app_violation_samples_au8[PRT_PACK_OVER_TEMP] = 0;
		}
		prt_app_violation_samples_au8[PRT_PACK_OVER_TEMP]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 < bms_config_nv_cell_cfg_gst.release_max_pack_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_HT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_HT);
		}
		prt_app_violation_samples_au8[PRT_PACK_OVER_TEMP] = 0;
	}
	/****************************************************************************************************************/

	/* Pack Under Temperature */
	if (batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 < bms_config_nv_cell_cfg_gst.abs_min_pack_1cc_s16)
	{
		if (prt_app_violation_samples_au8[PRT_PACK_UNDER_TEMP] > PROTECT_APP_MIN_PT_TMOUT)
		{

			if((batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					||(batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_P_LT);
#if 0
				if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
				{
					prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_CHARGING] = batt_param_pack_param_gst.pack_param_min_pack_1cc_s16;
				}
				else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
				{
					prt_app_breach_val_au16[UNDER_TEMPERATURE_DURING_DISCHARGING] = batt_param_pack_param_gst.pack_param_min_pack_1cc_s16;
				}
#endif

			}
			prt_app_violation_samples_au8[PRT_PACK_UNDER_TEMP] = 0;
		}
		prt_app_violation_samples_au8[PRT_PACK_UNDER_TEMP]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 > bms_config_nv_cell_cfg_gst.release_min_pack_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_LT);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_P_LT);
		}
		prt_app_violation_samples_au8[PRT_PACK_UNDER_TEMP] = 0;
	}

	/****************************************************************************************************************/
}

/**
=====================================================================================================================================================

@fn Name			: protect_app_check_pi_violation_v
@b Scope            : Global
@n@n@b Description  : Checking for pack current violation
@param Input Data   : NULL
@return Return Value: NULL

=====================================================================================================================================================
*/
void protect_app_check_pi_violation_v(void)
{
	/* Over Current in Charging */
	if (batt_param_pack_param_gst.pack_param_1mA_s32 > bms_config_nv_pi_cfg_gst.max_chg_1mA_s32)
	{
		if (prt_app_violation_samples_au8[PRT_PACK_OVER_CHG_CURRENT] >= PROTECT_APP_MAX_CHG_I_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				||(batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_CI);
#if 0
				prt_app_breach_val_au16[OVER_CURRENT_CHARGING] = (U16)(batt_param_pack_param_gst.pack_param_1mA_s32/100);
#endif

			}
			prt_app_violation_samples_au8[PRT_PACK_OVER_CHG_CURRENT] = 0;
		}
		prt_app_violation_samples_au8[PRT_PACK_OVER_CHG_CURRENT]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			prt_app_tmr_1s_flg_ab[PRT_OC_CHG] = COM_HDR_TRUE;
			if (prt_app_tmr_1s_au8[PRT_OC_CHG] >= PROTECT_APP_RECVRY_OC_CHG)
			{
				batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				prt_app_tmr_1s_flg_ab[PRT_OC_CHG] = COM_HDR_FALSE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_CI);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			prt_app_tmr_1s_flg_ab[PRT_OC_CHG] = COM_HDR_FALSE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_CI);
		}
		prt_app_violation_samples_au8[PRT_PACK_OVER_CHG_CURRENT] = 0;
	}
	/****************************************************************************************************************/

		/* Cont Current in Discharge */
		if (batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_pi_cfg_gst.cont_dsg_1mA_s32)
		{
			prt_app_cont_dchg_flg_u8 = COM_HDR_ENABLED;
			if (prt_app_violation_samples_au8[PRT_PACK_OVER_CONT_DSG_CURRENT] >= PROTECT_APP_CONT_DCHG_I_TMOUT)
			{
				if((batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
					||(batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_NA))
				{
					batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_DCI);
	#if 0
					prt_app_breach_val_au16[OVER_CURRENT_DISCHARGING] = (U16)(batt_param_pack_param_gst.pack_param_1mA_s32/100);
	#endif
				}
				prt_app_violation_samples_au8[PRT_PACK_OVER_CONT_DSG_CURRENT] = 0;
				prt_app_cont_dchg_flg_u8 = COM_HDR_DISABLED;
			}
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				prt_app_tmr_1s_flg_ab[PRT_OC_DSG] = COM_HDR_TRUE;
				if (prt_app_tmr_1s_au8[PRT_OC_DSG] > PROTECT_APP_RECVRY_OC_DCHG)
				{
					batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
					prt_app_tmr_1s_flg_ab[PRT_OC_DSG] = COM_HDR_FALSE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DCI);
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				prt_app_tmr_1s_flg_ab[PRT_OC_DSG] = COM_HDR_FALSE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DCI);
			}
			prt_app_violation_samples_au8[PRT_PACK_OVER_CONT_DSG_CURRENT] = 0;
			prt_app_dchg_flg_u8 = COM_HDR_DISABLED;
		}
	/****************************************************************************************************************/

	/* Over Current in Discharge */
	if (batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_pi_cfg_gst.max_dsg_1mA_s32)
	{
		prt_app_dchg_flg_u8 = COM_HDR_ENABLED;
		if (prt_app_violation_samples_au8[PRT_PACK_OVER_DSG_CURRENT] >= PROTECT_APP_MAX_DCHG_I_TMOUT)
		{
			if((batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_NOT_ACTIVE)
				||(batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_NA))
			{
				batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_DCI);
#if 0
				prt_app_breach_val_au16[OVER_CURRENT_DISCHARGING] = (U16)(batt_param_pack_param_gst.pack_param_1mA_s32/100);
#endif
			}
			prt_app_violation_samples_au8[PRT_PACK_OVER_DSG_CURRENT] = 0;
			prt_app_dchg_flg_u8 = COM_HDR_DISABLED;
		}
//		prt_app_violation_samples_au8[PRT_PACK_OVER_DSG_CURRENT]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			prt_app_tmr_1s_flg_ab[PRT_OC_DSG] = COM_HDR_TRUE;
			if (prt_app_tmr_1s_au8[PRT_OC_DSG] > PROTECT_APP_RECVRY_OC_DCHG)
			{
				batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				prt_app_tmr_1s_flg_ab[PRT_OC_DSG] = COM_HDR_FALSE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DCI);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			prt_app_tmr_1s_flg_ab[PRT_OC_DSG] = COM_HDR_FALSE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_DCI);
		}
		prt_app_violation_samples_au8[PRT_PACK_OVER_DSG_CURRENT] = 0;
		prt_app_dchg_flg_u8 = COM_HDR_DISABLED;
	}
	/****************************************************************************************************************/

	/* SCD Current in Discharge */
		if (batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_pi_cfg_gst.max_scd_surge_1mA_s32)
		{
				batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_SCD);
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				prt_app_tmr_1s_flg_ab[PRT_SCD_SW] = COM_HDR_TRUE;
				if (prt_app_tmr_1s_au8[PRT_SCD_SW] > PROTECT_APP_RECVRY_SC_DCHG)
				{

					batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
					prt_app_tmr_1s_flg_ab[PRT_SCD_SW] = COM_HDR_FALSE;
					system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_SCD);
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				prt_app_tmr_1s_flg_ab[PRT_SCD_SW] = COM_HDR_FALSE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_SCD);
			}
		}
}

/**
=====================================================================================================================================================

@fn Name			: protect_app_check_mt_Violation_v
@b Scope            : Global
@n@n@b Description  : Check for MOSFET Over & Under temperature violation
@param Input Data   : NIL
@return Return Value: NIL

=====================================================================================================================================================
*/
void protect_app_check_afe_die_temp_violation_v(void)
{

	/* afe die OverTemperature */
	if (batt_param_pack_param_gst.pack_param_afe_max_die_temp_1cc_s16 > bms_config_nv_cell_cfg_gst.abs_max_afe_dieT_1cc_s16)
	{
		if (prt_app_violation_samples_au8[PRT_AFE_OVER_TEMP] >= PROTECT_APP_MAX_MT_TMOUT)
		{
			batt_param_pack_param_gst.pack_param_pack_max_die_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_AFE_OVER_TEMP] = 0;
		}
		prt_app_violation_samples_au8[PRT_AFE_OVER_TEMP]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_pack_max_die_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_afe_max_die_temp_1cc_s16 < bms_config_nv_cell_cfg_gst.release_max_afe_dieT_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_pack_max_die_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_pack_max_die_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		prt_app_violation_samples_au8[PRT_AFE_OVER_TEMP] = 0;
	}
	/****************************************************************************************************************/

	/* afe die  UnderTemperature */
	if (batt_param_pack_param_gst.pack_param_afe_min_die_temp_1cc_s16 < bms_config_nv_cell_cfg_gst.abs_min_afe_dieT_1cc_s16)
	{
		if (prt_app_violation_samples_au8[PRT_AFE_UNDER_TEMP] >= PROTECT_APP_MIN_MT_TMOUT)
		{
			batt_param_pack_param_gst.pack_param_pack_min_die_temp_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_AFE_UNDER_TEMP] = 0;
		}
		prt_app_violation_samples_au8[PRT_AFE_UNDER_TEMP]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_pack_min_die_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_afe_min_die_temp_1cc_s16 > bms_config_nv_cell_cfg_gst.release_min_afe_dieT_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_pack_min_die_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_pack_min_die_temp_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		prt_app_violation_samples_au8[PRT_AFE_UNDER_TEMP] = 0;
	}
	/****************************************************************************************************************/

}

#if 1
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_hvadc_violation_v(void)
{
	/* Hvadc pack overvoltage */
	if (batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16 > bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16)
	{
		if (prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_OV] >= PROTECT_APP_HVADC_TMOUT)
		{
			batt_param_pack_param_gst.pack_param_hvadc_pk_ov_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_OV] = 0;
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_OV]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_hvadc_pk_ov_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16 < bms_config_nv_pv_cfg_gst.PV_release_upper_threshold_1cV_u16)
			{
				batt_param_pack_param_gst.pack_param_hvadc_pk_ov_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_hvadc_pk_ov_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_OV] = 0;
	}

	/*****************************************************************************************************************************************/

	/* Hvadc pack undervoltage */
	if (batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16 < bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16)
	{
		if (prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_UV] > PROTECT_APP_HVADC_TMOUT)
		{
			batt_param_pack_param_gst.pack_param_hvadc_pk_uv_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_UV] = 0;
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_UV]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_hvadc_pk_uv_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if (batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16 > bms_config_nv_pv_cfg_gst.PV_release_lower_threshold_1cV_u16)
			{
				batt_param_pack_param_gst.pack_param_hvadc_pk_uv_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_hvadc_pk_uv_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_PACK_UV] = 0;
	}

	/*****************************************************************************************************************************************/
	/* Hvadc fuse voltage violation check */
#if 0
	if((batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_fuse_1cV_u16 > 200)
		||((bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16-1 > batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16)
					||(bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16+1 < batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16)))/* TODO: Later need to add the correct threshold value */
#endif
	if((batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_fuse_1cV_u16 > 500)
			|| (batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16 <= 1000)
			|| (batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16 == BATT_PARAM_AI_FLT_VAL_U16))
	{
		if (prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG] > PROTECT_APP_HVADC_TMOUT)
		{
			batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG] = 0;
			system_diag_app_runtime_flt_code_gu64 |= (1 << SYSTEM_RT_FLT_BIT_HVADC_FUS_SNS);
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if(( (bms_config_nv_pv_cfg_gst.PV_lower_threshold_1cV_u16- 300) < batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16)
					&& ((bms_config_nv_pv_cfg_gst.PV_upper_threshold_1cV_u16 + 600) > batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16))
			{
				batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
				system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_HVADC_FUS_SNS);
			}
		}
		else
		{
			batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			system_diag_app_runtime_flt_code_gu64 &= ~(1 << SYSTEM_RT_FLT_BIT_HVADC_FUS_SNS);
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG] = 0;
	}
	/*****************************************************************************************************************************************/
#if 0
	/* Hvadc poscon voltage violation check when both contactor is closed */
	if( ADC_INIT_U16 != batt_param_pack_param_gst.pack_param_hvadc_poscon_vtg_1cv_u16   )
	{

		if(batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_poscon_1cV_u16 > 200)
		{
			if (prt_app_violation_samples_au8[PRT_APP_HVADC_POSCON_VTG] > PROTECT_APP_HVADC_TMOUT)
			{
				batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_poscon_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				prt_app_violation_samples_au8[PRT_APP_HVADC_POSCON_VTG] = 0;
			}
			prt_app_violation_samples_au8[PRT_APP_HVADC_POSCON_VTG]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_poscon_limit_active_e == PACK_PARAM_LIMIT_ACTIVE) //TODO INCUDE release timing
			{
				batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_poscon_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
			else
			{
				batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_poscon_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
			prt_app_violation_samples_au8[PRT_APP_HVADC_POSCON_VTG] = 0;
		}
	}
#endif

#if 0
	/* Hvadc fuse voltage violation check */
	if(batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_fuse_1cV_u16 > 40)	/* TODO: Later need to add the correct threshold value */
	{
		if (prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG] > PROTECT_APP_HVADC_TMOUT)
		{
			batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
			prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG] = 0;
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG]++;
	}
	else
	{
		if (batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		else
		{
			batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
		prt_app_violation_samples_au8[PRT_APP_HVADC_FUSE_VTG] = 0;
	}
#endif
	/*****************************************************************************************************************************************/
#if 0
	/* Hvadc mosfet voltage violation check when both mosfet is closed */
	if( (COM_HDR_MAX_U16 - 2) != batt_param_pack_param_gst.pack_param_hvadc_mfet_vtg_1cv_u16   )
	{
		/* TODO: Later need to add the correct threshold value */
		if(batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_mfet_1cV_u16 > 40)
		{
			if (prt_app_violation_samples_au8[PRT_APP_HVADC_MFET_VTG] > PROTECT_APP_HVADC_TMOUT)
			{
				batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_mfet_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
				prt_app_violation_samples_au8[PRT_APP_HVADC_MFET_VTG] = 0;
			}
			prt_app_violation_samples_au8[PRT_APP_HVADC_MFET_VTG]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_mfet_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			{
				batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_mfet_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
			else
			{
				batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_mfet_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			}
			prt_app_violation_samples_au8[PRT_APP_HVADC_MFET_VTG] = 0;
		}
	}
#endif
}




/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_hw_protection_recovery_v(void)
{
	/* Recovery */
	if (batt_param_pack_param_gst.pack_param_afe_functionality_error_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		prt_app_tmr_1s_flg_ab[PRT_AFE_HW] = COM_HDR_TRUE;
		if (prt_app_tmr_1s_au8[PRT_AFE_HW] > PROTECT_APP_RECVRY_HW_AFE_PRT)
		{
			batt_param_pack_param_gst.pack_param_afe_functionality_error_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
			prt_app_tmr_1s_flg_ab[PRT_AFE_HW] = COM_HDR_FALSE;
		}
	}
	else
	{
		batt_param_pack_param_gst.pack_param_afe_functionality_error_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		prt_app_tmr_1s_flg_ab[PRT_AFE_HW] = COM_HDR_FALSE;
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_afe_violation_v(BOOL occured_arg_b)
{
	if(occured_arg_b)
	{
		prt_app_violation_samples_au8[PRT_AFE_HW_RECV_COUNTER] = 0;
		prt_app_violation_samples_au8[PRT_AFE_HW_COUNTER]++;

		if(prt_app_violation_samples_au8[PRT_AFE_HW_COUNTER] > PROTECT_APP_AFE_ERR_ALLWD_OCCUR)
		{
			prt_app_violation_samples_au8[PRT_AFE_HW_COUNTER] = 6;
			batt_param_pack_param_gst.pack_param_afe_functionality_error_e = PACK_PARAM_LIMIT_ACTIVE;
		}
	}
	else
	{
		prt_app_violation_samples_au8[PRT_AFE_HW_RECV_COUNTER]++;

		if(prt_app_violation_samples_au8[PRT_AFE_HW_RECV_COUNTER] > PROTECT_APP_AFE_ERR_ALLWD_OCCUR)
		{
			prt_app_violation_samples_au8[PRT_AFE_HW_RECV_COUNTER] = 6;
			batt_param_pack_param_gst.pack_param_afe_functionality_error_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}

		prt_app_violation_samples_au8[PRT_AFE_HW_COUNTER] = 0;
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_afe_fet_malfn_v(BOOL occured_arg_b)
{
	if(occured_arg_b)
	{
		prt_app_violation_samples_au8[PRT_AFE_FET_RECV_MALFN] = 0;
		prt_app_violation_samples_au8[PRT_AFE_FET_MALFN]++;

		if(prt_app_violation_samples_au8[PRT_AFE_FET_MALFN] >= PROTECT_APP_AFE_FET_ERR_ALLWD_OCCUR)
		{
			batt_param_pack_param_gst.pack_param_afe_fet_malfunction_error_e = PACK_PARAM_LIMIT_ACTIVE;
		}
	}
	else
	{
		prt_app_violation_samples_au8[PRT_AFE_FET_RECV_MALFN] ++;

		if(prt_app_violation_samples_au8[PRT_AFE_FET_RECV_MALFN] > 10)
		{
			prt_app_violation_samples_au8[PRT_AFE_FET_MALFN] = 0;
			batt_param_pack_param_gst.pack_param_afe_fet_malfunction_error_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
		}
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 protect_app_safety_monitoring_u8(void)
{
	U8 ret_val_flag = 0;
	batt_param_action_te batt_param_taction_e = PACK_PARAM_ACTION_NA;
	/* P4 : HIGHEST PRIORITY FAULT -- Direct Decision */

	/* Irrecoverable fault - OT/UT (cell, Pack, AFE), OC_D, OC_C, SCD/XREADY(H/W), AFE Error , Cell open Wire,
	 * Cell open thermistor,  */
	if (//batt_param_pack_param_gst.pack_param_cell_min_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
		//	|| batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
		//	|| batt_param_pack_param_gst.pack_param_cell_min_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
		//	|| batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			batt_param_pack_param_gst.pack_param_pack_max_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			||batt_param_pack_param_gst.pack_param_fet_max_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			//||batt_param_pack_param_gst.pack_param_fet_min_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			//||batt_param_pack_param_gst.pack_param_pack_min_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u32 >= 1
		    || batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_afe_functionality_error_e == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_cell_open_short_limit_active == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_cell_open_temp_limit_active == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			||batt_param_pack_param_gst.pack_param_pack_max_die_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			|| batt_param_pack_param_gst.pack_param_pack_min_die_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			|| current_calib_params_gst.current_calib_mfet_sts_b == COM_HDR_TRUE)
	{
		//todo set pack_fault_status
		batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
		batt_param_pack_param_gst.pack_param_trip_action_e = batt_param_taction_e;
		return (ret_val_flag);
	}

#if 0 /* AFE fault status Based trip OFF action */
	U8 slave_id_u8 = 0;

	for (slave_id_u8 = SLAVE_IC_1; slave_id_u8 < SLAVE_NUM; slave_id_u8++) {
		if ((afe_driver_diag_status_un[slave_id_u8 - 1].diag_status_st.cell_leak_b == 1)
				|| (afe_driver_diag_status_un[slave_id_u8 - 1].diag_status_st.cell_open_detect_b == 1)
				|| (fault_register_tst[slave_id_u8 - 1].fault_status2_tun.faut_status2_bit_st.cb_open_flt_3)
				|| (fault_register_tst[slave_id_u8 - 1].fault_status2_tun.faut_status2_bit_st.gp_open_flt_6))
		{
			//todo set pack_fault_status
			batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
			return (ret_val_flag);

		}
	}
#endif

/*======================================================================================================================*/

	/* P2 : Pack Level Violation - LOWER PRIORITY than Cell Level Violation */

	/* Pack OV  */
	if (batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_ONLY_CHG_OFF;
	}
	/* Pack UV */
	else if (batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_ONLY_DHG_OFF;
	}
	/* Recovery is done here */
	else
	{
		batt_param_taction_e = PACK_PARAM_ACTION_NA;
	}

/*======================================================================================================================*/

	/* P3 : Cell Level Violation */

	/* Cell OV  + Cell UV */
	if (batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
			&& batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
		//TODO: Fault indication Del V is MAX ?
	}
#if 0
	/* Cell OV  */
	else if (batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_ONLY_CHG_OFF;
	}
#endif
	/* Cell OV  */
	else if ((batt_param_pack_param_gst.pack_param_max_cell_v_rechg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		|| (batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE))
	{
		batt_param_taction_e = PACK_PARAM_ACTION_ONLY_CHG_OFF;
	}
#if 0
	/* Cell UV */
	else if (batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_ONLY_DHG_OFF;
	}
#endif
	else if ((batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
			|| (batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE))
	{
		batt_param_taction_e = PACK_PARAM_ACTION_ONLY_DHG_OFF;
	}
	/* Recovery is only done if no pack fault is present */
	else if (batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE
			&& batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_NA;
	}


//	if(batt_param_taction_e != PACK_PARAM_ACTION_BOTH_OFF)
//	{
		if (batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE
				&& batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
			//sys_cot_flt_counter_u8++;
			//sys_dot_flt_counter_u8++;
			//TODO: Fault indication Del V is MAX ?
		}
		/* Cell OV  */
		else if (batt_param_pack_param_gst.pack_param_cell_max_chg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if(batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_ONLY_DHG_OFF)
			{
				batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
			}
			else
			{
				batt_param_taction_e = PACK_PARAM_ACTION_ONLY_CHG_OFF;
				//sys_cot_flt_counter_u8++;
			}
		}
		/* Cell OV dchg */
		else if (batt_param_pack_param_gst.pack_param_cell_max_dchg_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
		{
			if(batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_ONLY_CHG_OFF)
			{
				batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
			}
			else
			{
				batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
//				batt_param_taction_e = PACK_PARAM_ACTION_ONLY_DHG_OFF;
				//sys_dot_flt_counter_u8++;
			}
		}
		/* Recovery is only done if no pack fault is present */
		else if ((batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_active_e 	!= PACK_PARAM_LIMIT_ACTIVE
				&& batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE)
				&& (batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE
				&& batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e 	!= PACK_PARAM_LIMIT_ACTIVE)
				&& batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE)
		{
			batt_param_taction_e = PACK_PARAM_ACTION_NA;
		}
//	}
/*======================================================================================================================*/

	/* P1 : Overriding the recovery cases are handled here */
	/* Charging Current is +ve and discharging current is -ve */

	/* Trying to over charge */
	if (batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_ONLY_CHG_OFF
			&& batt_param_pack_param_gst.pack_param_1mA_s32 > (bms_config_nv_pi_cfg_gst.idle_chg_1mA_s16 /* *10*/))
	{
		batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
	}
	/* Trying to over discharge */
	if (batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_ONLY_DHG_OFF
			&& batt_param_pack_param_gst.pack_param_1mA_s32 < (bms_config_nv_pi_cfg_gst.idle_dsg_1mA_s16/* *10*/))
	{
		batt_param_taction_e = PACK_PARAM_ACTION_BOTH_OFF;
	}

/*======================================================================================================================*/

	/* Recovery by doing opposite action */
	/* batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_ONLY_DHG_OFF
				|| (batt_param_pack_param_gst.pack_param_trip_action_e == PACK_PARAM_ACTION_NA &&
				batt_param_taction_e == PACK_PARAM_ACTION_ONLY_DHG_OFF)*/
	/* Cell UV but charging */
	if (batt_param_taction_e == PACK_PARAM_ACTION_ONLY_DHG_OFF
			&& batt_param_pack_param_gst.pack_param_1mA_s32 > bms_config_nv_pi_cfg_gst.idle_chg_1mA_s16)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_NA;
		batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
	}
	/* Cell OV but discharging */
	if (batt_param_taction_e == PACK_PARAM_ACTION_ONLY_CHG_OFF
			&& batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_pi_cfg_gst.idle_dsg_1mA_s16)
	{
		batt_param_taction_e = PACK_PARAM_ACTION_NA;
		batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_active_e = PACK_PARAM_LIMIT_NOT_ACTIVE;
	}

	batt_param_pack_param_gst.pack_param_trip_action_e = batt_param_taction_e;
	return (ret_val_flag);
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_check_warning_flgs_v(void)
{
	/*======================================================================================================================*/
	/* Over Current Charging #1*/
		if (batt_param_pack_param_gst.pack_param_1mA_s32 > bms_config_nv_warn_mon_gst.warn_max_chg_1mA_s32)  /*20A*/
		{
			if (prt_app_warning_samples_au8[PRT_PACK_OVER_CHG_CURRENT] >= MAX_WARNING_TIMEOUT)
			{
				batt_param_pack_param_gst.pack_param_max_i_chg_limit_warning_b = COM_HDR_TRUE;
				prt_app_warning_samples_au8[PRT_PACK_OVER_CHG_CURRENT] = 0;
			}
			prt_app_warning_samples_au8[PRT_PACK_OVER_CHG_CURRENT]++;
		}
		else
		{
			if (batt_param_pack_param_gst.pack_param_max_i_chg_limit_warning_b == COM_HDR_TRUE)
			{


				prt_app_tmr_1s_flg_ab[PRT_OC_CHG_WARNING] = COM_HDR_TRUE;
				if(prt_app_tmr_1s_au8[PRT_OC_CHG_WARNING] >= PROTECT_APP_RECVRY_OC_CHG)
				{
					batt_param_pack_param_gst.pack_param_max_i_chg_limit_warning_b = COM_HDR_FALSE;
					prt_app_tmr_1s_flg_ab[PRT_OC_CHG_WARNING] = COM_HDR_FALSE;
					prt_app_tmr_1s_au8[PRT_OC_CHG_WARNING] = 0;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_max_i_chg_limit_warning_b = COM_HDR_FALSE;

			}
			prt_app_warning_samples_au8[PRT_PACK_OVER_CHG_CURRENT] = 0;
		}

		/* Over Current DisCharging #2*/
		if (batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_warn_mon_gst.warn_max_dsg_1mA_s32)
		{
			if (prt_app_warning_samples_au8[PRT_PACK_OVER_DSG_CURRENT] >= MAX_WARNING_TIMEOUT)
			{
				batt_param_pack_param_gst.pack_param_max_i_dsg_limit_warning_b = COM_HDR_TRUE;
				prt_app_warning_samples_au8[PRT_PACK_OVER_DSG_CURRENT] = 0;
			}
			prt_app_warning_samples_au8[PRT_PACK_OVER_DSG_CURRENT]++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_max_i_dsg_limit_warning_b == COM_HDR_TRUE)
			{
				prt_app_tmr_1s_flg_ab[PRT_OC_DSG_WARNING] = COM_HDR_TRUE;
				if(prt_app_tmr_1s_au8[PRT_OC_DSG_WARNING] >= PROTECT_APP_RECVRY_OC_DCHG)
				{
					batt_param_pack_param_gst.pack_param_max_i_dsg_limit_warning_b = COM_HDR_FALSE;
					prt_app_tmr_1s_flg_ab[PRT_OC_DSG_WARNING] = COM_HDR_FALSE;
					prt_app_tmr_1s_au8[PRT_OC_DSG_WARNING] = 0;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_max_i_dsg_limit_warning_b = COM_HDR_FALSE;
				prt_app_tmr_1s_flg_ab[PRT_OC_DSG_WARNING] = COM_HDR_FALSE;

			}

			prt_app_warning_samples_au8[PRT_PACK_OVER_DSG_CURRENT] = 0;
		}



		/* Surge Current - DisCharging #3*/
		if (batt_param_pack_param_gst.pack_param_1mA_s32 < bms_config_nv_warn_mon_gst.warn_max_scd_surge_1mA_s32)
		{
			if (prt_app_warning_samples_au8[PRT_SCD_FAULT] >= MAX_WARNING_TIMEOUT)
					{
							batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_warning_b = COM_HDR_TRUE;
							prt_app_warning_samples_au8[PRT_SCD_FAULT] = 0;
					}
			prt_app_warning_samples_au8[PRT_SCD_FAULT]++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_warning_b == COM_HDR_TRUE)
			{
				prt_app_tmr_1s_flg_ab[PRT_SCD_WARN] = COM_HDR_TRUE;
				if(prt_app_tmr_1s_au8[PRT_SCD_WARN] >= PROTECT_APP_RECVRY_SC_DCHG)
				{
					batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_warning_b = COM_HDR_FALSE;
					prt_app_tmr_1s_flg_ab[PRT_SCD_WARN] = COM_HDR_FALSE;
					prt_app_tmr_1s_au8[PRT_SCD_WARN] = 0;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_warning_b = COM_HDR_FALSE;
				prt_app_tmr_1s_flg_ab[PRT_SCD_WARN] = COM_HDR_FALSE;
			}
			prt_app_warning_samples_au8[PRT_SCD_FAULT] = 0;
		}

		/*======================================================================================================================*/
		/* Pack Under Voltage warning #4*/
		if (batt_param_pack_param_gst.pack_param_1cV_u16 < bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16)
		{

			if(prt_app_warning_samples_au8[PRT_PACK_UNDER_VOLTAGE] >= MAX_WARNING_TIMEOUT)
				{
					batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_warning_b = COM_HDR_TRUE;
					prt_app_warning_samples_au8[PRT_PACK_UNDER_VOLTAGE] = 0;
				}
			prt_app_warning_samples_au8[PRT_PACK_UNDER_VOLTAGE]++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_warning_b == COM_HDR_TRUE)

			{
				if(batt_param_pack_param_gst.pack_param_1cV_u16 > (bms_config_nv_warn_mon_gst.warn_pack_lower_threshold_1cV_u16+100))
				{
					batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_warning_b = COM_HDR_FALSE;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_min_pack_v_dchg_limit_warning_b = COM_HDR_FALSE;
			}

			prt_app_warning_samples_au8[PRT_PACK_UNDER_VOLTAGE]=0;
		}


		/* Pack Over Voltage warning #5*/
		if (batt_param_pack_param_gst.pack_param_1cV_u16 > bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16)
		{
			if(prt_app_warning_samples_au8[PRT_PACK_OVER_VOLTAGE] >= MAX_WARNING_TIMEOUT)
						{
							batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_warning_b = COM_HDR_TRUE;
							prt_app_warning_samples_au8[PRT_PACK_OVER_VOLTAGE] = 0;
						}
			prt_app_warning_samples_au8[PRT_PACK_OVER_VOLTAGE]++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_warning_b == COM_HDR_TRUE)
			{
				if (batt_param_pack_param_gst.pack_param_1cV_u16 < bms_config_nv_warn_mon_gst.warn_pack_upper_threshold_1cV_u16-100)
				{
					batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_warning_b = COM_HDR_FALSE;
				}
			}
			else
			{
			batt_param_pack_param_gst.pack_param_max_pack_v_chg_limit_warning_b = COM_HDR_FALSE;
			}
			prt_app_warning_samples_au8[PRT_PACK_OVER_VOLTAGE] = 0;
		}
	/*======================================================================================================================*/
		/* Cell Over Voltage warning #6*/
		//if ((ocv_soc_sfp*1000) > bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16)
		if(batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 >bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16)
		{
			if (prt_app_warning_samples_au8[PRT_CELL_OVER_VOLTAGE] >= MAX_WARNING_TIMEOUT)
			{
				batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_warning_b = COM_HDR_TRUE;
				prt_app_warning_samples_au8[PRT_CELL_OVER_VOLTAGE] = 0;
			}
			prt_app_warning_samples_au8[PRT_CELL_OVER_VOLTAGE]++;
		}
		else
		{

			if (batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_warning_b == COM_HDR_TRUE)
			{
				if (/*(ocv_soc_sfp*1000)*/ batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 < (bms_config_nv_warn_mon_gst.warn_max_cell_1mV_u16 - 100))
				{
					batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_warning_b = COM_HDR_FALSE;
				}
			}
			else
			{
				batt_param_pack_param_gst.pack_param_max_cell_v_chg_limit_warning_b = COM_HDR_FALSE;
			}
			prt_app_warning_samples_au8[PRT_CELL_OVER_VOLTAGE] = 0;
		}

		/* Cell Under Voltage warning #7 */
		//if ((ocv_soc_sfp * 1000) < bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16)
		if(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16)
		{

				if (prt_app_warning_samples_au8[PRT_CELL_UNDER_VOLTAGE] >= MAX_WARNING_TIMEOUT)
						{
							batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_warning_b = COM_HDR_TRUE;
							prt_app_warning_samples_au8[PRT_CELL_UNDER_VOLTAGE] = 0;
						}
				prt_app_warning_samples_au8[PRT_CELL_UNDER_VOLTAGE]++;
			}
			else
			{
				if(batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_warning_b == COM_HDR_TRUE)
				{
					if(/*(ocv_soc_sfp*1000)*/ batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 > (bms_config_nv_warn_mon_gst.warn_min_cell_1mV_u16+50))
						{
							batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_warning_b = COM_HDR_FALSE;
						}
				}
				batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_warning_b = COM_HDR_FALSE;
				prt_app_warning_samples_au8[PRT_CELL_UNDER_VOLTAGE] = 0;
			}
		/* Cell deep discharge warning #8 */
		if (/*(ocv_soc_sfp*1000) */batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < bms_config_nv_warn_mon_gst.warn_deep_discharge_1mV_u16)
			{
				if (prt_app_warning_samples_au8[PRT_CELL_DEEP_DISCHARGE] >= MAX_WARNING_TIMEOUT)
								{
									batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_warning_b = COM_HDR_TRUE;
									prt_app_warning_samples_au8[PRT_CELL_DEEP_DISCHARGE] = 0;
								}
				prt_app_warning_samples_au8[PRT_CELL_DEEP_DISCHARGE]++;

			}
			else
			{
				if(batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_warning_b == COM_HDR_TRUE)
				{
					if (/*(ocv_soc_sfp*1000)*/ batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 > (bms_config_nv_warn_mon_gst.warn_deep_discharge_1mV_u16+100))
					{
						batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_warning_b = COM_HDR_FALSE;
					}
				}
				else
				{
					batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_warning_b = COM_HDR_FALSE;
				}
				prt_app_warning_samples_au8[PRT_CELL_DEEP_DISCHARGE] = 0;
			}

		/*======================================================================================================================*/
		if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
		{
		/* Max cell temperature warning Charging #9*/
		if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 > (bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16 - 1500) ) /*5500 - 1500 = 4000*/
		{
			if (prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP] >= MAX_WARNING_TIMEOUT)
									{
									batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b = COM_HDR_TRUE;
									prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP] = 0;
									}
			prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP]++;
		}
		else
		{
				if(batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b == COM_HDR_TRUE)
				{
					if(batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 < (bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16- 1800)) /*release warning - 3700*/
					{
						batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b = COM_HDR_FALSE;
					}
				}
				else
				{
					batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b = COM_HDR_FALSE;
				}
			prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP] = 0;
		}
		}
		else{
			/* Max cell temperature warning Dis-Charging #10*/
					if (batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 > (bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16) )
					{
						if (prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP] >= MAX_WARNING_TIMEOUT)
												{
												batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b = COM_HDR_TRUE;
												prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP] = 0;
												}
						prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP]++;
					}
					else
					{
							if(batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b == COM_HDR_TRUE)
							{
								if(batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 < (bms_config_nv_warn_mon_gst.warn_max_cell_1cc_s16- 300)) /*5200*/
								{
									batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b = COM_HDR_FALSE;
								}
							}
							else
							{
								batt_param_pack_param_gst.pack_param_max_cell_temp_limit_warn_b = COM_HDR_FALSE;
							}
						prt_app_warning_samples_au8[PRT_CELL_OVER_TEMP] = 0;
					}

		}
		/* Min cell temperature warning - Charging #11*/
		if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
		{
		if (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 < (bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16 + 1000)) /*1000+ 1000 = 2000*/
		{
			if (prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP] >= MAX_WARNING_TIMEOUT)
			{
				batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b = COM_HDR_TRUE;
				prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
			}
			prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP]++;
		}
		else
		{
			if(batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b == COM_HDR_TRUE)
			{
				if (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > (bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16 + 1300)) /*1000 +1300 = 2300*/
				{
					batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b = COM_HDR_FALSE;
				}
			}
			else
			{
			  batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b = COM_HDR_FALSE;
			}
			prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
		}
		}
		else
		{
			/* Min cell temperature warning - DisCharging #12*/
			if (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 < bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16)
			{
				if (prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP] >= MAX_WARNING_TIMEOUT)
				{
					batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b = COM_HDR_TRUE;
					prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
				}
				prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP]++;
			}
			else
			{
				if(batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b == COM_HDR_TRUE)
				{
					if (batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 > bms_config_nv_warn_mon_gst.warn_min_cell_1cc_s16+300) /*1000 + 300 = 1300*/
					{
						batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b = COM_HDR_FALSE;
					}
				}
				else
				{
				  batt_param_pack_param_gst.pack_param_min_cell_temp_limit_warn_b = COM_HDR_FALSE;
				}
				prt_app_warning_samples_au8[PRT_CELL_UNDER_TEMP] = 0;
			}
		}


		/*Pack max temp #13*/
		if (batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 > bms_config_nv_warn_mon_gst.warn_max_pack_1cc_s16)
						{
							if (prt_app_warning_samples_au8[PRT_PACK_OVER_TEMP] >= MAX_WARNING_TIMEOUT)
													{
													batt_param_pack_param_gst.pack_param_max_pack_temp_limit_warn_b = COM_HDR_TRUE;
													prt_app_warning_samples_au8[PRT_PACK_OVER_TEMP] = 0;
													}
							prt_app_warning_samples_au8[PRT_PACK_OVER_TEMP]++;
						}
						else
						{
								if(batt_param_pack_param_gst.pack_param_max_pack_temp_limit_warn_b == COM_HDR_TRUE)
								{
									if(batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 < (bms_config_nv_warn_mon_gst.warn_max_pack_1cc_s16-200)) //5300
									{
										batt_param_pack_param_gst.pack_param_max_pack_temp_limit_warn_b = COM_HDR_FALSE;
									}
								}
								else
								{
									batt_param_pack_param_gst.pack_param_max_pack_temp_limit_warn_b = COM_HDR_FALSE;
								}
							prt_app_warning_samples_au8[PRT_PACK_OVER_TEMP] = 0;
						}
						/* Min pack temperature warning #14*/
						if (batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 < bms_config_nv_warn_mon_gst.warn_min_pack_1cc_s16)
						{
							if (prt_app_warning_samples_au8[PRT_PACK_UNDER_TEMP] >= MAX_WARNING_TIMEOUT)
							{
								batt_param_pack_param_gst.pack_param_min_pack_temp_limit_warn_b = COM_HDR_TRUE;
								prt_app_warning_samples_au8[PRT_PACK_UNDER_TEMP] = 0;
							}
							prt_app_warning_samples_au8[PRT_PACK_UNDER_TEMP]++;
						}
						else
						{
							if(batt_param_pack_param_gst.pack_param_min_pack_temp_limit_warn_b == COM_HDR_TRUE)
							{
								if (batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 > (bms_config_nv_warn_mon_gst.warn_min_pack_1cc_s16+200)) /*1700*/
								{
									batt_param_pack_param_gst.pack_param_min_pack_temp_limit_warn_b = COM_HDR_FALSE;
								}
							}
							else
							{
							  batt_param_pack_param_gst.pack_param_min_pack_temp_limit_warn_b = COM_HDR_FALSE;
							}
							prt_app_warning_samples_au8[PRT_PACK_UNDER_TEMP] = 0;
						}



			/*AFE max temp #15*/
			if (batt_param_pack_param_gst.pack_param_afe_max_die_temp_1cc_s16 > bms_config_nv_warn_mon_gst.warn_max_afe_temp_1cc_s16)
										{
											if (prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP] >= MAX_WARNING_TIMEOUT)
																	{
																	batt_param_pack_param_gst.pack_param_max_afe_die_temp_limit_warn_b = COM_HDR_TRUE;
																	prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP] = 0;
																	}
											prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP]++;
										}
										else
										{
												if(batt_param_pack_param_gst.pack_param_max_afe_die_temp_limit_warn_b == COM_HDR_TRUE)
												{
													if(batt_param_pack_param_gst.pack_param_afe_max_die_temp_1cc_s16 < (bms_config_nv_warn_mon_gst.warn_max_afe_temp_1cc_s16-300)) /*7200*/
													{
														batt_param_pack_param_gst.pack_param_max_afe_die_temp_limit_warn_b = COM_HDR_FALSE;
													}
												}
												else
												{
													batt_param_pack_param_gst.pack_param_max_afe_die_temp_limit_warn_b = COM_HDR_FALSE;
												}
											prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP] = 0;
										}
			/* Min AFE temperature warning #16*/
			if (batt_param_pack_param_gst.pack_param_afe_min_die_temp_1cc_s16 < bms_config_nv_warn_mon_gst.warn_min_afe_temp_1cc_s16)
			{
											if (prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP] >= MAX_WARNING_TIMEOUT)
											{
												batt_param_pack_param_gst.pack_param_min_afe_die_temp_limit_warn_b = COM_HDR_TRUE;
												prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP] = 0;
											}
											prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP]++;
										}
										else
										{
											if(batt_param_pack_param_gst.pack_param_min_afe_die_temp_limit_warn_b == COM_HDR_TRUE)
											{
												if (batt_param_pack_param_gst.pack_param_afe_min_die_temp_1cc_s16 > bms_config_nv_warn_mon_gst.warn_min_afe_temp_1cc_s16+300) /*1300*/
												{
													batt_param_pack_param_gst.pack_param_min_afe_die_temp_limit_warn_b = COM_HDR_FALSE;
												}
											}
											else
											{
											  batt_param_pack_param_gst.pack_param_min_afe_die_temp_limit_warn_b = COM_HDR_FALSE;
											}
											prt_app_warning_samples_au8[PRT_AFE_OVER_TEMP] = 0;
	}



	/*FET max temp #17*/
	if (batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[PACK_PARAM_MOD_ID_1] > bms_config_nv_warn_mon_gst.warn_max_mosfet_1cc_s16)
	{
		batt_param_pack_param_gst.pack_param_mosfet_max_temp_limit_warning_b = COM_HDR_TRUE;
	}
	else
	{
		batt_param_pack_param_gst.pack_param_mosfet_max_temp_limit_warning_b = COM_HDR_FALSE;
	}
}

void protect_app_afe_trip_duirng_chg_dsg(void)
{
		prt_app_violation_samples_au8[PRT_AFE_FAIL_DURING_OPERATION_ON]++;

		if(prt_app_violation_samples_au8[PRT_AFE_FAIL_DURING_OPERATION_ON] >= 3)
		{
			prt_app_violation_samples_au8[PRT_AFE_FAIL_DURING_OPERATION_ON]= 0;
			batt_param_pack_param_gst.pack_param_afe_malfunction_on_load_error_e = PACK_PARAM_LIMIT_ACTIVE;
		}


}

#if 0
/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void protect_app_scd_fault_repeated_occurance_v(void)
{
		prt_app_violation_samples_au8[PRT_APP_SCD_FAULT]++;

		if(prt_app_violation_samples_au8[PRT_APP_SCD_FAULT] >= 3)
		{
			prt_app_violation_samples_au8[PRT_APP_SCD_FAULT]= 0;
			batt_param_pack_param_gst.pack_param_scd_on_load_error_e = PACK_PARAM_LIMIT_ACTIVE;
		}

		/* TODO; IF required add it  */
		/* debug_comm_byte2_gu8 = prt_violation_samples_au8[PRT_APP_SCD_FAULT]; */
}

/**
 =====================================================================================================================================================

 @fn Name			 :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void protect_app_update_fault_code_v(void)
{
	if (batt_param_pack_param_gst.pack_param_scd_on_load_error_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_SCD_FAIL;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_afe_malfunction_on_load_error_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_AFE_HW_FAIL;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_afe_functionality_error_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_AFE_COMM_ERROR;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u32 >= 1)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_THERMISTOR_FAIL;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_max_i_scd_surge_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_SHORT_CIRCUIT;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_max_i_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_OVERCURRENT_DSG;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_max_i_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_OVERCURRENT_CHG;
		return;
	}

	if (batt_param_pack_param_gst.pack_param_cell_max_temp_chg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_OVERTEMP_CHG;
		return;
	}
	if (batt_param_pack_param_gst.pack_param_cell_max_temp_dsg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_OVERTEMP_DSG;
		return;
	}

/*	if (batt_param_pack_param_gst.pack_param_cell_max_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_OVERTEMP_DSG;
		return;
	}*/

	if (batt_param_pack_param_gst.pack_param_cell_min_temp_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
	{
		batt_param_pack_param_gst.pack_param_pack_fault_status_e = FAULT_PACK_UNDERTEMP_DSG;
		return;
	}

}

#endif
/************** Callback function definition ******************/
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void protect_app_tmr_1s_update_v(void)
{
	U8 index_u8 = 0 ;

	for(index_u8 = 0; index_u8 < PRT_MAX_STATE; index_u8++)
	{
		if(prt_app_tmr_1s_flg_ab[index_u8] == COM_HDR_TRUE)
		{
			prt_app_tmr_1s_au8[index_u8]++;
		}
		else
		{
			prt_app_tmr_1s_au8[index_u8] = 0;
		}
	}
}
#endif
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
