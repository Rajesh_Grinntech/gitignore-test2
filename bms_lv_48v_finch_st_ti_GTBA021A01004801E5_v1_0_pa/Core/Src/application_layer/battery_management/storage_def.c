/**
====================================================================================================================================================================================
@page storage_def_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	storage_def.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	30-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */

/* Application Header File */

/* Configuration Header File */
#include "flash_drv.h"

/* This header file */
#include "storage_def.h"

/* Driver header */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
U8 bootloader_current_sect_u16;

/** @} */

/* Public Function Definitions */
U8 storage_def_flash_store_data_u8(U32 *start_log_addr_arpu32,U32 max_log_addr_aru32,U8 *data_arpu8, U32 data_size_aru32);
U8 storage_def_flash_read_data_u8(U32 read_log_address_u32,U32 max_log_address_u32,U8* read_data_pu8, U32 read_data_size_u32);

/**
=====================================================================================================================================================

@fn Name			: storage_task_flash_store_data_u8
@b Scope            : global
@n@n@b Description  :
@param Input Data   :
@return Return Value: NA

=====================================================================================================================================================
*/
U8 storage_def_flash_store_data_u8(U32 *start_log_addr_arpu32,U32 max_log_addr_aru32,U8 *data_arpu8, U32 data_size_aru32)
{
	U8 spi_flash_write_status_u8 = COM_HDR_MAX_U8;

	if (*start_log_addr_arpu32 < max_log_addr_aru32)
	{
		spi_flash_write_status_u8 = flash_drv_app_wr_data_u8(data_arpu8,start_log_addr_arpu32, data_size_aru32);
	}
	else
	{
		spi_flash_write_status_u8++;
	}
	return spi_flash_write_status_u8;
}

/**
=====================================================================================================================================================

@fn Name			: storage_task_flash_read_data_u8
@b Scope            : global
@n@n@b Description  :
@param Input Data   :
@return Return Value: NA

=====================================================================================================================================================
*/
U8 storage_def_flash_read_data_u8(U32 read_log_addr_aru32,U32 max_log_addr_aru32,U8* read_data_arpu8, U32 read_data_size_aru32)
{
	U8 spi_flash_read_status_u8 = COM_HDR_MAX_U8;

	if (read_log_addr_aru32 < max_log_addr_aru32)
	{
		spi_flash_read_status_u8 = flash_drv_app_rd_data_u8(read_data_arpu8, read_log_addr_aru32, read_data_size_aru32);
	}
	else
	{
		spi_flash_read_status_u8++;
	}
	return spi_flash_read_status_u8;
}



/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
