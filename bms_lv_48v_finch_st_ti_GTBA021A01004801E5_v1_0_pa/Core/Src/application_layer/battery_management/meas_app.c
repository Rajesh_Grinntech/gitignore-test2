/**
====================================================================================================================================================================================
@page meas_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	meas_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	07-May-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

#include "batt_param.h"
#include "ai_app.h"

/* Configuration Header files */
#include "afe_driver.h"
#include "protect_app.h"
#include "current_calib.h"
#include "gt_timer.h"
#include "gt_system.h"

/* This header file */
#include "meas_app.h"

/* Driver header */
#include "bq76952.h"
//#include "ntc.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
S16 meas_app_adc_scaled_val_as16[BATT_PARAM_TEMP_ID_MAX];
meas_app_isense_tst meas_app_isense_st;
meas_app_cc_soc_tst meas_app_cc_soc_gst;

S16 curr_val_s16 = 0;
/** @} */

/** @{
 *  Public Variable Definitions */
U8 meas_app_temp_sts_u8 = 0;
BOOL meas_app_open_ckt_flg = 0;
/** @} */

/* Public Function Definitions */
void meas_app_init_cc(void);
void meas_app_compute_coulomb_count(void);
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 meas_app_init_u8(void)
{
	U8 ret_val_u8 = 0;

	/*  - Initial Configuration */

#if 0
	meas_app_init_cc();

	/* AFE - Initial configuration */
	//ret_val_u8 += afe_driver_configuration_u8();

	/* Start AFE ADC */
	if(ret_val_u8 == 0)
	{
		if(afe_driver_start_adc_measurement_u8(ADC_ALL_PARAMETERS))
		{
			ret_val_u8 += 1;


			// system_diag_app_fault_code_gu32 |= (1 << FAULT_BIT_AFE_START_MEAS);

			/* Try to wake-up one more time */
			if(afe_driver_init_u8() == 0)
			{
				/* Start the measurement again */
				if(afe_driver_start_adc_measurement_u8(ADC_ALL_PARAMETERS) == 0)
				{
					/* Show a success flag for the system */
					ret_val_u8 = 0;


					system_diag_app_fault_code_gu32 &= ~(1 << FAULT_BIT_AFE_START_MEAS);

					/* Notify the diag. task */
					diagmon_driver_init_fault_code_gu32 |= (1 << DIAG_BIT_AFE_PASS_SECOND_TRY);
				}
			}
		}
	}
#endif

	return (ret_val_u8);
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 meas_app_rd_afe_data_u8(void)
{
	U8 ret_val_u8 = 0;

	/* Read all the parameters from the AFE */
	if (afe_driver_read_all_parameter_u8())
	{
		ret_val_u8 += 1;
		protect_app_check_afe_violation_v(COM_HDR_TRUE);
		app_main_system_flt_status_u64 |= ((U64)1 << FAULT_BIT_AFE_READ);
		 //diagmon_driver_init_fault_code_gu32 |= (1 << DIAG_BIT_AFE_DATA_READY_FAILING);
	}
	else
	{
		app_main_system_flt_status_u64 &= ~((U64)1 << FAULT_BIT_AFE_READ);
		protect_app_check_afe_violation_v(COM_HDR_FALSE);
	}

#if 0

	/* Restart the AFE conversion */
	if(afe_driver_start_adc_measurement_u8(ADC_ALL_PARAMETERS))
	{
		ret_val_u8 += 1;
		/* Try to wake-up one more time */
		if (afe_driver_init_u8() == 0)
		{
			/* Start the measurement again */
			if (afe_driver_start_adc_measurement_u8(ADC_ALL_PARAMETERS) == 0)
			{
				/* Show a success flag for the system */
				ret_val_u8 = 0;

				/* Notify the diag. task */
				diagmon_driver_init_fault_code_gu32 |= (1 << DIAG_BIT_AFE_PASS_SECOND_TRY);
			}
		}
		else
		{

			 protect_app_check_afe_violation_v(COM_HDR_TRUE);
			diagmon_driver_init_fault_code_gu32 |= (1 << DIAG_BIT_AFE_COMM_FAILED);
		}
	}
	else
	{
		protect_app_check_afe_violation_v(COM_HDR_FALSE);
	}
#endif

	return (ret_val_u8);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
#if 0                                              /*  adc measurement has no temp sensors */
U8 meas_app_rd_adc_data_u8(void)
{
	U8 ret_val_u8 = 0;

	/* Get all the raw data form the ADC engine and update in ntc structure*/
	ret_val_u8 = ucNtc_Read_Adc_Data();

	if (ret_val_u8 == 0)
	{
		/* Only if it ready, update the new values */
		vNtc_Adc_Compute_Store_Temperature(MeasApp_adc_scaled_values_as16);
	}
	else
	{
		DIAGMON_driver_init_fault_code_gu32 |= (1 << DIAG_BIT_ADC_NTC_READY_FAILING);
	}

	/* LM73 Temperature get data */
	ret_val_u8 += ucLM73_Read_Temperature(&MeasApp_lm73_adc_scaled_value_s16);

	return (ret_val_u8);
}
#endif

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 meas_app_rd_current_sensor_data_u8(void)
{
	U8 ret_val_u8 = 0;
	//S16 curr_val_s16;
#if 0
	//S32 isense1_s32 = 0;
	//ai_app_status_gast[AI_APP_ISENSE_LC_AI_A].scaled_s32 = adc_app_data_gast[ADC_APP_ISENSE_LC_AI_A].scaled_s32;
	meas_app_isense_st.isense_raw_1mA_s32 = ai_app_status_gast[AI_APP_ISENSE_LC_AI_A].scaled_s32;
	if(meas_app_isense_st.isense_raw_1mA_s32 < 0)
	{
		meas_app_isense_st.isense_raw_1mA_s32 *= -1;
	}

	if(meas_app_isense_st.isense_raw_1mA_s32  < adc_app_current_sensor_config_st.isense_ch1_threshold_mA_s32 )
	{
		meas_app_isense_st.isense_unfiltered_s32 = ai_app_status_gast[AI_APP_ISENSE_LC_AI_A].scaled_s32;
	}
	else
	{
		meas_app_isense_st.isense_unfiltered_s32 = ai_app_status_gast[AI_APP_ISENSE_HC_AI_A].scaled_s32;
	}

	meas_app_isense_st.isense_unfiltered_s32 = (S32) (-1) * meas_app_isense_st.isense_unfiltered_s32;

	if (meas_app_isense_st.isense_unfiltered_s32 < 0)
	{
		if ((meas_app_isense_st.isense_unfiltered_s32*(-1))
				< adc_app_current_sensor_config_st.isense_limit_disconnect_mA_s16)
		{
			meas_app_isense_st.isense_unfiltered_s32 = 0;
		}
	}
	else
	{
		if (meas_app_isense_st.isense_unfiltered_s32
				< (adc_app_current_sensor_config_st.isense_limit_disconnect_mA_s16))
		{
			meas_app_isense_st.isense_unfiltered_s32 = 0;
		}
	}
	filter_app_process_s32_v(&filter_1mA_gst, meas_app_isense_st.isense_unfiltered_s32, FILTER_APP_1_8);

	meas_app_isense_st.isense_filtered_s32 = filter_1mA_gst.filtered_s32;
	batt_param_pack_param_gst.pack_param_1mA_s32 = filter_1mA_gst.filtered_s32;
	meas_app_compute_coulomb_count();
#endif

	/*#1 Update Current AFE */
	ret_val_u8 += afe_driver_read_current_u8( &curr_val_s16 );

#if 1
	if(curr_val_s16 >= -1 && curr_val_s16 <= 1)
	{
		batt_param_pack_param_gst.pack_param_1mA_s32 = 0;//(((curr_val_s16*509)-3345)/1000) ;//((~curr_val_s16) + 1);

	}
	else if (curr_val_s16 < 0)
	{
	batt_param_pack_param_gst.pack_param_1mA_s32 = ((curr_val_s16*51)/10) ;//((~curr_val_s16) + 1);
	}
	else
	{
		batt_param_pack_param_gst.pack_param_1mA_s32 = (((curr_val_s16*51)-752)/10) ;
	}
	ret_val_u8 += current_calib_get_calib_current_val_u8(&batt_param_pack_param_gst.pack_param_1mA_s32);
#else
	batt_param_pack_param_gst.pack_param_1mA_s32 = 10000; //10A
#endif
	return (ret_val_u8);
}

void meas_app_compute_coulomb_count(void)
{
	U32 delta_time_u32 = 0;
	meas_app_cc_soc_gst.new_sample_time_u32 = gt_timer_get_tick_u32();

		if(meas_app_cc_soc_gst.new_sample_time_u32 < meas_app_cc_soc_gst.old_sample_time_u32)
		{
			delta_time_u32 = (COM_HDR_MAX_U32 - meas_app_cc_soc_gst.old_sample_time_u32) + meas_app_cc_soc_gst.new_sample_time_u32;
		}
		else
		{
			delta_time_u32 = meas_app_cc_soc_gst.new_sample_time_u32 - meas_app_cc_soc_gst.old_sample_time_u32;
		}

		meas_app_cc_soc_gst.delta_1mAmSec_s32 = batt_param_pack_param_gst.pack_param_1mA_s32 * delta_time_u32;
		meas_app_cc_soc_gst.old_sample_time_u32 = meas_app_cc_soc_gst.new_sample_time_u32;

		/* This value will be update to the main variable and reseted after MEASUREMENT_UPDATE_TIME_10MS (30MS) (i.e, after 3 samples )*/
		meas_app_cc_soc_gst.cum_capacity_1mAmSec_s32 += meas_app_cc_soc_gst.delta_1mAmSec_s32;
}

void meas_app_init_cc(void)
{
		meas_app_cc_soc_gst.avg_current_1mA_s32 = 0;
		meas_app_cc_soc_gst.coulomb_cnt_scaled_s32 = 0;
		meas_app_cc_soc_gst.delta_1mAmSec_s32 = 0;
		meas_app_cc_soc_gst.cum_capacity_1mAmSec_s32 = 0;
		meas_app_cc_soc_gst.new_sample_time_u32 = gt_timer_get_tick_u32();
		meas_app_cc_soc_gst.old_sample_time_u32 = gt_timer_get_tick_u32();
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void meas_app_update_main_structure_v(void)
{
	/* Update the thermistor data */
		U8 channel_number_u8;
		U8 current_slave_num_u8 = 0;

		/* Initialize the faulty thermistor ID */
		batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u32 = 0;

		S16 cell_temp_1cc_s16 = 0;
		S16 mod_temp_1cc_s16 = 0;
		S32 cell_avg_temp_s32 = 0;
		S16 max_cell_temp_s16 = COM_HDR_MIN_S16;
		S16 min_cell_temp_s16 = COM_HDR_MAX_S16;
		U8 cell_index_maxT_u8 = COM_HDR_MIN_U8;
		U8 cell_index_minT_u8 = COM_HDR_MAX_U8;
		U8 mod_index_maxT_u8 = COM_HDR_MIN_U8;
		U8 mod_index_minT_u8 = COM_HDR_MAX_U8;
		S16 min_pack_temp_s16 = COM_HDR_MAX_S16;
		S16 max_pack_temp_s16 = COM_HDR_MIN_S16;
		S16 min_mod_die_temp_s16 = COM_HDR_MAX_S16;
		S16 max_mod_die_temp_s16 = COM_HDR_MIN_S16;
		U8 mod_index_max_dieT_u8 = COM_HDR_MIN_U8;
		U8 mod_index_min_dieT_u8 = COM_HDR_MAX_U8;

		/******************************************************** Cell Temperature *********************************/
		meas_app_temp_sts_u8 = 0;/** Resetting the Open threshold temperature before reading the values **/
#if 0
	for (current_slave_num_u8 = PACK_PARAM_MOD_ID_1; current_slave_num_u8 < PACK_PARAM_MOD_ID_MAX_NUM; current_slave_num_u8++)
	{
#endif
		for (channel_number_u8 = PACK_PARAM_THERMISTOR_ID_1; channel_number_u8 < BATT_PARAM_CELL_TEMP_ID_MAX; channel_number_u8++)
		{
			cell_temp_1cc_s16 = afe_driver_data_gst.cell_temp_1cc_as16[channel_number_u8]; /* 1d degC = 10degC -> 150dc = 15c*/
			batt_param_temp_param_gst[0][channel_number_u8].temp_meas_1cc_s16 = cell_temp_1cc_s16;
#if 0
			/* Thermistor (1-4) is used for measuring cell temperature */
			if (batt_param_temp_param_gst[current_slave_num_u8][channel_number_u8].temp_meas_1cc_s16 == NTC_SHORT_CIRCUIT_VALUE
					|| batt_param_temp_param_gst[current_slave_num_u8][channel_number_u8].temp_meas_1cc_s16 == NTC_OPEN_CIRCUIT_VALUE)
			{
				/*update faulty thermistor*/
				batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u64 |= (1 << channel_number_u8);
				continue;
			}
#endif


			cell_avg_temp_s32 += cell_temp_1cc_s16;

			/* Cell Minimum Temperature */
			if (cell_temp_1cc_s16 > max_cell_temp_s16)
			{
				max_cell_temp_s16 = cell_temp_1cc_s16;
				cell_index_maxT_u8 = channel_number_u8;
			}
			/* Cell Maximum Temperature */
			if (cell_temp_1cc_s16 < min_cell_temp_s16)
			{
				min_cell_temp_s16 = cell_temp_1cc_s16;
				cell_index_minT_u8 = channel_number_u8;
			}
			/* Open Threshold Temperature */
			if(cell_temp_1cc_s16 < bms_config_nv_cell_cfg_gst.temp_open_thershold)
			{
				meas_app_temp_sts_u8++;
				meas_app_open_ckt_flg = COM_HDR_ENABLED;
			}
		}

		batt_param_mod_param_gst[current_slave_num_u8].cell_index_maxT_e = cell_index_maxT_u8;
		batt_param_mod_param_gst[current_slave_num_u8].mod_max_1cc_s16 = max_cell_temp_s16;
		batt_param_mod_param_gst[current_slave_num_u8].cell_index_minT_e = cell_index_minT_u8;
		batt_param_mod_param_gst[current_slave_num_u8].mod_min_1cc_s16 = min_cell_temp_s16;
		max_cell_temp_s16 = COM_HDR_MIN_S16;
		min_cell_temp_s16 = COM_HDR_MAX_S16;
#if 0
	}
#endif
	batt_param_pack_param_gst.pack_param_avg_cell_1cc_s16 = (cell_avg_temp_s32/(BATT_PARAM_TOT_CELL_TEMP_ID_MAX - 9));



		/********************************************* Pack (or) Ambient Temperature Min & Max Calculation ******************************/
		/* Thermistor 6 is used for measuring Pack temperature */
#if 0
		for (channel_number_u8 = 0; channel_number_u8 < BATT_PARAM_PACK_TEMP_ID_MAX; channel_number_u8++)
		{
#endif
			batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16 = 	afe_driver_data_gst.pack_temp_1cc_s16;

			if((batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16  == NTC_SHORT_CIRCUIT_VALUE) ||
					(batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16  == NTC_OPEN_CIRCUIT_VALUE))
			{
				/*update faulty thermistor*/
				batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u32 |= (1 << 5 /*channel_number_u8*/);
				//continue;
			}
			/* pack Minimum Temperature */
			batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 = batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16;

			batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 = batt_param_pack_param_gst.pack_param_pk_temp_1cc_s16;
#if 0
			if (batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 > batt_param_temp_param_gst[channel_number_u8].temp_meas_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_min_pack_1cc_s16 =	batt_param_temp_param_gst[channel_number_u8].temp_meas_1cc_s16;
				batt_param_pack_param_gst.pack_param_min_T_pack_index_e = channel_number_u8;
			}
			/* pack Maximum Temperature */
			if (batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 < batt_param_temp_param_gst[channel_number_u8].temp_meas_1cc_s16)
			{
				batt_param_pack_param_gst.pack_param_max_pack_1cc_s16 =	batt_param_temp_param_gst[channel_number_u8].temp_meas_1cc_s16;
				batt_param_pack_param_gst.pack_param_max_T_pack_index_e = channel_number_u8;
			}

		}
#endif

		/********************************************* Mosfet Temperature Min & Max Calculation ******************************/
		/* Thermistor 5 is used for measuring Mosfet temperature */
		for (channel_number_u8 = 0; channel_number_u8 < BATT_PARAM_MFET_TEMP_ID_MAX; channel_number_u8++)
		{

			batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[ channel_number_u8] = afe_driver_data_gst.mfet_temp_1cc_s16;

			if((batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[ channel_number_u8]  == NTC_SHORT_CIRCUIT_VALUE) ||
					(batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[ channel_number_u8]  == NTC_OPEN_CIRCUIT_VALUE))
			{
				/*update faulty thermistor*/
				batt_param_pack_param_gst.pack_param_faulty_thermistor_id_u32 |= (1 << (channel_number_u8 + BATT_PARAM_CELL_TEMP_ID_MAX));
				continue;
			}

			batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16 = batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[channel_number_u8];

			batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16 = batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[channel_number_u8];

#if 0
			if (batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16 > batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[channel_number_u8])
			{
				batt_param_pack_param_gst.pack_param_min_mfet_1cc_s16 =	batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[channel_number_u8];
				batt_param_pack_param_gst.pack_param_min_T_mfet_index_e = channel_number_u8;
			}
			/* pack Maximum Temperature */
			if (batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16 < batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[channel_number_u8])
			{
				batt_param_pack_param_gst.pack_param_max_mfet_1cc_s16 =	batt_param_pack_param_gst.pack_param_mosfet_1cc_s16[channel_number_u8];
				batt_param_pack_param_gst.pack_param_max_T_mfet_index_e = channel_number_u8;
			}
#endif

		}

		/******************************************************** Cell Voltage ****************************************/

		/* Update the AFE data */
		U16 cell_volt_1mv_u16 = 0;
		U16 mod_volt_1mV_u16 = 0;
		S16 mod_die_temp_1cc_s16 = 0;
		U32 cell_avg_voltage_u32 = 0;
		U16 mod_avg_voltage_u16 = 0;
#if 0
		U16 mod_total_voltage_u16 = 0;
		U16 min_mod_1mV_u16 = COM_HDR_MAX_U16;
	    U16 max_mod_1mV_u16 = COM_HDR_MIN_U16;
#endif
		U16 max_cell_1mV_u16 = COM_HDR_MIN_U16;
		U16 min_cell_1mv_u16 = COM_HDR_MAX_U16;
		U8 cell_index_maxV_u8 = COM_HDR_MIN_U8;
		U8 cell_index_minV_u8 = COM_HDR_MAX_U8;
		U8 mod_index_maxV_u8 = COM_HDR_MIN_U8;
		U8 mod_index_minV_u8 = COM_HDR_MAX_U8;
		U16 min_pack_1mV_u16 = COM_HDR_MAX_U16;
		U16 max_pack_1mV_u16 = COM_HDR_MIN_U16;

#if 0
	for (current_slave_num_u8 = PACK_PARAM_MOD_ID_1; current_slave_num_u8 < PACK_PARAM_MOD_ID_MAX_NUM; current_slave_num_u8++)
	{
#endif
		for (channel_number_u8 = PACK_PARAM_CELL_ID_1;channel_number_u8 < BATT_PARAM_CELL_ID_MAX;channel_number_u8++)
		{
			cell_volt_1mv_u16 = afe_driver_data_gst.cell_voltage_au16[channel_number_u8];
			batt_param_cell_param_gst[current_slave_num_u8][channel_number_u8].cell_meas_1mV_u16 = cell_volt_1mv_u16;

			cell_avg_voltage_u32 +=
					batt_param_cell_param_gst[current_slave_num_u8][channel_number_u8].cell_meas_1mV_u16;


			/*Find min max cell voltage of the module */
			if(cell_volt_1mv_u16 > max_cell_1mV_u16)
			{
				max_cell_1mV_u16 = cell_volt_1mv_u16;
				cell_index_maxV_u8 = channel_number_u8;

			}
			if(cell_volt_1mv_u16 < min_cell_1mv_u16)
			{
				min_cell_1mv_u16 = cell_volt_1mv_u16;
				cell_index_minV_u8 =  channel_number_u8;
			}
			mod_avg_voltage_u16 += cell_volt_1mv_u16;
		}

		batt_param_mod_param_gst[current_slave_num_u8].mod_1mv_u16 = mod_avg_voltage_u16;
		batt_param_mod_param_gst[current_slave_num_u8].cell_index_maxV_e = cell_index_maxV_u8;
		batt_param_mod_param_gst[current_slave_num_u8].mod_max_1mv_u16 = max_cell_1mV_u16;
		batt_param_mod_param_gst[current_slave_num_u8].cell_index_minV_e = cell_index_minV_u8;
		batt_param_mod_param_gst[current_slave_num_u8].mod_min_1mv_u16 = min_cell_1mv_u16;
		max_cell_1mV_u16 = COM_HDR_MIN_U16;
		min_cell_1mv_u16 = COM_HDR_MAX_U16;
		mod_avg_voltage_u16 = 0;
#if 0
	}



		for (current_slave_num_u8 = PACK_PARAM_MOD_ID_1; current_slave_num_u8 < PACK_PARAM_MOD_ID_MAX_NUM; current_slave_num_u8++)
		{
#endif
			mod_die_temp_1cc_s16 = afe_driver_data_gst.int_temp_1cc_s16;
			batt_param_mod_param_gst[current_slave_num_u8].mod_die_T_S16 = mod_die_temp_1cc_s16;




			/* Minimum Voltage */
			mod_volt_1mV_u16 = batt_param_mod_param_gst[current_slave_num_u8].mod_min_1mv_u16;
			if(min_pack_1mV_u16 >  mod_volt_1mV_u16)
			{
				min_pack_1mV_u16 = mod_volt_1mV_u16;
				mod_index_minV_u8 = current_slave_num_u8;

			}

			/* Maximum Voltage */
			mod_volt_1mV_u16 = batt_param_mod_param_gst[current_slave_num_u8].mod_max_1mv_u16;
			if (max_pack_1mV_u16 < mod_volt_1mV_u16)
			{
				max_pack_1mV_u16 = mod_volt_1mV_u16;
				mod_index_maxV_u8 = current_slave_num_u8;
			}

			/*Minimum Temp*/
			mod_temp_1cc_s16 = batt_param_mod_param_gst[current_slave_num_u8].mod_min_1cc_s16;
			if(min_pack_temp_s16 > mod_temp_1cc_s16)
			{
				min_pack_temp_s16 = mod_temp_1cc_s16;
				mod_index_minT_u8 = current_slave_num_u8;
			}

			/*Max Temp*/
			mod_temp_1cc_s16 = batt_param_mod_param_gst[current_slave_num_u8].mod_max_1cc_s16;
			if(max_pack_temp_s16 < mod_temp_1cc_s16)
			{
				max_pack_temp_s16 = mod_temp_1cc_s16;
				mod_index_maxT_u8 = current_slave_num_u8;

			}

			/*Minimum Die Temp - AFE*/
			if(min_mod_die_temp_s16 > mod_die_temp_1cc_s16)
			{
				min_mod_die_temp_s16 = mod_die_temp_1cc_s16;
				mod_index_min_dieT_u8 = current_slave_num_u8;

			}

			/*Maximum Die Temp - AFE*/
			if( max_mod_die_temp_s16 < mod_die_temp_1cc_s16 )
			{
				max_mod_die_temp_s16 = mod_die_temp_1cc_s16;
				mod_index_max_dieT_u8 = current_slave_num_u8;
			}

#if 0
			mod_total_voltage_u16 = batt_param_mod_param_gst[current_slave_num_u8].mod_1mv_u16;
						if(min_mod_1mV_u16 > mod_total_voltage_u16)
						{
							min_mod_1mV_u16 = mod_total_voltage_u16;

						}

						if(max_mod_1mV_u16 < mod_total_voltage_u16)
						{
							max_mod_1mV_u16 = mod_total_voltage_u16;

						}





		}

		batt_param_pack_param_gst.pack_param_max_module_1mV_u16 = max_mod_1mV_u16;
			batt_param_pack_param_gst.pack_param_min_module_1mV_u16 = min_mod_1mV_u16;
#endif
	batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 =  min_pack_1mV_u16;
	batt_param_pack_param_gst.pack_param_min_V_cell_index_e = batt_param_mod_param_gst[mod_index_minV_u8].cell_index_minV_e;
	batt_param_pack_param_gst.pack_param_mod_at_minV_e = mod_index_minV_u8;
	batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 = max_pack_1mV_u16;
	batt_param_pack_param_gst.pack_param_max_V_cell_index_e = batt_param_mod_param_gst[mod_index_maxV_u8].cell_index_maxV_e;
	batt_param_pack_param_gst.pack_param_mod_at_maxV_e = mod_index_maxV_u8;

	batt_param_pack_param_gst.pack_param_min_cell_1cc_s16 = min_pack_temp_s16;
	batt_param_pack_param_gst.pack_param_min_T_cell_index_e = batt_param_mod_param_gst[mod_index_minV_u8].cell_index_minT_e;
	batt_param_pack_param_gst.pack_param_mod_at_minT_e = mod_index_minT_u8;
	batt_param_pack_param_gst.pack_param_max_cell_1cc_s16 = max_pack_temp_s16;
	batt_param_pack_param_gst.pack_param_max_T_cell_index_e = batt_param_mod_param_gst[mod_index_maxV_u8].cell_index_maxT_e;
	batt_param_pack_param_gst.pack_param_mod_at_maxT_e = mod_index_maxT_u8;

	batt_param_pack_param_gst.pack_param_afe_max_die_temp_1cc_s16 = max_mod_die_temp_s16;
	batt_param_pack_param_gst.pack_param_max_die_T_afe_index_e    = mod_index_max_dieT_u8;
    batt_param_pack_param_gst.pack_param_afe_min_die_temp_1cc_s16 = min_mod_die_temp_s16;
	batt_param_pack_param_gst.pack_param_min_die_T_afe_index_e = mod_index_min_dieT_u8;

	/* Delta in mV */
	batt_param_pack_param_gst.pack_param_delta_cell_1mV_u16 = batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 -
			batt_param_pack_param_gst.pack_param_min_cell_1mV_u16;

	/* Bal Error in % */
	batt_param_pack_param_gst.pack_param_bal_err_1cPC_u16 = (U16) ((U32)(batt_param_pack_param_gst.pack_param_delta_cell_1mV_u16 * 100)
																	/ (U32)(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 /*TODO Comment +1 */+ 1));

	/* Pack Voltage based on sum of cell voltage */

	batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16 = (U16)(cell_avg_voltage_u32 / BATT_PARAM_TOT_CELL_MAX);
	batt_param_pack_param_gst.pack_param_avg_cell_1cc_s16 = (S16)(cell_avg_temp_s32/(BATT_PARAM_TOT_CELL_TEMP_ID_MAX ));

	/*MCU Die Temperature*/
	batt_param_pack_param_gst.mcu_internal_die_temp_s16 = (S16)(ai_app_status_gast[AI_APP_INT_TEMP_AI_C].scaled_s32);

	/*Pack Voltages*/
	batt_param_pack_param_gst.pack_param_stack_vtg_1cV_u16 = (U16)(afe_driver_data_gst.tos_vtg_u16);
	batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16 = (U16)(afe_driver_data_gst.pack_pin_vtg_u16);
	batt_param_pack_param_gst.pack_param_1cV_u16 = (U16)(cell_avg_voltage_u32 / COM_HDR_SCALE_10);

	batt_param_pack_param_gst.pack_param_ld_det_vtg_1cV_u16 	= (U16)(afe_driver_data_gst.ld_pin_vtg_u16);

	if(ai_app_status_gast[AI_APP_FUSE_SENS_AI_MV].scaled_s32 < 0)
	{
		batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16 = (U16)(ai_app_status_gast[AI_APP_FUSE_SENS_AI_MV].scaled_s32 * -1);
	}
	else
	{
		batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16 = (U16)(ai_app_status_gast[AI_APP_FUSE_SENS_AI_MV].scaled_s32);
	}

	if (batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16
			> batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16) {
		batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_fuse_1cV_u16 =
				batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16
						- batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16;
	}
	else
	{
		batt_param_pack_param_gst.pack_param_delta_hvadc_betw_pk_fuse_1cV_u16 =
				batt_param_pack_param_gst.pack_param_hvadc_fuse_vtg_1cv_u16
						- batt_param_pack_param_gst.pack_param_hvadc_pack_vtg_1cv_u16;
	}

}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
