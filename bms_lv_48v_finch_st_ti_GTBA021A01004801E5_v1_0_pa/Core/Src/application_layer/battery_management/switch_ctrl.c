/**
====================================================================================================================================================================================
@page switch_ctrl_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	switch_ctrl.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	23-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */


/* Application Header File */
#include "batt_param.h"

/* Configuration Header File */


/* This header file */
#include "switch_ctrl.h"

/* Driver header */
#include "afe_driver.h"
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
static void switch_ctrl_main_mfet_ctrl(U8 fet_mode_aru8);
static void switch_ctrl_pre_mfet_ctrl(U8 fet_mode_aru8);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			: switch_ctrl_init_v
@b Scope            : global
@n@n@b Description  :
@param Input Data   :
@return Return Value: Null

=====================================================================================================================================================
*/
U8 switch_ctrl_init_u8()
{
	U8 ret_val_u8  = 0;

	return ret_val_u8;
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void switch_ctrl_mfet_ctrl_v(switch_ctrl_mfet_type_te mosfet_type_are, U8 mode_aru8)
{
	switch (mosfet_type_are)
	{
		/**********************************************************************************************************************/
		case SWITCH_CTRL_MAIN_MOSFET:
		{
			switch_ctrl_main_mfet_ctrl(mode_aru8);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_PRE_MOSFET:
		{
			switch_ctrl_pre_mfet_ctrl(mode_aru8);
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			/* Do something */
		}
		/**********************************************************************************************************************/
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void switch_ctrl_main_mfet_ctrl(U8 fet_mode_aru8)
{
	switch (fet_mode_aru8)
	{
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_BOTH_ON:
		{
			//afe_driver_fet_control_u8(AFE_DSG_FET, AFE_FET_ON);
			//afe_driver_fet_control_u8(AFE_CHG_FET, AFE_FET_ON);

			afe_driver_fet_control_u8(AFE_CHG_FET, AFE_FET_ON);
			afe_driver_fet_control_u8(AFE_DSG_FET, AFE_FET_ON);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_ONLY_DSG_ON:
		{
			afe_driver_fet_control_u8(AFE_CHG_FET, AFE_FET_OFF);
			afe_driver_fet_control_u8(AFE_DSG_FET, AFE_FET_ON);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_ONLY_CHG_ON:
		{
			afe_driver_fet_control_u8(AFE_CHG_FET, AFE_FET_ON);
			afe_driver_fet_control_u8(AFE_DSG_FET, AFE_FET_OFF);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_BOTH_OFF:
		{
			afe_driver_fet_control_u8(AFE_CHG_FET, AFE_FET_OFF);
			afe_driver_fet_control_u8(AFE_DSG_FET, AFE_FET_OFF);
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			afe_driver_fet_control_u8(AFE_CHG_FET, AFE_FET_OFF);
			afe_driver_fet_control_u8(AFE_DSG_FET, AFE_FET_OFF);
		}
		/**********************************************************************************************************************/
		#if 0
		/**********************************************************************************************************************/
		case PACK_PARAM_ACTION_NA:
		{
			SWITCH_CTRL_MAIN_DCHG_MFET_EN;
			SWITCH_CTRL_MAIN_CHG_MFET_EN;
		}
		break;
		/**********************************************************************************************************************/
		case PACK_PARAM_ACTION_ONLY_CHG_OFF:
		{
			SWITCH_CTRL_MAIN_DCHG_MFET_EN;
			SWITCH_CTRL_MAIN_CHG_MFET_DIS;
		}
		break;
		/**********************************************************************************************************************/
		case PACK_PARAM_ACTION_ONLY_DHG_OFF:
		{
			SWITCH_CTRL_MAIN_DCHG_MFET_DIS;
			SWITCH_CTRL_MAIN_CHG_MFET_EN;
		}
		break;
		/**********************************************************************************************************************/
		case PACK_PARAM_ACTION_BOTH_OFF:
		{
			SWITCH_CTRL_MAIN_DCHG_MFET_DIS;
			SWITCH_CTRL_MAIN_CHG_MFET_DIS;
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			SWITCH_CTRL_MAIN_DCHG_MFET_DIS;
			SWITCH_CTRL_MAIN_CHG_MFET_DIS;
		}
		/**********************************************************************************************************************/
		#endif
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void switch_ctrl_pre_mfet_ctrl(U8 fet_mode_aru8)
{
	switch (fet_mode_aru8)
	{
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_BOTH_ON:
		{
			afe_driver_fet_control_u8(AFE_PCHG_FET, AFE_FET_ON);
			afe_driver_fet_control_u8(AFE_PDSG_FET, AFE_FET_ON);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_ONLY_CHG_ON:
		{
			afe_driver_fet_control_u8(AFE_PCHG_FET, AFE_FET_ON);
			afe_driver_fet_control_u8(AFE_PDSG_FET, AFE_FET_OFF);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_ONLY_DSG_ON:
		{
			afe_driver_fet_control_u8(AFE_PCHG_FET, AFE_FET_OFF);
			afe_driver_fet_control_u8(AFE_PDSG_FET, AFE_FET_ON);
		}
		break;
		/**********************************************************************************************************************/
		case SWITCH_CTRL_ACTION_BOTH_OFF:
		{
			afe_driver_fet_control_u8(AFE_PCHG_FET, AFE_FET_OFF);
			afe_driver_fet_control_u8(AFE_PDSG_FET, AFE_FET_OFF);
		}
		break;
		/**********************************************************************************************************************/
		default:
		{
			afe_driver_fet_control_u8(AFE_PCHG_FET, AFE_FET_OFF);
			afe_driver_fet_control_u8(AFE_PDSG_FET, AFE_FET_OFF);
		}
		/**********************************************************************************************************************/
	}
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
