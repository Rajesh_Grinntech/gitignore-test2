/**
====================================================================================================================================================================================
@page bootloader_app_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bootloader_app.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	17-Jul-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */

/* Application Header File */
#include "storage_def.h"

#include "batt_param.h"

/* Configuration Header File */
#include "flash_drv.h"
#include "can_app.h"

/* This header file */
#include "bootloader_app.h"

/* Driver header */
#include "gt_memory.h"
#include "gt_timer.h"

#include "operating_system.h"
#include "gt_nvic.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

U8 bootloader_app_can_rx_local_buffer_au8[8] = { 0, };
U8 bootloader_app_uart_rx_local_buffer_au8[1152] = { 0, };
U8 boot_loader_spi_flash_data_au8[1024] = { 0, };
U8 bootloader_pgn_u8 = 0;
U8 bootloader_checksum_byte_au8[8] = { 0, };

U32 total_boot_loader_data_received_u32 = 0;


storage_task_update_boot_data_tst boot_loader_store_st;


/** @} */

/** @{
 *  Public Variable Definitions */
 U8 can_bootloader_cmd_received_gvlu8 = COM_HDR_FALSE;
volatile U8 uart_modem_bootloader_start_cmd_gu8 = COM_HDR_FALSE;
volatile U8 uart_ble_bootloader_start_cmd_gu8 = COM_HDR_FALSE;

U8 bootloader_app_can_tx_local_buffer_au8[8] = { 0, };
U8 read_boot_loader_buffer_au8[1024] = { 0, };

U32 boot_loader_data_rec_count_u32 = 0;
U32 data_reset_count_u32 = 0;
U32 boot_loader_config_flash_start_address_u32 = START_ADDRESS_BOOT_CONFIG_CHECK;
U32 start_boot_loader_write_address_u32 = START_ADDRESS_BOOTLOADER_LOG;
U32 start_read_boot_loader_address_u32 = START_ADDRESS_BOOTLOADER_LOG;

os_storage_queue_tst boot_storage_queue_st;
os_storage_queue_tst boot_uart_comm_queue_st;
flash_drv_blocks_erase_tst boot_loader_block_erase_st;
boot_loader_config_tst boot_loader_conf_gst,store_boot_rd_conf_st,store_boot_conf_st;
bootloader_app_sts_te boot_loader_status_e;

U8 btldr_data_channel_source_u8 = BOOTLOADER_APP_BTLDR_UPDATE_IDLE;
U8 bootloader_app_os_comm_event_type_gu8 = 0XFF;

/**** Boot_loader Section ****/
#if   (defined ( __CC_ARM ))
  __IO uint32_t VectorTable[48] __attribute__((at(0x20000000)));
#elif (defined (__ICCARM__))
#pragma location = 0x20000000
  __no_init __IO uint32_t VectorTable[48];
#elif defined   (  __GNUC__  )
  __IO uint32_t VectorTable[48] __attribute__((section(".RAMVectorTable")));
#elif defined ( __TASKING__ )
  __IO uint32_t VectorTable[48] __at(0x20000000);
#endif
/** @} */

/* Public Function Definitions */
U32 timer_check_u32 = 0;

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bootloader_app_can_rx_task_v(U8 pgn_aru8,U8 data_length_aru8,U8 *can_rx_data_arpu8)
{
	U8 i_u8 = 0;
	U8 mem_cpy_u8 = 5;

	/*BOOT_LOADER_APP_preferred_pgn_au8[0] = 0x85;
	 BOOT_LOADER_APP_preferred_pgn_au8[1] = 0x86;
	 BOOT_LOADER_APP_preferred_pgn_au8[2] = 0x87;*/

	memory_copy_u8_array_v(bootloader_app_can_rx_local_buffer_au8, can_rx_data_arpu8, data_length_aru8);

	switch(pgn_aru8)
	{
		/**********************************************************************************************************************/
		case BOOT_LOADER_ENABLE_ID:
		{

			if (bootloader_app_can_rx_local_buffer_au8[0] == BOOT_LOADER_ENABLE_CMD)
			{
				can_bootloader_cmd_received_gvlu8 = 1;
				nvic_disable_all_interrupts_v();
				nvic_enable_interrupt_v(CAN1_RX0_IRQn);
				nvic_enable_interrupt_v(TIM1_UP_TIM16_IRQn);
				total_boot_loader_data_received_u32 = 0;
				btldr_data_channel_source_u8 = BOOTLOADER_APP_CAN_COMM_CHANNEL;
				bootloader_app_os_comm_event_type_gu8 = OS_CAN_DEBUG_TX;

				boot_loader_conf_gst.new_firmware_size_u32 = ((U32) ((bootloader_app_can_rx_local_buffer_au8[1])))
										| ((U32) (bootloader_app_can_rx_local_buffer_au8[2]) << 8)
										| ((U32) (bootloader_app_can_rx_local_buffer_au8[3]) << 16)
										| ((U32) (bootloader_app_can_rx_local_buffer_au8[4]) << 24);
				//boot_firmware_size_u32 = boot_loader_conf_gst.new_firmware_size_u32;
				boot_loader_status_e = BOOT_LOADER_SUCCESS;
				boot_loader_block_erase_st.start_block_cnt_u16 = 1;
				boot_loader_block_erase_st.end_block_cnt_u16 = 4;
				//send to queue to storage task
				//boot_loader_data_erase_v();

				boot_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
				boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
				boot_storage_queue_st.payload_pv = &boot_loader_block_erase_st; /* dummy data must send */
				xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);
			}
		}
		break;
		/**********************************************************************************************************************/
		case BOOT_LOADER_DATA_ID:
		{
			memory_copy_u8_array_v(&boot_loader_spi_flash_data_au8[boot_loader_data_rec_count_u32],
					bootloader_app_can_rx_local_buffer_au8, data_length_aru8); /* length+1 required? */

			boot_loader_data_rec_count_u32 += data_length_aru8;
			total_boot_loader_data_received_u32 += data_length_aru8;

			if (boot_loader_data_rec_count_u32 >= 1024)
			{
				boot_loader_store_st.boot_data_e = STORAGE_BOOT_DATA;
				boot_loader_store_st.start_addr_u32 = start_boot_loader_write_address_u32;
				boot_loader_store_st.size_u32 = 1024;
				//boot_loader_store_st.boot_data_array_au8 = boot_loader_spi_flash_data_au8;

				boot_storage_queue_st.mode_e = OS_FLASH_MODE_WR;
				boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
				boot_storage_queue_st.payload_pv = &boot_loader_store_st;
				xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

				//send queue to storage task to write the flash
			}
		}
		break;
		/**********************************************************************************************************************/
		case BOOT_LOADER_EXIT_ID:
		{
			data_reset_count_u32 = 0;
			if (boot_loader_data_rec_count_u32 == 0)
			{
				//UPDATE CONFIG

				boot_loader_conf_gst.new_firmware_u8 = 0xAA;
				if (boot_loader_conf_gst.new_firmware_size_u32 == total_boot_loader_data_received_u32)
		  		{
					bootloader_pgn_u8 = BOOT_LOADER_ENABLE_PGN;

					boot_loader_conf_gst.channel_used_for_bootloader_data_u8 = BOOTLOADER_APP_CAN_COMM_CHANNEL;
					//send queue to update config

					boot_loader_store_st.boot_data_e = STORAGE_BOOT_CONFIG;
					boot_loader_store_st.start_addr_u32 = boot_loader_config_flash_start_address_u32;
					boot_loader_store_st.size_u32 = sizeof(boot_loader_conf_gst);
					//boot_loader_store_st.boot_data_array_au8 = &boot_loader_conf_gst;

					boot_storage_queue_st.mode_e = OS_FLASH_MODE_WR;
					boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
					boot_storage_queue_st.payload_pv = &boot_loader_store_st;
					xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

				}
				else
				{
					//ERASE CONFIG + BOOT DATA

					boot_loader_status_e = BOOT_LOADER_FAILURE;

					boot_loader_block_erase_st.start_block_cnt_u16 = 1;
					boot_loader_block_erase_st.end_block_cnt_u16 = 4;
									//send to queue to storage task
									//boot_loader_data_erase_v();

					boot_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
					boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
					boot_storage_queue_st.payload_pv = &boot_loader_block_erase_st; /* dummy data must send */
					xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

//					bootloader_pgn_u8 = BOOT_LOADER_ERROR_PGN;
//					bootloader_app_cmd_send_v(0x706,0xFE, 0xFF);
//					flashdriver_erase_flash_u8(CS25FL_ERASE_MODE_SECTOR, START_ADDRESS_BOOT_CONFIG_CHECK,
//							CS25FL_MAX_SECTOR_ERASE_TIME_MS);
//					boot_loader_data_erase_v();
//					gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 100);
//					NVIC_SystemReset();

				}
			}
			else
			{

				boot_loader_store_st.boot_data_e = STORAGE_BOOT_DATA;
				boot_loader_store_st.start_addr_u32 = start_boot_loader_write_address_u32;
				boot_loader_store_st.size_u32 = boot_loader_data_rec_count_u32;
				//boot_loader_store_st.boot_data_array_au8 = boot_loader_spi_flash_data_au8;

				boot_storage_queue_st.mode_e = OS_FLASH_MODE_WR;
				boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
				boot_storage_queue_st.payload_pv = &boot_loader_store_st;
				xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

			}
		}
		break;
		/**********************************************************************************************************************/
		case BOOT_LOADER_ERROR_ID:
		{
			boot_loader_status_e = BOOT_LOADER_FAILURE;

			can_bootloader_cmd_received_gvlu8 = 0; 
			nvic_enable_all_interrupts_v();

			btldr_data_channel_source_u8 = BOOTLOADER_APP_BTLDR_UPDATE_IDLE;
			//boot_loader_data_erase_v();
			start_boot_loader_write_address_u32 = START_ADDRESS_BOOTLOADER_LOG;
			start_read_boot_loader_address_u32 = START_ADDRESS_BOOTLOADER_LOG;
			boot_loader_conf_gst.new_firmware_u8 = 0;
			boot_loader_conf_gst.new_firmware_size_u32 = 0;
			boot_loader_data_rec_count_u32 = 0;
			total_boot_loader_data_received_u32 = 0;
			boot_loader_config_flash_start_address_u32 = START_ADDRESS_BOOT_CONFIG_CHECK;
			/* flash_drv_app_wr_data_u8((U8*) &boot_loader_conf_gst, &boot_loader_config_flash_start_address_u32,
					sizeof(boot_loader_config_tst)); */
			gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 100);

			boot_loader_block_erase_st.start_block_cnt_u16 = 1;
			boot_loader_block_erase_st.end_block_cnt_u16 = 4;
			//send to queue to storage task
			//boot_loader_data_erase_v();

			boot_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
			boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
			boot_storage_queue_st.payload_pv = &boot_loader_block_erase_st; /* dummy data must send */
			xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

			/* NVIC_SystemReset(); */
		}
		break;
		/**********************************************************************************************************************/
		default:
		{

		}
		break;
		/**********************************************************************************************************************/

	}
}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bootloader_app_can_tx_task_v(U32 id_aru32,U8 data_length_aru8,U8 *can_data_arpu8)
{
	//U8 mem_cpy_u8 = 5;
	memory_copy_u8_array_v(bootloader_app_can_tx_local_buffer_au8, can_data_arpu8,data_length_aru8);
		
	switch (id_aru32)
	{
		/*************************************************************************************************************************************************/
		case BOOT_LOADER_ENABLE_ACK:
			{
				gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 100);
				boot_loader_data_rec_count_u32 = 0;
				bootloader_pgn_u8 = BOOT_LOADER_ENABLE_PGN;
				 memory_set_u8_array_v(&boot_loader_spi_flash_data_au8[boot_loader_data_rec_count_u32], 0, 1024); /* length+1 required? */
				memory_set_u8_array_v(&read_boot_loader_buffer_au8[data_reset_count_u32], 0, 1024);
				bootloader_app_cmd_send_v(id_aru32,1,bootloader_app_can_tx_local_buffer_au8[0]);

			}break;
		/*************************************************************************************************************************************************/
			case BOOT_LOADER_DATA_ACK:
			{
			//mem_cpy_u8 = memory_compare_u8_array_u8(boot_loader_spi_flash_data_au8, read_boot_loader_buffer_au8, 1024);
			//crc_check_sum_u16 = boot_loader_app_crc_16_generate_u16(&bootloader_app_can_tx_local_buffer_au8[0], 1024);
			//memory_copy_u8_array_v((U8*) &bootloader_checksum_byte_au8[0], (U8*) &crc_check_sum_u16, 2);

			memory_copy_u8_array_v((U8*) &bootloader_checksum_byte_au8[0], (U8*) &bootloader_app_can_tx_local_buffer_au8, 2);
			bootloader_pgn_u8 = BOOT_LOADER_DATA_PGN;
			gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 300);
			bootloader_app_checksum_msg_v(id_aru32, bootloader_checksum_byte_au8, data_length_aru8);
			boot_loader_data_rec_count_u32 = 0;
			data_reset_count_u32 = 0;
			}
			break;
		/*************************************************************************************************************************************************/
			case BOOT_LOADER_EXIT_ACK:
			{

			if (boot_loader_data_rec_count_u32 == 0)
			{

				bootloader_app_cmd_send_v(id_aru32,bootloader_app_can_tx_local_buffer_au8[0],bootloader_app_can_tx_local_buffer_au8[1]);
				gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 100);

				NVIC_SystemReset();
			}
			else
			{
				memory_copy_u8_array_v((U8*) &bootloader_checksum_byte_au8[0], (U8*) &bootloader_app_can_tx_local_buffer_au8, 2);
				bootloader_app_checksum_msg_v(BOOT_LOADER_EXIT_ACK, bootloader_checksum_byte_au8, 2);
				boot_loader_data_rec_count_u32 = 0;

				if (boot_loader_conf_gst.new_firmware_size_u32 == total_boot_loader_data_received_u32)
				{
					boot_loader_conf_gst.new_firmware_u8 = 0xAA;

					boot_loader_config_flash_start_address_u32 = START_ADDRESS_BOOT_CONFIG_CHECK;

					//UPDATE CONFIG
					//send to queue

					boot_loader_store_st.boot_data_e = STORAGE_BOOT_CONFIG;
					boot_loader_store_st.start_addr_u32 = boot_loader_config_flash_start_address_u32;
					boot_loader_store_st.size_u32 = sizeof(boot_loader_conf_gst);
					//boot_loader_store_st.boot_data_array_au8 = &boot_loader_conf_gst;

					boot_storage_queue_st.mode_e = OS_FLASH_MODE_WR;
					boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
					boot_storage_queue_st.payload_pv = &boot_loader_store_st;
					xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

				}
				else
				{

					boot_loader_status_e = BOOT_LOADER_FAILURE;
					bootloader_pgn_u8 = BOOT_LOADER_ERROR_PGN;


					boot_loader_block_erase_st.start_block_cnt_u16 = 1;
					boot_loader_block_erase_st.end_block_cnt_u16 = 3;
									//send to queue to storage task
									//boot_loader_data_erase_v();

					boot_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
					boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
					boot_storage_queue_st.payload_pv = &boot_loader_block_erase_st; /* dummy data must send */
					xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);

					//bootloader_app_cmd_send_v(0x706, 0xFE, 0xFF);

					//DELETE CONFIG + boot data
					//					flashdriver_erase_flash_u8(CS25FL_ERASE_MODE_SECTOR, START_ADDRESS_BOOT_CONFIG_CHECK,
					//					CS25FL_MAX_SECTOR_ERASE_TIME_MS);
					//					boot_loader_data_erase_v();

				}

			}

		}break;
		/*************************************************************************************************************************************************/
	    case BOOT_LOADER_BMS_VERSION_NO_GET_ID:
	    {
	       //gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 100);
	       boot_loader_data_rec_count_u32 = 0;
	       bootloader_pgn_u8 = BOOT_LOADER_BMS_VERSION_NO_GET_ID;
	       memory_set_u8_array_v(&boot_loader_spi_flash_data_au8[boot_loader_data_rec_count_u32], 0, 1024); /* length+1 required? */
	       memory_set_u8_array_v(&read_boot_loader_buffer_au8[data_reset_count_u32], 0, 1024);
	       memory_set_u8_array_v(bootloader_app_can_tx_local_buffer_au8,0,8);
	       memory_copy_u8_array_v(bootloader_app_can_tx_local_buffer_au8, (U8*)&batt_param_batt_pack_details_gst, 2);
	       //bootloader_app_cmd_send_v(id_aru32,2,bootloader_app_can_tx_local_buffer_au8[0]);
	       //memory_copy_u8_array_v(bootloader_app_can_tx_local_buffer_au8, bms_software_version_no_u32, 2);
	       bootloader_app_checksum_msg_v(id_aru32,&bootloader_app_can_tx_local_buffer_au8[0],8);
	    }
	    break;
		default:
		{

		}
		break;
		/*************************************************************************************************************************************************/
	}
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/

U16 bootloader_app_crc16_generate_u16(U8 *data_arpu8, U16 length_aru16)
{
	U8 i_u8;
	U32 data_u32;
	U32 crc_u32 = 0xffff;

	if (length_aru16 == 0)
	{
		return (~crc_u32);
	}

	do
	{
		for (i_u8 = 0, data_u32 = (U32) 0xff & *data_arpu8++; i_u8 < 8; i_u8++, data_u32 >>= 1)
		{
			if ((crc_u32 & 0x0001) ^ (data_u32 & 0x0001))
			{
				crc_u32 = (crc_u32 >> 1) ^ POLY;
			}
			else
			{
				crc_u32 >>= 1;
			}
		}
	} while (--length_aru16);

	crc_u32 = ~crc_u32;
	data_u32 = crc_u32;
	crc_u32 = (crc_u32 << 8) | (data_u32 >> 8 & 0xff);

	return ((U16)crc_u32);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
#if 0
void bootloader_app_checksum_msg_v(U32 can_std_id_aru32,U8 *data_buf_arpu8, U8 data_cnt_aru8)
{
	bootloader_pgn_u8 = BOOT_LOADER_DATA_PGN;
	can_message_t boot_loader_check_sum_msg_st;
	if(COM_HDR_ENABLED == uart_modem_bootloader_start_cmd_gu8)
	{
		modem_comm_uart_msg_st.cmd_id_e = can_std_id_aru32;

		os_comm_uart_queue_st.event_e = OS_UART_2_TX;
		os_comm_uart_queue_st.payload_pv = (modem_comm_uart_msg_tst*) &modem_comm_uart_msg_st;
		xQueueSend(os_uart_comm_t10_qhandler_ge, &os_comm_uart_queue_st, COM_HDR_NULL);

	}
	else
	{
		boot_loader_check_sum_msg_st.id  = can_std_id_aru32;
		boot_loader_check_sum_msg_st.length = data_cnt_aru8;
		memory_copy_u8_array_v((U8*)boot_loader_check_sum_msg_st.data, data_buf_arpu8, data_cnt_aru8);

		storage_tx_can_comm_queue_st.event_e = OS_CAN_0_DEBUG_TX;
		storage_tx_can_comm_queue_st.payload_pv = &boot_loader_check_sum_msg_st;
		xQueueSend(os_can_comm_t9_qhandler_ge, &storage_tx_can_comm_queue_st, 2);

		/* Commented because of CAN data sending in queue */
		/* can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_NB_IT, &boot_loader_check_sum_msg_st); */
	}


}
#endif
void bootloader_app_checksum_msg_v(U32 can_std_id_aru32,U8 *data_buf_arpu8, U8 data_cnt_aru8)
{
	bootloader_pgn_u8 = can_std_id_aru32;
	can_app_message_tst boot_loader_check_sum_msg_st;
	boot_loader_check_sum_msg_st.can_id_u32 = BMS_BOOTLOADER_MESSAGE_ID;
	boot_loader_check_sum_msg_st.length_u8 = data_cnt_aru8;
	memory_copy_u8_array_v((U8*)boot_loader_check_sum_msg_st.data_au8, data_buf_arpu8, data_cnt_aru8);

	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_NB_IT, &boot_loader_check_sum_msg_st);
}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bootloader_app_cmd_send_v(U32 can_std_id_aru32, U8 command_aru8, U8 command_status_aru8)
{
	bootloader_pgn_u8 = BOOT_LOADER_ENABLE_PGN;
	can_app_message_tst boot_loader_cmd_msg_st;
	bootloader_pgn_u8 = can_std_id_aru32;
	boot_loader_cmd_msg_st.can_id_u32 = BMS_BOOTLOADER_MESSAGE_ID;
	boot_loader_cmd_msg_st.length_u8 = 8;
	boot_loader_cmd_msg_st.data_au8[0] = 0x51;
	boot_loader_cmd_msg_st.data_au8[1] = command_aru8;
	boot_loader_cmd_msg_st.data_au8[2] = command_status_aru8;
	boot_loader_cmd_msg_st.data_au8[3] = 0;
	boot_loader_cmd_msg_st.data_au8[4] = 0;
	boot_loader_cmd_msg_st.data_au8[5] = 0;
	boot_loader_cmd_msg_st.data_au8[6] = 0;
	boot_loader_cmd_msg_st.data_au8[7] = 0;

	can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_NB_IT, &boot_loader_cmd_msg_st);

}

void boot_loader_app_vector_remap_v(void)
{
	uint32_t ui32_VectorIndex = 0;
	for(ui32_VectorIndex = 0; ui32_VectorIndex < 48; ui32_VectorIndex++)
	{
		VectorTable[ui32_VectorIndex] = *(volatile  uint32_t*)((uint32_t)APPLICATION_ADDRESS_1 + (ui32_VectorIndex << 2));
	}
	__HAL_RCC_SYSCFG_CLK_ENABLE(); //for enable sys_config
	__HAL_SYSCFG_REMAPMEMORY_SRAM();
	SCB->VTOR = APPLICATION_ADDRESS_1;
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
