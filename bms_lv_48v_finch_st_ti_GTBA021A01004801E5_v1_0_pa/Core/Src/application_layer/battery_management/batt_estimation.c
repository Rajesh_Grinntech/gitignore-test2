/**
====================================================================================================================================================================================
@page batt_estimation_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	batt_estimation.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	26-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */

#include "common_header.h"

/* Application Header File */
#include "bms_config.h"
#include "meas_app.h"
#include "flash_drv.h"
#include "batt_param.h"
#include "batt_estimation.h"

/* Driver Header File */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
void batt_estimation_instantaneous_power_v(void);
void	batt_estimation_available_energy_v(void);
void	batt_estimation_state_of_charge_v(void);
void	batt_estimation_capacity_amphour_v(void);
void	batt_estimation_charge_cycles_v(void);
/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
batt_estimation_general_param_tst batt_estimation_general_param_st;
batt_estimation_flag_logs_tst batt_estimation_flag_logs_st;
/** @} */


/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void batt_estimation_init_parameters_v(void)
{
	batt_estimation_general_param_st.current_capacity_inital_estimated_b = COM_HDR_FALSE;
	batt_estimation_general_param_st.ocv_min_soc_1mV_u16 = 3000;
	batt_estimation_general_param_st.ocv_max_soc_1mV_u16 = 3650;
	batt_estimation_general_param_st.voltage_to_AHr_convertion_factor_f = (SFP) bms_config_nv_cell_cfg_gst.series_cell_capacity_1cAHr_u16 / (SFP) (batt_estimation_general_param_st.ocv_max_soc_1mV_u16 - batt_estimation_general_param_st.ocv_min_soc_1mV_u16);
	batt_estimation_general_param_st.voltage_to_AHr_convertion_factor_f *= COM_HDR_SCALE_10;
	/* Calculation depends on the rated capacity and operating range of the battery */
	/* capacity / ocv_max_soc_1mV_u16 - ocv_min_soc_1mV_u16 */
	batt_estimation_general_param_st.capacity_to_soc_f = ((SFP)100 / (SFP)bms_config_nv_cell_cfg_gst.series_cell_capacity_1cAHr_u16) * (SFP)100;
	batt_estimation_general_param_st.min_charge_capacity_capacity_u16 = 550; /* 80% of (7.6Ah - 10%)*/
	batt_estimation_general_param_st.degradation_health_per_cycle_u16 = 16;
}

void batt_estimation_estimate_all_parameters_v(void)
{
	batt_estimation_instantaneous_power_v();
	//batt_estimation_available_energy_v();
	//batt_estimation_state_of_charge_v();
	//batt_estimation_capacity_amphour_v();
	batt_estimation_charge_cycles_v();
}

void batt_estimation_instantaneous_power_v(void)
{
	/* Compute and update the Inst. Power */
	batt_param_pack_param_gst.pack_param_inst_power_1W_s16 = (S16)(((S32)batt_param_pack_param_gst.pack_param_1cV_u16 *
																		batt_param_pack_param_gst.pack_param_1mA_s32) / (S32)COM_HDR_SCALE_100000);
}

U8 BattEst_Counter_u8 = 0;
void batt_estimation_capacity_amphour_v(void)
{

	if (batt_estimation_general_param_st.current_capacity_inital_estimated_b == COM_HDR_FALSE)
	{
		SFP XFactor = 0;

		/* mV to V -- voltage difference from the base SOC_0_OCV */
		XFactor = (SFP) (batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 - batt_estimation_general_param_st.ocv_min_soc_1mV_u16) / (SFP) 1000;

		if(XFactor <= 0)
		{
			XFactor = 0;
		}

		/* SOC_0_OCV to SOC_100_OCV difference is mapped to the capacity of the cell */
		batt_param_pack_param_gst.pack_param_capacity_1cAhr_f =(U16)(XFactor * batt_estimation_general_param_st.voltage_to_AHr_convertion_factor_f * (SFP)100);
		BattEst_Counter_u8++;
		if(BattEst_Counter_u8 > 4)
		{
			batt_estimation_general_param_st.current_capacity_inital_estimated_b = COM_HDR_TRUE;
		}
	}
	else
	{

		SFP delta_cc_f = (SFP)meas_app_cc_soc_gst.cum_capacity_1mAmSec_s32 / (SFP)(10 * 1000 * 3600);
		batt_param_pack_param_gst.pack_param_capacity_1cAhr_f += delta_cc_f; // mA->cA * mSec->Sec * Sec->Hour

		if(delta_cc_f > 0)
		{
			batt_param_pack_param_gst.pack_param_cycle_complete_cap_1cAhr_f  += delta_cc_f; // mA->cA * mSec->Sec * Sec->Hour
		}
	}

	/* Re calibration of AH based on OCV value*/
	if (batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 >= batt_estimation_general_param_st.ocv_max_soc_1mV_u16)
	{
		batt_param_pack_param_gst.pack_param_capacity_1cAhr_f = bms_config_nv_cell_cfg_gst.series_cell_capacity_1cAHr_u16;
	}
	else if (batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 <= batt_estimation_general_param_st.ocv_min_soc_1mV_u16)
	{
		batt_param_pack_param_gst.pack_param_capacity_1cAhr_f = 0;
	}

	/* Avoid violation of Ah limits */
	if (batt_param_pack_param_gst.pack_param_capacity_1cAhr_f >= bms_config_nv_cell_cfg_gst.series_cell_capacity_1cAHr_u16)
	{
		batt_param_pack_param_gst.pack_param_capacity_1cAhr_f = bms_config_nv_cell_cfg_gst.series_cell_capacity_1cAHr_u16;
	}
	else if (batt_param_pack_param_gst.pack_param_capacity_1cAhr_f <= 0)
	{
		batt_param_pack_param_gst.pack_param_capacity_1cAhr_f = 0;
	}
}

void batt_estimation_available_energy_v(void)
{

	batt_param_pack_param_gst.pack_param_energy_1cWh_u16 = (U16)((SFP)batt_param_pack_param_gst.pack_param_1cV_u16 *
																	batt_param_pack_param_gst.pack_param_capacity_1cAhr_f) / (U16)100;
}

void batt_estimation_state_of_charge_v(void)
{
	batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = (U16) (batt_param_pack_param_gst.pack_param_capacity_1cAhr_f
			* batt_estimation_general_param_st.capacity_to_soc_f);

	if (batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 >= COM_HDR_SCALE_10000)
	{
		batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 =  COM_HDR_SCALE_10000;
	}
	else if (batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 <= COM_HDR_ZERO_U16)
	{
		batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = COM_HDR_ZERO_U16;
	}
}

void batt_estimation_charge_cycles_v(void)
{
	if((U16)batt_param_pack_param_gst.pack_param_cycle_complete_cap_1cAhr_f > batt_estimation_general_param_st.min_charge_capacity_capacity_u16)
	{
		batt_param_pack_param_gst.pack_param_cycle_complete_cap_1cAhr_f = 0;
		batt_param_pack_param_gst.pack_param_cycle_count_u32 += 1;
		batt_param_pack_param_gst.pack_param_soh_1mPC_u32 -= batt_estimation_general_param_st.degradation_health_per_cycle_u16;

		if(batt_param_pack_param_gst.pack_param_soh_1mPC_u32 >= COM_HDR_SCALE_100000)
		{
			batt_param_pack_param_gst.pack_param_soh_1mPC_u32 = COM_HDR_SCALE_100000;
		}
		else if(batt_param_pack_param_gst.pack_param_soh_1mPC_u32 <= 0)
		{
			batt_param_pack_param_gst.pack_param_soh_1mPC_u32 = 0;
		}

		batt_estimation_flag_logs_st.data_available_b = COM_HDR_TRUE;
		batt_estimation_flag_logs_st.cycle_count_u16 = batt_param_pack_param_gst.pack_param_cycle_count_u32;
		batt_estimation_flag_logs_st.soh_1mPC_u32 = batt_param_pack_param_gst.pack_param_soh_1mPC_u32;
		/* Log the SOH and Cycle data */
		U32 flash_addr_u32 = BATT_EST_FLASH_LOG_ADDR; //TODO check after adding flash files
		//ucFlashDriver_Erase_Flash(ERASE_MODE_SECTOR, flash_addr_u32);
		//ucFlash_Driver_App_Write_Data((U8*) &batt_estimation_flag_logs_st, &flash_addr_u32, sizeof(batt_estimation_flag_logs_st));
		flash_addr_u32 = BATT_EST_FLASH_LOG_ADDR;
		//ucFlash_Driver_App_Read_Data((U8*) &batt_estimation_flag_logs_st, flash_addr_u32, sizeof(batt_estimation_flag_logs_st));
	}
}

void vBattEstimation_Recover_Battery_Estimation(void)
{
#if 0
	U32 flash_addr_u32 = BATT_EST_FLASH_LOG_ADDR;
	ucFlash_Driver_App_Read_Data((U8*) &batt_estimation_flag_logs_st, flash_addr_u32, sizeof(batt_estimation_flag_logs_st));

	if (batt_estimation_flag_logs_st.data_available_b == TRUE)
	{
		if (batt_estimation_flag_logs_st.cycle_count_u16 != MAX_U16 && batt_estimation_flag_logs_st.soh_1mPC_u32 != MAX_U32)
		{
			batt_param_pack_param_gst.pack_param_soh_1mPC_u32 = batt_estimation_flag_logs_st.soh_1mPC_u32;
			batt_param_pack_param_gst.pack_param_cycle_count_u32 = batt_estimation_flag_logs_st.cycle_count_u16;
		}
	}
#endif
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
