/**
====================================================================================================================================================================================
@page storage_task_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	storage_task.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	KRISH					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	19-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */

/* Application Header File */
#include "common_var.h"
#include "storage_def.h"
#include "debug_comm.h"
#include "bootloader_app.h"

#include "batt_param.h"
/* Configuration Header File */
#include "bootsm.h"
#include "operating_system.h"
#include "flash_drv.h"
#include "can_app.h"
#include "low_pwr_app.h"

#include "gt_timer.h"
#include "gt_rtc.h"
/* This header file */
#include "storage_task.h"

/* Driver header */
#include "gt_memory.h"

/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */


/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

storage_task_update_boot_data_tst storage_task_boot_data_st;
U8 storage_task_update_batt_data_au8[30];
U8 storage_task_update_batt_data_au8[30];
U8 storage_task_spi_flash_wr_data_au8[1024] = { 0, };
U8 storage_task_spi_flash_rd_data_au8[1024] = { 0, };
can_app_message_tst storage_task_can_msg_st;
operating_system_can_comm_queue_tst storage_tx_can_comm_queue_st;
can_app_message_tst storage_comm_tx_msg_st;

/** @} */

/** @{
 *  Public Variable Definitions */
U8 dummy_data_u8 = 0XAA;
//U8 one_sec_str_flg = 0;
storage_task_update_batt_data_tst storage_task_update_batt_data_st;
storage_task_update_batt_data_tst storage_task_flsh_ret_update_batt_data_st;

storage_task_update_batt_data_tst storage_task_update_batt_engy_data_st;
/** @} */

/* Public Function Definitions */

#if 0
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8  storage_task_start_addr_init_u8()
{
	U8 ret_val_u8 = 0;
	U8 flash_addr_find;
	flash_addr_find = true;

	//flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_FULL_CHIP,start_address_BMS_1Second_Log,10000);

   while(flash_addr_find)
   {
	   if(start_address_BMS_1Second_Log < STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG)
	   {
		   ret_val_u8 = storage_def_flash_read_data_u8(start_address_BMS_1Second_Log,STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG,
				   	   	   	   	   	   	 (U8*)&rd_debug_battery_data_log_gst, sizeof(bms_debug_battery_data_log_tst));

		   if(ret_val_u8 != 0)
		   {
			   return ret_val_u8;
		   }
		//   start_address_BMS_1Second_Log += sizeof(bms_debug_battery_data_log_tst);
#if 1
		   if(rd_debug_battery_data_log_gst.time_stamp_u32 == STORAGE_DEF_FLASH_DEFAULT_VALUE)
		   {
			   flash_addr_find = false;
		   }
		   else
		   {
			   start_address_BMS_1Second_Log += sizeof(bms_debug_battery_data_log_tst);
		   }
#endif
		   memory_set_u8_array_v((U8*)&rd_debug_battery_data_log_gst,0,sizeof(bms_debug_battery_data_log_tst));
	   }
	   else
	   {
		   flash_addr_find=false;
		  /* ret_val_u8++;
		   return ret_val_u8;*/
	   }
   }
   return ret_val_u8;
}
#endif
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void storage_task_t8_v(void *parameter)
{
	U8 ret_val_u8;
#if 0
	bms_battery_data_log_tst battery_data_log_local_st;
#endif
	/* Unused warning removal */
	COM_HDR_UNUSED(parameter);

	/* Wait Until the OS is completely up */
	while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
	{
		vTaskDelay(10);
	}

	while (1)
	{
		lpapp_task_states_agu8[TASK_STORAGE] = COM_HDR_TRUE;
		if(xQueueReceive(os_storage_t7_qhandler_ge, &os_storage_queue_st, portMAX_DELAY) == pdTRUE)
		{
			lpapp_task_states_agu8[TASK_STORAGE] = COM_HDR_FALSE;
#if 0
			if(os_storage_queue_st.event_e == OS_STORAGE_ONE_SEC_DATA)
			{
				switch(os_storage_queue_st.mode_e)
				{

					/**********************************************************************************************************************/
					case OS_FLASH_MODE_WR:
					{
/*						storage_comm_tx_msg_st.id = 0x100;
						storage_comm_tx_msg_st.length = 1;
						storage_comm_tx_msg_st.data[0] = 55;
						storage_comm_tx_msg_st.data[0] = 55;
						can_app_transmit_data_u8(CAN_APP_DISO_INST_0, CAN_TX_MAILBOX0, CAN_TX_BLOCKING, &storage_comm_tx_msg_st);*/
						flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;

						rtc_get_date_time_u8();
						batt_param_update_batt_details_v();
						//battery_data_log_local_st = *(bms_battery_data_log_tst*)os_storage_queue_st.payload_pv;

						/* flash_write(get one second data from queue) */
//						flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_FULL_CHIP, 0x00, 20000);
						//flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR,start_address_BMS_1Second_Log,2000);
						//gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 100);
						storage_def_flash_store_data_u8(&start_address_BMS_1Second_Log, STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG,
														(U8*)&bms_debug_battery_data_log_gst, sizeof(bms_debug_battery_data_log_tst));

						//rd_addr_bms_1sec_log_u32 = start_address_BMS_1Second_Log - sizeof(bms_debug_battery_data_log_tst);

						//TIMER
						//storage_def_flash_read_data_u8(rd_addr_bms_1sec_log_u32,STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG,
							//							(U8*)&rd_debug_battery_data_log_gst, sizeof(bms_debug_battery_data_log_tst));

						os_storage_queue_st.mode_e = OS_FLASH_WRITE_READ_BACK;
						os_storage_queue_st.event_e = OS_STORAGE_ONE_SEC_DATA;
						//os_storage_queue_st.payload_pv = (bms_battery_data_log_tst *) &battery_data_log_local_st;
						os_storage_queue_st.payload_pv = (U8 *)&dummy_data_u8;
						xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,2);
					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_WRITE_READ_BACK:
					{
						//battery_data_log_local_st = *(bms_battery_data_log_tst*)os_storage_queue_st.payload_pv;
						memory_set_u8_array_v((U8*)&rd_debug_battery_data_log_gst,0,sizeof(bms_debug_battery_data_log_tst));
						/* To read the previous stored data */
						rd_addr_bms_1sec_log_u32 = start_address_BMS_1Second_Log - sizeof(bms_debug_battery_data_log_tst);
#if 0
						U32 strt_addr = 0x88;
						U32 end_addr = 0x90;
						U8 data[10] = {12,34,56,78,90,12,34,56,78};
						U8 data_rx[10];
						storage_def_flash_store_data_u8(&strt_addr, end_addr,data, sizeof(data));
						strt_addr = 0x88;
						end_addr = 0x90;
						storage_def_flash_read_data_u8(strt_addr,end_addr,data_rx, sizeof(data_rx));
#endif
						storage_def_flash_read_data_u8(rd_addr_bms_1sec_log_u32,STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG,
																	(U8*)&rd_debug_battery_data_log_gst, sizeof(bms_debug_battery_data_log_tst));

						//if( memory_comp_u8((U8*)&rd_bms_battery_data_log_gst, (U8*)&battery_data_log_local_st, sizeof(bms_battery_data_log_tst)))
//						 Comparison handled here
						if( memory_comp_u8((U8*)&rd_debug_battery_data_log_gst, (U8*)&bms_debug_battery_data_log_gst, sizeof(bms_debug_battery_data_log_tst)))
						{
							flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
						}
						else
						{
							flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
						}
						//flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
						//flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;

					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_RD:
					{
						rd_addr_bms_1sec_log_u32 = *(U32*)os_storage_queue_st.payload_pv;

						storage_def_flash_read_data_u8(rd_addr_bms_1sec_log_u32,STORAGE_DEF_END_ADDRESS_BMS_1SEC_LOG,
																		(U8*)&rd_bms_battery_data_log_gst, sizeof(bms_battery_data_log_tst));

					}
					break;
					/**********************************************************************************************************************/
					default :
					{

					}
					break;
					/**********************************************************************************************************************/
				}
			}
			else if(os_storage_queue_st.event_e == OS_STORAGE_DATA_RETRIVAL)
			{
				switch (os_storage_queue_st.mode_e)
				{
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_RD:
					{
/*						os_storage_queue_st.payload_pv = (U8*)os_storage_queue_st.payload_pv + 4;
						storage_task_update_batt_data_st.end_addr_u32 = *((U32*)os_storage_queue_st.payload_pv);*/

						storage_def_flash_read_data_u8(storage_task_flsh_ret_update_batt_data_st.start_addr_u32,
														storage_task_flsh_ret_update_batt_data_st.end_addr_u32,
														(U8*)&flsh_ret_debug_battery_data_log_gst, sizeof(bms_debug_battery_data_log_tst));

						//flsh_ret_debug_battery_data_log_gst.time_stamp_u32 = 0x87;
					}break;
					/**********************************************************************************************************************/
					default:
					{

					}
					break;
					/**********************************************************************************************************************/
				}
			}
#endif
			/*else*/ if (os_storage_queue_st.event_e == OS_STORAGE_UPDATE_BATT_DATA)
			{
				switch (os_storage_queue_st.mode_e)
				{
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_WR:
					{
						storage_def_flash_store_data_u8(&storage_task_update_batt_data_st.start_addr_u32,
								storage_task_update_batt_data_st.end_addr_u32,
								(U8*) storage_task_update_batt_data_st.bms_batt_upd_log_st,
								storage_task_update_batt_data_st.size_u32);

						/* Read data from queue which is previously written in flash */
						storage_task_update_batt_data_st.start_addr_u32 -= storage_task_update_batt_data_st.size_u32;
						os_storage_queue_st.mode_e = OS_FLASH_WRITE_READ_BACK;
						os_storage_queue_st.event_e = OS_STORAGE_UPDATE_BATT_DATA;
						os_storage_queue_st.payload_pv = (storage_task_update_batt_data_tst*) &storage_task_update_batt_data_st;
						xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);

					}
					break;
						/**********************************************************************************************************************/
					case OS_FLASH_RD_AND_SECTOR_ERASE:
					{
						flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;

						storage_task_update_batt_data_st.start_addr_u32 = *((U32*) os_storage_queue_st.payload_pv);
						storage_task_update_batt_data_st.end_addr_u32 = *((U32*) os_storage_queue_st.payload_pv + 1);
						storage_task_update_batt_data_st.size_u32 = *((U32*) os_storage_queue_st.payload_pv + 2);
						storage_task_update_batt_data_st.bms_batt_upd_log_st =  (void*)(*((U32*)os_storage_queue_st.payload_pv + 3));

						storage_def_flash_read_data_u8(storage_task_update_batt_data_st.start_addr_u32,
								storage_task_update_batt_data_st.end_addr_u32, (U8*) &storage_task_update_batt_data_au8[0],
								storage_task_update_batt_data_st.size_u32);

						/* TODO: Update the read & incrementing the necessary variables here */

						/* Send sector erase command */
						flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR,
								(storage_task_update_batt_data_st.start_addr_u32 / 4096),
								MAX_SECTOR_ERASE_TIME_MS);

						os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
						os_storage_queue_st.event_e = OS_STORAGE_UPDATE_BATT_DATA;
						os_storage_queue_st.payload_pv = (storage_task_update_batt_data_tst*) &storage_task_update_batt_data_st;
						xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);
					}
					break;
						/**********************************************************************************************************************/
					case OS_FLASH_WRITE_READ_BACK:
					{

						//rd_addr_batt_1_timedata_log_u32 = storage_task_update_batt_data_st.start_addr_u32 - storage_task_update_batt_data_st.size_u16;

						//flash_drv_addr_data_st.bms_batt_upd_log_st = *((bms_drive_onetime_log_tst*)os_storage_queue_st.payload_pv + 1);

						/* flash read data */
						storage_def_flash_read_data_u8(storage_task_update_batt_data_st.start_addr_u32,
								storage_task_update_batt_data_st.end_addr_u32, (U8*) &storage_task_update_batt_data_au8[0],
								storage_task_update_batt_data_st.size_u32);

						/* Comparison handled here */
						if (memory_comp_u8((U8*) &storage_task_update_batt_data_au8,
								(U8*) storage_task_update_batt_data_st.bms_batt_upd_log_st, sizeof(bms_drive_onetime_log_tst)))
						{
							flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
						}
						else
						{
							flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
						}

					}
					break;
						/**********************************************************************************************************************/
					case OS_FLASH_MODE_STATUS_CHECK:
					{
						ret_val_u8 = flash_drv_wait_for_wr_end_no_blocking_u8();
						if (((flash_drv_status_reg_1_u8 & 0x01) == 0x00))
						{
							os_storage_queue_st.mode_e = OS_FLASH_MODE_WR;
							os_storage_queue_st.event_e = OS_STORAGE_UPDATE_BATT_DATA;
							os_storage_queue_st.payload_pv =
									(storage_task_update_batt_data_tst*) &storage_task_update_batt_data_st;
							xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);
						}
						else
						{
							os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							os_storage_queue_st.event_e = OS_STORAGE_UPDATE_BATT_DATA;
							os_storage_queue_st.payload_pv = &dummy_data_u8; /* dummy data must send */
							xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st, 2);
						}
					}
					break;
						/**********************************************************************************************************************/
					default:
					{

					}
					break;
						/**********************************************************************************************************************/
				}
			}

			 else  if (os_storage_queue_st.event_e == OS_STORAGE_BOOTLOADER_UPDATE)
			{
				switch (os_storage_queue_st.mode_e)
				{
					/**********************************************************************************************************************/
					case OS_FLASH_ERASE_SECTOR:
					{
						flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;

						memory_copy_u8_array_v((U8*)&flash_drv_blocks_erase_st, os_storage_queue_st.payload_pv,
																				sizeof(flash_drv_blocks_erase_st));

						/* sector erase command from queue and send flash erase command */
//						flash_drv_blocks_erase_st.start_block_cnt_u16 = *((U32*) os_storage_queue_st.payload_pv);
//						flash_drv_blocks_erase_st.end_block_cnt_u16 = *((U32*) os_storage_queue_st.payload_pv + 1);
						storage_task_boot_data_st.start_addr_u32 =    START_ADDRESS_BOOT_CONFIG_CHECK;
						//(flash_drv_blocks_erase_st.start_block_cnt_u16 * 16 * 4096);

						if (flash_drv_blocks_erase_st.start_block_cnt_u16 <= flash_drv_blocks_erase_st.end_block_cnt_u16)
						{
							flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_BLOCK, (flash_drv_blocks_erase_st.start_block_cnt_u16 * 16 * 4096),
																													MAX_BLOCK_ERASE_TIME_MS);

							gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 300);
							boot_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
							boot_storage_queue_st.payload_pv = &flash_drv_blocks_erase_st;
							xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);
						}
					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_STATUS_CHECK:
					{
						ret_val_u8 = flash_drv_wait_for_wr_end_no_blocking_u8();
						if (((flash_drv_status_reg_1_u8 & 0x01) == 0x00))
						{
							flash_drv_blocks_erase_st.start_block_cnt_u16++;
							if (flash_drv_blocks_erase_st.start_block_cnt_u16 <= flash_drv_blocks_erase_st.end_block_cnt_u16)
							{
								boot_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
								boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
								boot_storage_queue_st.payload_pv = &flash_drv_blocks_erase_st;
								xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);
							}
							else
							{
								flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;
								boot_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR_CHECK;
								boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
								boot_storage_queue_st.payload_pv = &dummy_data_u8; /* dummy data must send */
								xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);
							}
						}
						else
						{
							boot_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
							boot_storage_queue_st.payload_pv = &dummy_data_u8; /* dummy data must send */
							xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);
						}
					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_ERASE_SECTOR_CHECK:
					{

						start_read_boot_loader_address_u32 = 0x2F900;
						flash_drv_app_rd_data_u8(storage_task_spi_flash_rd_data_au8,start_read_boot_loader_address_u32,
																							FLASH_DRV_FLASH_PAGE_SIZE);

						start_read_boot_loader_address_u32 = START_ADDRESS_BOOTLOADER_LOG;
						//gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND, 500);

						if ((storage_task_spi_flash_rd_data_au8[0] != 0xFF) || (storage_task_spi_flash_rd_data_au8[1] != 0xFF)
								|| (storage_task_spi_flash_rd_data_au8[2] != 0xFF))
						{
							flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;

						//	if(COM_HDR_ENABLED == uart_modem_bootloader_start_cmd_gu8)
						//	{
						//		modem_comm_uart_msg_st.data_au8[0] = 0xAA;
						//		modem_comm_uart_msg_st.length_u16 = 1;
						//	}
						//	else if(COM_HDR_ENABLED == can_bootloader_cmd_received_gvlu8)
							{
								storage_task_can_msg_st.data_au8[0] = 0xAA;
							}
						//	else if(COM_HDR_ENABLED == uart_ble_bootloader_start_cmd_gu8)
						//	{
						//		ble_comm_uart_msg_st.data_au8[0] = 0xAA;
						//		ble_comm_uart_msg_st.length_u16 = 1;
						//	}
						}
						else
						{
						//	if(COM_HDR_ENABLED == uart_modem_bootloader_start_cmd_gu8)
						//	{
						//		modem_comm_uart_msg_st.data_au8[0] = 0xAA;
						//		modem_comm_uart_msg_st.length_u16 = 1;
						//	}
						//	else if(COM_HDR_ENABLED == can_bootloader_cmd_received_gvlu8)
							{
								storage_task_can_msg_st.data_au8[0] = 0xAA;
							}
							flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
							
						//	else if(COM_HDR_ENABLED == uart_ble_bootloader_start_cmd_gu8)
						//	{
						//		ble_comm_uart_msg_st.data_au8[0] = 0xAA;
						//		ble_comm_uart_msg_st.length_u16 = 1;
						//	}
						}

#if 0
						if(COM_HDR_ENABLED == uart_modem_bootloader_start_cmd_gu8)
						{
							modem_comm_uart_msg_st.cmd_id_e = MODEM_COMM_CMD_MCU_BTLDR_START_ACK;

							if(BOOT_LOADER_FAILURE == boot_loader_status_e)
							{
								modem_comm_uart_msg_st.data_au8[0] = 0xFF;  /* Bootloader failed  */
								modem_comm_uart_msg_st.length_u16 = 1;
								modem_comm_uart_msg_st.cmd_id_e = MODEM_COMM_CMD_MCU_BTLDR_EXIT_ACK;
							}

							os_comm_uart_queue_st.event_e = OS_UART_2_TX;
							os_comm_uart_queue_st.payload_pv =	(modem_comm_uart_msg_tst*) &modem_comm_uart_msg_st;
							xQueueSend(os_uart_comm_t10_qhandler_ge, &os_comm_uart_queue_st, COM_HDR_NULL);
						}
#endif
				//		else if(COM_HDR_ENABLED == can_bootloader_cmd_received_gvlu8)
						{
							storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_START_ACK;
							storage_task_can_msg_st.length_u8 = 1;
							storage_tx_can_comm_queue_st.comm_event_e = OS_CAN_DEBUG_TX;
							storage_tx_can_comm_queue_st.can_rx_msg_pv = &storage_task_can_msg_st;

							if(BOOT_LOADER_FAILURE == boot_loader_status_e)
							{
								storage_task_can_msg_st.data_au8[0] = 0xFE;
								storage_task_can_msg_st.data_au8[0] = 0xFF;
								storage_task_can_msg_st.length_u8 = 2;
								storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;
								/* To make other tasks know bootloader exited*/
								can_bootloader_cmd_received_gvlu8 = 0;
						
							}

							xQueueSend(os_can_comm_queue_handler_ge, &storage_tx_can_comm_queue_st, 2);
						}
#if 0
						else if (COM_HDR_ENABLED == uart_ble_bootloader_start_cmd_gu8)
						{
							ble_comm_uart_msg_st.cmd_id_e = BLE_COMM_CMD_MCU_BTLDR_START_ACK;

							if(BOOT_LOADER_FAILURE == boot_loader_status_e)
							{
								ble_comm_uart_msg_st.data_au8[0] = 0xFF;  /* Bootloader failed  */
								ble_comm_uart_msg_st.length_u16 = 1;
								ble_comm_uart_msg_st.cmd_id_e = BLE_COMM_CMD_MCU_BTLDR_EXIT_ACK;
							}

							os_comm_uart_queue_st.event_e = OS_UART_0_TX;
							os_comm_uart_queue_st.payload_pv =	(modem_comm_uart_msg_tst*) &ble_comm_uart_msg_st;
							xQueueSend(os_uart_comm_t10_qhandler_ge, &os_comm_uart_queue_st, COM_HDR_NULL);

						}
#endif

					}break;
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_WR:
					{

						memory_copy_u8_array_v((U8*)&storage_task_boot_data_st,(U8*) os_storage_queue_st.payload_pv, 9);
					//	memory_copy_u8_array_v((U8*)&storage_task_boot_data_st.boot_data_e, os_storage_queue_st.payload_pv ,5);
//								storage_task_boot_data_st.size_u32 + 5);
						U8 i_u8;
						switch (storage_task_boot_data_st.boot_data_e)
						{
							/**********************************************************************************************************************/
							case STORAGE_BOOT_DATA:
							{
								//memory_copy_u8_array_v(storage_task_spi_flash_wr_data_au8,storage_task_boot_data_st.boot_data_array_au8,storage_task_boot_data_st.size_u32);
								U32 data_reset_count_u32 = 0;
								if (storage_task_boot_data_st.size_u32 >= 1024)
								{
									for (i_u8 = 0; i_u8 < 4; i_u8++)
									{
										//flash_drv_app_wr_data_u8(&storage_task_spi_flash_wr_data_au8[data_reset_count_u32],
										flash_drv_app_wr_data_u8(&boot_loader_spi_flash_data_au8[data_reset_count_u32],
												&start_boot_loader_write_address_u32, FLASH_DRV_FLASH_PAGE_SIZE);
										data_reset_count_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
									}



#if 1
									data_reset_count_u32 = 0;
									i_u8 = 0;
									for (i_u8 = 0; i_u8 < 4; i_u8++)
									{
										flash_drv_app_rd_data_u8(&read_boot_loader_buffer_au8[data_reset_count_u32],
																		start_read_boot_loader_address_u32, FLASH_DRV_FLASH_PAGE_SIZE);
										start_read_boot_loader_address_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
										data_reset_count_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
									}


									storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_DATA_ACK;
#endif


								}
								else
								{

									U32 storage_data_rec_count_u32;
									storage_data_rec_count_u32 = storage_task_boot_data_st.size_u32;
									while (storage_data_rec_count_u32 != 0)
									{
										if (storage_data_rec_count_u32 >= FLASH_DRV_FLASH_PAGE_SIZE)
										{
											/******** address increment itself by write function ***/
											//flash_drv_app_wr_data_u8(&storage_task_spi_flash_wr_data_au8[data_reset_count_u32],
											flash_drv_app_wr_data_u8(&boot_loader_spi_flash_data_au8[data_reset_count_u32],
													&start_boot_loader_write_address_u32, FLASH_DRV_FLASH_PAGE_SIZE);
											data_reset_count_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
											storage_data_rec_count_u32 -= FLASH_DRV_FLASH_PAGE_SIZE;
										}
										else
										{
											//flash_drv_app_wr_data_u8(&storage_task_spi_flash_wr_data_au8[data_reset_count_u32],
											flash_drv_app_wr_data_u8(&boot_loader_spi_flash_data_au8[data_reset_count_u32],
													&start_boot_loader_write_address_u32, storage_data_rec_count_u32);
											storage_data_rec_count_u32 -= storage_data_rec_count_u32;
										}
									}


#if 1
									storage_data_rec_count_u32 = 0;
									data_reset_count_u32 = 0;
									storage_data_rec_count_u32 = storage_task_boot_data_st.size_u32;
									while (storage_data_rec_count_u32 != 0)
									{
										if (storage_data_rec_count_u32 >= FLASH_DRV_FLASH_PAGE_SIZE)
										{
											flash_drv_app_rd_data_u8(&read_boot_loader_buffer_au8[data_reset_count_u32],
																		start_read_boot_loader_address_u32,
																		FLASH_DRV_FLASH_PAGE_SIZE);

											data_reset_count_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
											start_read_boot_loader_address_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
											storage_data_rec_count_u32 -= FLASH_DRV_FLASH_PAGE_SIZE;
										}
										else
										{
											flash_drv_app_rd_data_u8(&read_boot_loader_buffer_au8[data_reset_count_u32],
													start_read_boot_loader_address_u32, storage_data_rec_count_u32);
											storage_data_rec_count_u32 -= storage_data_rec_count_u32;
										}
									}


									storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;
								}

									/* Comparison handled here */
									if (memory_comp_u8(boot_loader_spi_flash_data_au8, read_boot_loader_buffer_au8,
											storage_task_boot_data_st.size_u32))
									{
										flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
										flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
									}
									else
									{
										flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
										flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;

									}
									U16 crc_check_sum_u16;
									crc_check_sum_u16 = bootloader_app_crc16_generate_u16(read_boot_loader_buffer_au8,
																							storage_task_boot_data_st.size_u32);


									//storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;
									storage_task_can_msg_st.data_au8[0] = crc_check_sum_u16;
									storage_task_can_msg_st.data_au8[1] = crc_check_sum_u16 >> 8;
#endif




							}break;
							/**********************************************************************************************************************/
							case STORAGE_BOOT_CONFIG:
							{
								//memory_copy_u8_array_v(&store_boot_conf_st,storage_task_boot_data_st.boot_data_array_au8,storage_task_boot_data_st.size_u32);


								 flash_drv_app_wr_data_u8((U8*) &boot_loader_conf_gst, &boot_loader_config_flash_start_address_u32,
																	sizeof(boot_loader_config_tst));


#if 1
									boot_loader_config_flash_start_address_u32 = START_ADDRESS_BOOT_CONFIG_CHECK;
									flash_drv_app_rd_data_u8((U8*) &store_boot_rd_conf_st,boot_loader_config_flash_start_address_u32,
																										sizeof(store_boot_rd_conf_st));
									if (memory_comp_u8((U8*)&store_boot_rd_conf_st, (U8*)&boot_loader_conf_gst, sizeof(store_boot_conf_st)))
									{
										flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
										flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;

									}
									else
									{
										flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
										flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;

									}


									storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;
									storage_task_can_msg_st.data_au8[0] = 0xAA;
									storage_task_can_msg_st.data_au8[1] = 0xAA;

#endif

							}
							break;
							/**********************************************************************************************************************/
						}

#if 0
						/* Read data from queue which is previously written in flash */
						boot_storage_queue_st.mode_e = OS_FLASH_WRITE_READ_BACK;
						boot_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_UPDATE;
						boot_storage_queue_st.payload_pv = (storage_task_update_batt_data_tst*) &storage_task_boot_data_st;
						xQueueSend(os_storage_t7_qhandler_ge, &boot_storage_queue_st, 2);
#endif
						//operating_system_can_comm_queue_tst storage_tx_can_comm_queue_st;
						storage_task_can_msg_st.length_u8 = 2;
						storage_tx_can_comm_queue_st.comm_event_e = OS_CAN_DEBUG_TX;
						storage_tx_can_comm_queue_st.can_rx_msg_pv = &storage_task_can_msg_st;
						xQueueSend(os_can_comm_queue_handler_ge, &storage_tx_can_comm_queue_st, 2);

					}
					break;
					/**********************************************************************************************************************/
					case  OS_FLASH_WRITE_READ_BACK:
					{


						memory_copy_u8_array_v((U8*)&storage_task_boot_data_st, os_storage_queue_st.payload_pv, 9);
//						memory_copy_u8_array_v((U8*)&storage_task_boot_data_st.boot_data_e, os_storage_queue_st.payload_pv + 4,
//														storage_task_boot_data_st.size_u32 + 5);

//						storage_task_boot_data_st.start_addr_u32 = *((U32*) os_storage_queue_st.payload_pv);
//						storage_task_boot_data_st.end_addr_u32 = *((U32*) os_storage_queue_st.payload_pv);
//						storage_task_boot_data_st.size_u16 = *((U32*) os_storage_queue_st.payload_pv + 2);

						switch (storage_task_boot_data_st.boot_data_e)
						{
							/**********************************************************************************************************************/
							case STORAGE_BOOT_DATA:
							{
								/* memory_copy_u8_array_v(storage_task_spi_flash_wr_data_au8,storage_task_boot_data_st.boot_data_array_au8,
																										storage_task_boot_data_st.size_u32); */
								U32 data_reset_count_u32 = 0;
								U8 i_u8;
								/* flash read data */
								if (storage_task_boot_data_st.size_u32 >= 1024)
								{
									for (i_u8 = 0; i_u8 < 4; i_u8++)
									{
										flash_drv_app_rd_data_u8(&read_boot_loader_buffer_au8[data_reset_count_u32],
																		start_read_boot_loader_address_u32, FLASH_DRV_FLASH_PAGE_SIZE);
										start_read_boot_loader_address_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
										data_reset_count_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
									}


									storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_DATA_ACK;

								}
								else
								{
									U32 storage_data_rec_count_u32;
									storage_data_rec_count_u32 = storage_task_boot_data_st.size_u32;
									while (storage_data_rec_count_u32 != 0)
									{
										if (storage_data_rec_count_u32 >= FLASH_DRV_FLASH_PAGE_SIZE)
										{
											flash_drv_app_rd_data_u8(&read_boot_loader_buffer_au8[data_reset_count_u32],
																		start_read_boot_loader_address_u32,
																		FLASH_DRV_FLASH_PAGE_SIZE);

											data_reset_count_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
											start_read_boot_loader_address_u32 += FLASH_DRV_FLASH_PAGE_SIZE;
											storage_data_rec_count_u32 -= FLASH_DRV_FLASH_PAGE_SIZE;
										}
										else
										{
											flash_drv_app_rd_data_u8(&read_boot_loader_buffer_au8[data_reset_count_u32],
													start_read_boot_loader_address_u32, storage_data_rec_count_u32);
											storage_data_rec_count_u32 -= storage_data_rec_count_u32;
										}
									}


									storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;


								}
								/* memory_copy_u8_array_v(storage_task_spi_flash_wr_data_au8,storage_task_boot_data_st.boot_data_array_au8,
																									storage_task_boot_data_st.size_u32); */
								/* Comparison handled here */
								if (memory_comp_u8(boot_loader_spi_flash_data_au8, read_boot_loader_buffer_au8,
										storage_task_boot_data_st.size_u32))
								{
									flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
									flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;
								}
								else
								{
									flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
									flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;

								}
								U16 crc_check_sum_u16;
								crc_check_sum_u16 = bootloader_app_crc16_generate_u16(read_boot_loader_buffer_au8,
																						storage_task_boot_data_st.size_u32);


								//storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;
								storage_task_can_msg_st.data_au8[0] = crc_check_sum_u16;
								storage_task_can_msg_st.data_au8[1] = crc_check_sum_u16 >> 8;



								//memory_copy_u8_array_v(storage_task_can_msg_st.data_au8,storage_task_spi_flash_rd_data_au8,1024);
							}
							break;
							/**********************************************************************************************************************/
							case STORAGE_BOOT_CONFIG:
							{
								boot_loader_config_flash_start_address_u32 = START_ADDRESS_BOOT_CONFIG_CHECK;
								flash_drv_app_rd_data_u8((U8*) &store_boot_rd_conf_st,boot_loader_config_flash_start_address_u32,
																									sizeof(store_boot_rd_conf_st));
								if (memory_comp_u8((U8*)&store_boot_rd_conf_st, (U8*)&boot_loader_conf_gst, sizeof(store_boot_conf_st)))
								{
									flash_drv_write_sts_u8 = FLASH_DRV_WR_FAILURE;
									flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_FAILED;

								}
								else
								{
									flash_drv_write_sts_u8 = FLASH_DRV_WR_SUCCESS;
									flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;

								}


								storage_task_can_msg_st.can_id_u32 = DEBUG_COMM_BOOT_EOF_ACK;
								storage_task_can_msg_st.data_au8[0] = 0xAA;
								storage_task_can_msg_st.data_au8[1] = 0xAA;


							}
							break;
							/**********************************************************************************************************************/
						}


						//operating_system_can_comm_queue_tst storage_tx_can_comm_queue_st;
						storage_task_can_msg_st.length_u8 = 2;
						storage_tx_can_comm_queue_st.comm_event_e = OS_CAN_DEBUG_TX;
						storage_tx_can_comm_queue_st.can_rx_msg_pv = &storage_task_can_msg_st;
						xQueueSend(os_can_comm_queue_handler_ge, &storage_tx_can_comm_queue_st, 2);



					}
					break;
					/**********************************************************************************************************************/
					default:
					{

					}
					break;
					/**********************************************************************************************************************/
				}
			}
			else if(os_storage_queue_st.event_e == OS_STORAGE_BOOTLOADER_ERASE)
			{
			   switch (os_storage_queue_st.mode_e)
			   {
			   		/**********************************************************************************************************************/
			    	case OS_FLASH_ERASE_SECTOR:
			      	{
			      		flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;

			      		/* sector erase command from queue and send flash erase command */
			      		flash_drv_blocks_erase_st.start_block_cnt_u16 = *((U32*)os_storage_queue_st.payload_pv);
			      		flash_drv_blocks_erase_st.end_block_cnt_u16 = *((U32*)os_storage_queue_st.payload_pv + 1);

                		if( flash_drv_blocks_erase_st.start_block_cnt_u16 < flash_drv_blocks_erase_st.end_block_cnt_u16)
                		{
                			flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_BLOCK, (flash_drv_blocks_erase_st.start_block_cnt_u16 * 16 * 4096),
																											MAX_BLOCK_ERASE_TIME_MS);

							os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
			    	  		os_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_ERASE;
			    	  		os_storage_queue_st.payload_pv = &flash_drv_blocks_erase_st;
			    	  		xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,2);

						}

			      	}
			      	break;
			      	/**********************************************************************************************************************/
				    case OS_FLASH_MODE_STATUS_CHECK :
				    {
				    	ret_val_u8 = flash_drv_wait_for_wr_end_no_blocking_u8();
				    	if(((flash_drv_status_reg_1_u8 & 0x01) == 0x00))
				    	{
				    		flash_drv_blocks_erase_st.start_block_cnt_u16 ++;
				    		if( flash_drv_blocks_erase_st.start_block_cnt_u16 < flash_drv_blocks_erase_st.end_block_cnt_u16)
				    		{
					    	  	os_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
					    	  	os_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_ERASE;
					    	  	os_storage_queue_st.payload_pv = &flash_drv_blocks_erase_st ;
					    	  	xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,2);
				    		}
				    		else
				    		{
				    			flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
				    		}

				    	}
					   else
					   {
					   	  	os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
					   	  	os_storage_queue_st.event_e = OS_STORAGE_BOOTLOADER_ERASE;
					   	  	os_storage_queue_st.payload_pv = &dummy_data_u8;				/* dummy data must send */
					   	  	xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,2);
					   }
				    }
				    /**********************************************************************************************************************/
			   	  	default:
			   	    {

			   	    }
			   	    break;
			   	    /**********************************************************************************************************************/
			   }
			}
#if 0
			else if(os_storage_queue_st.event_e == OS_STORAGE_CHIP_ERASE)
			{
				switch (os_storage_queue_st.mode_e)
				{
					/**********************************************************************************************************************/
					case OS_FLASH_ERASE_CHIP:
					{
						flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;
						os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
						os_storage_queue_st.event_e = OS_STORAGE_CHIP_ERASE;
						os_storage_queue_st.payload_pv = &dummy_data_u8;					/* dummy data must send */
						flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_FULL_CHIP, 0x00, 10000);
						xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);
					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_STATUS_CHECK :
					{
						ret_val_u8 = flash_drv_wait_for_wr_end_no_blocking_u8();
						if(((flash_drv_status_reg_1_u8 & 0x01) == 0x00))
						{
							/* Once chip erase command complete need to send ack data via CAN */
							operating_system_can_comm_queue_tst storage_tx_can_comm_queue_st;
							
							storage_tx_can_comm_queue_st.comm_event_e = OS_CAN_0_TX_AFTER_STORAGE_COMPLETE;
							storage_tx_can_comm_queue_st.can_rx_msg_pv = &dummy_data_u8;
							xQueueSend(os_can_comm_queue_handler_ge, &storage_tx_can_comm_queue_st, 2);
							flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
							start_address_BMS_1Second_Log  = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;
							flsh_retrival_size_u32 = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;


						}
						else
						{
						   os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
				           os_storage_queue_st.event_e = OS_STORAGE_CHIP_ERASE;
						   os_storage_queue_st.payload_pv = &dummy_data_u8;						/* dummy data must send */
						   xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);
						}
				  	}
				 	break;
				 	/**********************************************************************************************************************/
					default :
					{

					}
					/**********************************************************************************************************************/
			 	}
			}
			else if(os_storage_queue_st.event_e == OS_STORAGE_DATA_BLOCK_ERASE)
			{
				switch (os_storage_queue_st.mode_e)
				{
					/**********************************************************************************************************************/
					case OS_FLASH_ERASE_BLOCK:
					{
						flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;

						flash_drv_blocks_erase_st.start_block_cnt_u16 = *((U16*) os_storage_queue_st.payload_pv);
						flash_drv_blocks_erase_st.end_block_cnt_u16   = *((U16*) os_storage_queue_st.payload_pv + 1);

						if (flash_drv_blocks_erase_st.start_block_cnt_u16 < flash_drv_blocks_erase_st.end_block_cnt_u16)
						{
							/* send block erase */
							flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_BLOCK,
									(flash_drv_blocks_erase_st.start_block_cnt_u16 * STORAGE_DEF_SECTORS_PER_BLOCK
											* STORAGE_DEF_SECTOR_SIZE), MAX_BLOCK_ERASE_TIME_MS);

							os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							os_storage_queue_st.event_e = OS_STORAGE_DATA_BLOCK_ERASE;
							os_storage_queue_st.payload_pv = (flash_drv_blocks_erase_tst*)&flash_drv_blocks_erase_st;
							xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);
						}

					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_STATUS_CHECK :
					{
						ret_val_u8 = flash_drv_wait_for_wr_end_no_blocking_u8();
						if(((flash_drv_status_reg_1_u8 & 0x01) == 0x00))
						{
							flash_drv_blocks_erase_st.current_block_cnt_erased_u16 ++;
							flash_drv_blocks_erase_st.total_blocks_erased_in_percentage_u8 = (flash_drv_blocks_erase_st.current_block_cnt_erased_u16 * 100)/ flash_drv_blocks_erase_st.total_blocks_to_erase_u16;
							common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_state_u8 = DEBUG_COMM_EXT_FLASH_ERASE_PERCENT_STS_SEND_STATE;

							storage_tx_can_comm_queue_st.comm_event_e = OS_CAN_0_TX_AFTER_STORAGE_COMPLETE;
							storage_tx_can_comm_queue_st.can_rx_msg_pv = &dummy_data_u8;
							xQueueSend(os_can_comm_queue_handler_ge, &storage_tx_can_comm_queue_st, 2);

							flash_drv_blocks_erase_st.start_block_cnt_u16 ++;
							if (flash_drv_blocks_erase_st.start_block_cnt_u16 < flash_drv_blocks_erase_st.end_block_cnt_u16)
							{
								os_storage_queue_st.mode_e = OS_FLASH_ERASE_BLOCK;
								os_storage_queue_st.event_e = OS_STORAGE_DATA_BLOCK_ERASE;
								os_storage_queue_st.payload_pv = (U16 *)&flash_drv_blocks_erase_st;
								xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);
							}
							else
							{
								operating_system_can_comm_queue_tst storage_tx_can_comm_queue_st;

								storage_tx_can_comm_queue_st.comm_event_e = OS_CAN_0_TX_AFTER_STORAGE_COMPLETE;
								storage_tx_can_comm_queue_st.can_rx_msg_pv = &dummy_data_u8;
								xQueueSend(os_can_comm_queue_handler_ge, &storage_tx_can_comm_queue_st, 2);

								start_address_BMS_1Second_Log  = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;
								flsh_retrival_size_u32 = STORAGE_DEF_START_ADDRESS_BMS_1SEC_LOG;

						    	flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
							}
						}
						else
						{
							os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							os_storage_queue_st.event_e = OS_STORAGE_DATA_BLOCK_ERASE;
							os_storage_queue_st.payload_pv = &dummy_data_u8;							/* dummy data must send */
							xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);

						}
					}
					break;
					/**********************************************************************************************************************/
				    default :
				    {

					}
					/**********************************************************************************************************************/
				}
			}
			else if (os_storage_queue_st.event_e == OS_STORAGE_DATA_SECTOR_ERASE)
			{
				switch (os_storage_queue_st.mode_e)
				{
					/**********************************************************************************************************************/
					case OS_FLASH_ERASE_SECTOR:
					{
						flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_INPROGRESS;

						flash_drv_sector_erase_st.start_sect_cnt_u16 = *((U16*) os_storage_queue_st.payload_pv);
						flash_drv_sector_erase_st.end_sect_cnt_u16   = *((U16*) os_storage_queue_st.payload_pv + 1);

						if (flash_drv_sector_erase_st.start_sect_cnt_u16 < flash_drv_sector_erase_st.end_sect_cnt_u16)
						{
							/* send block erase */
							flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR,
														(flash_drv_sector_erase_st.start_sect_cnt_u16 * STORAGE_DEF_SECTOR_SIZE),
																										MAX_SECTOR_ERASE_TIME_MS);

							os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							os_storage_queue_st.event_e = OS_STORAGE_DATA_SECTOR_ERASE;
							os_storage_queue_st.payload_pv = (U16 *)&flash_drv_sector_erase_st;
							xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);
						}
					}
					break;
					/**********************************************************************************************************************/
					case OS_FLASH_MODE_STATUS_CHECK :
					{
						ret_val_u8 = flash_drv_wait_for_wr_end_no_blocking_u8();
						if(((flash_drv_status_reg_1_u8 & 0x01) == 0x00))
						{
							flash_drv_sector_erase_st.start_sect_cnt_u16 ++;
							if(flash_drv_sector_erase_st.start_sect_cnt_u16 < flash_drv_sector_erase_st.end_sect_cnt_u16)
							{
								os_storage_queue_st.mode_e = OS_FLASH_ERASE_SECTOR;
								os_storage_queue_st.event_e = OS_STORAGE_DATA_SECTOR_ERASE;
								os_storage_queue_st.payload_pv = (U16 *)&flash_drv_sector_erase_st;
								xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);
							}
							else
							{
								flash_drv_flash_operation_sts_u8 = FLASH_DRV_OPERATION_COMPLETED;
							}
						}
						else
						{
							os_storage_queue_st.mode_e = OS_FLASH_MODE_STATUS_CHECK;
							os_storage_queue_st.event_e = OS_STORAGE_DATA_SECTOR_ERASE;
							os_storage_queue_st.payload_pv = &dummy_data_u8;							/* dummy data must send */
							xQueueSend(os_storage_t7_qhandler_ge, &os_storage_queue_st,100);

						}
					}
					break;
					/**********************************************************************************************************************/
				    default :
				    {

					}
					/**********************************************************************************************************************/
				}
			}
#endif

		}
		

#if 0
		/* Low Power Mode functionality */
		while (lpapp_mode_status_gst.enable_low_power_b)
		{
			/* Loop Here */
			lpapp_task_states_agu8[TASK_STORAGE] = COM_HDR_TRUE;
			vTaskDelay(10);
		}

		lpapp_task_states_agu8[TASK_STORAGE] = COM_HDR_FALSE;
#endif

	}
}
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
