/**
====================================================================================================================================================================================
@page diagmonitor_task_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	diagmonitor_task.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	09-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include "app_main.h"
#include "low_pwr_app.h"

/* Configuration Header File */
#include "operating_system.h"
//#include "gpio_app.h"
#include "can_app.h"
#include "bootsm.h"

/* This header file */
#include "diagmonitor_task.h"

/* Driver Header File */
#include "gt_timer.h"
#include "gt_can.h"
#include "gt_system.h"
#include "gt_wdg.h"
#include "gt_memory.h"
#include "gt_nvic.h"


/** @{
 * Private Definitions **/
/** Type Definitions **/
#define DIAGMON_INIT_FAIL_ID    (0x100)

/** @} */

/* Private Function Prototypes */
void diagmonitor_task_start_fault_handler_v(void);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
U32 diagmon_driver_init_fault_code_gu32 = 0;
BOOL diagmon_hold_wdg_refresh_force_reset_b = COM_HDR_FALSE;

U32 diagmon_task_timer_start_agu32[10] ;
U16 diagmon_task_timer_stop_agu32[10] ;
U16 diagmon_task_timer_algo_stop_agu32[10] ;
BOOL diag_flt_rst_flg = COM_HDR_ENABLED;
/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */


/* Public Function Definitions */
void diagmonitor_task_monitor_v(void * parameter);
void diagmonitor_task_initialization_fault_handler_v(void);
U8 diagmonitor_task_enter_low_power_mode_u8(void);
/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void diagmonitor_task_monitor_v(void *parameter)
{

	/* Unused warning removal */
	COM_HDR_UNUSED(parameter);

	U8 ret_val_u8 = 0;
	static U32 flt_reset_counter_u32;

	bootsm_boot_status_e = BOOTSM_SYS_OS_STARTER_ENGINE_RUNNING;

	//INT_SYS_EnableIRQGlobal();
	//nvic_enable_all_Interrupts_v();
	/* Real-time task begins first so the post OS initialization is done here */
	ret_val_u8 = app_main_post_os_periph_config_u8();


	if(ret_val_u8 == 0)
	{
		/* If everything went smoothly */
		bootsm_boot_status_e = BOOTSM_SYS_OS_RUNNING;
	}
	else
	{

		/* IC Failed */
		system_init_flt_update_u8();

		bootsm_boot_status_e = BOOTSM_SYS_OS_APP_POSTOS_INT;
	}

	//diagmonitor_task_start_fault_handler_v();
	/* 23ms for the above part of the task  */

	while(1)
	{
		/* Task execution < 500us */
		diagmon_task_timer_stop_agu32[1] = gt_timer_get_tick_u32() - diagmon_task_timer_start_agu32[1];

		diagmon_task_timer_start_agu32[1] = gt_timer_get_tick_u32();// - diagmon_task_timer_start_agu32[1];


#if 0
		/* Every cycle refresh the Watch-dog Timer */
		if(diagmon_hold_wdg_refresh_force_reset_b == COM_HDR_FALSE)
		{
			wdg_refresh_counter_v();
		}

		/* Sleep / Low Power Mode Operation */

		diagmon_task_timer_algo_stop_agu32[1] = gt_timer_get_tick_u32() - diagmon_task_timer_start_agu32[1];
#endif
		wdg_refresh_counter_v();

		vTaskDelay(200);

		low_pwr_app_sm_v(); /* Uncomment in real board*/

		/** Fault reset counter after initilisation to prevent initial readings fault **/
		if(diag_flt_rst_flg == COM_HDR_ENABLED)
		{
			flt_reset_counter_u32++;
		}
		if(flt_reset_counter_u32 > 2)
		{
			system_diag_app_runtime_prev_flt_gu64 = 0x0000000000000000;
			system_diag_app_runtime_flt_code_gu64 = 0x0000000000000000;
			diag_flt_rst_flg = COM_HDR_DISABLED;
			flt_reset_counter_u32 = 0;
		}
	//	diagmon_task_timer_stop_agu32[1] = gt_timer_get_tick_u32() - diagmon_task_timer_start_agu32[1];
	}
}

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void diagmonitor_task_start_fault_handler_v(void)
{

	//can_app_message_tst can_comm_tx_msg_st;

	/* Check if there exist a fault */
	if (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
	{
		/* This is a OS Call so be careful */

		/* Transmit the Diagnostic data */
	}
	else
	{
		bootsm_boot_status_e = BOOTSM_SYS_OS_RUNNING;
	}
}


/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
