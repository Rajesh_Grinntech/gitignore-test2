/**
====================================================================================================================================================================================
@page bms_con_ctrl_task_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_con_ctrl_task.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	24-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */


#include "common_header.h"
//#include "sleep_app.h"
/* Application Header File */
#include "bms_afe_task.h"
#include "switch_ctrl.h"
#include "batt_param.h"
#include "bootsm.h"

/* Configuration Header File */
//#include "cont_op.h"
/* This header file */
#include "bms_mfet_ctrl_task.h"
#include "low_pwr_app.h"

/* Driver Header File */

//#include "bms_op_cmd_task.h"
//#include "can_comm_task.h"
//#include "ms_1000_msgs.h"
#include "task.h"
#include "gt_timer.h"
#include "bq76952.h"

/** @{
 * Private Definitions **/
U8 precharge_flag_u8 = 0;
U8 charge_mfet_flg_u8 = 0;
/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
//U8 bms_tsk_mfet_idl_flg_u8 = 0;
//U8 bms_tsk_mfet_idl_counter_u8 = 1;

BOOL idle_detect_set_timer_b = COM_HDR_FALSE;
U16  idle_detect_counter_u16 = 0;
BOOL pchg_pdsg_mode_b = 0;

#if !FET_TASK_SWITCH_CASE
BOOL idle_fet_change_req_b = COM_HDR_FALSE;
BOOL chg_dsg_deep_fet_change_req_b = COM_HDR_FALSE;
BOOL chg_dsg_fet_change_req_b = COM_HDR_FALSE;
BOOL chg_only_fet_change_req_b = COM_HDR_FALSE;
BOOL dsg_only_fet_change_req_b = COM_HDR_FALSE;
BOOL bothoff_fet_change_req_b = COM_HDR_FALSE;

#else
mfet_task_states_te mfet_task_states_e = STATE_IDLE;
mfet_task_states_te prev_mfet_task_states_e = STATE_IDLE;
#endif
/** @} */
BOOL fault_recovery_b = COM_HDR_FALSE;

/* Public Function Definitions */
void bms_mfet_ctrl_task_v(void *parameter);

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bms_mfet_ctrl_task_v(void *parameter)
{
	U8 error_code_u8 = 0;

	/* Unused warning removal */
	COM_HDR_UNUSED(parameter);
	COM_HDR_UNUSED(error_code_u8);

	/* Wait Until the OS is completely up */
	while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
	{
		vTaskDelay(10);
	}

	while(bms_afe_task_state_ge == BMS_AFE_TASK_START)
	{
		vTaskDelay(10);
	}

	while(BATT_MODE_FAULT == batt_param_pack_param_gst.pack_param_batt_mode_e)
	{
		vTaskDelay(10);
	}

#if !FET_TASK_SWITCH_CASE
	/* Idle Condition - PreChg and preDsg alone ON*/
	chg_dsg_fet_change_req_b = COM_HDR_TRUE;
	chg_dsg_deep_fet_change_req_b = COM_HDR_TRUE;
	chg_only_fet_change_req_b = COM_HDR_TRUE;
	dsg_only_fet_change_req_b = COM_HDR_TRUE;
	bothoff_fet_change_req_b = COM_HDR_TRUE;

	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);

	//	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
	//	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

	#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
	#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_BOTH_MAIN_MFET_ON;
	#endif




	while(1)
	{
		if( PACK_PARAM_ACTION_NA == batt_param_pack_param_gst.pack_param_trip_action_e)
		{
		if(BATT_MODE_IDLE == batt_param_pack_param_gst.pack_param_batt_mode_e)
		{
			idle_detect_set_timer_b = COM_HDR_TRUE;
			if (fault_recovery_b == COM_HDR_TRUE)
			{
				fault_recovery_b = COM_HDR_FALSE;
				idle_fet_change_req_b = COM_HDR_FALSE;
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
				//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
				//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_BOTH_MAIN_MFET_ON;
#endif

			}
			if(idle_detect_counter_u16 > 15) /* 600seconds = 1 min(60s) * 10 */
			{
				idle_detect_set_timer_b = COM_HDR_FALSE;
				idle_detect_counter_u16 = 0;
				pchg_pdsg_mode_b = 1;

				if(idle_fet_change_req_b)
				{
					idle_fet_change_req_b = COM_HDR_FALSE;
					chg_dsg_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_deep_fet_change_req_b = COM_HDR_TRUE;
					chg_only_fet_change_req_b = COM_HDR_TRUE;
					dsg_only_fet_change_req_b = COM_HDR_TRUE;
					bothoff_fet_change_req_b = COM_HDR_TRUE;

					/* Idle Condition - PreChg and preDsg alone ON*/
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);

					//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
					//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				}

				/* Updates Mosfet Status*/
				if(batt_param_pack_param_gst.pack_param_afe_fet_malfunction_error_e == PACK_PARAM_LIMIT_ACTIVE)
				{
					batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
							| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
							| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
							| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
				}
				else
				{
#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_BOTH_MAIN_MFET_ON;
#endif

				}
			}
		}
		else if((BATT_MODE_CHG == batt_param_pack_param_gst.pack_param_batt_mode_e)
				||  (BATT_MODE_DSG == batt_param_pack_param_gst.pack_param_batt_mode_e))
		{
			idle_detect_set_timer_b = COM_HDR_FALSE;
			idle_detect_counter_u16 = 0;



			if(pchg_pdsg_mode_b && (BATT_MODE_DSG == batt_param_pack_param_gst.pack_param_batt_mode_e))
			{
				pchg_pdsg_mode_b = 0;
				gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,850);
			}

			if((batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
					&& chg_dsg_deep_fet_change_req_b == COM_HDR_TRUE)
			{
				chg_dsg_deep_fet_change_req_b = COM_HDR_FALSE;
				idle_fet_change_req_b = COM_HDR_TRUE;
				chg_dsg_fet_change_req_b = COM_HDR_TRUE;
				chg_only_fet_change_req_b = COM_HDR_TRUE;
				dsg_only_fet_change_req_b = COM_HDR_TRUE;
				bothoff_fet_change_req_b = COM_HDR_TRUE;

				/* CHG/DSG Condition - CHg and DSg Main mfets alone ON*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
			}
			else if((chg_dsg_fet_change_req_b == COM_HDR_TRUE)
					&& (batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE))
			{
				idle_fet_change_req_b = COM_HDR_TRUE;
				chg_dsg_fet_change_req_b = COM_HDR_FALSE;
				chg_dsg_deep_fet_change_req_b = COM_HDR_TRUE;
				chg_only_fet_change_req_b = COM_HDR_TRUE;
				dsg_only_fet_change_req_b = COM_HDR_TRUE;
				bothoff_fet_change_req_b = COM_HDR_TRUE;

				/* CHG/DSG Condition - CHg and DSg Main mfets alone ON*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
				gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,20);  /* To inform Prabu - added by Yazh*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
			}

			/* Updates Mosfet Status*/
#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_BOTH_MAIN_MFET_ON;
#endif

		}
	}
	else
		{
			/* Fault condition - Main Mfets off*/
			if(		(idle_fet_change_req_b == COM_HDR_FALSE) ||
					(chg_dsg_fet_change_req_b == COM_HDR_FALSE) )
			{
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
			}
			//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
			fault_recovery_b = COM_HDR_TRUE;
			/* reset this variable*/
			pchg_pdsg_mode_b = COM_HDR_FALSE;

			if(PACK_PARAM_ACTION_ONLY_DHG_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
			{

				if(chg_only_fet_change_req_b == COM_HDR_TRUE)
				{
					idle_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_deep_fet_change_req_b = COM_HDR_TRUE;
					chg_only_fet_change_req_b = COM_HDR_FALSE;
					dsg_only_fet_change_req_b = COM_HDR_TRUE;
					bothoff_fet_change_req_b = COM_HDR_TRUE;

					//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
				}
			/* Updates Mosfet Status*/
#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = 	BATT_PARAM_CHG_MFET_ONLY_ON;
#endif


				/* Set this parameter to '1' when switch off Main mosfets & any of Pchg/PDsg Mosfets ON */
				pchg_pdsg_mode_b = COM_HDR_TRUE;
			}

			if(PACK_PARAM_ACTION_ONLY_CHG_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
			{

				if(dsg_only_fet_change_req_b == COM_HDR_TRUE)
				{
					idle_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_deep_fet_change_req_b = COM_HDR_TRUE;
					chg_only_fet_change_req_b = COM_HDR_TRUE;
					dsg_only_fet_change_req_b = COM_HDR_FALSE;
					bothoff_fet_change_req_b = COM_HDR_TRUE;

					//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);
				}

			/* Updates Mosfet Status*/
#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_DSG_MFET_ONLY_ON;
#endif


				/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
				pchg_pdsg_mode_b = COM_HDR_TRUE;
			}

			if(PACK_PARAM_ACTION_BOTH_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
			{
				if(bothoff_fet_change_req_b == COM_HDR_TRUE)
				{
					idle_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_fet_change_req_b = COM_HDR_TRUE;
					chg_dsg_deep_fet_change_req_b = COM_HDR_TRUE;
					chg_only_fet_change_req_b = COM_HDR_TRUE;
					dsg_only_fet_change_req_b = COM_HDR_TRUE;
					bothoff_fet_change_req_b = COM_HDR_FALSE;

					//switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

					/* Fault condition - with no pchg/pdsg needed - all mfets OFF */
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				}

			/* Updates Mosfet Status*/

#if FET_REAL_STATUS
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));
#else
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_ALL_MFET_OFF;
#endif

			}
		}


#else

	#if FET_TASK_WITH_PRE
	/* Initial state of Mosfets - With Pre*/
	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);//SWITCH_CTRL_ACTION_BOTH_OFF
	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);//SWITCH_CTRL_ACTION_BOTH_ON
	#else
	/* Initial state of Mosfets - Without Pre*/
	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
	switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
	pchg_pdsg_mode_b = COM_HDR_FALSE;
	#endif

	while(1)
	{
		/* Assigning Current Mosfet state to Prev_Mosfet State on every loop */
		prev_mfet_task_states_e = mfet_task_states_e;
		if(PACK_PARAM_ACTION_NA == batt_param_pack_param_gst.pack_param_trip_action_e)
		{
			if(BATT_MODE_IDLE == batt_param_pack_param_gst.pack_param_batt_mode_e)
			{
				idle_detect_set_timer_b = COM_HDR_TRUE;

				if (fault_recovery_b == COM_HDR_TRUE)
				{
					fault_recovery_b = COM_HDR_FALSE;
					#if FET_TASK_WITH_PRE
					mfet_task_states_e = STATE_IDLE;
					#else
					mfet_task_states_e = STATE_CHG_DSG;
					#endif

				}
				else if(idle_detect_counter_u16 > 15) /* 600seconds = 1 min(60s) * 10 */
				{
					idle_detect_set_timer_b = COM_HDR_FALSE;
					idle_detect_counter_u16 = 16;
					#if FET_TASK_WITH_PRE
					mfet_task_states_e = STATE_IDLE;
					#else
					mfet_task_states_e = STATE_CHG_DSG;
					#endif
				}
			}
			else if((BATT_MODE_CHG == batt_param_pack_param_gst.pack_param_batt_mode_e)
				||  (BATT_MODE_DSG == batt_param_pack_param_gst.pack_param_batt_mode_e))
			{
				idle_detect_set_timer_b = COM_HDR_FALSE;
				idle_detect_counter_u16 = 0;

				if(batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e == PACK_PARAM_LIMIT_ACTIVE)
				{
					#if FET_TASK_WITH_PRE
					mfet_task_states_e = STATE_CHG_DSG_DEEP;
					#else
					mfet_task_states_e = STATE_CHG_DSG;
					#endif
				}
				else if(batt_param_pack_param_gst.pack_param_cell_v_deep_dchg_limit_active_e != PACK_PARAM_LIMIT_ACTIVE)
				{
					mfet_task_states_e = STATE_CHG_DSG;
				}
			}
		}
		else if(PACK_PARAM_ACTION_ONLY_DHG_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
		{
			mfet_task_states_e = STATE_FAULT_CHG_ONLY_ON;
		}
		else if(PACK_PARAM_ACTION_ONLY_CHG_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
		{
			mfet_task_states_e = STATE_FAULT_DSG_ONLY_ON;
		}
		else if(PACK_PARAM_ACTION_BOTH_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
		{
			mfet_task_states_e = STATE_FAULT_BOTH_OFF;
		}
		if(prev_mfet_task_states_e != mfet_task_states_e){
		switch(mfet_task_states_e)
		{
		/**********************************************************************************************************************/
			case STATE_IDLE:
				/* Idle Condition - PreChg and preDsg alone ON*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);//SWITCH_CTRL_ACTION_BOTH_ON

				#if FET_TASK_WITH_PRE
				/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
				pchg_pdsg_mode_b = COM_HDR_TRUE;
				#endif

			break;
			/**********************************************************************************************************************/
			case STATE_CHG_DSG:

				if(pchg_pdsg_mode_b && (BATT_MODE_DSG == batt_param_pack_param_gst.pack_param_batt_mode_e))
				{
					gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,850);
				}

				/* Idle Condition - PreChg and preDsg alone ON*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);

				if(pchg_pdsg_mode_b){
					pchg_pdsg_mode_b = COM_HDR_FALSE;
					gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,20);
				}
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
			break;
			/**********************************************************************************************************************/
			case STATE_CHG_DSG_DEEP:
				/* Idle Condition - PreChg and preDsg alone ON*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

				#if FET_TASK_WITH_PRE
				/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
				pchg_pdsg_mode_b = COM_HDR_TRUE;
				#endif
			break;
			/**********************************************************************************************************************/
			case STATE_FAULT_CHG_ONLY_ON:
				/* Idle Condition - PreChg and preDsg alone ON*/
				#if FET_TASK_WITH_PRE
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				#else
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				#endif

				#if FET_TASK_WITH_PRE
				/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
				pchg_pdsg_mode_b = COM_HDR_TRUE;
				#endif

			break;
			/**********************************************************************************************************************/
			case STATE_FAULT_DSG_ONLY_ON:
				/* Idle Condition - PreChg and preDsg alone ON*/
				#if FET_TASK_WITH_PRE
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				#else
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				#endif

				#if FET_TASK_WITH_PRE
				/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
				pchg_pdsg_mode_b = COM_HDR_TRUE;
				#endif
			break;
			/**********************************************************************************************************************/
			case STATE_FAULT_BOTH_OFF:
				/* Idle Condition - PreChg and preDsg alone ON*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

				#if FET_TASK_WITH_PRE
				pchg_pdsg_mode_b = COM_HDR_FALSE; /* Pchg/Pdsg turned off*/
				#endif
			break;
			/**********************************************************************************************************************/

			default:
			{

			}

		} /* Closing switch case*/
		} /* Closing if prev != current state*/

	/* Updates Mosfet Status*/
	batt_param_pack_param_gst.pack_param_all_mfet_sts_e = (((bq76952_fet_stat_u.bits.pdsg_fet_b1 << 3) & 0x8)
			| ((bq76952_fet_stat_u.bits.pchg_fet_b1 << 2) & 0x4)
			| ((bq76952_fet_stat_u.bits.dsg_fet_b1 << 1) & 0x2)
			| (bq76952_fet_stat_u.bits.chg_fet_b1 & 0x1));


#endif

#if 0
		/**********************************************************************************************************************/
		if((batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16 != 0) && (batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 != 65535))/* AFE cell voltages will be zero immediately after power on reset - at that time mosfet status is getting changed -
		 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 To avoid that this condition has been added */
		{

			if(PACK_PARAM_ACTION_NA == batt_param_pack_param_gst.pack_param_trip_action_e)
			{
				if((precharge_flag_u8 == 0) && (batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_IDLE))
				{
					bms_tsk_mfet_idl_flg_u8 = 0;
					if((bms_tsk_mfet_idl_counter_u8 > 10) && (bms_tsk_mfet_idl_flg_u8 == 0))
					{
						/* Reseting parameters used in detecting real Batt IDLE */
						bms_tsk_mfet_idl_flg_u8 = 1;
						bms_tsk_mfet_idl_counter_u8 = 0;

						switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
						/* TODO Check with HW Delay Time - FET turn on delay */
						//gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,100);
						switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

						/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
						precharge_flag_u8 = 1;

						/* Update mosfet current state */
						batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_BOTH_PRE_MFET_ON;

					}
				}

				if(((batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
							|| (batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG))
						&& (precharge_flag_u8 == 1))
				{
					gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,850);
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_ON);
					/* TODO Check with HW Delay Time - FET turn on delay */
					//gt_timer_delay_v(TIMER_RESOLUTION_MILISECOND,100);
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

					/* Update mosfet current state */
					batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_BOTH_MAIN_MFET_ON;

					/* Set this parameter to '0'when switch off both Prechg/PreDsg FETs */
					precharge_flag_u8 = 0;

					/* Reseting parameters used in detecting real Batt IDLE */
					bms_tsk_mfet_idl_flg_u8 = 1;
					bms_tsk_mfet_idl_counter_u8 = 0;
				}
			}
#if 1
			else
			{
				/* When its not a Normal trip Action - Switch Main mosfets OFF in all cases below*/
				switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_MAIN_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

				/* Set this parameter to '1' when switch off Main mosfets & any of Prexg/PreDsg Mosfets ON */
				precharge_flag_u8 = 1;

				if(PACK_PARAM_ACTION_ONLY_CHG_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
				{
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_DSG_ON);

					/* Update mosfet current state */
					batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_PDSG_MFET_ONLY_ON;
				}
				else if(PACK_PARAM_ACTION_ONLY_DHG_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
				{
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_ONLY_CHG_ON);

					/* Update mosfet current state */
					batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_PCHG_MFET_ONLY_ON;
				}
				else if(PACK_PARAM_ACTION_BOTH_OFF == batt_param_pack_param_gst.pack_param_trip_action_e)
				{
					switch_ctrl_mfet_ctrl_v(SWITCH_CTRL_PRE_MOSFET,SWITCH_CTRL_ACTION_BOTH_OFF);

					/* Update mosfet current state */
					batt_param_pack_param_gst.pack_param_all_mfet_sts_e = BATT_PARAM_ALL_MFET_OFF;

					/* Set this parameter to '0'when switch off both Prechg/PreDsg FETs */
					precharge_flag_u8 = 0;
				}

			}

#endif
		}

#endif

#if 1
		/* Low Power Mode functionality */
		while (lpapp_mode_status_gst.enable_low_power_b)
		{
			/* Loop Here */
			lpapp_task_states_agu8[TASK_FET_CTRL] = COM_HDR_TRUE;
			vTaskDelay(10);
		}

		lpapp_task_states_agu8[TASK_FET_CTRL] = COM_HDR_FALSE;
#endif

		vTaskDelay(100);
	}
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
