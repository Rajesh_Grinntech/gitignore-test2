/**
====================================================================================================================================================================================
@page can_commtask_source_file

@subsection  Details

@n@b Title:
@n@b Filename:   	can_commtask.c
@n@b MCU:
@n@b Compiler:
@n@b Author:     	KRISH

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#
@n@b Date:       	19-Jun-2020
@n@b Description:  	Created

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include "debug_comm.h"
#include "client_comm.h"
#include "common_var.h"
#include "bms_afe_task.h"
/* Configuration Header File */
#include "bootsm.h"
#include "operating_system.h"
#include "can_app.h"
#include "low_pwr_app.h"

#include "gt_timer.h"
/* This header file */
#include "can_commtask.h"
/* Driver header */
/** @{
 * Private Definitions **/

/** @} */

/* Private Function Prototypes */
static void can_commtask_rx_process_v(can_app_message_tst * rx_can_message_arpst);
static void can_commtask_tx_process_v(can_app_message_tst * tx_can_message_arpst);
static void can_commtask_tx_fault_msg_v(can_app_message_tst * tx_can_message_arpst);

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */


/** @} */

/* Public Function Definitions */


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void can_comm_task_v(void *parameter)
{
	//U8 error_code_u8 = 0;

	/* Unused warning removal */
	COM_HDR_UNUSED(parameter);

	/* Wait Until the OS is completely up */
//	while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
//	{
//		vTaskDelay(10);
//	}

	while(1)
	{
		lpapp_task_states_agu8[TASK_COMM_CAN] = COM_HDR_TRUE;
		if(xQueueReceive(os_can_comm_queue_handler_ge, &os_can_comm_queue_st, portMAX_DELAY) == pdTRUE)
		{
			lpapp_task_states_agu8[TASK_COMM_CAN] = COM_HDR_FALSE;
			switch(os_can_comm_queue_st.comm_event_e)
			{
				/**********************************************************************************************************************/
				case OS_CAN_RX:
				{
					/* #1 -  */
					can_commtask_rx_process_v((can_app_message_tst *)os_can_comm_queue_st.can_rx_msg_pv);

				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_TX:
				{
					/* #2 -  */
					can_commtask_tx_process_v((can_app_message_tst *)os_can_comm_queue_st.can_rx_msg_pv);

				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_DEBUG_RX:
				{
					/* #3 -  */
					debug_comm_canrx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);

					debug_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);

				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_DEBUG_TX:
				{
					/* #4 -  */
					/*  Some states which is related to bootloader has been put to idle state in order to complete external flash
					 *  process - After that process gets completed, queue will send from that particular bootloader state for
					 *  transmitting the CAN data - At this time we are making that idle state to transmit state
					 */
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

					debug_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);
				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_CLIENT_RX:
				{
					client_comm_canrx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);
					client_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);
				}break;
				/**********************************************************************************************************************/
				case OS_CAN_CLIENT_TX:
				{
					client_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);
				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_TX_100MS:
				{
					/* #5 -  */

				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_TX_50MS:
				{
					/* #6 -  */

				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_TX_1000MS:
				{
					/* #7 -  */


				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_0_TX_FLT:
				{
					/* #8 -  */
					can_commtask_tx_fault_msg_v(os_can_comm_queue_st.can_rx_msg_pv);

				}
				break;
				/**********************************************************************************************************************/
				case OS_CAN_0_TX_AFTER_STORAGE_COMPLETE:
				{
					/* Some states which is related to storage task has been put to idle state in order to complete external flash
					 *  process - After that process gets completed, queue will send from that particular storage task state for
					 *  transmitting the CAN data - At this time we are making that idle state to transmit state
					 */
					common_var_can_state_gst[COMMON_VAR_DEBUG_SM].protocol_status_u8 = CAN_APP_STATE_TX;

					/* #9 -  */
					debug_comm_cantx_process_v((can_comm_task_can_msg_tst *)os_can_comm_queue_st.can_rx_msg_pv);

				}
				break;
				/**********************************************************************************************************************/
				default :
				{

				}
				break;
				/**********************************************************************************************************************/
			}
		}

#if 0
		/* Low Power Mode functionality */
		while (lpapp_mode_status_gst.enable_low_power_b)
		{
			/* Loop Here */
			lpapp_task_states_agu8[TASK_COMM_CAN] = COM_HDR_TRUE;
			vTaskDelay(10);
		}

		lpapp_task_states_agu8[TASK_COMM_CAN] = COM_HDR_FALSE;
#endif
	}

}


/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 can_commtask_identify_comm_type_u8(U32 can_rx_id_aru32)
{
	if((0x0800DAFA <= can_rx_id_aru32) && (0x08FFDAFA >= can_rx_id_aru32))
	{
		return COMMON_VAR_DEBUG_SM;
	}

	/* TODO: likewise check & return for Charging SM */


	/* TODO: Likewise check & return for Driving SM */

	return COMMON_VAR_MAX_SM;

}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void can_commtask_rx_process_v(can_app_message_tst * rx_can_message_arpst)
{



}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void can_commtask_tx_process_v(can_app_message_tst * tx_can_message_arpst)
{


}

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void can_comm_set_tmr_v(can_comm_sm_stage_te tmr_type,U32 tmr_val)
{
	switch(tmr_type)
	{
		/*case DRIVING_SM:
		{
			TIMING_StartChannel(&lptmr_0_pal_delay_instance, TIMER_LPIT_CHANNEL_0,((tmr_val * COM_HDR_SCALE_1000) / timer_lptimerpal0_resolution_gu64));
		}
		break;
		case CHARGING_SM:
		{
			TIMING_StartChannel(&lptmr_0_pal_delay_instance, TIMER_LPIT_CHANNEL_0,((tmr_val * COM_HDR_SCALE_1000) / timer_lptimerpal0_resolution_gu64));
		}
		break;
		case TRACKING_SM:
		{
			TIMING_StartChannel(&lptmr_0_pal_delay_instance, TIMER_LPIT_CHANNEL_0,((tmr_val * COM_HDR_SCALE_1000) / timer_lptimerpal0_resolution_gu64));
		}
		break;*/
		case CLIENT_SM:
		{
			can_comm_set_timer_gst[tmr_type].can_set_time_en_u8 = TIMEOUT_SET;
			gt_timer_set_one_shot_delay_v(tmr_val);
		}
		break;
		case DEBUG_SM:
		{
			can_comm_set_timer_gst[tmr_type].can_set_time_en_u8 = TIMEOUT_SET;
//			gt_timer_set_one_shot_delay_v(tmr_val);
//			TIMING_StartChannel(&lptmr_0_pal_delay_instance, TIMER_LPIT_CHANNEL_0,((tmr_val * COM_HDR_SCALE_1000) / timer_lptimerpal0_resolution_gu64));
		}
		break;
		default:
		{

		}
		break;
	}
}
/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void can_commtask_tx_fault_msg_v(can_app_message_tst * tx_can_message_arpst)
{


}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
