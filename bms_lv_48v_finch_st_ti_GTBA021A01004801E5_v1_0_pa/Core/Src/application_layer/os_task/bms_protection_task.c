/**
====================================================================================================================================================================================
@page bms_protection_task_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_protection_task.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	26-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */
#include "task.h"

/* Configuration Header File */

#include "bms_afe_task.h"
#include "protect_app.h"
#include "low_pwr_app.h"
#include "diagmonitor_task.h"
#include "gt_system.h"
#include "bootsm.h"
#include "batt_param.h"

/* This header file */
#include "bms_protection_task.h"

/* Driver Header File */

/** @{
 * Private Definitions **/
bms_protection_manage_state_te manage_task_e = MANAGE_CELL_V_T_LIMITS;
/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
/** @} */
#if 0 /* Under Voltage Simulation check*/
U32 dsg_limit_check_counter_u32 = 0;

#endif

/* Public Function Definitions */

void bms_protection_task_v(void *parameter);
/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bms_protection_task_v(void *parameter)
{

	/* Unused warning removal */
	COM_HDR_UNUSED(parameter);

		/* Wait Until the OS is completely up */
		while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
		{
			vTaskDelay(10);
		}

		while(bms_afe_task_state_ge != BMS_AFE_MEASUREMENT)
		{
			vTaskDelay(10);
		}

		while(1)
		{
#if 0
			for (manage_task_e = MANAGE_CELL_V_T_LIMITS; manage_task_e < MANAGE_MAX_NUM; manage_task_e++)
			{
				switch (manage_task_e)
				{
				case MANAGE_CELL_V_T_LIMITS:
				{
#endif
					/* Monitors the failure & recovery of the failure */
					/*
					 * 1. Cell open Short - always ON
					 * 2. Cell over Voltage - always ON
					 * 3. Cell under Voltage - always ON
					 * 4. Deep Discharge Voltage - always ON
					 * 5. Cell Balance error - Configurable - OFF
					 */
					protect_app_check_cv_violation_v();

#if 0 /* Under Voltage Simulation check*/
					dsg_limit_check_counter_u32++;
					if(dsg_limit_check_counter_u32 > 105)
					{
						dsg_limit_check_counter_u32 = 105;
					}
					else if(dsg_limit_check_counter_u32 > 100)
					{
						batt_param_pack_param_gst.pack_param_min_cell_v_redchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
						batt_param_pack_param_gst.pack_param_min_cell_v_dchg_limit_active_e = PACK_PARAM_LIMIT_ACTIVE;
					}
#endif

					/*
					 * 1.Cell Open Temp (Thermistor not connected) - always ON
					 * 2.Cell Over Temperature - Charging - always ON
					 * 3.Cell Over Temperature - Dis-Charging - always ON
					 * 4.Cell Under Temperature - Charging - always ON
					 * 5.Cell Under Temperature - Dis-Charging - always ON
					 */
					protect_app_check_ct_violation_v();


					/* Warning Flags check
					 *
					 * 1.  Over Current Charging
					 * 2.  Over Current DisCharging
					 * 3.  Surge Current - DisCharging
					 * 4.  Pack Under Voltage
					 * 5.  Pack Over Voltage
					 * 6.  Cell Over Voltage
					 * 7.  Cell Under Voltage
					 * 8.  Cell deep discharge
					 * 9.  Max cell temperature - Charging
					 * 10. Max cell temperature - DisCharging
					 * 11. Min cell temperature - Charging
					 * 12. Min cell temperature - DisCharging
					 * 13. Max Pack temperature
					 * 14. Min pack temperature
					 * 15. Max AFE temperature
					 * 16. Min AFE temperature
					 * 17. Max FET temperature
					 */
					protect_app_check_warning_flgs_v();
#if 0
				}
				break;
				case MANAGE_PACK_V_T_LIMITS:
				{
#endif
					/*
					 * 1.Delta -> Pack Voltage ~ Stack Voltage (Sum of all cell voltages)
					 * 2.Pack Over Voltage
					 * 3.Pack Under Voltage
					 */
					protect_app_check_pv_violation_v();

					/*Check HV-ADC Violations*/
					protect_app_check_hvadc_violation_v();

					/*
					 * 1. Pack Over Temperature
					 * 2. Pack Under Temperature
					 */
					protect_app_check_pt_violation_v();

					/*
					 * 1. FET Over Temperature
					 * 2. FET Under Temperature
					 */
					protect_app_check_fet_violation_v();

					/*
					 * 1. AFE Die - Over Temperature
					 * 2. AFE Die - Under Temperature
					 */
					protect_app_check_afe_die_temp_violation_v();

#if 0
				}
				break;
				case MANAGE_CURRENT_LIMITS:
				{
#endif
					/*
					 * 1.Max Chg current
					 * 2.Max Dsg Current
					 * 3.Surge Current in Discharge
					 */
					protect_app_check_pi_violation_v();
#if 0
				}
				break;
				case MANAGE_CELL_BALANCING:
				{
#endif
					//cell_bal_app_balancing_ctrl_algorithm_v();
#if 0
				}
				break;
				case MANAGE_RECOVER_HW_PROTECTION:
				{
#endif
					/* 10 Seconds Delay by MCU - After AFE IC recovers from failure*/
				//	protect_app_hw_protection_recovery_v();
#if 0
				}
				break;
				case MANAGE_POWERFET_DECISION:
				{
#endif
					/* BMS - Monitor Final Decision */
					protect_app_safety_monitoring_u8();
					protect_app_backup_previous_decision_v();

					/* TODO: Re-think! Update the Batt mode after FET control and decision taken */
					protect_app_batt_mode_update_v();

#if 0
				}
				break;
				case MANAGE_ESTIMATIONS:
				{
					//vBattEstimation_Estimate_All_Parameters();
				}
				default:
				{

				}
				}
			}
#endif

			if(diag_flt_rst_flg == COM_HDR_DISABLED)
				system_diag_app_runtime_prev_flt_gu64 |= system_diag_app_runtime_flt_code_gu64;
			else
			system_diag_app_runtime_prev_flt_gu64 = 0x0000000000000000;
#if 1
		/* Low Power Mode functionality */
		while (lpapp_mode_status_gst.enable_low_power_b)
		{
			/* Loop Here */
			lpapp_task_states_agu8[TASK_BMS_PROT] = COM_HDR_TRUE;
			vTaskDelay(10);
		}

		lpapp_task_states_agu8[TASK_BMS_PROT] = COM_HDR_FALSE;
#endif
			vTaskDelay(100);

		}
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
