/**
====================================================================================================================================================================================
@page bms_curr_estimation_task_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_curr_estimation_task.c
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	30-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */
#include "common_header.h"

/* Application Header File */

#include "meas_app.h"

/* Configuration Header File */
#include "operating_system.h"
#include "bootsm.h"
#include "low_pwr_app.h"
#include "batt_estimation.h"

/* This header file */
#include "bms_soc_estimation_task.h"
#include "bq76952.h"
/* Driver Header File */
//#include "estimate.h"
#include "nmc_kalman.h"

#include "batt_param.h"
#include "bootloader_app.h"
#include "cell_bal_app.h"
#include "bms_op_cmd_task.h"
#include "storage_task.h"
#include "flash_drv.h"
#include "gt_timer.h"

/** @{
 * Private Definitions **/
static os_curr_soc_queue_tst bms_curr_soc_r_queue_st;
/** @} */

/* Private Function Prototypes */
#if 1
static void engy_tp_flash_update_prv(void);

static void engy_tp_calc_wh_1s_prv(void);
static void engy_tp_calc_wh_1m_prv(void);
#endif
/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */
U16 soc_ocv_stablization_counter_u16 = 0;
BOOL soc_first_time_b = COM_HDR_FALSE;
U16 pack_soc_u16 = 0;

double sop_dischrge_power_dfp = 0.0;
double sop_charge_power_dfp = 0.0;

double charging_current_dfp = 0.0;
double discharge_current_dfp = 0.0;

double  min_pack_soc_dfp = 0.0;
double  max_pack_soc_sfp = 0.0;
double  avg_pack_soc_sfp = 0.0;
double  ocv_soc_sfp = 3.5;

double soc_pack_current_dfp = 0.0;
double soc_mincell_voltage_dfp = 0.0;
double soc_maxcell_voltage_dfp = 0.0;
double soc_avgcell_voltage_dfp = 0.0;
double delta_time_soc_dfp = 0.0;

U8 power_limit_chg_flag_u8 = 0;
U8 power_limit_dsg_flag_u8 = 0;

U32 old_sample_time_u32 =0;
U32 new_sample_time_u32 = 0;


#if 0
BOOL chg_vtg_pwt_lmt_b = COM_HDR_FALSE;
BOOL chg_temp_pwt_lmt_b = COM_HDR_FALSE;
BOOL chg_soc_pwt_lmt_b = COM_HDR_FALSE;
BOOL chg_curr_pwt_lmt_b = COM_HDR_FALSE;
BOOL dsg_vtg_pwt_lmt_b = COM_HDR_FALSE;
BOOL dsg_temp_pwt_lmt_b = COM_HDR_FALSE;
BOOL dsg_soc_pwt_lmt_b = COM_HDR_FALSE;
BOOL dsg_curr_pwt_lmt_b = COM_HDR_FALSE;
#endif
/** @} */

/** @{
 *  Public Variable Definitions */

/** @} */
#if 1
U32 timer_u32 = 0;

//U32 timer_50ms_u8; /* to handle 1s time interval for energy structure updation*/
//
BOOL update_1min_values_b = COM_HDR_FALSE;
BOOL update_1sec_values_b = COM_HDR_FALSE;
//BOOL update_1hour_values_b = COM_HDR_FALSE;
//
//U8 timer_1sec_u8 = 0;
//U8 timer_1min_u8 = 0;
//U8 timer_1hour_u16 = 0;

U32 slave_curr_1mA_u32 = 50; /*slave consuming current : 50mA*/
U32 main_curr_1mA_u32 = 0;  /* batt_current - slave current - cell balan_current(if there) */

//U64 chg_acc_chg_u64; /* charging Ah*/
//U64 dsg_acc_chg_u64; /* discharging Ah*/
//U64 total_acc_chg_u64;  /* Charging and discharging Ah*/

U64 chg_engy_tp_u64; /* charging Wh*/
U64 dsg_engy_tp_u64; /* discharging Wh*/
//
//
//S64 mod_sum_0Wh01_s64 = 0;
//S64 pack_sum_0Wh01_s64 = 0;
//S64 previous_pack_sum_0Wh01_s64 = 0;

batt_param_energy_tst		   batt_param_update_energy_gst;
energy_cwh_calc_tst energy_cwh_calc_gast;
#endif
/* Public Function Definitions */
void bms_soc_estimation_task_v(void * parameter);

#if 0
static void engy_tp_calc_prv(void);
#endif
/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bms_soc_estimation_task_v(void * parameter)
{
	/* Unused warning removal */
			COM_HDR_UNUSED(parameter);


		/* Wait Until the OS is completely up */
		while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
		{
			vTaskDelay(10);
		}


	while (1)
	{
#if 0
		/* when system is in ideal mode for 5 min goes to sleep */
		if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_IDLE)
		{
			U32 get_tick = gt_timer_get_tick_u32();
			if(bms_tsk_sleep_flg_u8)
			{
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_CAN;
				bms_tsk_sleep_counter_u32 = get_tick + BMS_OP_IDEAL_SLEEP_COUNTER;//gt_timer_get_tick_u32()
				bms_tsk_sleep_flg_u8 = COM_HDR_FALSE;
			}
			else
			{//gt_timer_get_tick_u32()
				if(get_tick > bms_tsk_sleep_counter_u32)
				{
					lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CAN;
		//			system_sleep_flg_u8 = 1;
		//			system_sleep_counter_u32 = 0;
				}
			}
		}
		else
		{
			bms_tsk_sleep_flg_u8 = COM_HDR_TRUE;
			bms_tsk_sleep_counter_u32 = 0;
		}
#endif
		lpapp_task_states_agu8[TASK_CURR_SOC_EST] = COM_HDR_TRUE;
		if (xQueueReceive(os_curr_soc_queue_handler_ge, &bms_curr_soc_r_queue_st, portMAX_DELAY))
		{
			lpapp_task_states_agu8[TASK_CURR_SOC_EST] = COM_HDR_FALSE;
			switch (bms_curr_soc_r_queue_st.source_e)
			{
				case OS_CURR_TIMER:
				{
/*					static U32 slp_flg;
					slp_flg++;
					if(slp_flg > 500)
					{
						bq76952_sleep_int_cnf();
						slp_flg = 0;
					}*/

#if 1 /* SOC Use*/
			new_sample_time_u32 = gt_timer_get_tick_u32();
			U32 delta_time_u32 = 0;
			SFP delta_time_for_soc_sfp;
			if(new_sample_time_u32 < old_sample_time_u32)
			{
				delta_time_u32 = (BATT_PARAM_AI_FLT_VAL_U32 - old_sample_time_u32) + new_sample_time_u32;
			}
			else
			{
				delta_time_u32 = new_sample_time_u32 - old_sample_time_u32;
			}

			old_sample_time_u32 = new_sample_time_u32;
			delta_time_for_soc_sfp = (SFP)delta_time_u32/3600;
			//diagmon_task_timer_stop_agu32[9] =  delta_time_u32;
//			soc_sop_batt_param_st_obj.time_interval_f64 = delta_time_u32/1000.0;
			delta_time_soc_dfp = delta_time_u32/1000.0;

			/*Current gets updates 10ms once in afe task measurement */
			meas_app_compute_coulomb_count();



			/* battery estimations*/
			batt_estimation_estimate_all_parameters_v();

			/*** SOC  ****/
			/*r0=0.016;r1=0.0024;c1=2064;eff=0.98,q=4.51 */

			/* For SOC alone -  'mcurrent' multiply by -1 and divide by 8 */
			soc_pack_current_dfp = ((batt_param_pack_param_gst.pack_param_1mA_s32 * -1) /4000);
			if(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < BATT_PARAM_AI_INIT_VAL_U16)
			{
				soc_mincell_voltage_dfp = (double)(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 /1000.0);
				soc_maxcell_voltage_dfp = (double)(batt_param_pack_param_gst.pack_param_max_cell_1mV_u16 /1000.0);
				soc_avgcell_voltage_dfp = (double)(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16 /1000.0);
			}
//			double  min_pack_soc_dfp = 0.0;
			min_pack_soc_dfp = nmc_kalman(soc_pack_current_dfp, soc_mincell_voltage_dfp, soc_maxcell_voltage_dfp, delta_time_soc_dfp,
						0.018, 0.017, 1200, 4.3, 0.99, soc_first_time_b, 0.01, 10, 0.005);
//			min_pack_soc_dfp = nmc_kalman(soc_pack_current_dfp,soc_mincell_voltage_dfp,delta_time_soc_dfp,,,,,,soc_first_time_b);
			soc_first_time_b = COM_HDR_TRUE;
			//max_pack_soc_dfp = nmc_kalman(soc_pack_current_dfp,soc_mincell_voltage_dfp,delta_time_soc_dfp,0.016,0.0024,2064,4.75,0.98,soc_first_time_b);
			//avg_pack_soc_dfp = nmc_kalman(soc_pack_current_dfp,soc_mincell_voltage_dfp,delta_time_soc_dfp,0.016,0.0024,2064,4.75,0.98,soc_first_time_b);

			batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = min_pack_soc_dfp * 10000;
			batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = min_pack_soc_dfp * 10000;
			batt_param_pack_param_gst.pack_param_min_cell_soc_u16 =  min_pack_soc_dfp * 10000;
			batt_param_pack_param_gst.pack_param_max_cell_soc_u16 =  min_pack_soc_dfp * 10000;
			/*if(batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 >= 10000)
			{
				batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = 10000;
			}
			if(batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 >= 10000)
			{
				batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = 10000;
			}
			if(batt_param_pack_param_gst.pack_param_min_cell_soc_u16 >= 10000)
			{
				batt_param_pack_param_gst.pack_param_min_cell_soc_u16 =  10000;
			}
			if(batt_param_pack_param_gst.pack_param_max_cell_soc_u16 >=10000)
			{
				batt_param_pack_param_gst.pack_param_max_cell_soc_u16 = 10000;
			}
*/
			/*** SOC  ****/
#if 0
			if(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < BATT_PARAM_AI_INIT_VAL_U16)
			{
				if(soc_ocv_stablization_counter_u16 < 10)
				{
					soc_ocv_stablization_counter_u16++;
					//soc_init_v((batt_param_pack_param_gst.pack_param_min_cell_1mV_u16/1000.0),&pack_soc_u16);
					soc_init_v((batt_param_pack_param_gst.pack_param_min_cell_1mV_u16/1000.0),										(batt_param_pack_param_gst.pack_param_max_cell_1mV_u16/1000.0),
											(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16/1000.0));

					//batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = pack_soc_u16 * 1000;
				}
				else
				{
					/* here we calculate soc for each cell */
					soc_main_v((batt_param_pack_param_gst.pack_param_min_cell_1cc_s16/10.0),
							(soc_pack_current_sfp),
							(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16/1000.0),
							&min_pack_soc_sfp,
							(batt_param_pack_param_gst.pack_param_max_cell_1mV_u16/1000.0),
							&max_pack_soc_sfp,
							(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16/1000.0),
							&avg_pack_soc_sfp,
							&ocv_soc_sfp);

					batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = avg_pack_soc_sfp * 10000;
					batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = avg_pack_soc_sfp * 10000;
					batt_param_pack_param_gst.pack_param_min_cell_soc_u16 =  min_pack_soc_sfp * 10000;
					batt_param_pack_param_gst.pack_param_max_cell_soc_u16 =  max_pack_soc_sfp * 10000;
					if(batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 >= 10000)
					{
						batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = 10000;
					}
					if(batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 >= 10000)
					{
						batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = 10000;
					}
					if(batt_param_pack_param_gst.pack_param_min_cell_soc_u16 >= 10000)
					{
						batt_param_pack_param_gst.pack_param_min_cell_soc_u16 =  10000;
					}
					if(batt_param_pack_param_gst.pack_param_max_cell_soc_u16 >=10000)
					{
						batt_param_pack_param_gst.pack_param_max_cell_soc_u16 = 10000;
					}


					sop_main_v((batt_param_pack_param_gst.pack_param_min_cell_1cc_s16/10.0),
											soc_pack_current_sfp,
											(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16/1000.0),
											&avg_pack_soc_sfp,
											&sop_charge_power_dfp,
											&sop_dischrge_power_dfp,
											&charging_current_dfp,
											&discharge_current_dfp,
											&power_limit_chg_flag_u8,
											&power_limit_dsg_flag_u8);
					batt_param_pack_param_gst.pack_param_sop_available_chg_pwr_u32 = sop_charge_power_dfp * 1000;
					batt_param_pack_param_gst.pack_param_sop_available_dsg_pwr_u32 = sop_dischrge_power_dfp * 1000;
					batt_param_pack_param_gst.pack_param_sop_max_dis_charge_current_mA_u32 = discharge_current_dfp *1000;
					batt_param_pack_param_gst.pack_param_sop_max_charge_current_mA_u32 = (charging_current_dfp *1000); //*(-1));
				}
			}
#endif

#endif

#if 1 /* Enery tp use*/
			//engy_tp_calc_prv();

					/* Called here for con open update if happened*/
					if((batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG )
							&& (batt_param_pack_param_gst.pack_param_batt_mode_e != BATT_MODE_FAULT)
							&& (batt_param_pack_param_gst.pack_param_1mA_s32 < BATT_PARAM_AI_INIT_VAL_S32)
								&& (batt_param_cell_param_gst[0][0].cell_meas_1mV_u16 < BATT_PARAM_AI_INIT_VAL_U16))
						{

							if(update_1sec_values_b)
							{
								engy_tp_calc_wh_1s_prv();
								update_1sec_values_b = COM_HDR_FALSE;
							}

							if(update_1min_values_b)
							{
								engy_tp_calc_wh_1m_prv();
								engy_tp_flash_update_prv();
							}
						}
#endif
				}
				break;
				default:
				{

				}break;
			}
		}



		/* Low Power Mode functionality */
		while (lpapp_mode_status_gst.enable_low_power_b)
		{
			/* Loop Here */
			lpapp_task_states_agu8[TASK_CURR_SOC_EST] = COM_HDR_TRUE;
			vTaskDelay(10);
		}

		lpapp_task_states_agu8[TASK_CURR_SOC_EST] = COM_HDR_FALSE;

	}

}


void bms_curr_soc_task_callback_v(void)
{
	if(bootsm_boot_status_e == BOOTSM_SYS_OS_RUNNING)
		{
		os_curr_soc_queue_tst  curr_soc_queue_st;
		curr_soc_queue_st.source_e = OS_CURR_TIMER;

#if 1
			xQueueSendFromISR(os_curr_soc_queue_handler_ge, &curr_soc_queue_st, COM_HDR_NULL);
#endif
		}
}
#if 1/* Energy Throughput calc - not used for now*/

#if 0 /* Old Algorithm - Not used*/
/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void engy_tp_calc_prv(void)
{
	/* Only calculate when current and voltage measurement is made*/
	if((batt_param_pack_param_gst.pack_param_1mA_s32 < BATT_PARAM_AI_INIT_VAL_S32)
			&& (batt_param_cell_param_gst[0][0].cell_meas_1mV_u16 < BATT_PARAM_AI_INIT_VAL_U16))
	{
		S32 calc_1_s32 = 0;
		S32 calc_2_s32 = 0;
	    S32 calc_3_s32 = 0;
	    S32 calc_4_s32 = 0;
	    S64 energy_0Wh01_s64 = 0;
	    S32 round_error_s32;

		/* called every 50ms so Inc on every pass */
		timer_50ms_u8++;

		if (timer_50ms_u8 >= 20)
		{
			/* 1 sec has passed so reset 10ms timer and inc 1 sec values */
			timer_50ms_u8 = 0;
			timer_1sec_u8++;
		    update_1sec_values_b = COM_HDR_TRUE;
		    if (timer_1sec_u8 >= 60)
	        {   /* 1 min has passed so reset 1s timer and inc 1 min values */
		    	timer_1sec_u8 = 0;
	            timer_1min_u8++;
	            update_1min_values_b = COM_HDR_TRUE;

	            if (timer_1min_u8 >= 60)
	            {   /* 1 hour has passed so reset 1min timer and inc 1 hr values */
	            	timer_1min_u8 = 0;
	                timer_1hour_u16++;
	                update_1hour_values_b = COM_HDR_TRUE;
	            }
	            else
	            {   /* wait for one hour to elapse */
	            }
	        }
	        else
	        {   /* wait for 1 minute to elapse */
	        }
	    }
	    else
	    {   /* wait for 1 second to elapse */
	    }

		pack_sum_0Wh01_s64 = 0;

		for (U8 mod_u8 = PACK_PARAM_MOD_ID_1; mod_u8 < PACK_PARAM_MOD_ID_MAX_NUM; mod_u8++)
	    {
	        mod_sum_0Wh01_s64 = 0;

	        for (U8 cell_u8 = PACK_PARAM_CELL_ID_1; cell_u8 < BATT_PARAM_CELL_ID_MAX; cell_u8++)
	        {
	        	/* as 50ms are up we can add in the Wh */
	            main_curr_1mA_s32 = batt_param_pack_param_gst.pack_param_1mA_s32;

	            /* Subtract Slave current consumption */
	            main_curr_1mA_s32 -= slave_curr_1mA_s32;

	            if (cell_bal_config_status_gst[mod_u8].bal_register_u16 & (1 << cell_u8))
	            {   /* Subtract the balance current value if resistor is turned on */
	                main_curr_1mA_s32 -=
	                        ( batt_param_cell_param_gst[mod_u8][cell_u8].cell_meas_1mV_u16 / 28); /* Resistot 28 Ohms in Cell balancing */
	            }

	            energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_0s01_s32 +=
	                    (((main_curr_1mA_s32  * batt_param_cell_param_gst[mod_u8][cell_u8].cell_meas_1mV_u16) / COM_HDR_SCALE_10000));

	            /* Check if 1s, 1m, and 1h calculations */
	            if (COM_HDR_TRUE == update_1sec_values_b)
	            {   /* 1 second elapsed so update counters */
	                round_error_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_0s01_s32 / 20); /* 1 samples for 50ms time -> 20 samples for 1 sec */
	                energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1s_s32 += round_error_s32;
	                /* Add any rounding error back to the 50ms value */
	                energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_0s01_s32 =
	                   (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_0s01_s32 - (round_error_s32 * 20));
	                if (COM_HDR_TRUE == update_1min_values_b)
	                {   /* 1 minute elapsed so update counters */
	                    round_error_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1s_s32 / 60);  /*  1 samples for 1s time -> 60 samples for 60sec/1min  */
	                    energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1m_s32 += round_error_s32;
	                    /* Add any rounding error back to the 1s value */
	                    energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1s_s32 =
	                       (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1s_s32 - (round_error_s32 * 60));
	                    if (COM_HDR_TRUE == update_1hour_values_b)
	                    {   /* 1 hour elapsed so update counters */
	                        round_error_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1m_s32 / 60); /*  1 samples for 1min time -> 60 samples for 60min/1hr  */
	                        energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_s32 += round_error_s32;
	                        /* This division creates an error which can be removed by setting the current in the 10ms count to
	                           this error so that it is accounted for next time around */
	                        energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1m_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1m_s32 -
	                                (round_error_s32 * 60));
	                    }
	                }
	            }

	            calc_1_s32 = energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_s32;
	            calc_2_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1m_s32 / 60); /* Converting enery(Watt-min to watt-hr)*/
	            calc_3_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_1s_s32 / (3600)); /* converting energy (Watt-s to watt-hr)*/
	            calc_4_s32 = (energy_wh_calc_gast[mod_u8][cell_u8].p_0Wh01_0s01_s32 / (72000)); /* check here*/

	            energy_0Wh01_s64 = (calc_1_s32 + calc_2_s32 + calc_3_s32 + calc_4_s32);

	          //  MOD_DEF_cell_param_gast[mod_u8][cell_u8].cell_p_0Wh01_s32 = (S32) energy_0Wh01_s64;

	            /* Accumulate Wh at module level */
	            mod_sum_0Wh01_s64 += energy_0Wh01_s64;
	        }

	        /* Scale by 10 for module level kWh calculations */
	        //MOD_DEF_cell_param_gast[mod_u8].mod_0Wh01_s32 = (mod_sum_0Wh01_s64 / (S64) COM_HDR_SCALE_10);

	        /* Accumulate Wh at Pack level */
	        pack_sum_0Wh01_s64 += (mod_sum_0Wh01_s64); //MOD_DEF_module_param_gast[mod_u8].mod_0Wh01_s32;
	    }

	   //TODO Check batt_param_pack_param_gst.pack_param_energy_1cWh_u16 = (S16) (pack_sum_0Wh01_s64);

	    /* TAFE requirements */
	    if(batt_param_pack_param_gst.pack_param_1mA_s32 < 0)
	    {
	        /* During discharge */
	    	batt_param_update_energy_gst.engy_tp_u64 += 10;//(pack_sum_0Wh01_s64 * -1);
	    }
	    else
	    {
	        /* During charge */
	    	batt_param_update_energy_gst.acc_chg_u64 += 2;// (pack_sum_0Wh01_s64) / (batt_param_pack_param_gst.pack_param_1cV_u16 ) ;
	    }

	    /* Update the previous pack sum*/
	    previous_pack_sum_0Wh01_s64 = pack_sum_0Wh01_s64;

	    update_1sec_values_b = COM_HDR_FALSE;
	    update_1hour_values_b = COM_HDR_FALSE;

		/* update the energy throughput in flash*/
		engy_tp_flash_update_prv();
	}


}

#endif

/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void engy_tp_calc_wh_1s_prv(void)
{
	for (U8 mod_u8 = PACK_PARAM_MOD_ID_1; mod_u8 < BATT_PARAM_TOTAL_SLAVES; mod_u8++)
	{
	     for (U8 cell_u8 = PACK_PARAM_CELL_ID_1; cell_u8 < BATT_PARAM_CELL_ID_MAX; cell_u8++)
	     {
	    	if(batt_param_pack_param_gst.pack_param_1mA_s32 < 0)
	    	{
	    		main_curr_1mA_u32 = (-1 * batt_param_pack_param_gst.pack_param_1mA_s32) ;
	    	}
	    	else
	    	{
	    		main_curr_1mA_u32 = batt_param_pack_param_gst.pack_param_1mA_s32;
	    	}

	    	if(main_curr_1mA_u32 < slave_curr_1mA_u32)
	    	{
	    		main_curr_1mA_u32 = 0;
	    	}
	    	else
	    	{
		       	/* Subtract Slave current consumption */
		    	main_curr_1mA_u32 -= slave_curr_1mA_u32;
	    	}

	        if (cell_bal_config_status_gst.bal_register_u16 & (1 << cell_u8))
	        {
		    	if(main_curr_1mA_u32 < (batt_param_cell_param_gst[mod_u8][cell_u8].cell_meas_1mV_u16 / 28))
		    	{
		    		main_curr_1mA_u32 = 0;
		    	}
		    	else
		    	{
		    		/* Subtract the balance current value if resistor is turned on */
			    	main_curr_1mA_u32 -= ( batt_param_cell_param_gst[mod_u8][cell_u8].cell_meas_1mV_u16 / 28); /* Resistot 28 Ohms in Cell balancing */;
		    	}


	        }

	        energy_cwh_calc_gast.engy_1s_cwh_u64 +=
	        			(((main_curr_1mA_u32  * batt_param_cell_param_gst[mod_u8][cell_u8].cell_meas_1mV_u16) / COM_HDR_SCALE_10000));
	     }
	}


}


/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void engy_tp_calc_wh_1m_prv(void)
{
	if(energy_cwh_calc_gast.engy_1s_cwh_u64 > 60)
	{
		batt_param_update_energy_gst.engy_1m_ah_cwh_u64 += (energy_cwh_calc_gast.engy_1s_cwh_u64 / 60);

#if 0
		energy_cwh_calc_gast.engy_1m_tp_cwh_u64 += (energy_cwh_calc_gast.engy_1s_cwh_u64 / 60);
#endif

		energy_cwh_calc_gast.engy_1s_cwh_u64 =  energy_cwh_calc_gast.engy_1s_cwh_u64  % 60;
	}

    /* TAFE requirements */
	if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_CHG)
	{
    	if(batt_param_update_energy_gst.engy_1m_ah_cwh_u64 > (60 * batt_param_pack_param_gst.pack_param_1cV_u16 ))
    	{
    		batt_param_update_energy_gst.acc_chg_u64 += (batt_param_update_energy_gst.engy_1m_ah_cwh_u64 / ( 60 * batt_param_pack_param_gst.pack_param_1cV_u16 )) ;

    		batt_param_update_energy_gst.engy_1m_ah_cwh_u64 = batt_param_update_energy_gst.engy_1m_ah_cwh_u64 % (60 * batt_param_pack_param_gst.pack_param_1cV_u16);
    	}

#if 0
    	if(energy_cwh_calc_gast.engy_1m_tp_cwh_u64 > 60)
    	{
    		chg_engy_tp_u64 += (energy_cwh_calc_gast.engy_1m_tp_cwh_u64 / 60);

    		energy_cwh_calc_gast.engy_1m_tp_cwh_u64 =  energy_cwh_calc_gast.engy_1m_tp_cwh_u64  % 60;
    	}
#endif

	}

#if 0
	else if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_DSG)
	{

    	if(energy_cwh_calc_gast.engy_1m_ah_cwh_u64 > (60 * batt_param_pack_param_gst.pack_param_1cV_u16 ))
    	{
    		dsg_acc_chg_u64 += (energy_cwh_calc_gast.engy_1m_ah_cwh_u64 / ( 60 * batt_param_pack_param_gst.pack_param_1cV_u16 )) ;

    		energy_cwh_calc_gast.engy_1m_ah_cwh_u64 = energy_cwh_calc_gast.engy_1m_ah_cwh_u64 % (60 * batt_param_pack_param_gst.pack_param_1cV_u16);
    	}

#if 0
    	if(energy_cwh_calc_gast.engy_1m_tp_cwh_u64 > 60)
    	{
    		dsg_engy_tp_u64 += (energy_cwh_calc_gast.engy_1m_tp_cwh_u64 / 60);

    		energy_cwh_calc_gast.engy_1m_tp_cwh_u64 =  energy_cwh_calc_gast.engy_1m_tp_cwh_u64  % 60;
    	}
#endif

	}
#endif


	//total_acc_chg_u64 = /*dsg_acc_chg_u64 +*/ chg_acc_chg_u64;

	if(batt_param_update_energy_gst.acc_chg_u64 >= 33)
	{
		batt_param_update_energy_gst.tot_cycle_count_u64 += batt_param_update_energy_gst.acc_chg_u64 / 33;

		batt_param_update_energy_gst.acc_chg_u64 = batt_param_update_energy_gst.acc_chg_u64 % 33;
	}

	batt_param_pack_param_gst.pack_param_energy_1cWh_u16 = (batt_param_update_energy_gst.acc_chg_u64 * batt_param_pack_param_gst.pack_param_1cV_u16);
	batt_param_pack_param_gst.pack_param_capacity_1cAhr_f = ((batt_param_update_energy_gst.acc_chg_u64 * 100) & 0xFFFF);

#if 0
	batt_param_update_energy_gst.engy_tp_u64 = dsg_engy_tp_u64 + chg_engy_tp_u64;
#endif
//    if(batt_param_pack_param_gst.pack_param_1mA_s32 < -500)
//    {
//        /* During discharge */
//    	//batt_param_update_energy_gst.acc_chg_u64 = 0;
//
//    	if(energy_cwh_calc_gast.engy_1m_cwh_u64 > 60)
//    	{
//    		batt_param_update_energy_gst.dsg_engy_tp_u64 += (energy_cwh_calc_gast.engy_1m_cwh_u64 / 60);
//
//    		energy_cwh_calc_gast.engy_1m_cwh_u64 =  energy_cwh_calc_gast.engy_1m_cwh_u64  % 60;
//    	}
//
//    }
//    else if(batt_param_pack_param_gst.pack_param_1mA_s32 > 500)
//    {
//        /* During charge */
//    	//batt_param_update_energy_gst.engy_tp_u64 = 0;
//
//    	if(energy_cwh_calc_gast.engy_1m_cwh_u64 > (60 * batt_param_pack_param_gst.pack_param_1cV_u16 ))
//    	{
//    		batt_param_update_energy_gst.acc_chg_u64 += (energy_cwh_calc_gast.engy_1m_cwh_u64 / ( 60 * batt_param_pack_param_gst.pack_param_1cV_u16 )) ;
//
//    		energy_cwh_calc_gast.engy_1m_cwh_u64 = energy_cwh_calc_gast.engy_1m_cwh_u64 % (60 * batt_param_pack_param_gst.pack_param_1cV_u16);
//    	}
//    }
//

}
/**
=====================================================================================================================================================

@fn Name	    :
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
static void engy_tp_flash_update_prv(void)
{
    /* Store data in flash*/
   //TODO Uncomment
	if(((batt_param_pack_param_gst.pack_param_1mA_s32 > 500) || (batt_param_pack_param_gst.pack_param_1mA_s32 < -500) ) && !can_bootloader_cmd_received_gvlu8)
    //if(1)
    {
        if(update_1min_values_b )
        {
        	update_1min_values_b = COM_HDR_FALSE;

        	batt_param_energy_gst.acc_chg_u64 = batt_param_update_energy_gst.acc_chg_u64;
        	batt_param_energy_gst.engy_1m_ah_cwh_u64 = batt_param_update_energy_gst.engy_1m_ah_cwh_u64;
	        batt_param_energy_gst.tot_cycle_count_u64 = batt_param_update_energy_gst.tot_cycle_count_u64;

	        storage_task_update_batt_engy_data_st.start_addr_u32 = START_ADDRESS_ENGY_TP;
			storage_task_update_batt_engy_data_st.end_addr_u32 = END_ADDRESS_ENGY_TP;
			storage_task_update_batt_engy_data_st.size_u32 =  sizeof(batt_param_energy_gst);
			storage_task_update_batt_engy_data_st.bms_batt_upd_log_st = (void*)&batt_param_energy_gst;

			os_storage_queue_tst engy_tp_storage_queue_st;
			engy_tp_storage_queue_st.mode_e = OS_FLASH_RD_AND_SECTOR_ERASE;
			engy_tp_storage_queue_st.event_e = OS_STORAGE_UPDATE_BATT_DATA;
			engy_tp_storage_queue_st.payload_pv = &storage_task_update_batt_engy_data_st; /* dummy data must send */
			xQueueSend(os_storage_t7_qhandler_ge, &engy_tp_storage_queue_st, 2);
        }

    }
    else
    {
    	update_1min_values_b = COM_HDR_FALSE;
    }


}
/**
=====================================================================================================================================================

@fn Name	    	:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
U8 engy_tp_flash_init_u8(void)
{
	U8 ret_val_u8 = 0;

	U32 flash_start_addr_u32 = START_ADDRESS_ENGY_TP;
	ret_val_u8 += flash_drv_app_rd_data_u8((U8*)&batt_param_energy_gst, flash_start_addr_u32, sizeof(batt_param_energy_gst));

	if((ret_val_u8 == 0) && (batt_param_energy_gst.tot_cycle_count_u64 == 0xFFFFFFFFFFFFFFFF))
	//if(1)
	{

		flash_start_addr_u32 = 0;
		flash_drv_erase_flash_u8(FLASH_DRV_ERASE_MODE_SECTOR,flash_start_addr_u32 , 200);

    	batt_param_energy_gst.acc_chg_u64 = 0;
    	batt_param_energy_gst.engy_1m_ah_cwh_u64 = 0;
		batt_param_energy_gst.tot_cycle_count_u64 = 0;

		flash_start_addr_u32 = 0;
		ret_val_u8 +=flash_drv_app_wr_data_u8((U8*)&batt_param_energy_gst, &flash_start_addr_u32, sizeof(batt_param_energy_gst));

		batt_param_update_energy_gst.acc_chg_u64 = 0;
		batt_param_update_energy_gst.engy_1m_ah_cwh_u64 = 0;
		batt_param_update_energy_gst.tot_cycle_count_u64 = 0;
	}
	else
	{
		batt_param_update_energy_gst.acc_chg_u64 = batt_param_energy_gst.acc_chg_u64;
		batt_param_update_energy_gst.engy_1m_ah_cwh_u64 = batt_param_energy_gst.engy_1m_ah_cwh_u64;
		batt_param_update_energy_gst.tot_cycle_count_u64 = batt_param_energy_gst.tot_cycle_count_u64;
	}

	return ret_val_u8;
}

#endif
/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
