/**
====================================================================================================================================================================================
@page bms_con_ctrl_task_source_file

@subsection  Details

@n@b Title:      						
@n@b Filename:   	bms_con_ctrl_task.c		
@n@b MCU:       						
@n@b Compiler:   						
@n@b Author:     	NIVU					

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection  Description
@b Description:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection MISRA Exceptions
@b MISRA @b Exceptions:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@subsection Notes
@b Notes:

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@n@b Rev_by:   		#1_TL#			
@n@b Date:       	24-Jun-2020	
@n@b Description:  	Created			

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(C) Copyright Grinntech. All Rights Reserved.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

@b DISCLAIMERS:
- This software is for development purposes only, and is not suitable for
production release until verified and validated by the end customer.
- This software is provided "as is". No warranties apply, whether express,
implied or statutory, including but not limited to implied warranties of
merchantability and fitness for a particular purpose.
- Grinntech reserves the right, without notice, to make changes to this
software.
- Grinntech is not, in any circumstances, liable for special, incidental,
or consequential damages for any reason whatsoever arising out of the use or
application of this software.

====================================================================================================================================================================================

*/

/* Common Header Files */


#include "common_header.h"
//#include "sleep_app.h"
/* Application Header File */
#include "bms_afe_task.h"
#include "switch_ctrl.h"
#include "batt_param.h"
#include "bootsm.h"

/* Configuration Header File */
//#include "cont_op.h"
/* This header file */
#include "bms_op_cmd_task.h"
#include "low_pwr_app.h"

/* Driver Header File */

//#include "bms_op_cmd_task.h"
//#include "can_comm_task.h"
//#include "ms_1000_msgs.h"
#include "task.h"
#include "gt_timer.h"

/** @{
 * Private Definitions **/
/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */


/** @{
 * Private Variable Definitions */

/** @} */

/** @{
 *  Public Variable Definitions */
/** @} */
U32 bms_tsk_sleep_counter_u32 = 0;
U8 bms_tsk_sleep_flg_u8 = 1;


/* Public Function Definitions */
void bms_task_op_cmd_t6_v(void *parameter);

/**
=====================================================================================================================================================

@fn Name			:
@b Scope            :
@n@n@b Description  :
@param Input Data   :
@return Return Value:

=====================================================================================================================================================
*/
void bms_task_op_cmd_t6_v(void *parameter)
{

	/* Unused warning removal */
	COM_HDR_UNUSED(parameter);

	while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
	{
		vTaskDelay(10);
	}

	while(1)
	{
#if 0
		/* when system is in ideal mode for 5 min goes to sleep */
		if(batt_param_pack_param_gst.pack_param_batt_mode_e == BATT_MODE_IDLE)
		{
			if(bms_tsk_sleep_flg_u8)
			{
				lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_DISABLE_CAN;
				bms_tsk_sleep_counter_u32 = gt_timer_get_tick_u32() + BMS_OP_IDEAL_SLEEP_COUNTER;
				bms_tsk_sleep_flg_u8 = COM_HDR_FALSE;
			}
			else
			{
				if(gt_timer_get_tick_u32() > bms_tsk_sleep_counter_u32)
				{
					lpapp_mode_status_gst.LPAPP_current_requesting_resource_e = SLEEP_ENABLE_CAN;
//					system_sleep_flg_u8 = 1;
//					system_sleep_counter_u32 = 0;
				}
			}
		}
		else
		{
			bms_tsk_sleep_flg_u8 = COM_HDR_TRUE;
			bms_tsk_sleep_counter_u32 = 0;
		}
#endif
	}
}

/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
