/**
====================================================================================================================================================================================
 @page bms_afe_task_source_file

 @subsection  Details

 @n@b Title:
 @n@b Filename:   	bms_afe_task.c
 @n@b MCU:
 @n@b Compiler:
 @n@b Author:     	NIVU

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 @subsection  Description
 @b Description:

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 @subsection MISRA Exceptions
 @b MISRA @b Exceptions:

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 @subsection Notes
 @b Notes:

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 @n@b Rev_by:   		#1_TL#
 @n@b Date:       	11-Jun-2020
 @n@b Description:  	Created

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 (C) Copyright Grinntech. All Rights Reserved.
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 @b DISCLAIMERS:
 - This software is for development purposes only, and is not suitable for
 production release until verified and validated by the end customer.
 - This software is provided "as is". No warranties apply, whether express,
 implied or statutory, including but not limited to implied warranties of
 merchantability and fitness for a particular purpose.
 - Grinntech reserves the right, without notice, to make changes to this
 software.
 - Grinntech is not, in any circumstances, liable for special, incidental,
 or consequential damages for any reason whatsoever arising out of the use or
 application of this software.

 ====================================================================================================================================================================================

 */

/* Common Header Files */


#include "common_header.h"

/* Application Header File */
#include "cell_bal_app.h"
#include "batt_estimation.h"
#include "meas_app.h"
#include "operating_system.h"
#include "batt_param.h"

/* Configuration Header File */
#include "bootsm.h"
#include "low_pwr_app.h"
#include "can_app.h"
#include "ai_app.h"
#include "current_calib.h"

/* Driver Header File */
#include "bq76952.h"


#include "bms_afe_task.h"

#include "gt_timer.h"
//#include "estimate.h"
#include "afe_driver.h"

/** @{
 * Private Definitions **/
#define MEASUREMENT_UPDATE_TIME_10MS			(1U)
#define BAL_FET_UPDATE_TIME_10MS				(50U)
#define DIAG_AFE_UPDATE_TIME_10MS               (10U)


/** @} */

/* Private Function Prototypes */

/** @{
 * Type definition */

/** @} */

/** @{
 * Private Variable Definitions */
operating_system_bms_afe_queue_tst  r_bms_afe_queue_st;
U8 afe_fault_u8 = 0;

/** @} */

/** @{
 *  Public Variable Definitions */
U8 boot_status_u8 = 0;
bms_afe_task_state_te bms_afe_task_state_ge = BMS_AFE_TASK_START;


/** @} */
U32 timer_check_10ms_au32[100];
U32 timer_check_100ms_au32[100];

U32 timer_check_10ms_start_u32 =0;
U32 timer_check_10ms_u32 = 0;

U32 timer_check_100ms_start_u32 =0;
U32 timer_check_100ms_u32 = 0;

U32 afe_fault_count_u32 =0;

/* Public Function Definitions */
void bms_afe_management_v(void * parameter);
void bms_afe_task_timer_callback_v(void);

/**
 =====================================================================================================================================================

 @fn Name	    :
 @b Scope            :
 @n@n@b Description  :
 @param Input Data   :
 @return Return Value:

 =====================================================================================================================================================
 */
void bms_afe_management_v(void * parameter)
{
	/* Unused warning removal */
		COM_HDR_UNUSED(parameter);
	U8 ret_val_u8 = 0;

	/* Wait Until the OS is completely up */
	while (bootsm_boot_status_e != BOOTSM_SYS_OS_RUNNING)
	{
		vTaskDelay(10);
	}
	bms_afe_task_state_ge = BMS_AFE_TASK_START;
	while (1)
	{
		lpapp_task_states_agu8[TASK_BMS_AFE] = COM_HDR_TRUE;
		if (xQueueReceive(os_bms_afe_queue_handler_ge, &r_bms_afe_queue_st,portMAX_DELAY))
		{
			lpapp_task_states_agu8[TASK_BMS_AFE] = COM_HDR_FALSE;
			switch (r_bms_afe_queue_st.event_e)
			{

			case BMS_MEASURE_VTG:
			{
			//	timer_check_100ms_start_u32 = HAL_GetTick();
				/*#1 Measure Cell, Pack Voltage & Temperature  from BQ76952 */
				meas_app_rd_afe_data_u8();

				/*#1 Measure MCU_ADC Channels - MCU Die Temp and Fuse Vtg  */
				ai_app_read_ai_u8();

				/*#2 Update Cell, Pack Voltage & Temperature  from BQ76952 */
				meas_app_update_main_structure_v();


				/*#3 Cell Balancing Fet control 	*/
				cell_bal_app_bal_cntrl_algorithm_v();
				//timer_check_100ms_u32++;

				bms_afe_task_state_ge = BMS_AFE_MEASUREMENT;

				/* current calibration process */
				if((current_calib_params_gst.current_calib_flg_b == COM_HDR_ENABLED) &&
					(current_calib_params_gst.current_calib_tmr_counter_u32 >= CURRENT_CALIB_RESOLUTION))
				{
					current_calib_estm_v();
				}
#if 0
				if(timer_check_100ms_u32 == 100)
				{
					timer_check_100ms_u32 = 0;
				}

				timer_check_100ms_au32[timer_check_100ms_u32] = HAL_GetTick() ;//- timer_check_100ms_start_u32;
				timer_check_100ms_u32++;
#endif
			}
			break;
			case BMS_MEASURE_CURRENT:
			{

#if 0 /* SOC Use*/
			new_sample_time_u32 = gt_timer_get_tick_u32();
			U32 delta_time_u32 = 0;
			SFP delta_time_for_soc_sfp;
			if(new_sample_time_u32 < old_sample_time_u32)
			{
				delta_time_u32 = (BATT_PARAM_AI_FLT_VAL_U32 - old_sample_time_u32) + new_sample_time_u32;
			}
			else
			{
				delta_time_u32 = new_sample_time_u32 - old_sample_time_u32;
			}

			old_sample_time_u32 = new_sample_time_u32;
			delta_time_for_soc_sfp = (SFP)delta_time_u32/3600;
			//diagmon_task_timer_stop_agu32[9] =  delta_time_u32;
			soc_sop_batt_param_st_obj.time_interval_f64 = delta_time_u32/1000.0;
#endif

			/*#1 Update Current measurement */
			ret_val_u8 += meas_app_rd_current_sensor_data_u8();
			if(ret_val_u8 != 0)
			{
				ret_val_u8 = afe_driver_init_u8();
			}

			//	ret_val_u8 += bq76952_get_all_prot_info_u16();
			//	ret_val_u8 += bq76952_get_all_pf_info_u16();

#if 0 /* SOC Use*/
			meas_app_compute_coulomb_count();

			/* For SOC alone divide by 4000 */
			soc_pack_current_sfp = ((batt_param_pack_param_gst.pack_param_1mA_s32 * 1) /4000.0);

			/* battery estimations*/
			batt_estimation_estimate_all_parameters_v();

			/*** SOC  ****/
			if(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16 < BATT_PARAM_AI_INIT_VAL_U16)
			{
				if(soc_ocv_stablization_counter_u16 < 10)
				{
					soc_ocv_stablization_counter_u16++;
					//soc_init_v((batt_param_pack_param_gst.pack_param_min_cell_1mV_u16/1000.0),&pack_soc_u16);
					soc_init_v((batt_param_pack_param_gst.pack_param_min_cell_1mV_u16/1000.0),										(batt_param_pack_param_gst.pack_param_max_cell_1mV_u16/1000.0),
											(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16/1000.0));

					//batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = pack_soc_u16 * 1000;
				}
				else
				{
					/* here we calculate soc for each cell */
					soc_main_v((batt_param_pack_param_gst.pack_param_min_cell_1cc_s16/10.0),
							(soc_pack_current_sfp),
							(batt_param_pack_param_gst.pack_param_min_cell_1mV_u16/1000.0),
							&min_pack_soc_sfp,
							(batt_param_pack_param_gst.pack_param_max_cell_1mV_u16/1000.0),
							&max_pack_soc_sfp,
							(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16/1000.0),
							&avg_pack_soc_sfp,
							&ocv_soc_sfp);

					batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = avg_pack_soc_sfp * 10000;
					batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = avg_pack_soc_sfp * 10000;
					batt_param_pack_param_gst.pack_param_min_cell_soc_u16 =  min_pack_soc_sfp * 10000;
					batt_param_pack_param_gst.pack_param_max_cell_soc_u16 =  max_pack_soc_sfp * 10000;
					if(batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 >= 10000)
					{
						batt_param_pack_param_gst.pack_param_usable_soc_1cPC_u16 = 10000;
					}
					if(batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 >= 10000)
					{
						batt_param_pack_param_gst.pack_param_coulambic_soc_1cPC_u16 = 10000;
					}
					if(batt_param_pack_param_gst.pack_param_min_cell_soc_u16 >= 10000)
					{
						batt_param_pack_param_gst.pack_param_min_cell_soc_u16 =  10000;
					}
					if(batt_param_pack_param_gst.pack_param_max_cell_soc_u16 >=10000)
					{
						batt_param_pack_param_gst.pack_param_max_cell_soc_u16 = 10000;
					}


					sop_main_v((batt_param_pack_param_gst.pack_param_min_cell_1cc_s16/10.0),
											soc_pack_current_sfp,
											(batt_param_pack_param_gst.pack_param_avg_cell_1mV_u16/1000.0),
											&avg_pack_soc_sfp,
											&sop_charge_power_dfp,
											&sop_dischrge_power_dfp,
											&charging_current_dfp,
											&discharge_current_dfp,
											&power_limit_chg_flag_u8,
											&power_limit_dsg_flag_u8);
					batt_param_pack_param_gst.pack_param_sop_available_chg_pwr_u32 = sop_charge_power_dfp * 1000;
					batt_param_pack_param_gst.pack_param_sop_available_dsg_pwr_u32 = sop_dischrge_power_dfp * 1000;
					batt_param_pack_param_gst.pack_param_sop_max_dis_charge_current_mA_u32 = discharge_current_dfp *1000;
					batt_param_pack_param_gst.pack_param_sop_max_charge_current_mA_u32 = (charging_current_dfp *1000); //*(-1));
				}
			}
#endif

#if 0
				if(timer_check_10ms_u32 == 100)
				{
					timer_check_10ms_u32 = 0;
				}

				timer_check_10ms_au32[timer_check_10ms_u32] = HAL_GetTick() ;//- timer_check_10ms_start_u32;
				timer_check_10ms_u32++;
#endif
			}
			break;
			case BMS_FAULT_HIT:
			{
				afe_fault_count_u32++;
			}
			break;

			default: {

			}
				break;

			}

#if 1
		/* Low Power Mode functionality */
		while (lpapp_mode_status_gst.enable_low_power_b)
		{
			/* Loop Here */
			lpapp_task_states_agu8[TASK_BMS_AFE] = COM_HDR_TRUE;
			vTaskDelay(10);
		}

		lpapp_task_states_agu8[TASK_BMS_AFE] = COM_HDR_FALSE;
#endif
		}

	}

}



/****************************************************************************************************************************************************
 * EOF
 ***************************************************************************************************************************************************/
