/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: xswap.c
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

/* Include Files */
#include "xswap.h"
#include "nmc_kalman.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * Arguments    : double x[4]
 * Return Type  : void
 */
void xswap(double x[4])
{
  double temp;
  temp = x[0];
  x[0] = x[2];
  x[2] = temp;
  temp = x[1];
  x[1] = x[3];
  x[3] = temp;
}

/*
 * File trailer for xswap.c
 *
 * [EOF]
 */
