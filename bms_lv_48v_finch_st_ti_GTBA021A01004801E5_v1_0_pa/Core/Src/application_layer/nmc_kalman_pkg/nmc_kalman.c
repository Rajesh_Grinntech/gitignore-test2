/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: nmc_kalman.c
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

/* Include Files */
#include "nmc_kalman.h"
#include "nmc_kalman_data.h"
#include "nmc_kalman_initialize.h"
#include "rt_nonfinite.h"
#include "svd.h"
#include <math.h>

/* Variable Definitions */
static double X[2];
static bool X_not_empty;
static double P_x[4];

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void X_not_empty_init(void)
{
  X_not_empty = false;
}

/*
 * cell parameters
 * Ubetter
 * [r0=0.016;r1=0.0024;c1=2064;]
 * Arguments    : double mcurrent
 *                double minvoltage
 *                double maxvoltage
 *                double deltatime
 *                double R0
 *                double R1
 *                double C1
 *                double Q
 *                double eff
 *                double firsttime
 *                double s_cov
 *                double p_cov
 *                double m_cov
 * Return Type  : double
 */
double nmc_kalman(double mcurrent, double minvoltage, double maxvoltage, double
                  deltatime, double R0, double R1, double C1, double Q, double
                  eff, double firsttime, double s_cov, double p_cov, double
                  m_cov)
{
  double SOCEstimated;
  double V;
  double eta;
  double a;
  double ocv;
  double lambda;
  double v1;
  double A_tmp;
  double A[4];
  double B[2];
  double b_A[2];
  int i;
  int i1;
  double c_A[4];
  double d;
  bool p;
  if (!isInitialized_nmc_kalman) {
    nmc_kalman_initialize();
  }

  /*  convert Ah to As */
  /* time sampling rate  */
  /*  Kalman Filter Initialisation */
  if (mcurrent <= 0.0) {
    V = maxvoltage;
    eta = eff;
  } else {
    V = minvoltage;
    eta = 1.0;
  }

  a = (V - 4.174) / 0.08279;
  ocv = (V - 4.178) / 0.5935;
  SOCEstimated = 0.1802 * exp(-(a * a)) + 0.8113 * exp(-(ocv * ocv));
  if (!X_not_empty) {
    X[0] = 0.0;
    X[1] = SOCEstimated;
    X_not_empty = true;
    P_x[0] = s_cov;
    P_x[1] = s_cov * 0.0;
    P_x[2] = s_cov * 0.0;
    P_x[3] = s_cov;
  }

  if (!(firsttime == 1.0)) {
    if (mcurrent <= 0.0) {
      ocv = 3.378 * exp(0.2269 * X[1]) + -0.4309 * exp(-36.48 * X[1]);

      /* Charge OCV */
      lambda = 245613.0 * exp(-(912.0 * X[1]) / 25.0) / 15625.0 + 3.832341E+6 *
        exp(2269.0 * X[1] / 10000.0) / 5.0E+6;
    } else {
      ocv = 3.354 * exp(0.2223 * X[1]) + -0.5579 * exp(-9.944 * X[1]);

      /* Discharge OCV */
      lambda = 6.934697E+6 * exp(-(1243.0 * X[1]) / 125.0) / 1.25E+6 +
        3.727971E+6 * exp(2223.0 * X[1] / 10000.0) / 5.0E+6;
    }

    v1 = X[0];

    /*  output equation */
    /*  %% Time update */
    A_tmp = exp(-deltatime / (R1 * C1));
    A[0] = A_tmp;
    A[2] = 0.0;
    A[1] = 0.0;
    A[3] = 1.0;
    B[0] = (1.0 - A_tmp) * R1;
    B[1] = -deltatime * eta / (Q * 3600.0);
    b_A[0] = (A_tmp * X[0] + 0.0 * X[1]) + B[0] * mcurrent;
    b_A[1] = (0.0 * X[0] + X[1]) + B[1] * mcurrent;
    for (i = 0; i < 2; i++) {
      X[i] = b_A[i];
      i1 = (int)A[i + 2];
      eta = A[i] * P_x[0] + (double)i1 * P_x[1];
      d = A[i] * P_x[2] + (double)i1 * P_x[3];
      c_A[i] = eta * A_tmp + d * 0.0;
      c_A[i + 2] = eta * 0.0 + d;
    }

    A_tmp = B[0] * p_cov;
    a = B[1] * p_cov;
    P_x[0] = c_A[0] + A_tmp * B[0];
    P_x[1] = c_A[1] + a * B[0];
    P_x[2] = c_A[2] + A_tmp * B[1];
    P_x[3] = c_A[3] + a * B[1];
    a = V - ((ocv - v1) - R0 * mcurrent);

    /*  %% Measurement Update */
    eta = lambda * P_x[3];
    ocv = 1.0 / ((-(-P_x[0] + lambda * P_x[1]) + (-P_x[2] + eta) * lambda) +
                 m_cov);
    d = (-P_x[0] + P_x[2] * lambda) * ocv;
    B[0] = d;
    X[0] += d * a;
    d = (-P_x[1] + eta) * ocv;
    X[1] += d * a;
    c_A[0] = 1.0 - (-B[0]);
    c_A[1] = 0.0 - (-d);
    c_A[2] = 0.0 - B[0] * lambda;
    c_A[3] = 1.0 - d * lambda;
    for (i = 0; i < 2; i++) {
      eta = c_A[i + 2];
      A[i] = c_A[i] * P_x[0] + eta * P_x[1];
      A[i + 2] = c_A[i] * P_x[2] + eta * P_x[3];
    }

    P_x[0] = A[0];
    P_x[1] = A[1];
    P_x[2] = A[2];
    P_x[3] = A[3];

    /*  %% Checking Matrix symmetry */
    p = ((!rtIsInf(P_x[0])) && (!rtIsNaN(P_x[0])));
    if ((!p) || (rtIsInf(P_x[1]) || rtIsNaN(P_x[1]))) {
      p = false;
    }

    if ((!p) || (rtIsInf(P_x[2]) || rtIsNaN(P_x[2]))) {
      p = false;
    }

    if ((!p) || (rtIsInf(P_x[3]) || rtIsNaN(P_x[3]))) {
      p = false;
    }

    if (p) {
      svd(P_x, A, B, c_A);
    } else {
      B[0] = rtNaN;
      B[1] = rtNaN;
      c_A[0] = rtNaN;
      c_A[1] = rtNaN;
      c_A[2] = rtNaN;
      c_A[3] = rtNaN;
    }

    for (i = 0; i < 2; i++) {
      eta = c_A[i + 2];
      d = c_A[i] * B[0] + eta * 0.0;
      eta = c_A[i] * 0.0 + eta * B[1];
      A[i] = d * c_A[0] + eta * c_A[2];
      A[i + 2] = d * c_A[1] + eta * c_A[3];
    }

    c_A[0] = (((P_x[0] + P_x[0]) + A[0]) + A[0]) / 4.0;
    A_tmp = P_x[1] + P_x[2];
    c_A[3] = (((P_x[3] + P_x[3]) + A[3]) + A[3]) / 4.0;
    P_x[0] = c_A[0];
    P_x[1] = ((A_tmp + A[1]) + A[2]) / 4.0;
    P_x[2] = ((A_tmp + A[2]) + A[1]) / 4.0;
    P_x[3] = c_A[3];
    SOCEstimated = fmin(fmax(X[1], 0.0), 1.0);
  }

  return SOCEstimated;
}

/*
 * File trailer for nmc_kalman.c
 *
 * [EOF]
 */
