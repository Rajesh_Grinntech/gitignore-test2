/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: nmc_kalman_data.h
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

#ifndef NMC_KALMAN_DATA_H
#define NMC_KALMAN_DATA_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "nmc_kalman_types.h"

/* Variable Declarations */
extern bool isInitialized_nmc_kalman;

#endif

/*
 * File trailer for nmc_kalman_data.h
 *
 * [EOF]
 */
