/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: xswap.h
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

#ifndef XSWAP_H
#define XSWAP_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "nmc_kalman_types.h"

/* Function Declarations */
extern void xswap(double x[4]);

#endif

/*
 * File trailer for xswap.h
 *
 * [EOF]
 */
