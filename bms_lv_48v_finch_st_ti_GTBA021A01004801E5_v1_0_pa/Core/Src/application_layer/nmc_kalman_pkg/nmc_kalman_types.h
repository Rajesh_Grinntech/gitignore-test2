/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: nmc_kalman_types.h
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

#ifndef NMC_KALMAN_TYPES_H
#define NMC_KALMAN_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for nmc_kalman_types.h
 *
 * [EOF]
 */
