/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: nmc_kalman.h
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

#ifndef NMC_KALMAN_H
#define NMC_KALMAN_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "nmc_kalman_types.h"

/* Function Declarations */
extern void X_not_empty_init(void);
extern double nmc_kalman(double mcurrent, double minvoltage, double maxvoltage,
  double deltatime, double R0, double R1, double C1, double Q, double eff,
  double firsttime, double s_cov, double p_cov, double m_cov);

#endif

/*
 * File trailer for nmc_kalman.h
 *
 * [EOF]
 */
