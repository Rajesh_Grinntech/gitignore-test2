/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: svd.c
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

/* Include Files */
#include "svd.h"
#include "nmc_kalman.h"
#include "rt_nonfinite.h"
#include "xnrm2.h"
#include "xrotg.h"
#include "xswap.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const double A[4]
 *                double U[4]
 *                double s[2]
 *                double V[4]
 * Return Type  : void
 */
void svd(const double A[4], double U[4], double s[2], double V[4])
{
  double f;
  double sm;
  double sqds;
  double temp;
  double nrm;
  double b_s[2];
  int m;
  double e[2];
  double r;
  int k;
  int iter;
  double snorm;
  int q;
  bool exitg1;
  int qs;
  int kase;
  int iy;
  f = A[0];
  sm = A[1];
  sqds = A[2];
  temp = A[3];
  nrm = xnrm2(A);
  if (nrm > 0.0) {
    if (A[0] < 0.0) {
      b_s[0] = -nrm;
    } else {
      b_s[0] = nrm;
    }

    if (fabs(b_s[0]) >= 1.0020841800044864E-292) {
      r = 1.0 / b_s[0];
      f = r * A[0];
      sm = r * A[1];
    } else {
      f = A[0] / b_s[0];
      sm = A[1] / b_s[0];
    }

    f++;
    b_s[0] = -b_s[0];
    r = -((f * A[2] + sm * A[3]) / f);
    if (!(r == 0.0)) {
      sqds = A[2] + r * f;
      temp = A[3] + r * sm;
    }
  } else {
    b_s[0] = 0.0;
  }

  m = 2;
  b_s[1] = temp;
  e[0] = sqds;
  e[1] = 0.0;
  U[2] = 0.0;
  U[3] = 1.0;
  if (b_s[0] != 0.0) {
    r = -((f * 0.0 + sm) / f);
    if (!(r == 0.0)) {
      U[2] = r * f;
      U[3] = 1.0 + r * sm;
    }

    U[1] = -sm;
    U[0] = -f + 1.0;
  } else {
    U[1] = 0.0;
    U[0] = 1.0;
  }

  V[2] = 0.0;
  V[3] = 1.0;
  V[1] = 0.0;
  V[0] = 1.0;
  if (b_s[0] != 0.0) {
    nrm = fabs(b_s[0]);
    r = b_s[0] / nrm;
    b_s[0] = nrm;
    e[0] = sqds / r;
    for (k = 1; k < 3; k++) {
      U[k - 1] *= r;
    }
  }

  if (e[0] != 0.0) {
    nrm = fabs(e[0]);
    r = nrm / e[0];
    e[0] = nrm;
    b_s[1] = temp * r;
    V[2] = r * 0.0;
    V[3] = r;
  }

  if (b_s[1] != 0.0) {
    nrm = fabs(b_s[1]);
    r = b_s[1] / nrm;
    b_s[1] = nrm;
    for (k = 3; k < 5; k++) {
      U[k - 1] *= r;
    }
  }

  iter = 0;
  snorm = fmax(fmax(b_s[0], e[0]), fmax(b_s[1], 0.0));
  while ((m > 0) && (iter < 75)) {
    q = m - 1;
    exitg1 = false;
    while (!(exitg1 || (q == 0))) {
      r = fabs(e[0]);
      if ((r <= 2.2204460492503131E-16 * (fabs(b_s[0]) + fabs(b_s[1]))) || (r <=
           1.0020841800044864E-292) || ((iter > 20) && (r <=
            2.2204460492503131E-16 * snorm))) {
        e[0] = 0.0;
        exitg1 = true;
      } else {
        q = 0;
      }
    }

    if (q == m - 1) {
      kase = 4;
    } else {
      qs = m;
      kase = m;
      exitg1 = false;
      while ((!exitg1) && (kase >= q)) {
        qs = kase;
        if (kase == q) {
          exitg1 = true;
        } else {
          r = 0.0;
          if (kase < m) {
            r = fabs(e[0]);
          }

          if (kase > q + 1) {
            r += fabs(e[0]);
          }

          nrm = fabs(b_s[kase - 1]);
          if ((nrm <= 2.2204460492503131E-16 * r) || (nrm <=
               1.0020841800044864E-292)) {
            b_s[kase - 1] = 0.0;
            exitg1 = true;
          } else {
            kase--;
          }
        }
      }

      if (qs == q) {
        kase = 3;
      } else if (qs == m) {
        kase = 1;
      } else {
        kase = 2;
        q = qs;
      }
    }

    switch (kase) {
     case 1:
      f = e[0];
      e[0] = 0.0;
      qs = m - 1;
      for (k = qs; k >= q + 1; k--) {
        xrotg(&b_s[0], &f, &nrm, &r);
        iy = (m - 1) << 1;
        temp = nrm * V[0] + r * V[iy];
        V[iy] = nrm * V[iy] - r * V[0];
        V[0] = temp;
        iy++;
        temp = nrm * V[1] + r * V[iy];
        V[iy] = nrm * V[iy] - r * V[1];
        V[1] = temp;
      }
      break;

     case 2:
      f = e[q - 1];
      e[q - 1] = 0.0;
      for (k = q + 1; k <= m; k++) {
        xrotg(&b_s[k - 1], &f, &sm, &sqds);
        r = e[k - 1];
        f = -sqds * r;
        e[k - 1] = r * sm;
        kase = (k - 1) << 1;
        iy = (q - 1) << 1;
        temp = sm * U[kase] + sqds * U[iy];
        U[iy] = sm * U[iy] - sqds * U[kase];
        U[kase] = temp;
        iy++;
        kase++;
        temp = sm * U[kase] + sqds * U[iy];
        U[iy] = sm * U[iy] - sqds * U[kase];
        U[kase] = temp;
      }
      break;

     case 3:
      r = b_s[m - 1];
      temp = fmax(fmax(fmax(fmax(fabs(r), fabs(b_s[0])), fabs(e[0])), fabs(b_s[q])),
                  fabs(e[q]));
      sm = r / temp;
      r = b_s[0] / temp;
      nrm = e[0] / temp;
      sqds = b_s[q] / temp;
      f = ((r + sm) * (r - sm) + nrm * nrm) / 2.0;
      nrm *= sm;
      nrm *= nrm;
      if ((f != 0.0) || (nrm != 0.0)) {
        r = sqrt(f * f + nrm);
        if (f < 0.0) {
          r = -r;
        }

        r = nrm / (f + r);
      } else {
        r = 0.0;
      }

      f = (sqds + sm) * (sqds - sm) + r;
      r = sqds * (e[q] / temp);
      for (k = q + 1; k < 2; k++) {
        xrotg(&f, &r, &sm, &sqds);
        f = sm * b_s[0] + sqds * e[0];
        r = sm * e[0] - sqds * b_s[0];
        e[0] = r;
        nrm = sqds * b_s[1];
        b_s[1] *= sm;
        temp = sm * V[0] + sqds * V[2];
        V[2] = sm * V[2] - sqds * V[0];
        V[0] = temp;
        temp = sm * V[1] + sqds * V[3];
        V[3] = sm * V[3] - sqds * V[1];
        V[1] = temp;
        b_s[0] = f;
        xrotg(&b_s[0], &nrm, &sm, &sqds);
        f = sm * r + sqds * b_s[1];
        b_s[1] = -sqds * r + sm * b_s[1];
        r = sqds * e[1];
        e[1] *= sm;
        temp = sm * U[0] + sqds * U[2];
        U[2] = sm * U[2] - sqds * U[0];
        U[0] = temp;
        temp = sm * U[1] + sqds * U[3];
        U[3] = sm * U[3] - sqds * U[1];
        U[1] = temp;
      }

      e[0] = f;
      iter++;
      break;

     default:
      if (b_s[q] < 0.0) {
        b_s[q] = -b_s[q];
        kase = q << 1;
        qs = kase + 2;
        for (k = kase + 1; k <= qs; k++) {
          V[k - 1] = -V[k - 1];
        }
      }

      while ((q + 1 < 2) && (b_s[0] < b_s[1])) {
        nrm = b_s[0];
        b_s[0] = b_s[1];
        b_s[1] = nrm;
        xswap(V);
        xswap(U);
        q = 1;
      }

      iter = 0;
      m--;
      break;
    }
  }

  s[0] = b_s[0];
  s[1] = b_s[1];
}

/*
 * File trailer for svd.c
 *
 * [EOF]
 */
