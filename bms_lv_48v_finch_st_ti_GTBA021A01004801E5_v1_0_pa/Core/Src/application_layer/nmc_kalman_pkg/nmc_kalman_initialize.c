/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: nmc_kalman_initialize.c
 *
 * MATLAB Coder version            : 5.0
 * C/C++ source code generated on  : 17-Jun-2021 19:05:39
 */

/* Include Files */
#include "nmc_kalman_initialize.h"
#include "nmc_kalman.h"
#include "nmc_kalman_data.h"
#include "rt_nonfinite.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void nmc_kalman_initialize(void)
{
  rt_InitInfAndNaN();
  X_not_empty_init();
  isInitialized_nmc_kalman = true;
}

/*
 * File trailer for nmc_kalman_initialize.c
 *
 * [EOF]
 */
